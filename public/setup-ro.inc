# Create header, using MET/MEST
timestamp=$(TZ='Europe/Bucharest' date "+%d %m %Y, %H:%M")

# Name of Columns
cname_name="Nume"
cname_percent="Procent"
cname_missing_strings="Nr. de șiruri restante pt. a ajunge la 80%"
cname_statistics="Statistici"
cname_packet="Pachet"
cname_manpages="Pagini de manual"
cname_date="Data și ora"

cname_onepageuntranslated="un fișier nu este tradus complet."
cname_severalpagesuntranslated="fișiere nu sunt traduse complet."

cname_intotal1="În total "
cname_intotal2=" fișiere nu sunt traduse."

cname_webtitle="Traducerea în limba română a paginilor de manual"
cname_overviewlink="Prezentare generală a paginilor de manual în limba română"
# For overview of (untranslated) pages
cname_nottranslated="Lista fișierelor care nu sunt traduse în limba română"
cname_explanation="Următoarele pagini sunt traduse în alte limbi, dar nu și în română"
