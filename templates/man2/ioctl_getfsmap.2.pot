# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-12-22 07:27+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "ioctl_getfsmap"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-10-30"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: SH
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid "ioctl_getfsmap - retrieve the physical layout of the filesystem"
msgstr ""

#. type: SH
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid ""
"B<#include E<lt>linux/fsmap.hE<gt>  >/* Definition of B<FS_IOC_GETFSMAP>,\n"
"B<                             FM?_OF_*>, and B<*FMR_OWN_*> constants */\n"
"B<#include E<lt>sys/ioctl.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "B<int ioctl(int >I<fd>B<, FS_IOC_GETFSMAP, struct fsmap_head * >I<arg>B<);>\n"
msgstr ""

#. type: SH
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"This B<ioctl>(2)  operation retrieves physical extent mappings for a "
"filesystem.  This information can be used to discover which files are mapped "
"to a physical block, examine free space, or find known bad blocks, among "
"other things."
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"The sole argument to this operation should be a pointer to a single I<struct "
"fsmap_head>:"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"struct fsmap {\n"
"    __u32 fmr_device;      /* Device ID */\n"
"    __u32 fmr_flags;       /* Mapping flags */\n"
"    __u64 fmr_physical;    /* Device offset of segment */\n"
"    __u64 fmr_owner;       /* Owner ID */\n"
"    __u64 fmr_offset;      /* File offset of segment */\n"
"    __u64 fmr_length;      /* Length of segment */\n"
"    __u64 fmr_reserved[3]; /* Must be zero */\n"
"};\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"struct fsmap_head {\n"
"    __u32 fmh_iflags;       /* Control flags */\n"
"    __u32 fmh_oflags;       /* Output flags */\n"
"    __u32 fmh_count;        /* # of entries in array incl. input */\n"
"    __u32 fmh_entries;      /* # of entries filled in (output) */\n"
"    __u64 fmh_reserved[6];  /* Must be zero */\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    struct fsmap fmh_keys[2];  /* Low and high keys for\n"
"                                  the mapping search */\n"
"    struct fsmap fmh_recs[];   /* Returned records */\n"
"};\n"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"The two I<fmh_keys> array elements specify the lowest and highest reverse-"
"mapping key for which the application would like physical mapping "
"information.  A reverse mapping key consists of the tuple (device, block, "
"owner, offset).  The owner and offset fields are part of the key because "
"some filesystems support sharing physical blocks between multiple files and "
"therefore may return multiple mappings for a given physical block."
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"Filesystem mappings are copied into the I<fmh_recs> array, which immediately "
"follows the header data."
msgstr ""

#. type: SS
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "Fields of struct fsmap_head"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"The I<fmh_iflags> field is a bit mask passed to the kernel to alter the "
"output.  No flags are currently defined, so the caller must set this value "
"to zero."
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"The I<fmh_oflags> field is a bit mask of flags set by the kernel concerning "
"the returned mappings.  If B<FMH_OF_DEV_T> is set, then the I<fmr_device> "
"field represents a I<dev_t> structure containing the major and minor numbers "
"of the block device."
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"The I<fmh_count> field contains the number of elements in the array being "
"passed to the kernel.  If this value is 0, I<fmh_entries> will be set to the "
"number of records that would have been returned had the array been large "
"enough; no mapping information will be returned."
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"The I<fmh_entries> field contains the number of elements in the I<fmh_recs> "
"array that contain useful information."
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid "The I<fmh_reserved> fields must be set to zero."
msgstr ""

#. type: SS
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "Keys"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"The two key records in I<fsmap_head.fmh_keys> specify the lowest and highest "
"extent records in the keyspace that the caller wants returned.  A filesystem "
"that can share blocks between files likely requires the tuple (I<device>, "
"I<physical>, I<owner>, I<offset>, I<flags>)  to uniquely index any "
"filesystem mapping record.  Classic non-sharing filesystems might be able to "
"identify any record with only (I<device>, I<physical>, I<flags>).  For "
"example, if the low key is set to (8:0, 36864, 0, 0, 0), the filesystem will "
"only return records for extents starting at or above 36\\ KiB on disk.  If "
"the high key is set to (8:0, 1048576, 0, 0, 0), only records below 1\\ MiB "
"will be returned.  The format of I<fmr_device> in the keys must match the "
"format of the same field in the output records, as defined below.  By "
"convention, the field I<fsmap_head.fmh_keys[0]> must contain the low key and "
"I<fsmap_head.fmh_keys[1]> must contain the high key for the request."
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"For convenience, if I<fmr_length> is set in the low key, it will be added to "
"I<fmr_block> or I<fmr_offset> as appropriate.  The caller can take advantage "
"of this subtlety to set up subsequent calls by copying "
"I<fsmap_head.fmh_recs[fsmap_head.fmh_entries - 1]> into the low key.  The "
"function I<fsmap_advance> (defined in I<linux/fsmap.h>)  provides this "
"functionality."
msgstr ""

#. type: SS
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "Fields of struct fsmap"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"The I<fmr_device> field uniquely identifies the underlying storage device.  "
"If the B<FMH_OF_DEV_T> flag is set in the header's I<fmh_oflags> field, this "
"field contains a I<dev_t> from which major and minor numbers can be "
"extracted.  If the flag is not set, this field contains a value that must be "
"unique for each unique storage device."
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"The I<fmr_physical> field contains the disk address of the extent in bytes."
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"The I<fmr_owner> field contains the owner of the extent.  This is an inode "
"number unless B<FMR_OF_SPECIAL_OWNER> is set in the I<fmr_flags> field, in "
"which case the value is determined by the filesystem.  See the section below "
"about owner values for more details."
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"The I<fmr_offset> field contains the logical address in the mapping record "
"in bytes.  This field has no meaning if the B<FMR_OF_SPECIAL_OWNER> or "
"B<FMR_OF_EXTENT_MAP> flags are set in I<fmr_flags>."
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid "The I<fmr_length> field contains the length of the extent in bytes."
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"The I<fmr_flags> field is a bit mask of extent state flags.  The bits are:"
msgstr ""

#. type: TP
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "B<FMR_OF_PREALLOC>"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid "The extent is allocated but not yet written."
msgstr ""

#. type: TP
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "B<FMR_OF_ATTR_FORK>"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid "This extent contains extended attribute data."
msgstr ""

#. type: TP
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "B<FMR_OF_EXTENT_MAP>"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid "This extent contains extent map information for the owner."
msgstr ""

#. type: TP
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "B<FMR_OF_SHARED>"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid "Parts of this extent may be shared."
msgstr ""

#. type: TP
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "B<FMR_OF_SPECIAL_OWNER>"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"The I<fmr_owner> field contains a special value instead of an inode number."
msgstr ""

#. type: TP
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "B<FMR_OF_LAST>"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid "This is the last record in the data set."
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid "The I<fmr_reserved> field will be set to zero."
msgstr ""

#. type: SS
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "Owner values"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"Generally, the value of the I<fmr_owner> field for non-metadata extents "
"should be an inode number.  However, filesystems are under no obligation to "
"report inode numbers; they may instead report B<FMR_OWN_UNKNOWN> if the "
"inode number cannot easily be retrieved, if the caller lacks sufficient "
"privilege, if the filesystem does not support stable inode numbers, or for "
"any other reason.  If a filesystem wishes to condition the reporting of "
"inode numbers based on process capabilities, it is strongly urged that the "
"B<CAP_SYS_ADMIN> capability be used for this purpose."
msgstr ""

#. type: TP
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "The following special owner values are generic to all filesystems:"
msgstr ""

#. type: TP
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "B<FMR_OWN_FREE>"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid "Free space."
msgstr ""

#. type: TP
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "B<FMR_OWN_UNKNOWN>"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"This extent is in use but its owner is not known or not easily retrieved."
msgstr ""

#. type: TP
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "B<FMR_OWN_METADATA>"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid "This extent is filesystem metadata."
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid "XFS can return the following special owner values:"
msgstr ""

#. type: TP
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "B<XFS_FMR_OWN_FREE>"
msgstr ""

#. type: TP
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "B<XFS_FMR_OWN_UNKNOWN>"
msgstr ""

#. type: TP
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "B<XFS_FMR_OWN_FS>"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"Static filesystem metadata which exists at a fixed address.  These are the "
"AG superblock, the AGF, the AGFL, and the AGI headers."
msgstr ""

#. type: TP
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "B<XFS_FMR_OWN_LOG>"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid "The filesystem journal."
msgstr ""

#. type: TP
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "B<XFS_FMR_OWN_AG>"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"Allocation group metadata, such as the free space btrees and the reverse "
"mapping btrees."
msgstr ""

#. type: TP
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "B<XFS_FMR_OWN_INOBT>"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid "The inode and free inode btrees."
msgstr ""

#. type: TP
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "B<XFS_FMR_OWN_INODES>"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid "Inode records."
msgstr ""

#. type: TP
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "B<XFS_FMR_OWN_REFC>"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid "Reference count information."
msgstr ""

#. type: TP
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "B<XFS_FMR_OWN_COW>"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid "This extent is being used to stage a copy-on-write."
msgstr ""

#. type: TP
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "B<XFS_FMR_OWN_DEFECTIVE:>"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"This extent has been marked defective either by the filesystem or the "
"underlying device."
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid "ext4 can return the following special owner values:"
msgstr ""

#. type: TP
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "B<EXT4_FMR_OWN_FREE>"
msgstr ""

#. type: TP
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "B<EXT4_FMR_OWN_UNKNOWN>"
msgstr ""

#. type: TP
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "B<EXT4_FMR_OWN_FS>"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"Static filesystem metadata which exists at a fixed address.  This is the "
"superblock and the group descriptors."
msgstr ""

#. type: TP
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "B<EXT4_FMR_OWN_LOG>"
msgstr ""

#. type: TP
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "B<EXT4_FMR_OWN_INODES>"
msgstr ""

#. type: TP
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "B<EXT4_FMR_OWN_BLKBM>"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid "Block bit map."
msgstr ""

#. type: TP
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "B<EXT4_FMR_OWN_INOBM>"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid "Inode bit map."
msgstr ""

#. type: SH
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid "On error, -1 is returned, and I<errno> is set to indicate the error."
msgstr ""

#. type: SH
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "ERRORS"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"The error placed in I<errno> can be one of, but is not limited to, the "
"following:"
msgstr ""

#. type: TP
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "B<EBADF>"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid "I<fd> is not open for reading."
msgstr ""

#. type: TP
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "B<EBADMSG>"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid "The filesystem has detected a checksum error in the metadata."
msgstr ""

#. type: TP
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "B<EFAULT>"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid "The pointer passed in was not mapped to a valid memory address."
msgstr ""

#. type: TP
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "B<EINVAL>"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"The array is not long enough, the keys do not point to a valid part of the "
"filesystem, the low key points to a higher point in the filesystem's "
"physical storage address space than the high key, or a nonzero value was "
"passed in one of the fields that must be zero."
msgstr ""

#. type: TP
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "B<ENOMEM>"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid "Insufficient memory to process the request."
msgstr ""

#. type: TP
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "B<EOPNOTSUPP>"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid "The filesystem does not support this command."
msgstr ""

#. type: TP
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "B<EUCLEAN>"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid "The filesystem metadata is corrupt and needs repair."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "VERSIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "The B<FS_IOC_GETFSMAP> operation first appeared in Linux 4.12."
msgstr ""

#. type: SH
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "STANDARDS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "This API is Linux-specific.  Not all filesystems support it."
msgstr ""

#. type: SH
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "EXAMPLES"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid "See I<io/fsmap.c> in the I<xfsprogs> distribution for a sample program."
msgstr ""

#. type: SH
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid "B<ioctl>(2)"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr ""

#. type: Plain text
#: mageia-cauldron
#, no-wrap
msgid ""
"struct fsmap {\n"
"    __u32 fmr_device;      /* Device ID */\n"
"    __u32 fmr_flags;       /* Mapping flags */\n"
"    __u64 fmr_physical;    /* Device offset of segment */\n"
"    __u64 fmr_owner;       /* Owner ID */\n"
"    __u64 fmr_offset;      /* File offset of segment */\n"
"    __u64 fmr_length;      /* Length of segment */\n"
"    __u64 fmr_reserved[3]; /* Must be zero */\n"
"};\n"
"\\&\n"
"struct fsmap_head {\n"
"    __u32 fmh_iflags;       /* Control flags */\n"
"    __u32 fmh_oflags;       /* Output flags */\n"
"    __u32 fmh_count;        /* # of entries in array incl. input */\n"
"    __u32 fmh_entries;      /* # of entries filled in (output) */\n"
"    __u64 fmh_reserved[6];  /* Must be zero */\n"
"\\&\n"
"    struct fsmap fmh_keys[2];  /* Low and high keys for\n"
"                                  the mapping search */\n"
"    struct fsmap fmh_recs[];   /* Returned records */\n"
"};\n"
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid "Linux."
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid "Not all filesystems support it."
msgstr ""

#. type: SH
#: mageia-cauldron
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid "Linux 4.12."
msgstr ""
