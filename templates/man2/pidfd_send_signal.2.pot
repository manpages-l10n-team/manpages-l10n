# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-28 16:42+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "pidfd_send_signal"
msgstr ""

#. type: TH
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "2024-07-23"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"pidfd_send_signal - send a signal to a process specified by a file descriptor"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "Standard C library (I<libc>,\\ I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>linux/signal.hE<gt>>     /* Definition of B<SIG*> constants */\n"
"B<#include E<lt>signal.hE<gt>>           /* Definition of B<SI_*> constants */\n"
"B<#include E<lt>sys/syscall.hE<gt>>      /* Definition of B<SYS_*> constants */\n"
"B<#include E<lt>unistd.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int syscall(SYS_pidfd_send_signal, int >I<pidfd>B<, int >I<sig>B<,>\n"
"B<            siginfo_t *_Nullable >I<info>B<, unsigned int >I<flags>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<Note>: glibc provides no wrapper for B<pidfd_send_signal>(), necessitating "
"the use of B<syscall>(2)."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#.  See the very detailed commit message for kernel commit
#.  3eb39f47934f9d5a3027fe00d906a45fe3a15fad
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<pidfd_send_signal>()  system call sends the signal I<sig> to the "
"target process referred to by I<pidfd>, a PID file descriptor that refers to "
"a process."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the I<info> argument points to a I<siginfo_t> buffer, that buffer should "
"be populated as described in B<rt_sigqueueinfo>(2)."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the I<info> argument is a null pointer, this is equivalent to specifying "
"a pointer to a I<siginfo_t> buffer whose fields match the values that are "
"implicitly supplied when a signal is sent using B<kill>(2):"
msgstr ""

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<si_signo> is set to the signal number;"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<si_errno> is set to 0;"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<si_code> is set to B<SI_USER>;"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<si_pid> is set to the caller's PID; and"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<si_uid> is set to the caller's real user ID."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The calling process must either be in the same PID namespace as the process "
"referred to by I<pidfd>, or be in an ancestor of that namespace."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The I<flags> argument is reserved for future use; currently, this argument "
"must be specified as 0."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On success, B<pidfd_send_signal>()  returns 0.  On error, -1 is returned and "
"I<errno> is set to indicate the error."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EBADF>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<pidfd> is not a valid PID file descriptor."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<sig> is not a valid signal."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The calling process is not in a PID namespace from which it can send a "
"signal to the target process."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<flags> is not 0."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The calling process does not have permission to send the signal to the "
"target process."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<pidfd> doesn't refer to the calling process, and I<info.si_code> is "
"invalid (see B<rt_sigqueueinfo>(2))."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ESRCH>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The target process does not exist (i.e., it has terminated and been waited "
"on)."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Linux."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Linux 5.1."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "PID file descriptors"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The I<pidfd> argument is a PID file descriptor, a file descriptor that "
"refers to process.  Such a file descriptor can be obtained in any of the "
"following ways:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "by opening a I</proc/>pid directory;"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "using B<pidfd_open>(2); or"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"via the PID file descriptor that is returned by a call to B<clone>(2)  or "
"B<clone3>(2)  that specifies the B<CLONE_PIDFD> flag."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<pidfd_send_signal>()  system call allows the avoidance of race "
"conditions that occur when using traditional interfaces (such as "
"B<kill>(2))  to signal a process.  The problem is that the traditional "
"interfaces specify the target process via a process ID (PID), with the "
"result that the sender may accidentally send a signal to the wrong process "
"if the originally intended target process has terminated and its PID has "
"been recycled for another process.  By contrast, a PID file descriptor is a "
"stable reference to a specific process; if that process terminates, "
"B<pidfd_send_signal>()  fails with the error B<ESRCH>."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"#define _GNU_SOURCE\n"
"#include E<lt>fcntl.hE<gt>\n"
"#include E<lt>limits.hE<gt>\n"
"#include E<lt>signal.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
"#include E<lt>sys/syscall.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"\\&\n"
"static int\n"
"pidfd_send_signal(int pidfd, int sig, siginfo_t *info,\n"
"                  unsigned int flags)\n"
"{\n"
"    return syscall(SYS_pidfd_send_signal, pidfd, sig, info, flags);\n"
"}\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int        pidfd, sig;\n"
"    char       path[PATH_MAX];\n"
"    siginfo_t  info;\n"
"\\&\n"
"    if (argc != 3) {\n"
"        fprintf(stderr, \"Usage: %s E<lt>pidE<gt> E<lt>signalE<gt>\\[rs]n\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    sig = atoi(argv[2]);\n"
"\\&\n"
"    /* Obtain a PID file descriptor by opening the /proc/PID directory\n"
"       of the target process. */\n"
"\\&\n"
"    snprintf(path, sizeof(path), \"/proc/%s\", argv[1]);\n"
"\\&\n"
"    pidfd = open(path, O_RDONLY);\n"
"    if (pidfd == -1) {\n"
"        perror(\"open\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    /* Populate a \\[aq]siginfo_t\\[aq] structure for use with\n"
"       pidfd_send_signal(). */\n"
"\\&\n"
"    memset(&info, 0, sizeof(info));\n"
"    info.si_code = SI_QUEUE;\n"
"    info.si_signo = sig;\n"
"    info.si_errno = 0;\n"
"    info.si_uid = getuid();\n"
"    info.si_pid = getpid();\n"
"    info.si_value.sival_int = 1234;\n"
"\\&\n"
"    /* Send the signal. */\n"
"\\&\n"
"    if (pidfd_send_signal(pidfd, sig, &info, 0) == -1) {\n"
"        perror(\"pidfd_send_signal\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#.  SRC END
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<clone>(2), B<kill>(2), B<pidfd_open>(2), B<rt_sigqueueinfo>(2), "
"B<sigaction>(2), B<pid_namespaces>(7), B<signal>(7)"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"If the I<info> argument is a NULL pointer, this is equivalent to specifying "
"a pointer to a I<siginfo_t> buffer whose fields match the values that are "
"implicitly supplied when a signal is sent using B<kill>(2):"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "VERSIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<pidfd_send_signal>()  first appeared in Linux 5.1."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<pidfd_send_signal>()  is Linux specific."
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"#define _GNU_SOURCE\n"
"#include E<lt>fcntl.hE<gt>\n"
"#include E<lt>limits.hE<gt>\n"
"#include E<lt>signal.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
"#include E<lt>sys/syscall.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"static int\n"
"pidfd_send_signal(int pidfd, int sig, siginfo_t *info,\n"
"                  unsigned int flags)\n"
"{\n"
"    return syscall(SYS_pidfd_send_signal, pidfd, sig, info, flags);\n"
"}\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int        pidfd, sig;\n"
"    char       path[PATH_MAX];\n"
"    siginfo_t  info;\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    if (argc != 3) {\n"
"        fprintf(stderr, \"Usage: %s E<lt>pidE<gt> E<lt>signalE<gt>\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "    sig = atoi(argv[2]);\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    /* Obtain a PID file descriptor by opening the /proc/PID directory\n"
"       of the target process. */\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "    snprintf(path, sizeof(path), \"/proc/%s\", argv[1]);\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    pidfd = open(path, O_RDONLY);\n"
"    if (pidfd == -1) {\n"
"        perror(\"open\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    /* Populate a \\[aq]siginfo_t\\[aq] structure for use with\n"
"       pidfd_send_signal(). */\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    memset(&info, 0, sizeof(info));\n"
"    info.si_code = SI_QUEUE;\n"
"    info.si_signo = sig;\n"
"    info.si_errno = 0;\n"
"    info.si_uid = getuid();\n"
"    info.si_pid = getpid();\n"
"    info.si_value.sival_int = 1234;\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "    /* Send the signal. */\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    if (pidfd_send_signal(pidfd, sig, &info, 0) == -1) {\n"
"        perror(\"pidfd_send_signal\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "2024-06-15"
msgstr ""

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-11-01"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr ""

#. type: Plain text
#: mageia-cauldron
#, no-wrap
msgid ""
"#define _GNU_SOURCE\n"
"#include E<lt>fcntl.hE<gt>\n"
"#include E<lt>limits.hE<gt>\n"
"#include E<lt>signal.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
"#include E<lt>sys/syscall.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"\\&\n"
"static int\n"
"pidfd_send_signal(int pidfd, int sig, siginfo_t *info,\n"
"                  unsigned int flags)\n"
"{\n"
"    return syscall(SYS_pidfd_send_signal, pidfd, sig, info, flags);\n"
"}\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int        pidfd, sig;\n"
"    char       path[PATH_MAX];\n"
"    siginfo_t  info;\n"
"\\&\n"
"    if (argc != 3) {\n"
"        fprintf(stderr, \"Usage: %s E<lt>pidE<gt> E<lt>signalE<gt>\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    sig = atoi(argv[2]);\n"
"\\&\n"
"    /* Obtain a PID file descriptor by opening the /proc/PID directory\n"
"       of the target process. */\n"
"\\&\n"
"    snprintf(path, sizeof(path), \"/proc/%s\", argv[1]);\n"
"\\&\n"
"    pidfd = open(path, O_RDONLY);\n"
"    if (pidfd == -1) {\n"
"        perror(\"open\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    /* Populate a \\[aq]siginfo_t\\[aq] structure for use with\n"
"       pidfd_send_signal(). */\n"
"\\&\n"
"    memset(&info, 0, sizeof(info));\n"
"    info.si_code = SI_QUEUE;\n"
"    info.si_signo = sig;\n"
"    info.si_errno = 0;\n"
"    info.si_uid = getuid();\n"
"    info.si_pid = getpid();\n"
"    info.si_value.sival_int = 1234;\n"
"\\&\n"
"    /* Send the signal. */\n"
"\\&\n"
"    if (pidfd_send_signal(pidfd, sig, &info, 0) == -1) {\n"
"        perror(\"pidfd_send_signal\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr ""
