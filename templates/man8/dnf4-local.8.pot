# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-28 16:31+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "DNF4-LOCAL"
msgstr ""

#. type: TH
#: debian-unstable
#, no-wrap
msgid "Feb 12, 2025"
msgstr ""

#. type: TH
#: debian-unstable
#, no-wrap
msgid "4.10.0"
msgstr ""

#. type: TH
#: debian-unstable
#, no-wrap
msgid "dnf-plugins-core"
msgstr ""

#. type: SH
#: debian-unstable
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-unstable
msgid "dnf4-local - DNF local Plugin"
msgstr ""

#. type: SH
#: debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-unstable
msgid ""
"Automatically copy all downloaded packages to a repository on the local "
"filesystem and generating repo metadata."
msgstr ""

#. type: Plain text
#: debian-unstable
msgid "B<NOTE:>"
msgstr ""

#. type: Plain text
#: debian-unstable
msgid ""
"Generating repo metadata will work only if you have installed "
"B<createrepo_c> package."
msgstr ""

#. type: SH
#: debian-unstable
#, no-wrap
msgid "CONFIGURATION"
msgstr ""

#. type: Plain text
#: debian-unstable
msgid "B</etc/dnf/plugins/local.conf>"
msgstr ""

#. type: Plain text
#: debian-unstable
msgid ""
"The minimal content of conf file should contain B<main> and B<createrepo> "
"sections with B<enabled> parameter, otherwise plugin will not work.:"
msgstr ""

#. type: Plain text
#: debian-unstable
#, no-wrap
msgid ""
"[main]\n"
"enabled = true\n"
msgstr ""

#. type: Plain text
#: debian-unstable
#, no-wrap
msgid ""
"[createrepo]\n"
"enabled = true\n"
msgstr ""

#. type: Plain text
#: debian-unstable
msgid ""
"For B<main> section you can specify B<repodir> paramater which sets path to "
"local repository."
msgstr ""

#. type: Plain text
#: debian-unstable
msgid "Other options and comments you can find in configuration file."
msgstr ""

#. type: SH
#: debian-unstable
#, no-wrap
msgid "AUTHOR"
msgstr ""

#. type: Plain text
#: debian-unstable
msgid "See AUTHORS in your Core DNF Plugins distribution"
msgstr ""

#. type: SH
#: debian-unstable
#, no-wrap
msgid "COPYRIGHT"
msgstr ""

#.  Generated by docutils manpage writer.
#. type: Plain text
#: debian-unstable
msgid "2014, Red Hat, Licensed under GPLv2+"
msgstr ""
