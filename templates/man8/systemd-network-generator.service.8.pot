# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-28 16:53+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SYSTEMD-NETWORK-GENERATOR\\&.SERVICE"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "systemd 257.3"
msgstr ""

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "systemd-network-generator.service"
msgstr ""

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"systemd-network-generator.service, systemd-network-generator - Generate "
"network configuration from the kernel command line"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "systemd-network-generator\\&.service"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid "/usr/lib/systemd/systemd-network-generator"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid ""
"systemd-network-generator\\&.service is a system service that translates "
"I<ip=> and related settings on the kernel command line (see below) into "
"B<systemd.network>(5), B<systemd.netdev>(5), and B<systemd.link>(5)  "
"configuration files understood by B<systemd-networkd.service>(8)  and "
"B<systemd-udevd.service>(8)\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "Files are generated in /run/systemd/network/\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"Note: despite the name, this generator executes as a normal systemd service "
"and is I<not> an implementation of the B<systemd.generator>(7)  concept\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "KERNEL COMMAND LINE OPTIONS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "This tool understands the following options:"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid "I<ip=>, I<nameserver=>, I<rd\\&.route=>, I<rd\\&.peerdns=>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid "Translated into B<systemd.network>(5)  files\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid ""
"In addition to the parameters B<dracut.cmdline>(7)  defines the I<ip=> "
"option accepts the special value \"link-local\"\\&. If selected, the network "
"interfaces will be configured for link-local addressing (IPv4LL, IPv6LL) "
"only, DHCP or IPv6RA will not be enabled\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid "Added in version 245\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide
msgid "I<ifname=>, I<net\\&.ifname_policy=>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid "Translated into B<systemd.link>(5)  files\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "I<vlan=>, I<bond=>, I<bridge=>, I<bootdev=>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid "Translated into B<systemd.netdev>(5)  files\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"See B<dracut.cmdline>(7)  and B<systemd-udevd.service>(8)  for option syntax "
"and details\\&."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "CREDENTIALS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide
msgid ""
"B<systemd-network-generator> supports the service credentials logic as "
"implemented by I<ImportCredential=>/I<LoadCredential=>/I<SetCredential=> "
"(see B<systemd.exec>(5)  for details)\\&. The following credentials are used "
"when passed in:"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide
msgid ""
"I<network\\&.conf\\&.*>, I<network\\&.link\\&.*>, I<network\\&.netdev\\&.*>, "
"I<network\\&.network\\&.*>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide
msgid ""
"These credentials should contain valid B<networkd.conf>(5), "
"B<systemd.link>(5), B<systemd.netdev>(5), B<systemd.network>(5)  "
"configuration data\\&. From each matching credential a separate file is "
"created\\&. Example: a passed credential network\\&.link\\&.50-foobar will "
"be copied into a configuration file 50-foobar\\&.link\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide
msgid ""
"Note that the resulting files are created world-readable, it\\*(Aqs hence "
"recommended to not include secrets in these credentials, but supply them via "
"separate credentials directly to systemd-networkd\\&.service\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide
msgid "Added in version 256\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide
msgid ""
"Note that by default the systemd-network-generator\\&.service unit file is "
"set up to inherit the these credentials from the service manager\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "B<systemd>(1), B<systemd-networkd.service>(8), B<dracut>(8)"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "systemd 254"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "/lib/systemd/systemd-network-generator"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"systemd-network-generator\\&.service is a system service that translates "
"I<ip=> and the related settings on the kernel command line (see below) into "
"B<systemd.network>(5), B<systemd.netdev>(5), and B<systemd.link>(5)  "
"configuration files understood by B<systemd-networkd.service>(8)  and "
"B<systemd-udevd.service>(8)\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "I<ip=>, I<rd\\&.route=>, I<rd\\&.peerdns=>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "\\(em translated into B<systemd.network>(5)  files\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid "I<ifname=>, I<net\\&.ifname-policy=>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "\\(em translated into B<systemd.link>(5)  files\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "\\(em translated into B<systemd.netdev>(5)  files\\&."
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "systemd 255"
msgstr ""
