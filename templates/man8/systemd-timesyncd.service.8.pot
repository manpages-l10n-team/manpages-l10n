# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-28 16:53+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SYSTEMD-TIMESYNCD\\&.SERVICE"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "systemd 257.3"
msgstr ""

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "systemd-timesyncd.service"
msgstr ""

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"systemd-timesyncd.service, systemd-timesyncd - Network Time Synchronization"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "systemd-timesyncd\\&.service"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid "/usr/lib/systemd/systemd-timesyncd"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"systemd-timesyncd is a system service that may be used to synchronize the "
"local system clock with a remote Network Time Protocol (NTP) server\\&. It "
"also saves the local time to disk every time the clock has been synchronized "
"and uses this to possibly advance the system realtime clock on subsequent "
"reboots to ensure it (roughly) monotonically advances even if the system "
"lacks a battery-buffered RTC chip\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"The systemd-timesyncd service implements SNTP only\\&. This minimalistic "
"service will step the system clock for large offsets or slowly adjust it for "
"smaller deltas\\&. Complex use cases that require full NTP support (and "
"where SNTP is not sufficient) are not covered by systemd-timesyncd\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"The NTP servers contacted are determined from the global settings in "
"B<timesyncd.conf>(5), the per-link static settings in \\&.network files, and "
"the per-link dynamic settings received over DHCP\\&. See "
"B<systemd.network>(5)  for further details\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"B<timedatectl>(1)\\*(Aqs B<set-ntp> command may be used to enable and start, "
"or disable and stop this service\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"B<timedatectl>(1)\\*(Aqs B<timesync-status> or B<show-timesync> command can "
"be used to show the current status of this service\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"systemd-timesyncd initialization delays the start of units that are ordered "
"after time-set\\&.target (see B<systemd.special>(7)  for details) until the "
"local time has been updated from /var/lib/systemd/timesync/clock (see below) "
"in order to make it roughly monotonic\\&. It does not delay other units "
"until synchronization with an accurate reference time sources has been "
"reached\\&. Use B<systemd-time-wait-sync.service>(8)  to achieve that, which "
"will delay start of units that are ordered after time-sync\\&.target until "
"synchronization to an accurate reference clock is reached\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide
msgid ""
"systemd and systemd-timesyncd advance the system clock to the \"epoch\" (the "
"lowest date above which the system clock time is assumed to be set correctly)"
"\\&. See \"System clock epoch\" section in B<systemd>(1)  for details\\&.  "
"systemd will set the clock when initializing, but /var/lib/systemd/timesync/"
"clock might not yet be available at that point\\&.  systemd-timesyncd will "
"advance the clock when it is started and notices that the system clock is "
"before the modification time of /var/lib/systemd/timesync/clock\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "FILES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "/var/lib/systemd/timesync/clock"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"The modification time (\"mtime\") of this file is updated on each successful "
"NTP synchronization or after each I<SaveIntervalSec=> time interval, as "
"specified in B<timesyncd.conf>(5)\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide
msgid ""
"If present, the modification time of this file is used for the epoch by "
"B<systemd>(1)  and systemd-timesyncd\\&.service\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid "Added in version 219\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "/run/systemd/timesync/synchronized"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide
msgid ""
"A file that is touched on each successful synchronization to assist systemd-"
"time-wait-sync and other applications in detecting synchronization to an "
"accurate reference clock\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid "Added in version 239\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"B<systemd>(1), B<timesyncd.conf>(5), B<systemd.network>(5), B<systemd-"
"networkd.service>(8), B<systemd-time-wait-sync.service>(8), "
"B<systemd.special>(7), B<timedatectl>(1), B<localtime>(5), B<hwclock>(8)"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "systemd 254"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "/lib/systemd/systemd-timesyncd"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"When initializing, the local clock is advanced to the modification time of "
"this file (if the file timestamp is in the past this adjustment is not made)"
"\\&. If the file does not exist yet, the clock is instead advanced to the "
"modification time of /usr/lib/clock-epoch \\(en if it exists \\(en or to a "
"time derived from the source tree at build time\\&. This mechanism is used "
"to ensure that the system clock remains somewhat reasonably initialized and "
"roughly monotonic across reboots, in case no battery-buffered local RTC is "
"available\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid "/usr/lib/clock-epoch"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"The modification time (\"mtime\") of this file is used for advancing the "
"system clock in case /var/lib/systemd/timesync/clock does not exist yet, see "
"above\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"A file that is touched on each successful synchronization, to assist systemd-"
"time-wait-sync and other applications to detecting synchronization with "
"accurate reference clocks\\&."
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "systemd 255"
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid "Added in version 254\\&."
msgstr ""
