# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-28 16:51+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SYSTEMD-BLESS-BOOT\\&.SERVICE"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "systemd 257.3"
msgstr ""

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "systemd-bless-boot.service"
msgstr ""

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"systemd-bless-boot.service, systemd-bless-boot - Mark current boot process "
"as successful"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "systemd-bless-boot\\&.service"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid "/usr/lib/systemd/systemd-bless-boot"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"systemd-bless-boot\\&.service is a system service that marks the current "
"boot process as successful\\&. It\\*(Aqs automatically pulled into the "
"initial transaction when B<systemd-bless-boot-generator>(8)  detects that "
"B<systemd-boot>(7)  style boot counting is used\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"Internally, the service operates based on the I<LoaderBootCountPath> EFI "
"variable (of the vendor UUID B<4a67b082-0a4c-41cf-b6c7-440b29bb8c4f>), which "
"is passed from the boot loader to the OS\\&. It contains a file system path "
"(relative to the EFI system partition) of the \\m[blue]B<Boot Loader "
"Specification>\\m[]\\&\\s-2\\u[1]\\d\\s+2 compliant boot loader entry file "
"or unified kernel image file that was used to boot up the system\\&.  "
"B<systemd-bless-boot\\&.service> removes the two \\*(Aqtries done\\*(Aq and "
"\\*(Aqtries left\\*(Aq numeric boot counters from the filename, which "
"indicates to future invocations of the boot loader that the entry has "
"completed booting successfully at least once\\&. (This service will hence "
"rename the boot loader entry file or unified kernel image file on the first "
"successful boot\\&.)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid ""
"The /usr/lib/systemd/systemd-bless-boot executable may also be invoked from "
"the command line, taking one of the following command arguments:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "B<status>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"The current status of the boot loader entry file or unified kernel image "
"file is shown\\&. This outputs one of \"good\", \"bad\", \"indeterminate\", "
"\"clean\", depending on the state and previous invocations of the "
"command\\&. The string \"indeterminate\" is shown initially after boot, "
"before it has been marked as \"good\" or \"bad\"\\&. The string \"good\" is "
"shown after the boot was marked as \"good\" with the B<good> command below, "
"and \"bad\" conversely after the B<bad> command was invoked\\&. The string "
"\"clean\" is returned when boot counting is currently not in effect\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "This command is implied if no command argument is specified\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid "Added in version 240\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "B<good>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"When invoked, the current boot loader entry file or unified kernel image "
"file will be marked as \"good\", executing the file rename operation "
"described above\\&. This command is intended to be invoked at the end of a "
"successful boot\\&. The systemd-bless-boot\\&.service unit invokes this "
"command\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "B<bad>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"When called the \\*(Aqtries left\\*(Aq counter in the boot loader entry file "
"name or unified kernel image file name is set to zero, marking the boot "
"loader entry or kernel image as \"bad\", so that the boot loader won\\*(Aqt "
"consider it anymore on future boots (at least as long as there are other "
"entries available that are not marked \"bad\" yet)\\&. This command is "
"normally not executed, but can be used to instantly put an end to the boot "
"counting logic if a problem is detected and persistently mark the boot entry "
"as bad\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "B<indeterminate>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"This command undoes any marking of the current boot loader entry file or "
"unified kernel image file as good or bad\\&. This is implemented by renaming "
"the boot loader entry file or unified kernel image file back to the path "
"encoded in the I<LoaderBootCountPath> EFI variable\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "Print a short help text and exit\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "B<--version>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "Print a short version string and exit\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "B<systemd>(1), B<systemd-boot>(7), B<systemd.special>(7)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid " 1."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "Boot Loader Specification"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"\\%https://uapi-group.org/specifications/specs/boot_loader_specification"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "systemd 254"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "/lib/systemd/systemd-bless-boot"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The /lib/systemd/systemd-bless-boot executable may also be invoked from the "
"command line, taking one of the following command arguments:"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "systemd 255"
msgstr ""
