# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-28 16:54+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SYSTEMD-VOLATILE-ROOT\\&.SERVICE"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "systemd 257.3"
msgstr ""

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "systemd-volatile-root.service"
msgstr ""

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"systemd-volatile-root.service, systemd-volatile-root - Make the root file "
"system volatile"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "systemd-volatile-root\\&.service"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid "/usr/lib/systemd/systemd-volatile-root"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"systemd-volatile-root\\&.service is a service that replaces the root "
"directory with a volatile memory file system (\"tmpfs\"), mounting the "
"original (non-volatile)  /usr/ inside it read-only\\&. This way, vendor data "
"from /usr/ is available as usual, but all configuration data in /etc/, all "
"state data in /var/ and all other resources stored directly under the root "
"directory are reset on boot and lost at shutdown, enabling fully stateless "
"systems\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"This service is only enabled if full volatile mode is selected, for example "
"by specifying \"systemd\\&.volatile=yes\" on the kernel command line\\&. "
"This service runs only in the initrd, before the system transitions to the "
"host\\*(Aqs root directory\\&. Note that this service is not used if "
"\"systemd\\&.volatile=state\" is used, as in that mode the root directory is "
"non-volatile\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "B<systemd>(1), B<systemd-fstab-generator>(8), B<kernel-command-line>(7)"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "systemd 254"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "/lib/systemd/systemd-volatile-root"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "systemd 255"
msgstr ""
