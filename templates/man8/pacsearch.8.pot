# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-28 16:42+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux
#, no-wrap
msgid "PACSEARCH"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "2025-02-01"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "Pacman-contrib 1\\&.11\\&.0"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "Pacman-contrib Manual"
msgstr ""

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux
msgid "pacsearch - a colorized search combining both -Ss and -Qs output"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux
msgid "I<pacsearch> [-n] E<lt>patternE<gt>"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"I<pacsearch> will perform a pacman search using both the local and the sync "
"databases\\&. Installed packages are easily identified with a [installed], "
"and local-only packages are also listed\\&. Example: pacsearch ^gnome"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-n, --nocolor>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Do no colorize output\\&."
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-h, --help>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Display syntax and command-line options\\&."
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-V, --version>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Display version information\\&."
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<pacman>(8)"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "BUGS"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Bugs? You must be kidding; there are no bugs in this software\\&. But if we "
"happen to be wrong, file an issue with as much detail as possible at https://"
"gitlab\\&.archlinux\\&.org/pacman/pacman-contrib/-/issues/new\\&."
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "AUTHORS"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Current maintainers:"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Johannes Löthberg E<lt>johannes@kyriasis\\&.comE<gt>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Daniel M\\&. Capella E<lt>polyzen@archlinux\\&.orgE<gt>"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"For additional contributors, use git shortlog -s on the pacman-"
"contrib\\&.git repository\\&."
msgstr ""
