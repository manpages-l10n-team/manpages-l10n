# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-28 16:40+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MODINFO"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "2025-02-21"
msgstr ""

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "kmod"
msgstr ""

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "modinfo"
msgstr ""

#. #-#-#-#-#  archlinux: modinfo.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  debian-bookworm: modinfo.8.pot (PACKAGE VERSION)  #-#-#-#-#
#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#. #-#-#-#-#  debian-unstable: modinfo.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  fedora-42: modinfo.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  fedora-rawhide: modinfo.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  mageia-cauldron: modinfo.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  opensuse-leap-16-0: modinfo.8.pot (PACKAGE VERSION)  #-#-#-#-#
#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#. #-#-#-#-#  opensuse-tumbleweed: modinfo.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "modinfo - Show information about a Linux Kernel module"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<modinfo> [B<-0>] [B<-F> I<field>] [B<-k> I<kernel>] [modulename|filename."
"\\&.\\&.\\&]"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<modinfo> B<-V>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<modinfo> B<-h>"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<modinfo> extracts information from the Linux Kernel modules given on the "
"command line.\\& If the module name is not a filename, then the /lib/"
"modules/ I<version> directory is searched, as is also done by B<modprobe>(8) "
"when loading kernel modules.\\&"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<modinfo> by default lists each attribute of the module in form "
"I<fieldname> : I<value>, for easy reading.\\& The filename is listed the "
"same way (although it'\\&s not really an attribute).\\&"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"This version of B<modinfo> can understand modules of any Linux Kernel "
"architecture.\\&"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-rawhide opensuse-leap-16-0
msgid "B<-V>, B<--version>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Print the B<modinfo> version.\\&"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide
msgid "B<-F> I<field>, B<--field>=I<field>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Only print this I<field> value, one per line.\\& This is most useful for "
"scripts.\\& Field names are case-insensitive.\\& Common fields (which may "
"not be in every module) include author, description, license, parm, depends, "
"and alias.\\& There are often multiple parm, alias and depends fields.\\& "
"The special I<field> filename lists the filename of the module.\\&"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide
msgid "B<-b> I<basedir>, B<--basedir>=I<basedir>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Root directory for modules, / by default.\\&"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<-k> I<kernel>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Provide information about a kernel other than the running one.\\& This is "
"particularly useful for distributions needing to extract information from a "
"newly installed (but not yet running) set of kernel modules.\\& For example, "
"you wish to find which firmware files are needed by various modules in a new "
"kernel for which you must make an initrd/initramfs image prior to booting.\\&"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-rawhide opensuse-leap-16-0
msgid "B<-0>, B<--null>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Use the ASCII zero character to separate I<field> values, instead of a new "
"line.\\& This is useful for scripts, since a new line can theoretically "
"appear inside a I<field>.\\&"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-rawhide opensuse-leap-16-0
msgid ""
"B<-a> B<--author>, B<-d> B<--description>, B<-l> B<--license>, B<-p> B<--"
"parameters>, B<-n> B<--filename>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"These are shortcuts for the B<--field> flag'\\&s author, description, "
"license, parm and filename arguments, to ease the transition from the old "
"modutils B<modinfo>.\\&"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide
msgid "Print the help message and exit.\\&"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"This manual page originally Copyright 2003, Rusty Russell, IBM Corporation."
"\\&"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<modprobe>(8)"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-rawhide
#, no-wrap
msgid "BUGS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide
msgid ""
"Please direct any bug reports to kmod'\\&s issue tracker at https://github."
"\\&com/kmod-project/kmod/issues/ alongside with version used, steps to "
"reproduce the problem and the expected outcome.\\&"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AUTHORS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Numerous contributions have come from the linux-modules mailing list "
"E<lt>linux-modules@vger.\\&kernel.\\&orgE<gt> and Github.\\& If you have a "
"clone of kmod.\\&git itself, the output of B<git-shortlog>(1) and B<git-"
"blame>(1) can show you the authors for specific parts of the project.\\&"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<Lucas De Marchi> E<lt>lucas.\\&de.\\&marchi@gmail.\\&comE<gt> is the "
"current maintainer of the project.\\&"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "12/10/2022"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid ""
"B<modinfo> [B<-0>] [B<-F\\ >I<field>] [B<-k\\ >I<kernel>] [modulename|"
"filename...]"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid "B<modinfo -V>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid "B<modinfo -h>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid ""
"B<modinfo> extracts information from the Linux Kernel modules given on the "
"command line\\&. If the module name is not a filename, then the /lib/modules/"
"I<version> directory is searched, as is also done by B<modprobe>(8)  when "
"loading kernel modules\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid ""
"B<modinfo> by default lists each attribute of the module in form "
"I<fieldname> : I<value>, for easy reading\\&. The filename is listed the "
"same way (although it\\*(Aqs not really an attribute)\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid ""
"This version of B<modinfo> can understand modules of any Linux Kernel "
"architecture\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid "Print the modinfo version\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid "B<-F>, B<--field>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid ""
"Only print this field value, one per line\\&. This is most useful for "
"scripts\\&. Field names are case-insensitive\\&. Common fields (which may "
"not be in every module) include author, description, license, parm, depends, "
"and alias\\&. There are often multiple parm, alias and depends fields\\&. "
"The special field filename lists the filename of the module\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid "B<-b >I<basedir>, B<--basedir >I<basedir>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid "Root directory for modules, / by default\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid "B<-k >I<kernel>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid ""
"Provide information about a kernel other than the running one\\&. This is "
"particularly useful for distributions needing to extract information from a "
"newly installed (but not yet running) set of kernel modules\\&. For example, "
"you wish to find which firmware files are needed by various modules in a new "
"kernel for which you must make an initrd/initramfs image prior to booting\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid ""
"Use the ASCII zero character to separate field values, instead of a new "
"line\\&. This is useful for scripts, since a new line can theoretically "
"appear inside a field\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid ""
"These are shortcuts for the B<--field> flag\\*(Aqs author, description, "
"license, parm and filename arguments, to ease the transition from the old "
"modutils B<modinfo>\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid ""
"This manual page originally Copyright 2003, Rusty Russell, IBM "
"Corporation\\&. Maintained by Jon Masters and others\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid "B<Jon Masters> E<lt>\\&jcm@jonmasters\\&.org\\&E<gt>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid "Developer"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid "B<Lucas De Marchi> E<lt>\\&lucas\\&.de\\&.marchi@gmail\\&.com\\&E<gt>"
msgstr ""

#. type: TH
#: debian-unstable
#, no-wrap
msgid "2025-02-25"
msgstr ""

#. type: TH
#: fedora-42 mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "2024-08-13"
msgstr ""

#. type: Plain text
#: fedora-42 mageia-cauldron opensuse-tumbleweed
msgid "B<-V> B<--version>"
msgstr ""

#. type: Plain text
#: fedora-42 mageia-cauldron opensuse-tumbleweed
msgid "B<-F> I<field> B<--field> I<field>"
msgstr ""

#. type: Plain text
#: fedora-42 mageia-cauldron opensuse-tumbleweed
msgid "B<-b> I<basedir> B<--basedir> I<basedir>"
msgstr ""

#. type: Plain text
#: fedora-42 mageia-cauldron opensuse-tumbleweed
msgid "B<-0> B<--null>"
msgstr ""

#. type: Plain text
#: fedora-42 mageia-cauldron opensuse-tumbleweed
msgid ""
"B<-a> B<--author> B<-d> B<--description> B<-l> B<--license> B<-p> B<--"
"parameters> B<-n> B<--filename>"
msgstr ""

#. type: TH
#: fedora-rawhide
#, no-wrap
msgid "2025-02-24"
msgstr ""

#. type: TH
#: opensuse-leap-16-0
#, no-wrap
msgid "03/05/2024"
msgstr ""
