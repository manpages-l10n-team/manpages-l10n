# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-12-06 18:00+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "GRUB-TERMINFO"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "May 2005"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "grub-terminfo (GNU GRUB 0.97)"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "FSF"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "grub-terminfo - Generate a terminfo command from a terminfo name"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<grub-terminfo> I<TERMNAME>"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Generate a terminfo command from a terminfo name."
msgstr ""

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "print this message and exit"
msgstr ""

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<-v>, B<--version>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "print the version information and exit"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The full documentation for B<grub-terminfo> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-terminfo> programs are properly installed "
"at your site, the command"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<info grub-terminfo>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "should give you access to the complete manual."
msgstr ""
