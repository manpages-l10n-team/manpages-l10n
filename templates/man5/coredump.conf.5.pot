# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-28 16:31+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "COREDUMP\\&.CONF"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "systemd 257.3"
msgstr ""

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "coredump.conf"
msgstr ""

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "coredump.conf, coredump.conf.d - Core dump storage configuration files"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "/etc/systemd/coredump\\&.conf"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "/run/systemd/coredump\\&.conf"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "/usr/local/lib/systemd/coredump\\&.conf"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "/usr/lib/systemd/coredump\\&.conf"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "/etc/systemd/coredump\\&.conf\\&.d/*\\&.conf"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "/run/systemd/coredump\\&.conf\\&.d/*\\&.conf"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "/usr/local/lib/systemd/coredump\\&.conf\\&.d/*\\&.conf"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "/usr/lib/systemd/coredump\\&.conf\\&.d/*\\&.conf"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"These files configure the behavior of B<systemd-coredump>(8), a handler for "
"core dumps invoked by the kernel\\&. Whether B<systemd-coredump> is used is "
"determined by the kernel\\*(Aqs I<kernel\\&.core_pattern> B<sysctl>(8)  "
"setting\\&. See B<systemd-coredump>(8)  and B<core>(5)  pages for the "
"details\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "CONFIGURATION DIRECTORIES AND PRECEDENCE"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"The default configuration is set during compilation, so configuration is "
"only needed when it is necessary to deviate from those defaults\\&. The main "
"configuration file is loaded from one of the listed directories in order of "
"priority, only the first file found is used: /etc/systemd/, /run/systemd/, /"
"usr/local/lib/systemd/ \\&\\s-2\\u[1]\\d\\s+2, /usr/lib/systemd/\\&. The "
"vendor version of the file contains commented out entries showing the "
"defaults as a guide to the administrator\\&. Local overrides can also be "
"created by creating drop-ins, as described below\\&. The main configuration "
"file can also be edited for this purpose (or a copy in /etc/ if it\\*(Aqs "
"shipped under /usr/), however using drop-ins for local configuration is "
"recommended over modifications to the main configuration file\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"In addition to the main configuration file, drop-in configuration snippets "
"are read from /usr/lib/systemd/*\\&.conf\\&.d/, /usr/local/lib/systemd/"
"*\\&.conf\\&.d/, and /etc/systemd/*\\&.conf\\&.d/\\&. Those drop-ins have "
"higher precedence and override the main configuration file\\&. Files in the "
"*\\&.conf\\&.d/ configuration subdirectories are sorted by their filename in "
"lexicographic order, regardless of in which of the subdirectories they "
"reside\\&. When multiple files specify the same option, for options which "
"accept just a single value, the entry in the file sorted last takes "
"precedence, and for options which accept a list of values, entries are "
"collected as they occur in the sorted files\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"When packages need to customize the configuration, they can install drop-ins "
"under /usr/\\&. Files in /etc/ are reserved for the local administrator, who "
"may use this logic to override the configuration files installed by vendor "
"packages\\&. Drop-ins have to be used to override package drop-ins, since "
"the main configuration file has lower precedence\\&. It is recommended to "
"prefix all filenames in those subdirectories with a two-digit number and a "
"dash, to simplify the ordering\\&. This also defines a concept of drop-in "
"priorities to allow OS vendors to ship drop-ins within a specific range "
"lower than the range used by users\\&. This should lower the risk of package "
"drop-ins overriding accidentally drop-ins defined by users\\&. It is "
"recommended to use the range 10-40 for drop-ins in /usr/ and the range 60-90 "
"for drop-ins in /etc/ and /run/, to make sure that local and transient drop-"
"ins take priority over drop-ins shipped by the OS vendor\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"To disable a configuration file supplied by the vendor, the recommended way "
"is to place a symlink to /dev/null in the configuration directory in /etc/, "
"with the same filename as the vendor configuration file\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "All options are configured in the [Coredump] section:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<Storage=>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Controls where to store cores\\&. One of \"none\", \"external\", and "
"\"journal\"\\&. When \"none\", the core dumps may be logged (including the "
"backtrace if possible), but not stored permanently\\&. When \"external\" "
"(the default), cores will be stored in /var/lib/systemd/coredump/\\&. When "
"\"journal\", cores will be stored in the journal and rotated following "
"normal journal rotation patterns\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"When cores are stored in the journal, they might be compressed following "
"journal compression settings, see B<journald.conf>(5)\\&. When cores are "
"stored externally, they will be compressed by default, see below\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Note that in order to process a coredump (i\\&.e\\&. extract a stack trace) "
"the core must be written to disk first\\&. Thus, unless I<ProcessSizeMax=> "
"is set to 0 (see below), the core will be written to /var/lib/systemd/"
"coredump/ either way (under a temporary filename, or even in an unlinked "
"file), I<Storage=> thus only controls whether to leave it there even after "
"it was processed\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Added in version 215\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<Compress=>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Controls compression for external storage\\&. Takes a boolean argument, "
"which defaults to \"yes\"\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<ProcessSizeMax=>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The maximum size in bytes of a core which will be processed\\&. Core dumps "
"exceeding this size may be stored, but the stack trace will not be "
"generated\\&. Like other sizes in this same config file, the usual suffixes "
"to the base of 1024 are allowed (B, K, M, G, T, P, and E)\\&. Defaults to 1G "
"on 32-bit systems, 32G on 64-bit systems\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Setting I<Storage=none> and I<ProcessSizeMax=0> disables all coredump "
"handling except for a log entry\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "I<EnterNamespace=>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Controls whether B<systemd-coredump> will attempt to use the mount tree of a "
"process that crashed in PID namespace\\&. Access to the namespace\\*(Aqs "
"mount tree might be necessary to generate a fully symbolized backtrace\\&. "
"If set to \"yes\", then B<systemd-coredump> will obtain the mount tree from "
"corresponding mount namespace and will try to generate the stack trace using "
"the binary and libraries from the mount namespace\\&. Note that the coredump "
"of the namespaced process might still be saved in /var/lib/systemd/coredump/ "
"even if I<EnterNamespace=> is set to \"no\"\\&. Defaults to \"no\"\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Added in version 257\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<ExternalSizeMax=>, I<JournalSizeMax=>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The maximum (compressed or uncompressed) size in bytes of a coredump to be "
"saved in separate files on disk (default: 1G on 32-bit systems, 32G on 64-"
"bit systems) or in the journal (default: 767M)\\&. Note that the journal "
"service enforces a hard limit on journal log records of 767M, and will "
"ignore larger submitted log records\\&. Hence, I<JournalSizeMax=> may be "
"lowered relative to the default, but not increased\\&. Unit suffixes are "
"allowed just as in B<ProcessSizeMax=>\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<ExternalSizeMax=infinity> sets the core size to unlimited\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<MaxUse=>, I<KeepFree=>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Enforce limits on the disk space, specified in bytes, taken up by externally "
"stored core dumps\\&. Unit suffixes are allowed just as in "
"B<ProcessSizeMax=>\\&.  B<MaxUse=> makes sure that old core dumps are "
"removed as soon as the total disk space taken up by core dumps grows beyond "
"this limit (defaults to 10% of the total disk size)\\&.  B<KeepFree=> "
"controls how much disk space to keep free at least (defaults to 15% of the "
"total disk size)\\&. Note that the disk space used by core dumps might "
"temporarily exceed these limits while core dumps are processed\\&. Note that "
"old core dumps are also removed based on time via B<systemd-tmpfiles>(8)\\&. "
"Set either value to 0 to turn off size-based cleanup\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The defaults for all values are listed as comments in the template /etc/"
"systemd/coredump\\&.conf file that is installed by default\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<systemd-journald.service>(8), B<coredumpctl>(1), B<systemd-tmpfiles>(8)"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: IP
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid " 1."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"💣💥🧨💥💥💣 Please note that those configuration files must be available at "
"all times. If /usr/local/ is a separate partition, it may not be available "
"during early boot, and must not be used for configuration."
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "systemd 254"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The default configuration is set during compilation, so configuration is "
"only needed when it is necessary to deviate from those defaults\\&. "
"Initially, the main configuration file in /etc/systemd/ contains commented "
"out entries showing the defaults as a guide to the administrator\\&. Local "
"overrides can be created by editing this file or by creating drop-ins, as "
"described below\\&. Using drop-ins for local configuration is recommended "
"over modifications to the main configuration file\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"In addition to the \"main\" configuration file, drop-in configuration "
"snippets are read from /usr/lib/systemd/*\\&.conf\\&.d/, /usr/local/lib/"
"systemd/*\\&.conf\\&.d/, and /etc/systemd/*\\&.conf\\&.d/\\&. Those drop-ins "
"have higher precedence and override the main configuration file\\&. Files in "
"the *\\&.conf\\&.d/ configuration subdirectories are sorted by their "
"filename in lexicographic order, regardless of in which of the "
"subdirectories they reside\\&. When multiple files specify the same option, "
"for options which accept just a single value, the entry in the file sorted "
"last takes precedence, and for options which accept a list of values, "
"entries are collected as they occur in the sorted files\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"When packages need to customize the configuration, they can install drop-ins "
"under /usr/\\&. Files in /etc/ are reserved for the local administrator, who "
"may use this logic to override the configuration files installed by vendor "
"packages\\&. Drop-ins have to be used to override package drop-ins, since "
"the main configuration file has lower precedence\\&. It is recommended to "
"prefix all filenames in those subdirectories with a two-digit number and a "
"dash, to simplify the ordering of the files\\&."
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "systemd 255"
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid ""
"The default configuration is set during compilation, so configuration is "
"only needed when it is necessary to deviate from those defaults\\&. The main "
"configuration file is either in /usr/lib/systemd/ or /etc/systemd/ and "
"contains commented out entries showing the defaults as a guide to the "
"administrator\\&. Local overrides can be created by creating drop-ins, as "
"described below\\&. The main configuration file can also be edited for this "
"purpose (or a copy in /etc/ if it\\*(Aqs shipped in /usr/) however using "
"drop-ins for local configuration is recommended over modifications to the "
"main configuration file\\&."
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid ""
"When packages need to customize the configuration, they can install drop-ins "
"under /usr/\\&. Files in /etc/ are reserved for the local administrator, who "
"may use this logic to override the configuration files installed by vendor "
"packages\\&. Drop-ins have to be used to override package drop-ins, since "
"the main configuration file has lower precedence\\&. It is recommended to "
"prefix all filenames in those subdirectories with a two-digit number and a "
"dash, to simplify the ordering of the files\\&. This also defined a concept "
"of drop-in priority to allow distributions to ship drop-ins within a "
"specific range lower than the range used by users\\&. This should lower the "
"risk of package drop-ins overriding accidentally drop-ins defined by "
"users\\&."
msgstr ""

#. type: TH
#: opensuse-leap-16-0
#, no-wrap
msgid "systemd 257.2"
msgstr ""
