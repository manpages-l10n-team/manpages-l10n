# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-28 16:44+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "proc_sys_fs"
msgstr ""

#. type: TH
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "2024-06-28"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "/proc/sys/fs/ - kernel variables related to filesystems"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</proc/sys/fs/>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This directory contains the files and subdirectories for kernel variables "
"related to filesystems."
msgstr ""

#. type: TP
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "I</proc/sys/fs/aio-max-nr>"
msgstr ""

#. type: TQ
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "I</proc/sys/fs/aio-nr> (both since Linux 2.6.4)"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<aio-nr> is the running total of the number of events specified by "
"B<io_setup>(2)  calls for all currently active AIO contexts.  If I<aio-nr> "
"reaches I<aio-max-nr>, then B<io_setup>(2)  will fail with the error "
"B<EAGAIN>.  Raising I<aio-max-nr> does not result in the preallocation or "
"resizing of any kernel data structures."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</proc/sys/fs/binfmt_misc>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Documentation for files in this directory can be found in the Linux kernel "
"source in the file I<Documentation/admin-guide/binfmt-misc.rst> (or in "
"I<Documentation/binfmt_misc.txt> on older kernels)."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</proc/sys/fs/dentry-state> (since Linux 2.2)"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This file contains information about the status of the directory cache "
"(dcache).  The file contains six numbers, I<nr_dentry>, I<nr_unused>, "
"I<age_limit> (age in seconds), I<want_pages> (pages requested by system) and "
"two dummy values."
msgstr ""

#. type: IP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<nr_dentry> is the number of allocated dentries (dcache entries).  This "
"field is unused in Linux 2.2."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<nr_unused> is the number of unused dentries."
msgstr ""

#.  looks like this is unused in Linux 2.2 to Linux 2.6
#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<age_limit> is the age in seconds after which dcache entries can be "
"reclaimed when memory is short."
msgstr ""

#.  looks like this is unused in Linux 2.2 to Linux 2.6
#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<want_pages> is nonzero when the kernel has called shrink_dcache_pages() "
"and the dcache isn't pruned yet."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</proc/sys/fs/dir-notify-enable>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This file can be used to disable or enable the I<dnotify> interface "
"described in B<fcntl>(2)  on a system-wide basis.  A value of 0 in this file "
"disables the interface, and a value of 1 enables it."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</proc/sys/fs/dquot-max>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This file shows the maximum number of cached disk quota entries.  On some "
"(2.4) systems, it is not present.  If the number of free cached disk quota "
"entries is very low and you have some awesome number of simultaneous system "
"users, you might want to raise the limit."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</proc/sys/fs/dquot-nr>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This file shows the number of allocated disk quota entries and the number of "
"free disk quota entries."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</proc/sys/fs/epoll/> (since Linux 2.6.28)"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This directory contains the file I<max_user_watches>, which can be used to "
"limit the amount of kernel memory consumed by the I<epoll> interface.  For "
"further details, see B<epoll>(7)."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</proc/sys/fs/file-max>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This file defines a system-wide limit on the number of open files for all "
"processes.  System calls that fail when encountering this limit fail with "
"the error B<ENFILE>.  (See also B<setrlimit>(2), which can be used by a "
"process to set the per-process limit, B<RLIMIT_NOFILE>, on the number of "
"files it may open.)  If you get lots of error messages in the kernel log "
"about running out of file handles (open file descriptions)  (look for \"VFS: "
"file-max limit E<lt>numberE<gt> reached\"), try increasing this value:"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "echo 100000 E<gt> /proc/sys/fs/file-max\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Privileged processes (B<CAP_SYS_ADMIN>)  can override the I<file-max> limit."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</proc/sys/fs/file-nr>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This (read-only) file contains three numbers: the number of allocated file "
"handles (i.e., the number of open file descriptions; see B<open>(2)); the "
"number of free file handles; and the maximum number of file handles (i.e., "
"the same value as I</proc/sys/fs/file-max>).  If the number of allocated "
"file handles is close to the maximum, you should consider increasing the "
"maximum.  Before Linux 2.6, the kernel allocated file handles dynamically, "
"but it didn't free them again.  Instead the free file handles were kept in a "
"list for reallocation; the \"free file handles\" value indicates the size of "
"that list.  A large number of free file handles indicates that there was a "
"past peak in the usage of open file handles.  Since Linux 2.6, the kernel "
"does deallocate freed file handles, and the \"free file handles\" value is "
"always zero."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</proc/sys/fs/inode-max> (only present until Linux 2.2)"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This file contains the maximum number of in-memory inodes.  This value "
"should be 3\\[en]4 times larger than the value in I<file-max>, since "
"I<stdin>, I<stdout> and network sockets also need an inode to handle them.  "
"When you regularly run out of inodes, you need to increase this value."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Starting with Linux 2.4, there is no longer a static limit on the number of "
"inodes, and this file is removed."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</proc/sys/fs/inode-nr>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "This file contains the first two values from I<inode-state>."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</proc/sys/fs/inode-state>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This file contains seven numbers: I<nr_inodes>, I<nr_free_inodes>, "
"I<preshrink>, and four dummy values (always zero)."
msgstr ""

#.  This can be slightly more than
#.  .I inode\-max
#.  because Linux allocates them one page full at a time.
#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<nr_inodes> is the number of inodes the system has allocated.  "
"I<nr_free_inodes> represents the number of free inodes."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<preshrink> is nonzero when the I<nr_inodes> E<gt> I<inode-max> and the "
"system needs to prune the inode list instead of allocating more; since Linux "
"2.4, this field is a dummy value (always zero)."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</proc/sys/fs/inotify/> (since Linux 2.6.13)"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This directory contains files I<max_queued_events>, I<max_user_instances>, "
"and I<max_user_watches>, that can be used to limit the amount of kernel "
"memory consumed by the I<inotify> interface.  For further details, see "
"B<inotify>(7)."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</proc/sys/fs/lease-break-time>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This file specifies the grace period that the kernel grants to a process "
"holding a file lease (B<fcntl>(2))  after it has sent a signal to that "
"process notifying it that another process is waiting to open the file.  If "
"the lease holder does not remove or downgrade the lease within this grace "
"period, the kernel forcibly breaks the lease."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</proc/sys/fs/leases-enable>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This file can be used to enable or disable file leases (B<fcntl>(2))  on a "
"system-wide basis.  If this file contains the value 0, leases are disabled.  "
"A nonzero value enables leases."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</proc/sys/fs/mount-max> (since Linux 4.9)"
msgstr ""

#.  commit d29216842a85c7970c536108e093963f02714498
#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The value in this file specifies the maximum number of mounts that may exist "
"in a mount namespace.  The default value in this file is 100,000."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</proc/sys/fs/mqueue/> (since Linux 2.6.6)"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This directory contains files I<msg_max>, I<msgsize_max>, and I<queues_max>, "
"controlling the resources used by POSIX message queues.  See "
"B<mq_overview>(7)  for details."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</proc/sys/fs/nr_open> (since Linux 2.6.25)"
msgstr ""

#.  commit 9cfe015aa424b3c003baba3841a60dd9b5ad319b
#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This file imposes a ceiling on the value to which the B<RLIMIT_NOFILE> "
"resource limit can be raised (see B<getrlimit>(2)).  This ceiling is "
"enforced for both unprivileged and privileged process.  The default value in "
"this file is 1048576.  (Before Linux 2.6.25, the ceiling for "
"B<RLIMIT_NOFILE> was hard-coded to the same value.)"
msgstr ""

#. type: TP
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "I</proc/sys/fs/overflowgid>"
msgstr ""

#. type: TQ
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "I</proc/sys/fs/overflowuid>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"These files allow you to change the value of the fixed UID and GID.  The "
"default is 65534.  Some filesystems support only 16-bit UIDs and GIDs, "
"although in Linux UIDs and GIDs are 32 bits.  When one of these filesystems "
"is mounted with writes enabled, any UID or GID that would exceed 65535 is "
"translated to the overflow value before being written to disk."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</proc/sys/fs/pipe-max-size> (since Linux 2.6.35)"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "See B<pipe>(7)."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</proc/sys/fs/pipe-user-pages-hard> (since Linux 4.5)"
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</proc/sys/fs/pipe-user-pages-soft> (since Linux 4.5)"
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</proc/sys/fs/protected_fifos> (since Linux 4.19)"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "The value in this file is/can be set to one of the following:"
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "0"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Writing to FIFOs is unrestricted."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "1"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Don't allow B<O_CREAT> B<open>(2)  on FIFOs that the caller doesn't own in "
"world-writable sticky directories, unless the FIFO is owned by the owner of "
"the directory."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "2"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"As for the value 1, but the restriction also applies to group-writable "
"sticky directories."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The intent of the above protections is to avoid unintentional writes to an "
"attacker-controlled FIFO when a program expected to create a regular file."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</proc/sys/fs/protected_hardlinks> (since Linux 3.6)"
msgstr ""

#.  commit 800179c9b8a1e796e441674776d11cd4c05d61d7
#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"When the value in this file is 0, no restrictions are placed on the creation "
"of hard links (i.e., this is the historical behavior before Linux 3.6).  "
"When the value in this file is 1, a hard link can be created to a target "
"file only if one of the following conditions is true:"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The calling process has the B<CAP_FOWNER> capability in its user namespace "
"and the file UID has a mapping in the namespace."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The filesystem UID of the process creating the link matches the owner (UID) "
"of the target file (as described in B<credentials>(7), a process's "
"filesystem UID is normally the same as its effective UID)."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "All of the following conditions are true:"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "the target is a regular file;"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "the target file does not have its set-user-ID mode bit enabled;"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"the target file does not have both its set-group-ID and group-executable "
"mode bits enabled; and"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"the caller has permission to read and write the target file (either via the "
"file's permissions mask or because it has suitable capabilities)."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The default value in this file is 0.  Setting the value to 1 prevents a "
"longstanding class of security issues caused by hard-link-based time-of-"
"check, time-of-use races, most commonly seen in world-writable directories "
"such as I</tmp>.  The common method of exploiting this flaw is to cross "
"privilege boundaries when following a given hard link (i.e., a root process "
"follows a hard link created by another user).  Additionally, on systems "
"without separated partitions, this stops unauthorized users from \"pinning\" "
"vulnerable set-user-ID and set-group-ID files against being upgraded by the "
"administrator, or linking to special files."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</proc/sys/fs/protected_regular> (since Linux 4.19)"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Writing to regular files is unrestricted."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Don't allow B<O_CREAT> B<open>(2)  on regular files that the caller doesn't "
"own in world-writable sticky directories, unless the regular file is owned "
"by the owner of the directory."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The intent of the above protections is similar to I<protected_fifos>, but "
"allows an application to avoid writes to an attacker-controlled regular "
"file, where the application expected to create one."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</proc/sys/fs/protected_symlinks> (since Linux 3.6)"
msgstr ""

#.  commit 800179c9b8a1e796e441674776d11cd4c05d61d7
#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"When the value in this file is 0, no restrictions are placed on following "
"symbolic links (i.e., this is the historical behavior before Linux 3.6).  "
"When the value in this file is 1, symbolic links are followed only in the "
"following circumstances:"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"the filesystem UID of the process following the link matches the owner (UID) "
"of the symbolic link (as described in B<credentials>(7), a process's "
"filesystem UID is normally the same as its effective UID);"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "the link is not in a sticky world-writable directory; or"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "the symbolic link and its parent directory have the same owner (UID)"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"A system call that fails to follow a symbolic link because of the above "
"restrictions returns the error B<EACCES> in I<errno>."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The default value in this file is 0.  Setting the value to 1 avoids a "
"longstanding class of security issues based on time-of-check, time-of-use "
"races when accessing symbolic links."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</proc/sys/fs/suid_dumpable> (since Linux 2.6.13)"
msgstr ""

#.  The following is based on text from Documentation/sysctl/kernel.txt
#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The value in this file is assigned to a process's \"dumpable\" flag in the "
"circumstances described in B<prctl>(2).  In effect, the value in this file "
"determines whether core dump files are produced for set-user-ID or otherwise "
"protected/tainted binaries.  The \"dumpable\" setting also affects the "
"ownership of files in a process's I</proc/>pid directory, as described above."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Three different integer values can be specified:"
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<0\\ (default)>"
msgstr ""

#.  In kernel source: SUID_DUMP_DISABLE
#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This provides the traditional (pre-Linux 2.6.13) behavior.  A core dump will "
"not be produced for a process which has changed credentials (by calling "
"B<seteuid>(2), B<setgid>(2), or similar, or by executing a set-user-ID or "
"set-group-ID program)  or whose binary does not have read permission enabled."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<1\\ (\"debug\")>"
msgstr ""

#.  In kernel source: SUID_DUMP_USER
#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"All processes dump core when possible.  (Reasons why a process might "
"nevertheless not dump core are described in B<core>(5).)  The core dump is "
"owned by the filesystem user ID of the dumping process and no security is "
"applied.  This is intended for system debugging situations only: this mode "
"is insecure because it allows unprivileged users to examine the memory "
"contents of privileged processes."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<2\\ (\"suidsafe\")>"
msgstr ""

#.  In kernel source: SUID_DUMP_ROOT
#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Any binary which normally would not be dumped (see \"0\" above)  is dumped "
"readable by root only.  This allows the user to remove the core dump file "
"but not to read it.  For security reasons core dumps in this mode will not "
"overwrite one another or other files.  This mode is appropriate when "
"administrators are attempting to debug problems in a normal environment."
msgstr ""

#.  9520628e8ceb69fa9a4aee6b57f22675d9e1b709
#.  54b501992dd2a839e94e76aa392c392b55080ce8
#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Additionally, since Linux 3.6, I</proc/sys/kernel/core_pattern> must either "
"be an absolute pathname or a pipe command, as detailed in B<core>(5).  "
"Warnings will be written to the kernel log if I<core_pattern> does not "
"follow these rules, and no core dump will be produced."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For details of the effect of a process's \"dumpable\" setting on ptrace "
"access mode checking, see B<ptrace>(2)."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</proc/sys/fs/super-max>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This file controls the maximum number of superblocks, and thus the maximum "
"number of mounted filesystems the kernel can have.  You need increase only "
"I<super-max> if you need to mount more filesystems than the current value in "
"I<super-max> allows you to."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</proc/sys/fs/super-nr>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "This file contains the number of filesystems currently mounted."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<proc>(5), B<proc_sys>(5)"
msgstr ""

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "2024-05-02"
msgstr ""

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr ""

#. type: TP
#: debian-unstable fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#, no-wrap
msgid "I</proc/sys/fs/aio-max-nr> and I</proc/sys/fs/aio-nr> (since Linux 2.6.4)"
msgstr ""

#. type: TP
#: debian-unstable fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#, no-wrap
msgid "I</proc/sys/fs/overflowgid> and I</proc/sys/fs/overflowuid>"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-09-30"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr ""

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr ""
