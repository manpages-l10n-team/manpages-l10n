# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-28 16:44+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "proc_pid_io"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "/proc/pid/io - I/O statistics"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</proc/>pidI</io> (since Linux 2.6.20)"
msgstr ""

#.  commit 7c3ab7381e79dfc7db14a67c6f4f3285664e1ec2
#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"This file contains I/O statistics for the process and its waited-for "
"children, for example:"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"#B< cat /proc/3828/io>\n"
"rchar: 323934931\n"
"wchar: 323929600\n"
"syscr: 632687\n"
"syscw: 632675\n"
"read_bytes: 0\n"
"write_bytes: 323932160\n"
"cancelled_write_bytes: 0\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "The fields are as follows:"
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<rchar>: characters read"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"The number of bytes returned by successful B<read>(2)  and similar system "
"calls."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<wchar>: characters written"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"The number of bytes returned by successful B<write>(2)  and similar system "
"calls."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<syscr>: read syscalls"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"The number of \"file read\" system calls\\[em]those from the B<read>(2)  "
"family, B<sendfile>(2), B<copy_file_range>(2), and B<ioctl>(2)  "
"B<BTRFS_IOC_ENCODED_READ>[B<_32>] (including when invoked by the kernel as "
"part of other syscalls)."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<syscw>: write syscalls"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"The number of \"file write\" system calls\\[em]those from the B<write>(2)  "
"family, B<sendfile>(2), B<copy_file_range>(2), and B<ioctl>(2)  "
"B<BTRFS_IOC_ENCODED_WRITE>[B<_32>] (including when invoked by the kernel as "
"part of other syscalls)."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<read_bytes>: bytes read"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"The number of bytes really fetched from the storage layer.  This is accurate "
"for block-backed filesystems."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<write_bytes>: bytes written"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "The number of bytes really sent to the storage layer."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<cancelled_write_bytes>:"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"The above statistics fail to account for truncation: if a process writes 1 "
"MB to a regular file and then removes it, said 1 MB will not be written, but "
"I<will> have nevertheless been accounted as a 1 MB write.  This field "
"represents the number of bytes \"saved\" from I/O writeback.  This can yield "
"to having done negative I/O if caches dirtied by another process are "
"truncated.  I<cancelled_write_bytes> applies to I/O already accounted-for in "
"I<write_bytes>."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Permission to access this file is governed by B<ptrace>(2)  access mode "
"B<PTRACE_MODE_READ_FSCREDS>."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "CAVEATS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"These counters are not atomic: on systems where 64-bit integer operations "
"may tear, a counter could be updated simultaneously with a read, yielding an "
"incorrect intermediate value."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<getrusage>(2), B<proc>(5)"
msgstr ""

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-08-15"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr ""

#.  commit 7c3ab7381e79dfc7db14a67c6f4f3285664e1ec2
#. type: Plain text
#: mageia-cauldron
msgid "This file contains I/O statistics for the process, for example:"
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid ""
"The number of bytes which this task has caused to be read from storage.  "
"This is simply the sum of bytes which this process passed to B<read>(2)  and "
"similar system calls.  It includes things such as terminal I/O and is "
"unaffected by whether or not actual physical disk I/O was required (the read "
"might have been satisfied from pagecache)."
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid ""
"The number of bytes which this task has caused, or shall cause to be written "
"to disk.  Similar caveats apply here as with I<rchar>."
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid ""
"Attempt to count the number of read I/O operations\\[em]that is, system "
"calls such as B<read>(2)  and B<pread>(2)."
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid ""
"Attempt to count the number of write I/O operations\\[em]that is, system "
"calls such as B<write>(2)  and B<pwrite>(2)."
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid ""
"Attempt to count the number of bytes which this process really did cause to "
"be fetched from the storage layer.  This is accurate for block-backed "
"filesystems."
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid ""
"Attempt to count the number of bytes which this process caused to be sent to "
"the storage layer."
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid ""
"The big inaccuracy here is truncate.  If a process writes 1 MB to a file and "
"then deletes the file, it will in fact perform no writeout.  But it will "
"have been accounted as having caused 1 MB of write.  In other words: this "
"field represents the number of bytes which this process caused to not "
"happen, by truncating pagecache.  A task can cause \"negative\" I/O too.  If "
"this task truncates some dirty pagecache, some I/O which another task has "
"been accounted for (in its I<write_bytes>)  will not be happening."
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid ""
"I<Note>: In the current implementation, things are a bit racy on 32-bit "
"systems: if process A reads process B's I</proc/>pidI</io> while process B "
"is updating one of these 64-bit counters, process A could see an "
"intermediate result."
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid ""
"Permission to access this file is governed by a ptrace access mode "
"B<PTRACE_MODE_READ_FSCREDS> check; see B<ptrace>(2)."
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid "B<proc>(5)"
msgstr ""

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr ""
