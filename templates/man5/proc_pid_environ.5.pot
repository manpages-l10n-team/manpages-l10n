# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-28 16:44+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "proc_pid_environ"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-06-15"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "/proc/pid/environ - initial environment"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</proc/>pidI</environ>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"This file contains the initial environment that was set when the currently "
"executing program was started via B<execve>(2).  The entries are separated "
"by null bytes (\\[aq]\\[rs]0\\[aq]), and there may be a null byte at the "
"end.  Thus, to print out the environment of process 1, you would do:"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "$B< cat /proc/1/environ | tr \\[aq]\\[rs]000\\[aq] \\[aq]\\[rs]n\\[aq]>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If, after an B<execve>(2), the process modifies its environment (e.g., by "
"calling functions such as B<putenv>(3)  or modifying the B<environ>(7)  "
"variable directly), this file will I<not> reflect those changes."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Furthermore, a process may change the memory location that this file refers "
"via B<prctl>(2)  operations such as B<PR_SET_MM_ENV_START>."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Permission to access this file is governed by a ptrace access mode "
"B<PTRACE_MODE_READ_FSCREDS> check; see B<ptrace>(2)."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<proc>(5)"
msgstr ""

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-08-15"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid ""
"This file contains the initial environment that was set when the currently "
"executing program was started via B<execve>(2).  The entries are separated "
"by null bytes (\\[aq]\\e0\\[aq]), and there may be a null byte at the end.  "
"Thus, to print out the environment of process 1, you would do:"
msgstr ""

#. type: Plain text
#: mageia-cauldron
#, no-wrap
msgid "$B< cat /proc/1/environ | tr \\[aq]\\e000\\[aq] \\[aq]\\en\\[aq]>\n"
msgstr ""

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr ""
