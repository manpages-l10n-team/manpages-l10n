# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-16 05:47+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Dd
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "May 31, 1993"
msgstr ""

#. type: Dt
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "GO-FISH 6"
msgstr ""

#. type: Sh
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid "E<.Nm go-fish>"
msgstr ""

#. type: Nd
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "play"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid "E<.Dq Go Fish>"
msgstr ""

#. type: Sh
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid "E<.Nm> E<.Op Fl p>"
msgstr ""

#. type: Sh
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid "E<.Nm> is the game E<.Dq Go Fish>, a traditional children's card game."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid ""
"The computer deals the player and itself seven cards, and places the rest of "
"the deck face-down (figuratively).  The object of the game is to collect "
"E<.Dq books>, or all of the members of a single rank.  For example, "
"collecting four 2's would give the player a E<.Dq book of 2's>."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid "The options are as follows:"
msgstr ""

#. type: It
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Fl p"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid "Professional mode."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid ""
"The computer makes a random decision as to who gets to start the game, and "
"then the computer and player take turns asking each other for cards of a "
"specified rank.  If the asked player has any cards of the requested rank, "
"they give them up to the asking player.  A player must have at least one of "
"the cards of the rank they request in their hand.  When a player asks for a "
"rank of which the other player has no cards, the asker is told to E<.Dq Go "
"Fish!>.  Then, the asker draws a card from the non-dealt cards.  If they "
"draw the card they asked for, they continue their turn, asking for more "
"ranks from the other player.  Otherwise, the other player gets a turn."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid ""
"When a player completes a book, either by getting cards from the other "
"player or drawing from the deck, they set those cards aside and the rank is "
"no longer in play."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid ""
"The game ends when either player no longer has any cards in their hand.  The "
"player with the most books wins."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid "E<.Nm> provides instructions as to what input it accepts."
msgstr ""

#. type: Sh
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "BUGS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid "The computer cheats only rarely."
msgstr ""
