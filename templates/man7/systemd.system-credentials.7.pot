# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-28 16:53+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYSTEMD\\&.SYSTEM-CREDENTIALS"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "systemd 257.3"
msgstr ""

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "systemd.system-credentials"
msgstr ""

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "systemd.system-credentials - System Credentials"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"\\m[blue]B<System and Service Credentials>\\m[]\\&\\s-2\\u[1]\\d\\s+2 are "
"data objects that may be passed into booted systems or system services as "
"they are invoked\\&. They can be acquired from various external sources, and "
"propagated into the system and from there into system services\\&. "
"Credentials may optionally be encrypted with a machine-specific key and/or "
"locked to the local TPM2 device, and are only decrypted when the consuming "
"service is invoked\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"System credentials may be used to provision and configure various aspects of "
"the system\\&. Depending on the consuming component credentials are only "
"used on initial invocations or are needed for all invocations\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Credentials may be used for any kind of data, binary or text, and may carry "
"passwords, secrets, certificates, cryptographic key material, identity "
"information, configuration, and more\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "WELL KNOWN SYSTEM CREDENTIALS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<firstboot\\&.keymap>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The console key mapping to set (e\\&.g\\&.  \"de\")\\&. Read by B<systemd-"
"firstboot>(1), and only honoured if no console keymap has been configured "
"before\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Added in version 252\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<firstboot\\&.locale>, I<firstboot\\&.locale-messages>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The system locale to set (e\\&.g\\&.  \"de_DE\\&.UTF-8\")\\&. Read by "
"B<systemd-firstboot>(1), and only honoured if no locale has been configured "
"before\\&.  I<firstboot\\&.locale> sets \"LANG\", while "
"I<firstboot\\&.locale-message> sets \"LC_MESSAGES\"\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<firstboot\\&.timezone>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The system timezone to set (e\\&.g\\&.  \"Europe/Berlin\")\\&. Read by "
"B<systemd-firstboot>(1), and only honoured if no system timezone has been "
"configured before\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<login\\&.issue>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The data of this credential is written to /etc/issue\\&.d/50-"
"provision\\&.conf, if the file doesn\\*(Aqt exist yet\\&.  B<agetty>(8)  "
"reads this file and shows its contents at the login prompt of terminal "
"logins\\&. See B<issue>(5)  for details\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Consumed by /usr/lib/tmpfiles\\&.d/provision\\&.conf, see "
"B<tmpfiles.d>(5)\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<login\\&.motd>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The data of this credential is written to /etc/motd\\&.d/50-"
"provision\\&.conf, if the file doesn\\*(Aqt exist yet\\&.  B<pam_motd>(8)  "
"reads this file and shows its contents as \"message of the day\" during "
"terminal logins\\&. See B<motd>(5)  for details\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<network\\&.hosts>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The data of this credential is written to /etc/hosts, if the file "
"doesn\\*(Aqt exist yet\\&. See B<hosts>(5)  for details\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<network\\&.dns>, I<network\\&.search_domains>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"DNS server information and search domains\\&. Read by B<systemd-"
"resolved.service>(8)\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Added in version 253\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"I<network\\&.conf\\&.*>, I<network\\&.link\\&.*>, I<network\\&.netdev\\&.*>, "
"I<network\\&.network\\&.*>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Configures network devices\\&. Read by B<systemd-network-"
"generator.service>(8)\\&. These credentials should contain valid "
"B<networkd.conf>(5), B<systemd.link>(5), B<systemd.netdev>(5), "
"B<systemd.network>(5)  configuration data\\&. From each matching credential "
"a separate file is created\\&. Example: the contents of a credential "
"network\\&.link\\&.50-foobar will be copied into a file 50-foobar\\&.link\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Note that the resulting files are created world-readable, it\\*(Aqs hence "
"recommended to not include secrets in these credentials, but supply them via "
"separate credentials directly to systemd-networkd\\&.service, e\\&.g\\&.  "
"I<network\\&.wireguard\\&.*> as described below\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Added in version 256\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "I<network\\&.wireguard\\&.*>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Configures secrets for WireGuard netdevs\\&. Read by B<systemd-"
"networkd.service>(8)\\&. For more information, refer to the B<[WireGuard]> "
"section of B<systemd.netdev>(5)\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<passwd\\&.hashed-password\\&.root>, I<passwd\\&.plaintext-password\\&.root>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"May contain the password (either in UNIX hashed format, or in plaintext) for "
"the root users\\&. Read by both B<systemd-firstboot>(1)  and B<systemd-"
"sysusers>(8), and only honoured if no root password has been configured "
"before\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<passwd\\&.shell\\&.root>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"The path to the shell program (e\\&.g\\&.  \"/bin/bash\") for the root "
"user\\&. Read by both B<systemd-firstboot>(1)  and B<systemd-sysusers>(8), "
"and only honoured if no root shell has been configured before\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<ssh\\&.authorized_keys\\&.root>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The data of this credential is written to /root/\\&.ssh/authorized_keys, if "
"the file doesn\\*(Aqt exist yet\\&. This allows provisioning SSH access for "
"the system\\*(Aqs root user\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "I<ssh\\&.listen>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"May be used to configure SSH sockets the system shall be reachable on\\&. "
"See B<systemd-ssh-generator>(8)  for details\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<sysusers\\&.extra>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Additional B<sysusers.d>(5)  lines to process during boot\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<sysctl\\&.extra>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Additional B<sysctl.d>(5)  lines to process during boot\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<tmpfiles\\&.extra>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Additional B<tmpfiles.d>(5)  lines to process during boot\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<fstab\\&.extra>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Additional mounts to establish at boot\\&. For details, see B<systemd-fstab-"
"generator>(8)\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Added in version 254\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<vconsole\\&.keymap>, I<vconsole\\&.keymap_toggle>, I<vconsole\\&.font>, "
"I<vconsole\\&.font_map>, I<vconsole\\&.font_unimap>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Console settings to apply, see B<systemd-vconsole-setup.service>(8)  for "
"details\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<getty\\&.ttys\\&.serial>, I<getty\\&.ttys\\&.container>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Used for spawning additional login prompts, see B<systemd-getty-"
"generator>(8)  for details\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "I<journal\\&.forward_to_socket>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Used by B<systemd-journald>(8)  to determine where to forward log messages "
"for socket forwarding, see B<journald.conf>(5)  for details\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "I<journal\\&.storage>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Used by B<systemd-journald>(8)  to determine where to store journal files, "
"see B<journald.conf>(5)  for details\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<vmm\\&.notify_socket>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Configures an B<sd_notify>(3)  compatible B<AF_VSOCK> socket the service "
"manager will report status information, ready notification and exit status "
"on\\&. For details see B<systemd>(1)\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "I<shell\\&.prompt\\&.prefix>, I<shell\\&.prompt\\&.suffix>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Defines strings to prefix and suffix any interactive UNIX shell prompt "
"with\\&. For details see B<pam_systemd>(8)\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Added in version 257\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "I<shell\\&.welcome>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Define a string to print when an interactive UNIX shell initializes\\&. For "
"details see B<pam_systemd>(8)\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<system\\&.machine_id>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Takes a 128bit ID to initialize the machine ID from (if it is not set yet)"
"\\&. Interpreted by the service manager (PID 1)\\&. For details see "
"B<systemd>(1)\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "I<system\\&.hostname>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Accepts a (transient) hostname to configure during early boot\\&. The static "
"hostname specified in /etc/hostname, if configured, takes precedence over "
"this setting\\&. Interpreted by the service manager (PID 1)\\&. For details "
"see B<systemd>(1)\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "I<home\\&.create\\&.*>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Creates a home area for the specified user with the user record data passed "
"in\\&. For details see B<homectl>(1)\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"I<cryptsetup\\&.passphrase>, I<cryptsetup\\&.tpm2-pin>, "
"I<cryptsetup\\&.fido2-pin>, I<cryptsetup\\&.pkcs11-pin>, "
"I<cryptsetup\\&.luks2-pin>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Specifies the passphrase/PINs to use for unlock encrypted storage "
"volumes\\&. For details see B<systemd-cryptsetup>(8)\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "I<systemd\\&.extra-unit\\&.*>, I<systemd\\&.unit-dropin\\&.*>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"These credentials specify extra units and drop-ins to add to the system\\&. "
"For details see B<systemd-debug-generator>(8)\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "I<udev\\&.conf\\&.*>, I<udev\\&.rules\\&.*>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Configures udev configuration file and udev rules\\&. Read by systemd-udev-"
"load-credentials\\&.service, which invokes B<udevadm control --load-"
"credentials>\\&. These credentials directly translate to a matching "
"B<udev.conf>(5)  or B<udev>(7)  rules file\\&. Example: the contents of a "
"credential udev\\&.conf\\&.50-foobar will be copied into a file /run/udev/"
"udev\\&.conf\\&.d/50-foobar\\&.conf, and udev\\&.rules\\&.50-foobar will be "
"copied into a file /run/udev/rules\\&.d/50-foobar\\&.rules\\&. See "
"B<udev>(7), B<udev.conf>(5), and B<udevadm>(8)  for details\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "I<import\\&.pull>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Specified disk images (tarballs and DDIs) to automatically download and "
"install at boot\\&. For details see B<systemd-import-generator>(8)\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<systemd>(1), B<kernel-command-line>(7), B<smbios-type-11>(7)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid " 1."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "System and Service Credentials"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "\\%https://systemd.io/CREDENTIALS"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "systemd 254"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"May contain the password (either in UNIX hashed format, or in plaintext) for "
"the root users\\&. Read by both B<systemd-firstboot>(1)  and B<systemd-"
"sysusers>(1), and only honoured if no root password has been configured "
"before\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"The path to the shell program (e\\&.g\\&.  \"/bin/bash\") for the root "
"user\\&. Read by both B<systemd-firstboot>(1)  and B<systemd-sysusers>(1), "
"and only honoured if no root shell has been configured before\\&."
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "systemd 255"
msgstr ""

#. type: TH
#: opensuse-leap-16-0
#, no-wrap
msgid "systemd 257.2"
msgstr ""
