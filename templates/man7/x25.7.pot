# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-28 16:57+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "x25"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "x25 - ITU-T X.25 / ISO/IEC\\ 8208 protocol interface"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>sys/socket.hE<gt>>\n"
"B<#include E<lt>linux/x25.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<x25_socket>B< = socket(AF_X25, SOCK_SEQPACKET, 0);>\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"X25 sockets provide an interface to the X.25 packet layer protocol.  This "
"allows applications to communicate over a public X.25 data network as "
"standardized by International Telecommunication Union's recommendation X.25 "
"(X.25 DTE-DCE mode).  X25 sockets can also be used for communication without "
"an intermediate X.25 network (X.25 DTE-DTE mode) as described in ISO/IEC\\ "
"8208."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Message boundaries are preserved \\[em] a B<read>(2)  from a socket will "
"retrieve the same chunk of data as output with the corresponding "
"B<write>(2)  to the peer socket.  When necessary, the kernel takes care of "
"segmenting and reassembling long messages by means of the X.25 M-bit.  There "
"is no hard-coded upper limit for the message size.  However, reassembling of "
"a long message might fail if there is a temporary lack of system resources "
"or when other constraints (such as socket memory or buffer size limits) "
"become effective.  If that occurs, the X.25 connection will be reset."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Socket addresses"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<AF_X25> socket address family uses the I<struct sockaddr_x25> for "
"representing network addresses as defined in ITU-T recommendation X.121."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"struct sockaddr_x25 {\n"
"    sa_family_t sx25_family;    /* must be AF_X25 */\n"
"    x25_address sx25_addr;      /* X.121 Address */\n"
"};\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<sx25_addr> contains a char array I<x25_addr[]> to be interpreted as a null-"
"terminated string.  I<sx25_addr.x25_addr[]> consists of up to 15 (not "
"counting the terminating null byte) ASCII characters forming the X.121 "
"address.  Only the decimal digit characters from \\[aq]0\\[aq] to \\[aq]9\\"
"[aq] are allowed."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Socket options"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The following X.25-specific socket options can be set by using "
"B<setsockopt>(2)  and read with B<getsockopt>(2)  with the I<level> argument "
"set to B<SOL_X25>."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<X25_QBITINCL>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Controls whether the X.25 Q-bit (Qualified Data Bit) is accessible by the "
"user.  It expects an integer argument.  If set to 0 (default), the Q-bit is "
"never set for outgoing packets and the Q-bit of incoming packets is "
"ignored.  If set to 1, an additional first byte is prepended to each message "
"read from or written to the socket.  For data read from the socket, a 0 "
"first byte indicates that the Q-bits of the corresponding incoming data "
"packets were not set.  A first byte with value 1 indicates that the Q-bit of "
"the corresponding incoming data packets was set.  If the first byte of the "
"data written to the socket is 1, the Q-bit of the corresponding outgoing "
"data packets will be set.  If the first byte is 0, the Q-bit will not be set."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The AF_X25 protocol family is a new feature of Linux 2.2."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Plenty, as the X.25 PLP implementation is B<CONFIG_EXPERIMENTAL>."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "This man page is incomplete."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"There is no dedicated application programmer's header file yet; you need to "
"include the kernel header file I<E<lt>linux/x25.hE<gt>>.  "
"B<CONFIG_EXPERIMENTAL> might also imply that future versions of the "
"interface are not binary compatible."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"X.25 N-Reset events are not propagated to the user process yet.  Thus, if a "
"reset occurred, data might be lost without notice."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<socket>(2), B<socket>(7)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Jonathan Simon Naylor: \\[lq]The Re-Analysis and Re-Implementation of X.25.\\"
"[rq] The URL is E<.UR ftp://ftp.pspt.fi\\:/pub\\:/ham\\:/linux\\:/ax25\\:/"
"x25doc.tgz> E<.UE .>"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "x25 - ITU-T X.25 / ISO-8208 protocol interface"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"X25 sockets provide an interface to the X.25 packet layer protocol.  This "
"allows applications to communicate over a public X.25 data network as "
"standardized by International Telecommunication Union's recommendation X.25 "
"(X.25 DTE-DCE mode).  X25 sockets can also be used for communication without "
"an intermediate X.25 network (X.25 DTE-DTE mode) as described in ISO-8208."
msgstr ""

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2024-01-28"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr ""

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr ""
