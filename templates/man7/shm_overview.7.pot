# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-28 16:49+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "shm_overview"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "shm_overview - overview of POSIX shared memory"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The POSIX shared memory API allows processes to communicate information by "
"sharing a region of memory."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The interfaces employed in the API are:"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<shm_open>(3)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Create and open a new object, or open an existing object.  This is analogous "
"to B<open>(2).  The call returns a file descriptor for use by the other "
"interfaces listed below."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ftruncate>(2)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Set the size of the shared memory object.  (A newly created shared memory "
"object has a length of zero.)"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<mmap>(2)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Map the shared memory object into the virtual address space of the calling "
"process."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<munmap>(2)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Unmap the shared memory object from the virtual address space of the calling "
"process."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<shm_unlink>(3)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Remove a shared memory object name."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<close>(2)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Close the file descriptor allocated by B<shm_open>(3)  when it is no longer "
"needed."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<fstat>(2)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Obtain a I<stat> structure that describes the shared memory object.  Among "
"the information returned by this call are the object's size (I<st_size>), "
"permissions (I<st_mode>), owner (I<st_uid>), and group (I<st_gid>)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<fchown>(2)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "To change the ownership of a shared memory object."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<fchmod>(2)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "To change the permissions of a shared memory object."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Versions"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX shared memory is supported since Linux 2.4 and glibc 2.2."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Persistence"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"POSIX shared memory objects have kernel persistence: a shared memory object "
"will exist until the system is shut down, or until all processes have "
"unmapped the object and it has been deleted with B<shm_unlink>(3)"
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linking"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Programs using the POSIX shared memory API must be compiled with I<cc -lrt> "
"to link against the real-time library, I<librt>."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Accessing shared memory objects via the filesystem"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On Linux, shared memory objects are created in a (B<tmpfs>(5))  virtual "
"filesystem, normally mounted under I</dev/shm>.  Since Linux 2.6.19, Linux "
"supports the use of access control lists (ACLs)  to control the permissions "
"of objects in the virtual filesystem."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Typically, processes must synchronize their access to a shared memory "
"object, using, for example, POSIX semaphores."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"System V shared memory (B<shmget>(2), B<shmop>(2), etc.) is an older shared "
"memory API.  POSIX shared memory provides a simpler, and better designed "
"interface; on the other hand POSIX shared memory is somewhat less widely "
"available (especially on older systems) than System V shared memory."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<fchmod>(2), B<fchown>(2), B<fstat>(2), B<ftruncate>(2), "
"B<memfd_create>(2), B<mmap>(2), B<mprotect>(2), B<munmap>(2), B<shmget>(2), "
"B<shmop>(2), B<shm_open>(3), B<shm_unlink>(3), B<sem_overview>(7)"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-12-04"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr ""

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr ""
