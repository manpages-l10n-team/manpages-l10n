# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-28 16:45+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "pthread_create"
msgstr ""

#. type: TH
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "2024-12-13"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "pthread_create - create a new thread"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "POSIX threads library (I<libpthread>,\\ I<-lpthread>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>pthread.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int pthread_create(pthread_t *restrict >I<thread>B<,>\n"
"B<                   const pthread_attr_t *restrict >I<attr>B<,>\n"
"B<                   typeof(void *(void *)) *>I<start_routine>B<,>\n"
"B<                   void *restrict >I<arg>B<);>\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<pthread_create>()  function starts a new thread in the calling "
"process.  The new thread starts execution by invoking I<start_routine>(); "
"I<arg> is passed as the sole argument of I<start_routine>()."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The new thread terminates in one of the following ways:"
msgstr ""

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"It calls B<pthread_exit>(3), specifying an exit status value that is "
"available to another thread in the same process that calls "
"B<pthread_join>(3)."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"It returns from I<start_routine>().  This is equivalent to calling "
"B<pthread_exit>(3)  with the value supplied in the I<return> statement."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "It is canceled (see B<pthread_cancel>(3))."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Any of the threads in the process calls B<exit>(3), or the main thread "
"performs a return from I<main>().  This causes the termination of all "
"threads in the process."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The I<attr> argument points to a I<pthread_attr_t> structure whose contents "
"are used at thread creation time to determine attributes for the new thread; "
"this structure is initialized using B<pthread_attr_init>(3)  and related "
"functions.  If I<attr> is NULL, then the thread is created with default "
"attributes."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Before returning, a successful call to B<pthread_create>()  stores the ID of "
"the new thread in the buffer pointed to by I<thread>; this identifier is "
"used to refer to the thread in subsequent calls to other pthreads functions."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The new thread inherits a copy of the creating thread's signal mask "
"(B<pthread_sigmask>(3)).  The set of pending signals for the new thread is "
"empty (B<sigpending>(2)).  The new thread does not inherit the creating "
"thread's alternate signal stack (B<sigaltstack>(2))."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The new thread inherits the calling thread's floating-point environment "
"(B<fenv>(3))."
msgstr ""

#.  CLOCK_THREAD_CPUTIME_ID in clock_gettime(2)
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The initial value of the new thread's CPU-time clock is 0 (see "
"B<pthread_getcpuclockid>(3))."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux-specific details"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The new thread inherits copies of the calling thread's capability sets (see "
"B<capabilities>(7))  and CPU affinity mask (see B<sched_setaffinity>(2))."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On success, B<pthread_create>()  returns 0; on error, it returns an error "
"number, and the contents of I<*thread> are undefined."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EAGAIN>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Insufficient resources to create another thread."
msgstr ""

#.  NOTE! The following should match the description in fork(2)
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"A system-imposed limit on the number of threads was encountered.  There are "
"a number of limits that may trigger this error: the B<RLIMIT_NPROC> soft "
"resource limit (set via B<setrlimit>(2)), which limits the number of "
"processes and threads for a real user ID, was reached; the kernel's system-"
"wide limit on the number of processes and threads, I</proc/sys/kernel/"
"threads-max>, was reached (see B<proc>(5)); or the maximum number of PIDs, "
"I</proc/sys/kernel/pid_max>, was reached (see B<proc>(5))."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Invalid settings in I<attr>."
msgstr ""

#.  FIXME . Test the following
#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"No permission to set the scheduling policy and parameters specified in "
"I<attr>."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr ""

#. type: tbl table
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ""

#. type: tbl table
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<pthread_create>()"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2001."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"See B<pthread_self>(3)  for further information on the thread ID returned in "
"I<*thread> by B<pthread_create>().  Unless real-time scheduling policies are "
"being employed, after a call to B<pthread_create>(), it is indeterminate "
"which thread\\[em]the caller or the new thread\\[em]will next execute."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"A thread may either be I<joinable> or I<detached>.  If a thread is joinable, "
"then another thread can call B<pthread_join>(3)  to wait for the thread to "
"terminate and fetch its exit status.  Only when a terminated joinable thread "
"has been joined are the last of its resources released back to the system.  "
"When a detached thread terminates, its resources are automatically released "
"back to the system: it is not possible to join with the thread in order to "
"obtain its exit status.  Making a thread detached is useful for some types "
"of daemon threads whose exit status the application does not need to care "
"about.  By default, a new thread is created in a joinable state, unless "
"I<attr> was set to create the thread in a detached state (using "
"B<pthread_attr_setdetachstate>(3))."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Under the NPTL threading implementation, if the B<RLIMIT_STACK> soft "
"resource limit I<at the time the program started> has any value other than "
"\"unlimited\", then it determines the default stack size of new threads.  "
"Using B<pthread_attr_setstacksize>(3), the stack size attribute can be "
"explicitly set in the I<attr> argument used to create a thread, in order to "
"obtain a stack size other than the default.  If the B<RLIMIT_STACK> resource "
"limit is set to \"unlimited\", a per-architecture value is used for the "
"stack size: 2 MB on most architectures; 4 MB on POWER and Sparc-64."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In the obsolete LinuxThreads implementation, each of the threads in a "
"process has a different process ID.  This is in violation of the POSIX "
"threads specification, and is the source of many other nonconformances to "
"the standard; see B<pthreads>(7)."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The program below demonstrates the use of B<pthread_create>(), as well as a "
"number of other functions in the pthreads API."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In the following run, on a system providing the NPTL threading "
"implementation, the stack size defaults to the value given by the \"stack "
"size\" resource limit:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"$B< ulimit -s>\n"
"8192            # The stack size limit is 8 MB (0x800000 bytes)\n"
"$B< ./a.out hola salut servus>\n"
"Thread 1: top of stack near 0xb7dd03b8; argv_string=hola\n"
"Thread 2: top of stack near 0xb75cf3b8; argv_string=salut\n"
"Thread 3: top of stack near 0xb6dce3b8; argv_string=servus\n"
"Joined with thread 1; returned value was HOLA\n"
"Joined with thread 2; returned value was SALUT\n"
"Joined with thread 3; returned value was SERVUS\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In the next run, the program explicitly sets a stack size of 1\\ MB (using "
"B<pthread_attr_setstacksize>(3))  for the created threads:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"$B< ./a.out -s 0x100000 hola salut servus>\n"
"Thread 1: top of stack near 0xb7d723b8; argv_string=hola\n"
"Thread 2: top of stack near 0xb7c713b8; argv_string=salut\n"
"Thread 3: top of stack near 0xb7b703b8; argv_string=servus\n"
"Joined with thread 1; returned value was HOLA\n"
"Joined with thread 2; returned value was SALUT\n"
"Joined with thread 3; returned value was SERVUS\n"
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Program source"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"#include E<lt>ctype.hE<gt>\n"
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>pthread.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
"#include E<lt>sys/types.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"\\&\n"
"#define handle_error_en(en, msg) \\[rs]\n"
"        do { errno = en; perror(msg); exit(EXIT_FAILURE); } while (0)\n"
"\\&\n"
"#define handle_error(msg) \\[rs]\n"
"        do { perror(msg); exit(EXIT_FAILURE); } while (0)\n"
"\\&\n"
"struct thread_info {    /* Used as argument to thread_start() */\n"
"    pthread_t thread_id;        /* ID returned by pthread_create() */\n"
"    int       thread_num;       /* Application-defined thread # */\n"
"    char     *argv_string;      /* From command-line argument */\n"
"};\n"
"\\&\n"
"/* Thread start function: display address near top of our stack,\n"
"   and return upper-cased copy of argv_string. */\n"
"\\&\n"
"static void *\n"
"thread_start(void *arg)\n"
"{\n"
"    struct thread_info *tinfo = arg;\n"
"    char *uargv;\n"
"\\&\n"
"    printf(\"Thread %d: top of stack near %p; argv_string=%s\\[rs]n\",\n"
"           tinfo-E<gt>thread_num, (void *) &tinfo, tinfo-E<gt>argv_string);\n"
"\\&\n"
"    uargv = strdup(tinfo-E<gt>argv_string);\n"
"    if (uargv == NULL)\n"
"        handle_error(\"strdup\");\n"
"\\&\n"
"    for (char *p = uargv; *p != \\[aq]\\[rs]0\\[aq]; p++)\n"
"        *p = toupper(*p);\n"
"\\&\n"
"    return uargv;\n"
"}\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int                 s, opt;\n"
"    void                *res;\n"
"    size_t              num_threads;\n"
"    ssize_t             stack_size;\n"
"    pthread_attr_t      attr;\n"
"    struct thread_info  *tinfo;\n"
"\\&\n"
"    /* The \"-s\" option specifies a stack size for our threads. */\n"
"\\&\n"
"    stack_size = -1;\n"
"    while ((opt = getopt(argc, argv, \"s:\")) != -1) {\n"
"        switch (opt) {\n"
"        case \\[aq]s\\[aq]:\n"
"            stack_size = strtoul(optarg, NULL, 0);\n"
"            break;\n"
"\\&\n"
"        default:\n"
"            fprintf(stderr, \"Usage: %s [-s stack-size] arg...\\[rs]n\",\n"
"                    argv[0]);\n"
"            exit(EXIT_FAILURE);\n"
"        }\n"
"    }\n"
"\\&\n"
"    num_threads = argc - optind;\n"
"\\&\n"
"    /* Initialize thread creation attributes. */\n"
"\\&\n"
"    s = pthread_attr_init(&attr);\n"
"    if (s != 0)\n"
"        handle_error_en(s, \"pthread_attr_init\");\n"
"\\&\n"
"    if (stack_size E<gt> 0) {\n"
"        s = pthread_attr_setstacksize(&attr, stack_size);\n"
"        if (s != 0)\n"
"            handle_error_en(s, \"pthread_attr_setstacksize\");\n"
"    }\n"
"\\&\n"
"    /* Allocate memory for pthread_create() arguments. */\n"
"\\&\n"
"    tinfo = calloc(num_threads, sizeof(*tinfo));\n"
"    if (tinfo == NULL)\n"
"        handle_error(\"calloc\");\n"
"\\&\n"
"    /* Create one thread for each command-line argument. */\n"
"\\&\n"
"    for (size_t tnum = 0; tnum E<lt> num_threads; tnum++) {\n"
"        tinfo[tnum].thread_num = tnum + 1;\n"
"        tinfo[tnum].argv_string = argv[optind + tnum];\n"
"\\&\n"
"        /* The pthread_create() call stores the thread ID into\n"
"           corresponding element of tinfo[]. */\n"
"\\&\n"
"        s = pthread_create(&tinfo[tnum].thread_id, &attr,\n"
"                           &thread_start, &tinfo[tnum]);\n"
"        if (s != 0)\n"
"            handle_error_en(s, \"pthread_create\");\n"
"    }\n"
"\\&\n"
"    /* Destroy the thread attributes object, since it is no\n"
"       longer needed. */\n"
"\\&\n"
"    s = pthread_attr_destroy(&attr);\n"
"    if (s != 0)\n"
"        handle_error_en(s, \"pthread_attr_destroy\");\n"
"\\&\n"
"    /* Now join with each thread, and display its returned value. */\n"
"\\&\n"
"    for (size_t tnum = 0; tnum E<lt> num_threads; tnum++) {\n"
"        s = pthread_join(tinfo[tnum].thread_id, &res);\n"
"        if (s != 0)\n"
"            handle_error_en(s, \"pthread_join\");\n"
"\\&\n"
"        printf(\"Joined with thread %d; returned value was %s\\[rs]n\",\n"
"               tinfo[tnum].thread_num, (char *) res);\n"
"        free(res);      /* Free memory allocated by thread */\n"
"    }\n"
"\\&\n"
"    free(tinfo);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#.  SRC END
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<getrlimit>(2), B<pthread_attr_init>(3), B<pthread_cancel>(3), "
"B<pthread_detach>(3), B<pthread_equal>(3), B<pthread_exit>(3), "
"B<pthread_getattr_np>(3), B<pthread_join>(3), B<pthread_self>(3), "
"B<pthread_setattr_default_np>(3), B<pthreads>(7)"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "POSIX threads library (I<libpthread>, I<-lpthread>)"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
#, no-wrap
msgid ""
"B<int pthread_create(pthread_t *restrict >I<thread>B<,>\n"
"B<                   const pthread_attr_t *restrict >I<attr>B<,>\n"
"B<                   void *(*>I<start_routine>B<)(void *),>\n"
"B<                   void *restrict >I<arg>B<);>\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "POSIX.1-2001, POSIX.1-2008."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Under the NPTL threading implementation, if the B<RLIMIT_STACK> soft "
"resource limit I<at the time the program started> has any value other than "
"\"unlimited\", then it determines the default stack size of new threads.  "
"Using B<pthread_attr_setstacksize>(3), the stack size attribute can be "
"explicitly set in the I<attr> argument used to create a thread, in order to "
"obtain a stack size other than the default.  If the B<RLIMIT_STACK> resource "
"limit is set to \"unlimited\", a per-architecture value is used for the "
"stack size.  Here is the value for a few architectures:"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "Architecture"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "Default stack size"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "i386"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "2 MB"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "IA-64"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "32 MB"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "PowerPC"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "4 MB"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "S/390"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "Sparc-32"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "Sparc-64"
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "x86_64"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"#include E<lt>ctype.hE<gt>\n"
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>pthread.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"#define handle_error_en(en, msg) \\e\n"
"        do { errno = en; perror(msg); exit(EXIT_FAILURE); } while (0)\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"#define handle_error(msg) \\e\n"
"        do { perror(msg); exit(EXIT_FAILURE); } while (0)\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"struct thread_info {    /* Used as argument to thread_start() */\n"
"    pthread_t thread_id;        /* ID returned by pthread_create() */\n"
"    int       thread_num;       /* Application-defined thread # */\n"
"    char     *argv_string;      /* From command-line argument */\n"
"};\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"/* Thread start function: display address near top of our stack,\n"
"   and return upper-cased copy of argv_string. */\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"static void *\n"
"thread_start(void *arg)\n"
"{\n"
"    struct thread_info *tinfo = arg;\n"
"    char *uargv;\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    printf(\"Thread %d: top of stack near %p; argv_string=%s\\en\",\n"
"           tinfo-E<gt>thread_num, (void *) &tinfo, tinfo-E<gt>argv_string);\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    uargv = strdup(tinfo-E<gt>argv_string);\n"
"    if (uargv == NULL)\n"
"        handle_error(\"strdup\");\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    for (char *p = uargv; *p != \\[aq]\\e0\\[aq]; p++)\n"
"        *p = toupper(*p);\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    return uargv;\n"
"}\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int                 s, opt;\n"
"    void                *res;\n"
"    size_t              num_threads;\n"
"    ssize_t             stack_size;\n"
"    pthread_attr_t      attr;\n"
"    struct thread_info  *tinfo;\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "    /* The \"-s\" option specifies a stack size for our threads. */\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    stack_size = -1;\n"
"    while ((opt = getopt(argc, argv, \"s:\")) != -1) {\n"
"        switch (opt) {\n"
"        case \\[aq]s\\[aq]:\n"
"            stack_size = strtoul(optarg, NULL, 0);\n"
"            break;\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"        default:\n"
"            fprintf(stderr, \"Usage: %s [-s stack-size] arg...\\en\",\n"
"                    argv[0]);\n"
"            exit(EXIT_FAILURE);\n"
"        }\n"
"    }\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "    num_threads = argc - optind;\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "    /* Initialize thread creation attributes. */\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    s = pthread_attr_init(&attr);\n"
"    if (s != 0)\n"
"        handle_error_en(s, \"pthread_attr_init\");\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    if (stack_size E<gt> 0) {\n"
"        s = pthread_attr_setstacksize(&attr, stack_size);\n"
"        if (s != 0)\n"
"            handle_error_en(s, \"pthread_attr_setstacksize\");\n"
"    }\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "    /* Allocate memory for pthread_create() arguments. */\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    tinfo = calloc(num_threads, sizeof(*tinfo));\n"
"    if (tinfo == NULL)\n"
"        handle_error(\"calloc\");\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "    /* Create one thread for each command-line argument. */\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    for (size_t tnum = 0; tnum E<lt> num_threads; tnum++) {\n"
"        tinfo[tnum].thread_num = tnum + 1;\n"
"        tinfo[tnum].argv_string = argv[optind + tnum];\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"        /* The pthread_create() call stores the thread ID into\n"
"           corresponding element of tinfo[]. */\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"        s = pthread_create(&tinfo[tnum].thread_id, &attr,\n"
"                           &thread_start, &tinfo[tnum]);\n"
"        if (s != 0)\n"
"            handle_error_en(s, \"pthread_create\");\n"
"    }\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    /* Destroy the thread attributes object, since it is no\n"
"       longer needed. */\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    s = pthread_attr_destroy(&attr);\n"
"    if (s != 0)\n"
"        handle_error_en(s, \"pthread_attr_destroy\");\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "    /* Now join with each thread, and display its returned value. */\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    for (size_t tnum = 0; tnum E<lt> num_threads; tnum++) {\n"
"        s = pthread_join(tinfo[tnum].thread_id, &res);\n"
"        if (s != 0)\n"
"            handle_error_en(s, \"pthread_join\");\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"        printf(\"Joined with thread %d; returned value was %s\\en\",\n"
"               tinfo[tnum].thread_num, (char *) res);\n"
"        free(res);      /* Free memory allocated by thread */\n"
"    }\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    free(tinfo);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "2024-06-15"
msgstr ""

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2024-02-12"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr ""

#. type: Plain text
#: mageia-cauldron
#, no-wrap
msgid ""
"#include E<lt>ctype.hE<gt>\n"
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>pthread.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"\\&\n"
"#define handle_error_en(en, msg) \\e\n"
"        do { errno = en; perror(msg); exit(EXIT_FAILURE); } while (0)\n"
"\\&\n"
"#define handle_error(msg) \\e\n"
"        do { perror(msg); exit(EXIT_FAILURE); } while (0)\n"
"\\&\n"
"struct thread_info {    /* Used as argument to thread_start() */\n"
"    pthread_t thread_id;        /* ID returned by pthread_create() */\n"
"    int       thread_num;       /* Application-defined thread # */\n"
"    char     *argv_string;      /* From command-line argument */\n"
"};\n"
"\\&\n"
"/* Thread start function: display address near top of our stack,\n"
"   and return upper-cased copy of argv_string. */\n"
"\\&\n"
"static void *\n"
"thread_start(void *arg)\n"
"{\n"
"    struct thread_info *tinfo = arg;\n"
"    char *uargv;\n"
"\\&\n"
"    printf(\"Thread %d: top of stack near %p; argv_string=%s\\en\",\n"
"           tinfo-E<gt>thread_num, (void *) &tinfo, tinfo-E<gt>argv_string);\n"
"\\&\n"
"    uargv = strdup(tinfo-E<gt>argv_string);\n"
"    if (uargv == NULL)\n"
"        handle_error(\"strdup\");\n"
"\\&\n"
"    for (char *p = uargv; *p != \\[aq]\\e0\\[aq]; p++)\n"
"        *p = toupper(*p);\n"
"\\&\n"
"    return uargv;\n"
"}\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int                 s, opt;\n"
"    void                *res;\n"
"    size_t              num_threads;\n"
"    ssize_t             stack_size;\n"
"    pthread_attr_t      attr;\n"
"    struct thread_info  *tinfo;\n"
"\\&\n"
"    /* The \"-s\" option specifies a stack size for our threads. */\n"
"\\&\n"
"    stack_size = -1;\n"
"    while ((opt = getopt(argc, argv, \"s:\")) != -1) {\n"
"        switch (opt) {\n"
"        case \\[aq]s\\[aq]:\n"
"            stack_size = strtoul(optarg, NULL, 0);\n"
"            break;\n"
"\\&\n"
"        default:\n"
"            fprintf(stderr, \"Usage: %s [-s stack-size] arg...\\en\",\n"
"                    argv[0]);\n"
"            exit(EXIT_FAILURE);\n"
"        }\n"
"    }\n"
"\\&\n"
"    num_threads = argc - optind;\n"
"\\&\n"
"    /* Initialize thread creation attributes. */\n"
"\\&\n"
"    s = pthread_attr_init(&attr);\n"
"    if (s != 0)\n"
"        handle_error_en(s, \"pthread_attr_init\");\n"
"\\&\n"
"    if (stack_size E<gt> 0) {\n"
"        s = pthread_attr_setstacksize(&attr, stack_size);\n"
"        if (s != 0)\n"
"            handle_error_en(s, \"pthread_attr_setstacksize\");\n"
"    }\n"
"\\&\n"
"    /* Allocate memory for pthread_create() arguments. */\n"
"\\&\n"
"    tinfo = calloc(num_threads, sizeof(*tinfo));\n"
"    if (tinfo == NULL)\n"
"        handle_error(\"calloc\");\n"
"\\&\n"
"    /* Create one thread for each command-line argument. */\n"
"\\&\n"
"    for (size_t tnum = 0; tnum E<lt> num_threads; tnum++) {\n"
"        tinfo[tnum].thread_num = tnum + 1;\n"
"        tinfo[tnum].argv_string = argv[optind + tnum];\n"
"\\&\n"
"        /* The pthread_create() call stores the thread ID into\n"
"           corresponding element of tinfo[]. */\n"
"\\&\n"
"        s = pthread_create(&tinfo[tnum].thread_id, &attr,\n"
"                           &thread_start, &tinfo[tnum]);\n"
"        if (s != 0)\n"
"            handle_error_en(s, \"pthread_create\");\n"
"    }\n"
"\\&\n"
"    /* Destroy the thread attributes object, since it is no\n"
"       longer needed. */\n"
"\\&\n"
"    s = pthread_attr_destroy(&attr);\n"
"    if (s != 0)\n"
"        handle_error_en(s, \"pthread_attr_destroy\");\n"
"\\&\n"
"    /* Now join with each thread, and display its returned value. */\n"
"\\&\n"
"    for (size_t tnum = 0; tnum E<lt> num_threads; tnum++) {\n"
"        s = pthread_join(tinfo[tnum].thread_id, &res);\n"
"        if (s != 0)\n"
"            handle_error_en(s, \"pthread_join\");\n"
"\\&\n"
"        printf(\"Joined with thread %d; returned value was %s\\en\",\n"
"               tinfo[tnum].thread_num, (char *) res);\n"
"        free(res);      /* Free memory allocated by thread */\n"
"    }\n"
"\\&\n"
"    free(tinfo);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr ""
