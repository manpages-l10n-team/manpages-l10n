# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-28 16:45+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "pthread_cleanup_push"
msgstr ""

#. type: TH
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "2024-12-13"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"pthread_cleanup_push, pthread_cleanup_pop - push and pop thread cancelation "
"clean-up handlers"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "POSIX threads library (I<libpthread>,\\ I<-lpthread>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>pthread.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid ""
"B<void pthread_cleanup_push(typeof(void (void *)) *>I<routine>B<, void *>I<arg>B<);>\n"
"B<void pthread_cleanup_pop(int >I<execute>B<);>\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"These functions manipulate the calling thread's stack of thread-cancelation "
"clean-up handlers.  A clean-up handler is a function that is automatically "
"executed when a thread is canceled (or in various other circumstances "
"described below); it might, for example, unlock a mutex so that it becomes "
"available to other threads in the process."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<pthread_cleanup_push>()  function pushes I<routine> onto the top of "
"the stack of clean-up handlers.  When I<routine> is later invoked, it will "
"be given I<arg> as its argument."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<pthread_cleanup_pop>()  function removes the routine at the top of the "
"stack of clean-up handlers, and optionally executes it if I<execute> is "
"nonzero."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"A cancelation clean-up handler is popped from the stack and executed in the "
"following circumstances:"
msgstr ""

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"When a thread is canceled, all of the stacked clean-up handlers are popped "
"and executed in the reverse of the order in which they were pushed onto the "
"stack."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"When a thread terminates by calling B<pthread_exit>(3), all clean-up "
"handlers are executed as described in the preceding point.  (Clean-up "
"handlers are I<not> called if the thread terminates by performing a "
"I<return> from the thread start function.)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"When a thread calls B<pthread_cleanup_pop>()  with a nonzero I<execute> "
"argument, the top-most clean-up handler is popped and executed."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"POSIX.1 permits B<pthread_cleanup_push>()  and B<pthread_cleanup_pop>()  to "
"be implemented as macros that expand to text containing \\[aq]B<{>\\[aq] and "
"\\[aq]B<}>\\[aq], respectively.  For this reason, the caller must ensure "
"that calls to these functions are paired within the same function, and at "
"the same lexical nesting level.  (In other words, a clean-up handler is "
"established only during the execution of a specified section of code.)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Calling B<longjmp>(3)  (B<siglongjmp>(3))  produces undefined results if any "
"call has been made to B<pthread_cleanup_push>()  or "
"B<pthread_cleanup_pop>()  without the matching call of the pair since the "
"jump buffer was filled by B<setjmp>(3)  (B<sigsetjmp>(3)).  Likewise, "
"calling B<longjmp>(3)  (B<siglongjmp>(3))  from inside a clean-up handler "
"produces undefined results unless the jump buffer was also filled by "
"B<setjmp>(3)  (B<sigsetjmp>(3))  inside the handler."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "These functions do not return a value."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr ""

#. #-#-#-#-#  archlinux: pthread_cleanup_push.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: pthread_cleanup_push.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SH VERSIONS
#.  Available since glibc 2.0
#. type: Plain text
#. #-#-#-#-#  debian-unstable: pthread_cleanup_push.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  fedora-42: pthread_cleanup_push.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: pthread_cleanup_push.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: pthread_cleanup_push.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-16-0: pthread_cleanup_push.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: pthread_cleanup_push.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "There are no errors."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr ""

#. type: tbl table
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ""

#. type: tbl table
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<pthread_cleanup_push>(),\n"
"B<pthread_cleanup_pop>()"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On glibc, the B<pthread_cleanup_push>()  and B<pthread_cleanup_pop>()  "
"functions I<are> implemented as macros that expand to text containing \\"
"[aq]B<{>\\[aq] and \\[aq]B<}>\\[aq], respectively.  This means that "
"variables declared within the scope of paired calls to these functions will "
"be visible within only that scope."
msgstr ""

#.  The text was actually added in the 2004 TC2
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"POSIX.1 says that the effect of using I<return>, I<break>, I<continue>, or "
"I<goto> to prematurely leave a block bracketed B<pthread_cleanup_push>()  "
"and B<pthread_cleanup_pop>()  is undefined.  Portable applications should "
"avoid doing this."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2001.  glibc 2.0."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The program below provides a simple example of the use of the functions "
"described in this page.  The program creates a thread that executes a loop "
"bracketed by B<pthread_cleanup_push>()  and B<pthread_cleanup_pop>().  This "
"loop increments a global variable, I<cnt>, once each second.  Depending on "
"what command-line arguments are supplied, the main thread sends the other "
"thread a cancelation request, or sets a global variable that causes the "
"other thread to exit its loop and terminate normally (by doing a I<return>)."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In the following shell session, the main thread sends a cancelation request "
"to the other thread:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"$ B<./a.out>\n"
"New thread started\n"
"cnt = 0\n"
"cnt = 1\n"
"Canceling thread\n"
"Called clean-up handler\n"
"Thread was canceled; cnt = 0\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"From the above, we see that the thread was canceled, and that the "
"cancelation clean-up handler was called and it reset the value of the global "
"variable I<cnt> to 0."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In the next run, the main program sets a global variable that causes other "
"thread to terminate normally:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"$ B<./a.out x>\n"
"New thread started\n"
"cnt = 0\n"
"cnt = 1\n"
"Thread terminated normally; cnt = 2\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"From the above, we see that the clean-up handler was not executed (because "
"I<cleanup_pop_arg> was 0), and therefore the value of I<cnt> was not reset."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In the next run, the main program sets a global variable that causes the "
"other thread to terminate normally, and supplies a nonzero value for "
"I<cleanup_pop_arg>:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"$ B<./a.out x 1>\n"
"New thread started\n"
"cnt = 0\n"
"cnt = 1\n"
"Called clean-up handler\n"
"Thread terminated normally; cnt = 0\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In the above, we see that although the thread was not canceled, the clean-up "
"handler was executed, because the argument given to "
"B<pthread_cleanup_pop>()  was nonzero."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Program source"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>pthread.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>sys/types.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"\\&\n"
"#define handle_error_en(en, msg) \\[rs]\n"
"        do { errno = en; perror(msg); exit(EXIT_FAILURE); } while (0)\n"
"\\&\n"
"static int done = 0;\n"
"static int cleanup_pop_arg = 0;\n"
"static int cnt = 0;\n"
"\\&\n"
"static void\n"
"cleanup_handler(void *arg)\n"
"{\n"
"    printf(\"Called clean-up handler\\[rs]n\");\n"
"    cnt = 0;\n"
"}\n"
"\\&\n"
"static void *\n"
"thread_start(void *arg)\n"
"{\n"
"    time_t curr;\n"
"\\&\n"
"    printf(\"New thread started\\[rs]n\");\n"
"\\&\n"
"    pthread_cleanup_push(cleanup_handler, NULL);\n"
"\\&\n"
"    curr = time(NULL);\n"
"\\&\n"
"    while (!done) {\n"
"        pthread_testcancel();           /* A cancelation point */\n"
"        if (curr E<lt> time(NULL)) {\n"
"            curr = time(NULL);\n"
"            printf(\"cnt = %d\\[rs]n\", cnt);  /* A cancelation point */\n"
"            cnt++;\n"
"        }\n"
"    }\n"
"\\&\n"
"    pthread_cleanup_pop(cleanup_pop_arg);\n"
"    return NULL;\n"
"}\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    pthread_t thr;\n"
"    int s;\n"
"    void *res;\n"
"\\&\n"
"    s = pthread_create(&thr, NULL, thread_start, NULL);\n"
"    if (s != 0)\n"
"        handle_error_en(s, \"pthread_create\");\n"
"\\&\n"
"    sleep(2);           /* Allow new thread to run a while */\n"
"\\&\n"
"    if (argc E<gt> 1) {\n"
"        if (argc E<gt> 2)\n"
"            cleanup_pop_arg = atoi(argv[2]);\n"
"        done = 1;\n"
"\\&\n"
"    } else {\n"
"        printf(\"Canceling thread\\[rs]n\");\n"
"        s = pthread_cancel(thr);\n"
"        if (s != 0)\n"
"            handle_error_en(s, \"pthread_cancel\");\n"
"    }\n"
"\\&\n"
"    s = pthread_join(thr, &res);\n"
"    if (s != 0)\n"
"        handle_error_en(s, \"pthread_join\");\n"
"\\&\n"
"    if (res == PTHREAD_CANCELED)\n"
"        printf(\"Thread was canceled; cnt = %d\\[rs]n\", cnt);\n"
"    else\n"
"        printf(\"Thread terminated normally; cnt = %d\\[rs]n\", cnt);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#.  SRC END
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<pthread_cancel>(3), B<pthread_cleanup_push_defer_np>(3), "
"B<pthread_setcancelstate>(3), B<pthread_testcancel>(3), B<pthreads>(7)"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "POSIX threads library (I<libpthread>, I<-lpthread>)"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
#, no-wrap
msgid ""
"B<void pthread_cleanup_push(void (*>I<routine>B<)(void *), void *>I<arg>B<);>\n"
"B<void pthread_cleanup_pop(int >I<execute>B<);>\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "POSIX.1-2001, POSIX.1-2008."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"On Linux, the B<pthread_cleanup_push>()  and B<pthread_cleanup_pop>()  "
"functions I<are> implemented as macros that expand to text containing \\"
"[aq]B<{>\\[aq] and \\[aq]B<}>\\[aq], respectively.  This means that "
"variables declared within the scope of paired calls to these functions will "
"be visible within only that scope."
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>pthread.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>sys/types.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"#define handle_error_en(en, msg) \\e\n"
"        do { errno = en; perror(msg); exit(EXIT_FAILURE); } while (0)\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"static int done = 0;\n"
"static int cleanup_pop_arg = 0;\n"
"static int cnt = 0;\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"static void\n"
"cleanup_handler(void *arg)\n"
"{\n"
"    printf(\"Called clean-up handler\\en\");\n"
"    cnt = 0;\n"
"}\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"static void *\n"
"thread_start(void *arg)\n"
"{\n"
"    time_t curr;\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "    printf(\"New thread started\\en\");\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "    pthread_cleanup_push(cleanup_handler, NULL);\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "    curr = time(NULL);\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    while (!done) {\n"
"        pthread_testcancel();           /* A cancelation point */\n"
"        if (curr E<lt> time(NULL)) {\n"
"            curr = time(NULL);\n"
"            printf(\"cnt = %d\\en\", cnt);  /* A cancelation point */\n"
"            cnt++;\n"
"        }\n"
"    }\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    pthread_cleanup_pop(cleanup_pop_arg);\n"
"    return NULL;\n"
"}\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    pthread_t thr;\n"
"    int s;\n"
"    void *res;\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    s = pthread_create(&thr, NULL, thread_start, NULL);\n"
"    if (s != 0)\n"
"        handle_error_en(s, \"pthread_create\");\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "    sleep(2);           /* Allow new thread to run a while */\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    if (argc E<gt> 1) {\n"
"        if (argc E<gt> 2)\n"
"            cleanup_pop_arg = atoi(argv[2]);\n"
"        done = 1;\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    } else {\n"
"        printf(\"Canceling thread\\en\");\n"
"        s = pthread_cancel(thr);\n"
"        if (s != 0)\n"
"            handle_error_en(s, \"pthread_cancel\");\n"
"    }\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    s = pthread_join(thr, &res);\n"
"    if (s != 0)\n"
"        handle_error_en(s, \"pthread_join\");\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    if (res == PTHREAD_CANCELED)\n"
"        printf(\"Thread was canceled; cnt = %d\\en\", cnt);\n"
"    else\n"
"        printf(\"Thread terminated normally; cnt = %d\\en\", cnt);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "2024-06-15"
msgstr ""

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr ""

#. type: Plain text
#: mageia-cauldron
#, no-wrap
msgid ""
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>pthread.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>sys/types.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"\\&\n"
"#define handle_error_en(en, msg) \\e\n"
"        do { errno = en; perror(msg); exit(EXIT_FAILURE); } while (0)\n"
"\\&\n"
"static int done = 0;\n"
"static int cleanup_pop_arg = 0;\n"
"static int cnt = 0;\n"
"\\&\n"
"static void\n"
"cleanup_handler(void *arg)\n"
"{\n"
"    printf(\"Called clean-up handler\\en\");\n"
"    cnt = 0;\n"
"}\n"
"\\&\n"
"static void *\n"
"thread_start(void *arg)\n"
"{\n"
"    time_t curr;\n"
"\\&\n"
"    printf(\"New thread started\\en\");\n"
"\\&\n"
"    pthread_cleanup_push(cleanup_handler, NULL);\n"
"\\&\n"
"    curr = time(NULL);\n"
"\\&\n"
"    while (!done) {\n"
"        pthread_testcancel();           /* A cancelation point */\n"
"        if (curr E<lt> time(NULL)) {\n"
"            curr = time(NULL);\n"
"            printf(\"cnt = %d\\en\", cnt);  /* A cancelation point */\n"
"            cnt++;\n"
"        }\n"
"    }\n"
"\\&\n"
"    pthread_cleanup_pop(cleanup_pop_arg);\n"
"    return NULL;\n"
"}\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    pthread_t thr;\n"
"    int s;\n"
"    void *res;\n"
"\\&\n"
"    s = pthread_create(&thr, NULL, thread_start, NULL);\n"
"    if (s != 0)\n"
"        handle_error_en(s, \"pthread_create\");\n"
"\\&\n"
"    sleep(2);           /* Allow new thread to run a while */\n"
"\\&\n"
"    if (argc E<gt> 1) {\n"
"        if (argc E<gt> 2)\n"
"            cleanup_pop_arg = atoi(argv[2]);\n"
"        done = 1;\n"
"\\&\n"
"    } else {\n"
"        printf(\"Canceling thread\\en\");\n"
"        s = pthread_cancel(thr);\n"
"        if (s != 0)\n"
"            handle_error_en(s, \"pthread_cancel\");\n"
"    }\n"
"\\&\n"
"    s = pthread_join(thr, &res);\n"
"    if (s != 0)\n"
"        handle_error_en(s, \"pthread_join\");\n"
"\\&\n"
"    if (res == PTHREAD_CANCELED)\n"
"        printf(\"Thread was canceled; cnt = %d\\en\", cnt);\n"
"    else\n"
"        printf(\"Thread terminated normally; cnt = %d\\en\", cnt);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr ""
