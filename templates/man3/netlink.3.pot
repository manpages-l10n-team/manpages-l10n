# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-28 16:41+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "netlink"
msgstr ""

#. type: TH
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "2024-11-17"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "netlink - Netlink macros"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "Standard C library (I<libc>,\\ I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>asm/types.hE<gt>>\n"
"B<#include E<lt>linux/netlink.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int NLMSG_ALIGN(size_t >I<size>B<);>\n"
"B<int NLMSG_LENGTH(size_t >I<size>B<);>\n"
"B<int NLMSG_SPACE(size_t >I<size>B<);>\n"
"B<void *NLMSG_DATA(struct nlmsghdr *>I<nlh>B<);>\n"
"B<struct nlmsghdr *NLMSG_NEXT(struct nlmsghdr *>I<nlh>B<, int >I<size>B<);>\n"
"B<int NLMSG_OK(struct nlmsghdr *>I<nlh>B<, int >I<size>B<);>\n"
"B<int NLMSG_PAYLOAD(struct nlmsghdr *>I<nlh>B<, int >I<size>B<);>\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<E<lt>linux/netlink.hE<gt>> defines several standard macros to access or "
"create a netlink datagram.  They are similar in spirit to the macros defined "
"in B<cmsg>(3)  for auxiliary data.  The buffer passed to and from a netlink "
"socket should be accessed using only these macros."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<NLMSG_ALIGN>()"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "Round the size of a netlink message up to align it properly."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<NLMSG_LENGTH>()"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid ""
"Given the payload size, I<size>, this macro returns the aligned size to "
"store in the I<nlmsg_len> field of the I<nlmsghdr>."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<NLMSG_SPACE>()"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid ""
"Return the number of bytes that a netlink message with payload of I<size> "
"would occupy."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<NLMSG_DATA>()"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Return a pointer to the payload associated with the passed I<nlmsghdr>."
msgstr ""

#.  this is bizarre, maybe the interface should be fixed.
#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<NLMSG_NEXT>()"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid ""
"Get the next I<nlmsghdr> in a multipart message.  The caller must check if "
"the current I<nlmsghdr> didn't have the B<NLMSG_DONE> set\\[em]this function "
"doesn't return NULL on end.  The I<size> argument is an lvalue containing "
"the remaining size of the message buffer.  This macro decrements it by the "
"size of the message header."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<NLMSG_OK>()"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Return true if the netlink message is not truncated and is in a form "
"suitable for parsing."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<NLMSG_PAYLOAD>()"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "Return the size of the payload associated with the I<nlmsghdr>."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"It is often better to use netlink via I<libnetlink> than via the low-level "
"kernel interface."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Linux."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<libnetlink>(3), B<netlink>(7)"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
#, no-wrap
msgid ""
"B<int NLMSG_ALIGN(size_t >I<len>B<);>\n"
"B<int NLMSG_LENGTH(size_t >I<len>B<);>\n"
"B<int NLMSG_SPACE(size_t >I<len>B<);>\n"
"B<void *NLMSG_DATA(struct nlmsghdr *>I<nlh>B<);>\n"
"B<struct nlmsghdr *NLMSG_NEXT(struct nlmsghdr *>I<nlh>B<, int >I<len>B<);>\n"
"B<int NLMSG_OK(struct nlmsghdr *>I<nlh>B<, int >I<len>B<);>\n"
"B<int NLMSG_PAYLOAD(struct nlmsghdr *>I<nlh>B<, int >I<len>B<);>\n"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "Round the length of a netlink message up to align it properly."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid ""
"Given the payload length, I<len>, this macro returns the aligned length to "
"store in the I<nlmsg_len> field of the I<nlmsghdr>."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid ""
"Return the number of bytes that a netlink message with payload of I<len> "
"would occupy."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid ""
"Get the next I<nlmsghdr> in a multipart message.  The caller must check if "
"the current I<nlmsghdr> didn't have the B<NLMSG_DONE> set\\[em]this function "
"doesn't return NULL on end.  The I<len> argument is an lvalue containing the "
"remaining length of the message buffer.  This macro decrements it by the "
"length of the message header."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "Return the length of the payload associated with the I<nlmsghdr>."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "These macros are nonstandard Linux extensions."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "2024-05-02"
msgstr ""

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr ""

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr ""
