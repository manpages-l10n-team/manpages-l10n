# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-28 16:39+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "__malloc_hook"
msgstr ""

#. type: TH
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "2025-01-05"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"__malloc_hook, __malloc_initialize_hook, __memalign_hook, __free_hook, "
"__realloc_hook, __after_morecore_hook - malloc debugging variables "
"(DEPRECATED)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "Standard C library (I<libc>,\\ I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>malloc.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid ""
"B<typeof(void *(size_t >I<size>B<, const void *>I<caller>B<))>\n"
"B<*volatile  __malloc_hook;>\n"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid ""
"B<typeof(void *(void *>I<p>B<, size_t >I<size>B<, const void *>I<caller>B<))>\n"
"B<*volatile  __realloc_hook;>\n"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid ""
"B<typeof(void *(size_t >I<align>B<, size_t >I<size>B<, const void *>I<caller>B<))>\n"
"B<*volatile  __memalign_hook;>\n"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid ""
"B<typeof(void *(void *>I<p>B<, const void *>I<caller>B<))>\n"
"B<*volatile  __free_hook;>\n"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid ""
"B<typeof(void (void))            *__malloc_initialize_hook;>\n"
"B<typeof(void (void)) *volatile   __after_mrecore_hook;>\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The GNU C library lets you modify the behavior of B<malloc>(3), "
"B<realloc>(3), and B<free>(3)  by specifying appropriate hook functions.  "
"You can use these hooks to help you debug programs that use dynamic memory "
"allocation, for example."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The variable B<__malloc_initialize_hook> points at a function that is called "
"once when the malloc implementation is initialized.  This is a weak "
"variable, so it can be overridden in the application with a definition like "
"the following:"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "typeof(void (void))  *__malloc_initialize_hook = my_init_hook;\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Now the function I<my_init_hook>()  can do the initialization of all hooks."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The four functions pointed to by B<__malloc_hook>, B<__realloc_hook>, "
"B<__memalign_hook>, B<__free_hook> have a prototype like the functions "
"B<malloc>(3), B<realloc>(3), B<memalign>(3), B<free>(3), respectively, "
"except that they have a final argument I<caller> that gives the address of "
"the caller of B<malloc>(3), etc."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The variable B<__after_morecore_hook> points at a function that is called "
"each time after B<sbrk>(2)  was asked for more memory."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "GNU."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr ""

#.  https://bugzilla.redhat.com/show_bug.cgi?id=450187
#.  http://sourceware.org/bugzilla/show_bug.cgi?id=9957
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The use of these hook functions is not safe in multithreaded programs, and "
"they are now deprecated.  From glibc 2.24 onwards, the "
"B<__malloc_initialize_hook> variable has been removed from the API, and from "
"glibc 2.34 onwards, all the hook variables have been removed from the API.  "
"Programmers should instead preempt calls to the relevant functions by "
"defining and exporting B<malloc>(), B<free>(), B<realloc>(), and B<calloc>()."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Here is a short example of how to use these variables."
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>malloc.hE<gt>\n"
"\\&\n"
"/* Prototypes for our hooks */\n"
"static void my_init_hook(void);\n"
"static void *my_malloc_hook(size_t, const void *);\n"
"\\&\n"
"/* Variables to save original hooks */\n"
"static typeof(void *(size_t, const void *))  *old_malloc_hook;\n"
"\\&\n"
"/* Override initializing hook from the C library */\n"
"typeof(void (void))  *__malloc_initialize_hook = my_init_hook;\n"
"\\&\n"
"static void\n"
"my_init_hook(void)\n"
"{\n"
"    old_malloc_hook = __malloc_hook;\n"
"    __malloc_hook = my_malloc_hook;\n"
"}\n"
"\\&\n"
"static void *\n"
"my_malloc_hook(size_t size, const void *caller)\n"
"{\n"
"    void *result;\n"
"\\&\n"
"    /* Restore all old hooks */\n"
"    __malloc_hook = old_malloc_hook;\n"
"\\&\n"
"    /* Call recursively */\n"
"    result = malloc(size);\n"
"\\&\n"
"    /* Save underlying hooks */\n"
"    old_malloc_hook = __malloc_hook;\n"
"\\&\n"
"    /* printf() might call malloc(), so protect it too */\n"
"    printf(\"malloc(%zu) called from %p returns %p\\[rs]n\",\n"
"            size, caller, result);\n"
"\\&\n"
"    /* Restore our own hooks */\n"
"    __malloc_hook = my_malloc_hook;\n"
"\\&\n"
"    return result;\n"
"}\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<mallinfo>(3), B<malloc>(3), B<mcheck>(3), B<mtrace>(3)"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-01-07"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
#, no-wrap
msgid "B<void *(*volatile __malloc_hook)(size_t >I<size>B<, const void *>I<caller>B<);>\n"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
#, no-wrap
msgid ""
"B<void *(*volatile __realloc_hook)(void *>I<ptr>B<, size_t >I<size>B<,>\n"
"B<                         const void *>I<caller>B<);>\n"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
#, no-wrap
msgid ""
"B<void *(*volatile __memalign_hook)(size_t >I<alignment>B<, size_t >I<size>B<,>\n"
"B<                         const void *>I<caller>B<);>\n"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
#, no-wrap
msgid "B<void (*volatile __free_hook)(void *>I<ptr>B<, const void *>I<caller>B<);>\n"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
#, no-wrap
msgid "B<void (*__malloc_initialize_hook)(void);>\n"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
#, no-wrap
msgid "B<void (*volatile __after_morecore_hook)(void);>\n"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
#, no-wrap
msgid "void (*__malloc_initialize_hook)(void) = my_init_hook;\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "These functions are GNU extensions."
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>malloc.hE<gt>\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"/* Prototypes for our hooks */\n"
"static void my_init_hook(void);\n"
"static void *my_malloc_hook(size_t, const void *);\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"/* Variables to save original hooks */\n"
"static void *(*old_malloc_hook)(size_t, const void *);\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"/* Override initializing hook from the C library */\n"
"void (*__malloc_initialize_hook)(void) = my_init_hook;\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"static void\n"
"my_init_hook(void)\n"
"{\n"
"    old_malloc_hook = __malloc_hook;\n"
"    __malloc_hook = my_malloc_hook;\n"
"}\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"static void *\n"
"my_malloc_hook(size_t size, const void *caller)\n"
"{\n"
"    void *result;\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    /* Restore all old hooks */\n"
"    __malloc_hook = old_malloc_hook;\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    /* Call recursively */\n"
"    result = malloc(size);\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    /* Save underlying hooks */\n"
"    old_malloc_hook = __malloc_hook;\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    /* printf() might call malloc(), so protect it too */\n"
"    printf(\"malloc(%zu) called from %p returns %p\\en\",\n"
"            size, caller, result);\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    /* Restore our own hooks */\n"
"    __malloc_hook = my_malloc_hook;\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    return result;\n"
"}\n"
msgstr ""

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "2024-06-15"
msgstr ""

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>malloc.hE<gt>\n"
"\\&\n"
"/* Prototypes for our hooks */\n"
"static void my_init_hook(void);\n"
"static void *my_malloc_hook(size_t, const void *);\n"
"\\&\n"
"/* Variables to save original hooks */\n"
"static void *(*old_malloc_hook)(size_t, const void *);\n"
"\\&\n"
"/* Override initializing hook from the C library */\n"
"void (*__malloc_initialize_hook)(void) = my_init_hook;\n"
"\\&\n"
"static void\n"
"my_init_hook(void)\n"
"{\n"
"    old_malloc_hook = __malloc_hook;\n"
"    __malloc_hook = my_malloc_hook;\n"
"}\n"
"\\&\n"
"static void *\n"
"my_malloc_hook(size_t size, const void *caller)\n"
"{\n"
"    void *result;\n"
"\\&\n"
"    /* Restore all old hooks */\n"
"    __malloc_hook = old_malloc_hook;\n"
"\\&\n"
"    /* Call recursively */\n"
"    result = malloc(size);\n"
"\\&\n"
"    /* Save underlying hooks */\n"
"    old_malloc_hook = __malloc_hook;\n"
"\\&\n"
"    /* printf() might call malloc(), so protect it too */\n"
"    printf(\"malloc(%zu) called from %p returns %p\\[rs]n\",\n"
"            size, caller, result);\n"
"\\&\n"
"    /* Restore our own hooks */\n"
"    __malloc_hook = my_malloc_hook;\n"
"\\&\n"
"    return result;\n"
"}\n"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr ""

#. type: Plain text
#: mageia-cauldron
#, no-wrap
msgid ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>malloc.hE<gt>\n"
"\\&\n"
"/* Prototypes for our hooks */\n"
"static void my_init_hook(void);\n"
"static void *my_malloc_hook(size_t, const void *);\n"
"\\&\n"
"/* Variables to save original hooks */\n"
"static void *(*old_malloc_hook)(size_t, const void *);\n"
"\\&\n"
"/* Override initializing hook from the C library */\n"
"void (*__malloc_initialize_hook)(void) = my_init_hook;\n"
"\\&\n"
"static void\n"
"my_init_hook(void)\n"
"{\n"
"    old_malloc_hook = __malloc_hook;\n"
"    __malloc_hook = my_malloc_hook;\n"
"}\n"
"\\&\n"
"static void *\n"
"my_malloc_hook(size_t size, const void *caller)\n"
"{\n"
"    void *result;\n"
"\\&\n"
"    /* Restore all old hooks */\n"
"    __malloc_hook = old_malloc_hook;\n"
"\\&\n"
"    /* Call recursively */\n"
"    result = malloc(size);\n"
"\\&\n"
"    /* Save underlying hooks */\n"
"    old_malloc_hook = __malloc_hook;\n"
"\\&\n"
"    /* printf() might call malloc(), so protect it too */\n"
"    printf(\"malloc(%zu) called from %p returns %p\\en\",\n"
"            size, caller, result);\n"
"\\&\n"
"    /* Restore our own hooks */\n"
"    __malloc_hook = my_malloc_hook;\n"
"\\&\n"
"    return result;\n"
"}\n"
msgstr ""

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr ""
