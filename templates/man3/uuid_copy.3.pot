# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-03-29 10:47+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "UUID_COPY"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-07-20"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Programmer\\(aqs Manual"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "uuid_copy - copy a UUID value"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<#include E<lt>uuid.hE<gt>>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<void uuid_copy(uuid_t >I<dst>B<, uuid_t >I<src>B<);>"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "The B<uuid_copy>() function copies the UUID variable I<src> to I<dst>."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "The copied UUID is returned in the location pointed to by I<dst>."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AUTHORS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Theodore Y. Ts\\(cqo"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<uuid>(3), B<uuid_clear>(3), B<uuid_compare>(3), B<uuid_generate>(3), "
"B<uuid_is_null>(3), B<uuid_parse>(3), B<uuid_unparse>(3)"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "For bug reports, use the issue tracker at"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<libuuid> library is part of the util-linux package since version "
"2.15.1. It can be downloaded from"
msgstr ""
