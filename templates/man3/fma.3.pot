# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-28 16:33+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "fma"
msgstr ""

#. type: TH
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "2024-07-23"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "fma, fmaf, fmal - floating-point multiply and add"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "Math library (I<libm>,\\ I<-lm>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>math.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<double fma(double >I<x>B<, double >I<y>B<, double >I<z>B<);>\n"
"B<float fmaf(float >I<x>B<, float >I<y>B<, float >I<z>B<);>\n"
"B<long double fmal(long double >I<x>B<, long double >I<y>B<, long double >I<z>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<fma>(), B<fmaf>(), B<fmal>():"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"These functions compute I<x> * I<y> + I<z>.  The result is rounded as one "
"ternary operation according to the current rounding mode (see B<fenv>(3))."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"These functions return the value of I<x> * I<y> + I<z>, rounded as one "
"ternary operation."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "If I<x> or I<y> is a NaN, a NaN is returned."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If I<x> times I<y> is an exact infinity, and I<z> is an infinity with the "
"opposite sign, a domain error occurs, and a NaN is returned."
msgstr ""

#.  POSIX.1-2008 allows some possible differences for the following two
#.  domain error cases, but on Linux they are treated the same (AFAICS).
#.  Nevertheless, we'll mirror POSIX.1 and describe the two cases
#.  separately.
#.  POSIX.1 says that a NaN or an implementation-defined value shall
#.  be returned for this case.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If one of I<x> or I<y> is an infinity, the other is 0, and I<z> is not a "
"NaN, a domain error occurs, and a NaN is returned."
msgstr ""

#.  POSIX.1 makes the domain error optional for this case.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If one of I<x> or I<y> is an infinity, and the other is 0, and I<z> is a "
"NaN, a domain error occurs, and a NaN is returned."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If I<x> times I<y> is not an infinity times zero (or vice versa), and I<z> "
"is a NaN, a NaN is returned."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the result overflows, a range error occurs, and an infinity with the "
"correct sign is returned."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the result underflows, a range error occurs, and a signed 0 is returned."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"See B<math_error>(7)  for information on how to determine whether an error "
"has occurred when calling these functions."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The following errors can occur:"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Domain error: I<x> * I<y> + I<z>, or I<x> * I<y> is invalid and I<z> is not a NaN"
msgstr ""

#.  .I errno
#.  is set to
#.  .BR EDOM .
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "An invalid floating-point exception (B<FE_INVALID>)  is raised."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Range error: result overflow"
msgstr ""

#.  .I errno
#.  is set to
#.  .BR ERANGE .
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "An overflow floating-point exception (B<FE_OVERFLOW>)  is raised."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Range error: result underflow"
msgstr ""

#.  .I errno
#.  is set to
#.  .BR ERANGE .
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "An underflow floating-point exception (B<FE_UNDERFLOW>)  is raised."
msgstr ""

#. #-#-#-#-#  archlinux: fma.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . Is it intentional that these functions do not set errno?
#.  Bug raised: https://www.sourceware.org/bugzilla/show_bug.cgi?id=6801
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: fma.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . Is it intentional that these functions do not set errno?
#.  Bug raised: http://sources.redhat.com/bugzilla/show_bug.cgi?id=6801
#. type: Plain text
#. #-#-#-#-#  debian-unstable: fma.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . Is it intentional that these functions do not set errno?
#.  Bug raised: https://www.sourceware.org/bugzilla/show_bug.cgi?id=6801
#. type: Plain text
#. #-#-#-#-#  fedora-42: fma.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . Is it intentional that these functions do not set errno?
#.  Bug raised: https://www.sourceware.org/bugzilla/show_bug.cgi?id=6801
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: fma.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . Is it intentional that these functions do not set errno?
#.  Bug raised: https://www.sourceware.org/bugzilla/show_bug.cgi?id=6801
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: fma.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . Is it intentional that these functions do not set errno?
#.  Bug raised: https://www.sourceware.org/bugzilla/show_bug.cgi?id=6801
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-16-0: fma.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . Is it intentional that these functions do not set errno?
#.  Bug raised: https://www.sourceware.org/bugzilla/show_bug.cgi?id=6801
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: fma.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . Is it intentional that these functions do not set errno?
#.  Bug raised: https://www.sourceware.org/bugzilla/show_bug.cgi?id=6801
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "These functions do not set I<errno>."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr ""

#. type: tbl table
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ""

#. type: tbl table
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<fma>(),\n"
"B<fmaf>(),\n"
"B<fmal>()"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "glibc 2.1.  C99, POSIX.1-2001."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<remainder>(3), B<remquo>(3)"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-12-15"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "Math library (I<libm>, I<-lm>)"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "VERSIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "These functions were added in glibc 2.1."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "C99, POSIX.1-2001, POSIX.1-2008."
msgstr ""

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "2024-05-02"
msgstr ""

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr ""

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr ""
