# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-03-29 10:47+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "UUID_TIME"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Programmer\\(aqs Manual"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "uuid_time - extract the time at which the UUID was created"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<#include E<lt>uuid.hE<gt>>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<time_t uuid_time(uuid_t >I<uu>B<, struct timeval *>I<ret_tv>B<)>"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<uuid_time>() function extracts the time at which the supplied time-"
"based UUID I<uu> was created. Note that the UUID creation time is only "
"encoded within certain types of UUIDs. This function can only reasonably "
"expect to extract the creation time for UUIDs created with the "
"B<uuid_generate_time>(3) and B<uuid_generate_time_safe>(3) functions. It may "
"or may not work with UUIDs created by other mechanisms."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The time at which the UUID was created, in seconds since January 1, 1970 GMT "
"(the epoch), is returned (see B<time>(2)). The time at which the UUID was "
"created, in seconds and microseconds since the epoch, is also stored in the "
"location pointed to by I<ret_tv> (see B<gettimeofday>(2))."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AUTHORS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Theodore Y. Ts\\(cqo"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<uuid>(3), B<uuid_clear>(3), B<uuid_compare>(3), B<uuid_copy>(3), "
"B<uuid_generate>(3), B<uuid_is_null>(3), B<uuid_parse>(3), B<uuid_unparse>(3)"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "For bug reports, use the issue tracker at"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<libuuid> library is part of the util-linux package since version "
"2.15.1. It can be downloaded from"
msgstr ""
