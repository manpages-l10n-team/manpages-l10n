# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-28 16:51+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "strerror"
msgstr ""

#. type: TH
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "2024-12-24"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"strerror, strerrorname_np, strerrordesc_np, strerror_r, strerror_l - return "
"string describing error number"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "Standard C library (I<libc>,\\ I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>string.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<char *strerror(int >I<errnum>B<);>\n"
"B<const char *strerrorname_np(int >I<errnum>B<);>\n"
"B<const char *strerrordesc_np(int >I<errnum>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int strerror_r(int >I<errnum>B<, char >I<buf>B<[.>I<size>B<], size_t >I<size>B<);>\n"
"               /* XSI-compliant */\n"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid ""
"B<char *strerror_r(int >I<errnum>B<, char >I<buf>B<[.>I<size>B<], size_t >I<size>B<);>\n"
"               /* GNU-specific */\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<char *strerror_l(int >I<errnum>B<, locale_t >I<locale>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<strerrorname_np>(), B<strerrordesc_np>():"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "    _GNU_SOURCE\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<strerror_r>():"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"    The XSI-compliant version is provided if:\n"
"        (_POSIX_C_SOURCE E<gt>= 200112L) && ! _GNU_SOURCE\n"
"    Otherwise, the GNU-specific version is provided.\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<strerror>()  function returns a pointer to a string that describes the "
"error code passed in the argument I<errnum>, possibly using the "
"B<LC_MESSAGES> part of the current locale to select the appropriate "
"language.  (For example, if I<errnum> is B<EINVAL>, the returned description "
"will be \"Invalid argument\".)  This string must not be modified by the "
"application, and the returned pointer will be invalidated on a subsequent "
"call to B<strerror>()  or B<strerror_l>(), or if the thread that obtained "
"the string exits.  No other library function, including B<perror>(3), will "
"modify this string."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Like B<strerror>(), the B<strerrordesc_np>()  function returns a pointer to "
"a string that describes the error code passed in the argument I<errnum>, "
"with the difference that the returned string is not translated according to "
"the current locale."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<strerrorname_np>()  function returns a pointer to a string containing "
"the name of the error code passed in the argument I<errnum>.  For example, "
"given B<EPERM> as an argument, this function returns a pointer to the string "
"\"EPERM\".  Given B<0> as an argument, this function returns a pointer to "
"the string \"0\"."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "strerror_r()"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<strerror_r>()  is like B<strerror>(), but might use the supplied buffer "
"I<buf> instead of allocating one internally.  This function is available in "
"two versions: an XSI-compliant version specified in POSIX.1-2001 (available "
"since glibc 2.3.4, but not POSIX-compliant until glibc 2.13), and a GNU-"
"specific version (available since glibc 2.0).  The XSI-compliant version is "
"provided with the feature test macros settings shown in the SYNOPSIS; "
"otherwise the GNU-specific version is provided.  If no feature test macros "
"are explicitly defined, then (since glibc 2.4)  B<_POSIX_C_SOURCE> is "
"defined by default with the value 200112L, so that the XSI-compliant version "
"of B<strerror_r>()  is provided by default."
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid ""
"The XSI-compliant B<strerror_r>()  is preferred for portable applications.  "
"It returns the error string in the user-supplied buffer I<buf> of size "
"I<size>."
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid ""
"The GNU-specific B<strerror_r>()  returns a pointer to a string containing "
"the error message.  This may be either a pointer to a string that the "
"function stores in I<buf>, or a pointer to some (immutable) static string "
"(in which case I<buf> is unused).  If the function stores a string in "
"I<buf>, then at most I<size> bytes are stored (the string may be truncated "
"if I<size> is too small and I<errnum> is unknown).  The string always "
"includes a terminating null byte (\\[aq]\\[rs]0\\[aq])."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "strerror_l()"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<strerror_l>()  is like B<strerror>(), but maps I<errnum> to a locale-"
"dependent error message in the locale specified by I<locale>.  The behavior "
"of B<strerror_l>()  is undefined if I<locale> is the special locale object "
"B<LC_GLOBAL_LOCALE> or is not a valid locale object handle."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<strerror>(), B<strerror_l>(), and the GNU-specific B<strerror_r>()  "
"functions return the appropriate error description string, or an \"Unknown "
"error nnn\" message if the error number is unknown."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On success, B<strerrorname_np>()  and B<strerrordesc_np>()  return the "
"appropriate error description string.  If I<errnum> is an invalid error "
"number, these functions return NULL."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The XSI-compliant B<strerror_r>()  function returns 0 on success.  On error, "
"a (positive) error number is returned (since glibc 2.13), or -1 is returned "
"and I<errno> is set to indicate the error (before glibc 2.13)."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"POSIX.1-2001 and POSIX.1-2008 require that a successful call to "
"B<strerror>()  or B<strerror_l>()  shall leave I<errno> unchanged, and note "
"that, since no function return value is reserved to indicate an error, an "
"application that wishes to check for errors should initialize I<errno> to "
"zero before the call, and then check I<errno> after the call."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The value of I<errnum> is not a valid error number."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ERANGE>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Insufficient storage was supplied to contain the error description string."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr ""

#. type: tbl table
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ""

#. type: tbl table
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ""

#. #-#-#-#-#  archlinux: strerror.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  debian-bookworm: strerror.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: tbl table
#. #-#-#-#-#  debian-unstable: strerror.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-42: strerror.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-rawhide: strerror.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  mageia-cauldron: strerror.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  opensuse-leap-16-0: strerror.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  opensuse-tumbleweed: strerror.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<strerror>()"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<strerrorname_np>(),\n"
"B<strerrordesc_np>()"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<strerror_r>(),\n"
"B<strerror_l>()"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Before glibc 2.32, B<strerror>()  is not MT-Safe."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<strerror_r>()"
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<strerror_l>()"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<strerrorname_np>()"
msgstr ""

#. type: TQ
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<strerrordesc_np>()"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "GNU."
msgstr ""

#.  e.g., Solaris 8, HP-UX 11
#.  e.g., FreeBSD 5.4, Tru64 5.1B
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"POSIX.1-2001 permits B<strerror>()  to set I<errno> if the call encounters "
"an error, but does not specify what value should be returned as the function "
"result in the event of an error.  On some systems, B<strerror>()  returns "
"NULL if the error number is unknown.  On other systems, B<strerror>()  "
"returns a string something like \"Error nnn occurred\" and sets I<errno> to "
"B<EINVAL> if the error number is unknown.  C99 and POSIX.1-2008 require the "
"return value to be non-NULL."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2001, C89."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2001."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "glibc 2.6.  POSIX.1-2008."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "glibc 2.32."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<strerrorname_np>()  and B<strerrordesc_np>()  are thread-safe and async-"
"signal-safe."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<err>(3), B<errno>(3), B<error>(3), B<perror>(3), B<strsignal>(3), "
"B<locale>(7), B<signal-safety>(7)"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
#, no-wrap
msgid ""
"B<int strerror_r(int >I<errnum>B<, char >I<buf>B<[.>I<buflen>B<], size_t >I<buflen>B<);>\n"
"               /* XSI-compliant */\n"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
#, no-wrap
msgid ""
"B<char *strerror_r(int >I<errnum>B<, char >I<buf>B<[.>I<buflen>B<], size_t >I<buflen>B<);>\n"
"               /* GNU-specific */\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<strerror>()  function returns a pointer to a string that describes the "
"error code passed in the argument I<errnum>, possibly using the "
"B<LC_MESSAGES> part of the current locale to select the appropriate "
"language.  (For example, if I<errnum> is B<EINVAL>, the returned description "
"will be \"Invalid argument\".)  This string must not be modified by the "
"application, but may be modified by a subsequent call to B<strerror>()  or "
"B<strerror_l>().  No other library function, including B<perror>(3), will "
"modify this string."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<strerrorname_np>()  function returns a pointer to a string containing "
"the name of the error code passed in the argument I<errnum>.  For example, "
"given B<EPERM> as an argument, this function returns a pointer to the string "
"\"EPERM\"."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<strerror_r>()  function is similar to B<strerror>(), but is thread "
"safe.  This function is available in two versions: an XSI-compliant version "
"specified in POSIX.1-2001 (available since glibc 2.3.4, but not POSIX-"
"compliant until glibc 2.13), and a GNU-specific version (available since "
"glibc 2.0).  The XSI-compliant version is provided with the feature test "
"macros settings shown in the SYNOPSIS; otherwise the GNU-specific version is "
"provided.  If no feature test macros are explicitly defined, then (since "
"glibc 2.4)  B<_POSIX_C_SOURCE> is defined by default with the value 200112L, "
"so that the XSI-compliant version of B<strerror_r>()  is provided by default."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid ""
"The XSI-compliant B<strerror_r>()  is preferred for portable applications.  "
"It returns the error string in the user-supplied buffer I<buf> of length "
"I<buflen>."
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"The GNU-specific B<strerror_r>()  returns a pointer to a string containing "
"the error message.  This may be either a pointer to a string that the "
"function stores in I<buf>, or a pointer to some (immutable) static string "
"(in which case I<buf> is unused).  If the function stores a string in "
"I<buf>, then at most I<buflen> bytes are stored (the string may be truncated "
"if I<buflen> is too small and I<errnum> is unknown).  The string always "
"includes a terminating null byte (\\[aq]\\e0\\[aq])."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "VERSIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "The B<strerror_l>()  function first appeared in glibc 2.6."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<strerrorname_np>()  and B<strerrordesc_np>()  functions first appeared "
"in glibc 2.32."
msgstr ""

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "MT-Unsafe race:strerror"
msgstr ""

#.  FIXME . for later review when Issue 8 is one day released...
#.  A future POSIX.1 may remove strerror_r()
#.  http://austingroupbugs.net/tag_view_page.php?tag_id=8
#.  http://austingroupbugs.net/view.php?id=508
#. type: Plain text
#: debian-bookworm
msgid ""
"B<strerror>()  is specified by POSIX.1-2001, POSIX.1-2008, and C99.  "
"B<strerror_r>()  is specified by POSIX.1-2001 and POSIX.1-2008."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<strerror_l>()  is specified in POSIX.1-2008."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The GNU-specific functions B<strerror_r>(), B<strerrorname_np>(), and "
"B<strerrordesc_np>()  are nonstandard extensions."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The GNU C Library uses a buffer of 1024 characters for B<strerror>().  This "
"buffer size therefore should be sufficient to avoid an B<ERANGE> error when "
"calling B<strerror_r>()."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<err>(3), B<errno>(3), B<error>(3), B<perror>(3), B<strsignal>(3), "
"B<locale>(7)"
msgstr ""

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "2024-06-15"
msgstr ""

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
msgid ""
"The GNU-specific B<strerror_r>()  returns a pointer to a string containing "
"the error message.  This may be either a pointer to a string that the "
"function stores in I<buf>, or a pointer to some (immutable) static string "
"(in which case I<buf> is unused).  If the function stores a string in "
"I<buf>, then at most I<buflen> bytes are stored (the string may be truncated "
"if I<buflen> is too small and I<errnum> is unknown).  The string always "
"includes a terminating null byte (\\[aq]\\[rs]0\\[aq])."
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr ""

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr ""
