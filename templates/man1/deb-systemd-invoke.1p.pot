# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-01-10 10:18+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: ds C+
#: debian-bookworm
#, no-wrap
msgid "C\\v'-.1v'\\h'-1p'\\s-2+\\h'-1p'+\\s0\\v'.1v'\\h'-1p'"
msgstr ""

#.  ========================================================================
#. type: IX
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Title"
msgstr ""

#.  ========================================================================
#. type: IX
#: debian-bookworm debian-unstable
#, no-wrap
msgid "DEB-SYSTEMD-INVOKE 1p"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "DEB-SYSTEMD-INVOKE"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-09-18"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "1.65.2"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "init-system-helpers"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "deb-systemd-invoke - wrapper around systemctl, respecting policy-rc.d"
msgstr ""

#. type: IX
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: IX
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Header"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"\\&B<deb-systemd-invoke> [B<--user>] start|stop|restart I<unit file> ..."
msgstr ""

#. type: IX
#: debian-bookworm debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"\\&B<deb-systemd-invoke> is a Debian-specific helper script which asks /usr/"
"sbin/policy-rc.d before performing a systemctl call."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"\\&B<deb-systemd-invoke> is intended to be used from maintscripts to start "
"systemd unit files. It is specifically \\s-1NOT\\s0 intended to be used "
"interactively by users. Instead, users should run systemd and use systemctl, "
"or not bother about the systemd enabled state in case they are not running "
"systemd."
msgstr ""

#. type: TH
#: debian-unstable
#, no-wrap
msgid "2024-12-29"
msgstr ""

#. type: TH
#: debian-unstable
#, no-wrap
msgid "1.68"
msgstr ""

#. type: Plain text
#: debian-unstable
msgid ""
"\\&B<deb-systemd-invoke> [B<--user>] start|stop|restart I<unit\\ "
"file>\\ ...  \\&B<deb-systemd-invoke> [B<--user>] [B<--no-dbus>] daemon-"
"reload|daemon-reexec"
msgstr ""

#. type: Plain text
#: debian-unstable
msgid ""
"\\&B<deb-systemd-invoke> is intended to be used from maintscripts to manage "
"systemd unit files. It is specifically NOT intended to be used interactively "
"by users. Instead, users should run systemd and use systemctl, or not bother "
"about the systemd enabled state in case they are not running systemd."
msgstr ""
