# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-25 22:55-0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux
#, no-wrap
msgid "DIFFPKG"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "2025-01-06"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "\\ \" "
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "\\ \""
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux
msgid "diffpkg - Compare package files using different modes."
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux
msgid "diffpkg [OPTIONS] [MODES] [FILE|PKGNAME...]"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Searches for a locally built package corresponding to the PKGBUILD, and "
"downloads the last version of that package from the Pacman repositories.  It "
"then compares the package archives using different modes while using simple "
"tar content list by default."
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"When given one package, use it to diff against the locally built one.  When "
"given two packages, diff both packages against each other."
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"In either case, a package name will be converted to a filename from the "
"cache or pool, and diffpkg will proceed as though this filename was "
"initially specified."
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-M, --makepkg-config>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Set an alternate makepkg configuration file"
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-P, --pool>=I<DIR>"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Search diff target in pool dir (default \\f(CR\\*(Aq/srv/ftp/pool\\*(Aq\\fR)"
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-v, --verbose>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Provide more detailed/unfiltered output"
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-h, --help>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Show a help text"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "OUTPUT OPTIONS"
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<--color>[=I<WHEN>]"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Color output; I<WHEN> is \\f(CR\\*(Aqnever\\*(Aq\\fR, "
"\\f(CR\\*(Aqalways\\*(Aq\\fR, or \\f(CR\\*(Aqauto\\*(Aq\\fR; Plain B<--"
"color> means B<--color=>I<auto>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-u, -U, --unified>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "I<Output 3 lines of unified context>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-y, --side-by-side>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "I<Output in two columns>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-W, --width>I<[=NUM]>"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"I<Output at most NUM (default >\\f(CR\\*(Aqauto\\*(AqI<) print columns; NUM "
"can be >\\f(CR\\*(Aqauto\\*(AqI<, >\\f(CR\\*(Aqcolumns\\*(AqI< or a number.  "
">\\f(CR\\*(Aqauto\\*(AqI< will be resolved to the maximum line length of "
"both files, guaranteeing the diff to be uncut.>"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "MODES"
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-l, --list>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Activate tar content list diff mode (default)"
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-d, --diffoscope>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Activate diffoscope diff mode"
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-p, --pkginfo>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Activate .PKGINFO diff mode"
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-b, --buildinfo>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Activate .BUILDINFO diff mode"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "HOMEPAGE"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"I<Please report bugs and feature requests in the issue tracker. Please do "
"your best to provide a reproducible test case for bugs.>"
msgstr ""
