# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-03-29 09:42+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "IRQTOP"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "March 2018"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "irqtop"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "User Commands"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "irqtop - Observe IRQ and SoftIRQ in a top-like fashion"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Shows interrupt rates (per second) per cpu.  Also shows irq affinity ('.' "
"for disabled cpus), and rps/xps affinity ('+' rx, '-' tx, '*' tx/rx).  Can "
"show packet rate per eth queue."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "irqtop [-h] [-d] [-b] [-t|-x] [-i|-s] [-r]"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-d>, B<--delay>=I<\\,n\\/>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "refresh interval"
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-s>, B<--softirq>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "select softirqs only"
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-i>, B<--irq>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "select hardware irqs only"
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-e>, B<--eth>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "show extra eth stats (from ethtool)"
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-R>, B<--rps>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "enable display of rps/xps"
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-x>, B<--table>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "output in table mode (default)"
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-t>, B<--top>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "output in flat top mode"
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-b>, B<--batch>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "output non-interactively"
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-r>, B<--reverse>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "reverse sort order"
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-C>, B<--nocolor>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "disable colors"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "Rates marked as '.' is forbidden by smp_affinity mask."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "htop(1), top(1), ntop(1)"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "AUTHOR AND LICENSE"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"I<irqtop> was written by ABC E<lt>abc@openwall.comE<gt> and is licensed "
"under the GNU GPL."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"This man page was written by Axel Beckert E<lt>abe@debian.orgE<gt> for the "
"Debian GNU/Linux distribution (but it may be used by others)."
msgstr ""

#. type: Plain text
#: debian-unstable
msgid "show extra eth stats (from ethtool(8))"
msgstr ""
