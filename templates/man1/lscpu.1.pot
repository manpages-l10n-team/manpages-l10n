# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-12-22 07:29+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "LSCPU"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-08-04"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "User Commands"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "lscpu - display information about the CPU architecture"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<lscpu> [options]"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<lscpu> gathers CPU architecture information from I<sysfs>, I</proc/"
"cpuinfo> and any applicable architecture-specific libraries (e.g. B<librtas> "
"on Powerpc). The command output can be optimized for parsing or for easy "
"readability by humans. The information includes, for example, the number of "
"CPUs, threads, cores, sockets, and Non-Uniform Memory Access (NUMA) nodes. "
"There is also information about the CPU caches and cache sharing, family, "
"model, bogoMIPS, byte order, and stepping."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The default output formatting on terminal is subject to change and maybe "
"optimized for better readability. The output for non-terminals (e.g., pipes) "
"is never affected by this optimization and it is always in \"Field: data\\"
"(rsn\" format. Use for example \"B<lscpu | less>\" to see the default output "
"without optimizations."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"In virtualized environments, the CPU architecture information displayed "
"reflects the configuration of the guest operating system which is typically "
"different from the physical (host) system. On architectures that support "
"retrieving physical topology information, B<lscpu> also displays the number "
"of physical sockets, chips, cores in the host system."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Options that result in an output table have a I<list> argument. Use this "
"argument to customize the command output. Specify a comma-separated list of "
"column labels to limit the output table to only the specified columns, "
"arranged in the specified order. See B<COLUMNS> for a list of valid column "
"labels. The column labels are not case sensitive."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Not all columns are supported on all architectures. If an unsupported column "
"is specified, B<lscpu> prints the column but does not provide any data for "
"it."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The cache sizes are reported as summary from all CPUs. The versions before "
"v2.34 reported per-core sizes, but this output was confusing due to "
"complicated CPUs topology and the way how caches are shared between CPUs. "
"For more details about caches see B<--cache>. Since version v2.37 B<lscpu> "
"follows cache IDs as provided by Linux kernel and it does not always start "
"from zero."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-a>, B<--all>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Include lines for online and offline CPUs in the output (default for B<-e>). "
"This option may only be specified together with option B<-e> or B<-p>."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-B>, B<--bytes>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Print the sizes in bytes rather than in a human-readable format."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"By default, the unit, sizes are expressed in, is byte, and unit prefixes are "
"in power of 2^10 (1024). Abbreviations of symbols are exhibited truncated in "
"order to reach a better readability, by exhibiting alone the first letter of "
"them; examples: \"1 KiB\" and \"1 MiB\" are respectively exhibited as \"1 "
"K\" and \"1 M\", then omitting on purpose the mention \"iB\", which is part "
"of these abbreviations."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-b>, B<--online>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Limit the output to online CPUs (default for B<-p>). This option may only be "
"specified together with option B<-e> or B<-p>."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-C>, B<--caches>[=I<list>]"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Display details about CPU caches. For details about available information "
"see B<--help> output."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"If the I<list> argument is omitted, all columns for which data is available "
"are included in the command output."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"When specifying the I<list> argument, the string of option, equal sign (=), "
"and I<list> must not contain any blanks or other whitespace. Examples: B<-"
"C=NAME,ONE-SIZE> or B<--caches=NAME,ONE-SIZE>."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The default list of columns may be extended if list is specified in the "
"format +list (e.g., B<lscpu -C=+ALLOC-POLICY>)."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-c>, B<--offline>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Limit the output to offline CPUs. This option may only be specified together "
"with option B<-e> or B<-p>."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-e>, B<--extended>[=I<list>]"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Display the CPU information in human-readable format."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"If the I<list> argument is omitted, the default columns are included in the "
"command output.  The default output is subject to change."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"When specifying the I<list> argument, the string of option, equal sign (=), "
"and I<list> must not contain any blanks or other whitespace. Examples: \\"
"(aqB<-e=cpu,node>\\(aq or \\(aqB<--extended=cpu,node>\\(aq."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The default list of columns may be extended if list is specified in the "
"format +list (e.g., lscpu -e=+MHZ)."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-J>, B<--json>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Use JSON output format for the default summary or extended output (see B<--"
"extended>)."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-p>, B<--parse>[=I<list>]"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Optimize the command output for easy parsing."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"If the I<list> argument is omitted, the command output is compatible with "
"earlier versions of B<lscpu>. In this compatible format, two commas are used "
"to separate CPU cache columns. If no CPU caches are identified the cache "
"column is omitted. If the I<list> argument is used, cache columns are "
"separated with a colon (:)."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"When specifying the I<list> argument, the string of option, equal sign (=), "
"and I<list> must not contain any blanks or other whitespace. Examples: \\"
"(aqB<-p=cpu,node>\\(aq or \\(aqB<--parse=cpu,node>\\(aq."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The default list of columns may be extended if list is specified in the "
"format +list (e.g., lscpu -p=+MHZ)."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-s>, B<--sysroot> I<directory>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Gather CPU data for a Linux instance other than the instance from which the "
"B<lscpu> command is issued. The specified I<directory> is the system root of "
"the Linux instance to be inspected."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-x>, B<--hex>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Use hexadecimal masks for CPU sets (for example \"ff\"). The default is to "
"print the sets in list format (for example 0,1). Note that before version "
"2.30 the mask has been printed with 0x prefix."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-y>, B<--physical>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Display physical IDs for all columns with topology elements (core, socket, "
"etc.). Other than logical IDs, which are assigned by B<lscpu>, physical IDs "
"are platform-specific values that are provided by the kernel. Physical IDs "
"are not necessarily unique and they might not be arranged sequentially. If "
"the kernel could not retrieve a physical ID for an element B<lscpu> prints "
"the dash (-) character."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "The CPU logical numbers are not affected by this option."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<--output-all>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Output all available columns. This option must be combined with either B<--"
"extended>, B<--parse> or B<--caches>."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "BUGS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The basic overview of CPU family, model, etc. is always based on the first "
"CPU only."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Sometimes in Xen Dom0 the kernel reports wrong data."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "On virtual hardware the number of cores per socket, etc. can be wrong."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AUTHORS"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<chcpu>(8)"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "For bug reports, use the issue tracker at"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<lscpu> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""
