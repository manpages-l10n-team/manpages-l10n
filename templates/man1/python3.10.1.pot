# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-16 05:58+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "PYTHON"
msgstr ""

#.  To view this file while editing, run it through groff:
#.    groff -Tascii -man python.man | less
#. type: SH
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"python - an interpreted, interactive, object-oriented programming language"
msgstr ""

#. type: SH
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"B<python> [ B<-B> ] [ B<-b> ] [ B<-d> ] [ B<-E> ] [ B<-h> ] [ B<-i> ] [ B<-"
"I> ]"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid ""
"       [\n"
"B<-m>\n"
"I<module-name>\n"
"]\n"
"[\n"
"B<-q>\n"
"]\n"
"[\n"
"B<-O>\n"
"]\n"
"[\n"
"B<-OO>\n"
"]\n"
"[\n"
"B<-s>\n"
"]\n"
"[\n"
"B<-S>\n"
"]\n"
"[\n"
"B<-u>\n"
"]\n"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid ""
"       [\n"
"B<-v>\n"
"]\n"
"[\n"
"B<-V>\n"
"]\n"
"[\n"
"B<-W>\n"
"I<argument>\n"
"]\n"
"[\n"
"B<-x>\n"
"]\n"
"[\n"
"B<-X>\n"
"I<option>\n"
"]\n"
"[\n"
"B<-?>\n"
"]\n"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid ""
"       [\n"
"B<--check-hash-based-pycs>\n"
"I<default>\n"
"|\n"
"I<always>\n"
"|\n"
"I<never>\n"
"]\n"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid ""
"       [\n"
"B<-c>\n"
"I<command>\n"
"|\n"
"I<script>\n"
"|\n"
"-\n"
"]\n"
"[\n"
"I<arguments>\n"
"]\n"
msgstr ""

#. type: SH
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"Python is an interpreted, interactive, object-oriented programming language "
"that combines remarkable power with very clear syntax.  For an introduction "
"to programming in Python, see the Python Tutorial.  The Python Library "
"Reference documents built-in and standard types, constants, functions and "
"modules.  Finally, the Python Reference Manual describes the syntax and "
"semantics of the core language in (perhaps too) much detail.  (These "
"documents may be located via the B<INTERNET RESOURCES> below; they may be "
"installed on your system as well.)"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"Python's basic power can be extended with your own modules written in C or C+"
"+.  On most systems such modules may be dynamically loaded.  Python is also "
"adaptable as an extension language for existing applications.  See the "
"internal documentation for hints."
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"Documentation for installed Python modules and packages can be viewed by "
"running the B<pydoc> program."
msgstr ""

#. type: SH
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "COMMAND LINE OPTIONS"
msgstr ""

#. type: TP
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<-B>"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid "Don't write I<.pyc> files on import. See also PYTHONDONTWRITEBYTECODE."
msgstr ""

#. type: TP
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<-b>"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"Issue warnings about str(bytes_instance), str(bytearray_instance)  and "
"comparing bytes/bytearray with str. (-bb: issue errors)"
msgstr ""

#. type: TP
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<-c >I<command>"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"Specify the command to execute (see next section).  This terminates the "
"option list (following options are passed as arguments to the command)."
msgstr ""

#. type: TP
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<--check-hash-based-pycs >I<mode>"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"Configure how Python evaluates the up-to-dateness of hash-based .pyc files."
msgstr ""

#. type: TP
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<-d>"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"Turn on parser debugging output (for expert only, depending on compilation "
"options)."
msgstr ""

#. type: TP
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<-E>"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"Ignore environment variables like PYTHONPATH and PYTHONHOME that modify the "
"behavior of the interpreter."
msgstr ""

#. type: TP
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<-h ,  -? ,  --help>"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid "Prints the usage for the interpreter executable and exits."
msgstr ""

#. type: TP
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<-i>"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"When a script is passed as first argument or the B<-c> option is used, enter "
"interactive mode after executing the script or the command.  It does not "
"read the $PYTHONSTARTUP file.  This can be useful to inspect global "
"variables or a stack trace when a script raises an exception."
msgstr ""

#. type: TP
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<-I>"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"Run Python in isolated mode. This also implies B<-E> and B<-s>. In isolated "
"mode sys.path contains neither the script's directory nor the user's site-"
"packages directory. All PYTHON* environment variables are ignored, too.  "
"Further restrictions may be imposed to prevent the user from injecting "
"malicious code."
msgstr ""

#. type: TP
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<-m >I<module-name>"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"Searches I<sys.path> for the named module and runs the corresponding I<.py> "
"file as a script. This terminates the option list (following options are "
"passed as arguments to the module)."
msgstr ""

#. type: TP
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<-O>"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"Remove assert statements and any code conditional on the value of __debug__; "
"augment the filename for compiled (bytecode) files by adding .opt-1 before "
"the .pyc extension."
msgstr ""

#. type: TP
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<-OO>"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"Do B<-O> and also discard docstrings; change the filename for compiled "
"(bytecode) files by adding .opt-2 before the .pyc extension."
msgstr ""

#. type: TP
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<-q>"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"Do not print the version and copyright messages. These messages are also "
"suppressed in non-interactive mode."
msgstr ""

#. type: TP
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<-s>"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid "Don't add user site directory to sys.path."
msgstr ""

#. type: TP
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<-S>"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"Disable the import of the module I<site> and the site-dependent "
"manipulations of I<sys.path> that it entails.  Also disable these "
"manipulations if I<site> is explicitly imported later."
msgstr ""

#. type: TP
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<-u>"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"Force the stdout and stderr streams to be unbuffered.  This option has no "
"effect on the stdin stream."
msgstr ""

#. type: TP
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<-v>"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"Print a message each time a module is initialized, showing the place "
"(filename or built-in module) from which it is loaded.  When given twice, "
"print a message for each file that is checked for when searching for a "
"module.  Also provides information on module cleanup at exit."
msgstr ""

#. type: TP
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<-V ,  --version>"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"Prints the Python version number of the executable and exits.  When given "
"twice, print more information about the build."
msgstr ""

#. type: TP
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<-W >I<argument>"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"Warning control. Python's warning machinery by default prints warning "
"messages to I<sys.stderr>."
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"The simplest settings apply a particular action unconditionally to all "
"warnings emitted by a process (even those that are otherwise ignored by "
"default):"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid ""
"  -Wdefault  # Warn once per call location\n"
"  -Werror    # Convert to exceptions\n"
"  -Walways   # Warn every time\n"
"  -Wmodule   # Warn once per calling module\n"
"  -Wonce     # Warn once per Python process\n"
"  -Wignore   # Never warn\n"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"The action names can be abbreviated as desired and the interpreter will "
"resolve them to the appropriate action name. For example, B<-Wi> is the same "
"as B<-Wignore .>"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid "The full form of argument is: I<action:message:category:module:lineno>"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"Empty fields match all values; trailing empty fields may be omitted. For "
"example B<-W ignore::DeprecationWarning> ignores all DeprecationWarning "
"warnings."
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"The I<action> field is as explained above but only applies to warnings that "
"match the remaining fields."
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"The I<message> field must match the whole printed warning message; this "
"match is case-insensitive."
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"The I<category> field matches the warning category (ex: "
"\"DeprecationWarning\"). This must be a class name; the match test whether "
"the actual warning category of the message is a subclass of the specified "
"warning category."
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"The I<module> field matches the (fully-qualified) module name; this match is "
"case-sensitive."
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"The I<lineno> field matches the line number, where zero matches all line "
"numbers and is thus equivalent to an omitted line number."
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"Multiple B<-W> options can be given; when a warning matches more than one "
"option, the action for the last matching option is performed. Invalid B<-W> "
"options are ignored (though, a warning message is printed about invalid "
"options when the first warning is issued)."
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"Warnings can also be controlled using the B<PYTHONWARNINGS> environment "
"variable and from within a Python program using the warnings module.  For "
"example, the warnings.filterwarnings() function can be used to use a regular "
"expression on the warning message."
msgstr ""

#. type: TP
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<-X >I<option>"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"Set implementation specific option. The following options are available:"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "    -X faulthandler: enable faulthandler\n"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid ""
"    -X showrefcount: output the total reference count and number of used\n"
"        memory blocks when the program finishes or after each statement in the\n"
"        interactive interpreter. This only works on debug builds\n"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid ""
"    -X tracemalloc: start tracing Python memory allocations using the\n"
"        tracemalloc module. By default, only the most recent frame is stored in a\n"
"        traceback of a trace. Use -X tracemalloc=NFRAME to start tracing with a\n"
"        traceback limit of NFRAME frames\n"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid ""
"    -X importtime: show how long each import takes. It shows module name,\n"
"        cumulative time (including nested imports) and self time (excluding\n"
"        nested imports). Note that its output may be broken in multi-threaded\n"
"        application. Typical usage is python3 -X importtime -c 'import asyncio'\n"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid ""
"    -X dev: enable CPython's \"development mode\", introducing additional runtime\n"
"        checks which are too expensive to be enabled by default. It will not be\n"
"        more verbose than the default if the code is correct: new warnings are\n"
"        only emitted when an issue is detected. Effect of the developer mode:\n"
"           * Add default warning filter, as -W default\n"
"           * Install debug hooks on memory allocators: see the PyMem_SetupDebugHooks()\n"
"             C function\n"
"           * Enable the faulthandler module to dump the Python traceback on a crash\n"
"           * Enable asyncio debug mode\n"
"           * Set the dev_mode attribute of sys.flags to True\n"
"           * io.IOBase destructor logs close() exceptions\n"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid ""
"    -X utf8: enable UTF-8 mode for operating system interfaces, overriding the default\n"
"        locale-aware mode. -X utf8=0 explicitly disables UTF-8 mode (even when it would\n"
"        otherwise activate automatically). See PYTHONUTF8 for more details\n"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid ""
"    -X pycache_prefix=PATH: enable writing .pyc files to a parallel tree rooted at the\n"
"        given directory instead of to the code tree.\n"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "    -X warn_default_encoding: enable opt-in EncodingWarning for 'encoding=None'\n"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid ""
"    -X int_max_str_digits=number: limit the size of intE<lt>-E<gt>str conversions.\n"
"       This helps avoid denial of service attacks when parsing untrusted data.\n"
"       The default is sys.int_info.default_max_str_digits.  0 disables.\n"
msgstr ""

#. type: TP
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<-x>"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"Skip the first line of the source.  This is intended for a DOS specific hack "
"only.  Warning: the line numbers in error messages will be off by one!"
msgstr ""

#. type: SH
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "INTERPRETER INTERFACE"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"The interpreter interface resembles that of the UNIX shell: when called with "
"standard input connected to a tty device, it prompts for commands and "
"executes them until an EOF is read; when called with a file name argument or "
"with a file as standard input, it reads and executes a I<script> from that "
"file; when called with B<-c> I<command>, it executes the Python statement(s) "
"given as I<command>.  Here I<command> may contain multiple statements "
"separated by newlines.  Leading whitespace is significant in Python "
"statements! In non-interactive mode, the entire input is parsed before it is "
"executed."
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"If available, the script name and additional arguments thereafter are passed "
"to the script in the Python variable I<sys.argv>, which is a list of strings "
"(you must first I<import sys> to be able to access it).  If no script name "
"is given, I<sys.argv[0]> is an empty string; if B<-c> is used, "
"I<sys.argv[0]> contains the string I<'-c'.> Note that options interpreted by "
"the Python interpreter itself are not placed in I<sys.argv>."
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"In interactive mode, the primary prompt is `E<gt>E<gt>E<gt>'; the second "
"prompt (which appears when a command is not complete) is `...'.  The prompts "
"can be changed by assignment to I<sys.ps1> or I<sys.ps2>.  The interpreter "
"quits when it reads an EOF at a prompt.  When an unhandled exception occurs, "
"a stack trace is printed and control returns to the primary prompt; in non-"
"interactive mode, the interpreter exits after printing the stack trace.  The "
"interrupt signal raises the I<Keyboard\\%Interrupt> exception; other UNIX "
"signals are not caught (except that SIGPIPE is sometimes ignored, in favor "
"of the I<IOError> exception).  Error messages are written to stderr."
msgstr ""

#. type: SH
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "FILES AND DIRECTORIES"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"These are subject to difference depending on local installation conventions; "
"${prefix} and ${exec_prefix} are installation-dependent and should be "
"interpreted as for GNU software; they may be the same.  The default for both "
"is I</usr/local>."
msgstr ""

#. type: IP
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "I<${exec_prefix}/bin/python>"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid "Recommended location of the interpreter."
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid "I<${prefix}/lib/pythonE<lt>versionE<gt>>"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid "I<${exec_prefix}/lib/pythonE<lt>versionE<gt>>"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"Recommended locations of the directories containing the standard modules."
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid "I<${prefix}/include/pythonE<lt>versionE<gt>>"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid "I<${exec_prefix}/include/pythonE<lt>versionE<gt>>"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"Recommended locations of the directories containing the include files needed "
"for developing Python extensions and embedding the interpreter."
msgstr ""

#. type: SH
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "ENVIRONMENT VARIABLES"
msgstr ""

#. type: IP
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "PYTHONHOME"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"Change the location of the standard Python libraries.  By default, the "
"libraries are searched in ${prefix}/lib/pythonE<lt>versionE<gt> and $"
"{exec_prefix}/lib/pythonE<lt>versionE<gt>, where ${prefix} and $"
"{exec_prefix} are installation-dependent directories, both defaulting to I</"
"usr/local>.  When $PYTHONHOME is set to a single directory, its value "
"replaces both ${prefix} and ${exec_prefix}.  To specify different values for "
"these, set $PYTHONHOME to ${prefix}:${exec_prefix}."
msgstr ""

#. type: IP
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "PYTHONPATH"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"Augments the default search path for module files.  The format is the same "
"as the shell's $PATH: one or more directory pathnames separated by colons.  "
"Non-existent directories are silently ignored.  The default search path is "
"installation dependent, but generally begins with ${prefix}/lib/"
"pythonE<lt>versionE<gt> (see PYTHONHOME above).  The default search path is "
"always appended to $PYTHONPATH.  If a script argument is given, the "
"directory containing the script is inserted in the path in front of "
"$PYTHONPATH.  The search path can be manipulated from within a Python "
"program as the variable I<sys.path>."
msgstr ""

#. type: IP
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "PYTHONPLATLIBDIR"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid "Override sys.platlibdir."
msgstr ""

#. type: IP
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "PYTHONSTARTUP"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"If this is the name of a readable file, the Python commands in that file are "
"executed before the first prompt is displayed in interactive mode.  The file "
"is executed in the same name space where interactive commands are executed "
"so that objects defined or imported in it can be used without qualification "
"in the interactive session.  You can also change the prompts I<sys.ps1> and "
"I<sys.ps2> in this file."
msgstr ""

#. type: IP
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "PYTHONOPTIMIZE"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"If this is set to a non-empty string it is equivalent to specifying the B<-"
"O> option. If set to an integer, it is equivalent to specifying B<-O> "
"multiple times."
msgstr ""

#. type: IP
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "PYTHONDEBUG"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"If this is set to a non-empty string it is equivalent to specifying the B<-"
"d> option. If set to an integer, it is equivalent to specifying B<-d> "
"multiple times."
msgstr ""

#. type: IP
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "PYTHONDONTWRITEBYTECODE"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"If this is set to a non-empty string it is equivalent to specifying the B<-"
"B> option (don't try to write I<.pyc> files)."
msgstr ""

#. type: IP
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "PYTHONINSPECT"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"If this is set to a non-empty string it is equivalent to specifying the B<-"
"i> option."
msgstr ""

#. type: IP
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "PYTHONIOENCODING"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid ""
"If this is set before running the interpreter, it overrides the encoding used\n"
"for stdin/stdout/stderr, in the syntax\n"
"I<encodingname>B<:>I<errorhandler>\n"
"The\n"
"I<errorhandler>\n"
"part is optional and has the same meaning as in str.encode. For stderr, the\n"
"I<errorhandler>\n"
" part is ignored; the handler will always be \\'backslashreplace\\'.\n"
msgstr ""

#. type: IP
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "PYTHONNOUSERSITE"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"If this is set to a non-empty string it is equivalent to specifying the B<-"
"s> option (Don't add the user site directory to sys.path)."
msgstr ""

#. type: IP
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "PYTHONUNBUFFERED"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"If this is set to a non-empty string it is equivalent to specifying the B<-"
"u> option."
msgstr ""

#. type: IP
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "PYTHONVERBOSE"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"If this is set to a non-empty string it is equivalent to specifying the B<-"
"v> option. If set to an integer, it is equivalent to specifying B<-v> "
"multiple times."
msgstr ""

#. type: IP
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "PYTHONWARNINGS"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"If this is set to a comma-separated string it is equivalent to specifying "
"the B<-W> option for each separate value."
msgstr ""

#. type: IP
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "PYTHONHASHSEED"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"If this variable is set to \"random\", a random value is used to seed the "
"hashes of str and bytes objects."
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"If PYTHONHASHSEED is set to an integer value, it is used as a fixed seed for "
"generating the hash() of the types covered by the hash randomization.  Its "
"purpose is to allow repeatable hashing, such as for selftests for the "
"interpreter itself, or to allow a cluster of python processes to share hash "
"values."
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"The integer must be a decimal number in the range [0,4294967295].  "
"Specifying the value 0 will disable hash randomization."
msgstr ""

#. type: IP
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "PYTHONINTMAXSTRDIGITS"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"Limit the maximum digit characters in an int value when converting from a "
"string and when converting an int back to a str.  A value of 0 disables the "
"limit.  Conversions to or from bases 2, 4, 8, 16, and 32 are never limited."
msgstr ""

#. type: IP
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "PYTHONMALLOC"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"Set the Python memory allocators and/or install debug hooks. The available "
"memory allocators are I<malloc> and I<pymalloc>.  The available debug hooks "
"are I<debug>, I<malloc_debug>, and I<pymalloc_debug>."
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"When Python is compiled in debug mode, the default is I<pymalloc_debug> and "
"the debug hooks are automatically used. Otherwise, the default is "
"I<pymalloc>."
msgstr ""

#. type: IP
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "PYTHONMALLOCSTATS"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"If set to a non-empty string, Python will print statistics of the pymalloc "
"memory allocator every time a new pymalloc object arena is created, and on "
"shutdown."
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"This variable is ignored if the $B<PYTHONMALLOC> environment variable is "
"used to force the B<malloc>(3)  allocator of the C library, or if Python is "
"configured without pymalloc support."
msgstr ""

#. type: IP
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "PYTHONASYNCIODEBUG"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"If this environment variable is set to a non-empty string, enable the debug "
"mode of the asyncio module."
msgstr ""

#. type: IP
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "PYTHONTRACEMALLOC"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"If this environment variable is set to a non-empty string, start tracing "
"Python memory allocations using the tracemalloc module."
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"The value of the variable is the maximum number of frames stored in a "
"traceback of a trace. For example, I<PYTHONTRACEMALLOC=1> stores only the "
"most recent frame."
msgstr ""

#. type: IP
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "PYTHONFAULTHANDLER"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"If this environment variable is set to a non-empty string, "
"I<faulthandler.enable()> is called at startup: install a handler for "
"SIGSEGV, SIGFPE, SIGABRT, SIGBUS and SIGILL signals to dump the Python "
"traceback."
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid "This is equivalent to the B<-X faulthandler> option."
msgstr ""

#. type: IP
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "PYTHONEXECUTABLE"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"If this environment variable is set, I<sys.argv[0]> will be set to its value "
"instead of the value got through the C runtime. Only works on Mac OS X."
msgstr ""

#. type: IP
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "PYTHONUSERBASE"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"Defines the user base directory, which is used to compute the path of the "
"user I<site-packages> directory and Distutils installation paths for "
"I<python setup.py install --user>."
msgstr ""

#. type: IP
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "PYTHONPROFILEIMPORTTIME"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"If this environment variable is set to a non-empty string, Python will show "
"how long each import takes. This is exactly equivalent to setting B<-X "
"importtime> on the command line."
msgstr ""

#. type: IP
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "PYTHONBREAKPOINT"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"If this environment variable is set to 0, it disables the default debugger. "
"It can be set to the callable of your debugger of choice."
msgstr ""

#. type: SS
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Debug-mode variables"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"Setting these variables only has an effect in a debug build of Python, that "
"is, if Python was configured with the B<--with-pydebug> build option."
msgstr ""

#. type: IP
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "PYTHONTHREADDEBUG"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"If this environment variable is set, Python will print threading debug "
"info.  The feature is deprecated in Python 3.10 and will be removed in "
"Python 3.12."
msgstr ""

#. type: IP
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "PYTHONDUMPREFS"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"If this environment variable is set, Python will dump objects and reference "
"counts still alive after shutting down the interpreter."
msgstr ""

#. type: SH
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid "The Python Software Foundation: https://www.python.org/psf/"
msgstr ""

#. type: SH
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "INTERNET RESOURCES"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid "Main website: https://www.python.org/"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid "Documentation: https://docs.python.org/"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid "Developer resources: https://devguide.python.org/"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid "Downloads: https://www.python.org/downloads/"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid "Module repository: https://pypi.org/"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid "Newsgroups: comp.lang.python, comp.lang.python.announce"
msgstr ""

#. type: SH
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "LICENSING"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"Python is distributed under an Open Source license.  See the file "
"\"LICENSE\" in the Python source distribution for information on terms & "
"conditions for accessing and otherwise using Python and for a DISCLAIMER OF "
"ALL WARRANTIES."
msgstr ""
