# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-28 16:35+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "GRUB-SET-BOOTFLAG"
msgstr ""

#. type: TH
#: fedora-42 fedora-rawhide
#, no-wrap
msgid "February 2025"
msgstr ""

#. type: TH
#: fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "GRUB 2.12"
msgstr ""

#. type: TH
#: fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "User Commands"
msgstr ""

#. type: SH
#: fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron
msgid "grub-set-bootflag - set a bootflag in the GRUB environment block"
msgstr ""

#. type: SH
#: fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron
msgid ""
"B<'grub-set-bootflag> I<\\,E<lt>bootflagE<gt>', where E<lt>bootflagE<gt> is "
"one of:\\/>"
msgstr ""

#. type: SH
#: fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron
msgid "boot_success menu_show_once"
msgstr ""

#. type: SH
#: fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron
msgid ""
"The full documentation for B<grub-set-bootflag> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-set-bootflag> programs are properly "
"installed at your site, the command"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron
msgid "B<info grub-set-bootflag>"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron
msgid "should give you access to the complete manual."
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "October 2024"
msgstr ""
