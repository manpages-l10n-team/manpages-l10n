# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-16 05:56+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "PS2ASCII"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "18 Sept 2024"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "10.04.0"
msgstr ""

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Ghostscript Tools"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "ps2ascii - Ghostscript translator from PostScript or PDF to text"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<ps2ascii> [ I<input.ps> [ I<output.txt> ] ]"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<ps2ascii> I<input.pdf> [ I<output.txt> ]"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<ps2ascii> uses B<gs>(1) to extract text from B<PostScript>(tm) or Adobe "
"B<Portable Document Format> (PDF)  files. If no files are specified on the "
"command line, B<gs> reads from standard input.  If no output file is "
"specified, the ASCII text is written to standard output."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The old B<ps2ascii.ps> program was deprecated and removed some years ago, "
"the scripts now use the B<txtwrite> device to extract text from the input. "
"This does a generally better job than the old PostScript program and can "
"extract Unicode not just ASCII. However it no longer supports the B<COMPLEX> "
"feature."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Further documentation on the txtwrite device can be found at https://"
"ghostscript.readthedocs.io/en/latest/Devices.html#text-output"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "VERSION"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "This document was last revised for Ghostscript version 10.04.0."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Artifex Software, Inc. are the primary maintainers of Ghostscript.  David M. "
"Jones E<lt>dmjones@theory.lcs.mit.eduE<gt> made substantial improvements to "
"B<ps2ascii>."
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "21 September 2022"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "10.00.0"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "ps2ascii - Ghostscript translator from PostScript or PDF to ASCII"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<ps2ascii> uses B<gs>(1) to extract ASCII text from B<PostScript>(tm) or "
"Adobe B<Portable Document Format> (PDF)  files. If no files are specified on "
"the command line, B<gs> reads from standard input; but PDF input must come "
"from an explicitly-named file, not standard input.  If no output file is "
"specified, the ASCII text is written to standard output."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<ps2ascii> doesn't look at font encoding, and isn't very good at dealing "
"with kerning, so for PostScript (but not currently PDF), you might consider "
"B<pstotext> (see below)."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "FILES"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Run \"B<gs -h>\" to find the location of Ghostscript documentation on your "
"system, from which you can get more details."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"pstotext(1), http://www.research.digital.com/SRC/virtualpaper/pstotext.html"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "This document was last revised for Ghostscript version 10.00.0."
msgstr ""
