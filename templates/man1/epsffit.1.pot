# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-28 16:32+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-unstable
#, no-wrap
msgid "PSUTILS"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "2025-01-09"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "psutils 3.3.9"
msgstr ""

#. type: TH
#: archlinux debian-unstable
#, no-wrap
msgid "User Command"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable
msgid "psutils"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable
msgid "B<psutils> [OPTION...] LLX LLY URX URY [INFILE [OUTFILE]]"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Fit an Encapsulated PostScript file to a given bounding box."
msgstr ""

#. type: TP
#: archlinux debian-unstable
#, no-wrap
msgid "B<LLX>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable
msgid "x coordinate of lower left corner of the box"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable
msgid "y coordinate of lower left corner of the box"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable
msgid "x coordinate of upper right corner of the box"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable
msgid "y coordinate of upper right corner of the box"
msgstr ""

#. type: TP
#: archlinux debian-unstable
#, no-wrap
msgid "B<INFILE>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable
msgid "`-' or no INFILE argument means standard input"
msgstr ""

#. type: TP
#: archlinux debian-unstable
#, no-wrap
msgid "B<OUTFILE>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable
msgid "`-' or no OUTFILE argument means standard output"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: TP
#: archlinux debian-unstable
#, no-wrap
msgid "B<-c>, B<--center>, B<--centre>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "center the image in the given bounding box"
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-r>, B<--rotate>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "rotate the image by 90 degrees counter-clockwise"
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-a>, B<--aspect>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "adjust the aspect ratio to fit the bounding box"
msgstr ""

#. type: TP
#: archlinux debian-unstable
#, no-wrap
msgid "B<-m>, B<--maximize>, B<--maximise>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "rotate the image to fill more of the page if possible"
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-s>, B<--showpage>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable
msgid "append a /showpage to the file to force printing"
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-v>, B<--version>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable
msgid "show program's version number and exit"
msgstr ""

#. type: TP
#: archlinux debian-unstable
#, no-wrap
msgid "B<-q>, B<--quiet>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable
msgid "don't show progress"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable
#, no-wrap
msgid "Reuben Thomas E<lt>rrt@sc3d.orgE<gt>\n"
msgstr ""

#. type: TH
#: debian-bookworm fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "EPSFFIT"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "PSUtils Release 1 Patchlevel 17"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "epsffit - fit encapsulated PostScript file (EPSF) into constrained size"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<epsffit> [ B<-c> ] [ B<-r> ] [ B<-a> ] [ B<-m> ] [ B<-s> ] I<llx lly urx "
"ury> [ B<infile> [ B<outfile> ] ]"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"I<Epsffit> fits an EPSF file (encapsulated PostScript) to a given bounding "
"box.  The coordinates of the box are given by B<(llx,lly)> for the lower "
"left, and B<(urx,ury)> for the upper right, in PostScript units (points)."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"If no input or output files are specified, I<epsffit> read from the standard "
"input and writes to the standard output."
msgstr ""

#. type: IP
#: debian-bookworm
#, no-wrap
msgid "B<-c>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Center the image in the given bounding box."
msgstr ""

#. type: IP
#: debian-bookworm
#, no-wrap
msgid "B<-r>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Rotate the image by 90 degrees counter-clockwise."
msgstr ""

#. type: IP
#: debian-bookworm
#, no-wrap
msgid "B<-a>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Adjust the aspect ratio to fit the bounding box. The default is to preserve "
"the aspect ratio."
msgstr ""

#. type: IP
#: debian-bookworm
#, no-wrap
msgid "B<-m>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Rotates the image to maximise the size if it would fit the specified "
"bounding box better."
msgstr ""

#. type: IP
#: debian-bookworm
#, no-wrap
msgid "B<-s>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Add a I<showpage> at the end of the file to force the image to print."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Copyright (C) Angus J. C. Duggan 1991-1995"
msgstr ""

#. type: SH
#: debian-bookworm fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"psbook(1), psselect(1), pstops(1), epsffit(1), psnup(1), psresize(1), "
"psmerge(1), fixscribeps(1), getafm(1), fixdlsrps(1), fixfmps(1), "
"fixpsditps(1), fixpspps(1), fixtpps(1), fixwfwps(1), fixwpps(1), fixwwps(1), "
"extractres(1), includeres(1), showchar(1)"
msgstr ""

#. type: SH
#: debian-bookworm fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "TRADEMARKS"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<PostScript> is a trademark of Adobe Systems Incorporated."
msgstr ""

#. type: TH
#: debian-unstable
#, no-wrap
msgid "2025-02-15"
msgstr ""

#. type: TH
#: debian-unstable
#, no-wrap
msgid "psutils 3.3.8"
msgstr ""

#. type: TH
#: fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "February 2023"
msgstr ""

#. type: TH
#: fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "epsffit 2.10"
msgstr ""

#. type: TH
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "epsffit - fit an Encapsulated PostScript file to a given bounding box"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"B<epsffit> [I<\\,OPTION\\/>...] I<\\,LLX LLY URX URY \\/>[I<\\,INFILE \\/"
">[I<\\,OUTFILE\\/>]]"
msgstr ""

#. type: TP
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-c>, B<--center>"
msgstr ""

#. type: TP
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-m>, B<--maximize>"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "append a I<\\,/showpage\\/> to the file to force printing"
msgstr ""

#. type: TP
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "display this help and exit"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "display version information and exit"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"(LLX, LLY) are the coordinates of the lower left corner of the box, and "
"(URX, URY) the upper right."
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"If OUTFILE is not specified, writes to standard output.  If INFILE is not "
"specified, reads from standard input."
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "See B<psutils>(1)  for the available units."
msgstr ""

#. type: SS
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "Exit status:"
msgstr ""

#. type: TP
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "0"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "if OK,"
msgstr ""

#. type: TP
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "1"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"if arguments or options are incorrect, or there is some other problem "
"starting up,"
msgstr ""

#. type: TP
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"if there is some problem during processing, typically an error reading or "
"writing an input or output file."
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Written by Angus J. C. Duggan and Reuben Thomas."
msgstr ""

#. type: SH
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron
msgid ""
"Copyright \\(co Reuben Thomas 2016-2022.  Released under the GPL version 3, "
"or (at your option) any later version."
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<psutils>(1), B<paper>(1)"
msgstr ""

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "December 2021"
msgstr ""

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "epsffit 2.08"
msgstr ""

#. type: TP
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr ""

#. type: Plain text
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Copyright \\(co Reuben Thomas 2016.  Released under the GPL version 3, or "
"(at your option) any later version."
msgstr ""
