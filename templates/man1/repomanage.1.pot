# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-28 16:46+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "REPOMANAGE"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Jan 22, 2023"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "4.3.1"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "dnf-plugins-core"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "repomanage - redirecting to DNF repomanage Plugin"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "Manage a repository or a simple directory of rpm packages."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid ""
"B<dnf repomanage [E<lt>optional-optionsE<gt>] [E<lt>optionsE<gt>] "
"E<lt>pathE<gt>>"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"I<repomanage> prints newest or older packages in a repository specified by "
"E<lt>pathE<gt> for easy piping to xargs or similar programs. In case "
"E<lt>pathE<gt> doesn’t contain a valid repodata, it is searched for rpm "
"packages which are then used instead.  If the repodata are present, "
"I<repomanage> uses them as the source of truth, it doesn’t verify that they "
"match the present rpm packages. In fact, I<repomanage> can run with just the "
"repodata, no rpm packages are needed."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid ""
"In order to work correctly with modular packages, E<lt>pathE<gt> has to "
"contain repodata with modular metadata. If modular content is present, "
"I<repomanage> prints packages from newest or older stream versions in "
"addition to newest or older non-modular packages."
msgstr ""

#. type: SS
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Options"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid ""
"All general DNF options are accepted, see I<Options> in B<dnf(8)> for "
"details."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid ""
"The following options set what packages are displayed. These options are "
"mutually exclusive, i.e. only one can be specified. If no option is "
"specified, the newest packages are shown."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "B<--old>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid ""
"Show older packages (for a package or a stream show all versions except the "
"newest one)."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "B<--oldonly>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Show older packages (same as –old, but exclude the newest packages even when "
"it’s included in the older stream versions)."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "B<--new>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "Show newest packages."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "The following options control how packages are displayed in the output:"
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "B<-s, --space>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "Print resulting set separated by space instead of newline."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "B<-k E<lt>keep-numberE<gt>, --keep E<lt>keep-numberE<gt>>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "Limit the resulting set to newest B<E<lt>keep-numberE<gt>> packages."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "EXAMPLES"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "Display newest packages in current repository (directory):"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"^\".ft C$\n"
"dnf repomanage --new .\n"
"^\".ft P$\n"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "Display 2 newest versions of each package in “home” directory:"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"^\".ft C$\n"
"dnf repomanage --new --keep 2 ~/\n"
"^\".ft P$\n"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid ""
"Display oldest packages separated by space in current repository (directory):"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"^\".ft C$\n"
"dnf repomanage --old --space .\n"
"^\".ft P$\n"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "AUTHOR"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "See AUTHORS in your Core DNF Plugins distribution"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "COPYRIGHT"
msgstr ""

#.  Generated by docutils manpage writer.
#. type: Plain text
#: debian-bookworm
msgid "2023, Red Hat, Licensed under GPLv2+"
msgstr ""

#. type: TH
#: debian-unstable
#, no-wrap
msgid "Feb 12, 2025"
msgstr ""

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "4.10.0"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "dnf repomanage --new .\n"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "dnf repomanage --new --keep 2 ~/\n"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "dnf repomanage --old --space .\n"
msgstr ""

#.  Generated by docutils manpage writer.
#. type: Plain text
#: debian-unstable fedora-42 fedora-rawhide
msgid "2014, Red Hat, Licensed under GPLv2+"
msgstr ""

#. type: TH
#: fedora-42 fedora-rawhide
#, no-wrap
msgid "Jan 16, 2025"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide
msgid ""
"I<repomanage> prints newest or older packages in a repository specified by "
"E<lt>pathE<gt> for easy piping to xargs or similar programs. In case "
"E<lt>pathE<gt> doesn\\(aqt contain a valid repodata, it is searched for rpm "
"packages which are then used instead.  If the repodata are present, "
"I<repomanage> uses them as the source of truth, it doesn\\(aqt verify that "
"they match the present rpm packages. In fact, I<repomanage> can run with "
"just the repodata, no rpm packages are needed."
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide
msgid ""
"Show older packages (same as --old, but exclude the newest packages even "
"when it\\(aqs included in the older stream versions)."
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide
msgid "Display 2 newest versions of each package in \"home\" directory:"
msgstr ""
