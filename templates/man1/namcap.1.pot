# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-02-09 17:04+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux
#, no-wrap
msgid "namcap"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "Jan 14, 2024"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "namcap 3.5.2"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "User Commands"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux
msgid "namcap - package analysis utility"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<namcap [options] E<lt>package|PKGBUILDE<gt> [package|PKGBUILD] ...>"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"B<namcap> is a I<package analysis> utility that looks for problems with Arch "
"Linux packages or their PKGBUILD files.  It can apply rules to the file "
"list, the files themselves, or individual PKGBUILD files."
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Rules return lists of messages.  Each message can be one of three types: "
"error, warning, or information (think of them as notes or comments).  Errors "
"(designated by 'E:') are things that namcap is very sure are wrong and need "
"to be fixed.  Warnings (designated by 'W:') are things that namcap thinks "
"should be changed but if you know what you're doing then you can leave "
"them.  Information (designated 'I:') are only shown when you use the info "
"argument.  Information messages give information that might be helpful but "
"isn't anything that needs changing."
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-e> RULELIST, B<--exclude=>RULELIST"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Do not run RULELIST rules on the package"
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-i, --info>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "display information messages"
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-L, --list>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "return a list of valid rules and their descriptions"
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-m, --machine-readable>"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"displays easily parseable namcap tags instead of the normal human readable "
"description; for example using non-fhs-man-page instead of \"Non-FHS man "
"page (%s) found. Use /usr/share/man instead\". A full list of namcap tags "
"along with their human readable descriptions can be found at /usr/share/"
"namcap/tags."
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-r> RULELIST, B<--rules=>RULELIST"
msgstr ""

#. type: Plain text
#: archlinux
msgid "only apply RULELIST rules to the package"
msgstr ""

#. type: Plain text
#: archlinux
msgid "RULELIST is a comma-separated list of rule names"
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-v, --version>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "print version and exit"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "RULES"
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<arrays>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Verifies that array variables are actually arrays and not strings"
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<badbackups>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Verifies that backup entries don't begin with /"
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<carch>"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Looks for references to i686 and i586 and tells you to use $CARCH instead"
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<capsnames>"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Checks a PKGBUILD to verify that the package name has no upper case "
"characters"
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<capsnamespkg>"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Checks a package to verify that the package name has no upper case characters"
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<checksums>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Makes sure that a PKGBUILD includes valid checksums"
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<depends>"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"This module runs ldd on all executables, gets the link-level dependencies, "
"finds the smallest subset of dependencies that cover the link-level "
"dependencies, and compares that list to the depends of the package.  It "
"returns messages in three cases: dependency detected and not included, "
"dependency included but already satisfied, and dependency included and not "
"needed.  These suggestions are just guidelines and all package builders "
"should take this into account (i.e. you're smarter than namcap is)"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Some cases where namcap fails are dlopen() and obscure links.  dlopen()'d "
"libraries don't show up because they are loaded at run time: in the case of "
"a program that loads plugins.  Obscure links are the cases where only a "
"small portion of the package needs something to run; usually, the small "
"portion won't be accessed unless that thing is installed (i.e. a java plugin "
"for mozilla included in the java package)."
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<directoryname>"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Checks the directory names in the package file list for standard directories "
"(i.e. /etc, /usr/bin, /usr/lib).  All files in directories that are non-"
"standard are returned"
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<elffiles>"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Gives an error if it finds ELF (binary) files outside the standard paths (/"
"bin, /usr/bin, etc.)"
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<emptydir>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Looks for directories in a package that don't contain any files"
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<extravars>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Verifies that extra variables start with an underscore"
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<fhsinfopages>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Verifies correct installation of info pages"
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<fhsmanpages>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Verifies correct installation of man pages"
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<fileownership>"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Verifies that all files are owned by root:root and not some other user or "
"group"
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<gnomemime>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Checks for generated GNOME mime files"
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<hicoloricons>"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Checks whether the hicolor icon cache is updated when a package installs "
"files in /usr/share/icons/hicolor"
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<infodirectory>"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Verifies that a package does not contain the info directory file (/usr/share/"
"info/dir)"
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<invalidstartdir>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Any PKGBUILDs that refer to files in $startdir are incorrect"
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<libtool>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Warns if libtool (*.la) files are present"
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<license>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Verifies that the licenses variable has been filled in in a PKGBUILD."
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<licensepkg>"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Verifies that the licenses variable has been filled in in a package. For "
"packages with a custom license it checks whether the license file has been "
"installed in /usr/share/licenses/$pkgname/"
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<perllocal>"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Searches for perllocal.pod.  perllocal.pod is a nasty file that's included "
"during most perl module installations"
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<permissions>"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Checks basic file and and directory permissions.  It returns warnings about "
"world readable bits not being set, world writable bits being set, and world "
"executable bit not being set for directories"
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<rpath>"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Gives an error if a binary has RPATH set to something other than /usr/lib"
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<runpath>"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Gives an error if a binary has RUNPATH set to something other than /usr/"
"lib, /usr/lib32"
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<scrollkeeper>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Verifies that there aren't any scrollkeeper directories"
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<sfurl>"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Warns if a PKGBUILD is downloading from a specific sourceforge mirror "
"instead of the generic downloads.sourceforge.net"
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<symlink>"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Finds out symbolic and hard links which do not point to a valid path in the "
"package"
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<tags>"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"For package maintainers, this module verifies that we have remembered to add "
"a Maintainer and Contributor comment."
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<url>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Verifies that we have the url variable set in the PKGBUILD"
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<urlpkg>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Verifies that we have the url variable set in the package file"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "EXAMPLES"
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<namcap foo.pkg.tar.gz>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "apply all rules to foo.pkg.tar.gz"
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<namcap -r depends,symlink foo.pkg.tar.gz>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "apply the depends and symlink rules to foo.pkg.tar.gz"
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<namcap PKGBUILD>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "apply all PKGBUILD based rules to the file PKGBUILD"
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<namcap --list>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "list all of the available rules"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "COPYRIGHT"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Copyright \\(co 2003-2023 Namcap contributors, see AUTHORS for details."
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"This is free software; see the source for copying conditions.  There is NO "
"warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE."
msgstr ""
