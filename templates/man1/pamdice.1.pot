# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-28 16:42+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Pamdice User Manual"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "01 April 2007"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "netpbm documentation"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "pamdice - slice a Netpbm image into many horizontally and/or vertically"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLE"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"CW<\n"
"    $ pamdice myimage.ppm -outstem=myimage_part -width=10 -height=8\n"
"    $ pamundice myimage_part_%1d_%1a.ppm -across=10 -down=8 E<gt>myimage.ppm>\n"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"CW<    $ pamdice myimage.ppm -outstem=myimage_part -height=12 -voverlap=9>\n"
"\n"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<pamdice>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<-outstem=>I<filenamestem>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "[B<-width=>I<width>]"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "[B<-height=>I<height>]"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "[B<-hoverlap=>I<hoverlap>]"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "[B<-voverlap=>I<voverlap>]"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "[B<-verbose>]"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "[I<filename>]"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"You can use the minimum unique abbreviation of the options.  You can use two "
"hyphens instead of one.  You can separate an option name from its value with "
"white space instead of an equals sign."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "This program is part of B<Netpbm>(1)  \\&."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<pamdice> reads a PAM, PBM, PGM, or PPM image as input and splits it "
"horizontally and/or vertically into equal size pieces and writes them into "
"separate files as the same kind of image.  You can optionally make the "
"pieces overlap."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"See the B<-outstem> option for information on naming of the output files."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<-width> and B<-height> options determine the size of the output pieces."
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"B<pamundice> can rejoin the images.  For finer control, you can also use"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid "B<pnmcat>."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"One use for this is to make pieces that take less computer resources than "
"the whole image to process.  For example, you might have an image so large "
"that an image editor can't read it all into memory or processes it very "
"slowly.  With B<pamdice>, you can split it into smaller pieces, edit one at "
"a time, and then reassemble them."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Another use for this is to print a large image in small printer-sized pieces "
"that you can glue together.  B<ppmglobe> does a similar thing; it lets you "
"glue the pieces together into a sphere."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If you want to cut pieces from an image individually, not in a regular grid, "
"use B<pamcut>."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"In addition to the options common to all programs based on libnetpbm\n"
"(most notably B<-quiet>, see \n"
"E<.UR index.html#commonoptions>\n"
" Common Options\n"
"E<.UE>\n"
"\\&), B<pamdice> recognizes the following\n"
"command line options:\n"
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-outstem=>filenamestem"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This option determines the names of the output files.  Each output file is "
"named I<filenamestem>B<_>I<y>B<_>I<x>B<.>I<type> where I<filenamestem> is "
"the value of the B<-outstem> option, I<x> and y are the horizontal and "
"vertical locations, respectively, in the input image of the output image, "
"zero being the leftmost and top, and I<type> is B<.pbm>, B<.pgm>, B<.ppm>, "
"or B<.pam>, depending on the type of image."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-width=>I<width>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"gives the width in pixels of the output images.  The rightmost pieces are "
"smaller than this if the input image is not a multiple of I<width> pixels "
"wide."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-height=>I<height>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"gives the height in pixels of the output images.  The bottom pieces are "
"smaller than this if the input image is not a multiple of I<height> pixels "
"high."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-hoverlap=>I<hoverlap>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"gives the horizontal overlap in pixels between output images.  Each image in "
"a row will overlap the previous one by I<hoverlap> pixels.  By default, "
"there is no overlap."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "This option was new in Netpbm 10.23 (July 2004)."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-voverlap=>I<voverlap>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"gives the vertical overlap in pixels between output images.  Each row of "
"images will overlap the previous row by I<voverlap> pixels.  By default, "
"there is no overlap."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-verbose>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Print information about the processing to Standard Error."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<pamdice> was new in Netpbm 9.25 (March 2002)."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Before Netpbm 10.29 (August 2005), there was a limit of 100 slices in each "
"direction."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"B<pamundice>(1)  \\&, B<pamcut>(1)  \\&, B<pnmcat>(1)  \\&, B<pgmslice>(1)  "
"\\&, B<ppmglobe>(1)  \\& B<pnm>(1)  \\& B<pam>(1)  \\&"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DOCUMENT SOURCE"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This manual page was generated by the Netpbm tool 'makeman' from HTML "
"source.  The master documentation is at"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<http://netpbm.sourceforge.net/doc/pamdice.html>"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"B<pamundice> can rejoin the images.  For finer control, you can also use "
"B<pamcat>."
msgstr ""

#. type: Plain text
#: debian-unstable fedora-42 fedora-rawhide
msgid ""
"This option determines the names of the output files.  Each output file is "
"named I<filenamestem>B<_>I<y>B<_>I<x>B<.>I<type>, where I<filenamestem> is "
"the value of the B<-outstem> option, I<x> and I<y> are the horizontal and "
"vertical locations, respectively, in the input image of the output image, "
"zero being the leftmost and top, and I<type> is B<.pbm>, B<.pgm>, B<.ppm>, "
"or B<.pam>, depending on the type of image."
msgstr ""

#. type: Plain text
#: debian-unstable fedora-42 fedora-rawhide
msgid ""
"B<pamundice>(1)  \\&, B<pamcut>(1)  \\&, B<pamcat>(1)  \\&, B<pgmslice>(1)  "
"\\&, B<ppmglobe>(1)  \\& B<pnm>(1)  \\& B<pam>(1)  \\&"
msgstr ""

#. type: Plain text
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<pamundice>(1)  \\&, B<pamcut>(1)  \\&, B<pamcat>(1)  \\&, B<pgmslice>(1)  "
"\\&, B<ppmglobe>(1)  \\& B<pnm>(5)  \\& B<pam>(5)  \\&"
msgstr ""
