# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-03-29 09:44+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "LSIRQ"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "User Commands"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "lsirq - utility to display kernel interrupt information"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<lsirq> [options]"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Display kernel interrupt counter information."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The default output is subject to change. So whenever possible, you should "
"avoid using default outputs in your scripts. Always explicitly define "
"expected columns by using B<--output>."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-n>, B<--noheadings>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Don\\(cqt print headings."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-o>, B<--output> I<list>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Specify which output columns to print. Use B<--help> to get a list of all "
"supported columns. The default list of columns may be extended if list is "
"specified in the format I<+list>."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-s>, B<--sort> I<column>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Specify sort criteria by column name. See B<--help> output to get column "
"names."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-J>, B<--json>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Use JSON output format."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-P>, B<--pairs>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Produce output in the form of key=\"value\" pairs. All potentially unsafe "
"characters are hex-escaped (\\(rsxE<lt>codeE<gt>)."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-S>, B<--softirq>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Show softirqs information."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Display help text and exit."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-V>, B<--version>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Print version and exit."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AUTHORS"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "For bug reports, use the issue tracker at"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<lsirq> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""
