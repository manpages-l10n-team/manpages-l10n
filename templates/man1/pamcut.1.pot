# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-28 16:42+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Pamcut User Manual"
msgstr ""

#. type: TH
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "04 October 2019"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "netpbm documentation"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid "pamcut - cut a rectangle out of a PAM, PBM, PGM, or PPM image"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<pamcut>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "[B<-left >I<colnum>]"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "[B<-right >I<colnum>]"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "[B<-top >I<rownum>]"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "[B<-bottom >I<rownum>]"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "[B<-width >I<cols>]"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "[B<-height >I<rows>]"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "[B<-pad>]"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "[B<-cropleft >I<numcols>]"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "[B<-cropright >I<numcols>]"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "[B<-croptop >I<numrows>]"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "[B<-cropbottom >I<numrows>]"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "[B<-verbose>]"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "[I<left> I<top> I<width> I<height>]"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "[I<pnmfile>]"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Minimum unique abbreviations of option are acceptable.  You may use double "
"hyphens instead of single hyphen to denote options.  You may use white space "
"in place of the equals sign to separate an option name from its value."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "This program is part of B<Netpbm>(1)  \\&."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<pamcut> reads a PAM, PBM, PGM, or PPM image as input and extracts the "
"specified rectangle, and produces the same kind of image as output."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"You can specify either the rectangle to cut out and keep or specify the "
"edges to crop off and discard, or a combination."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"To request edges be cropped off, use options B<-cropleft>, B<-cropright>, B<-"
"croptop>, and B<-cropbottom> to indicate how many rows or columns to discard."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For example, B<-cropleft=50 -cropright=200> means to discard the leftmost 50 "
"and rightmost 200 columns."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"To specify the rectangle to keep, use B<-left>, B<-right>, B<-top>, B<-"
"bottom>, B<-width>, B<-height>, and B<-pad> options."
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"For example, B<-left=50 -right=200> means to keep the 150 columns between "
"Columns 50 and 200 inclusive."
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"You can code any mixture of the options.  What you don't specify defaults.  "
"Those defaults are in favor of minimal cutting and in favor of cutting the "
"right and bottom edges off.  It is an error to overspecify, i.e. to specify "
"all three of B<-left>, B<-right>, and B<-width> or B<-top>, B<-bottom>, and "
"B<-height> or B<right> as well as B<-cropright>."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"There is an older way to specify the rectangle to keep: positional "
"arguments.  Arguments were the only way available before July 2000, but you "
"should not use them in new applications.  Options are easier to remember and "
"read, more expressive, and allow you to use defaults."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If you use both options and arguments, the two specifications get mixed in "
"an unspecified way."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"To use arguments, specify all four of the I<left>, I<top>, I<width>, and "
"I<height> arguments.  I<left> and I<top> have the same effect as specifying "
"them as the argument of a B<-left> or B<-top> option, respectively.  "
"I<width> and I<height> have the same effect as specifying them as the "
"argument of a B<-width> or B<-height> option, respectively, where they are "
"positive.  Where they are not positive, they have the same effect as "
"specifying one less than the value as the argument to a B<-right> or B<-"
"bottom> option, respectively.  (E.g. I<width> = 0 makes the cut go all the "
"way to the right edge).  Before July 2000, negative numbers were not allowed "
"for I<width> and I<height>."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Input is from Standard Input if you don't specify the input file I<pnmfile>."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Output is to Standard Output."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<pamcut> works on a multi-image stream.  It cuts each image in the stream "
"independently and produces a multi-image stream output.  Before Netpbm 10.32 "
"(March 2006), it ignored all but the first image in the stream."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If you are splitting a single image into multiple same-size images, "
"B<pamdice> is faster and easier than running B<pamcut> multiple times."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<pamcomp> is also useful for cutting and padding an image to a certain "
"size.  You create a background image of the desired frame dimensions and "
"overlay the subject image on it."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"In addition to the options common to all programs based on libnetpbm\n"
"(most notably B<-quiet>, see \n"
"E<.UR index.html#commonoptions>\n"
" Common Options\n"
"E<.UE>\n"
"\\&), B<pamcut> recognizes the following\n"
"command line options:\n"
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-left=>I<colnum>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The column number of the leftmost column to be in the output.  Columns left "
"of this get cut out.  If a nonnegative number, it refers to columns numbered "
"from 0 at the left, increasing to the right.  If negative, it refers to "
"columns numbered -1 at the right, decreasing to the left."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-right=>I<colnum>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The column number of the rightmost column to be in the output, numbered the "
"same as for B<-left.> Columns to the right of this get cut out."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-top=>I<rownum>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The row number of the topmost row to be in the output.  Rows above this get "
"cut out.  If a nonnegative number it refers to rows numbered from 0 at the "
"top, increasing downward.  If negative, it refers to columns numbered -1 at "
"the bottom, decreasing upward."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-bottom=>I<rownum>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The row number of the bottom-most row to be in the output, numbered the same "
"as for B<-top>.  Rows below this get cut out."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-width=>I<cols>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "The number of columns to be in the output.  Must be positive."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-height=>I<rows>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "The number of rows to be in the output.  Must be positive."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-cropleft>"
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-cropright>"
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-croptop>"
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-cropbottom>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"These options tell how many rows or columns to crop from the left, right, "
"top, or bottom edges, respectively."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "The value must not be negative."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"These option were new in Netpbm 10.85 (December 2018).  Before that, you can "
"achieve the same thing with B<-left>, B<top>, and negative values for B<-"
"right> and B<-bottom>.  Remember to subtract one in the latter case; e.g. "
"the equivalent of B<-cropright=1> is B<-right=-2>."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-pad>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the rectangle you specify is not entirely within the input image, "
"B<pamcut> fails unless you also specify B<-pad>.  In that case, it pads the "
"output with black up to the edges you specify.  You can use this option if "
"you need to have an image of certain dimensions and have an image of "
"arbitrary dimensions."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<pnmpad> also adds borders to an image, but you specify their width "
"directly."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<pamcomp> does a more general form of this padding.  Create a background "
"image of the frame dimensions and overlay the subject image on it.  You can "
"use options to have the subject image in the center of the frame or against "
"any edge and make the padding any color (the padding color is the color of "
"the background image)."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-verbose>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Print information about the processing to Standard Error."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<pnmcrop>(1)  \\&, B<pamdice>(1)  \\&, B<pamcomp>(1)  \\&, B<pnmpad>(1)  "
"\\&, B<pnmcat>(1)  \\&, B<pgmslice>(1)  \\&, B<pnm>(1)  \\&"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<pamcut> was derived from B<pnmcut> in Netpbm 9.20 (May 2001).  It was the "
"first Netpbm program adapted to the new PAM format and programming library."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The predecessor B<pnmcut> was one of the oldest tools in the Netpbm package."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Copyright (C) 1989 by Jef Poskanzer."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DOCUMENT SOURCE"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This manual page was generated by the Netpbm tool 'makeman' from HTML "
"source.  The master documentation is at"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<http://netpbm.sourceforge.net/doc/pamcut.html>"
msgstr ""

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "26 February 2024"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "pamcut - select a rectangular region of a PAM, PBM, PGM, or PPM image"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "[B<-reportonly>]"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"For example, B<-left=50 -right=200> means to keep the 151 columns between "
"Columns 50 and 200 inclusive."
msgstr ""

#. type: Plain text
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"You can code any mixture of the options.  What you don't specify defaults.  "
"Those defaults are in favor of minimal cutting and in favor of cutting the "
"right and bottom edges off and (with B<-pad>) minimal padding and padding on "
"the right and bottom.  It is an error to overspecify, i.e. to specify all "
"three of B<-left>, B<-right>, and B<-width> or B<-top>, B<-bottom>, and B<-"
"height> or B<right> as well as B<-cropright>."
msgstr ""

#. type: TP
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-reportonly>"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"  This causes B<pamcut> to write to Standard Output a description of the\n"
"  cutting it would have done instead of producing an output image.  See\n"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"E<.UR #reportonly>\n"
"below\n"
"E<.UE>\n"
"\\& for a description of this output and ways\n"
"  to use it.\n"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"That description is one line of text, containing 8 decimal numbers of\n"
"  pixels, separated by spaces:\n"
msgstr ""

#. type: IP
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "\\(bu"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "column number of left cut"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "column number of right cut"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "row number of top cut"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "row number of bottom cut"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "width of input image"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "height of input image"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "width of output image"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "height of output image"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"The column number of the left cut is the column number in the input image of "
"the leftmost column of the output image.  for the right cut, it is for the "
"rightmost column of the output.  Top and bottom are analogous."
msgstr ""

#. type: Plain text
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"I<The column and row numbers can be negative> if you specified B<-pad> and "
"B<pamcut> would have added padding.  Likewise, they can be beyond the right "
"and bottom edge of the input image."
msgstr ""

#. type: Plain text
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Example:"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "      10 109 -1 98 150 80 100 100\n"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "This option was new in Netpbm 11.06 (March 2024)."
msgstr ""

#. type: Plain text
#: debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid ""
"B<pnmcrop>(1)  \\&, B<pamdice>(1)  \\&, B<pamcomp>(1)  \\&, B<pnmpad>(1)  "
"\\&, B<pamcat>(1)  \\&, B<pgmslice>(1)  \\&, B<pnm>(1)  \\&"
msgstr ""

#. type: Plain text
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<pnmcrop>(1)  \\&, B<pamdice>(1)  \\&, B<pamcomp>(1)  \\&, B<pnmpad>(1)  "
"\\&, B<pamcat>(1)  \\&, B<pgmslice>(1)  \\&, B<pnm>(5)  \\&"
msgstr ""
