# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-28 16:38+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: SY
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "localedef"
msgstr ""

#. type: TH
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "2024-11-25"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "localedef - compile locale definition files"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "[I<options>] I<outputpath>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<--add-to-archive> [I<options>] I<compiledpath>"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "B<--delete-from-archive> [I<options>] I<localename>\\ .\\|.\\|."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<--list-archive> [I<options>]"
msgstr ""

#. #-#-#-#-#  archlinux: localedef.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  debian-bookworm: localedef.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  debian-unstable: localedef.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-42: localedef.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-rawhide: localedef.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  opensuse-leap-16-0: localedef.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  opensuse-tumbleweed: localedef.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--usage>"
msgstr ""

#. #-#-#-#-#  archlinux: localedef.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  debian-bookworm: localedef.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  debian-unstable: localedef.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-42: localedef.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-rawhide: localedef.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  opensuse-leap-16-0: localedef.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  opensuse-tumbleweed: localedef.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<localedef> program reads the indicated I<charmap> and I<input> files, "
"compiles them to a binary form quickly usable by the locale functions in the "
"C library (B<setlocale>(3), B<localeconv>(3), etc.), and places the output "
"in I<outputpath>."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "The I<outputpath> argument is interpreted as follows:"
msgstr ""

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If I<outputpath> contains a slash character ('/'), it is interpreted as the "
"name of the directory where the output definitions are to be stored.  In "
"this case, there is a separate output file for each locale category "
"(I<LC_TIME>, I<LC_NUMERIC>, and so on)."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the B<--no-archive> option is used, I<outputpath> is the name of a "
"subdirectory in I</usr/lib/locale> where per-category compiled files are "
"placed."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Otherwise, I<outputpath> is the name of a locale and the compiled locale "
"data is added to the archive file I</usr/lib/locale/locale-archive>.  A "
"locale archive is a memory-mapped file which contains all the system-"
"provided locales; it is used by all localized programs when the environment "
"variable B<LOCPATH> is not set."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In any case, B<localedef> aborts if the directory in which it tries to write "
"locale files has not already been created."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If no I<charmapfile> is given, the value I<ANSI_X3.4-1968> (for ASCII) is "
"used by default.  If no I<inputfile> is given, or if it is given as a dash "
"(-), B<localedef> reads from standard input."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Operation-selection options"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"A few options direct B<localedef> to do something other than compile locale "
"definitions.  Only one of these options should be used at a time."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--add-to-archive>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Add the I<compiledpath> directories to the locale archive file.  The "
"directories should have been created by previous runs of B<localedef>, using "
"B<--no-archive>."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--delete-from-archive>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Delete the named locales from the locale archive file."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--list-archive>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "List the locales contained in the locale archive file."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Other options"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Some of the following options are sensible only for certain operations; "
"generally, it should be self-evident which ones.  Notice that B<-f> and B<-"
"c> are reversed from what you might expect; that is, B<-f> is not the same "
"as B<--force>."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-f>I< charmapfile>, B<--charmap=>I<charmapfile>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Specify the file that defines the character set that is used by the input "
"file.  If I<charmapfile> contains a slash character ('/'), it is interpreted "
"as the name of the character map.  Otherwise, the file is sought in the "
"current directory and the default directory for character maps.  If the "
"environment variable B<I18NPATH> is set, I<$I18NPATH/charmaps/> and "
"I<$I18NPATH/> are also searched after the current directory.  The default "
"directory for character maps is printed by B<localedef --help>."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-i>I< inputfile>, B<--inputfile=>I<inputfile>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Specify the locale definition file to compile.  The file is sought in the "
"current directory and the default directory for locale definition files.  If "
"the environment variable B<I18NPATH> is set, I<$I18NPATH/locales/> and "
"I<$I18NPATH> are also searched after the current directory.  The default "
"directory for locale definition files is printed by B<localedef --help>."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-u>I< repertoirefile>, B<--repertoire-map=>I<repertoirefile>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Read mappings from symbolic names to Unicode code points from "
"I<repertoirefile>.  If I<repertoirefile> contains a slash character ('/'), "
"it is interpreted as the pathname of the repertoire map.  Otherwise, the "
"file is sought in the current directory and the default directory for "
"repertoire maps.  If the environment variable B<I18NPATH> is set, "
"I<$I18NPATH/repertoiremaps/> and I<$I18NPATH> are also searched after the "
"current directory.  The default directory for repertoire maps is printed by "
"B<localedef --help>."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-A>I< aliasfile>, B<--alias-file=>I<aliasfile>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Use I<aliasfile> to look up aliases for locale names.  There is no default "
"aliases file."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--force>"
msgstr ""

#. type: TQ
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-c>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Write the output files even if warnings were generated about the input file."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--verbose>"
msgstr ""

#. type: TQ
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-v>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Generate extra warnings about errors that are normally ignored."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--big-endian>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Generate big-endian output."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--little-endian>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Generate little-endian output."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--no-archive>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Do not use the locale archive file, instead create I<outputpath> as a "
"subdirectory in the same directory as the locale archive file, and create "
"separate output files for locale categories in it.  This is helpful to "
"prevent system locale archive updates from overwriting custom locales "
"created with B<localedef>."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--no-hard-links>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Do not create hard links between installed locales."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--no-warnings=>I<warnings>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Comma-separated list of warnings to disable.  Supported warnings are "
"I<ascii> and I<intcurrsym>."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--posix>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Conform strictly to POSIX.  Implies B<--verbose>.  This option currently has "
"no other effect.  POSIX conformance is assumed if the environment variable "
"B<POSIXLY_CORRECT> is set."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--prefix=>I<pathname>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Set the prefix to be prepended to the full archive pathname.  By default, "
"the prefix is empty.  Setting the prefix to I<foo>, the archive would be "
"placed in I<foo/usr/lib/locale/locale-archive>."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--quiet>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Suppress all notifications and warnings, and report only fatal errors."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--replace>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Replace a locale in the locale archive file.  Without this option, if the "
"locale is in the archive file already, an error occurs."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--warnings=>I<warnings>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Comma-separated list of warnings to enable.  Supported warnings are I<ascii> "
"and I<intcurrsym>."
msgstr ""

#. type: TQ
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-?>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Print a usage summary and exit.  Also prints the default paths used by "
"B<localedef>."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Print a short usage summary and exit."
msgstr ""

#. type: TQ
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-V>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Print the version number, license, and disclaimer of warranty for "
"B<localedef>."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "EXIT STATUS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "One of the following exit values can be returned by B<localedef>:"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<0>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Command completed successfully."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<1>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Warnings or errors occurred, output files were written."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<4>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Errors encountered, no output created."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ENVIRONMENT"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<POSIXLY_CORRECT>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "The B<--posix> flag is assumed if this environment variable is set."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<I18NPATH>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "A colon-separated list of search directories for files."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</usr/share/i18n/charmaps>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Usual default character map path."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</usr/share/i18n/locales>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Usual default path for locale definition files."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</usr/share/i18n/repertoiremaps>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Usual default repertoire map path."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</usr/lib/locale/locale-archive>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Usual default locale archive location."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</usr/lib/locale>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Usual default path for compiled individual locale data files."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<outputpath/LC_ADDRESS>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"An output file that contains information about formatting of addresses and "
"geography-related items."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<outputpath/LC_COLLATE>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"An output file that contains information about the rules for comparing "
"strings."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<outputpath/LC_CTYPE>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "An output file that contains information about character classes."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<outputpath/LC_IDENTIFICATION>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "An output file that contains metadata about the locale."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<outputpath/LC_MEASUREMENT>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"An output file that contains information about locale measurements (metric "
"versus US customary)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<outputpath/LC_MESSAGES/SYS_LC_MESSAGES>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"An output file that contains information about the language messages should "
"be printed in, and what an affirmative or negative answer looks like."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<outputpath/LC_MONETARY>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"An output file that contains information about formatting of monetary values."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<outputpath/LC_NAME>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "An output file that contains information about salutations for persons."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<outputpath/LC_NUMERIC>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"An output file that contains information about formatting of nonmonetary "
"numeric values."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<outputpath/LC_PAPER>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"An output file that contains information about settings related to standard "
"paper size."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<outputpath/LC_TELEPHONE>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"An output file that contains information about formats to be used with "
"telephone services."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<outputpath/LC_TIME>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"An output file that contains information about formatting of data and time "
"values."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Compile the locale files for Finnish in the UTF-8 character set and add it "
"to the default locale archive with the name B<fi_FI.UTF-8>:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "localedef -f UTF-8 -i fi_FI fi_FI.UTF-8\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The next example does the same thing, but generates files into the "
"I<fi_FI.UTF-8> directory which can then be used by programs when the "
"environment variable B<LOCPATH> is set to the current directory (note that "
"the last argument must contain a slash):"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "localedef -f UTF-8 -i fi_FI ./fi_FI.UTF-8\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<locale>(1), B<charmap>(5), B<locale>(5), B<repertoiremap>(5), B<locale>(7)"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-10"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
msgid "B<--delete-from-archive> [I<options>] I<localename> ..."
msgstr ""

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<-c>, B<--force>"
msgstr ""

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr ""

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<-?>, B<--help>"
msgstr ""

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr ""

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "2024-05-02"
msgstr ""

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr ""

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr ""
