# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-28 16:44+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "PSMERGE"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "PSUtils Release 1 Patchlevel 17"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "psmerge - filter to merge several PostScript files into one"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<psmerge> [ B<-o>I<out.ps> ] I<file.ps ...>"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"I<Psmerge> merges PostScript documents into a single document. It only works "
"in the specific case the the files were created using the same application, "
"with the same device setup and resources (fonts, procsets, patterns, files, "
"etc)  loaded."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"If the B<-o> option is used, output will be sent to the file named, "
"otherwise it will go to standard output."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"I<Psmerge> will merge multiple files concatenated into a single file as if "
"they were in separate files."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "AUTHOR"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Copyright (C) Angus J. C. Duggan 1991-1995"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"psbook(1), psselect(1), pstops(1), epsffit(1), psnup(1), psresize(1), "
"psmerge(1), fixscribeps(1), getafm(1), fixdlsrps(1), fixfmps(1), "
"fixpsditps(1), fixpspps(1), fixtpps(1), fixwfwps(1), fixwpps(1), fixwwps(1), "
"extractres(1), includeres(1), showchar(1)"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "TRADEMARKS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<PostScript> is a trademark of Adobe Systems Incorporated."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "BUGS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"I<psmerge> is for a very specific case; it does not merge files in the "
"general case."
msgstr ""

#. type: TH
#: debian-unstable
#, no-wrap
msgid "PSUtils"
msgstr ""

#. type: Plain text
#: debian-unstable
msgid ""
"I<Psmerge> uses Ghostscript to merge PostScript documents into a single "
"document.  It only works in the specific case the the files were created "
"using the same application, with the same device setup and resources (fonts, "
"procsets, patterns, files, etc) loaded."
msgstr ""

#. type: Plain text
#: debian-unstable
msgid "Written by Angus J. C. Duggan."
msgstr ""

#. type: Plain text
#: debian-unstable
msgid "B<psutils>(1)"
msgstr ""
