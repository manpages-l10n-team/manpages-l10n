# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-16 05:58+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "RECFMT"
msgstr ""

#. type: TH
#: debian-bookworm fedora-42 fedora-rawhide
#, no-wrap
msgid "April 2022"
msgstr ""

#. type: TH
#: debian-bookworm fedora-42 fedora-rawhide
#, no-wrap
msgid "recfmt 1.9"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "User Commands"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "recfmt - apply a template to records"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "B<recfmt> [I<\\,OPTION\\/>]... [I<\\,TEMPLATE\\/>]"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "Apply a template to records read from standard input."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "B<-f>, B<--file>=I<\\,FILENAME\\/>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "read the template to apply from a file."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "B<--help>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "print a help message and exit."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "B<--version>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "show version and exit."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "AUTHOR"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "Written by Jose E. Marchesi."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "Report bugs to: bug-recutils@gnu.org"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid ""
"GNU recutils home page: E<lt>https://www.gnu.org/software/recutils/E<gt>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "General help using GNU software: E<lt>http://www.gnu.org/gethelp/E<gt>"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "COPYRIGHT"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid ""
"Copyright \\(co 2010-2020 Jose E. Marchesi.  License GPLv3+: GNU GPL version "
"3 or later E<lt>http://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid ""
"The full documentation for B<recfmt> is maintained as a Texinfo manual.  If "
"the B<info> and B<recfmt> programs are properly installed at your site, the "
"command"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "B<info recutils>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "should give you access to the complete manual."
msgstr ""

#. type: TH
#: debian-unstable
#, no-wrap
msgid "March 2024"
msgstr ""

#. type: TH
#: debian-unstable
#, no-wrap
msgid "GNU recutils 1.9"
msgstr ""
