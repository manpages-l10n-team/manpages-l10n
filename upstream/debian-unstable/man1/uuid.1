.\" SPDX-License-Identifier: 0BSD
.\"
.Dd December  9, 2024
.ds doc-volume-operating-system OSSP
.Dt UUID 1
.Os ossp-uuid 1.6.4
.
.Sh NAME
.Nm uuid
.Nd generate, convert, and decode Universally Unique IDentifiers
.Sh SYNOPSIS
.Nm
.Op Fl n Ar count
.Op Fl o Ar outfile
.Op Fl r Ns \&| Ns Fl F Cm BIN Ns \&| Ns Cm STR Ns \&| Ns Cm SIV
.Op Fl v Cm 1
.Op Fl m1
.
.Nm
.Op Fl n Ar count
.Op Fl o Ar outfile
.Op Fl r Ns \&| Ns Fl F Cm BIN Ns \&| Ns Cm STR Ns \&| Ns Cm SIV
.Fl v Cm 3
.Ar uuid Ns \&| Ns Cm nil Ns \&| Ns Cm max Ns \&| Ns Cm ns:\& Ns Brq Cm DNS Ns \&| Ns Cm URL Ns \&| Ns Cm OID Ns \&| Ns Cm X500
.Ar data
.
.Nm
.Op Fl n Ar count
.Op Fl o Ar outfile
.Op Fl r Ns \&| Ns Fl F Cm BIN Ns \&| Ns Cm STR Ns \&| Ns Cm SIV
.Fl v Cm 4
.
.Nm
.Op Fl n Ar count
.Op Fl o Ar outfile
.Op Fl r Ns \&| Ns Fl F Cm BIN Ns \&| Ns Cm STR Ns \&| Ns Cm SIV
.Fl v Cm 5
.Ar uuid Ns \&| Ns Cm nil Ns \&| Ns Cm max Ns \&| Ns Cm ns:\& Ns Brq Cm DNS Ns \&| Ns Cm URL Ns \&| Ns Cm OID Ns \&| Ns Cm X500
.Ar data
.
.Nm
.Op Fl n Ar count
.Op Fl o Ar outfile
.Op Fl r Ns \&| Ns Fl F Cm BIN Ns \&| Ns Cm STR Ns \&| Ns Cm SIV
.Fl v Cm 6
.
.Nm
.Op Fl n Ar count
.Op Fl o Ar outfile
.Op Fl r Ns \&| Ns Fl F Cm BIN Ns \&| Ns Cm STR Ns \&| Ns Cm SIV
.Fl v Cm 7
.Pp
.
.Nm
.Fl d
.Op Fl o Ar outfile
.Op Cm \h'\w'\|-r\fR|\fP'u' Ns Fl F Cm \h'\w'BIN\fR|\fP'u'STR Ns \&| Ns Cm SIV
.Ar uuid
.
.Nm
.Fl d
.Op Fl o Ar outfile
.Op Fl r Ns \&| Ns Fl F Cm BIN Ns \&| Ns Cm STR Ns \&| Ns Cm SIV
.Sy -
.
.Sh DESCRIPTION
By default, generates a UUID (Universally Unique Identifier),
of a version specified by
.Fl v
.Pq default Sy 1
in the canonical
.Cm STR Ns ing
format.
.br
With
.Fl d ,
decodes
.Ar uuid
.Pq or reads it in from the standard input stream if Qq Sy - ,
yielding something akin to
.Bd -literal -compact -offset 2n
encode: STR:     92a3d3de-6bbf-11ef-9f5b-774ebb537938
        SIV:     194917928982963228599463962120788605240
decode: variant: DCE 1.1, ISO/IEC 11578:1996
        version: 1 (time and node based)
        content: time:  2024-09-05 19:46:41.491043.0 UTC
                 clock: 8027 (usually random)
                 node:  77:4e:bb:53:79:38 (local multicast)
.Ed
or
.Bd -literal -compact -offset 2n
encode: STR:     7e017d98-cecc-41f6-8030-552d23cd38e6
        SIV:     167490467173639937831846401373276813542
decode: variant: DCE 1.1, ISO/IEC 11578:1996
        version: 4 (random data based)
        content: 7E:01:7D:98:CE:CC:01:F6:00:30:55:2D:23:CD:38:E6
                 (no semantics: random data only)
.Ed
.Pp
A UUID is a 128-bit (16-byte) number whose format (method of generation)
makes it very likely that it will be unique, both spatially and temporally.
This makes them well-suited for identifying anything from ephemera like database queries to databases themselves.
.Pp
The three principal
.Fl F Ns ormats
are:
.Bl -tag -compact -offset 2n -width ".Cm SIV Pq Single Integer Value"
.It Cm STR Ns ing
.Li 01234567-890a-bcde-f012-3456789abcde
(hexadecimal string broken up by dashes)
.
.It Cm SIV Pq Single Integer Value
128-bit decimal integer
.
.It Cm BIN Ns ary or \&"raw"
the 16 bytes of the UUID in big endian order (most significant first)
.El
.Pp
UUIDs come in a few
.Fl v Ns ersions :
.Bl -tag -compact -offset 2n -width ".Sy 4"
.It Sy 1
based on the current time (to the precision of 100ns),
and the host's MAC address
(this implementation uses the MAC address of the first NIC it finds, unless
.Fl m ,
in which case random data is substituted for the MAC)
.
.It Sy 6
very similar, but the most significant (slowest-changing) parts of the timestamp are stored first,
which improves locality in some database applications
.Pp
.
.It Sy 7
contains the current
.Em \s[-1z]UNIX\s0
time \(em versions
.Sy 1
and
.Sy 6
encode a UUID time which is off by like 400 years \(em (to the precision of 1ms),
then 10 bytes of random data.
.br
.Em This version should be preferred to 1 and 6.\&
.Pp
.
.It Sy 3
hashes the "namespace" (first argument, either a
.Cm STR Ns ing-format UUID or a well-known name (see below))
and the "name" (second argument, arbitrary data) with the
.Xr MD5 3
digest, and uses that directly (a few branding bytes are replaced to identify the format)
.
.It Sy 5
the same as version
.Sy 3 ,
but uses the first 16 bytes of the
.Xr SHA1 3
digest
.Pp
.
.It Sy 4
just random data
.El
.Pp
The namespaces understood when generating version 3 and 5 UUIDs are:
.Bl -tag -compact -offset 2n -width ".Cm ns:X500"
.It Ar uuid
(any valid
.Cm STR Ns ingified
UUID)
.It Cm nil
.Li 00000000-0000-0000-0000-000000000000
\(em the special all-zero UUID
.
.It Cm max
.Li ffffffff-ffff-ffff-ffff-ffffffffffff
\(em the special all-bits-set/sorts-after-everything sentinel UUID
.
.It Cm ns:DNS
.Li 6ba7b810-9dad-11d1-80b4-00c04fd430c8
\(em for fully-qualified domain names
.
.It Cm ns:URL
.Li 6ba7b811-9dad-11d1-80b4-00c04fd430c8
\(em for URLs
.
.It Cm ns:OID
.Li 6ba7b812-9dad-11d1-80b4-00c04fd430c8
\(em for ISO OIDs
.
.It Cm ns:X500
.Li 6ba7b814-9dad-11d1-80b4-00c04fd430c8
\(em for X.500 Distinguished Names
.El
.
.Sh OPTIONS
.Bl -tag -compact -width ".Fl F Cm BIN Ns \&| Ns Cm STR Ns \&| Ns Cm SIV"
.It Fl n Ar count
Generate
.Ar count
UUIDs.
Defaults to
.Sy 1 .
.
.It Fl o Ar outfile
Write to
.Ar outfile .
.
.It Fl F Cm BIN Ns \&| Ns Cm STR Ns \&| Ns Cm SIV
Produce output in the given format.
Defaults to
.Cm STR .
.
.It Fl r
.Fl F Cm BIN
.
.It Fl v Cm 1 Ns \&| Ns Cm 3 Ns \&| Ns Cm 4 Ns \&| Ns Cm 5 Ns \&| Ns Cm 6 Ns \&| Ns Cm 7
Version to generate.
Defaults to
.Cm 1 .
.
.It Fl m
.Fl v Cm 1 Ns \&| Ns Cm 6
only:
ignore the current host's MAC addresses, use random data instead
(this may still happen if the if a MAC can't be determined or all MACs are multicast).
.
.It Fl 1
.Fl v Cm 1
only,
.Ar count
>1 only:
generate each UUID independently.
Version 1 UUIDs have a field that increases monotonically within a session; thus, for example, in
.Bd -literal -compact -offset 2n
.Li $ Nm Fl n Ar 4
366ab5a3-6bc4-11ef-a31a-0026b986fdd4
366ab5ce-6bc4-11ef-a31b-0026b986fdd4
366ab5f8-6bc4-11ef-a31c-0026b986fdd4
366ab621-6bc4-11ef-a31d-0026b986fdd4
                   \(ha\(ha\(ha\(ha
.Ed
the highlighted column
.Em starts
random, but then increments.
.Fl 1
generates each UUID de novo.
.Pp
.
.It Fl d
Decode and parse
.Ar uuid .
.Fl F
sets which format to
.Em read .
.Fl F Cm BIN
is only available when reading from the standard input stream
.Pq Qq Sy - .
.El
.
.Sh EXAMPLES
A web site can be uniquely identified with a version 5 (SHA-1), namespace
.Cm ns:DNS
UUID, and a web-page \(em
.Cm ns:URL
(note that this information cannot be extracted, and only serves as a way to avoid collisions for otherwise-identical names):
.Bd -literal -compact
.Li $ Nm Fl v Cm 5 ns:DNS Ar hinfo.network.\&
f50b485f-ac66-591d-b95f-2c946c5a5668
.Li $ Nm Fl v Cm 5 ns:URL Ar https://hinfo.network
8cebc56f-51d0-5323-a031-1b9258de14f8
.Li $ Nm Fl d Ar 8cebc56f-51d0-5323-a031-1b9258de14f8
encode: STR:     f50b485f-ac66-591d-b95f-2c946c5a5668
        SIV:     325719442146270326702531202385345926760
decode: variant: DCE 1.1, ISO/IEC 11578:1996
        version: 5 (name based, SHA-1)
        content: F5:0B:48:5F:AC:66:09:1D:39:5F:2C:94:6C:5A:56:68
                 (not decipherable: truncated SHA-1 message digest only)
.Ed
.Pp
.Bd -literal -compact
.Li $ Nm Fl v Cm 7
8cebc56f-51d0-5323-a031-1b9258de14f8
.Li $ Nm Fl d Ar 8cebc56f-51d0-5323-a031-1b9258de14f8
encode: STR:     0191c41a-9de1-7f96-b8cb-88e4348ee74c
        SIV:     2086088501348764349551890213853128524
decode: variant: DCE 1.1, ISO/IEC 11578:1996
        version: 7 (UNIX time + random data)
        content: time:   2024-09-05 21:32:44.385 UTC
                 random: 0F:96:38:CB:88:E4:34:8E:E7:4C
.Ed
.
.Sh SEE ALSO
.Xr uuid 3 ,
.Xr OSSP::uuid 3
.
.Sh STANDARDS
.Lk https://datatracker.ietf.org/doc/html/rfc9562 "RFC 9562: Universally Unique IDentifiers (UUIDs)"
supersedes all previous standards with an unbecoming brevity.
.Pp
There are many Versions, but only one useful Variant.
