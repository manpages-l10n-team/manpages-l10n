.\"***************************************************************************
.\" Copyright 2019-2022,2023 Thomas E. Dickey                                *
.\" Copyright 1998-2012,2017 Free Software Foundation, Inc.                  *
.\"                                                                          *
.\" Permission is hereby granted, free of charge, to any person obtaining a  *
.\" copy of this software and associated documentation files (the            *
.\" "Software"), to deal in the Software without restriction, including      *
.\" without limitation the rights to use, copy, modify, merge, publish,      *
.\" distribute, distribute with modifications, sublicense, and/or sell       *
.\" copies of the Software, and to permit persons to whom the Software is    *
.\" furnished to do so, subject to the following conditions:                 *
.\"                                                                          *
.\" The above copyright notice and this permission notice shall be included  *
.\" in all copies or substantial portions of the Software.                   *
.\"                                                                          *
.\" THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS  *
.\" OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF               *
.\" MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.   *
.\" IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,   *
.\" DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR    *
.\" OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR    *
.\" THE USE OR OTHER DEALINGS IN THE SOFTWARE.                               *
.\"                                                                          *
.\" Except as contained in this notice, the name(s) of the above copyright   *
.\" holders shall not be used in advertising or otherwise to promote the     *
.\" sale, use or other dealings in this Software without prior written       *
.\" authorization.                                                           *
.\"***************************************************************************
.\"
.\" $Id: curs_addstr.3x,v 1.37 2023/11/18 21:18:55 tom Exp $
.TH addstr 3NCURSES 2023-11-18 "ncurses 6.4" "Library calls"
.ie \n(.g \{\
.ds `` \(lq
.ds '' \(rq
.\}
.el \{\
.ie t .ds `` ``
.el   .ds `` ""
.ie t .ds '' ''
.el   .ds '' ""
.\}
.
.de bP
.ie n  .IP \(bu 4
.el    .IP \(bu 2
..
.SH NAME
\fB\%addstr\fP,
\fB\%addnstr\fP,
\fB\%waddstr\fP,
\fB\%waddnstr\fP,
\fB\%mvaddstr\fP,
\fB\%mvaddnstr\fP,
\fB\%mvwaddstr\fP,
\fB\%mvwaddnstr\fP \-
add a string to a \fIcurses\fR window and advance the cursor
.SH SYNOPSIS
.nf
\fB#include <ncursesw/curses.h>
.PP
\fBint addstr(const char *\fIstr\fP);
\fBint addnstr(const char *\fIstr\fP, int \fIn\fP);
\fBint waddstr(WINDOW *\fIwin\fP, const char *\fIstr\fP);
\fBint waddnstr(WINDOW *\fIwin\fP, const char *\fIstr\fP, int \fIn\fP);
.PP
\fBint mvaddstr(int \fIy\fP, int \fIx\fP, const char *\fIstr\fP);
\fBint mvaddnstr(int \fIy\fP, int \fIx\fP, const char *\fIstr\fP, int \fIn\fP);
\fBint mvwaddstr(WINDOW *\fIwin\fP, int \fIy\fP, int \fIx\fP, const char *\fIstr\fP);
\fBint mvwaddnstr(WINDOW *\fIwin\fP, int \fIy\fP, int \fIx\fP, const char *\fIstr\fP, int \fIn\fP);
.fi
.SH DESCRIPTION
These functions write the (null-terminated) character string
\fIstr\fP on the given window.
It is similar to calling \fBwaddch\fP once for each byte in the string.
.PP
The \fImv\fP functions perform cursor movement once, before writing any
characters.
Thereafter, the cursor is advanced as a side-effect of writing to the window.
.PP
The four functions with \fIn\fP as the last argument
write at most \fIn\fP bytes,
or until a terminating null is reached.
If \fIn\fP is \-1, then the entire string will be added.
.SH RETURN VALUE
All functions return the integer \fBERR\fP upon failure and \fBOK\fP on success.
.PP
X/Open does not define any error conditions.
This implementation returns an error
.bP
if the window pointer is null or
.bP
if the string pointer is null or
.bP
if the corresponding calls to \fBwaddch\fP return an error.
.PP
Functions with a \*(``mv\*('' prefix first perform a cursor movement using
\fBwmove\fP, and return an error if the position is outside the window,
or if the window pointer is null.
.SH NOTES
All of these functions except \fBwaddnstr\fP may be macros.
.SH PORTABILITY
These functions are described in the XSI Curses standard, Issue 4.
.SH SEE ALSO
\fB\%ncurses\fP(3NCURSES),
\fB\%addch\fP(3NCURSES),
\fB\%addchstr\fP(3NCURSES),
\fB\%addwstr\fP(3NCURSES),
\fB\%add_wchstr\fP(3NCURSES)
