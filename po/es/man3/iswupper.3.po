# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Miguel Pérez Ibars <mpi79470@alu.um.es>, 2004.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2025-02-28 16:37+0100\n"
"PO-Revision-Date: 2004-06-26 19:53+0200\n"
"Last-Translator: Miguel Pérez Ibars <mpi79470@alu.um.es>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.04.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "upper"
msgid "iswupper"
msgstr "upper"

#. type: TH
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "2024-07-23"
msgstr "23 ​​Julio 2024"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr "Páginas de Manual de Linux 6.12"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "iswupper - test for uppercase wide character"
msgstr "iswupper - comprueba si un carácter ancho es mayúsculas"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "Standard C library (I<libc>,\\ I<-lc>)"
msgstr "Biblioteca Estándar C (I<libc>,\\ I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>wctype.hE<gt>>\n"
msgstr "B<#include E<lt>wctype.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<int iswupper(wint_t >I<wc>B<);>\n"
msgstr "B<int iswupper(wint_t >I<wc>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<iswupper>()  function is the wide-character equivalent of the "
"B<isupper>(3)  function.  It tests whether I<wc> is a wide character "
"belonging to the wide-character class \"upper\"."
msgstr ""
"La función B<iswupper>() es la equivalente para caracteres anchos de la "
"función B<isupper>(3). Verifica si I<wc> es un carácter ancho perteneciente "
"a la clase de caracteres anchos \"upper\"."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The wide-character class \"upper\" is a subclass of the wide-character class "
"\"alpha\", and therefore also a subclass of the wide-character class "
"\"alnum\", of the wide-character class \"graph\" and of the wide-character "
"class \"print\"."
msgstr ""
"La clase de caracteres anchos \"upper\" es una subclase de la clase de "
"caracteres anchos \"alpha\" y por tanto también es una subclase de la clase "
"de caracteres anchos \"alnum\", una subclase de la clase de caracteres "
"anchos \"graph\" y de la clase \"print\"."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Being a subclass of the wide-character class \"print\", the wide-character "
"class \"upper\" is disjoint from the wide-character class \"cntrl\"."
msgstr ""
"Al ser una subclase de la clase de caracteres anchos \"print\", la clase de "
"caracteres anchos \"upper\" es disjunta de la clase de caracteres anchos "
"\"cntrl\"."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Being a subclass of the wide-character class \"graph\", the wide-character "
"class \"upper\" is disjoint from the wide-character class \"space\" and its "
"subclass \"blank\"."
msgstr ""
"Al ser una subclase de la clase de caracteres anchos \"graph\", la clase de "
"caracteres anchos \"upper\" es disjunta de la clase de caracteres anchos "
"\"space\" y de su subclase \"blank\"."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Being a subclass of the wide-character class \"alnum\", the wide-character "
"class \"upper\" is disjoint from the wide-character class \"punct\"."
msgstr ""
"Al ser una subclase de la clase de caracteres anchos \"alnum\", la clase de "
"caracteres anchos \"upper\" es disjunta de la clase de caracteres anchos "
"\"punct\"."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Being a subclass of the wide-character class \"alpha\", the wide-character "
"class \"upper\" is disjoint from the wide-character class \"digit\"."
msgstr ""
"Al ser una subclase de la clase de caracteres anchos \"alpha\" la clase de "
"caracteres anchos \"upper\" es disjunta de la clase de caracteres anchos "
"\"digit\"."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The wide-character class \"upper\" contains at least those characters I<wc> "
"which are equal to I<towupper(wc)> and different from I<towlower(wc)>."
msgstr ""
"La clase de caracteres anchos \"upper\" contiene al menos los caracteres "
"I<wc> que son iguales a I<towupper(wc)> y diferentes de I<towlower(wc)>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The wide-character class \"upper\" always contains at least the letters \\"
#| "(aqA\\(aq to \\(aqZ\\(aq."
msgid ""
"The wide-character class \"upper\" always contains at least the letters \\"
"[aq]A\\[aq] to \\[aq]Z\\[aq]."
msgstr ""
"La clase de caracteres anchos \"upper\" siempre contiene al menos las letras "
"desde la \\(aqA\\(aq a la \\(aqZ\\(aq."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOR DEVUELTO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<iswupper>()  function returns nonzero if I<wc> is a wide character "
"belonging to the wide-character class \"upper\".  Otherwise, it returns zero."
msgstr ""
"La función B<iswupper>() devuelve un valor no nulo si I<wc> es una carácter "
"ancho perteneciente a la clase de caracteres anchos \"upper\". En otro caso "
"devuelve cero."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRIBUTOS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Para obtener una explicación de los términos usados en esta sección, véase "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interfaz"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atributo"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valor"

#. type: tbl table
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<iswupper>()"
msgstr "B<iswupper>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Seguridad del hilo"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe locale"
msgstr "Configuración regional de multi-hilo seguro"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "ESTÁNDARES"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr "C11, POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIAL"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2001, C99."
msgstr "POSIX.1-2001, C99."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTAS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The behavior of B<iswupper>()  depends on the B<LC_CTYPE> category of the "
"current locale."
msgstr ""
"El comportamiento de B<iswupper>() depende de la categoría B<LC_CTYPE> de la "
"localización actual."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "This function is not very appropriate for dealing with Unicode "
#| "characters, because Unicode knows about three cases: upper, lower and "
#| "title case."
msgid ""
"This function is not very appropriate for dealing with Unicode characters, "
"because Unicode knows about three cases: upper, lower, and title case."
msgstr ""
"Esta función no es muy apropiada para trabajar con caracteres Unicode debido "
"a que Unicode conoce de tres casos, mayúsculas, minúsculas y título."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<isupper>(3), B<iswctype>(3), B<towupper>(3)"
msgstr "B<isupper>(3), B<iswctype>(3), B<towupper>(3)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 Febrero 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Páginas de Manual de Linux 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca Estándar C (I<libc>, I<-lc>)"

#. type: Plain text
#: debian-bookworm
msgid "POSIX.1-2001, POSIX.1-2008, C99."
msgstr "POSIX.1-2001, POSIX.1-2008, C99."

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "2024-05-02"
msgstr "2 Mayo 2024"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Páginas de Manual de Linux 6.9.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 Octubre 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Páginas de Manual de Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Páginas de Manual de Linux (no publicadas)"
