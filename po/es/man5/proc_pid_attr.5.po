# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Juan Piernas <piernas@ditec.um.es>, 1998-1999, 2005.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.22.0\n"
"POT-Creation-Date: 2025-02-28 16:44+0100\n"
"PO-Revision-Date: 2024-03-29 09:47+0100\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "proc_pid_attr"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2 Mayo 2024"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr "Páginas de Manual de Linux 6.12"

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "/proc/pid/attr/ - security-related attributes"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</proc/>pidI</attr/>"
msgstr ""

#
#.  https://lwn.net/Articles/28222/
#.  From:    Stephen Smalley <sds@epoch.ncsc.mil>
#.  To:	     LKML and others
#.  Subject: [RFC][PATCH] Process Attribute API for Security Modules
#.  Date:    08 Apr 2003 16:17:52 -0400
#. 	http://www.nsa.gov/research/_files/selinux/papers/module/x362.shtml
#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The files in this directory provide an API for security modules.  The "
"contents of this directory are files that can be read and written in order "
"to set security-related attributes.  This directory was added to support "
"SELinux, but the intention was that the API be general enough to support "
"other security modules.  For the purpose of explanation, examples of how "
"SELinux uses these files are provided below."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This directory is present only if the kernel was configured with "
"B<CONFIG_SECURITY>."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</proc/>pidI</attr/current> (since Linux 2.6.0)"
msgstr "I</proc/>pidI</attr/current> (desde Linux 2.6.0)"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The contents of this file represent the current security attributes of the "
"process."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In SELinux, this file is used to get the security context of a process.  "
"Prior to Linux 2.6.11, this file could not be used to set the security "
"context (a write was always denied), since SELinux limited process security "
"transitions to B<execve>(2)  (see the description of I</proc/>pidI</attr/"
"exec>, below).  Since Linux 2.6.11, SELinux lifted this restriction and "
"began supporting \"set\" operations via writes to this node if authorized by "
"policy, although use of this operation is only suitable for applications "
"that are trusted to maintain any desired separation between the old and new "
"security contexts."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Prior to Linux 2.6.28, SELinux did not allow threads within a multithreaded "
"process to set their security context via this node as it would yield an "
"inconsistency among the security contexts of the threads sharing the same "
"memory space.  Since Linux 2.6.28, SELinux lifted this restriction and began "
"supporting \"set\" operations for threads within a multithreaded process if "
"the new security context is bounded by the old security context, where the "
"bounded relation is defined in policy and guarantees that the new security "
"context has a subset of the permissions of the old security context."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Other security modules may choose to support \"set\" operations via writes "
"to this node."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</proc/>pidI</attr/exec> (since Linux 2.6.0)"
msgstr "I</proc/>pidI</attr/exec> (desde Linux 2.6.0)"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This file represents the attributes to assign to the process upon a "
"subsequent B<execve>(2)."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In SELinux, this is needed to support role/domain transitions, and "
"B<execve>(2)  is the preferred point to make such transitions because it "
"offers better control over the initialization of the process in the new "
"security label and the inheritance of state.  In SELinux, this attribute is "
"reset on B<execve>(2)  so that the new program reverts to the default "
"behavior for any B<execve>(2)  calls that it may make.  In SELinux, a "
"process can set only its own I</proc/>pidI</attr/exec> attribute."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</proc/>pidI</attr/fscreate> (since Linux 2.6.0)"
msgstr "I</proc/>pidI</attr/fscreate> (desde Linux 2.6.0)"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This file represents the attributes to assign to files created by subsequent "
"calls to B<open>(2), B<mkdir>(2), B<symlink>(2), and B<mknod>(2)"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"SELinux employs this file to support creation of a file (using the "
"aforementioned system calls)  in a secure state, so that there is no risk of "
"inappropriate access being obtained between the time of creation and the "
"time that attributes are set.  In SELinux, this attribute is reset on "
"B<execve>(2), so that the new program reverts to the default behavior for "
"any file creation calls it may make, but the attribute will persist across "
"multiple file creation calls within a program unless it is explicitly "
"reset.  In SELinux, a process can set only its own I</proc/>pidI</attr/"
"fscreate> attribute."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</proc/>pidI</attr/keycreate> (since Linux 2.6.18)"
msgstr "I</proc/>pidI</attr/keycreate> (desde Linux 2.6.18)"

#.  commit 4eb582cf1fbd7b9e5f466e3718a59c957e75254e
#.  commit b68101a1e8f0263dbc7b8375d2a7c57c6216fb76
#.  commit d410fa4ef99112386de5f218dd7df7b4fca910b4
#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If a process writes a security context into this file, all subsequently "
"created keys (B<add_key>(2))  will be labeled with this context.  For "
"further information, see the kernel source file I<Documentation/security/"
"keys/core.rst> (or file I<Documentation/security/keys.txt> between Linux 3.0 "
"and Linux 4.13, or I<Documentation/keys.txt> before Linux 3.0)."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</proc/>pidI</attr/prev> (since Linux 2.6.0)"
msgstr "I</proc/>pidI</attr/prev> (desde Linux 2.6.0)"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This file contains the security context of the process before the last "
"B<execve>(2); that is, the previous value of I</proc/>pidI</attr/current>."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</proc/>pidI</attr/socketcreate> (since Linux 2.6.18)"
msgstr "I</proc/>pidI</attr/socketcreate> (desde Linux 2.6.18)"

#.  commit 42c3e03ef6b298813557cdb997bd6db619cd65a2
#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If a process writes a security context into this file, all subsequently "
"created sockets will be labeled with this context."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<proc>(5)"
msgstr "B<proc>(5)"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Páginas de Manual de Linux 6.9.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-08-15"
msgstr "15 Agosto 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Páginas de Manual de Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Páginas de Manual de Linux (no publicadas)"
