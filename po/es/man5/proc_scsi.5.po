# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Juan Piernas <piernas@ditec.um.es>, 1998-1999, 2005.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.22.0\n"
"POT-Creation-Date: 2025-02-28 16:44+0100\n"
"PO-Revision-Date: 2024-03-29 09:48+0100\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "proc_scsi"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2 Mayo 2024"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr "Páginas de Manual de Linux 6.12"

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "/proc/scsi/ - SCSI"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</proc/scsi/>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"A directory with the I<scsi> mid-level pseudo-file and various SCSI low-"
"level driver directories, which contain a file for each SCSI host in this "
"system, all of which give the status of some part of the SCSI IO subsystem.  "
"These files contain ASCII structures and are, therefore, readable with "
"B<cat>(1)."
msgstr ""
"Directorio con pseudoficheros I<scsi> de nivel medio y varios directorios de "
"manejadores (I<drivers>) SCSI de bajo nivel, que contienen un fichero para "
"cada host SCSI presente en el sistema, cada uno de los cuales da el estado "
"de alguna parte del subsystema de E/S SCSI. Estos ficheros contienen "
"estructuras ASCII y, por tanto, son legibles con B<cat>(1)."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"You can also write to some of the files to reconfigure the subsystem or "
"switch certain features on or off."
msgstr ""
"También se puede escribir en algunos de estos ficheros para reconfigurar el "
"subsistema o para activar y desactivar ciertas características."

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</proc/scsi/scsi>"
msgstr "I</proc/scsi/scsi>"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This is a listing of all SCSI devices known to the kernel.  The listing is "
"similar to the one seen during bootup.  scsi currently supports only the "
"I<add-single-device> command which allows root to add a hotplugged device to "
"the list of known devices."
msgstr ""
"Esta es una lista de todos los dispositivos SCSI conocidos por el núcleo. La "
"lista es similar a la que se ve durante el arranque. Actualmente, scsi "
"únicamente soporta la orden I<add-single-device> que permite al "
"administrador añadir a la lista de dispositivos conocidos un dispositivo "
"conectado \"en caliente\"."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "The command"
msgstr "La orden"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "echo \\[aq]scsi add-single-device 1 0 5 0\\[aq] E<gt> /proc/scsi/scsi\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"will cause host scsi1 to scan on SCSI channel 0 for a device on ID 5 LUN 0.  "
"If there is already a device known on this address or the address is "
"invalid, an error will be returned."
msgstr ""
"hará que el host scsi1 explore el canal SCSI 0 en busca de un dispositivo en "
"la dirección ID 5 LUN 0. Si ya hay un dispositivo conocido en esa dirección "
"o si la dirección es inválida, se devolverá un error."

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</proc/scsi/>drivernameI</>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<drivername> can currently be NCR53c7xx, aha152x, aha1542, aha1740, "
"aic7xxx, buslogic, eata_dma, eata_pio, fdomain, in2000, pas16, qlogic, "
"scsi_debug, seagate, t128, u15-24f, ultrastore, or wd7000.  These "
"directories show up for all drivers that registered at least one SCSI HBA.  "
"Every directory contains one file per registered host.  Every host-file is "
"named after the number the host was assigned during initialization."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Reading these files will usually show driver and host configuration, "
"statistics, and so on."
msgstr ""
"La lectura de estos ficheros mostrará normalmente la configuración del "
"manejador y el host, estadísticas, etc."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Writing to these files allows different things on different hosts.  For "
"example, with the I<latency> and I<nolatency> commands, root can switch on "
"and off command latency measurement code in the eata_dma driver.  With the "
"I<lockup> and I<unlock> commands, root can control bus lockups simulated by "
"the scsi_debug driver."
msgstr ""
"La escritura en estos ficheros permite diferentes cosas sobre diferentes "
"hosts. Por ejemplo, con las órdenes I<latency> y I<nolatency>, el "
"administrador puede activar y desactivar en el manejador eata_dma el código "
"para la medición de la latencia de las órdenes. Con las órdenes I<lockup> y "
"I<unlock>, el administrador puede controlar las búsquedas de bus simuladas "
"por el manejador scsi_debug."

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<proc>(5)"
msgstr "B<proc>(5)"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Páginas de Manual de Linux 6.9.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-08-15"
msgstr "15 Agosto 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Páginas de Manual de Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Páginas de Manual de Linux (no publicadas)"
