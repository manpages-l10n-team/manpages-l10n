# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Hanno Wagner <wagner@bidnix.bid.fh-hannover.de>, 1996.
# Helge Kreutzmann <debian@helgefjell.de>, 2012, 2019.
# Martin Eberhard Schauer <Martin.E.Schauer@gmx.de>, 2010, 2012.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2013, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.23.1\n"
"POT-Creation-Date: 2025-02-28 16:55+0100\n"
"PO-Revision-Date: 2024-06-29 13:19+0200\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.12.3\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ustat"
msgstr "ustat"

#. type: TH
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "2024-07-23"
msgstr "23. Juli 2024"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr "Linux man-pages 6.12"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "ustat - get filesystem statistics"
msgstr "ustat - erstellt Dateisystem-Statistiken"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHEK"

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "Standard C library (I<libc>,\\ I<-lc>)"
msgstr "Standard-C-Bibliothek (I<libc>,\\ I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>sys/types.hE<gt>>\n"
"B<#include E<lt>unistd.hE<gt>>    /* libc[45] */\n"
"B<#include E<lt>ustat.hE<gt>>     /* glibc2 */\n"
msgstr ""
"B<#include E<lt>sys/types.hE<gt>>\n"
"B<#include E<lt>unistd.hE<gt>>    /* Libc[45] */\n"
"B<#include E<lt>ustat.hE<gt>>     /* Glibc2 */\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<[[deprecated]] int ustat(dev_t >I<dev>B<, struct ustat *>I<ubuf>B<);>\n"
msgstr "B<[[veraltet]] int ustat(dev_t >I<gerät>B<, struct ustat *>I<upuf>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<ustat>()  returns information about a mounted filesystem.  I<dev> is a "
"device number identifying a device containing a mounted filesystem.  I<ubuf> "
"is a pointer to a I<ustat> structure that contains the following members:"
msgstr ""
"B<ustat>() gibt Informationen über ein eingehängtes Dateisystem zurück. Die "
"Gerätenummer I<gerät> identifiziert das eingehängte Dateisystem. I<upuf> ist "
"ein Zeiger auf eine I<ustat>-Struktur, welche die folgenden Elemente enthält:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"daddr_t f_tfree;      /* Total free blocks */\n"
"ino_t   f_tinode;     /* Number of free inodes */\n"
"char    f_fname[6];   /* Filsys name */\n"
"char    f_fpack[6];   /* Filsys pack name */\n"
msgstr ""
"daddr_t\tf_tfree;\t/* Gesamtzahl der freien Blöcke */\n"
"ino_t\tf_tinode;\t/* Anzahl der freien Inodes */\n"
"char\tf_fname[6];\t/* Name des Dateisystems */\n"
"char\tf_fpack[6];\t/* Name des gepackten Dateisystems [?] */\n"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"The last two fields, I<f_fname> and I<f_fpack>, are not implemented and will "
"always be filled with null bytes (\\[aq]\\[rs]0\\[aq])."
msgstr ""
"Die letzten beiden Felder, I<f_fname> und I<f_fpack>, sind nicht "
"implementiert und werden immer mit Nullbytes (»\\[rs]0«) gefüllt."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "RÜCKGABEWERT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On success, zero is returned and the I<ustat> structure pointed to by "
"I<ubuf> will be filled in.  On error, -1 is returned, and I<errno> is set to "
"indicate the error."
msgstr ""
"Bei Erfolg wird Null zurückgegeben und die durch I<upuf> bestimmte I<ustat>-"
"Struktur wird ausgefüllt. Bei einem Fehler wird -1 zurückgegeben und "
"I<errno> gesetzt, um den Fehler anzuzeigen."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "FEHLER"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<ubuf> points outside of your accessible address space."
msgstr "I<upuf> zeigt aus Ihrem adressierbaren Adressraum heraus."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<dev> does not refer to a device containing a mounted filesystem."
msgstr ""
"I<gerät> verweist nicht auf ein Gerät, das ein eingehängtes Dateisystem "
"enthält."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOSYS>"
msgstr "B<ENOSYS>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The mounted filesystem referenced by I<dev> does not support this operation, "
"or any version of Linux before Linux 1.3.16."
msgstr ""
"Das eingehängte Dateisystem, auf das von I<gerät> verwiesen wird, "
"unterstützt diese Operation nicht, desgleichen auch keine Linux-Version vor "
"1.3.16."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "None."
msgstr "Keine."

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "GESCHICHTE"

#.  SVr4 documents additional error conditions ENOLINK, ECOMM, and EINTR
#.  but has no ENOSYS condition.
#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "SVr4.  Removed in glibc 2.28."
msgstr "SVr4. Wurde in Glibc 2.28 entfernt."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<ustat>()  is deprecated and has been provided only for compatibility.  All "
"new programs should use B<statfs>(2)  instead."
msgstr ""
"B<ustat>() ist veraltet und wurde nur zu Kompatibilitätszwecken "
"bereitgestellt. Alle neuen Programme sollten stattdessen B<statfs>(2) "
"benutzen."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HP-UX notes"
msgstr "Anmerkungen zu HP-UX"

#.  Some software tries to use this in order to test whether the
#.  underlying filesystem is NFS.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The HP-UX version of the I<ustat> structure has an additional field, "
"I<f_blksize>, that is unknown elsewhere.  HP-UX warns: For some filesystems, "
"the number of free inodes does not change.  Such filesystems will return -1 "
"in the field I<f_tinode>.  For some filesystems, inodes are dynamically "
"allocated.  Such filesystems will return the current number of free inodes."
msgstr ""
"Die HP-UX-Version der I<ustat>-Struktur enthält ein zusätzliches "
"Feld,I<f_blksize>, das andernorts nicht bekannt ist. HP-UX warnt: Für manche "
"Dateisysteme ändert sich die Anzahl der freien Inodes nicht. Solche "
"Dateisysteme werden im Feld I<f_tinode> -1 zurückgeben. Für einige "
"Dateisysteme werden die Inodes dynamisch bereitgestellt. Diese Dateisysteme "
"geben die aktuelle Anzahl freier Inodes zurück."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<stat>(2), B<statfs>(2)"
msgstr "B<stat>(2), B<statfs>(2)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5. Februar 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Standard-C-Bibliothek (I<libc>, I<-lc>)"

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"The last two fields, I<f_fname> and I<f_fpack>, are not implemented and will "
"always be filled with null bytes (\\[aq]\\e0\\[aq])."
msgstr ""
"Die letzten beiden Felder, I<f_fname> und I<f_fpack>, sind nicht "
"implementiert und werden immer mit Nullbytes (»\\e0«) gefüllt."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONEN"

#. type: Plain text
#: debian-bookworm
msgid ""
"Since glibc 2.28, glibc no longer provides a wrapper for this system call."
msgstr ""
"Seit Version 2.28 stellt Glibc keinen Wrapper mehr für diesen Systemaufruf "
"bereit."

#.  SVr4 documents additional error conditions ENOLINK, ECOMM, and EINTR
#.  but has no ENOSYS condition.
#. type: Plain text
#: debian-bookworm
msgid "SVr4."
msgstr "SVr4."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NOTES"
msgstr "ANMERKUNGEN"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "2024-06-15"
msgstr "15. Juni 2024"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Linux man-pages 6.9.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31. Oktober 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Linux man-pages (unveröffentlicht)"
