# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Helge Kreutzmann <debian@helgefjell.de>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.21.0\n"
"POT-Creation-Date: 2025-02-16 05:55+0100\n"
"PO-Revision-Date: 2024-01-10 21:31+0100\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: Dd
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "January 9, 1999"
msgstr "9. Januar 1999"

#. type: Dt
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "POM 6"
msgstr "POM 6"

#. type: Sh
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "E<.Nm pom>"
msgstr "E<.Nm pom>"

#. type: Nd
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "display the phase of the moon"
msgstr "Anzeige der Mondphase"

#. type: Sh
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "E<.Nm> E<.Op [[[[[cc]yy]mm]dd]HH]>"
msgstr "E<.Nm> E<.Op [[[[[cc]yy]mm]dd]HH]>"

#. type: Sh
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"The E<.Nm> utility displays the current phase of the moon.  Useful for "
"selecting software completion target dates and predicting managerial "
"behavior."
msgstr ""
"Das Hilfswerkzeug E<.Nm> zeigt die aktuelle Phase des Mondes an. Nützlich "
"für die Auswahl von Daten zur Fertigstellung von Software oder der "
"Vorhersage von Manager-Verhalten."

#. type: It
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "Ar [[[[[cc]yy]mm]dd]HH]"
msgstr "Ar [[[[[cc]yy]mm]dd]HH]"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Display the phase of the moon for a given time.  The format is similar to "
"the canonical representation used by E<.Xr date 1>."
msgstr ""
"Zeigt die Mondphase für eine angegebene Zeit an. Das Format ist ähnlich dem "
"der von E<.Xr date 1> verwandten kanonischen Darstellung."

#. type: Sh
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "E<.Xr date 1>"
msgstr "E<.Xr date 1>"

#. type: Sh
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "E<.Nm> was written by E<.An Keith E. Brandt>."
msgstr "E<.Nm> wurde von E<.An Keith E. Brandt> geschrieben."

#. type: Sh
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "FEHLER"

# FIXME Epoch is no range, but a point in time: Only times after starting the E<.Ux> epoch (january 1, 1970, 00:00 UTC will be accepted
#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Times must be within range of the E<.Ux> epoch."
msgstr "Zeiten müssen innerhalb des Bereichs der E<.Ux>-Epoch sein."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"This program does not allow for the difference between the TDT and UTC "
"timescales (about one minute at the time of writing)."
msgstr ""
"Dieses Programm ermöglicht nicht die Unterscheidung zwischen den TDT- und "
"UTC-Zeitskalen (zum Zeitpunkt der Erstellung ungefähr eine Minute)."

#. type: Sh
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "ACKNOWLEDGEMENTS"
msgstr "DANKSAGUNGEN"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"This program is based on algorithms from E<.%B Practical Astronomy with Your "
"Calculator, Third Edition> by Peter Duffett-Smith E<.Aq pjds@mrao.cam.ac.uk>."
msgstr ""
"Dieses Programm basiert auf Algorithmen aus E<.%B Practical Astronomy with "
"Your Calculator, Dritte Auflage> von Peter Duffett-Smith E<.Aq "
"pjds@mrao.cam.ac.uk>."
