# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Helge Kreutzmann <debian@helgefjell.de>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.2.0\n"
"POT-Creation-Date: 2023-06-27 19:52+0200\n"
"PO-Revision-Date: 2021-08-17 16:10+0200\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: Dd
#: debian-bookworm debian-unstable
#, no-wrap
msgid "September 7, 2001"
msgstr "7. September 2001"

#. type: Dt
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SSH-ARGV0 1"
msgstr "SSH-ARGV0 1"

# FIXME (po4a) siehe https://lists.debian.org/debian-l10n-german/2020/12/msg00108.html
#. type: Os
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Debian"
msgstr "Debian"

#. type: Os
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Project"
msgstr "Projekt"

#. type: Sh
#: debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "E<.Nm ssh-argv0>"
msgstr "E<.Nm ssh-argv0>"

#. type: Nd
#: debian-bookworm debian-unstable
#, no-wrap
msgid "replaces the old ssh command-name as hostname handling"
msgstr "ersetzt die alte »Befehlsname als Rechnername«-Handhabung von SSH"

#. type: Sh
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"E<.Ar hostname | user@hostname> E<.Op Fl l Ar login_name> E<.Op Ar command>"
msgstr ""
"E<.Ar Rechnername | Benutzer@Rechnername> E<.Op Fl l Ar Anmeldename> E<.Op "
"Ar Befehl>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"E<.Ar hostname | user@hostname> E<.Op Fl afgknqstvxACNTX1246> E<.Op Fl b Ar "
"bind_address> E<.Op Fl c Ar cipher_spec> E<.Op Fl e Ar escape_char> E<.Op Fl "
"i Ar identity_file> E<.Op Fl l Ar login_name> E<.Op Fl m Ar mac_spec> E<.Op "
"Fl o Ar option> E<.Op Fl p Ar port> E<.Op Fl F Ar configfile> E<.Oo Fl L Xo> "
"E<.Sm off> E<.Ar port>: E<.Ar host>: E<.Ar hostport> E<.Sm on> E<.Xc> E<.Oc> "
"E<.Oo Fl R Xo> E<.Sm off> E<.Ar port>: E<.Ar host>: E<.Ar hostport> E<.Sm "
"on> E<.Xc> E<.Oc> E<.Op Fl D Ar port> E<.Op Ar command>"
msgstr ""
"E<.Ar Rechnername | Benutzer@Rechnername> E<.Op Fl afgknqstvxACNTX1246> "
"E<.Op Fl b Ar Bindadresse> E<.Op Fl c Ar Chiffrespez> E<.Op Fl e Ar "
"Maskierzeichen> E<.Op Fl i Ar Identitätsdatei> E<.Op Fl l Ar Anmeldename> "
"E<.Op Fl m Ar MAC-Spez> E<.Op Fl o Ar Option> E<.Op Fl p Ar Port> E<.Op Fl F "
"Ar Konfigdatei> E<.Oo Fl L Xo> E<.Sm off> E<.Ar Port>: E<.Ar Rechner>: E<.Ar "
"Rechnerport> E<.Sm on> E<.Xc> E<.Oc> E<.Oo Fl R Xo> E<.Sm off> E<.Ar Port>: "
"E<.Ar Rechner>: E<.Ar Rechnerport> E<.Sm on> E<.Xc> E<.Oc> E<.Op Fl D Ar "
"Port> E<.Op Ar Befehl>"

#. type: Sh
#: debian-bookworm debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"E<.Nm> replaces the old ssh command-name as hostname handling.  If you link "
"to this script with a hostname then executing the link is equivalent to "
"having executed ssh with that hostname as an argument.  All other arguments "
"are passed to ssh and will be processed normally."
msgstr ""
"E<.Nm> ersetzt die alte »Befehlsname als Rechnername«-Handhabung. Falls Sie "
"auf dieses Skript einen Link mit dem Namen des Rechners setzen, dann ist die "
"Ausführung des Links äquivalent zu der Ausführung von SSH mit diesem "
"Rechnernamen als Argument. Alle anderen Argumente werden an SSH "
"weitergegeben und normal verarbeitet."

#. type: Sh
#: debian-bookworm debian-unstable
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONEN"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "See E<.Xr ssh 1>."
msgstr "Siehe E<.Xr ssh 1>."

#. type: Sh
#: debian-bookworm debian-unstable
#, no-wrap
msgid "FILES"
msgstr "DATEIEN"

#. type: Sh
#: debian-bookworm debian-unstable
#, no-wrap
msgid "AUTHORS"
msgstr "AUTOREN"

# WONTFIX The other ssh manpages use the macro .An to denote author names, this is missing here 
#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"OpenSSH is a derivative of the original and free ssh 1.2.12 release by Tatu "
"Ylonen.  Aaron Campbell, Bob Beck, Markus Friedl, Niels Provos, Theo de "
"Raadt and Dug Song removed many bugs, re-added newer features and created "
"OpenSSH.  Markus Friedl contributed the support for SSH protocol versions "
"1.5 and 2.0.  Natalie Amery wrote this ssh-argv0 script and the associated "
"documentation."
msgstr ""
"OpenSSH ist eine Ableitung der ursprünglichen und freien SSH-1.2.12-"
"Veröffentlichung durch E<.An Tatu Ylonen>. E<.An Aaron Campbell, Bob Beck, "
"Markus Friedl, Niels Provos, Theo de Raadt> und E<.An Dug Song> entfernten "
"viele Fehler, fügten neue Funktionalitäten wieder hinzu und erstellten "
"OpenSSH. E<.An Markus Friedl> steuerte die Unterstützung für SSH-"
"Protokollversion 1.5 und 2.0 bei. E<.An Natalie Amery> schrieb dieses Skript "
"ssh-argv0 und die zugehörige Dokumentation."

#. type: Sh
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "E<.Xr ssh 1>"
msgstr "E<.Xr ssh 1>"
