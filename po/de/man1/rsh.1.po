# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Helge Kreutzmann <debian@helgefjell.de>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.19.0\n"
"POT-Creation-Date: 2024-02-09 17:07+0100\n"
"PO-Revision-Date: 2023-06-08 09:26+0200\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux
#, no-wrap
msgid "RSH"
msgstr "RSH"

#. type: TH
#: archlinux
#, no-wrap
msgid "December 2023"
msgstr "Dezember 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "GNU inetutils 2.5"
msgstr "GNU inetutils 2.5"

#. type: TH
#: archlinux
#, no-wrap
msgid "User Commands"
msgstr "Dienstprogramme für Benutzer"

#. type: SH
#: archlinux
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux
msgid "rsh - Remote shell client"
msgstr "rsh - Client für ferne Shells"

#. type: SH
#: archlinux
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux
msgid ""
"B<rsh> [I<\\,OPTION\\/>...] [I<\\,USER@\\/>]I<\\,HOST \\/>[I<\\,COMMAND \\/"
">[I<\\,ARG\\/>...]]"
msgstr ""
"B<rsh> [I<\\,OPTION\\/>…] [I<\\,BENUTZER@\\/>]I<\\,RECHNER \\/>[I<\\,BEFEHL "
"\\/>[I<\\,ARG\\/>…]]"

#. type: SH
#: archlinux
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux
msgid "remote shell"
msgstr "ferne Shell"

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-4>, B<--ipv4>"
msgstr "B<-4>, B<--ipv4>"

#. type: Plain text
#: archlinux
msgid "use only IPv4"
msgstr "verwendet nur IPv4."

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-6>, B<--ipv6>"
msgstr "B<-6>, B<--ipv6>"

#. type: Plain text
#: archlinux
msgid "use only IPv6"
msgstr "verwendet nur IPv6."

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-8>, B<--8-bit>"
msgstr "B<-8>, B<--8-bit>"

#. type: Plain text
#: archlinux
msgid "allows an eight-bit input data path at all times"
msgstr "Erlaubt zu jeder Zeit einen achtbittigen Eingabepfad."

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-d>, B<--debug>"
msgstr "B<-d>, B<--debug>"

# WONTFIX setsockopt → B<setsockopt> // Different style convention
#. type: Plain text
#: archlinux
msgid "turns on socket debugging (see setsockopt(2))"
msgstr "Schaltet die Fehlersuche bei Sockets ein (siehe B<setsockopt>(2))."

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-e>, B<--escape>=I<\\,CHAR\\/>"
msgstr "B<-e>, B<--escape>=I<\\,ZEICHEN\\/>"

#. type: Plain text
#: archlinux
msgid "allows user specification of the escape character (``~'' by default)"
msgstr ""
"Ermöglicht die Festlegung des Maskierzeichens (standardmäßig »~«) durch den "
"Benutzer."

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-l>, B<--user>=I<\\,USER\\/>"
msgstr "B<-l>, B<--user>=I<\\,BENUTZER\\/>"

#. type: Plain text
#: archlinux
msgid "run as USER on the remote system"
msgstr "Ausführung als BENUTZER auf dem fernen System."

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-n>, B<--no-input>"
msgstr "B<-n>, B<--no-input>"

#. type: Plain text
#: archlinux
msgid "use I<\\,/dev/null\\/> as input"
msgstr "Verwendet I<\\,/dev/null\\/> als Eingabe."

# FIXME -? → B<-?>
#. type: TP
#: archlinux
#, no-wrap
msgid "-?, B<--help>"
msgstr "B<-?>, B<--help>"

#. type: Plain text
#: archlinux
msgid "give this help list"
msgstr "gibt eine kurze Hilfe aus."

#. type: TP
#: archlinux
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: archlinux
msgid "give a short usage message"
msgstr "gibt eine kurze Meldung zur Verwendung aus."

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux
msgid "print program version"
msgstr "gibt die Programmversion aus."

#. type: Plain text
#: archlinux
msgid ""
"Mandatory or optional arguments to long options are also mandatory or "
"optional for any corresponding short options."
msgstr ""
"Erforderliche oder optionale Argumente für lange Optionen sind ebenso "
"erforderlich bzw. optional für die entsprechenden Kurzoptionen."

#. type: SH
#: archlinux
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux
msgid "Written by many authors."
msgstr "Geschrieben von vielen Autoren."

#. type: SH
#: archlinux
#, no-wrap
msgid "REPORTING BUGS"
msgstr "FEHLER MELDEN"

#. type: Plain text
#: archlinux
msgid "Report bugs to E<lt>bug-inetutils@gnu.orgE<gt>."
msgstr ""
"Melden Sie Fehler (auf Englisch) an E<.MT bug-inetutils@gnu.org> E<.ME .>"

#. type: SH
#: archlinux
#, no-wrap
msgid "COPYRIGHT"
msgstr "COPYRIGHT"

#. type: Plain text
#: archlinux
msgid ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2023 Free Software Foundation, Inc. Lizenz GPLv3+: E<.UR "
"https://gnu.org/licenses/gpl.html> GNU GPL Version 3 E<.UE> oder neuer."

#. type: Plain text
#: archlinux
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Dies ist freie Software: Sie können sie verändern und weitergeben. Es gibt "
"KEINE GARANTIE, soweit gesetzlich zulässig."

#. type: SH
#: archlinux
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

# WONTFIX rshd(1) → B<rshd>(1) // different style convention
#. type: Plain text
#: archlinux
msgid "rshd(1)"
msgstr "B<rshd>(1)"

# WONTFIX B<info> → B<info>(1) // Comes from help2man, does not use section numbers
#. type: Plain text
#: archlinux
msgid ""
"The full documentation for B<rsh> is maintained as a Texinfo manual.  If the "
"B<info> and B<rsh> programs are properly installed at your site, the command"
msgstr ""
"Die vollständige Dokumentation für B<rsh> wird als ein Texinfo-Handbuch "
"gepflegt. Wenn die Programme B<info>(1) und B<rsh> auf Ihrem Rechner "
"ordnungsgemäß installiert sind, können Sie mit dem Befehl"

#. type: Plain text
#: archlinux
msgid "B<info rsh>"
msgstr "B<info rsh>"

#. type: Plain text
#: archlinux
msgid "should give you access to the complete manual."
msgstr "auf das vollständige Handbuch zugreifen."
