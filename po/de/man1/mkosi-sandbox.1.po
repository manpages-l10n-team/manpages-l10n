# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Helge Kreutzmann <debian@helgefjell.de>, 2025.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.25.1\n"
"POT-Creation-Date: 2025-02-14 20:38+0100\n"
"PO-Revision-Date: 2025-03-03 06:58+0100\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "mkosi-sandbox"
msgstr "mkosi-sandbox"

#. type: SH
#: archlinux debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
msgid "mkosi-sandbox \\[em] Run commands in a custom sandbox"
msgstr "mkosi-sandbox \\[en] Befehle in einer angepassten Sandbox ausführen"

#. type: SH
#: archlinux debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
msgid "\\f[CR]mkosi-sandbox [options\\&...] command [arguments]\\fR"
msgstr "\\f[CR]mkosi-sandbox [Optionen…] Befehl [Argumente]\\fR"

#. type: SH
#: archlinux debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

# FIXME \\f[CR]mkosi-sandbox\\f[R] → \\f[B]mkosi-sandbox\\f[R]
#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
msgid ""
"\\f[CR]mkosi-sandbox\\f[R] runs the given command in a custom sandbox.  The "
"sandbox is configured by specifying command line options that configure "
"individual parts of the sandbox.\\fR"
msgstr ""
"\\f[B]mkosi-sandbox\\f[R] führt den angegebenen Befehl in einer angepassten "
"Sandbox aus. Diese Sandbox wird durch die Angabe von Befehlszeilenoptionen "
"für bestimmte Teile der Sandbox konfiguriert.\\fR"

# FIXME \\f[CR]mkosi-sandbox\\f[R] → \\f[B]mkosi-sandbox\\f[R]
# FIXME \\f[CR]bash\\f[R] → \\f[B]bash\\f[R](1)
#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
msgid ""
"If no command is specified, \\f[CR]mkosi-sandbox\\f[R] will start "
"\\f[CR]bash\\f[R] in the sandbox.\\fR"
msgstr ""
"Falls kein Befehl angegeben ist, wird \\f[B]mkosi-sandbox\\f[R] "
"\\f[B]bash\\f[R](1) in der Sandbox starten."

#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
msgid ""
"Note that this sandbox is not designed to be a security boundary.  Its "
"intended purpose is to allow running commands in an isolated environment so "
"they are not affected by the host system."
msgstr ""
"Beachten Sie, dass diese Sandbox nicht als Sicherheitsgrenze entwickelt "
"wurde. Sie ist zur Ausführung von Befehlen in einer isolierten Umgebung "
"gedacht, so dass diese nicht vom Wirtsystem beeinflusst werden."

#. type: SH
#: archlinux debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONEN"

#. type: TP
#: archlinux debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "\\f[CR]--tmpfs DST\\fR"
msgstr "\\f[CR]--tmpfs ZIEL\\fR"

#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
msgid "Mounts a new tmpfs at \\f[CR]DST\\f[R] in the sandbox.\\fR"
msgstr "Hängt ein neues Tmpfs unter \\f[CR]ZIEL\\f[R] in der Sandbox ein.\\fR"

#. type: TP
#: archlinux debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "\\f[CR]--dev DST\\fR"
msgstr "\\f[CR]--dev ZIEL\\fR"

#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
msgid ""
"Sets up a private \\f[CR]/dev\\f[R] at \\f[CR]DST\\f[R] in the sandbox.  "
"This private \\f[CR]/dev\\f[R] will only contain the basic device nodes "
"required for a functioning sandbox (e.g.\\ \\f[CR]/dev/null\\f[R]) and no "
"actual devices.\\fR"
msgstr ""
"Richtet ein privates \\f[CR]/dev\\f[R] unter \\f[CR]ZIEL\\f[R] in der "
"Sandbox ein. Dieses private \\f[CR]/dev\\f[R] wird nur die grundlegenden "
"Geräteknoten beinhalten, die für eine funktionierende Sandbox notwendig sind "
"(z.B.\\ \\f[CR]/dev/null\\f[R]) und keine tatsächlichen Geräte.\\fR"

#. type: TP
#: archlinux debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "\\f[CR]--proc DST\\fR"
msgstr "\\f[CR]--proc ZIEL\\fR"

#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
msgid ""
"Mounts \\f[CR]/proc\\f[R] from the host at \\f[CR]DST\\f[R] in the sandbox."
"\\fR"
msgstr ""
"Hängt \\f[CR]/proc\\f[R] des Wirtsystems unter \\f[CR]ZIEL\\f[R] in der "
"Sandbox ein.\\fR"

#. type: TP
#: archlinux debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "\\f[CR]--dir DST\\fR"
msgstr "\\f[CR]--dir ZIEL\\fR"

#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
msgid ""
"Creates a directory and all missing parent directories at \\f[CR]DST\\f[R] "
"in the sandbox.  All directories are created with mode 755 unless the path "
"ends with \\f[CR]/tmp\\f[R] or \\f[CR]/var/tmp\\f[R] in which case it is "
"created with mode 1777.\\fR"
msgstr ""
"Erstellt ein Verzeichnis und alle übergeordneten Verzeichnisse unter "
"\\f[CR]ZIEL\\f[R] in der Sandbox. Alle Verzeichnisse werden mit Modus 755 "
"erstellt, außer der Pfad endet auf \\f[CR]/tmp\\f[R] oder \\f[CR]/var/"
"tmp\\f[R], dann wird es mit Modus 1777 erstellt.\\fR"

#. type: TP
#: archlinux debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "\\f[CR]--bind SRC DST\\fR"
msgstr "\\f[CR]--bind QUELLE ZIEL\\fR"

#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
msgid ""
"The source path \\f[CR]SRC\\f[R] is recursively bind mounted to "
"\\f[CR]DST\\f[R] in the sandbox.  The mountpoint is created in the sandbox "
"if it does not yet exist.  Any missing parent directories in the sandbox are "
"created as well.\\fR"
msgstr ""
"Der Quellpfad \\f[CR]QUELLE\\f[R] wird rekursiv unter \\f[CR]ZIEL\\f[R] in "
"der Sandbox bind-eingehängt. Der Einhängepunkt wird in der Sandbox erstellt, "
"falls er nicht existiert. Es werden auch alle fehlenden Überverzeichnisse in "
"der Sandbox erstellt."

#. type: TP
#: archlinux debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "\\f[CR]--bind-try SRC DST\\fR"
msgstr "\\f[CR]--bind-try QUELLE ZIEL\\fR"

#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
msgid ""
"Like \\f[CR]--bind\\f[R], but doesn\\[cq]t fail if the source path doesn\\"
"[cq]t exist.\\fR"
msgstr ""
"Ähnlich wie \\f[CR]--bind\\f[R], schlägt aber nicht fehl, wenn der Quellpfad "
"nicht existiert.\\fR"

#. type: TP
#: archlinux debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "\\f[CR]--ro-bind SRC DST\\fR"
msgstr "\\f[CR]--ro-bind QUELLE ZIEL\\fR"

#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
msgid "Like \\f[CR]--bind\\f[R], but does a recursive readonly bind mount.\\fR"
msgstr ""
"Wie \\f[CR]--bind\\f[R], führt aber eine schreibgeschützte rekursive Bind-"
"Einhängung durch.\\fR"

#. type: TP
#: archlinux debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "\\f[CR]--ro-bind-try SRC DST\\fR"
msgstr "\\f[CR]--ro-bind-try QUELLE ZIEL\\fR"

#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
msgid ""
"Like \\f[CR]--bind-try\\f[R], but does a recursive readonly bind mount.\\fR"
msgstr ""
"Wie \\f[CR]--bind-try\\f[R], führt aber eine schreibgeschützte rekursive "
"Bind-Einhängung durch.\\fR"

#. type: TP
#: archlinux debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "\\f[CR]--symlink SRC DST\\fR"
msgstr "\\f[CR]--symlink QUELLE ZIEL\\fR"

#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
msgid ""
"Creates a symlink at \\f[CR]DST\\f[R] in the sandbox pointing to "
"\\f[CR]SRC\\f[R].  If \\f[CR]DST\\f[R] already exists and is a file or "
"symlink, a temporary symlink is created and mounted on top of "
"\\f[CR]DST\\f[R].\\fR"
msgstr ""
"Erstellt einen Symlink unter \\f[CR]ZIEL\\f[R] in der Sandbox, der auf "
"\\f[CR]QUELLE\\f[R] zeigt. Falls \\f[CR]ZIEL\\f[R] bereits existiert und "
"eine Datei oder ein Symlink ist, wird ein temporärer Symlink erstellt und "
"auf \\f[CR]ZIEL\\f[R] eingehängt.\\fR"

#. type: TP
#: archlinux debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "\\f[CR]--write DATA DST\\fR"
msgstr "\\f[CR]--write DATEN ZIEL\\fR"

#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
msgid ""
"Writes the string from \\f[CR]DATA\\f[R] to \\f[CR]DST\\f[R] in the sandbox."
"\\fR"
msgstr ""
"Schreibt die Zeichenkette aus \\f[CR]DATEN\\f[R] an \\f[CR]ZIEL\\f[R] in der "
"Sandbox.\\fR"

#. type: TP
#: archlinux debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "\\f[CR]--overlay-lowerdir DIR\\fR"
msgstr "\\f[CR]--overlay-lowerdir VERZ\\fR"

#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
msgid ""
"Adds \\f[CR]DIR\\f[R] from the host as a new lower directory for the next "
"overlayfs mount.\\fR"
msgstr ""
"Fügt \\f[CR]VERZ\\f[R] vom Wirt als neues unteres Verzeichnis für die "
"nächste Overlayfs-Einhängung hinzu.\\fR"

#. type: TP
#: archlinux debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "\\f[CR]--overlay-upperdir DIR\\fR"
msgstr "\\f[CR]--overlay-upperdir VERZ\\fR"

# FIXME workdir → working directory   (as in next string)
#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
msgid ""
"Sets the upper directory for the next overlayfs mount to \\f[CR]DIR\\f[R] "
"from the host.  If set to \\f[CR]tmpfs\\f[R], the upperdir and workdir will "
"be subdirectories of a fresh tmpfs mount.\\fR"
msgstr ""
"Setzt das obere Verzeichnis für die nächste Overlayfs-Einhängung auf "
"\\f[CR]VERZ\\f[R] vom Wirt. Falls auf \\f[CR]tmpfs\\f[R] gesetzt, werden das "
"obere und das Arbeitsverzeichnis Unterverzeichnisse der frischen Tmpfs-"
"Einhängung sein.\\fR"

#. type: TP
#: archlinux debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "\\f[CR]--overlay-workdir DIR\\fR"
msgstr "\\f[CR]--overlay-workdir VERZ\\fR"

#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
msgid ""
"Sets the working directory for the next overlayfs mount to \\f[CR]DIR\\f[R] "
"from the host.\\fR"
msgstr ""
"Setzt das Arbeitsverzeichnis für die nächste Overlayfs-Einhängung auf "
"\\f[CR]VERZ\\f[R] von dem Wirt.\\fR"

#. type: TP
#: archlinux debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "\\f[CR]--overlay DST\\fR"
msgstr "\\f[CR]--overlay ZIEL\\fR"

#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
msgid ""
"Mounts a new overlay filesystem at \\f[CR]DST\\f[R] in the sandbox.  The "
"lower directories, upper directory and working directory are specified using "
"the \\f[CR]--overlay-lowerdir\\f[R], \\f[CR]--overlay-upperdir\\f[R] and "
"\\f[CR]--overlay-workdir\\f[R] options respectively.  After each \\f[CR]--"
"overlay\\f[R] option is parsed, the other overlay options are reset.\\fR"
msgstr ""
"Hängt ein neues Overlay-Dateisystem unter \\f[CR]ZIEL\\f[R] in die Sandbox "
"ein. Die unteren Verzeichnisse, das obere und das Arbeitsverzeichnis werden "
"mittels der Optionen \\f[CR]--overlay-lowerdir\\f[R], \\f[CR]--overlay-"
"upperdir\\f[R] bzw. \\f[CR]--overlay-workdir\\f[R] festgelegt. Nachdem jede "
"Option \\f[CR]--overlay\\f[R] ausgewertet wurde, werden die anderen Overlay-"
"Optionen zurückgesetzt.\\fR"

#. type: TP
#: archlinux debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "\\f[CR]--unsetenv NAME\\fR"
msgstr "\\f[CR]--unsetenv NAME\\fR"

#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
msgid "Unsets the \\f[CR]NAME\\f[R] environment variable in the sandbox.\\fR"
msgstr ""
"Entfernt eine in der Sandbox gesetzte Umgebungsvariable \\f[CR]NAME\\f[R]."
"\\fR"

#. type: TP
#: archlinux debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "\\f[CR]--setenv NAME VALUE\\fR"
msgstr "\\f[CR]--setenv NAME WERT\\fR"

# FIXME Missing full stop
#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
msgid ""
"Sets the \\f[CR]NAME\\f[R] environment variable to \\f[CR]VALUE\\f[R] in the "
"sandbox\\fR"
msgstr ""
"Setzt in der Sandbox die Umgebungsvariable \\f[CR]NAME\\f[R] auf "
"\\f[CR]WERT\\f[R].\\fR"

#. type: TP
#: archlinux debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "\\f[CR]--chdir DIR\\fR"
msgstr "\\f[CR]--chdir VERZ\\fR"

#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
msgid "Changes the working directory to \\f[CR]DIR\\f[R] in the sandbox.\\fR"
msgstr ""
"Ändert das Arbeitsverzeichnis in der Sandbox auf \\f[CR]VERZ\\f[R].\\fR"

#. type: TP
#: archlinux debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "\\f[CR]--same-dir\\fR"
msgstr "\\f[CR]--same-dir\\fR"

# FIXME \\f[CR]mkosi-sandbox\\f[R] → \\f[B]mkosi-sandbox\\f[R]
#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
msgid ""
"Changes to the working directory in the sandbox to the current working "
"directory that \\f[CR]mkosi-sandbox\\f[R] is invoked in on the host.\\fR"
msgstr ""
"Ändert das Arbeitsverzeichnis in der Sandbox auf das aktuelle "
"Arbeitsverzeichnis, in dem \\f[B]mkosi-sandbox\\f[R] auf dem Wirt aufgerufen "
"wurde.\\fR"

#. type: TP
#: archlinux debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "\\f[CR]--become-root\\fR"
msgstr "\\f[CR]--become-root\\fR"

#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
msgid ""
"Maps the current user to the root user in the sandbox.  If this option is "
"not specified, the current user is mapped to itself in the sandbox.  "
"Regardless of whether this option is specified or not, the current user will "
"have a full set of ambient capabilities in the sandbox.  This includes "
"\\f[CR]CAP_SYS_ADMIN\\f[R] which means that the invoked process in the "
"sandbox will be able to do bind mounts and other operations.\\fR"
msgstr ""
"Bildet den aktuellen Benutzer auf den Benutzer »root« in der Sandbox ab. "
"Falls diese Option nicht angegeben ist, wird der aktuelle Benutzer in der "
"Sandbox auf sich selbst abgebildet. Unabhängig von der Angabe dieser Option "
"wird der aktuelle Benutzer einen vollständigen Satz an Umgebungs-Capabilitys "
"in der Sandbox haben. Dazu gehört \\f[CR]CAP_SYS_ADMIN\\f[R], was bedeutet, "
"dass der aufgerufene Prozess in der Sandbox in der Lage sein wird, Bind-"
"Einhängungen und andere Aktionen durchzuführen.\\fR"

# FIXME \\f[CR]mkosi-sandbox\\f[R] → \\f[B]mkosi-sandbox\\f[R]
#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
msgid ""
"If \\f[CR]mkosi-sandbox\\f[R] is invoked as the root user, this option won\\"
"[cq]t do anything.\\fR"
msgstr ""
"Falls \\f[B]mkosi-sandbox\\f[R] als Benutzer root aufgerufen wird, macht "
"diese Option nichts.\\fR"

#. type: TP
#: archlinux debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "\\f[CR]--suppress-chown\\fR"
msgstr "\\f[CR]--suppress-chown\\fR"

# FIXME \\f[CR]mkosi-sandbox\\f[R] → \\f[B]mkosi-sandbox\\f[R]
# FIXME \\f[CR]chown()\\f[R] → \\f[B]chown\\f[R](2) ?
#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
msgid ""
"Specifying this option causes all calls to \\f[CR]chown()\\f[R] or similar "
"system calls to become a noop in the sandbox.  This is primarily useful when "
"invoking package managers in the sandbox which might try to \\f[CR]chown()"
"\\f[R] files to different users or groups which would fail unless "
"\\f[CR]mkosi-sandbox\\f[R] is invoked by a privileged user.\\fR"
msgstr ""
"Durch Angabe dieser Option werden alle Aufrufe von \\f[CR]chown()\\f[R] oder "
"ähnliche Systemaufrufe in der Sandbox wirkungslos. Dies ist hauptsächlich "
"für den Aufruf von Paketverwaltern in der Sandbox nützlich, da diese "
"versuchen könnten, Dateien mittels \\f[CR]chown()\\f[R] auf andere Benutzer "
"oder Gruppen zu ändern und dies fehlschlagen könnte, wenn \\f[B]mkosi-"
"sandbox\\f[R] nicht als privilegierter Benutzer aufgerufen wird.\\fR"

#. type: TP
#: archlinux debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "\\f[CR]--unshare-net\\fR"
msgstr "\\f[CR]--unshare-net\\fR"

# FIXME \\f[CR]mkosi-sandbox\\f[R] → \\f[B]mkosi-sandbox\\f[R]
#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
msgid ""
"Specifying this option makes \\f[CR]mkosi-sandbox\\f[R] unshare a network "
"namespace if possible.\\fR"
msgstr ""
"Durch Angabe dieser Option trennt \\f[B]mkosi-sandbox\\f[R] wenn möglich "
"einen Netzwerknamensraum ab.\\fR"

#. type: TP
#: archlinux debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "\\f[CR]--unshare-ipc\\fR"
msgstr "\\f[CR]--unshare-ipc\\fR"

# FIXME \\f[CR]mkosi-sandbox\\f[R] → \\f[B]mkosi-sandbox\\f[R]
#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
msgid ""
"Specifying this option makes \\f[CR]mkosi-sandbox\\f[R] unshare an IPC "
"namespace if possible.\\fR"
msgstr ""
"Durch Angabe dieser Option trennt \\f[B]mkosi-sandbox\\f[R] wenn möglich "
"einen IPC-Namensraum ab.\\fR"

# FIXME FD should be in italics
#. type: TP
#: archlinux debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "\\f[CR]--exec-fd FD\\fR"
msgstr "\\f[CR]--exec-fd DD\\fR"

# FIXME \\f[CR]mkosi-sandbox\\f[R] → \\f[B]mkosi-sandbox\\f[R]
# FIXME FD should be in italics
# FIXME \\f[CR]execvp()\\f[R] → \\f[B]execvp\\f[R](3) ?
#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
msgid ""
"The specified \\f[CR]FD\\f[R] will be closed when \\f[CR]mkosi-sandbox\\f[R] "
"calls \\f[CR]execvp()\\f[R].  This is useful to wait until all setup logic "
"has completed before continuing execution in the parent process invoking "
"\\f[CR]mkosi-sandbox\\f[R].\\fR"
msgstr ""
"Der angegebene \\f[CR]DD\\f[R] wird geschlossen, wenn \\f[B]mkosi-"
"sandbox\\f[R] \\f[CR]execvp()\\f[R] aufruft. Dies ist nützlich, damit auf "
"die Beendigung sämtlicher Einrichtungslogik gewartet wird, bevor im "
"übergeordneten Prozess, der \\f[B]mkosi-sandbox\\f[R] aufgerufen hat, "
"fortgefahren wird.\\fR"

#. type: TP
#: archlinux debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "\\f[CR]--version\\fR"
msgstr "\\f[CR]--version\\fR"

#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
msgid "Show package version."
msgstr "Zeigt die Paketversion."

#. type: TP
#: archlinux debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "\\f[CR]--help\\f[R], \\f[CR]-h\\fR"
msgstr "\\f[CR]--help\\f[R], \\f[CR]-h\\fR"

#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
msgid "Show brief usage information."
msgstr "Zeigt einen kurzen Hinweis zum Aufruf."

#. type: SH
#: archlinux debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "BEISPIELE"

# FIXME \\f[CR]bash\\f[R] → \\f[B]bash\\f[R](1)
#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
msgid ""
"Start \\f[CR]bash\\f[R] in the current working directory in its own network "
"namespace as the current user.\\fR"
msgstr ""
"\\f[B]bash\\f[R](1) im aktuellen Arbeitsverzeichnis mit seinem eigenen "
"Netzwerknamensraum als aktuellen Benutzer starten:\\fR"

#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "mkosi-sandbox --bind / / --same-dir --unshare-net\n"
msgstr "mkosi-sandbox --bind / / --same-dir --unshare-net\n"

# FIXME \\f[CR]id\\f[R] → \\f[B]id\\f[R](1)
#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
msgid ""
"Run \\f[CR]id\\f[R] as the root user in a sandbox with only \\f[CR]/"
"usr\\f[R] from the host plus the necessary symlinks to be able to run "
"commands.\\fR"
msgstr ""
"\\f[B]id\\f[R](1) als der Benutzer root mit nur \\f[CR]/usr\\f[R] vom "
"Hauptsystem sowie den notwendigen Symlinks, um Befehle ausführen zu können, "
"in der Sandbox ausführen:\\fR"

# FIXME The line "--symlink usr/bin /bin" is used twice - is this needed?
#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
#, no-wrap
msgid ""
"mkosi-sandbox \\[rs]\n"
"    --ro-bind /usr /usr \\[rs]\n"
"    --symlink usr/bin /bin \\[rs]\n"
"    --symlink usr/bin /bin \\[rs]\n"
"    --symlink usr/lib /lib \\[rs]\n"
"    --symlink usr/lib64 /lib64 \\[rs]\n"
"    --symlink usr/sbin /sbin \\[rs]\n"
"    --dev /dev \\[rs]\n"
"    --proc /proc \\[rs]\n"
"    --tmpfs /tmp \\[rs]\n"
"    --become-root \\[rs]\n"
"    id\n"
msgstr ""
"mkosi-sandbox \\[rs]\n"
"    --ro-bind /usr /usr \\[rs]\n"
"    --symlink usr/bin /bin \\[rs]\n"
"    --symlink usr/bin /bin \\[rs]\n"
"    --symlink usr/lib /lib \\[rs]\n"
"    --symlink usr/lib64 /lib64 \\[rs]\n"
"    --symlink usr/sbin /sbin \\[rs]\n"
"    --dev /dev \\[rs]\n"
"    --proc /proc \\[rs]\n"
"    --tmpfs /tmp \\[rs]\n"
"    --become-root \\[rs]\n"
"    id\n"

#. type: SH
#: archlinux debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
msgid "\\f[CR]mkosi(1)\\fR"
msgstr "\\f[CR]mkosi(1)\\fR"
