# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Karl Eichwalder <ke@suse.de>
# Lutz Behnke <lutz.behnke@gmx.de>
# Michael Piefel <piefel@debian.org>
# Michael Schmidt <michael@guug.de>
# Mario Blättermann <mario.blaettermann@gmail.com>, 2014, 2021, 2022, 2023.
# Dr. Tobias Quathamer <toddy@debian.org>, 2010, 2012, 2016.
# Helge Kreutzmann <debian@helgefjell.de>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.25.1\n"
"POT-Creation-Date: 2025-02-28 16:30+0100\n"
"PO-Revision-Date: 2025-01-31 19:29+0100\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.12.3\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "CHMOD"
msgstr "CHMOD"

#. type: TH
#: archlinux fedora-rawhide
#, no-wrap
msgid "February 2025"
msgstr "Februar 2025"

#. type: TH
#: archlinux fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "GNU coreutils 9.6"
msgstr "GNU coreutils 9.6"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Dienstprogramme für Benutzer"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "chmod - change file mode bits"
msgstr "chmod - Dateimodusbits ändern"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<chmod> [I<\\,OPTION\\/>]... I<\\,MODE\\/>[I<\\,,MODE\\/>]... I<\\,FILE\\/"
">..."
msgstr ""
"B<chmod> [I<\\,OPTION\\/>]… I<\\,MODUS\\/>[I<\\,,MODUS\\/>]… I<\\,DATEI\\/>…"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<chmod> [I<\\,OPTION\\/>]... I<\\,OCTAL-MODE FILE\\/>..."
msgstr "B<chmod> [I<\\,OPTION\\/>]… I<\\,OKTAL‐MODUS DATEI\\/>…"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<chmod> [I<\\,OPTION\\/>]... I<\\,--reference=RFILE FILE\\/>..."
msgstr "B<chmod> [I<\\,OPTION\\/>]… I<\\,--reference=RDATEI DATEI\\/>…"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This manual page documents the GNU version of B<chmod>.  B<chmod> changes "
"the file mode bits of each given file according to I<mode>, which can be "
"either a symbolic representation of changes to make, or an octal number "
"representing the bit pattern for the new mode bits."
msgstr ""
"Diese Handbuchseite beschreibt die GNU-Version von B<chmod>. B<chmod> ändert "
"die Dateimodusbits jeder angegebenen Datei gemäß I<Modus>, der entweder eine "
"symbolische Repräsentation der durchzuführenden Änderungen sein kann oder "
"eine oktale Zahl, die das Bitmuster für die neuen Modusbits darstellt."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The format of a symbolic mode is [B<ugoa>.\\|.\\|.][[B<-+=>][I<perms>.\\|."
"\\|.].\\|.\\|.], where I<perms> is either zero or more letters from the set "
"B<rwxXst>, or a single letter from the set B<ugo>.  Multiple symbolic modes "
"can be given, separated by commas."
msgstr ""
"Das Format eines symbolischen Modus ist [B<ugoa>…][[B<-+=>][I<Rechte>.]…], "
"wobei I<Rechte> entweder keines oder mehrere Zeichen aus der Menge B<rwxXst> "
"ist oder ein einzelner Buchstabe aus der Menge B<ugo>. Es können mehrere "
"symbolische Modi, durch Kommata getrennt, angegeben werden."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"A combination of the letters B<ugoa> controls which users' access to the "
"file will be changed: the user who owns it (B<u>), other users in the file's "
"group (B<g>), other users not in the file's group (B<o>), or all users "
"(B<a>).  If none of these are given, the effect is as if (B<a>) were given, "
"but bits that are set in the umask are not affected."
msgstr ""
"Eine Kombination der Buchstaben B<ugoa> steuert, für welche Benutzer die "
"Zugriffsrechte der Datei geändert werden: Der Benutzer, dem die Datei gehört "
"(B<u>), andere Benutzer, die der Gruppe der Datei angehören (B<g>), andere "
"Benutzer, die nicht der Gruppe der Datei angehören (B<o>) oder alle Benutzer "
"(B<a>). Wenn keiner der Buchstaben angegeben wurde, ist der Effekt so, als "
"sei B<a> angegeben worden, aber Bits, die in der umask gesetzt sind, werden "
"nicht beeinflusst."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The operator B<+> causes the selected file mode bits to be added to the "
"existing file mode bits of each file; B<-> causes them to be removed; and "
"B<=> causes them to be added and causes unmentioned bits to be removed "
"except that a directory's unmentioned set user and group ID bits are not "
"affected."
msgstr ""
"Der Operator B<+> bewirkt, dass die ausgewählten Dateimodusbits zu den "
"existierenden Dateimodusbits jeder Datei hinzugefügt werden, B<-> bewirkt, "
"dass sie entfernt werden. B<=> bewirkt, dass sie hinzugefügt werden und "
"nicht erwähnte Bits entfernt werden, mit der Ausnahme, dass bei "
"Verzeichnissen nicht erwähnte »set-user-ID«- und »set-group-ID«-Bits nicht "
"beeinflusst werden."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The letters B<rwxXst> select file mode bits for the affected users: read "
"(B<r>), write (B<w>), execute (or search for directories)  (B<x>), execute/"
"search only if the file is a directory or already has execute permission for "
"some user (B<X>), set user or group ID on execution (B<s>), restricted "
"deletion flag or sticky bit (B<t>).  Instead of one or more of these "
"letters, you can specify exactly one of the letters B<ugo>: the permissions "
"granted to the user who owns the file (B<u>), the permissions granted to "
"other users who are members of the file's group (B<g>), and the permissions "
"granted to users that are in neither of the two preceding categories (B<o>)."
msgstr ""
"Die Buchstaben B<rwxXst> wählen die Dateimodusbits für die betroffenen "
"Benutzer aus: Lesen (B<r>), Schreiben (B<w>), Ausführen (oder Suchen bei "
"Verzeichnissen) (B<x>), nur Ausführen/Suchen, wenn die Datei ein Verzeichnis "
"ist oder bereits für einige Benutzer die Ausführungsrechte besitzt (B<X>), "
"die Benutzer- oder Gruppenkennung bei der Ausführung setzen (B<s>), Schalter "
"für eingeschränktes Löschen oder »klebriges« (sticky) Bit (B<t>). Statt "
"einer oder mehrerer dieser Buchstaben können Sie exakt einen der Buchstaben "
"B<ugo> angeben: Die Rechte werden dem Benutzer verliehen, dem die Datei "
"gehört (B<u>), die Rechte werden anderen Benutzern verliehen, die Mitglieder "
"der Gruppe der Datei sind (B<g>) oder die Rechte werden Benutzern verliehen, "
"die in keiner der vorherigen zwei Kategorien sind (B<o>)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"A numeric mode is from one to four octal digits (0-7), derived by adding up "
"the bits with values 4, 2, and 1.  Omitted digits are assumed to be leading "
"zeros.  The first digit selects the set user ID (4) and set group ID (2) and "
"restricted deletion or sticky (1) attributes.  The second digit selects "
"permissions for the user who owns the file: read (4), write (2), and execute "
"(1); the third selects permissions for other users in the file's group, with "
"the same values; and the fourth for other users not in the file's group, "
"with the same values."
msgstr ""
"Ein numerischer Modus besteht aus einer bis vier oktalen Ziffern (0-7), die "
"aus der Summe der Bits mit den Werten 4, 2 und 1 abgeleitet werden. "
"Weggelassene Ziffern werden als führende Nullen angenommen. Die erste Ziffer "
"wählt die Attribute »set-user-ID« (4), »set-group-ID« (2) und "
"eingeschränktes Löschen oder »klebrig« (sticky, 1). Die zweite Ziffer wählt "
"die Rechte des Benutzers aus, dem die Datei gehört: Lesen (4), Schreiben (2) "
"und Ausführen (1). Die dritte Ziffer wählt die Rechte für Benutzer aus, die "
"in der Gruppe der Datei sind, dabei gelten dieselben Werte; die vierte "
"Ziffer gilt für Benutzer, die nicht in der Gruppe der Datei sind, auch hier "
"gelten dieselben Werte."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<chmod> doesn't change the permissions of symbolic links; the B<chmod> "
"system call cannot change their permissions on most systems, and most "
"systems ignore permissions of symbolic links.  However, for each symbolic "
"link listed on the command line, B<chmod> changes the permissions of the "
"pointed-to file.  In contrast, B<chmod> ignores symbolic links encountered "
"during recursive directory traversals. Options that modify this behavior are "
"described in the B<OPTIONS> section."
msgstr ""
"B<chmod> ändert die Rechte von symbolischen Links nicht, denn der "
"Systemaufruf von B<chmod> kann auf den meisten Systemen deren Rechte nicht "
"ändern und die meisten System ignorieren die Berechtigungen von symbolischen "
"Links. Allerdings ändert B<chmod> bei jedem in der Befehlszeile "
"aufgelisteten symbolischen Link die Rechte der Datei, auf die dieser zeigt. "
"Im Gegensatz dazu ignoriert B<chmod> symbolische Links, die bei einem "
"rekursiven Durchwandern von Verzeichnissen angetroffen werden. Optionen, die "
"dieses Verhalten ändern, sind im Abschnitt B<OPTIONEN> beschrieben."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SETUID AND SETGID BITS"
msgstr "SETUID- UND SETGID-BITS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<chmod> clears the set-group-ID bit of a regular file if the file's group "
"ID does not match the user's effective group ID or one of the user's "
"supplementary group IDs, unless the user has appropriate privileges.  "
"Additional restrictions may cause the set-user-ID and set-group-ID bits of "
"I<MODE> or I<RFILE> to be ignored.  This behavior depends on the policy and "
"functionality of the underlying B<chmod> system call.  When in doubt, check "
"the underlying system behavior."
msgstr ""
"B<chmod> löscht das »set-group-ID«-Bit einer regulären Datei, wenn die "
"Gruppenkennung der Datei nicht mit der effektiven Gruppenkennung des "
"Benutzers oder einer seiner zusätzlichen Gruppenkennungen übereinstimmt, es "
"sei denn, der Benutzer hat die entsprechenden Privilegien. Zusätzliche "
"Einschränkungen können bewirken, dass die Bits »set-user-ID« und »set-group-"
"ID« aus I<MODUS> oder I<RDATEI> ignoriert werden. Dieses Verhalten hängt von "
"der Richtlinie und Funktionalität des zu Grunde liegenden Systemaufrufs "
"B<chmod> ab. Im Zweifel überprüfen Sie das Verhalten des zu Grunde liegenden "
"Systems."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For directories B<chmod> preserves set-user-ID and set-group-ID bits unless "
"you explicitly specify otherwise.  You can set or clear the bits with "
"symbolic modes like B<u+s> and B<g-s>.  To clear these bits for directories "
"with a numeric mode requires an additional leading zero like B<00755>, "
"leading minus like B<-6000>, or leading equals like B<=755>."
msgstr ""
"Für Verzeichnisse lässt B<chmod> die Bits »set-user-ID« und »set-group-ID« "
"unangetastet, es sei denn, Sie geben dies explizit anders an. Sie können "
"diese Bits mit symbolischen Modi wie B<u+s> und B<g-s> setzen. Um diese Bits "
"für Verzeichnisse mit einem numerischen Modus zu bereinigen, wird eine "
"vorangestellte Null wie in B<00755>, ein vorangestelltes Minus wie in "
"B<-6000> oder ein vorangestelltes Gleichheitszeichen wie in B<=755> benötigt."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RESTRICTED DELETION FLAG OR STICKY BIT"
msgstr "SCHALTER FÜR EINGESCHRÄNKTES LÖSCHEN ODER »KLEBRIGES« (STICKY) BIT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The restricted deletion flag or sticky bit is a single bit, whose "
"interpretation depends on the file type.  For directories, it prevents "
"unprivileged users from removing or renaming a file in the directory unless "
"they own the file or the directory; this is called the I<restricted deletion "
"flag> for the directory, and is commonly found on world-writable directories "
"like B</tmp>.  For regular files on some older systems, the bit saves the "
"program's text image on the swap device so it will load more quickly when "
"run; this is called the I<sticky bit>."
msgstr ""
"Der Schalter für eingeschränktes Löschen oder »klebriges« (sticky) Bit ist "
"ein einzelnes Bit, dessen Interpretation vom Dateityp abhängt. Bei "
"Verzeichnissen verhindert es, dass nicht privilegierte Benutzer eine Datei "
"in diesem Verzeichnis löschen oder umbenennen, es sei denn, ihnen gehört die "
"Datei oder das Verzeichnis; dies nennt sich »Schalter für eingeschränktes "
"Löschen« für das Verzeichnis und wird üblicherweise bei allgemein "
"schreibbaren Verzeichnissen wie B</tmp> verwendet. Bei regulären Dateien auf "
"einigen älteren Systemen sorgt das Bit dafür, dass das »Text-Image« eines "
"Programms auf dem Auslagerungsgerät gespeichert wird. Dadurch kann das "
"Programm schneller geladen werden, wenn es ausgeführt wird; dies nennt sich "
"das »klebrige« (sticky) Bit."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Change the mode of each FILE to MODE.  With B<--reference>, change the mode "
"of each FILE to that of RFILE."
msgstr ""
"Den Modus jeder DATEI auf MODUS setzen. Mit B<--reference> wird der Modus "
"jeder DATEI auf den Modus von RDATEI gesetzt."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-c>, B<--changes>"
msgstr "B<-c>, B<--changes>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "like verbose but report only when a change is made"
msgstr "Wie B<--verbose>, aber nur durchgeführte Änderungen berichten"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-f>, B<--silent>, B<--quiet>"
msgstr "B<-f>, B<--silent>, B<--quiet>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "suppress most error messages"
msgstr "Die meisten Fehlermeldungen unterdrücken"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "output a diagnostic for every file processed"
msgstr "Eine Diagnose für jede verarbeitete Datei ausgeben"

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--dereference>"
msgstr "B<--dereference>"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"affect the referent of each symbolic link, rather than the symbolic link "
"itself"
msgstr ""
"Die referenzierte Datei jedes symbolischen Links ändern statt des "
"symbolischen Links selbst"

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-h>, B<--no-dereference>"
msgstr "B<-h>, B<--no-dereference>"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "affect each symbolic link, rather than the referent"
msgstr "Den symbolischen Link ändern, statt die referenzierte Datei"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--no-preserve-root>"
msgstr "B<--no-preserve-root>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "do not treat '/' specially (the default)"
msgstr "»/« nicht besonders behandeln (Voreinstellung)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--preserve-root>"
msgstr "B<--preserve-root>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "fail to operate recursively on '/'"
msgstr "Rekursive Bearbeitung von »/« ablehnen"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--reference>=I<\\,RFILE\\/>"
msgstr "B<--reference>=I<\\,RDATEI\\/>"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"use RFILE's mode instead of specifying MODE values.  RFILE is always "
"dereferenced if a symbolic link."
msgstr ""
"benutzt die Gruppe von RDATEI statt einer direkt angegebenen GRUPPE. RDATEI "
"wird immer dereferenziert, wenn es ein symbolischer Link ist."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-R>, B<--recursive>"
msgstr "B<-R>, B<--recursive>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "change files and directories recursively"
msgstr "Dateien und Verzeichnisse rekursiv ändern"

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"The following options modify how a hierarchy is traversed when the B<-R> "
"option is also specified.  If more than one is specified, only the final one "
"takes effect. B<-H> is the default."
msgstr ""
"Die folgenden Optionen bestimmen, wie eine Hierarchie abgearbeitet wird, "
"wenn die Option B<-R> ebenfalls angegeben ist. Wenn mehr als eine angegeben "
"ist, wirkt sich nur die Letzte aus. B<-H> ist die Vorgabe."

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-H>"
msgstr "B<-H>"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"if a command line argument is a symbolic link to a directory, traverse it"
msgstr ""
"Wenn ein Befehlszeilenargument ein symbolischer Link auf ein Verzeichnis "
"ist, dieses durchlaufen"

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-L>"
msgstr "B<-L>"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "traverse every symbolic link to a directory encountered"
msgstr "Jeden gefundenen symbolischen Link auf ein Verzeichnis durchlaufen"

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-P>"
msgstr "B<-P>"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "do not traverse any symbolic links"
msgstr "Überhaupt keine symbolischen Links durchlaufen"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "zeigt Hilfeinformationen an und beendet das Programm."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "gibt Versionsinformationen aus und beendet das Programm."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Each MODE is of the form '[ugoa]*([-+=]([rwxXst]*|[ugo]))+|[-+=][0-7]+'."
msgstr ""
"Jeder MODUS hat die Form »[ugoa]*([-+=]([rwxXst]*|[ugo]))+|[-+=][0-7]+«."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Written by David MacKenzie and Jim Meyering."
msgstr "Geschrieben von David MacKenzie und Jim Meyering."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "FEHLER MELDEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Onlinehilfe für GNU coreutils: E<.UR https://www.gnu.org/software/coreutils/"
">E<.UE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Melden Sie Fehler in der Programmübersetzung an E<.UR https://"
"translationproject.org/team/de.html> das deutschsprachige Team beim GNU "
"Translation ProjectE<.UE .>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "COPYRIGHT"

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"Copyright \\(co 2025 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2025 Free Software Foundation, Inc. Lizenz GPLv3+: E<.UR "
"https://gnu.org/licenses/gpl.html> GNU GPL Version 3 E<.UE> oder neuer."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Dies ist freie Software: Sie können sie verändern und weitergeben. Es gibt "
"KEINE GARANTIE, soweit gesetzlich zulässig."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<chmod>(2)"
msgstr "B<chmod>(2)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Full documentation E<lt>https://www.gnu.org/software/coreutils/chmodE<gt>"
msgstr ""
"Vollständige Dokumentation unter: E<lt>https://www.gnu.org/software/"
"coreutils/chmodE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "or available locally via: info \\(aq(coreutils) chmod invocation\\(aq"
msgstr "oder lokal verfügbar mit: info \\(aq(coreutils) chmod invocation\\(aq"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "September 2022"
msgstr "September 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid ""
"B<chmod> never changes the permissions of symbolic links; the B<chmod> "
"system call cannot change their permissions.  This is not a problem since "
"the permissions of symbolic links are never used.  However, for each "
"symbolic link listed on the command line, B<chmod> changes the permissions "
"of the pointed-to file.  In contrast, B<chmod> ignores symbolic links "
"encountered during recursive directory traversals."
msgstr ""
"B<chmod> ändert niemals die Rechte von symbolischen Links, denn der "
"Systemaufruf von B<chmod> kann deren Rechte nicht ändern. Das ist aber kein "
"Problem, weil die Rechte von symbolischen Links niemals benutzt werden. "
"Allerdings ändert B<chmod> bei jedem in der Befehlszeile aufgelisteten "
"symbolischen Link die Rechte der Datei, auf die dieser zeigt. Im Gegensatz "
"dazu ignoriert B<chmod> symbolische Links, die bei einem rekursiven "
"Durchwandern von Verzeichnissen angetroffen werden."

#. type: Plain text
#: debian-bookworm
msgid "use RFILE's mode instead of MODE values"
msgstr "Modus von RDATEI anstatt der MODUS-Werte verwenden"

#. type: Plain text
#: debian-bookworm
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2022 Free Software Foundation, Inc. Lizenz GPLv3+: E<.UR "
"https://gnu.org/licenses/gpl.html> GNU GPL Version 3 E<.UE> oder neuer."

#. type: TH
#: debian-unstable
#, no-wrap
msgid "October 2024"
msgstr "Oktober 2024"

#. type: TH
#: debian-unstable mageia-cauldron
#, no-wrap
msgid "GNU coreutils 9.5"
msgstr "GNU coreutils 9.5"

# FIXME '-H' → B<-H>
#. type: Plain text
#: debian-unstable mageia-cauldron
msgid ""
"The following options modify how a hierarchy is traversed when the B<-R> "
"option is also specified.  If more than one is specified, only the final one "
"takes effect. '-H' is the default."
msgstr ""
"Die folgenden Optionen bestimmen, wie eine Hierarchie abgearbeitet wird, "
"wenn die Option B<-R> ebenfalls angegeben ist. Wenn mehr als eine angegeben "
"ist, wirkt sich nur die Letzte aus. B<-H> ist die Vorgabe."

#. type: Plain text
#: debian-unstable mageia-cauldron
msgid ""
"Copyright \\(co 2024 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2024 Free Software Foundation, Inc. Lizenz GPLv3+: E<.UR "
"https://gnu.org/licenses/gpl.html> GNU GPL Version 3 E<.UE> oder neuer."

#. type: TH
#: fedora-42 opensuse-tumbleweed
#, no-wrap
msgid "January 2025"
msgstr "Januar 2025"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "March 2024"
msgstr "März 2024"

#. type: TH
#: opensuse-leap-16-0
#, no-wrap
msgid "January 2024"
msgstr "Januar 2024"

#. type: TH
#: opensuse-leap-16-0
#, no-wrap
msgid "GNU coreutils 9.4"
msgstr "GNU coreutils 9.4"

#. type: Plain text
#: opensuse-leap-16-0
msgid ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2023 Free Software Foundation, Inc. Lizenz GPLv3+: E<.UR "
"https://gnu.org/licenses/gpl.html> GNU GPL Version 3 E<.UE> oder neuer."
