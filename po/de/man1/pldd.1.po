# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Helge Kreutzmann <debian@helgefjell.de>, 2023,2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.25.1\n"
"POT-Creation-Date: 2025-02-28 16:43+0100\n"
"PO-Revision-Date: 2025-02-01 06:47+0100\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. #-#-#-#-#  archlinux: pldd.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SY
#. #-#-#-#-#  debian-bookworm: pldd.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TH
#. #-#-#-#-#  debian-unstable: pldd.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TH
#. #-#-#-#-#  fedora-42: pldd.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TH
#. #-#-#-#-#  fedora-rawhide: pldd.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TH
#. #-#-#-#-#  mageia-cauldron: pldd.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TH
#. #-#-#-#-#  opensuse-leap-16-0: pldd.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TH
#. #-#-#-#-#  opensuse-tumbleweed: pldd.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SY
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "pldd"
msgstr "pldd"

#. type: TH
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "2024-11-25"
msgstr "25. November 2024"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr "Linux man-pages 6.12"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "pldd - display dynamic shared objects linked into a process"
msgstr ""
"pldd - In einen Prozess gelinkte dynamische Laufzeitbibliotheken anzeigen"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "I<pid>"
msgstr "I<PID>"

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "I<option>"
msgstr "I<Option>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<pldd> command displays a list of the dynamic shared objects (DSOs) "
"that are linked into the process with the specified process ID (PID).  The "
"list includes the libraries that have been dynamically loaded using "
"B<dlopen>(3)."
msgstr ""
"Der Befehl B<pldd> zeigt eine Liste von dynamischen Laufzeitbibliotheken "
"(DSOs) an, die in den Prozess mit der angegebenen Kennung (PID) gelinkt "
"sind. Die Liste enthält die Bibliotheken, die dynamisch mittels B<dlopen>(3) "
"geladen wurden."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONEN"

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: TQ
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-?>"
msgstr "B<-?>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Display a help message and exit."
msgstr "Zeigt eine Hilfenachricht an und beendet sich."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Display a short usage message and exit."
msgstr "Zeigt eine kurze Gebrauchsinformation und beendet sich."

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: TQ
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-V>"
msgstr "B<-V>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Display program version information and exit."
msgstr "Zeigt Programmversionsinformationen an und beendet sich."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "EXIT STATUS"
msgstr "EXIT-STATUS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On success, B<pldd> exits with the status 0.  If the specified process does "
"not exist, the user does not have permission to access its dynamic shared "
"object list, or no command-line arguments are supplied, B<pldd> exists with "
"a status of 1.  If given an invalid option, it exits with the status 64."
msgstr ""
"Im Erfolgsfall beendet sich B<pldd> mit dem Status 0. Falls der angegebene "
"Prozess nicht existiert, der Benutzer keine Berechtigungen zum Zugriff auf "
"die dynamische Laufzeitbibliothek hat oder keine Befehlszeilenargumente "
"übergeben wurden, beendet sich B<pldd> mit einem Status 1. Falls eine "
"ungültige Option angegeben wird, beendet es sich mit dem Status 64."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONEN"

#.  There are man pages on Solaris and HP-UX.
#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Some other systems have a similar command."
msgstr "Einige andere Systeme haben einen ähnlichen Befehl."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "None."
msgstr "Keine."

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "GESCHICHTE"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "glibc 2.15."
msgstr "Glibc 2.15."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ANMERKUNGEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The command"
msgstr "Der Befehl"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "lsof -p PID\n"
msgstr "lsof -p PID\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"also shows output that includes the dynamic shared objects that are linked "
"into a process."
msgstr ""
"zeigt auch die Ausgabe, die die dynamischen Laufzeitbibliotheken enthält, "
"die in einen Prozess gelinkt sind."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<gdb>(1)  I<info shared> command also shows the shared libraries being "
"used by a process, so that one can obtain similar output to B<pldd> using a "
"command such as the following (to monitor the process with the specified "
"I<pid>):"
msgstr ""
"Der Befehl I<info shared> von B<gdb>(1) zeigt auch die Laufzeitbibliotheken "
"an, die von einem Prozess verwandt werden, so dass Sie damit eine ähnliche "
"Ausgabe wie B<pldd> erhalten können, wenn Sie einen Befehl der folgenden Art "
"verwenden (um einen Prozess mit der angegebenen I<pid> zu überwachen):"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"$ B<gdb -ex \"set confirm off\" -ex \"set height 0\" -ex \"info shared\" \\[rs]>\n"
"        B<-ex \"quit\" -p $pid | grep \\[aq]\\[ha]0x.*0x\\[aq]>\n"
msgstr ""
"$ B<gdb -ex \"set confirm off\" -ex \"set height 0\" -ex \"info shared\" \\[rs]>\n"
"        B<-ex \"quit\" -p $pid | grep \\[aq]\\[ha]0x.*0x\\[aq]>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "FEHLER"

#.  glibc commit 1a4c27355e146b6d8cc6487b998462c7fdd1048f
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"From glibc 2.19 to glibc 2.29, B<pldd> was broken: it just hung when "
"executed.  This problem was fixed in glibc 2.30, and the fix has been "
"backported to earlier glibc versions in some distributions."
msgstr ""
"Von Glibc 2.19 bis Glibc 2.29 war B<pldd> defekt: bei der Ausführung hing es "
"einfach. Das Problem wurde in Glibc 2.30 behoben und die Korrektur wurde in "
"einigen Distributionen auf ältere Glibc-Versionen zurückportiert."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "BEISPIELE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"$ B<echo $$>               # Display PID of shell\n"
"1143\n"
"$ B<pldd $$>               # Display DSOs linked into the shell\n"
"1143:   /usr/bin/bash\n"
"linux-vdso.so.1\n"
"/lib64/libtinfo.so.5\n"
"/lib64/libdl.so.2\n"
"/lib64/libc.so.6\n"
"/lib64/ld-linux-x86-64.so.2\n"
"/lib64/libnss_files.so.2\n"
msgstr ""
"$ B<echo $$>               # PID der Shell anzeigen\n"
"1143\n"
"$ B<pldd $$>               # DSOs anzeigen, die in die Shell gelinkt sind\n"
"1143:   /usr/bin/bash\n"
"linux-vdso.so.1\n"
"/lib64/libtinfo.so.5\n"
"/lib64/libdl.so.2\n"
"/lib64/libc.so.6\n"
"/lib64/ld-linux-x86-64.so.2\n"
"/lib64/libnss_files.so.2\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<ldd>(1), B<lsof>(1), B<dlopen>(3), B<ld.so>(8)"
msgstr "B<ldd>(1), B<lsof>(1), B<dlopen>(3), B<ld.so>(8)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5. Februar 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
#, no-wrap
msgid ""
"B<pldd >I<pid>\n"
"B<pldd>I< option>\n"
msgstr ""
"B<pldd >I<PID>\n"
"B<pldd>I< Option>\n"

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<-?>, B<--help>"
msgstr "B<-?>, B<--help>"

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: debian-bookworm
msgid "B<pldd> is available since glibc 2.15."
msgstr "B<pldd> ist seit Glibc 2.15 verfügbar."

#.  There are man pages on Solaris and HP-UX.
#. type: Plain text
#: debian-bookworm
msgid ""
"The B<pldd> command is not specified by POSIX.1.  Some other systems have a "
"similar command."
msgstr ""
"Der Befehl B<pldd> ist nicht in POSIX.1 spezifiziert. Einige andere Systeme "
"verfügen über einen ähnlichen Befehl."

#. type: Plain text
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid ""
"$ B<gdb -ex \"set confirm off\" -ex \"set height 0\" -ex \"info shared\" \\e>\n"
"        B<-ex \"quit\" -p $pid | grep \\[aq]\\[ha]0x.*0x\\[aq]>\n"
msgstr ""
"$ B<gdb -ex \"set confirm off\" -ex \"set height 0\" -ex \"info shared\" \\e>\n"
"        B<-ex \"quit\" -p $pid | grep \\[aq]\\[ha]0x.*0x\\[aq]>\n"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "2024-06-15"
msgstr "15. Juni 2024"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Linux man-pages 6.9.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31. Oktober 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Linux man-pages (unveröffentlicht)"
