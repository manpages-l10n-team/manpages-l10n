# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Helge Kreutzmann <debian@helgefjell.de>, 2025.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.25.1\n"
"POT-Creation-Date: 2025-02-28 16:32+0100\n"
"PO-Revision-Date: 2025-02-28 18:29+0100\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "MKPASSWD"
msgstr "MKPASSWD"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "22 August 1994"
msgstr "22. August 1994"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "mkpasswd - generate new password, optionally apply it to a user"
msgstr ""
"mkpasswd - Ein neues Passwort erstellen und optional auf einen Benutzer "
"anwenden"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: debian-bookworm
msgid "B<mkpasswd> I<[> I<args> ] [ I<user> ]"
msgstr "B<mkpasswd> I<[> I<Arg> ] [ I<Benutzer> ]"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "INTRODUCTION"
msgstr "EINFÜHRUNG"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"B<mkpasswd> generates passwords and can apply them automatically to users.  "
"mkpasswd is based on the code from Chapter 23 of the O'Reilly book "
"\"Exploring Expect\"."
msgstr ""
"B<mkpasswd> erstellt Passwörter und kann sie automatisch auf Benutzer "
"anwenden. B<mkpasswd> basiert auf dem Code von Kapitel 23 des O'Reilly-Buchs "
"»Exploring Expect«."

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "USAGE"
msgstr "AUFRUF"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "With no arguments, B<mkpasswd> returns a new password."
msgstr "Ohne Argumente liefert B<mkpasswd> ein neues Passwort zurück."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "  mkpasswd"
msgstr "  mkpasswd"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "With a user name, B<mkpasswd> assigns a new password to the user."
msgstr ""
"Mit einem Benutzernamen weist B<mkpasswd> dem Benutzer ein neues Passwort zu."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "  mkpasswd don"
msgstr "  mkpasswd don"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "The passwords are randomly generated according to the flags below."
msgstr ""
"Die Passwörter werden gemäß der nachfolgenden Schalter zufällig erstellt."

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "FLAGS"
msgstr "SCHALTER"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The B<-l> flag defines the length of the password.  The default is 9.  The "
"following example creates a 20 character password."
msgstr ""
"Der Schalter B<-l> definiert die Länge des Passworts. Die Vorgabe ist 9. Das "
"folgende Beispiel erstellt ein Passwort mit 20 Zeichen."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "  mkpasswd -l 20"
msgstr "  mkpasswd -l 20"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The B<-d> flag defines the minimum number of digits that must be in the "
"password.  The default is 2.  The following example creates a password with "
"at least 3 digits."
msgstr ""
"Der Schalter B<-d> definiert die minimale Anzahl an Ziffern, die im Passwort "
"enthalten sein müssen. Die Vorgabe ist 2. Das folgende Beispiel erstellt ein "
"Passwort mit mindestens 3 Ziffern."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "  mkpasswd -d 3"
msgstr "  mkpasswd -d 3"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The B<-c> flag defines the minimum number of lowercase alphabetic characters "
"that must be in the password.  The default is 2."
msgstr ""
"Der Schalter B<-c> definiert die minimale Anzahl an alphabetischen "
"Kleinbuchstaben, die im Passwort enthalten sein müssen. Die Vorgabe ist 2."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The B<-C> flag defines the minimum number of uppercase alphabetic characters "
"that must be in the password.  The default is 2."
msgstr ""
"Der Schalter B<-C> definiert die minimale Anzahl an alphabetischen "
"Großbuchstaben, die im Passwort enthalten sein müssen. Die Vorgabe ist 2."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The B<-s> flag defines the minimum number of special characters that must be "
"in the password.  The default is 1."
msgstr ""
"Der Schalter B<-s> definiert die minimale Anzahl an Sonderzeichen, die im "
"Passwort enthalten sein müssen. Die Vorgabe ist 1."

# FIXME /etc/yppasswd → I</etc/yppasswd>
# FIXME /bin/passwd → I</bin/passwd>
#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The B<-p> flag names a program to set the password.  By default, /etc/"
"yppasswd is used if present, otherwise /bin/passwd is used."
msgstr ""
"Der Schalter B<-p> benennt ein Programm, um das Passwort zu setzen. "
"Standardmäßig wird I</etc/yppasswd> verwandt, falls vorhanden, ansonsten I</"
"bin/passwd>."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The B<-2> flag causes characters to be chosen so that they alternate between "
"right and left hands (qwerty-style), making it harder for anyone watching "
"passwords being entered.  This can also make it easier for a password-"
"guessing program."
msgstr ""
"Der Schalter B<-2> führt dazu, dass die Zeichen so gewählt werden, dass sie "
"zwischen der rechten und der linken Hand (auf QWERTY-artigen Tastaturen) "
"abwechseln. Damit wird es für Personen, die bei der Eingabe von Passwörtern "
"zuschauen, erschwert. Dies erleichtert es auch Passwort-Rateprogrammen."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The B<-v> flag causes the password-setting interaction to be visible.  By "
"default, it is suppressed."
msgstr ""
"Der Schalter B<-v> führt dazu, dass die Passwort-Einstellungs-Interaktion "
"sichtbar wird. Standardmäßig wird sie unterdrückt."

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "EXAMPLE"
msgstr "BEISPIEL"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The following example creates a 15-character password that contains at least "
"3 digits and 5 uppercase characters."
msgstr ""
"Das folgende Beispiel erstellt ein 15-stelliges Passwort, das mindestens 3 "
"Ziffern und 5 Großbuchstaben enthält:"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "  mkpasswd -l 15 -d 3 -C 5"
msgstr "  mkpasswd -l 15 -d 3 -C 5"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"I<\"Exploring Expect: A Tcl-Based Toolkit for Automating Interactive "
"Programs\"> by Don Libes, O'Reilly and Associates, January 1995."
msgstr ""
"I<\"Exploring Expect: A Tcl-Based Toolkit for Automating Interactive "
"Programs\"> von Don Libes, O'Reilly and Associates, Januar 1995."

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "Don Libes, National Institute of Standards and Technology"
msgstr "Don Libes, National Institute of Standards and Technology"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"B<mkpasswd> is in the public domain.  NIST and I would appreciate credit if "
"this program or parts of it are used."
msgstr ""
"B<mkpasswd> ist gemeinfrei (»public domain«). Der Autor und NIST würden sich "
"aber freuen, wenn die Urheberschaft bei der Verwendung des Programms oder "
"Teilen davon erwähnt würde."

#. type: Plain text
#: debian-unstable
msgid "B<mkpasswd> [ I<args> ] [ I<user> ]"
msgstr "B<mkpasswd> [ I<Arg> ] [ I<Benutzer> ]"
