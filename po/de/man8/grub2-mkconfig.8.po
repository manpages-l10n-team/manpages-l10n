# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2020, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.21.0\n"
"POT-Creation-Date: 2025-02-28 16:35+0100\n"
"PO-Revision-Date: 2024-03-02 13:08+0100\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 23.04.1\n"

#. type: TH
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "GRUB-MKCONFIG"
msgstr "GRUB-MKCONFIG"

#. type: TH
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "February 2025"
msgstr "Februar 2025"

#. type: TH
#: fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "GRUB 2.12"
msgstr "GRUB 2.12"

#. type: TH
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "System Administration Utilities"
msgstr "Systemverwaltungswerkzeuge"

#. type: SH
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "grub-mkconfig - generate a GRUB configuration file"
msgstr "grub-mkconfig - eine GRUB-Konfigurationsdatei erzeugen"

#. type: SH
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<grub-mkconfig> [I<\\,OPTION\\/>]"
msgstr "B<grub-mkconfig> [I<\\,OPTION\\/>]"

#. type: SH
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Generate a grub config file"
msgstr "Erzeugt eine Grub-Konfigurationsdatei."

#. type: TP
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-o>, B<--output>=I<\\,FILE\\/>"
msgstr "B<-o>, B<--output>=I<\\,DATEI\\/>"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "output generated config to FILE [default=stdout]"
msgstr ""
"gibt die erzeugte Konfiguration in die angegebene DATEI aus (standardmäßig "
"wird in die Standardausgabe geschrieben)."

#. type: TP
#: fedora-42 fedora-rawhide
#, no-wrap
msgid "B<--no-grubenv-update>"
msgstr "B<--no-grubenv-update>"

#. type: Plain text
#: fedora-42 fedora-rawhide
msgid "do not update variables in the grubenv file"
msgstr "aktualisiert keine Variablen in der B<grubenv>-Datei."

#. type: TP
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "print this message and exit"
msgstr "gibt eine Hilfemeldung aus und beendet das Programm."

#. type: TP
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "print the version information and exit"
msgstr "gibt die Versionsinformation aus und beendet das Programm."

#. type: SH
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "FEHLER MELDEN"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr "Melden Sie Fehler (auf Englisch) an E<.MT bug-grub@gnu.org> E<.ME .>"

#. type: SH
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<grub-install>(8)"
msgstr "B<grub-install>(8)"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"The full documentation for B<grub-mkconfig> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-mkconfig> programs are properly installed "
"at your site, the command"
msgstr ""
"Die vollständige Dokumentation für B<grub-mkconfig> wird als ein Texinfo-"
"Handbuch gepflegt. Wenn die Programme B<info>(1) und B<grub-mkconfig> auf "
"Ihrem Rechner ordnungsgemäß installiert sind, können Sie mit dem Befehl"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<info grub-mkconfig>"
msgstr "B<info grub-mkconfig>"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "should give you access to the complete manual."
msgstr "auf das vollständige Handbuch zugreifen."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "October 2024"
msgstr "Oktober 2024"

#. type: TH
#: opensuse-leap-16-0
#, no-wrap
msgid "December 2024"
msgstr "Dezember 2024"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "GRUB2 2.12"
msgstr "GRUB2 2.12"
