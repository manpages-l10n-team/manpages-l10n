# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Helge Kreutzmann <debian@helgefjell.de>, 2020,2021.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.20.0\n"
"POT-Creation-Date: 2025-02-28 16:54+0100\n"
"PO-Revision-Date: 2024-05-29 12:48+0200\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYSTEMD-USERDBD\\&.SERVICE"
msgstr "SYSTEMD-USERDBD\\&.SERVICE"

#. type: TH
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "systemd 257.3"
msgstr "systemd 257.3"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "systemd-userdbd.service"
msgstr "systemd-userdbd.service"

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"systemd-userdbd.service, systemd-userdbd - JSON User/Group Record Query "
"Multiplexer/NSS Compatibility"
msgstr ""
"systemd-userdbd.service, systemd-userdbd - JSON-Benutzer-/Gruppendatensatz-"
"Multiplexer/NSS-Kompatibilität"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "systemd-userdbd\\&.service"
msgstr "systemd-userdbd\\&.service"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "/usr/lib/systemd/systemd-userdbd"
msgstr "/usr/lib/systemd/systemd-userdbd"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<systemd-userdbd> is a system service that multiplexes user/group lookups "
"to all local services that provide JSON user/group record definitions to the "
"system\\&. In addition it synthesizes JSON user/group records from classic "
"UNIX/glibc NSS user/group records in order to provide full backwards "
"compatibility\\&. It may also pick up statically defined JSON user/group "
"records from files in /etc/userdb/, /run/userdb/, /run/host/userdb/ and /usr/"
"lib/userdb/ with the \"\\&.user\" extension\\&."
msgstr ""
"B<systemd-userdbd> ist ein Systemdienst, der Benutzer-/Gruppen-Nachschlagung "
"auf alle lokalen Dienste parallel verteilt, die dem System JSON-Benutzer-/"
"Gruppendatensatzdefinitionen bereitstellen\\&. Zusätzlich erstellt er "
"künstliche JSON-Benutzer-/Gruppendatensätze aus den klassischen UNIX/Glibc-"
"NSS-Benutzer-/Gruppendatensätzen, um vollständige Rückwärtskompatibilität "
"bereitzustellen\\&. Er kann auch statisch definierte JSON-Benutzer-/"
"Gruppendatensätze von Erweiterungsdateien in /etc/userdb/, /run/userdb/, /"
"run/host/userdb/ und /usr/lib/userdb/ mit der Erweiterung »\\&.user« "
"aufnehmen."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Most of B<systemd-userdbd>\\*(Aqs functionality is accessible through the "
"B<userdbctl>(1)  command\\&."
msgstr ""
"Der größte Teil der Funktionalität von B<systemd-userdbd> ist über den "
"Befehl B<userdbctl>(1) erreichbar\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The user and group records this service provides access to follow the "
"\\m[blue]B<JSON User Records>\\m[]\\&\\s-2\\u[1]\\d\\s+2 and \\m[blue]B<JSON "
"Group Record>\\m[]\\&\\s-2\\u[2]\\d\\s+2 definitions\\&. This service "
"implements the \\m[blue]B<User/Group Record Lookup API via Varlink>\\m[]"
"\\&\\s-2\\u[3]\\d\\s+2, and multiplexes access other services implementing "
"this API, too\\&. It is thus both server and client of this API\\&."
msgstr ""
"Die Benutzer- und Gruppendatensätze, auf die dieser Dienst Zugriff gewährt, "
"folgen den \\m[blue]B<JSON-Benutzerdatensätze>\\m[]\\&\\s-2\\u[1]\\d\\s+2- "
"und \\m[blue]B<JSON-Gruppendatensatz>\\m[]\\&\\s-2\\u[2]\\d\\s+2-"
"Definitionen\\&. Dieser Dienst implementiert das \\m[blue]B<Benutzer-/"
"Gruppen-Datensatznachschlage-API über Varlink>\\m[]\\&\\s-2\\u[3]\\d\\s+2 "
"und verteilt den Zugriff parallel auf andere Dienste, die auch dieses API "
"verwenden\\&. Er ist daher sowohl Server als auch Client dieses APIs\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This service provides three distinct \\m[blue]B<Varlink>\\m[]"
"\\&\\s-2\\u[4]\\d\\s+2 services: B<io\\&.systemd\\&.Multiplexer> provides a "
"single, unified API for querying JSON user and group records\\&. Internally "
"it talks to all other user/group record services running on the system in "
"parallel and forwards any information discovered\\&. This simplifies clients "
"substantially since they need to talk to a single service only instead of "
"all of them in parallel\\&.  B<io\\&.systemd\\&.NameServiceSwitch> provides "
"compatibility with classic UNIX/glibc NSS user records, i\\&.e\\&. converts "
"B<struct passwd> and B<struct group> records as acquired with APIs such as "
"B<getpwnam>(1)  to JSON user/group records, thus hiding the differences "
"between the services as much as possible\\&.  B<io\\&.systemd\\&.DropIn> "
"makes JSON user/group records from the aforementioned drop-in directories "
"available\\&."
msgstr ""
"Dieser Dienst stellt drei unterschiedliche \\m[blue]B<Varlink>\\m[]"
"\\&\\s-2\\u[4]\\d\\s+2-Dienste zur Verfügung: "
"B<io\\&.systemd\\&.Multiplexer> stellt eine einfache, vereinigte API zur "
"Abfrage von JSON-Benutzer- und Gruppendatensätzen bereit\\&. Intern "
"kommuniziert er parallel mit anderen Benutzer-/Gruppendatensatzdiensten, die "
"auf dem System laufen, und leitet alle erkannten Informationen weiter\\&. "
"Dies vereinfacht Clients deutlich, da sie nur mit einem einzelnen Dienst "
"kommunizieren müssen, statt parallel mit allen\\&. "
"B<io\\&.systemd\\&.NameServiceSwitch> stellt die Kompatibilität mit "
"klassischen UNIX/Glibc-NSS-Benutzerdatensätzen her, d\\&.h\\&. er "
"konvertiert B<struct passwd>- und B<struct group>-Datensätze, wie sie mit "
"APIs wie B<getpwnam>(1) erhalten werden, in JSON-Benutzer-/"
"Gruppendatensätze, wodurch die Unterschiede in den Diensten soweit möglich "
"versteckt werden\\&. B<io\\&.systemd\\&.DropIn> stellt JSON-Benutzer-/"
"Gruppendatensätze von den vorgenannten Ergänzungsverzeichnissen bereit\\&."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<systemd>(1), B<nss-systemd>(8), B<userdbctl>(1), B<systemd-"
"homed.service>(8)"
msgstr ""
"B<systemd>(1), B<nss-systemd>(8), B<userdbctl>(1), B<systemd-"
"homed.service>(8)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ANMERKUNGEN"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid " 1."
msgstr " 1."

# WONTFIX: In other files singular, i.e. Records → Record // Upstream: Done on purpose
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "JSON User Records"
msgstr "JSON-Benutzerdatensätze"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "\\%https://systemd.io/USER_RECORD"
msgstr "\\%https://systemd.io/USER_RECORD"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid " 2."
msgstr " 2."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "JSON Group Record"
msgstr "JSON-Gruppendatensatz"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "\\%https://systemd.io/GROUP_RECORD"
msgstr "\\%https://systemd.io/GROUP_RECORD"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid " 3."
msgstr " 3."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "User/Group Record Lookup API via Varlink"
msgstr "Benutzer-/Gruppen-Datensatznachschlage-API über Varlink"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "\\%https://systemd.io/USER_GROUP_API"
msgstr "\\%https://systemd.io/USER_GROUP_API"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid " 4."
msgstr " 4."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Varlink"
msgstr "Varlink"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "\\%https://varlink.org/"
msgstr "\\%https://varlink.org/"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "systemd 254"
msgstr "systemd 254"

#. type: Plain text
#: debian-bookworm
msgid "/lib/systemd/systemd-userdbd"
msgstr "/lib/systemd/systemd-userdbd"

#. type: Plain text
#: debian-bookworm
msgid ""
"B<systemd-userdbd> is a system service that multiplexes user/group lookups "
"to all local services that provide JSON user/group record definitions to the "
"system\\&. In addition it synthesizes JSON user/group records from classic "
"UNIX/glibc NSS user/group records in order to provide full backwards "
"compatibility\\&. It may also pick up statically defined JSON user/group "
"records from drop-in files in /etc/userdb/, /run/userdb/, /run/host/userdb/ "
"and /usr/lib/userdb/\\&."
msgstr ""
"B<systemd-userdbd> ist ein Systemdienst, der Benutzer-/Gruppen-Nachschlagung "
"auf alle lokalen Dienste parallel verteilt, die dem System JSON-Benutzer-/"
"Gruppendatensatzdefinitionen bereitstellen\\&. Zusätzlich erstellt er "
"künstliche JSON-Benutzer-/Gruppendatensätze aus den klassischen UNIX/Glibc-"
"NSS-Benutzer-/Gruppendatensätzen, um vollständige Rückwärtskompatibilität "
"bereitzustellen\\&. Er kann auch statisch definierte JSON-Benutzer-/"
"Gruppendatensätze von Ergänzungsdateien in /etc/userdb/, /run/userdb/, /run/"
"host/userdb/ und /usr/lib/userdb/ aufnehmen."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "systemd 255"
msgstr "systemd 255"

#. type: TH
#: opensuse-leap-16-0
#, no-wrap
msgid "systemd 257.2"
msgstr "systemd 257.2"
