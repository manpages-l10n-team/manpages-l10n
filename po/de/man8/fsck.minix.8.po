# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2014, 2016, 2020, 2021, 2022.
# Dr. Tobias Quathamer <toddy@debian.org>, 2017.
# Helge Kreutzmann <debian@helgefjell.de>, 2014, 2018.
msgid ""
msgstr ""
"Project-Id-Version: manpages-de\n"
"POT-Creation-Date: 2024-03-29 09:39+0100\n"
"PO-Revision-Date: 2022-02-11 11:36+0100\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.12.2\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "FSCK.MINIX"
msgstr "FSCK.MINIX"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr "11. Mai 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "System Administration"
msgstr "System-Administration"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: debian-bookworm
msgid "fsck.minix - check consistency of Minix filesystem"
msgstr "fsck.minix - Konsistenzprüfung eines Minix-Dateisystems"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: debian-bookworm
msgid "B<fsck.minix> [options] I<device>"
msgstr "B<fsck.minix> [Optionen] I<Gerät>"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: debian-bookworm
msgid ""
"B<fsck.minix> performs a consistency check for the Linux MINIX filesystem."
msgstr "fsck.minix - Konsistenzprüfung eines Linux-MINIX-Dateisystems"

#. type: Plain text
#: debian-bookworm
msgid ""
"The program assumes the filesystem is quiescent. B<fsck.minix> should not be "
"used on a mounted device unless you can be sure nobody is writing to it. "
"Remember that the kernel can write to device when it searches for files."
msgstr ""
"Das Programm geht davon aus, dass das Dateisystem nicht aktiv ist. "
"B<fsck.minix> sollte nicht auf ein eingehängtes Gerät angewendet werden, es "
"sei denn, Sie können sicherstellen, dass niemand schreibend darauf zugreift. "
"Denken Sie auch daran, dass der Kernel bei der Suche nach Dateien "
"Schreibvorgänge auslösen kann."

#. type: Plain text
#: debian-bookworm
msgid "The I<device> name will usually have the following form:"
msgstr "Der Name des I<Gerät>es hat normalerweise die folgende Form:"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid ".sp\n"
msgstr ".sp\n"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "/dev/hda[1-63]"
msgstr "/dev/hda[1-63]"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "IDE disk 1"
msgstr "IDE-Platte 1"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "/dev/hdb[1-63]"
msgstr "/dev/hdb[1-63]"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "IDE disk 2"
msgstr "IDE-Platte 2"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "/dev/sda[1-15]"
msgstr "/dev/sda[1-15]"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "SCSI disk 1"
msgstr "SCSI-Platte 1"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "/dev/sdb[1-15]"
msgstr "/dev/sdb[1-15]"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "SCSI disk 2"
msgstr "SCSI-Platte 2"

#. type: Plain text
#: debian-bookworm
msgid ""
"If the filesystem was changed, i.e., repaired, then B<fsck.minix> will print "
"\"FILE SYSTEM HAS BEEN CHANGED\" and will B<sync>(2) three times before "
"exiting. There is I<no> need to reboot after check."
msgstr ""
"Wenn das Dateisystem geändert (repariert) wurde, dann meldet B<fsck.minix> "
"»DATEISYSTEM WURDE GEÄNDERT« und führt vor dem Beenden drei Mal B<sync>(2) "
"aus. Ein Neustart ist zu diesem Zeitpunkt I<nicht> nötig."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "WARNING"
msgstr "WARNUNG"

#. type: Plain text
#: debian-bookworm
msgid ""
"B<fsck.minix> should B<not> be used on a mounted filesystem. Using "
"B<fsck.minix> on a mounted filesystem is very dangerous, due to the "
"possibility that deleted files are still in use, and can seriously damage a "
"perfectly good filesystem! If you absolutely have to run B<fsck.minix> on a "
"mounted filesystem, such as the root filesystem, make sure nothing is "
"writing to the disk, and that no files are \"zombies\" waiting for deletion."
msgstr ""
"B<fsck.minix> sollte B<nicht> auf ein eingehängtes Dateisystem angewendet "
"werden. Dies ist sehr gefährlich, weil gelöschte Dateien möglicherweise noch "
"in Benutzung sind, was ein perfektes Dateisystem ernsthaft beschädigen kann! "
"Wenn Sie absolut keinen anderen Weg sehen, als B<fsck.minix> auf einem "
"eingehängten Dateisystem auszuführen (zum Beispiel der Dateisystemwurzel), "
"stellen Sie sicher, dass keine Schreibzugriffe stattfinden und keine Dateien "
"als »Zombies« auf das endgültige Löschen warten."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONEN"

#. type: Plain text
#: debian-bookworm
msgid "B<-l>, B<--list>"
msgstr "B<-l>, B<--list>"

#. type: Plain text
#: debian-bookworm
msgid "List all filenames."
msgstr "listet alle Dateinamen auf."

#. type: Plain text
#: debian-bookworm
msgid "B<-r>, B<--repair>"
msgstr "B<-r>, B<--repair>"

#. type: Plain text
#: debian-bookworm
msgid "Perform interactive repairs."
msgstr "führt interaktive Reparaturen durch."

#. type: Plain text
#: debian-bookworm
msgid "B<-a>, B<--auto>"
msgstr "B<-a>, B<--auto>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Perform automatic repairs. This option implies B<--repair> and serves to "
"answer all of the questions asked with the default. Note that this can be "
"extremely dangerous in the case of extensive filesystem damage."
msgstr ""
"führt automatische Reparaturen aus. Diese Option bezieht B<--repair> ein und "
"dient dazu, alle gestellten Fragen mit den Vorgabeeinstellungen zu "
"beantworten. Beachten Sie, dass dies extrem gefährlich sein kann, wenn das "
"Dateisystem schwer beschädigt ist."

#. type: Plain text
#: debian-bookworm
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: debian-bookworm
msgid "Be verbose."
msgstr "Ausführlicher Modus."

#. type: Plain text
#: debian-bookworm
msgid "B<-s>, B<--super>"
msgstr "B<-s>, B<--super>"

#. type: Plain text
#: debian-bookworm
msgid "Output super-block information."
msgstr "gibt Informationen zum Superblock aus."

#. type: Plain text
#: debian-bookworm
msgid "B<-m>, B<--uncleared>"
msgstr "B<-m>, B<--uncleared>"

#. type: Plain text
#: debian-bookworm
msgid "Activate MINIX-like \"mode not cleared\" warnings."
msgstr "aktiviert »Modus nicht zurückgesetzt«-Warnungen im MINIX-Stil."

#. type: Plain text
#: debian-bookworm
msgid "B<-f>, B<--force>"
msgstr "B<-f>, B<--force>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Force a filesystem check even if the filesystem was marked as valid. Marking "
"is done by the kernel when the filesystem is unmounted."
msgstr ""
"erzwingt eine Dateisystemüberprüfung, selbst wenn das Dateisystem als gültig "
"markiert wurde. Diese Markierung wird durch den Kernel vorgenommen, wenn das "
"Dateisystem ausgehängt wird."

#. type: Plain text
#: debian-bookworm
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: debian-bookworm
msgid "Display help text and exit."
msgstr "zeigt einen Hilfetext an und beendet das Programm."

#. type: Plain text
#: debian-bookworm
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: debian-bookworm
msgid "Print version and exit."
msgstr "zeigt die Versionsnummer an und beendet das Programm."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "DIAGNOSTICS"
msgstr "DIAGNOSE"

#. type: Plain text
#: debian-bookworm
msgid ""
"There are numerous diagnostic messages. The ones mentioned here are the most "
"commonly seen in normal usage."
msgstr ""
"Es gibt zahlreiche Diagnosemeldungen. Die hier erwähnten sind jene, die bei "
"gewöhnlicher Benutzung am häufigsten zu sehen sind."

#. type: Plain text
#: debian-bookworm
msgid ""
"If the device does not exist, B<fsck.minix> will print \"unable to read "
"super block\". If the device exists, but is not a MINIX filesystem, "
"B<fsck.minix> will print \"bad magic number in super-block\"."
msgstr ""
"Falls das Gerät nicht existiert, gibt B<fsck.minix> die Meldung »Superblock "
"konnte nicht gelesen werden« aus. Existiert das Gerät zwar, ist aber kein "
"MINIX-Dateisystem, wird die Meldung »Ungültige magische Zahl im Superblock« "
"ausgegeben."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "EXIT STATUS"
msgstr "EXIT-STATUS"

#. type: Plain text
#: debian-bookworm
msgid "The exit status returned by B<fsck.minix> is the sum of the following:"
msgstr ""
"Der Exit-Status von B<fsck.minix> ergibt sich aus den folgenden Bedingungen:"

#. type: Plain text
#: debian-bookworm
msgid "B<0>"
msgstr "B<0>"

#. type: Plain text
#: debian-bookworm
msgid "No errors"
msgstr "Keine Fehler"

#. type: Plain text
#: debian-bookworm
msgid "B<3>"
msgstr "B<3>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Filesystem errors corrected, system should be rebooted if filesystem was "
"mounted"
msgstr ""
"Dateisystemfehler wurden korrigiert, das System sollte neu gestartet werden, "
"falls das Dateisystem eingehängt war"

#. type: Plain text
#: debian-bookworm
msgid "B<4>"
msgstr "B<4>"

#. type: Plain text
#: debian-bookworm
msgid "Filesystem errors left uncorrected"
msgstr "Dateisystemfehler wurden nicht korrigiert"

#. type: Plain text
#: debian-bookworm
msgid "B<7>"
msgstr "B<7>"

#. type: Plain text
#: debian-bookworm
msgid "Combination of exit statuses 3 and 4"
msgstr "Kombination der Exit-Status 3 und 4"

#. type: Plain text
#: debian-bookworm
msgid "B<8>"
msgstr "B<8>"

#. type: Plain text
#: debian-bookworm
msgid "Operational error"
msgstr "Betriebsfehler"

#. type: Plain text
#: debian-bookworm
msgid "B<16>"
msgstr "B<16>"

#. type: Plain text
#: debian-bookworm
msgid "Usage or syntax error"
msgstr "Aufruf- oder Syntaxfehler"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AUTHORS"
msgstr "AUTOREN"

#. type: Plain text
#: debian-bookworm
msgid "Exit status values by"
msgstr "Exit-Status-Werte durch"

#. type: Plain text
#: debian-bookworm
msgid "Added support for filesystem valid flag:"
msgstr "Unterstützung für den Gültig-Schalter für Dateisysteme:"

#. type: Plain text
#: debian-bookworm
msgid "Check to prevent fsck of mounted filesystem added by"
msgstr ""
"Überprüfung, ob Fsck auf eingehängten Dateisystemen ausgeführt wird, von"

#. type: Plain text
#: debian-bookworm
msgid "Minix v2 fs support by"
msgstr "Minix-v2-Dateisystemunterstützung durch"

#. type: Plain text
#: debian-bookworm
msgid "updated by"
msgstr "aktualisiert von"

#. type: Plain text
#: debian-bookworm
msgid "Portability patch by"
msgstr "Portabilitäts-Patch durch"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: debian-bookworm
msgid ""
"B<fsck>(8), B<fsck.ext2>(8), B<mkfs>(8), B<mkfs.ext2>(8), B<mkfs.minix>(8), "
"B<reboot>(8)"
msgstr ""
"B<fsck>(8), B<fsck.ext2>(8), B<mkfs>(8), B<mkfs.ext2>(8), B<mkfs.minix>(8), "
"B<reboot>(8)"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "REPORTING BUGS"
msgstr "FEHLER MELDEN"

#. type: Plain text
#: debian-bookworm
msgid "For bug reports, use the issue tracker at"
msgstr "Verwenden Sie zum Melden von Fehlern das Fehlererfassungssystem auf"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AVAILABILITY"
msgstr "VERFÜGBARKEIT"

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<fsck.minix> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""
"Der Befehl B<fsck.minix> ist Teil des Pakets util-linux, welches "
"heruntergeladen werden kann von:"
