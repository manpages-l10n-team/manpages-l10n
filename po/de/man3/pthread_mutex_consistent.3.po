# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Helge Kreutzmann <debian@helgefjell.de>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.25.1\n"
"POT-Creation-Date: 2025-02-28 16:45+0100\n"
"PO-Revision-Date: 2025-02-02 07:43+0100\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "pthread_mutex_consistent"
msgstr "pthread_mutex_consistent"

#. type: TH
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "2024-07-23"
msgstr "23. Juli 2024"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr "Linux man-pages 6.12"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "pthread_mutex_consistent - make a robust mutex consistent"
msgstr "pthread_mutex_consistent - Einen robusten Mutex konsistent machen"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHEK"

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "POSIX threads library (I<libpthread>,\\ I<-lpthread>)"
msgstr "POSIX-Threads-Bibliothek (I<libpthread>,\\ I<-lpthread>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>pthread.hE<gt>>\n"
msgstr "B<#include E<lt>pthread.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<int pthread_mutex_consistent(pthread_mutex_t *>I<mutex>B<);>\n"
msgstr "B<int pthread_mutex_consistent(pthread_mutex_t *>I<mutex>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Mit Glibc erforderliche Feature-Test-Makros (siehe "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<pthread_mutex_consistent>():"
msgstr "B<pthread_mutex_consistent>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "    _POSIX_C_SOURCE E<gt>= 200809L\n"
msgstr "    _POSIX_C_SOURCE E<gt>= 200809L\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This function makes a robust mutex consistent if it is in an inconsistent "
"state.  A mutex can be left in an inconsistent state if its owner terminates "
"while holding the mutex, in which case the next owner who acquires the mutex "
"will succeed and be notified by a return value of B<EOWNERDEAD> from a call "
"to B<pthread_mutex_lock>()."
msgstr ""
"Diese Funktion macht einen robusten Mutex konsistent, falls er in einem "
"inkonsistenten Zustand ist. Ein Mutex kann in einem inkonsistenten Zustand "
"verbleiben, wenn sich sein Eigentümer beendet, während er den Mutex hält. In "
"diesem Fall wird der nächste Eigentümer, der den Mutex erlangt, erfolgreich "
"sein und durch einen Rückgabewert von B<EOWNERDEAD> bei einem Aufruf von "
"B<pthread_mutex_lock>() benachrichtigt werden."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "RÜCKGABEWERT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On success, I<pthread_mutex_consistent>()  returns 0.  Otherwise, it returns "
"a positive error number to indicate the error."
msgstr ""
"Im Erfolgsfall liefert I<pthread_mutex_consistent>() 0 zurück. Andernfalls "
"liefert sie eine positive Fehlernummer zurück, um den Fehler anzuzeigen."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "FEHLER"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The mutex is either not robust or is not in an inconsistent state."
msgstr ""
"Der Mutex ist entweder nicht robust oder nicht in einem inkonsistenten "
"Zustand."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "GESCHICHTE"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "glibc 2.12.  POSIX.1-2008."
msgstr "Glibc 2.12. POSIX.1-2008."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Before the addition of B<pthread_mutex_consistent>()  to POSIX, glibc "
"defined the following equivalent nonstandard function if B<_GNU_SOURCE> was "
"defined:"
msgstr ""
"Bevor B<pthread_mutex_consistent>() zu POSIX hinzugefügt wurde, definierte "
"Glibc die folgende, nicht standardisierte äquivalente Funktion, falls "
"B<_GNU_SOURCE> definiert war:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<[[deprecated]]>\n"
"B<int pthread_mutex_consistent_np(const pthread_mutex_t *>I<mutex>B<);>\n"
msgstr ""
"B<[[veraltet]]>\n"
"B<int pthread_mutex_consistent_np(const pthread_mutex_t *>I<mutex>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This GNU-specific API, which first appeared in glibc 2.4, is nowadays "
"obsolete and should not be used in new programs; since glibc 2.34 it has "
"been marked as deprecated."
msgstr ""
"Diese GNU-spezifische API, die erstmals in Glibc 2.4 erschien, ist "
"heutzutage veraltet und sollte in neuen Programmen nicht mehr verwandt "
"werden; seit Glibc 2.34 ist sie als veraltet markiert."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ANMERKUNGEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<pthread_mutex_consistent>()  simply informs the implementation that the "
"state (shared data)  guarded by the mutex has been restored to a consistent "
"state and that normal operations can now be performed with the mutex.  It is "
"the application's responsibility to ensure that the shared data has been "
"restored to a consistent state before calling B<pthread_mutex_consistent>()."
msgstr ""
"B<pthread_mutex_consistent>() informiert die Implementierung einfach, dass "
"der vom Mutex gesicherte Zustand (gemeinsam verwandte Daten) in einem "
"konsistenten Zustand wiederhergestellt wurde und dass jetzt normale Aktionen "
"mit dem Mutex durchgeführt werden können. Es liegt in der Verantwortung der "
"Anwendung sicherzustellen, dass die gemeinsamen Daten in einen konsistenten "
"Zustand wiederhergestellt wurden, bevor sie B<pthread_mutex_consistent>() "
"aufruft."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "BEISPIELE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "See B<pthread_mutexattr_setrobust>(3)."
msgstr "Siehe B<pthread_mutexattr_setrobust>(3)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<pthread_mutex_lock>(3), B<pthread_mutexattr_getrobust>(3), "
"B<pthread_mutexattr_init>(3), B<pthread_mutexattr_setrobust>(3), "
"B<pthreads>(7)"
msgstr ""
"B<pthread_mutex_lock>(3), B<pthread_mutexattr_getrobust>(3), "
"B<pthread_mutexattr_init>(3), B<pthread_mutexattr_setrobust>(3), "
"B<pthreads>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-12-04"
msgstr "4. Dezember 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "POSIX threads library (I<libpthread>, I<-lpthread>)"
msgstr "POSIX-Threads-Bibliothek (I<libpthread>, I<-lpthread>)"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONEN"

#. type: Plain text
#: debian-bookworm
msgid "B<pthread_mutex_consistent>()  was added in glibc 2.12."
msgstr "B<pthread_mutex_consistent>() wurde in Glibc 2.12 hinzugefügt."

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "2024-05-02"
msgstr "2. Mai 2024"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Linux man-pages 6.9.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31. Oktober 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Linux man-pages (unveröffentlicht)"
