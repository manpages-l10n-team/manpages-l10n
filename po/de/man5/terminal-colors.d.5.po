# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2014-2015, 2020-2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.18.1\n"
"POT-Creation-Date: 2024-03-29 09:55+0100\n"
"PO-Revision-Date: 2023-05-14 06:10+0200\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.12.2\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "TERMINAL-COLORS.D"
msgstr "TERMINAL-COLORS.D"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr "11. Mai 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "File formats"
msgstr "Dateiformate"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: debian-bookworm
msgid "terminal-colors.d - configure output colorization for various utilities"
msgstr ""
"terminal-colors.d - farbliche Darstellung der Ausgabe für verschiedene "
"Dienstprogramme einrichten"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: debian-bookworm
msgid "/etc/terminal-colors.d/I<[[name][@term].][type]>"
msgstr "/etc/terminal-colors.d/I<[[Name][@Terminal].][Typ]>"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: debian-bookworm
msgid ""
"Files in this directory determine the default behavior for utilities when "
"coloring output."
msgstr ""
"Die Dateien in diesem Verzeichnis bestimmen das Standardverhalten von "
"Dienstprogrammen, wenn deren Ausgabe farbig dargestellt wird."

#. type: Plain text
#: debian-bookworm
msgid ""
"The I<name> is a utility name. The name is optional and when none is "
"specified then the file is used for all unspecified utilities."
msgstr ""
"Der I<Name> ist der Name eines Dienstprogramms. Dieser Name ist optional; "
"wenn keiner angegeben ist, dann wird diese Datei für alle nicht explizit "
"angegebenen Dienstprogramme verwendet."

#. type: Plain text
#: debian-bookworm
msgid ""
"The I<term> is a terminal identifier (the B<TERM> environment variable). The "
"terminal identifier is optional and when none is specified then the file is "
"used for all unspecified terminals."
msgstr ""
"I<Terminal> ist ein Terminalbezeichner (die Umgebungsvariable B<TERM>). Der "
"Terminalbezeichner ist optional; wenn nichts angegeben ist, wird die Datei "
"auf alle ansonsten nicht spezifizierten Terminals angewendet."

#. type: Plain text
#: debian-bookworm
msgid "The I<type> is a file type. Supported file types are:"
msgstr "Der I<Typ> ist ein Dateityp. Folgende Typen werden unterstützt:"

#. type: Plain text
#: debian-bookworm
msgid "B<disable>"
msgstr "B<disable>"

#. type: Plain text
#: debian-bookworm
msgid "Turns off output colorization for all compatible utilities."
msgstr "schaltet die farbige Ausgabe aller kompatiblen Dienstprogramme aus."

#. type: Plain text
#: debian-bookworm
msgid "B<enable>"
msgstr "B<enable>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Turns on output colorization; any matching B<disable> files are ignored."
msgstr ""
"schaltet die farbige Darstellung ein; jede passende B<disable>-Datei wird "
"ignoriert."

#. type: Plain text
#: debian-bookworm
msgid "B<scheme>"
msgstr "B<scheme>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Specifies colors used for output. The file format may be specific to the "
"utility, the default format is described below."
msgstr ""
"legt die für die Ausgabe zu verwendenden Farben fest. Das Dateiformat kann "
"programmbedingt abweichen, das Standardformat wird nachfolgend beschrieben."

#. type: Plain text
#: debian-bookworm
msgid ""
"If there are more files that match for a utility, then the file with the "
"more specific filename wins. For example, the filename \"@xterm.scheme\" has "
"less priority than \"dmesg@xterm.scheme\". The lowest priority are those "
"files without a utility name and terminal identifier (e.g., \"disable\")."
msgstr ""
"Sollten mehrere Dateien existieren, die auf eines der Dienstprogramme passen "
"würden, dann wird die Datei mit dem spezifischeren Dateinamen bevorzugt. "
"Beispielsweise hat der Dateiname »@xterm.scheme« eine niedrigere Priorität "
"als »dmesg@xterm.scheme«. Die niedrigste Priorität haben die Dateien ohne "
"Programmname und Terminalbezeichner (zum Beispiel »disable«)."

#. type: Plain text
#: debian-bookworm
msgid ""
"The user-specific I<$XDG_CONFIG_HOME/terminal-colors.d> or I<$HOME/.config/"
"terminal-colors.d> overrides the global setting."
msgstr ""
"Die benutzerspezifischen I<$XDG_CONFIG_HOME/terminal-colors.d> oder "
"I<$HOME/.config/terminal-colors.d> setzen die globale Einstellung außer "
"Kraft."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "DEFAULT SCHEME FILES FORMAT"
msgstr "VORGEGEBENES FORMAT FÜR SCHEMA-DATEIEN"

#. type: Plain text
#: debian-bookworm
msgid "The following statement is recognized:"
msgstr "Die folgende Anweisung wird verarbeitet:"

#. type: Plain text
#: debian-bookworm
msgid "B<name color-sequence>"
msgstr "B<Name Farbsequenz>"

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<name> is a logical name of color sequence (for example \"error\"). The "
"names are specific to the utilities. For more details always see the "
"B<COLORS> section in the man page for the utility."
msgstr ""
"Der B<Name> ist ein logischer Name einer Farbsequenz (zum Beispiel »error«). "
"Die Namen sind vom jeweiligen Dienstprogramm abhängig. Weitere Details "
"finden Sie im Abschnitt B<FARBEN> der Handbuchseite des jeweiligen "
"Dienstprogramms."

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<color-sequence> is a color name, ASCII color sequences or escape "
"sequences."
msgstr ""
"Die B<Farbsequenz> kann ein Farbname, eine ASCII-Farbsequenz oder eine "
"Escape-Sequenz sein."

#. type: SS
#: debian-bookworm
#, no-wrap
msgid "Color names"
msgstr "Farbbezeichnungen"

#. type: Plain text
#: debian-bookworm
msgid ""
"black, blink, blue, bold, brown, cyan, darkgray, gray, green, halfbright, "
"lightblue, lightcyan, lightgray, lightgreen, lightmagenta, lightred, "
"magenta, red, reset, reverse, and yellow."
msgstr ""
"black, blink, blue, bold, brown, cyan, darkgray, gray, green, halfbright, "
"lightblue, lightcyan, lightgray, lightgreen, lightmagenta, lightred, "
"magenta, red, reset, reverse und yellow."

#. type: SS
#: debian-bookworm
#, no-wrap
msgid "ANSI color sequences"
msgstr "ANSI-Farbsequenzen"

#. type: Plain text
#: debian-bookworm
msgid ""
"The color sequences are composed of sequences of numbers separated by "
"semicolons. The most common codes are:"
msgstr ""
"Die Farbsequenzen werden aus Zahlenfolgen gebildet, die durch Semikola "
"getrennt sind. Die am häufigsten verwendeten Codes sind:"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid ".sp\n"
msgstr ".sp\n"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "0"
msgstr "0"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "to restore default color"
msgstr "voreingestellte Farbe wiederherstellen"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "1"
msgstr "1"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "for brighter colors"
msgstr "für hellere Farben"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "4"
msgstr "4"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "for underlined text"
msgstr "für unterstrichenen Text"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "5"
msgstr "5"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "for flashing text"
msgstr "für blinkenden Text"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "30"
msgstr "30"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "for black foreground"
msgstr "für schwarzen Vordergrund"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "31"
msgstr "31"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "for red foreground"
msgstr "für roten Vordergrund"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "32"
msgstr "32"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "for green foreground"
msgstr "für grünen Vordergrund"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "33"
msgstr "33"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "for yellow (or brown) foreground"
msgstr "für gelben (oder braunen) Vordergrund"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "34"
msgstr "34"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "for blue foreground"
msgstr "für blauen Vordergrund"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "35"
msgstr "35"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "for purple foreground"
msgstr "für Purpur-Vordergrund"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "36"
msgstr "36"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "for cyan foreground"
msgstr "für Cyan-Vordergrund"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "37"
msgstr "37"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "for white (or gray) foreground"
msgstr "für weißen (oder grauen) Vordergrund"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "40"
msgstr "40"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "for black background"
msgstr "für schwarzen Hintergrund"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "41"
msgstr "41"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "for red background"
msgstr "für roten Hintergrund"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "42"
msgstr "42"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "for green background"
msgstr "für grünen Hintergrund"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "43"
msgstr "43"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "for yellow (or brown) background"
msgstr "für gelben (oder braunen) Hintergrund"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "44"
msgstr "44"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "for blue background"
msgstr "für blauen Hintergrund"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "45"
msgstr "45"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "for purple background"
msgstr "für Purpur-Hintergrund"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "46"
msgstr "46"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "for cyan background"
msgstr "für Cyan-Hintergrund"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "47"
msgstr "47"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "for white (or gray) background"
msgstr "für weißen (oder grauen) Hintergrund"

#. type: SS
#: debian-bookworm
#, no-wrap
msgid "Escape sequences"
msgstr "Escape-Sequenzen"

#. type: Plain text
#: debian-bookworm
msgid ""
"To specify control or blank characters in the color sequences, C-style \\(rs-"
"escaped notation can be used:"
msgstr ""
"So fügen Sie Steuer- oder Leerzeichen in die Farbsequenzen ein, wobei "
"Maskierungen im C-Stil unterstützt werden:"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "B<\\(rsa>"
msgstr "B<\\(rsa>"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "Bell (ASCII 7)"
msgstr "Tonsignal (ASCII 7)"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "B<\\(rsb>"
msgstr "B<\\(rsb>"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "Backspace (ASCII 8)"
msgstr "Rückschritt (ASCII 8)"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "B<\\(rse>"
msgstr "B<\\(rse>"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "Escape (ASCII 27)"
msgstr "Escape (ASCII 27)"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "B<\\(rsf>"
msgstr "B<\\(rsf>"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "Form feed (ASCII 12)"
msgstr "Seitenvorschub (ASCII 12)"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "B<\\(rsn>"
msgstr "B<\\(rsn>"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "Newline (ASCII 10)"
msgstr "Zeilenvorschub (ASCII 10)"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "B<\\(rsr>"
msgstr "B<\\(rsr>"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "Carriage Return (ASCII 13)"
msgstr "Wagenrücklauf (ASCII 13)"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "B<\\(rst>"
msgstr "B<\\(rst>"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "Tab (ASCII 9)"
msgstr "Tabulator (ASCII 9)"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "B<\\(rsv>"
msgstr "B<\\(rsv>"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "Vertical Tab (ASCII 11)"
msgstr "Vertikaler Tabulator (ASCII 11)"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "B<\\(rs?>"
msgstr "B<\\(rs?>"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "Delete (ASCII 127)"
msgstr "Zeichen löschen (ASCII 127)"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "B<\\(rs_>"
msgstr "B<\\(rs_>"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "Space"
msgstr "Leerzeichen"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "B<\\(rs\\(rs>"
msgstr "B<\\(rs\\(rs>"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "Backslash (\\(rs)"
msgstr "Rückschrägstrich (\\(rs)"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "B<\\(rs^>"
msgstr "B<\\(rs^>"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "Caret (^)"
msgstr "Zirkumflex (^)"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "B<\\(rs#>"
msgstr "B<\\(rs#>"

#. type: tbl table
#: debian-bookworm
#, no-wrap
msgid "Hash mark (#)"
msgstr "Raute-Zeichen (#)"

#. type: Plain text
#: debian-bookworm
msgid ""
"Please note that escapes are necessary to enter a space, backslash, caret, "
"or any control character anywhere in the string, as well as a hash mark as "
"the first character."
msgstr ""
"Bitte beachten Sie, dass Maskierungen nötig sind, um ein Leerzeichen, einen "
"Rückschrägstrich, ein Caret oder ein anderes Steuerzeichen in die "
"Zeichenkette einzufügen, oder eine Raute als erstes Zeichen."

#. type: Plain text
#: debian-bookworm
msgid ""
"For example, to use a red background for alert messages in the output of "
"B<dmesg>(1), use:"
msgstr ""
"Um beispielsweise einen roten Hintergrund für Warnmeldungen in der Ausgabe "
"von B<dmesg>(1) anzeigen zu lassen, verwenden Sie:"

#. type: Plain text
#: debian-bookworm
msgid ""
"B<echo \\(aqalert 37;41\\(aq E<gt>E<gt> /etc/terminal-colors.d/dmesg.scheme>"
msgstr ""
"B<echo \\(aqalert 37;41\\(aq E<gt>E<gt> /etc/terminal-colors.d/dmesg.scheme>"

#. type: SS
#: debian-bookworm
#, no-wrap
msgid "Comments"
msgstr "Kommentare"

#. type: Plain text
#: debian-bookworm
msgid ""
"Lines where the first non-blank character is a # (hash) are ignored. Any "
"other use of the hash character is not interpreted as introducing a comment."
msgstr ""
"Zeilen, deren erstes Nicht-Leerzeichen eine Raute ist (#), werden ignoriert. "
"Jede sonstige Verwendung des Raute-Zeichens wird nicht als Einleitung eines "
"Kommentars interpretiert."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "ENVIRONMENT"
msgstr "UMGEBUNGSVARIABLEN"

#. type: Plain text
#: debian-bookworm
msgid "B<TERMINAL_COLORS_DEBUG>=all"
msgstr "B<TERMINAL_COLORS_DEBUG>=all"

#. type: Plain text
#: debian-bookworm
msgid "enables debug output."
msgstr "aktiviert die Debug-Ausgabe."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "FILES"
msgstr "DATEIEN"

#. type: Plain text
#: debian-bookworm
msgid "I<$XDG_CONFIG_HOME/terminal-colors.d>"
msgstr "I<$XDG_CONFIG_HOME/terminal-colors.d>"

#. type: Plain text
#: debian-bookworm
msgid "I<$HOME/.config/terminal-colors.d>"
msgstr "I<$HOME/.config/terminal-colors.d>"

#. type: Plain text
#: debian-bookworm
msgid "I</etc/terminal-colors.d>"
msgstr "I</etc/terminal-colors.d>"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "EXAMPLE"
msgstr "BEISPIEL"

#. type: Plain text
#: debian-bookworm
msgid "Disable colors for all compatible utilities:"
msgstr "Farbige Ausgabe für alle kompatiblen Dienstprogramme deaktivieren:"

#. type: Plain text
#: debian-bookworm
msgid "B<touch /etc/terminal-colors.d/disable>"
msgstr "B<touch /etc/terminal-colors.d/disable>"

#. type: Plain text
#: debian-bookworm
msgid "Disable colors for all compatible utils on a vt100 terminal:"
msgstr ""
"Farbige Ausgabe für alle kompatiblen Dienstprogramme in einem VT100-Terminal "
"deaktivieren:"

#. type: Plain text
#: debian-bookworm
msgid "B<touch /etc/terminal-colors.d/@vt100.disable>"
msgstr "B<touch /etc/terminal-colors.d/@vt100.disable>"

#. type: Plain text
#: debian-bookworm
msgid "Disable colors for all compatible utils except B<dmesg>(1):"
msgstr ""
"Farbige Ausgabe für alle kompatiblen Dienstprogramme außer B<dmesg>(1) "
"deaktivieren:"

#. type: Plain text
#: debian-bookworm
msgid "B<touch /etc/terminal-colors.d/dmesg.enable>"
msgstr "B<touch /etc/terminal-colors.d/dmesg.enable>"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "COMPATIBILITY"
msgstr "KOMPATIBILITÄT"

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<terminal-colors.d> functionality is currently supported by all util-"
"linux utilities which provides colorized output. For more details always see "
"the B<COLORS> section in the man page for the utility."
msgstr ""
"Die Funktionalität von B<terminal-colors.d> wird derzeit von allen "
"Dienstprogrammen aus util-linux unterstützt, die ihre Ausgaben farbig "
"darstellen können. Weitere Informationen finden Sie im Abschnitt B<FARBEN> "
"der Handbuchseite des jeweiligen Programms."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "REPORTING BUGS"
msgstr "FEHLER MELDEN"

#. type: Plain text
#: debian-bookworm
msgid "For bug reports, use the issue tracker at"
msgstr "Verwenden Sie zum Melden von Fehlern das Fehlererfassungssystem auf"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AVAILABILITY"
msgstr "VERFÜGBARKEIT"

#. type: Plain text
#: debian-bookworm
msgid ""
"B<terminal-colors.d> is part of the util-linux package which can be "
"downloaded from"
msgstr ""
"B<terminal-colors.d> ist Teil des Pakets util-linux, welches heruntergeladen "
"werden kann von:"
