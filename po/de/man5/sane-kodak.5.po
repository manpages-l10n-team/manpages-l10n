# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2020.
# Helge Kreutzmann <debian@helgefjell.de>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.12.1\n"
"POT-Creation-Date: 2025-02-16 06:00+0100\n"
"PO-Revision-Date: 2022-01-29 06:36+0100\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.12.0\n"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "sane-kodak"
msgstr "sane-kodak"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "10 Feb 2010"
msgstr "10. Februar 2010"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SANE Scanner Access Now Easy"
msgstr "SANE Scanner Access Now Easy"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "sane-kodak - SANE backend for big Kodak flatbed and ADF scanners"
msgstr ""
"sane-kodak - SANE-Backend für Großformat-Flachbett- und ADF-Scanner von Kodak"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<sane-kodak> library implements a SANE (Scanner Access Now Easy) "
"backend which provides access to large Kodak flatbed and ADF scanners."
msgstr ""
"Die Bibliothek B<sane-kodak> implementiert ein SANE-(Scanner Access Now "
"Easy) Backend zum Zugriff auf die Großformat-Flachbett- und ADF-Scanner von "
"Kodak."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This document describes backend version 7, which shipped with SANE 1.0.21."
msgstr ""
"Dieses Dokument beschreibt das Backend in Version 7, welches mit SANE 1.0.21 "
"geliefert wird."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SUPPORTED HARDWARE"
msgstr "UNTERSTÜTZTE HARDWARE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This version should support models which speak the Kodak SCSI and Firewire "
"protocols. The i1860 was used to develop the backend, but other models may "
"work with only minimal modifications. Please see the list at I<http://"
"www.sane-project.org/sane-supported-devices.html> for an updated list."
msgstr ""
"Diese Version sollte Modelle unterstützen, welche über die SCSI- und "
"Firewire-Protokolle von Kodak kommunizieren. Zum Entwickeln des Backends kam "
"der i1860 zum Einsatz, aber andere Modelle sollten mit lediglich minimalen "
"Anpassungen funktionieren. Eine aktualisierte Liste finden Sie auf I<http://"
"www.sane-project.org/sane-supported-devices.html>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If you have a machine not on that list, or reported as 'untested': the best "
"way to determine level of support is to test the scanner directly, or to "
"collect a trace of the windows driver in action.  Please contact the author "
"for help or with test results."
msgstr ""
"Falls Sie eine Maschine besitzen, die nicht auf der Liste steht oder als "
"»untested«: Die beste Art, den Unterstützungstatus zu ermitteln, ist, den "
"Scanner direkt zu testen oder einen Mitschnitt des Windows-Treibers im "
"Betrieb aufzunehmen. Bitte nehmen Sie (auf Englisch) mit dem Autor für Hilfe "
"oder mit Testergebnissen Kontakt auf."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "UNSUPPORTED HARDWARE"
msgstr "NICHT UNTERSTÜTZTE HARDWARE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Most of the recent Kodak consumer or workgroup level machines are based on "
"other chipsets and are not supported by this backend. Some of these scanners "
"may be supported by another backend."
msgstr ""
"Die meisten der aktuellen Kodak-Geräte für Endkunden- oder "
"Unternehmenseinsatz basieren auf anderen Chipsätzen und werden von diesem "
"Backend nicht unterstützt. Einige dieser Scanner könnten von anderen "
"Backends unterstützt werden."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Effort has been made to expose the basic hardware options, including:"
msgstr ""
"Es wurde versucht, die grundlegenden Optionen der Hardware offenzulegen, "
"einschließlich:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<--source s>"
msgstr "B<--source s>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Selects the source for the scan. Options may include \"Flatbed\", \"ADF "
"Front\", \"ADF Back\", \"ADF Duplex\"."
msgstr ""
"wählt die Quelle für den Scan. Optionen können »Flatbed«, »ADF Front«, »ADF "
"Back« oder »ADF Duplex« sein."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<--mode m>"
msgstr "B<--mode m>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Selects the mode for the scan. Options may include \"Lineart\", "
"\"Halftone\", \"Gray\", and \"Color\"."
msgstr ""
"wählt den Scanmodus. Optionen können »Lineart«, »Halftone«, »Gray« und "
"»Color« sein."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<--resolution>"
msgstr "B<--resolution>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Controls scan resolution. Available choices may be limited by mode."
msgstr ""
"Wählt die Scan-Auflösung. Die verfügbaren Auflösungen sind vom Modus "
"abhängig."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<--tl-x>, B<--tl-y>, B<--br-x>, B<--br-y>"
msgstr "B<--tl-x>, B<--tl-y>, B<--br-x>, B<--br-y>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Sets scan area upper left and lower right coordinates. These are renamed B<-"
"t>, B<-l>, B<-x>, B<-y> by some frontends."
msgstr ""
"setzt die oberen linken und unteren rechten Koordinaten des Scan-Bereichs. "
"Manche Oberflächen benennen diese in B<-t>, B<-l>, B<-x>, B<-y> um."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<--page-width>, B<--page-height>"
msgstr "B<--page-width>, B<--page-height>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Sets paper size. Used by scanner to determine centering of scan coordinates "
"when using the ADF (Automatic Document Feeder) and to detect double feed "
"errors."
msgstr ""
"setzt die Papiergröße. Wird von Scannern verwendet, um beim Einsatz des "
"automatischen Dokumenteneinzugs (ADF) die Zentrierung der Scan-Koordinaten "
"zu bestimmen und zu erkennen, ob mehrere Seiten gleichzeitig eingezogen "
"wurden."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Other options will be available based on the capabilities of the scanner.  "
"Use I<scanimage --help> to get a list, but be aware that some options may be "
"settable only when another option has been set, and that advanced options "
"may be hidden by some frontend programs."
msgstr ""
"Andere Optionen könnten abhängig von den Fähigkeiten des Scanners verfügbar "
"sein. Verwenden Sie I<scanimage --help> für eine Liste, aber denken Sie "
"daran, dass einige Optionen nur gesetzt werden können, wenn auch andere "
"Optionen gesetzt werden konnten und dass einige fortgeschrittene Optionen "
"durch einige Programmoberflächen versteckt sein könnten."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "CONFIGURATION FILE"
msgstr "KONFIGURATIONSDATEI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The configuration file I<kodak.conf> is used to tell the backend how to look "
"for scanners, and provide options controlling the operation of the backend.  "
"This file is read each time the frontend asks the backend for a list of "
"scanners, generally only when the frontend starts. If the configuration file "
"is missing, the backend will use a set of compiled defaults, which are "
"identical to the default configuration file shipped with SANE."
msgstr ""
"Die Konfigurationsdatei I<kodak.conf> wird dazu verwandt, dem Backend "
"mitzuteilen, wie nach Scannern gesucht werden soll und stellt Optionen zum "
"Steuern der Aktionen des Backends bereit. Diese Datei wird jedes Mal "
"gelesen, wenn die Oberfläche das Backend um eine Liste von Scannern bittet, "
"im Allgemeinen nur, wenn die Oberfläche gestartet wird. Falls die "
"Konfigurationsdatei fehlt, wird das Backend eine Reihe von einkompilierten "
"Vorgaben verwenden, die mit der Vorgabe-Konfigurationsdatei übereinstimmen, "
"die mit SANE ausgeliefert wird."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Scanners can be specified in the configuration file in 2 ways:"
msgstr ""
"Scanner können in der Konfigurationsdatei auf zwei Arten festgelegt werden:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "scsi KODAK"
msgstr "scsi KODAK"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Requests backend to search all scsi buses in the system for a device which "
"reports itself to be a scanner made by 'KODAK'."
msgstr ""
"weist das Backend an, alle SCSI-Busse in dem System nach einem Gerät zu "
"durchsuchen, das sich selbst als ein von »KODAK« hergestellter Scanner "
"meldet."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "\"scsi /dev/sg0\" (or other scsi device file)"
msgstr "»scsi /dev/sg0« (oder eine andere SCSI-Gerätedatei)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Requests backend to open the named scsi device. Only useful if you have "
"multiple compatible scanners connected to your system, and need to specify "
"one. Probably should not be used with the other \"scsi\" line above."
msgstr ""
"weist das Backend an, das benannte SCSI-Gerät zu öffnen. Nur nützlich, wenn "
"mehrere kompatible Scanner an Ihrem System angeschlossen sind und Sie einen "
"auswählen müssen. Dies sollte wahrscheinlich nicht mit einer der anderen "
"oben dargestellten »scsi«-Zeilen verwendet werden."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The only configuration option supported is \"buffer-size=xxx\", allowing you "
"to set the number of bytes in the data buffer to something other than the "
"compiled-in default, 32768 (32K). Some users report that their scanner will "
"\"hang\" mid-page, or fail to transmit the image if the buffer is not large "
"enough."
msgstr ""
"Die einzige unterstützte Konfigurationsoption ist »buffer-size=xxx«, die es "
"Ihnen ermöglicht, die Anzahl der Byte in dem Datenpuffer auf etwas anderes "
"als die einkompilierte Vorgabe 32768 (32 kB) zu setzen. Einige Benutzer "
"berichten, dass ihr Scanner sich in der Mitte der Seite »aufhängt« oder beim "
"Übertragen des Bildes versagt, falls der Puffer nicht groß genug ist."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Note: This option may appear multiple times in the configuration file. It "
"only applies to scanners discovered by 'scsi/usb' lines that follow this "
"option."
msgstr ""
"Hinweis: Diese Option kann mehrfach in der Konfigurationsdatei auftauchen. "
"Sie gilt nur für Scanner, die mit den »scsi/usb«-Zeilen ermittelt werden, "
"die dieser Option folgen."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Note: The backend does not place an upper bound on this value, as some users "
"required it to be quite large. Values above the default are not recommended, "
"and may crash your OS or lockup your scsi card driver. You have been warned."
msgstr ""
"Hinweis: Das Backend legt keine obere Grenze an diesen Wert an, da einige "
"Benutzer einen sehr großen Wert benötigen. Werte oberhalb der Vorgabe werden "
"nicht empfohlen und könnten Ihr Betriebssystem zum Absturz bringen oder "
"Ihren SCSI-Kartentreiber sperren. Sie wurden gewarnt."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ENVIRONMENT"
msgstr "UMGEBUNGSVARIABLEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The backend uses a single environment variable, B<SANE_DEBUG_KODAK>, which "
"enables debugging output to stderr. Valid values are:"
msgstr ""
"Das Backend verwendet B<SANE_DEBUG_KODAK> als einzige Umgebungsvariable, "
"welche bewirkt, dass Debugging-Ausgaben in die Standardfehlerausgabe "
"geschrieben werden. Zulässige Werte sind:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "5 Errors"
msgstr "5 Fehler"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "10 Function trace"
msgstr "10 Funktionsdatenspuren"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "15 Function detail"
msgstr "15 Funktionsdetails"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "20 Option commands"
msgstr "20 Optionsbefehle"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "25 SCSI trace"
msgstr "25 SCSI-Ablaufverfolgung"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "30 SCSI detail"
msgstr "30 SCSI-Detail"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "35 Useless noise"
msgstr "35 Nutzloses Rauschen"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "KNOWN ISSUES"
msgstr "BEKANNTE PROBLEME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Most hardware options are either not supported or not exposed for control by "
"the user, including: multifeed detection, image compression, autocropping, "
"endorser, thresholding, multi-stream, etc."
msgstr ""
"Die meisten Hardwareoptionen werden entweder nicht unterstützt oder können "
"vom Benutzer nicht direkt gesteuert werden. Dies betrifft die Erkennung von "
"Mehrfacheinzügen, die Bildkompression, automatisches Beschneiden, Markierer, "
"Schwellwert, Mehrfachdatenströme usw."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "CREDITS"
msgstr "DANKSAGUNGEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The various authors of the B<sane-fujitsu>(5)  backend provided useful code."
msgstr ""
"Die verschiedenen Autoren des Backends B<sane-fujitsu>(5) stellten "
"nützlichen Code zur Verfügung."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Kodak provided access to hardware, documentation and personnel."
msgstr ""
"Kodak stellte Zugriff auf Hardware, Dokumentation und Personal zur Verfügung."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<sane>(7), B<sane-scsi>(5), B<scanimage>(1)"
msgstr "B<sane>(7), B<sane-scsi>(5), B<scanimage>(1)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "m. allan noah: E<lt>I<kitno455 a t gmail d o t com>E<gt>"
msgstr "m. allan noah: E<lt>I<kitno455 a t gmail d o t com>E<gt>"
