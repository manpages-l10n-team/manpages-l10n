# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Martin Eberhard Schauer <Martin.E.Schauer@gmx.de>, 2012.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.21.0\n"
"POT-Creation-Date: 2025-02-28 16:47+0100\n"
"PO-Revision-Date: 2024-02-16 17:43+0100\n"
"Last-Translator: Dr. Tobias Quathamer <toddy@debian.org>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "rtc"
msgstr "rtc"

#. type: TH
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2. Mai 2024"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr "Linux man-pages 6.12"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "rtc - real-time clock"
msgstr "rtc - Echtzeituhr"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "#include E<lt>linux/rtc.hE<gt>\n"
msgstr "#include E<lt>linux/rtc.hE<gt>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<int ioctl(>I<fd>B<, RTC_>I<request>B<, >I<param>B<);>\n"
msgstr "B<int ioctl(>I<fd>B<, RTC_>I<request>B<, >I<param>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

# Doch besser Echtzeituhr?
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "This is the interface to drivers for real-time clocks (RTCs)."
msgstr "Dies ist die Schnittstelle zu Treibern für Echtzeit-Uhren (RTCs)."

# Auch hier Verbindungen mit Uhr zusammen schreiben?
# Noch nicht geklärt: Chris schlägt vor:
# s/Interrupts/Unterbrechungen/
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Most computers have one or more hardware clocks which record the current "
"\"wall clock\" time.  These are called \"Real Time Clocks\" (RTCs).  One of "
"these usually has battery backup power so that it tracks the time even while "
"the computer is turned off.  RTCs often provide alarms and other interrupts."
msgstr ""
"Die meisten Computer verfügen über eine oder mehrere Hardware-Uhren, die die "
"aktuelle »Wanduhr«-Zeit erfassen. Diese werden als »Real Time Clocks« (RTC) "
"bezeichnet. Eine von ihnen ist in der Regel batteriegepuffert, sodass sie "
"die Zeit verfolgt, auch während der Computer ausgeschaltet ist. RTCs stellen "
"oft Alarme und andere Interrupts bereit."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"All i386 PCs, and ACPI-based systems, have an RTC that is compatible with "
"the Motorola MC146818 chip on the original PC/AT.  Today such an RTC is "
"usually integrated into the mainboard's chipset (south bridge), and uses a "
"replaceable coin-sized backup battery."
msgstr ""
"In allen i386-PCs und ACPI-basierten Systemen ist eine RTC eingebaut, die "
"kompatibel zum Chip aus dem Originalen PC/AT ist, dem Motorola MC146818. "
"Heutzutage ist solch eine RTC im Allgemeinen in den Chipsatz des Mainboards "
"(South Bridge) integriert und nutzt eine austauschbare münzgroße Batterie."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Non-PC systems, such as embedded systems built around system-on-chip "
"processors, use other implementations.  They usually won't offer the same "
"functionality as the RTC from a PC/AT."
msgstr ""
"Nicht-PC-Systeme wie beispielsweise eingebettete Systeme, die um »System-on-"
"chip«-Prozessoren aufgebaut sind, nutzen andere Implementierungen. "
"Üblicherweise bieten sie nicht die gleiche Funktionalität wie die RTC aus "
"einem PC/AT."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RTC vs system clock"
msgstr "RTC im Vergleich zur Systemuhr"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"RTCs should not be confused with the system clock, which is a software clock "
"maintained by the kernel and used to implement B<gettimeofday>(2)  and "
"B<time>(2), as well as setting timestamps on files, and so on.  The system "
"clock reports seconds and microseconds since a start point, defined to be "
"the POSIX Epoch: 1970-01-01 00:00:00 +0000 (UTC).  (One common "
"implementation counts timer interrupts, once per \"jiffy\", at a frequency "
"of 100, 250, or 1000 Hz.)  That is, it is supposed to report wall clock "
"time, which RTCs also do."
msgstr ""
"RTCs sollten nicht mit der Systemuhr verwechselt werden. Diese ist eine "
"Software-Uhr, die vom Kernel gepflegt wird. Er verwendet sie für die "
"Implementierung von B<gettimeofday>(2) und B<time>(2) sowie für die "
"Zeitstempel von Dateien usw. Die Systemuhr zählt Sekunden und Mikrosekunden "
"seit einem Startpunkt, der als die »POSIX Epoch« (1970-01-01 00:00:00 +0000 "
"(UTC)) definiert ist. (Eine verbreitete Umsetzung zählt Timer-Interrupts, "
"einmal pro »Jiffy«, bei einer Frequenz von 100, 250 oder 1000 Hz). Das "
"heißt, sie sollte die »Wanduhr«-Zeit angeben, wie es auch RTCS tun."

# s/ Zustand niedrigen Energieverbrauchs/Energiesparmodus/ ?
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"A key difference between an RTC and the system clock is that RTCs run even "
"when the system is in a low power state (including \"off\"), and the system "
"clock can't.  Until it is initialized, the system clock can only report time "
"since system boot ... not since the POSIX Epoch.  So at boot time, and after "
"resuming from a system low power state, the system clock will often be set "
"to the current wall clock time using an RTC.  Systems without an RTC need to "
"set the system clock using another clock, maybe across the network or by "
"entering that data manually."
msgstr ""
"Ein wesentlicher Unterschied zwischen einer RTC und der Systemuhr ist, dass "
"RTCs auch dann laufen, wenn sich das System in einem Zustand niedrigen "
"Energieverbrauchs (einschließlich »ausgeschaltet«) befindet, und die "
"Systemuhr das nicht kann. Bis sie initialisiert wird, kann die Systemuhr nur "
"die Zeit seit Systemstart berichten … und nicht seit der POSIX-Epoche. Also "
"wird beim Booten und nach der Rückkehr aus einem Niedrigenergie-Zustand des "
"Systems die Systemuhr oft über eine RTC auf die aktuelle Wanduhr-Zeit "
"gesetzt werden. Systeme ohne eine RTC müssen die Systemuhr mit einer anderen "
"Uhr stellen, vielleicht über das Netzwerk oder durch die manuelle Eingabe "
"der Daten."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RTC functionality"
msgstr "RTC-Funktionen"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"RTCs can be read and written with B<hwclock>(8), or directly with the "
"B<ioctl>(2)  requests listed below."
msgstr ""
"RTCs können direkt mit den im Folgenden aufgeführten B<ioctl>(2)-Aufrufen "
"und mittels B<hwclock>(8) gelesen und geschrieben werden."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Besides tracking the date and time, many RTCs can also generate interrupts"
msgstr ""
"Neben der Verfolgung von Zeit und Datum können viele RTCs Interrupts erzeugen"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr "\\[bu]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "on every clock update (i.e., once per second);"
msgstr "bei jeder Aktualisierung der Uhr (d.h. einmal pro Sekunde);"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"at periodic intervals with a frequency that can be set to any power-of-2 "
"multiple in the range 2 Hz to 8192 Hz;"
msgstr ""
"in periodischen Abständen mit einer Frequenz, die ein Mehrfaches einer "
"beliebigen Zweierpotenz im Bereich zwischen 2 Hz und 8192 Hz ist;"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "on reaching a previously specified alarm time."
msgstr "bei Erreichen einer vorher festgelegten Alarmzeit."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Each of those interrupt sources can be enabled or disabled separately.  On "
"many systems, the alarm interrupt can be configured as a system wakeup "
"event, which can resume the system from a low power state such as Suspend-to-"
"RAM (STR, called S3 in ACPI systems), Hibernation (called S4 in ACPI "
"systems), or even \"off\" (called S5 in ACPI systems).  On some systems, the "
"battery backed RTC can't issue interrupts, but another one can."
msgstr ""
"Jede dieser Interruptquellen kann separat aktiviert oder deaktiviert werden. "
"Auf vielen Systemen kann ein solch ein Interrupt als ein Ereignis zum "
"»Wecken« aus einem Energiesparzustand wie  Suspend-to-RAM (STR, auf ACPI-"
"Systemen S3 genannt), Hibernation (S4 auf ACPI-Systemen) oder sogar »aus« "
"(S5) konfiguriert werden. Auf manchen Systemen kann nicht die "
"batteriegepufferte RTC Interrupts auslösen, sondern eine andere."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The I</dev/rtc> (or I</dev/rtc0>, I</dev/rtc1>, etc.)  device can be opened "
"only once (until it is closed) and it is read-only.  On B<read>(2)  and "
"B<select>(2)  the calling process is blocked until the next interrupt from "
"that RTC is received.  Following the interrupt, the process can read a long "
"integer, of which the least significant byte contains a bit mask encoding "
"the types of interrupt that occurred, while the remaining 3 bytes contain "
"the number of interrupts since the last B<read>(2)."
msgstr ""
"Das Gerät I</dev/rtc> (oder I</dev/rtc0>, I</dev/rtc1> usw.) kann nur einmal "
"(bis es wieder geschlossen wird) und nur für einen Lesezugriff geöffnet "
"werden. Beim Aufruf von B<read>(2) und B<select>(2) wird der aufrufende "
"Prozess blockiert, bis er den nächsten Interrupt von dieser RTC empfängt. Im "
"Anschluss an den Interrupt kann der Prozess einen »long integer« auslesen. "
"Dessen niedrigstwertiges Byte enthält eine Bitmaske, die die Typen der "
"aufgetretenen Interrupts codiert, während die verbleibenden 3 Bytes die "
"Anzahl von Interrupts seit dem letzten B<read>(2) enthalten."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ioctl(2) interface"
msgstr "B<ioctl>(2)-Schnittstelle"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The following B<ioctl>(2)  requests are defined on file descriptors "
"connected to RTC devices:"
msgstr ""
"Die folgenden B<ioctl>(2)-Anfragen sind für mit RTC-Geräten verbundene "
"Dateideskriptoren definiert:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<RTC_RD_TIME>"
msgstr "B<RTC_RD_TIME>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Returns this RTC's time in the following structure:"
msgstr "gibt die Zeit der RTC in der folgenden Struktur zurück:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"struct rtc_time {\n"
"    int tm_sec;\n"
"    int tm_min;\n"
"    int tm_hour;\n"
"    int tm_mday;\n"
"    int tm_mon;\n"
"    int tm_year;\n"
"    int tm_wday;     /* unused */\n"
"    int tm_yday;     /* unused */\n"
"    int tm_isdst;    /* unused */\n"
"};\n"
msgstr ""
"struct rtc_time {\n"
"    int tm_sec;\n"
"    int tm_min;\n"
"    int tm_hour;\n"
"    int tm_mday;\n"
"    int tm_mon;\n"
"    int tm_year;\n"
"    int tm_wday;     /* nicht verwendet */\n"
"    int tm_yday;     /* nicht verwendet */\n"
"    int tm_isdst;    /* nicht verwendet */\n"
"};\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The fields in this structure have the same meaning and ranges as for the "
"I<tm> structure described in B<gmtime>(3).  A pointer to this structure "
"should be passed as the third B<ioctl>(2)  argument."
msgstr ""
"Die Felder dieser Struktur haben die gleiche Bedeutung und die gleichen "
"Wertebereiche wie die in B<gmtime>(3) beschriebene Strukur I<tm>. Als "
"drittes Argument von B<ioctl>(2) sollte ein Zeiger auf diese Strukur "
"übergeben werden."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<RTC_SET_TIME>"
msgstr "B<RTC_SET_TIME>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Sets this RTC's time to the time specified by the I<rtc_time> structure "
"pointed to by the third B<ioctl>(2)  argument.  To set the RTC's time the "
"process must be privileged (i.e., have the B<CAP_SYS_TIME> capability)."
msgstr ""
"setzt die Zeit der RTC auf die in der I<rtc_time>-Struktur festgelegte Zeit, "
"auf die das dritte B<ioctl>(2)-Argument zeigt. Zum Setzen der Zeit muss der "
"Prozess privilegiert sein (d.h. über die Capability B<CAP_SYS_TIME> "
"verfügen)."

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<RTC_ALM_READ>"
msgstr "B<RTC_ALM_READ>"

#. type: TQ
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<RTC_ALM_SET>"
msgstr "B<RTC_ALM_SET>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Read and set the alarm time, for RTCs that support alarms.  The alarm "
"interrupt must be separately enabled or disabled using the B<RTC_AIE_ON>, "
"B<RTC_AIE_OFF> requests.  The third B<ioctl>(2)  argument is a pointer to an "
"I<rtc_time> structure.  Only the I<tm_sec>, I<tm_min>, and I<tm_hour> fields "
"of this structure are used."
msgstr ""
"liest und setzt die Alarmzeit bei RTCs, die Alarme unterstützen. Der "
"Interrupt für den Alarm muss separat mit den Anfragen B<RTC_AIE_ON> und "
"B<RTC_AIE_OFF> aktiviert oder deaktiviert werden. Das dritte Argument für "
"B<ioctl>(2) ist ein Zeiger auf eine I<rtc_time>-Struktur. Von der Struktur "
"werden lediglich die Felder I<tm_sec>, I<tm_min> und I<tm_hour> ausgewertet."

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<RTC_IRQP_READ>"
msgstr "B<RTC_IRQP_READ>"

#. type: TQ
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<RTC_IRQP_SET>"
msgstr "B<RTC_IRQP_SET>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Read and set the frequency for periodic interrupts, for RTCs that support "
"periodic interrupts.  The periodic interrupt must be separately enabled or "
"disabled using the B<RTC_PIE_ON>, B<RTC_PIE_OFF> requests.  The third "
"B<ioctl>(2)  argument is an I<unsigned long\\ *> or an I<unsigned long>, "
"respectively.  The value is the frequency in interrupts per second.  The set "
"of allowable frequencies is the multiples of two in the range 2 to 8192.  "
"Only a privileged process (i.e., one having the B<CAP_SYS_RESOURCE> "
"capability) can set frequencies above the value specified in I</proc/sys/dev/"
"rtc/max-user-freq>.  (This file contains the value 64 by default.)"
msgstr ""
"liest und setzt die Frequenz periodischer Interrupts bei RTCs, die "
"periodische Interrupts unterstützen. Der periodische Interrupt muss separat "
"mit den Anfragen B<RTC_PIE_ON> und B<RTC_PIE_OFF> aktiviert oder deaktiviert "
"werden. Das dritte Argument von B<ioctl>(2) ist ein I<unsigned long\\ *> "
"beziehungsweise ein I<unsigned long>. Der Wert ist die Anzahl der Interrupts "
"pro Sekunde. Der Satz von zulässigen Frequenzen besteht aus Vielfachen von "
"zwei aus dem Bereich von 2 bis 8192. Nur ein privilegierter Prozess (d.h. "
"einer mit der B<CAP_SYS_RESOURCE>-Capability) kann Frequenzen über dem in I</"
"proc/sys/dev/rtc/max-user-freq> festgelegten Wert festlegen. (Diese Datei "
"enthält standardmäßig den Wert 64.)"

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<RTC_AIE_ON>"
msgstr "B<RTC_AIE_ON>"

#. type: TQ
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<RTC_AIE_OFF>"
msgstr "B<RTC_AIE_OFF>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Enable or disable the alarm interrupt, for RTCs that support alarms.  The "
"third B<ioctl>(2)  argument is ignored."
msgstr ""
"aktiviert oder deaktiviert den Alarm-Interrupt für RTCs, die Alarme "
"unterstützen. Das dritte Argument von B<ioctl>(2) wird ignoriert."

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<RTC_UIE_ON>"
msgstr "B<RTC_UIE_ON>"

#. type: TQ
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<RTC_UIE_OFF>"
msgstr "B<RTC_UIE_OFF>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Enable or disable the interrupt on every clock update, for RTCs that support "
"this once-per-second interrupt.  The third B<ioctl>(2)  argument is ignored."
msgstr ""
"aktiviert oder deaktiviert den Interrupt bei jeder Aktualisierung der Uhr "
"für RTCs, die diesen »einmal pro Sekunde«-Interrupt unterstützen. Das dritte "
"Argument von B<ioctl>(2) wird ignoriert."

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<RTC_PIE_ON>"
msgstr "B<RTC_PIE_ON>"

#. type: TQ
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<RTC_PIE_OFF>"
msgstr "B<RTC_PIE_OFF>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Enable or disable the periodic interrupt, for RTCs that support these "
"periodic interrupts.  The third B<ioctl>(2)  argument is ignored.  Only a "
"privileged process (i.e., one having the B<CAP_SYS_RESOURCE> capability) can "
"enable the periodic interrupt if the frequency is currently set above the "
"value specified in I</proc/sys/dev/rtc/max-user-freq>."
msgstr ""
"aktiviert oder deaktiviert den periodischen Interrupt für RTCs, die diese "
"periodischen Interrupts unterstützen. Das dritte Argument von B<ioctl>(2) "
"wird ignoriert. Nur ein privilegierter Prozess (d.h. einer mit der "
"B<CAP_SYS_RESOURCE>-Capability) kann den periodischen Interrupt aktivieren, "
"wenn die Frequenz über dem in I</proc/sys/dev/rtc/max-user-freq> "
"festgelegten Wert liegt."

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<RTC_EPOCH_READ>"
msgstr "B<RTC_EPOCH_READ>"

#. type: TQ
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<RTC_EPOCH_SET>"
msgstr "B<RTC_EPOCH_SET>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Many RTCs encode the year in an 8-bit register which is either interpreted "
"as an 8-bit binary number or as a BCD number.  In both cases, the number is "
"interpreted relative to this RTC's Epoch.  The RTC's Epoch is initialized to "
"1900 on most systems but on Alpha and MIPS it might also be initialized to "
"1952, 1980, or 2000, depending on the value of an RTC register for the "
"year.  With some RTCs, these operations can be used to read or to set the "
"RTC's Epoch, respectively.  The third B<ioctl>(2)  argument is an I<unsigned "
"long\\ *> or an I<unsigned long>, respectively, and the value returned (or "
"assigned) is the Epoch.  To set the RTC's Epoch the process must be "
"privileged (i.e., have the B<CAP_SYS_TIME> capability)."
msgstr ""
"Viele RTCs codieren das Jahr in einem 8-Bit-Register, das entweder als 8-Bit-"
"Binärzahl oder als BCD-Zahl interpretiert wird. In beiden Fällen wird die "
"Zahl relativ zum zeitlichen Bezugspunkt (Epoch) dieser RTC interpretiert. "
"Auf den meisten Systemen ist dies das Jahr 1900, aber auf Alpha und MIPS "
"könnte es abhängig vom RTC-Register für das Jahr auch 1952, 1980 oder 2000 "
"sein. Bei einigen RTCs kann mit diesen Operationen das Bezugsjahr gelesen "
"bzw. gesetzt werden. Das dritte Argument von B<ioctl>(2) ist ein I<unsigned "
"long\\ *> oder ein I<unsigned long> und entsprechend ist der zurückgegebene "
"(oder zugewiesene Wert) das Bezugsjahr. Um das Bezugsjahr der RTC zu setzen, "
"muss der Prozess privilegiert sein (d.h. über die Capability B<CAP_SYS_TIME> "
"verfügen)."

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<RTC_WKALM_RD>"
msgstr "B<RTC_WKALM_RD>"

#. type: TQ
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<RTC_WKALM_SET>"
msgstr "B<RTC_WKALM_SET>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Some RTCs support a more powerful alarm interface, using these ioctls to "
"read or write the RTC's alarm time (respectively) with this structure:"
msgstr ""
"Einige RTCs unterstützen eine leistungsfähigere Alarm-Schnittstelle mittels "
"dieser »ioctls« zum Schreiben oder Lesen der Alarmzeit der RTC mit dieser "
"Struktur: "

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"struct rtc_wkalrm {\n"
"    unsigned char enabled;\n"
"    unsigned char pending;\n"
"    struct rtc_time time;\n"
"};\n"
msgstr ""
"struct rtc_wkalrm {\n"
"    unsigned char enabled;\n"
"    unsigned char pending;\n"
"    struct rtc_time time;\n"
"};\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The I<enabled> flag is used to enable or disable the alarm interrupt, or to "
"read its current status; when using these calls, B<RTC_AIE_ON> and "
"B<RTC_AIE_OFF> are not used.  The I<pending> flag is used by B<RTC_WKALM_RD> "
"to report a pending interrupt (so it's mostly useless on Linux, except when "
"talking to the RTC managed by EFI firmware).  The I<time> field is as used "
"with B<RTC_ALM_READ> and B<RTC_ALM_SET> except that the I<tm_mday>, "
"I<tm_mon>, and I<tm_year> fields are also valid.  A pointer to this "
"structure should be passed as the third B<ioctl>(2)  argument."
msgstr ""
"Der Schalter I<enabled> wird zur Aktivierung oder Deaktivierung des Alarm-"
"Interrupts oder zur Ermittlung seines aktuellen Status verwendet; wenn Sie "
"diese Aufrufe einsetzen, werden B<RTC_AIE_ON> und B<RTC_AIE_OFF> nicht "
"beachtet. Der Schalter I<pending> wird von B<RTC_WKALM_RD> verwendet, um "
"einen anstehenden Interrupt anzuzeigen. (Er ist also unter Linux meist "
"nutzlos - es sei denn, es wird mit durch EFI-Firmware verwalteteten RTCs "
"kommuniziert.) Das Feld I<time> wird mit B<RTC_ALM_READ> und B<RTC_ALM_SET> "
"verwendet, mit dem Unterschied, dass die Felder I<tm_mday>, I<tm_mon> und "
"I<tm_year> ebenfalls gültig sind. Ein Zeiger auf diese Struktur sollte als "
"drittes Argument an B<ioctl>(2) übergeben werden."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "DATEIEN"

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</dev/rtc>"
msgstr "I</dev/rtc>"

#. type: TQ
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</dev/rtc0>"
msgstr "I</dev/rtc0>"

#. type: TQ
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</dev/rtc1>"
msgstr "I</dev/rtc1>"

#. type: TQ
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "\\&.\\|.\\|."
msgstr "…"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "RTC special character device files."
msgstr "(zeichenorientierte) RTC-Gerätedateien."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</proc/driver/rtc>"
msgstr "I</proc/driver/rtc>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "status of the (first) RTC."
msgstr "Status der (ersten) RTC."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ANMERKUNGEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"When the kernel's system time is synchronized with an external reference "
"using B<adjtimex>(2)  it will update a designated RTC periodically every 11 "
"minutes.  To do so, the kernel has to briefly turn off periodic interrupts; "
"this might affect programs using that RTC."
msgstr ""
"Wenn die Systemzeit des Kernels mittels B<adjtimex>(2) mit einer externen "
"Referenz synchronisiert wird, wird er eine bestimmte RTC periodisch alle 11 "
"Minuten aktualisieren. Dafür muss der Kernel kurzzeitig periodische "
"Interrupts ausschalten. Dadurch könnten Programme beeinträchtigt werden, die "
"diese RTC verwenden."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"An RTC's Epoch has nothing to do with the POSIX Epoch which is used only for "
"the system clock."
msgstr ""
"Der Zeitbezugspunkt (Epoch) hat nichts mit der POSIX Epoch zu tun, die "
"lediglich für die Systemuhr verwendet wird."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the year according to the RTC's Epoch and the year register is less than "
"1970 it is assumed to be 100 years later, that is, between 2000 and 2069."
msgstr ""
"Wenn das Jahr entsprechend der RTC-Epoch und dem Jahres-Register kleiner als "
"1970 ist, werden 100 Jahre drauf geschlagen, also ein Jahr zwischen 2000 und "
"2069 angenommen."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Some RTCs support \"wildcard\" values in alarm fields, to support scenarios "
"like periodic alarms at fifteen minutes after every hour, or on the first "
"day of each month.  Such usage is nonportable; portable user-space code "
"expects only a single alarm interrupt, and will either disable or "
"reinitialize the alarm after receiving it."
msgstr ""
"Einige RTCs unterstützen Platzhalterwerte (wildcards) in den Alarm-Feldern, "
"um Szenarien wie regelmäßige Alarme 15 Minuten nach jeder vollen Stunde oder "
"am ersten Tag eines jeden Monats zu unterstützen. Die Verwendung ist nicht "
"portabel; portabler User-Space-Code erwartet lediglich einen einzelnen Alarm-"
"Interrupt und wird den Alarm bei Erhalt entweder deaktivieren oder neu "
"initialisieren."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Some RTCs support periodic interrupts with periods that are multiples of a "
"second rather than fractions of a second; multiple alarms; programmable "
"output clock signals; nonvolatile memory; and other hardware capabilities "
"that are not currently exposed by this API."
msgstr ""
"Einige RTCs unterstützen periodische Interrupts mit Zeiten, die ein "
"Vielfaches einer Sekunde anstatt Bruchteile einer Sekunde sind; mehrere "
"Alarme, programmierbare Ausgangs-Taktsignale; nichtflüchtigen Speicher und "
"weitere Fähigkeiten, die derzeit nicht von dieser API zugänglich gemacht "
"werden."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<date>(1), B<adjtimex>(2), B<gettimeofday>(2), B<settimeofday>(2), "
"B<stime>(2), B<time>(2), B<gmtime>(3), B<time>(7), B<hwclock>(8)"
msgstr ""
"B<date>(1), B<adjtimex>(2), B<gettimeofday>(2), B<settimeofday>(2), "
"B<stime>(2), B<time>(2), B<gmtime>(3), B<time>(7), B<hwclock>(8)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<Documentation/rtc.txt> in the Linux kernel source tree"
msgstr "I<Documentation/rtc.txt> im Linux-Kernelquelltext-Verzeichnis"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5. Februar 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<RTC_ALM_READ>, B<RTC_ALM_SET>"
msgstr "B<RTC_ALM_READ>, B<RTC_ALM_SET>"

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<RTC_IRQP_READ>, B<RTC_IRQP_SET>"
msgstr "B<RTC_IRQP_READ>, B<RTC_IRQP_SET>"

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<RTC_AIE_ON>, B<RTC_AIE_OFF>"
msgstr "B<RTC_AIE_ON>, B<RTC_AIE_OFF>"

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<RTC_UIE_ON>, B<RTC_UIE_OFF>"
msgstr "B<RTC_UIE_ON>, B<RTC_UIE_OFF>"

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<RTC_PIE_ON>, B<RTC_PIE_OFF>"
msgstr "B<RTC_PIE_ON>, B<RTC_PIE_OFF>"

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<RTC_EPOCH_READ>, B<RTC_EPOCH_SET>"
msgstr "B<RTC_EPOCH_READ>, B<RTC_EPOCH_SET>"

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<RTC_WKALM_RD>, B<RTC_WKALM_SET>"
msgstr "B<RTC_WKALM_RD>, B<RTC_WKALM_SET>"

# Ja, die Übersetzung ist etwas frei.
#. type: TP
#: debian-bookworm
#, no-wrap
msgid "I</dev/rtc>, I</dev/rtc0>, I</dev/rtc1>, etc."
msgstr "I</dev/rtc>, I</dev/rtc0>, I</dev/rtc1> etc."

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Linux man-pages 6.9.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31. Oktober 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Linux man-pages (unveröffentlicht)"
