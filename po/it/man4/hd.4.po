# Italian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Alessandro Rubini <rubini@linux.it>, 1997.
# Giulio Daprelà <giulio@pluto.it>, 2005.
# Marco Curreli <marcocurreli@tiscali.it>, 2013, 2018.
# Giuseppe Sacco <eppesuig@debian.org>, 2024
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.0.0\n"
"POT-Creation-Date: 2025-02-28 16:35+0100\n"
"PO-Revision-Date: 2024-07-06 14:08+0200\n"
"Last-Translator: Giuseppe Sacco <eppesuig@debian.org>\n"
"Language-Team: Italian <pluto-ildp@lists.pluto.it>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "hd"
msgstr "hd"

#. type: TH
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2 maggio 2024"

#. type: TH
#: archlinux
#, fuzzy, no-wrap
#| msgid "Linux man-pages 6.03"
msgid "Linux man-pages 6.12"
msgstr "Linux man-pages 6.03"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "hd - MFM/IDE hard disk devices"
msgstr "hd - MFM/IDE hard disk devices"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIZIONE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<hd*> devices are block devices to access MFM/IDE hard disk drives in "
"raw mode.  The master drive on the primary IDE controller (major device "
"number 3) is B<hda>; the slave drive is B<hdb>.  The master drive of the "
"second controller (major device number 22)  is B<hdc> and the slave is "
"B<hdd>."
msgstr ""
"I dispositivi B<hd*> sono file speciali a blocchi per l'accesso ai drive dei "
"dischi fissi MFM/IDE in modo «raw». Il primo disco (master) sul primo "
"controller IDE (numero primario 3) è B<hda>; il secondo disco (slave) è "
"B<hdb>. Il primo disco del secondo controller (numero primario 22) è B<hdc> "
"e il secondo disco è B<hdd>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"General IDE block device names have the form B<hd>I<X> , or B<hd>I<XP> , "
"where I<X> is a letter denoting the physical drive, and I<P> is a number "
"denoting the partition on that physical drive.  The first form, B<hd>I<X> , "
"is used to address the whole drive.  Partition numbers are assigned in the "
"order the partitions are discovered, and only nonempty, nonextended "
"partitions get a number.  However, partition numbers 1\\[en]4 are given to "
"the four partitions described in the MBR (the \"primary\" partitions), "
"regardless of whether they are unused or extended.  Thus, the first logical "
"partition will be B<hd>I<X>B<5> \\&.  Both DOS-type partitioning and BSD-"
"disklabel partitioning are supported.  You can have at most 63 partitions on "
"an IDE disk."
msgstr ""
"I nomi generali per i dispositivi IDE hanno la forma B<hd>I<X> o B<hd>I<XP>, "
"dove I<X> è una lettera indicante il disco fisico e I<P> è un numero "
"indicante la partizione su quel disco fisico. La prima forma, B<hd>I<X>, "
"viene usata per indirizzare l'intero disco. I numeri di partizione vengono "
"assegnati nell'ordine in cui le partizioni vengono scoperte, e solo quelle "
"non vuote e non estese ottengono un numero. D'altra parte, i numeri di "
"partizione da 1 a 4 vengono assegnati alle quattro partizioni descritte nel "
"MBR (le partizioni «primarie»), sia che siano inutilizzate sia che siano "
"estese. Perciò, la prima partizione logica sarà B<hd>I<X>B<5>\\&. Sia il "
"partizionamento di tipo DOS sia le «disklabel» di BSD sono supportate. Si "
"possono avere fino a 63 partizioni in un disco IDE."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For example, I</dev/hda> refers to all of the first IDE drive in the system; "
"and I</dev/hdb3> refers to the third DOS \"primary\" partition on the second "
"one."
msgstr ""
"Per esempio, I</dev/hda> si riferisce all'intero primo disco IDE del "
"sistema, e I</dev/hdb3> si riferisce alla terza partizione «primaria» sul "
"secondo disco."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "They are typically created by:"
msgstr "I file speciali sono creati tipicamente con:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"mknod -m 660 /dev/hda b 3 0\n"
"mknod -m 660 /dev/hda1 b 3 1\n"
"mknod -m 660 /dev/hda2 b 3 2\n"
"\\&...\n"
"mknod -m 660 /dev/hda8 b 3 8\n"
"mknod -m 660 /dev/hdb b 3 64\n"
"mknod -m 660 /dev/hdb1 b 3 65\n"
"mknod -m 660 /dev/hdb2 b 3 66\n"
"\\&...\n"
"mknod -m 660 /dev/hdb8 b 3 72\n"
"chown root:disk /dev/hd*\n"
msgstr ""
"mknod -m 660 /dev/hda b 3 0\n"
"mknod -m 660 /dev/hda1 b 3 1\n"
"mknod -m 660 /dev/hda2 b 3 2\n"
"\\&...\n"
"mknod -m 660 /dev/hda8 b 3 8\n"
"mknod -m 660 /dev/hdb b 3 64\n"
"mknod -m 660 /dev/hdb1 b 3 65\n"
"mknod -m 660 /dev/hdb2 b 3 66\n"
"\\&...\n"
"mknod -m 660 /dev/hdb8 b 3 72\n"
"chown root:disk /dev/hd*\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "FILE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I</dev/hd*>"
msgstr "I</dev/hd*>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VEDERE ANCHE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<chown>(1), B<mknod>(1), B<sd>(4), B<mount>(8)"
msgstr "B<chown>(1), B<mknod>(1), B<sd>(4), B<mount>(8)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 febbraio 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Linux man-pages 6.9.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 ottobre 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Linux man-pages (non rilasciato)"
