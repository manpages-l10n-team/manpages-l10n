# Vietnamese translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2025-02-28 16:31+0100\n"
"PO-Revision-Date: 2022-01-18 19:49+0100\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Vietnamese <>\n"
"Language: vi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DF"
msgstr "DF"

#. type: TH
#: archlinux fedora-rawhide
#, no-wrap
msgid "February 2025"
msgstr "Tháng 2 năm 2025"

#. type: TH
#: archlinux fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "GNU coreutils 9.6"
msgstr "GNU coreutils 9.6"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Các câu lệnh"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "TÊN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid "no file systems processed"
msgid "df - report file system space usage"
msgstr "không có hệ thống tập tin được xử lý"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "TÓM TẮT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<df> [I<\\,OPTION\\/>]... [I<\\,FILE\\/>]..."
msgstr "B<df> [I<\\,TÙY_CHỌN\\/>]… [I<\\,TẬP_TIN\\/>]…"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "MÔ TẢ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This manual page documents the GNU version of B<df>.  B<df> displays the "
"amount of space available on the file system containing each file name "
"argument.  If no file name is given, the space available on all currently "
"mounted file systems is shown.  Space is shown in 1K blocks by default, "
"unless the environment variable POSIXLY_CORRECT is set, in which case 512-"
"byte blocks are used."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If an argument is the absolute file name of a device node containing a "
"mounted file system, B<df> shows the space available on that file system "
"rather than on the file system containing the device node.  This version of "
"B<df> cannot show the space available on unmounted file systems, because on "
"most kinds of systems doing so requires non-portable intimate knowledge of "
"file system structures."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "TÙY CHỌN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Show information about the file system on which each FILE resides, or all "
"file systems by default."
msgstr ""
"Hiển thị thông tin về hệ thống tập tin nơi mà TẬP_TIN cư ngụ, hoặc tất cả "
"các tập tin theo mặc định."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Mandatory arguments to long options are mandatory for short options too."
msgstr "Tùy chọn dài yêu cầu đối số thì tùy chọn ngắn cũng vậy."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-a>, B<--all>"
msgstr "B<-a>, B<--all>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "include pseudo, duplicate, inaccessible file systems"
msgstr "gồm cả những hệ thống tập tin giả, trùng, không truy cập được"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-B>, B<--block-size>=I<\\,SIZE\\/>"
msgstr "B<-B>, B<--block-size>=I<\\,CỠ\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"scale sizes by SIZE before printing them; e.g., \\&'-BM' prints sizes in "
"units of 1,048,576 bytes; see SIZE format below"
msgstr ""
"biến đổi cỡ theo CỠ trước khi in. Ví dụ: B<-BM> sẽ in cỡ của đơn vị theo "
"1,048,576 bytes; xem định dạng CỠ ở phía dưới."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-h>, B<--human-readable>"
msgstr "B<-h>, B<--human-readable>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "print sizes in powers of 1024 (e.g., 1023M)"
msgstr "hiển thị kích cỡ theo dạng mũ 1024 (v.d. 1023M)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-H>, B<--si>"
msgstr "B<-H>, B<--si>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "print sizes in powers of 1000 (e.g., 1.1G)"
msgstr "hiển thị kích cỡ theo dạng mũ 1000 (v.d. 1.1G)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-i>, B<--inodes>"
msgstr "B<-i>, B<--inodes>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "list inode information instead of block usage"
msgstr "liêt kê thông tin về inode thay cho sử dụng khối"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-k>"
msgstr "B<-k>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "like B<--block-size>=I<\\,1K\\/>"
msgstr "giống như B<--block-size>=I<\\,1K\\/>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-l>, B<--local>"
msgstr "B<-l>, B<--local>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "limit listing to local file systems"
msgstr "chỉ liệt kê hệ thống tập tin cục bộ"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--no-sync>"
msgstr "B<--no-sync>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "do not invoke sync before getting usage info (default)"
msgstr "không gọi sync trước khi lấy thông tin sử dụng (mặc định)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--output>[=I<\\,FIELD_LIST\\/>]"
msgstr "B<--output>[=I<\\,D.SÁCH_TRƯỜNG\\/>]"

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "use the output format defined by FIELD_LIST, or print all fields if "
#| "FIELD_LIST is omitted."
msgid ""
"use the output format defined by FIELD_LIST, or print all fields if "
"FIELD_LIST is omitted"
msgstr ""
"dùng định dạng kết xuất được định nghĩa bởi D.SÁCH_TRƯỜNG, hay hiển thị tất "
"cả các trường nếu D.SÁCH_TRƯỜNG để trống."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-P>, B<--portability>"
msgstr "B<-P>, B<--portability>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "use the POSIX output format"
msgstr "sử dụng định dạng kết quả POSIX"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--sync>"
msgstr "B<--sync>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "invoke sync before getting usage info"
msgstr "gọi sync trước khi lấy thông tin cách dùng"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--total>"
msgstr "B<--total>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"elide all entries insignificant to available space, and produce a grand total"
msgstr ""
"lược đi tất cả các mục tin vô nghĩa với không gian sẵn sàng, và sản sinh "
"tổng cộng lớn"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-t>, B<--type>=I<\\,TYPE\\/>"
msgstr "B<-t>, B<--type>=I<\\,KIỂU\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "limit listing to file systems of type TYPE"
msgstr "chỉ liệt kê các hệ thống tập tin KIỂU"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-T>, B<--print-type>"
msgstr "B<-T>, B<--print-type>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "print file system type"
msgstr "in ra kiểu hệ thống tập tin"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-x>, B<--exclude-type>=I<\\,TYPE\\/>"
msgstr "B<-x>, B<--exclude-type>=I<\\,KIỂU\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "limit listing to file systems not of type TYPE"
msgstr "giới hạn liệt kê các hệ thống tập tin không phải KIỂU"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>"
msgstr "B<-v>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "(ignored)"
msgstr "(bị bỏ qua)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "hiển thị trợ giúp này rồi thoát"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "đưa ra thông tin phiên bản rồi thoát"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Display values are in units of the first available SIZE from B<--block-"
"size>, and the DF_BLOCK_SIZE, BLOCK_SIZE and BLOCKSIZE environment "
"variables.  Otherwise, units default to 1024 bytes (or 512 if "
"POSIXLY_CORRECT is set)."
msgstr ""
"Giá trị hiển thị theo đơn vị kích CỠ sẵn có đầu tiên từ B<--block-size>, và "
"các biến đổi môi trường DF_BLOCK_SIZE, BLOCK_SIZE và BLOCKSIZE. Không thì "
"đơn vị mặc định là 1024 byte (hoặc 512 nếu đặt POSIXLY_CORRECT)."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "\n"
#| "The SIZE argument is an integer and optional unit (example: 10K is "
#| "10*1024).\n"
#| "Units are K,M,G,T,P,E,Z,Y (powers of 1024) or KB,MB,... (powers of "
#| "1000).\n"
msgid ""
"The SIZE argument is an integer and optional unit (example: 10K is "
"10*1024).  Units are K,M,G,T,P,E,Z,Y,R,Q (powers of 1024) or KB,MB,... "
"(powers of 1000).  Binary prefixes can be used, too: KiB=K, MiB=M, and so on."
msgstr ""
"\n"
"CỠ là số nguyên và các đơn vị tùy chọn (ví dụ: 10M là 10*1024*1024).\n"
"Các đơn vị là K, M, G, T, P, E, Z, Y (số mũ của 1024) hay KB, MB, … (số mũ "
"của 1000).\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"FIELD_LIST is a comma-separated list of columns to be included.  Valid field "
"names are: 'source', 'fstype', 'itotal', 'iused', 'iavail', 'ipcent', "
"\\&'size', 'used', 'avail', 'pcent', 'file' and 'target' (see info page)."
msgstr ""
"D.SÁCH_TRƯỜNG là danh sách các trường được ngăn cách nhau bởi dấu phẩy các "
"cột được bao gồm. Các tên trường hợp lệ là: “source”, “fstype”, “itotal”, "
"“iused”, “iavail”, “ipcent”, “size”, “used”, “avail”, “pcent”, “file” và "
"“target” (xem trang info)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "TÁC GIẢ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Written by Torbjorn Granlund, David MacKenzie, and Paul Eggert."
msgstr "Viết bởi Torbjorn Granlund, David MacKenzie và Paul Eggert."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "THÔNG BÁO LỖI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Trợ giúp trực tuyến GNU coreutils: E<lt>https://www.gnu.org/software/"
"coreutils/E<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Report %s translation bugs to <https://translationproject.org/team/>\n"
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Hãy thông báo lỗi dịch “%s” cho <https://translationproject.org/team/"
"vi.html>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "BẢN QUYỀN"

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"Copyright \\(co 2025 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2025 Free Software Foundation, Inc.  Giấy phép GPL pb3+ : "
"Giấy phép Công cộng GNU phiên bản 3 hay sau E<lt>https://gnu.org/licenses/"
"gpl.htmlE<gt>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Đây là phần mềm tự do: bạn có quyền sửa đổi và phát hành lại nó. KHÔNG CÓ "
"BẢO HÀNH GÌ CẢ, với điều khiển được pháp luật cho phép."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "XEM THÊM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Full documentation E<lt>https://www.gnu.org/software/coreutils/dfE<gt>"
msgstr ""
"Tài liệu đầy đủ có tại: E<lt>https://www.gnu.org/software/coreutils/dfE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "or available locally via: info \\(aq(coreutils) df invocation\\(aq"
msgstr "hoặc sẵn có nội bộ thông qua: info \\(aq(coreutils) df invocation\\(aq"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "September 2022"
msgstr "Tháng 9 năm 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#. type: Plain text
#: debian-bookworm
msgid ""
"If an argument is the absolute file name of a device node containing a "
"mounted file system, B<df> shows the space available on that file system "
"rather than on the file system containing the device node.  This version of "
"B<df> cannot show the space available on unmounted file systems, because on "
"most kinds of systems doing so requires very nonportable intimate knowledge "
"of file system structures."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
msgid ""
"use the output format defined by FIELD_LIST, or print all fields if "
"FIELD_LIST is omitted."
msgstr ""
"dùng định dạng kết xuất được định nghĩa bởi D.SÁCH_TRƯỜNG, hay hiển thị tất "
"cả các trường nếu D.SÁCH_TRƯỜNG để trống."

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid ""
#| "\n"
#| "The SIZE argument is an integer and optional unit (example: 10K is "
#| "10*1024).\n"
#| "Units are K,M,G,T,P,E,Z,Y (powers of 1024) or KB,MB,... (powers of "
#| "1000).\n"
msgid ""
"The SIZE argument is an integer and optional unit (example: 10K is "
"10*1024).  Units are K,M,G,T,P,E,Z,Y (powers of 1024) or KB,MB,... (powers "
"of 1000).  Binary prefixes can be used, too: KiB=K, MiB=M, and so on."
msgstr ""
"\n"
"CỠ là số nguyên và các đơn vị tùy chọn (ví dụ: 10M là 10*1024*1024).\n"
"Các đơn vị là K, M, G, T, P, E, Z, Y (số mũ của 1024) hay KB, MB, … (số mũ "
"của 1000).\n"

#. type: Plain text
#: debian-bookworm
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  Giấy phép GPL pb3+ : "
"Giấy phép Công cộng GNU phiên bản 3 hay sau E<lt>https://gnu.org/licenses/"
"gpl.htmlE<gt>."

#. type: TH
#: debian-unstable
#, no-wrap
msgid "October 2024"
msgstr "Tháng 10 năm 2024"

#. type: TH
#: debian-unstable mageia-cauldron
#, no-wrap
msgid "GNU coreutils 9.5"
msgstr "GNU coreutils 9.5"

#. type: Plain text
#: debian-unstable mageia-cauldron
msgid ""
"Copyright \\(co 2024 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2024 Free Software Foundation, Inc.  Giấy phép GPL pb3+ : "
"Giấy phép Công cộng GNU phiên bản 3 hay sau E<lt>https://gnu.org/licenses/"
"gpl.htmlE<gt>."

#. type: TH
#: fedora-42 opensuse-tumbleweed
#, no-wrap
msgid "January 2025"
msgstr "Tháng 1 năm 2025"

#. type: TP
#: fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--direct>"
msgstr "B<--direct>"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron
msgid "show statistics for a file instead of mount point"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "March 2024"
msgstr "Tháng 3 năm 2024"

#. type: TH
#: opensuse-leap-16-0
#, no-wrap
msgid "January 2024"
msgstr "Tháng 1 năm 2024"

#. type: TH
#: opensuse-leap-16-0
#, no-wrap
msgid "GNU coreutils 9.4"
msgstr "GNU coreutils 9.4"

#. type: Plain text
#: opensuse-leap-16-0
msgid ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  Giấy phép GPL pb3+ : "
"Giấy phép Công cộng GNU phiên bản 3 hay sau E<lt>https://gnu.org/licenses/"
"gpl.htmlE<gt>."
