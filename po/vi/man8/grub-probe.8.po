# Vietnamese translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2025-02-28 16:35+0100\n"
"PO-Revision-Date: 2022-01-18 19:49+0100\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Vietnamese <>\n"
"Language: vi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "GRUB-PROBE"
msgstr "GRUB-PROBE"

#. type: TH
#: archlinux
#, no-wrap
msgid "February 2025"
msgstr "Tháng 2 năm 2025"

#. type: TH
#: archlinux
#, no-wrap
msgid "GRUB 2:2.12.r212.g4dc616657-2"
msgstr ""

#. type: TH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "System Administration Utilities"
msgstr "Tiện ích quản trị hệ thống"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr "TÊN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "grub-probe - probe device information for GRUB"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "TÓM TẮT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"B<grub-probe> [I<\\,OPTION\\/>...] [I<\\,OPTION\\/>]... [I<\\,PATH|DEVICE\\/"
">]"
msgstr ""
"B<grub-probe> [I<\\,TÙY_CHỌN\\/>…] [I<\\,TÙY_CHỌN\\/>]… [I<\\,ĐƯỜNG_DẪN|"
"THIẾT_BỊ\\/>]"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "MÔ TẢ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"Probe device information for a given path (or device, if the B<-d> option is "
"given)."
msgstr ""
"Thông tin thiết bị thăm dò được cho đường dẫn đã cho (hoặc thiết bị, nếu tùy "
"chọn B<-d> được chỉ ra)."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-0>"
msgstr "B<-0>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "separate items in output using ASCII NUL characters"
msgstr "ngăn cách các mục tin trong đầu ra sử dụng các ký tự NULL ASCII"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-d>, B<--device>"
msgstr "B<-d>, B<--device>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "given argument is a system device, not a path"
msgstr "đưa ra tham số là thiết bị hệ thống, không phải đường dẫn"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-m>, B<--device-map>=I<\\,FILE\\/>"
msgstr "B<-m>, B<--device-map>=I<\\,TẬP_TIN\\/>"

#. type: Plain text
#: archlinux
msgid "use FILE as the device map [default=//boot/grub/device.map]"
msgstr "dùng TẬP_TIN làm ánh xạ thiết bị [mặc định=//boot/grub/device.map]"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-t>, B<--target>=I<\\,TARGET\\/>"
msgstr "B<-t>, B<--target>=I<\\,ĐÍCH\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"print TARGET available targets: abstraction, arc_hints, baremetal_hints, "
"bios_hints, compatibility_hint, cryptodisk_uuid, device, disk, drive, "
"efi_hints, fs, fs_label, fs_uuid, gpt_parttype, hints_string, "
"ieee1275_hints, msdos_parttype, partmap, partuuid, zero_check [default=fs]"
msgstr ""
"in ĐÍCH; các đích có thể dùng: abstraction, arc_hints, baremetal_hints, "
"bios_hints, compatibility_hint, cryptodisk_uuid, device, disk, drive, "
"efi_hints, fs, fs_label, fs_uuid, gpt_parttype, hints_string, "
"ieee1275_hints, msdos_parttype, partmap, partuuid, zero_check [mặc định=fs]"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "print verbose messages (pass twice to enable debug printing)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "-?, B<--help>"
msgstr "-?, B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "give this help list"
msgstr "hiển thị trợ giúp này"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "give a short usage message"
msgstr "hiển thị cách sử dụng dạng ngắn gọn"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "print program version"
msgstr "in ra phiên bản chương trình"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"Mandatory or optional arguments to long options are also mandatory or "
"optional for any corresponding short options."
msgstr ""
"Tham số là bắt buộc hay tham số chỉ là tùy chọn cho các tùy chọn dài cũng "
"đồng thời là bắt buộc hay không bắt buộc cho các tùy chọn ngắn tương ứng với "
"nó."

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "REPORTING BUGS"
msgstr "THÔNG BÁO LỖI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr ""
"Hãy thông báo lỗi cho  E<lt>bug-grub@gnu.orgE<gt>. Thông báo lỗi dịch cho: "
"E<lt>http://translationproject.org/team/vi.htmlE<gt>."

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "XEM THÊM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "B<grub-fstest>(1)"
msgstr "B<grub-fstest>(1)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"The full documentation for B<grub-probe> is maintained as a Texinfo manual.  "
"If the B<info> and B<grub-probe> programs are properly installed at your "
"site, the command"
msgstr ""
"Tài liệu hướng dẫn đầy đủ về B<grub-probe> được bảo trì dưới dạng một sổ tay "
"Texinfo.  Nếu chương trình B<info> và B<grub-probe> được cài đặt đúng ở địa "
"chỉ của bạn thì câu lệnh"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "B<info grub-probe>"
msgstr "B<info grub-probe>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "should give you access to the complete manual."
msgstr "nên cho phép bạn truy cập đến toàn bộ sổ tay."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "February 2024"
msgstr "Tháng 2 năm 2024"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "GRUB 2.12-1~bpo12+1"
msgstr "GRUB 2.12-1~bpo12+1"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "use FILE as the device map [default=/boot/grub/device.map]"
msgstr "dùng TẬP_TIN làm ánh xạ thiết bị [mặc định=/boot/grub/device.map]"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "July 2024"
msgstr "Tháng 7 năm 2024"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "GRUB 2.12-5"
msgstr "GRUB 2.12-5"
