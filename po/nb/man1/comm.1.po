# Norwegian bokmål translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.11.0\n"
"POT-Creation-Date: 2025-02-28 16:30+0100\n"
"PO-Revision-Date: 2021-09-03 20:09+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Norwegian bokmål <>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "COMM"
msgstr "COMM"

#. type: TH
#: archlinux fedora-rawhide
#, no-wrap
msgid "February 2025"
msgstr "Februar 2025"

#. type: TH
#: archlinux fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "GNU coreutils 9.6"
msgstr "GNU coreutils 9.6"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Brukerkommandoer"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAVN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid "Compare sorted files FILE1 and FILE2 line by line.\n"
msgid "comm - compare two sorted files line by line"
msgstr "Sammenlikn de sorterte filene FIL1 og FIL2, linje for linje.\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "OVERSIKT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<comm> [I<\\,OPTION\\/>]... I<\\,FILE1 FILE2\\/>"
msgstr "B<comm> [I<\\,VALG\\/>]... I<\\,FIL1 FIL2\\/>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESKRIVELSE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Compare sorted files FILE1 and FILE2 line by line."
msgstr "Sammenlikn de sorterte filene FIL1 og FIL2, linje for linje."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "When FILE1 or FILE2 (not both) is -, read standard input."
msgstr ""
"Hvis hvis FIL1 eller FIL2 (ikke begge) er «-», leser programmet standard "
"inndata."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"With no options, produce three-column output.  Column one contains lines "
"unique to FILE1, column two contains lines unique to FILE2, and column three "
"contains lines common to both files."
msgstr ""
"Uten valg produseres utdata med tre kolonner. Kolonne en inneholder linjene "
"som er unike for FIL1, kolonne to inneholder linjene som er unike for FIL2, "
"mens kolonne tre inneholder linjene som er felles for begge filene."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-1>"
msgstr "B<-1>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "suppress column 1 (lines unique to FILE1)"
msgstr "Utelat kolonne 1 (linjer som kun finnes i FIL1)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-2>"
msgstr "B<-2>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "suppress column 2 (lines unique to FILE2)"
msgstr "Utelat kolonne 2 (linjer som kun finnes i FIL2)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-3>"
msgstr "B<-3>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "suppress column 3 (lines that appear in both files)"
msgstr "Utelat kolonne 2 (linjer som finnes i begge filer)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--check-order>"
msgstr "B<--check-order>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"check that the input is correctly sorted, even if all input lines are "
"pairable"
msgstr ""
"Sjekker om inndata er korrekt sortert, selv om alle inndatalinjer kan pares."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--nocheck-order>"
msgstr "B<--nocheck-order>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "do not check that the input is correctly sorted"
msgstr "Sjekker ikke om inndata er korrekt sortert."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--output-delimiter>=I<\\,STR\\/>"
msgstr "B<--output-delimiter>=I<\\,STRENG\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "separate columns with STR"
msgstr "Skiller kolonner med STRENG."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--total>"
msgstr "B<--total>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "output a summary"
msgstr "Skriv ut en oppsummering."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-z>, B<--zero-terminated>"
msgstr "B<-z>, B<--zero-terminated>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "line delimiter is NUL, not newline"
msgstr "Skill mellom linjer med NUL i stedet for linjeskift."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "Vis denne hjelpeteksten og avslutt."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "Vis versjonsinformasjon og avslutt."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid "Note, comparisons honor the rules specified by 'LC_COLLATE'."
msgid "Comparisons honor the rules specified by 'LC_COLLATE'."
msgstr ""
"Merk at sammenlikningsprosessen følger reglene som velges av «LC_COLLATE»."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "EKSEMPLER"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "comm -12 file1 file2"
msgstr "comm -12 fil1 fil2"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Print only lines present in both file1 and file2."
msgstr "Bare skriv ut linjer som finnes i både fil1 og fil2."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "comm -3 file1 file2"
msgstr "comm -3 fil1 fil2"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Print lines in file1 not in file2, and vice versa."
msgstr "Bare skriv ut linjer som ikke finnes i begge filer."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "OPPHAVSMANN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Written by Richard M. Stallman and David MacKenzie."
msgstr "Skrevet av Richard M. Stallman og David MacKenzie."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPPORTERING AV FEIL"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Få hjelp til bruk av GNU coreutils på nett: E<lt>https://www.gnu.org/"
"software/coreutils/E<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Rapporter oversettelsesfeil til E<lt>https://translationproject.org/team/"
"nb.htmlE<gt>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "OPPHAVSRETT"

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"Copyright \\(co 2025 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2025 Free Software Foundation, Inc.  Lisens GPLv3+: GNU GPL "
"versjon 3 eller senere E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Dette er fri programvare. Du kan endre og dele den videre. Det stilles INGEN "
"GARANTI, i den grad dette tillates av gjeldende lovverk."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SE OGSÅ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid "join(1), uniq(1)"
msgid "B<join>(1), B<uniq>(1)"
msgstr "B<join>(1), B<uniq>(1)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Full documentation E<lt>https://www.gnu.org/software/coreutils/commE<gt>"
msgstr ""
"Fullstendig dokumentasjon: E<lt>https://www.gnu.org/software/coreutils/"
"commE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "or available locally via: info \\(aq(coreutils) comm invocation\\(aq"
msgstr "eller lokalt: info \\(aq(coreutils) comm invocation\\(aq"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "September 2022"
msgstr "September 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid "Note, comparisons honor the rules specified by 'LC_COLLATE'."
msgstr ""
"Merk at sammenlikningsprosessen følger reglene som velges av «LC_COLLATE»."

#. type: Plain text
#: debian-bookworm
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  Lisens GPLv3+: GNU GPL "
"versjon 3 eller senere E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: debian-unstable
#, no-wrap
msgid "October 2024"
msgstr "Oktober 2024"

#. type: TH
#: debian-unstable mageia-cauldron
#, no-wrap
msgid "GNU coreutils 9.5"
msgstr "GNU coreutils 9.5"

#. type: Plain text
#: debian-unstable mageia-cauldron
msgid ""
"Copyright \\(co 2024 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2024 Free Software Foundation, Inc.  Lisens GPLv3+: GNU GPL "
"versjon 3 eller senere E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: fedora-42 opensuse-tumbleweed
#, no-wrap
msgid "January 2025"
msgstr "Januar 2025"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "March 2024"
msgstr "Mars 2024"

#. type: TH
#: opensuse-leap-16-0
#, no-wrap
msgid "January 2024"
msgstr "Januar 2024"

#. type: TH
#: opensuse-leap-16-0
#, no-wrap
msgid "GNU coreutils 9.4"
msgstr "GNU coreutils 9.4"

#. type: Plain text
#: opensuse-leap-16-0
msgid ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  Lisens GPLv3+: GNU GPL "
"versjon 3 eller senere E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
