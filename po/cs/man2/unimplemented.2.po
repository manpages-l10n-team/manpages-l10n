# Czech translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Pavel Heimlich <tropikhajma@gmail.com>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2025-02-28 16:55+0100\n"
"PO-Revision-Date: 2009-02-09 20:06+0100\n"
"Last-Translator: Pavel Heimlich <tropikhajma@gmail.com>\n"
"Language-Team: Czech <diskuze-l10n-cz@lists.openalt.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 20.08.2\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "UNIMPLEMENTED"
msgstr "UNIMPLEMENTED"

#. type: TH
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2. května 2024"

#. type: TH
#: archlinux
#, fuzzy, no-wrap
#| msgid "Linux man-pages 6.03"
msgid "Linux man-pages 6.12"
msgstr "Linux man-pages 6.03"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "JMÉNO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"afs_syscall, break, fattach, fdetach, ftime, getmsg, getpmsg, gtty, "
"isastream, lock, madvise1, mpx, prof, profil, putmsg, putpmsg, security, "
"stty, tuxcall, ulimit, vserver - unimplemented system calls"
msgstr ""
"afs_syscall, break, fattach, fdetach, ftime, getmsg, getpmsg, gtty, "
"isastream, lock, madvise1, mpx, prof, profil, putmsg, putpmsg, security, "
"stty, tuxcall, ulimit, vserver - neimplementovaná systémová volání"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "POUŽITÍ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "Unimplemented system calls."
msgid "Unimplemented system calls.\n"
msgstr "afs_syscall, break, fattach, fdetach, ftime, getmsg, getpmsg, gtty, isastream, lock, madvise1, mpx, multiplexer, prof, pro- fil, putmsg, putpmsg, security, stty, tuxcall, ulimit, vserver - neimplementovaná systémová volání."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "POPIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "These system calls are not implemented in the Linux kernel."
msgstr "Tato systémová volání nejsou implementována v Linuxovém jádře 2.6.2."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "NÁVRATOVÉ HODNOTY"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "These system calls always return -1 and set I<errno> to B<ENOSYS>."
msgstr "Tato systémová volání vždy vrací -1 a nastaví I<errno> na B<ENOSYS>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "POZNÁMKY"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Note that B<ftime>(3), B<profil>(3), and B<ulimit>(3)  are implemented as "
"library functions."
msgstr ""
"Funkce B<ftime>(3), B<profil>(3) a B<ulimit>(3) jsou implementovány jako "
"knihovní funkce."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Some system calls, like B<alloc_hugepages>(2), B<free_hugepages>(2), "
"B<ioperm>(2), B<iopl>(2), and B<vm86>(2)  exist only on certain "
"architectures."
msgstr ""
"Některá systémová volání jako B<alloc_hugepages>(2), B<free_hugepages>(2), "
"B<ioperm>(2), B<iopl>(2)  a B<vm86>(2) existují pouze na některých "
"architekturách."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Some system calls, like B<ipc>(2), B<create_module>(2), B<init_module>(2), "
"and B<delete_module>(2)  exist only when the Linux kernel was built with "
"support for them."
msgstr ""
"Některá systémová volání jako B<ipc>(2), B<create_module>(2), "
"B<init_module>(2) a B<delete_module>(2)  existují pouze pokud byla podpora "
"těchto volání zakompilována do linuxového jádra."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "DALŠÍ INFORMACE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<syscalls>(2)"
msgstr "B<syscalls>(2)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-10-09"
msgstr "9. října 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Linux man-pages 6.9.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31. října 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Linux man-pages (nevydané)"
