# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.21.0\n"
"POT-Creation-Date: 2025-02-28 16:34+0100\n"
"PO-Revision-Date: 2024-02-24 12:32+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "getfsent"
msgstr "getfsent"

#. type: TH
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "2024-07-23"
msgstr "23 iulie 2024"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr "Pagini de manual de Linux 6.12"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"getfsent, getfsspec, getfsfile, setfsent, endfsent - handle fstab entries"
msgstr ""
"getfsent, getfsspec, getfsfile, setfsent, endfsent - manipulează intrările "
"fișierului „fstab”"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "Standard C library (I<libc>,\\ I<-lc>)"
msgstr "Biblioteca C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>fstab.hE<gt>>\n"
msgstr "B<#include E<lt>fstab.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int setfsent(void);>\n"
"B<struct fstab *getfsent(void);>\n"
"B<void endfsent(void);>\n"
msgstr ""
"B<int setfsent(void);>\n"
"B<struct fstab *getfsent(void);>\n"
"B<void endfsent(void);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<struct fstab *getfsfile(const char *>I<mount_point>B<);>\n"
"B<struct fstab *getfsspec(const char *>I<special_file>B<);>\n"
msgstr ""
"B<struct fstab *getfsfile(const char *>I<mount_point>B<);>\n"
"B<struct fstab *getfsspec(const char *>I<special_file>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"These functions read from the file I</etc/fstab>.  The I<struct fstab> is "
"defined by:"
msgstr ""
"Aceste funcții citesc din fișierul I</etc/fstab>. Structura I<struct fstab> "
"este definită de:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"struct fstab {\n"
"    char       *fs_spec;       /* block device name */\n"
"    char       *fs_file;       /* mount point */\n"
"    char       *fs_vfstype;    /* filesystem type */\n"
"    char       *fs_mntops;     /* mount options */\n"
"    const char *fs_type;       /* rw/rq/ro/sw/xx option */\n"
"    int         fs_freq;       /* dump frequency, in days */\n"
"    int         fs_passno;     /* pass number on parallel dump */\n"
"};\n"
msgstr ""
"struct fstab {\n"
"    char       *fs_spec;       /* numele dispozitivului de blocuri */\n"
"    char       *fs_file;       /* punctul de montare */\n"
"    char       *fs_vfstype;    /* tipul sistemului de fișiere */\n"
"    char       *fs_mntops;     /* opțiuni montare*/\n"
"    const char *fs_type;       /* opțiunea rw/rq/ro/sw/xx */\n"
"    int         fs_freq;       /* frecvența de descărcare, în zile */\n"
"    int         fs_passno;     /* numărul de trecere pe descărcare paralelă */\n"
"};\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Here the field I<fs_type> contains (on a *BSD system)  one of the five "
"strings \"rw\", \"rq\", \"ro\", \"sw\", \"xx\" (read-write, read-write with "
"quota, read-only, swap, ignore)."
msgstr ""
"Aici, câmpul I<fs_type> conține (pe un sistem *BSD) unul dintre cele cinci "
"șiruri „rw”, „rq”, „ro”, „sw”, „xx” („read-write” citire-scriere, „read-"
"write with quota” citire-scriere cu cotă, „read-only” doar-citire, „swap” "
"spațiu de interschimb, „ignore” ignoră)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The function B<setfsent>()  opens the file when required and positions it at "
"the first line."
msgstr ""
"Funcția B<setfsent>() deschide fișierul atunci când este necesar și îl "
"poziționează la prima linie."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The function B<getfsent>()  parses the next line from the file.  (After "
"opening it when required.)"
msgstr ""
"Funcția B<getfsent>() analizează următoarea linie din fișier; (după ce îl "
"deschide, dacă este necesar)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The function B<endfsent>()  closes the file when required."
msgstr "Funcția B<endfsent>() închide fișierul atunci când este necesar."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The function B<getfsspec>()  searches the file from the start and returns "
"the first entry found for which the I<fs_spec> field matches the "
"I<special_file> argument."
msgstr ""
"Funcția B<getfsspec>() caută în fișier de la început și returnează prima "
"intrare găsită pentru care câmpul I<fs_spec> se potrivește cu argumentul "
"I<special_file>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The function B<getfsfile>()  searches the file from the start and returns "
"the first entry found for which the I<fs_file> field matches the "
"I<mount_point> argument."
msgstr ""
"Funcția B<getfsfile>() caută fișierul de la început și returnează prima "
"intrare găsită pentru care câmpul I<fs_file> se potrivește cu argumentul "
"I<mount_point>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOAREA RETURNATĂ"

#. #-#-#-#-#  archlinux: getfsent.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: getfsent.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .SH HISTORY
#.  The
#.  .BR getfsent ()
#.  function appeared in 4.0BSD; the other four functions appeared in 4.3BSD.
#. type: Plain text
#. #-#-#-#-#  debian-unstable: getfsent.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  fedora-42: getfsent.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: getfsent.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: getfsent.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-16-0: getfsent.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: getfsent.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Upon success, the functions B<getfsent>(), B<getfsfile>(), and "
"B<getfsspec>()  return a pointer to a I<struct fstab>, while B<setfsent>()  "
"returns 1.  Upon failure or end-of-file, these functions return NULL and 0, "
"respectively."
msgstr ""
"În caz de succes, funcțiile B<getfsent>(), B<getfsfile>() și B<getfsspec>() "
"returnează un indicator la o I<structură fstab>, în timp ce B<setfsent>() "
"returnează 1. În caz de eșec sau de sfârșit de fișier, aceste funcții "
"returnează NULL și, respectiv, 0."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRIBUTE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Pentru o explicație a termenilor folosiți în această secțiune, a se vedea "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interfață"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atribut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valoare"

#. type: tbl table
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<endfsent>(),\n"
"B<setfsent>()"
msgstr ""
"B<endfsent>(),\n"
"B<setfsent>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Siguranța firelor"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Unsafe race:fsent"
msgstr "MT-Unsafe race:fsent"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<getfsent>(),\n"
"B<getfsspec>(),\n"
"B<getfsfile>()"
msgstr ""
"B<getfsent>(),\n"
"B<getfsspec>(),\n"
"B<getfsfile>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Unsafe race:fsent locale"
msgstr "MT-Unsafe race:fsent locale"

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIUNI"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Several operating systems have these functions, for example, *BSD, SunOS, "
"Digital UNIX, AIX (which also has a B<getfstype>()).  HP-UX has functions of "
"the same names, that however use a I<struct checklist> instead of a I<struct "
"fstab>, and calls these functions obsolete, superseded by B<getmntent>(3)."
msgstr ""
"Mai multe sisteme de operare au aceste funcții, de exemplu, *BSD, SunOS, "
"Digital UNIX, AIX (care are, de asemenea, un B<getfstype>()). HP-UX are "
"funcții cu aceleași nume, care folosesc însă o I<struct checklist> în loc de "
"o I<struct fstab>, și numește aceste funcții învechite, fiind înlocuite de "
"B<getmntent>(3)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDE"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "None."
msgstr "Niciunul."

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ISTORIC"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<getfsent>()  function appeared in 4.0BSD; the other four functions "
"appeared in 4.3BSD."
msgstr ""
"Funcția B<getfsent>() a apărut în 4.0BSD; celelalte patru funcții au apărut "
"în 4.3BSD."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "These functions are not thread-safe."
msgstr "Aceste funcții nu sunt sigure pentru fire de execuție."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Since Linux allows mounting a block special device in several places, and "
"since several devices can have the same mount point, where the last device "
"with a given mount point is the interesting one, while B<getfsfile>()  and "
"B<getfsspec>()  only return the first occurrence, these two functions are "
"not suitable for use under Linux."
msgstr ""
"Deoarece Linux permite montarea unui dispozitiv special de bloc în mai multe "
"locuri și deoarece mai multe dispozitive pot avea același punct de montare, "
"ultimul dispozitiv cu un anumit punct de montare fiind cel interesant, în "
"timp ce B<getfsfile>() și B<getfsspec>() returnează doar prima apariție, "
"aceste două funcții nu sunt adecvate pentru utilizare în Linux."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<getmntent>(3), B<fstab>(5)"
msgstr "B<getmntent>(3), B<fstab>(5)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-12-15"
msgstr "15 decembrie 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pagini de manual de Linux 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca C standard (I<libc>, I<-lc>)"

#. type: Plain text
#: debian-bookworm
msgid ""
"These functions are not in POSIX.1.  Several operating systems have them, "
"for example, *BSD, SunOS, Digital UNIX, AIX (which also has a "
"B<getfstype>()).  HP-UX has functions of the same names, that however use a "
"I<struct checklist> instead of a I<struct fstab>, and calls these functions "
"obsolete, superseded by B<getmntent>(3)."
msgstr ""
"Aceste funcții nu se găsesc în POSIX.1. Mai multe sisteme de operare au "
"aceste funcții, de exemplu, *BSD, SunOS, Digital UNIX, AIX (care are, de "
"asemenea, un B<getfstype>()). HP-UX are funcții cu aceleași nume, care "
"folosesc însă o I<struct checklist> în loc de o I<struct fstab>, și numește "
"aceste funcții învechite, fiind înlocuite de B<getmntent>(3)."

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "2024-05-02"
msgstr "2 mai 2024"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Pagini de manual de Linux 6.9.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 octombrie 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pagini de manual de Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Pagini de manual Linux (nepublicate)"
