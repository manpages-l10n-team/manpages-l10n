# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.20.0\n"
"POT-Creation-Date: 2025-02-28 16:46+0100\n"
"PO-Revision-Date: 2023-12-14 00:21+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "random_r"
msgstr "random_r"

#. type: TH
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "2024-07-23"
msgstr "23 iulie 2024"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr "Pagini de manual de Linux 6.12"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"random_r, srandom_r, initstate_r, setstate_r - reentrant random number "
"generator"
msgstr ""
"random_r, srandom_r, initstate_r, setstate_r - generator de numere aleatoare "
"reentrante"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "Standard C library (I<libc>,\\ I<-lc>)"
msgstr "Biblioteca C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>stdlib.hE<gt>>\n"
msgstr "B<#include E<lt>stdlib.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int random_r(struct random_data *restrict >I<buf>B<,>\n"
"B<             int32_t *restrict >I<result>B<);>\n"
"B<int srandom_r(unsigned int >I<seed>B<, struct random_data *>I<buf>B<);>\n"
msgstr ""
"B<int random_r(struct random_data *restrict >I<buf>B<,>\n"
"B<             int32_t *restrict >I<result>B<);>\n"
"B<int srandom_r(unsigned int >I<seed>B<, struct random_data *>I<buf>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int initstate_r(unsigned int >I<seed>B<, char >I<statebuf>B<[restrict .>I<statelen>B<],>\n"
"B<             size_t >I<statelen>B<, struct random_data *restrict >I<buf>B<);>\n"
"B<int setstate_r(char *restrict >I<statebuf>B<,>\n"
"B<             struct random_data *restrict >I<buf>B<);>\n"
msgstr ""
"B<int initstate_r(unsigned int >I<seed>B<, char >I<statebuf>B<[restrict .>I<statelen>B<],>\n"
"B<             size_t >I<statelen>B<, struct random_data *restrict >I<buf>B<);>\n"
"B<int setstate_r(char *restrict >I<statebuf>B<,>\n"
"B<             struct random_data *restrict >I<buf>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Cerințe pentru macrocomenzi de testare a caracteristicilor pentru glibc "
"(consultați B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<random_r>(), B<srandom_r>(), B<initstate_r>(), B<setstate_r>():"
msgstr "B<random_r>(), B<srandom_r>(), B<initstate_r>(), B<setstate_r>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"    /* glibc E<gt>= 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _SVID_SOURCE || _BSD_SOURCE\n"
msgstr ""
"    /* glibc E<gt>= 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _SVID_SOURCE || _BSD_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"These functions are the reentrant equivalents of the functions described in "
"B<random>(3).  They are suitable for use in multithreaded programs where "
"each thread needs to obtain an independent, reproducible sequence of random "
"numbers."
msgstr ""
"Aceste funcții sunt echivalentele reentrante ale funcțiilor descrise în "
"B<random>(3). Ele sunt adecvate pentru utilizarea în programe cu mai multe "
"fire de execuție, în care fiecare fir de execuție trebuie să obțină o "
"secvență independentă și reproductibilă de numere aleatoare."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<random_r>()  function is like B<random>(3), except that instead of "
"using state information maintained in a global variable, it uses the state "
"information in the argument pointed to by I<buf>, which must have been "
"previously initialized by B<initstate_r>().  The generated random number is "
"returned in the argument I<result>."
msgstr ""
"Funcția B<random_r>() este ca și B<random>(3), cu excepția faptului că, în "
"loc să utilizeze informațiile de stare păstrate într-o variabilă globală, "
"utilizează informațiile de stare din argumentul indicat de I<buf>, care "
"trebuie să fi fost inițializat anterior de B<initstate_r>().  Numărul "
"aleatoriu generat este returnat în argumentul I<result>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<srandom_r>()  function is like B<srandom>(3), except that it "
"initializes the seed for the random number generator whose state is "
"maintained in the object pointed to by I<buf>, which must have been "
"previously initialized by B<initstate_r>(), instead of the seed associated "
"with the global state variable."
msgstr ""
"Funcția B<srandom_r>() este la fel ca B<srandom>(3), cu excepția faptului că "
"inițializează sămânța pentru generatorul de numere aleatoare a cărui stare "
"este menținută în obiectul indicat de I<buf>, care trebuie să fi fost "
"inițializat în prealabil de B<initstate_r>(), în loc de sămânța asociată cu "
"variabila de stare globală."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<initstate_r>()  function is like B<initstate>(3)  except that it "
"initializes the state in the object pointed to by I<buf>, rather than "
"initializing the global state variable.  Before calling this function, the "
"I<buf.state> field must be initialized to NULL.  The B<initstate_r>()  "
"function records a pointer to the I<statebuf> argument inside the structure "
"pointed to by I<buf>.  Thus, I<statebuf> should not be deallocated so long "
"as I<buf> is still in use.  (So, I<statebuf> should typically be allocated "
"as a static variable, or allocated on the heap using B<malloc>(3)  or "
"similar.)"
msgstr ""
"Funcția B<initstate_r>() este ca B<initstate>(3), cu excepția faptului că "
"inițializează starea în obiectul indicat de I<buf>, în loc să inițializeze "
"variabila de stare globală. Înainte de a apela această funcție, câmpul "
"I<buf.state> trebuie inițializat la NULL.  Funcția B<initstate_r>() "
"înregistrează un indicator la argumentul I<statebuf> în interiorul "
"structurii indicate de I<buf>. Astfel, I<statebuf> nu trebuie să fie "
"eliberat atâta timp cât I<buf> este încă în uz; (așadar, I<statebuf> ar "
"trebui să fie alocat de obicei ca o variabilă statică sau să fie alocat pe "
"grămadă cu ajutorul B<malloc>(3) sau similar)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<setstate_r>()  function is like B<setstate>(3)  except that it "
"modifies the state in the object pointed to by I<buf>, rather than modifying "
"the global state variable.  I<state> must first have been initialized using "
"B<initstate_r>()  or be the result of a previous call of B<setstate_r>()."
msgstr ""
"Funcția B<setstate_r>() este ca B<setstate>(3), cu excepția faptului că "
"modifică starea din obiectul indicat de I<buf>, în loc să modifice variabila "
"de stare globală. I<state> trebuie mai întâi să fi fost inițializat cu "
"B<initstate_r>() sau să fie rezultatul unui apel anterior al funcției "
"B<setstate_r>()."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOAREA RETURNATĂ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"All of these functions return 0 on success.  On error, -1 is returned, with "
"I<errno> set to indicate the error."
msgstr ""
"Toate aceste funcții returnează 0 în caz de succes.  În caz de eroare, se "
"returnează -1, cu I<errno> configurată pentru a indica eroarea."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERORI-IEȘIRE"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "A state array of less than 8 bytes was specified to B<initstate_r>()."
msgstr ""
"O matrice de stare mai mică de 8 octeți a fost specificată la "
"B<initstate_r>()."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The I<statebuf> or I<buf> argument to B<setstate_r>()  was NULL."
msgstr "Argumentul I<statebuf> sau I<buf> la B<setstate_r>() a fost NULL."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The I<buf> or I<result> argument to B<random_r>()  was NULL."
msgstr "Argumentul I<buf> sau I<result> din B<random_r>() a fost NULL."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRIBUTE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Pentru o explicație a termenilor folosiți în această secțiune, a se vedea "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interfață"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atribut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valoare"

#. type: tbl table
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<random_r>(),\n"
"B<srandom_r>(),\n"
"B<initstate_r>(),\n"
"B<setstate_r>()"
msgstr ""
"B<random_r>(),\n"
"B<srandom_r>(),\n"
"B<initstate_r>(),\n"
"B<setstate_r>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Siguranța firelor"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe race:buf"
msgstr "MT-Safe race:buf"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDE"

#.  These functions appear to be on Tru64, but don't seem to be on
#.  Solaris, HP-UX, or FreeBSD.
#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "GNU."
msgstr "GNU."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ERORI"

#.  FIXME . https://sourceware.org/bugzilla/show_bug.cgi?id=3662
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<initstate_r>()  interface is confusing.  It appears that the "
"I<random_data> type is intended to be opaque, but the implementation "
"requires the user to either initialize the I<buf.state> field to NULL or "
"zero out the entire structure before the call."
msgstr ""
"Interfața B<initstate_r>() este confuză. Se pare că tipul I<random_data> "
"este menit să fie opac, dar implementarea cere utilizatorului fie să "
"inițializeze câmpul I<buf.state> la NULL, fie să elimine întreaga structură "
"înainte de apel."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<drand48>(3), B<rand>(3), B<random>(3)"
msgstr "B<drand48>(3), B<rand>(3), B<random>(3)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 februarie 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pagini de manual de Linux 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca C standard (I<libc>, I<-lc>)"

#.  These functions appear to be on Tru64, but don't seem to be on
#.  Solaris, HP-UX, or FreeBSD.
#. type: Plain text
#: debian-bookworm
msgid "These functions are nonstandard glibc extensions."
msgstr "Aceste funcții sunt extensii glibc nestandardizate."

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "2024-05-02"
msgstr "2 mai 2024"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Pagini de manual de Linux 6.9.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 octombrie 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pagini de manual de Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Pagini de manual Linux (nepublicate)"
