# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.24.0\n"
"POT-Creation-Date: 2025-02-28 16:39+0100\n"
"PO-Revision-Date: 2025-02-02 11:58+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.5\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "makecontext"
msgstr "makecontext"

#. type: TH
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "2024-12-13"
msgstr "13 decembrie 2024"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr "Pagini de manual de Linux 6.12"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "makecontext, swapcontext - manipulate user context"
msgstr "makecontext, swapcontext - manipulează contextul utilizatorului"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "Standard C library (I<libc>,\\ I<-lc>)"
msgstr "Biblioteca C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>ucontext.hE<gt>>\n"
msgstr "B<#include E<lt>ucontext.hE<gt>>\n"

#. type: Plain text
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid ""
"B<void makecontext(ucontext_t *>I<ucp>B<, typeof(void (int >I<arg0>B<, ...)) *>I<func>B<,>\n"
"B<                 int >I<argc>B<, ...);>\n"
"B<int swapcontext(ucontext_t *restrict >I<oucp>B<,>\n"
"B<                 const ucontext_t *restrict >I<ucp>B<);>\n"
msgstr ""
"B<void makecontext(ucontext_t *>I<ucp>B<, typeof(void (int >I<arg0>B<, ...)) *>I<func>B<,>\n"
"B<                 int >I<argc>B<, ...);>\n"
"B<int swapcontext(ucontext_t *restrict >I<oucp>B<,>\n"
"B<                 const ucontext_t *restrict >I<ucp>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In a System V-like environment, one has the type I<ucontext_t> (defined in "
"I<E<lt>ucontext.hE<gt>> and described in B<getcontext>(3))  and the four "
"functions B<getcontext>(3), B<setcontext>(3), B<makecontext>(), and "
"B<swapcontext>()  that allow user-level context switching between multiple "
"threads of control within a process."
msgstr ""
"Într-un mediu de tip System V, există tipul I<ucontext_t> (definit în "
"I<E<lt>ucontext.hE<gt>> și descris în B<getcontext>(3)) și cele patru "
"funcții B<getcontext>(3), B<setcontext>(3), B<makecontext>() și "
"B<swapcontext>() care permit comutarea contextului la nivel de utilizator "
"între mai multe fire de control în cadrul unui proces."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<makecontext>()  function modifies the context pointed to by I<ucp> "
"(which was obtained from a call to B<getcontext>(3)).  Before invoking "
"B<makecontext>(), the caller must allocate a new stack for this context and "
"assign its address to I<ucp-E<gt>uc_stack>, and define a successor context "
"and assign its address to I<ucp-E<gt>uc_link>."
msgstr ""
"Funcția B<makecontext>() modifică contextul indicat de I<ucp> (care a fost "
"obținut în urma unui apel la B<getcontext>(3)).  Înainte de a invoca "
"B<makecontext>(), apelantul trebuie să aloce o nouă stivă pentru acest "
"context și să îi atribuie adresa lui I<ucp-E<gt>uc_stack>, precum și să "
"definească un context succesor și să îi atribuie adresa lui I<ucp-"
"E<gt>uc_link>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"When this context is later activated (using B<setcontext>(3)  or "
"B<swapcontext>())  the function I<func> is called, and passed the series of "
"integer (I<int>)  arguments that follow I<argc>; the caller must specify the "
"number of these arguments in I<argc>.  When this function returns, the "
"successor context is activated.  If the successor context pointer is NULL, "
"the thread exits."
msgstr ""
"Atunci când acest context este activat ulterior (utilizând B<setcontext>(3) "
"sau B<swapcontext>()), funcția I<func> este apelată și i se transmite seria "
"de argumente întregi (I<int>) care urmează după I<argc>; apelantul trebuie "
"să specifice numărul acestor argumente în I<argc>.  Atunci când această "
"funcție returnează, contextul succesor este activat.  Dacă indicatorul "
"contextului succesor este NULL, firul de execuție iese."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<swapcontext>()  function saves the current context in the structure "
"pointed to by I<oucp>, and then activates the context pointed to by I<ucp>."
msgstr ""
"Funcția B<swapcontext>() salvează contextul curent în structura indicată de "
"I<oucp>, iar apoi activează contextul indicat de I<ucp>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOAREA RETURNATĂ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"When successful, B<swapcontext>()  does not return.  (But we may return "
"later, in case I<oucp> is activated, in which case it looks like "
"B<swapcontext>()  returns 0.)  On error, B<swapcontext>()  returns -1 and "
"sets I<errno> to indicate the error."
msgstr ""
"Atunci când reușește, B<swapcontext>() nu returnează. (Dar am putea reveni "
"mai târziu, în cazul în care I<oucp> este activat, caz în care se pare că "
"B<swapcontext>() returnează 0.) În caz de eroare, B<swapcontext>() "
"returnează -1 și configurează I<errno> pentru a indica eroarea."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERORI-IEȘIRE"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr "B<ENOMEM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Insufficient stack space left."
msgstr "Spațiu rămas în stivă insuficient."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRIBUTE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Pentru o explicație a termenilor folosiți în această secțiune, a se vedea "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interfață"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atribut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valoare"

#. type: tbl table
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<makecontext>()"
msgstr "B<makecontext>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Siguranța firelor"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe race:ucp"
msgstr "MT-Safe race:ucp"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<swapcontext>()"
msgstr "B<swapcontext>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe race:oucp race:ucp"
msgstr "MT-Safe race:oucp race:ucp"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDE"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "None."
msgstr "Niciunul."

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ISTORIC"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"glibc 2.1.  SUSv2, POSIX.1-2001.  Removed in POSIX.1-2008, citing "
"portability issues, and recommending that applications be rewritten to use "
"POSIX threads instead."
msgstr ""
"glibc 2.1.  SUSv2, POSIX.1-2001. Eliminată în POSIX.1-2008, citând probleme "
"de portabilitate și recomandând ca aplicațiile să fie rescrise pentru a "
"utiliza în schimb fire POSIX."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The interpretation of I<ucp-E<gt>uc_stack> is just as in B<sigaltstack>(2), "
"namely, this struct contains the start and length of a memory area to be "
"used as the stack, regardless of the direction of growth of the stack.  "
"Thus, it is not necessary for the user program to worry about this direction."
msgstr ""
"Interpretarea I<ucp-E<gt>uc_stack> este la fel ca în B<sigaltstack>(2), și "
"anume, această structură conține începutul și lungimea unei zone de memorie "
"care urmează să fie utilizată ca stivă, indiferent de direcția de creștere a "
"stivei. Astfel, nu este necesar ca programul utilizatorului să se preocupe "
"de această direcție."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On architectures where I<int> and pointer types are the same size (e.g., "
"x86-32, where both types are 32 bits), you may be able to get away with "
"passing pointers as arguments to B<makecontext>()  following I<argc>.  "
"However, doing this is not guaranteed to be portable, is undefined according "
"to the standards, and won't work on architectures where pointers are larger "
"than I<int>s.  Nevertheless, starting with glibc 2.8, glibc makes some "
"changes to B<makecontext>(), to permit this on some 64-bit architectures "
"(e.g., x86-64)."
msgstr ""
"Pe arhitecturile în care tipurile I<int> și indicatorul sunt de aceeași "
"dimensiune (de exemplu, x86-32, unde ambele tipuri sunt de 32 de biți), este "
"posibil să puteți scăpa pasând indicatorii ca argumente la B<makecontext>() "
"după I<argc>. Cu toate acestea, acest lucru nu este garantat a fi portabil, "
"nu este definit în conformitate cu standardele și nu va funcționa pe "
"arhitecturile în care indicatorii sunt mai mari decât I<int>-urile.  Cu "
"toate acestea, începând cu glibc 2.8, glibc face unele modificări la "
"B<makecontext>(), pentru a permite acest lucru pe unele arhitecturi pe 64 de "
"biți (de exemplu, x86-64)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "EXEMPLE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The example program below demonstrates the use of B<getcontext>(3), "
"B<makecontext>(), and B<swapcontext>().  Running the program produces the "
"following output:"
msgstr ""
"Programul de exemplu de mai jos demonstrează utilizarea B<getcontext>(3), "
"B<makecontext>() și B<swapcontext>(). Rularea programului produce următoarea "
"ieșire:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"$B< ./a.out>\n"
"main: swapcontext(&uctx_main, &uctx_func2)\n"
"func2: started\n"
"func2: swapcontext(&uctx_func2, &uctx_func1)\n"
"func1: started\n"
"func1: swapcontext(&uctx_func1, &uctx_func2)\n"
"func2: returning\n"
"func1: returning\n"
"main: exiting\n"
msgstr ""
"$B< ./a.out>\n"
"main: swapcontext(&uctx_main, &uctx_func2)\n"
"func2: inițiată\n"
"func2: swapcontext(&uctx_func2, &uctx_func1)\n"
"func1: inițiată\n"
"func1: swapcontext(&uctx_func1, &uctx_func2)\n"
"func2: returnând\n"
"func1: returnând\n"
"main: se iese\n"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Program source"
msgstr "Sursa programului"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>ucontext.hE<gt>\n"
"\\&\n"
"static ucontext_t uctx_main, uctx_func1, uctx_func2;\n"
"\\&\n"
"#define handle_error(msg) \\[rs]\n"
"    do { perror(msg); exit(EXIT_FAILURE); } while (0)\n"
"\\&\n"
"static void\n"
"func1(void)\n"
"{\n"
"    printf(\"%s: started\\[rs]n\", __func__);\n"
"    printf(\"%s: swapcontext(&uctx_func1, &uctx_func2)\\[rs]n\", __func__);\n"
"    if (swapcontext(&uctx_func1, &uctx_func2) == -1)\n"
"        handle_error(\"swapcontext\");\n"
"    printf(\"%s: returning\\[rs]n\", __func__);\n"
"}\n"
"\\&\n"
"static void\n"
"func2(void)\n"
"{\n"
"    printf(\"%s: started\\[rs]n\", __func__);\n"
"    printf(\"%s: swapcontext(&uctx_func2, &uctx_func1)\\[rs]n\", __func__);\n"
"    if (swapcontext(&uctx_func2, &uctx_func1) == -1)\n"
"        handle_error(\"swapcontext\");\n"
"    printf(\"%s: returning\\[rs]n\", __func__);\n"
"}\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    char func1_stack[16384];\n"
"    char func2_stack[16384];\n"
"\\&\n"
"    if (getcontext(&uctx_func1) == -1)\n"
"        handle_error(\"getcontext\");\n"
"    uctx_func1.uc_stack.ss_sp = func1_stack;\n"
"    uctx_func1.uc_stack.ss_size = sizeof(func1_stack);\n"
"    uctx_func1.uc_link = &uctx_main;\n"
"    makecontext(&uctx_func1, func1, 0);\n"
"\\&\n"
"    if (getcontext(&uctx_func2) == -1)\n"
"        handle_error(\"getcontext\");\n"
"    uctx_func2.uc_stack.ss_sp = func2_stack;\n"
"    uctx_func2.uc_stack.ss_size = sizeof(func2_stack);\n"
"    /* Successor context is f1(), unless argc E<gt> 1 */\n"
"    uctx_func2.uc_link = (argc E<gt> 1) ? NULL : &uctx_func1;\n"
"    makecontext(&uctx_func2, func2, 0);\n"
"\\&\n"
"    printf(\"%s: swapcontext(&uctx_main, &uctx_func2)\\[rs]n\", __func__);\n"
"    if (swapcontext(&uctx_main, &uctx_func2) == -1)\n"
"        handle_error(\"swapcontext\");\n"
"\\&\n"
"    printf(\"%s: exiting\\[rs]n\", __func__);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>ucontext.hE<gt>\n"
"\\&\n"
"static ucontext_t uctx_main, uctx_func1, uctx_func2;\n"
"\\&\n"
"#define handle_error(msg) \\[rs]\n"
"    do { perror(msg); exit(EXIT_FAILURE); } while (0)\n"
"\\&\n"
"static void\n"
"func1(void)\n"
"{\n"
"    printf(\"%s: inițiată\\[rs]n\", __func__);\n"
"    printf(\"%s: swapcontext(&uctx_func1, &uctx_func2)\\[rs]n\", __func__);\n"
"    if (swapcontext(&uctx_func1, &uctx_func2) == -1)\n"
"        handle_error(\"swapcontext\");\n"
"    printf(\"%s: returnând\\[rs]n\", __func__);\n"
"}\n"
"\\&\n"
"static void\n"
"func2(void)\n"
"{\n"
"    printf(\"%s: inișiată\\[rs]n\", __func__);\n"
"    printf(\"%s: swapcontext(&uctx_func2, &uctx_func1)\\[rs]n\", __func__);\n"
"    if (swapcontext(&uctx_func2, &uctx_func1) == -1)\n"
"        handle_error(\"swapcontext\");\n"
"    printf(\"%s: returnând\\[rs]n\", __func__);\n"
"}\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    char func1_stack[16384];\n"
"    char func2_stack[16384];\n"
"\\&\n"
"    if (getcontext(&uctx_func1) == -1)\n"
"        handle_error(\"getcontext\");\n"
"    uctx_func1.uc_stack.ss_sp = func1_stack;\n"
"    uctx_func1.uc_stack.ss_size = sizeof(func1_stack);\n"
"    uctx_func1.uc_link = &uctx_main;\n"
"    makecontext(&uctx_func1, func1, 0);\n"
"\\&\n"
"    if (getcontext(&uctx_func2) == -1)\n"
"        handle_error(\"getcontext\");\n"
"    uctx_func2.uc_stack.ss_sp = func2_stack;\n"
"    uctx_func2.uc_stack.ss_size = sizeof(func2_stack);\n"
"    /* Contextul succesor este f1(), cu excepția cazului în care argc E<gt> 1 */\n"
"    uctx_func2.uc_link = (argc E<gt> 1) ? NULL : &uctx_func1;\n"
"    makecontext(&uctx_func2, func2, 0);\n"
"\\&\n"
"    printf(\"%s: swapcontext(&uctx_main, &uctx_func2)\\[rs]n\", __func__);\n"
"    if (swapcontext(&uctx_main, &uctx_func2) == -1)\n"
"        handle_error(\"swapcontext\");\n"
"\\&\n"
"    printf(\"%s: se iese\\[rs]n\", __func__);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"

#.  SRC END
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<sigaction>(2), B<sigaltstack>(2), B<sigprocmask>(2), B<getcontext>(3), "
"B<sigsetjmp>(3)"
msgstr ""
"B<sigaction>(2), B<sigaltstack>(2), B<sigprocmask>(2), B<getcontext>(3), "
"B<sigsetjmp>(3)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-12-15"
msgstr "15 decembrie 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pagini de manual de Linux 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca C standard (I<libc>, I<-lc>)"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
#, no-wrap
msgid ""
"B<void makecontext(ucontext_t *>I<ucp>B<, void (*>I<func>B<)(), int >I<argc>B<, ...);>\n"
"B<int swapcontext(ucontext_t *restrict >I<oucp>B<,>\n"
"B<                const ucontext_t *restrict >I<ucp>B<);>\n"
msgstr ""
"B<void makecontext(ucontext_t *>I<ucp>B<, void (*>I<func>B<)(), int >I<argc>B<, ...);>\n"
"B<int swapcontext(ucontext_t *restrict >I<oucp>B<,>\n"
"B<                const ucontext_t *restrict >I<ucp>B<);>\n"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIUNI"

#. type: Plain text
#: debian-bookworm
msgid "B<makecontext>()  and B<swapcontext>()  are provided since glibc 2.1."
msgstr ""
"B<makecontext>() și B<swapcontext>() sunt furnizate începând cu glibc 2.1."

#. type: Plain text
#: debian-bookworm
msgid ""
"SUSv2, POSIX.1-2001.  POSIX.1-2008 removes the specifications of "
"B<makecontext>()  and B<swapcontext>(), citing portability issues, and "
"recommending that applications be rewritten to use POSIX threads instead."
msgstr ""
"SUSv2, POSIX.1-2001.  POSIX.1-2008 elimină specificațiile B<makecontext>() "
"și B<swapcontext>(), invocând probleme de portabilitate și recomandând ca "
"aplicațiile să fie rescrise pentru a utiliza în schimb fire POSIX."

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>ucontext.hE<gt>\n"
msgstr ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>ucontext.hE<gt>\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "static ucontext_t uctx_main, uctx_func1, uctx_func2;\n"
msgstr "static ucontext_t uctx_main, uctx_func1, uctx_func2;\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"#define handle_error(msg) \\e\n"
"    do { perror(msg); exit(EXIT_FAILURE); } while (0)\n"
msgstr ""
"#define handle_error(msg) \\e\n"
"    do { perror(msg); exit(EXIT_FAILURE); } while (0)\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"static void\n"
"func1(void)\n"
"{\n"
"    printf(\"%s: started\\en\", __func__);\n"
"    printf(\"%s: swapcontext(&uctx_func1, &uctx_func2)\\en\", __func__);\n"
"    if (swapcontext(&uctx_func1, &uctx_func2) == -1)\n"
"        handle_error(\"swapcontext\");\n"
"    printf(\"%s: returning\\en\", __func__);\n"
"}\n"
msgstr ""
"static void\n"
"func1(void)\n"
"{\n"
"    printf(\"%s: inițiată\\en\", __func__);\n"
"    printf(\"%s: swapcontext(&uctx_func1, &uctx_func2)\\en\", __func__);\n"
"    if (swapcontext(&uctx_func1, &uctx_func2) == -1)\n"
"        handle_error(\"swapcontext\");\n"
"    printf(\"%s: returnând\\en\", __func__);\n"
"}\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"static void\n"
"func2(void)\n"
"{\n"
"    printf(\"%s: started\\en\", __func__);\n"
"    printf(\"%s: swapcontext(&uctx_func2, &uctx_func1)\\en\", __func__);\n"
"    if (swapcontext(&uctx_func2, &uctx_func1) == -1)\n"
"        handle_error(\"swapcontext\");\n"
"    printf(\"%s: returning\\en\", __func__);\n"
"}\n"
msgstr ""
"static void\n"
"func2(void)\n"
"{\n"
"    printf(\"%s: inițiată\\en\", __func__);\n"
"    printf(\"%s: swapcontext(&uctx_func2, &uctx_func1)\\en\", __func__);\n"
"    if (swapcontext(&uctx_func2, &uctx_func1) == -1)\n"
"        handle_error(\"swapcontext\");\n"
"    printf(\"%s: returnând\\en\", __func__);\n"
"}\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    char func1_stack[16384];\n"
"    char func2_stack[16384];\n"
msgstr ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    char func1_stack[16384];\n"
"    char func2_stack[16384];\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    if (getcontext(&uctx_func1) == -1)\n"
"        handle_error(\"getcontext\");\n"
"    uctx_func1.uc_stack.ss_sp = func1_stack;\n"
"    uctx_func1.uc_stack.ss_size = sizeof(func1_stack);\n"
"    uctx_func1.uc_link = &uctx_main;\n"
"    makecontext(&uctx_func1, func1, 0);\n"
msgstr ""
"    if (getcontext(&uctx_func1) == -1)\n"
"        handle_error(\"getcontext\");\n"
"    uctx_func1.uc_stack.ss_sp = func1_stack;\n"
"    uctx_func1.uc_stack.ss_size = sizeof(func1_stack);\n"
"    uctx_func1.uc_link = &uctx_main;\n"
"    makecontext(&uctx_func1, func1, 0);\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    if (getcontext(&uctx_func2) == -1)\n"
"        handle_error(\"getcontext\");\n"
"    uctx_func2.uc_stack.ss_sp = func2_stack;\n"
"    uctx_func2.uc_stack.ss_size = sizeof(func2_stack);\n"
"    /* Successor context is f1(), unless argc E<gt> 1 */\n"
"    uctx_func2.uc_link = (argc E<gt> 1) ? NULL : &uctx_func1;\n"
"    makecontext(&uctx_func2, func2, 0);\n"
msgstr ""
"    if (getcontext(&uctx_func2) == -1)\n"
"        handle_error(\"getcontext\");\n"
"    uctx_func2.uc_stack.ss_sp = func2_stack;\n"
"    uctx_func2.uc_stack.ss_size = sizeof(func2_stack);\n"
"    /* Contextul succesor este f1(), cu excepția cazului în care argc E<gt> 1 */\n"
"    uctx_func2.uc_link = (argc E<gt> 1) ? NULL : &uctx_func1;\n"
"    makecontext(&uctx_func2, func2, 0);\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    printf(\"%s: swapcontext(&uctx_main, &uctx_func2)\\en\", __func__);\n"
"    if (swapcontext(&uctx_main, &uctx_func2) == -1)\n"
"        handle_error(\"swapcontext\");\n"
msgstr ""
"    printf(\"%s: swapcontext(&uctx_main, &uctx_func2)\\en\", __func__);\n"
"    if (swapcontext(&uctx_main, &uctx_func2) == -1)\n"
"        handle_error(\"swapcontext\");\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    printf(\"%s: exiting\\en\", __func__);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"    printf(\"%s: se iese\\en\", __func__);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "2024-06-15"
msgstr "15 iunie 2024"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Pagini de manual de Linux 6.9.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 octombrie 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pagini de manual de Linux 6.06"

#. type: Plain text
#: mageia-cauldron
#, no-wrap
msgid ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>ucontext.hE<gt>\n"
"\\&\n"
"static ucontext_t uctx_main, uctx_func1, uctx_func2;\n"
"\\&\n"
"#define handle_error(msg) \\e\n"
"    do { perror(msg); exit(EXIT_FAILURE); } while (0)\n"
"\\&\n"
"static void\n"
"func1(void)\n"
"{\n"
"    printf(\"%s: started\\en\", __func__);\n"
"    printf(\"%s: swapcontext(&uctx_func1, &uctx_func2)\\en\", __func__);\n"
"    if (swapcontext(&uctx_func1, &uctx_func2) == -1)\n"
"        handle_error(\"swapcontext\");\n"
"    printf(\"%s: returning\\en\", __func__);\n"
"}\n"
"\\&\n"
"static void\n"
"func2(void)\n"
"{\n"
"    printf(\"%s: started\\en\", __func__);\n"
"    printf(\"%s: swapcontext(&uctx_func2, &uctx_func1)\\en\", __func__);\n"
"    if (swapcontext(&uctx_func2, &uctx_func1) == -1)\n"
"        handle_error(\"swapcontext\");\n"
"    printf(\"%s: returning\\en\", __func__);\n"
"}\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    char func1_stack[16384];\n"
"    char func2_stack[16384];\n"
"\\&\n"
"    if (getcontext(&uctx_func1) == -1)\n"
"        handle_error(\"getcontext\");\n"
"    uctx_func1.uc_stack.ss_sp = func1_stack;\n"
"    uctx_func1.uc_stack.ss_size = sizeof(func1_stack);\n"
"    uctx_func1.uc_link = &uctx_main;\n"
"    makecontext(&uctx_func1, func1, 0);\n"
"\\&\n"
"    if (getcontext(&uctx_func2) == -1)\n"
"        handle_error(\"getcontext\");\n"
"    uctx_func2.uc_stack.ss_sp = func2_stack;\n"
"    uctx_func2.uc_stack.ss_size = sizeof(func2_stack);\n"
"    /* Successor context is f1(), unless argc E<gt> 1 */\n"
"    uctx_func2.uc_link = (argc E<gt> 1) ? NULL : &uctx_func1;\n"
"    makecontext(&uctx_func2, func2, 0);\n"
"\\&\n"
"    printf(\"%s: swapcontext(&uctx_main, &uctx_func2)\\en\", __func__);\n"
"    if (swapcontext(&uctx_main, &uctx_func2) == -1)\n"
"        handle_error(\"swapcontext\");\n"
"\\&\n"
"    printf(\"%s: exiting\\en\", __func__);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>ucontext.hE<gt>\n"
"\\&\n"
"static ucontext_t uctx_main, uctx_func1, uctx_func2;\n"
"\\&\n"
"#define handle_error(msg) \\e\n"
"    do { perror(msg); exit(EXIT_FAILURE); } while (0)\n"
"\\&\n"
"static void\n"
"func1(void)\n"
"{\n"
"    printf(\"%s: inițiată\\en\", __func__);\n"
"    printf(\"%s: swapcontext(&uctx_func1, &uctx_func2)\\en\", __func__);\n"
"    if (swapcontext(&uctx_func1, &uctx_func2) == -1)\n"
"        handle_error(\"swapcontext\");\n"
"    printf(\"%s: returnând\\en\", __func__);\n"
"}\n"
"\\&\n"
"static void\n"
"func2(void)\n"
"{\n"
"    printf(\"%s: inițiată\\en\", __func__);\n"
"    printf(\"%s: swapcontext(&uctx_func2, &uctx_func1)\\en\", __func__);\n"
"    if (swapcontext(&uctx_func2, &uctx_func1) == -1)\n"
"        handle_error(\"swapcontext\");\n"
"    printf(\"%s: returnând\\en\", __func__);\n"
"}\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    char func1_stack[16384];\n"
"    char func2_stack[16384];\n"
"\\&\n"
"    if (getcontext(&uctx_func1) == -1)\n"
"        handle_error(\"getcontext\");\n"
"    uctx_func1.uc_stack.ss_sp = func1_stack;\n"
"    uctx_func1.uc_stack.ss_size = sizeof(func1_stack);\n"
"    uctx_func1.uc_link = &uctx_main;\n"
"    makecontext(&uctx_func1, func1, 0);\n"
"\\&\n"
"    if (getcontext(&uctx_func2) == -1)\n"
"        handle_error(\"getcontext\");\n"
"    uctx_func2.uc_stack.ss_sp = func2_stack;\n"
"    uctx_func2.uc_stack.ss_size = sizeof(func2_stack);\n"
"    /* Contextul succesor este f1(), cu excepția cazului în care argc E<gt> 1 */\n"
"    uctx_func2.uc_link = (argc E<gt> 1) ? NULL : &uctx_func1;\n"
"    makecontext(&uctx_func2, func2, 0);\n"
"\\&\n"
"    printf(\"%s: swapcontext(&uctx_main, &uctx_func2)\\en\", __func__);\n"
"    if (swapcontext(&uctx_main, &uctx_func2) == -1)\n"
"        handle_error(\"swapcontext\");\n"
"\\&\n"
"    printf(\"%s: se iese\\en\", __func__);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Pagini de manual Linux (nepublicate)"
