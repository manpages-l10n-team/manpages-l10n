# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.19.0\n"
"POT-Creation-Date: 2025-02-28 16:33+0100\n"
"PO-Revision-Date: 2023-07-16 09:52+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "fma"
msgstr "fma"

#. type: TH
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "2024-07-23"
msgstr "23 iulie 2024"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr "Pagini de manual de Linux 6.12"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "fma, fmaf, fmal - floating-point multiply and add"
msgstr "fma, fmaf, fmal - multiplicare și adunare în virgulă mobilă"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "Math library (I<libm>,\\ I<-lm>)"
msgstr "Biblioteca de matematică (I<libm>, I<-lm>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>math.hE<gt>>\n"
msgstr "B<#include E<lt>math.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<double fma(double >I<x>B<, double >I<y>B<, double >I<z>B<);>\n"
"B<float fmaf(float >I<x>B<, float >I<y>B<, float >I<z>B<);>\n"
"B<long double fmal(long double >I<x>B<, long double >I<y>B<, long double >I<z>B<);>\n"
msgstr ""
"B<double fma(double >I<x>B<, double >I<y>B<, double >I<z>B<);>\n"
"B<float fmaf(float >I<x>B<, float >I<y>B<, float >I<z>B<);>\n"
"B<long double fmal(long double >I<x>B<, long double >I<y>B<, long double >I<z>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Cerințe pentru macrocomenzi de testare a caracteristicilor pentru glibc "
"(consultați B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<fma>(), B<fmaf>(), B<fmal>():"
msgstr "B<fma>(), B<fmaf>(), B<fmal>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"
msgstr "    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"These functions compute I<x> * I<y> + I<z>.  The result is rounded as one "
"ternary operation according to the current rounding mode (see B<fenv>(3))."
msgstr ""
"Aceste funcții calculează I<x> * I<y> + I<z>.  Rezultatul este rotunjit ca o "
"operație ternară în funcție de modul de rotunjire curent (a se vedea "
"B<fenv>(3))."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOAREA RETURNATĂ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"These functions return the value of I<x> * I<y> + I<z>, rounded as one "
"ternary operation."
msgstr ""
"Aceste funcții returnează valoarea I<x> * I<y> + I<z>, rotunjită ca o "
"operație ternară."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "If I<x> or I<y> is a NaN, a NaN is returned."
msgstr ""
"Dacă I<x> sau I<y> nu este un număr („Not a Number”: NaN), se returnează un "
"NaN."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If I<x> times I<y> is an exact infinity, and I<z> is an infinity with the "
"opposite sign, a domain error occurs, and a NaN is returned."
msgstr ""
"Dacă I<x> ori I<y> este un infinit exact, iar I<z> este un infinit cu semnul "
"opus, apare o eroare de domeniu și se returnează un NaN."

#.  POSIX.1-2008 allows some possible differences for the following two
#.  domain error cases, but on Linux they are treated the same (AFAICS).
#.  Nevertheless, we'll mirror POSIX.1 and describe the two cases
#.  separately.
#.  POSIX.1 says that a NaN or an implementation-defined value shall
#.  be returned for this case.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If one of I<x> or I<y> is an infinity, the other is 0, and I<z> is not a "
"NaN, a domain error occurs, and a NaN is returned."
msgstr ""
"Dacă unul dintre I<x> sau I<y> este un infinit, celălalt este 0, iar I<z> nu "
"este un NaN, apare o eroare de domeniu și se returnează un NaN."

#.  POSIX.1 makes the domain error optional for this case.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If one of I<x> or I<y> is an infinity, and the other is 0, and I<z> is a "
"NaN, a domain error occurs, and a NaN is returned."
msgstr ""
"Dacă unul dintre I<x> sau I<y> este un infinit, celălalt este 0, iar I<z> "
"este un NaN, apare o eroare de domeniu și se returnează un NaN."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If I<x> times I<y> is not an infinity times zero (or vice versa), and I<z> "
"is a NaN, a NaN is returned."
msgstr ""
"Dacă I<x> înmulțit cu I<y> nu este un infinit înmulțit cu zero (sau "
"viceversa), iar I<z> este un NaN, se returnează un NaN."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the result overflows, a range error occurs, and an infinity with the "
"correct sign is returned."
msgstr ""
"În cazul în care rezultatul este depășit, se produce o eroare de interval și "
"se returnează un infinit cu semnul corect."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the result underflows, a range error occurs, and a signed 0 is returned."
msgstr ""
"În cazul în care rezultatul nu este suficient de mare (sublimită), apare o "
"eroare de interval și se returnează un 0 cu semn."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERORI-IEȘIRE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"See B<math_error>(7)  for information on how to determine whether an error "
"has occurred when calling these functions."
msgstr ""
"Consultați B<math_error>(7) pentru informații despre cum să determinați dacă "
"a apărut o eroare la apelarea acestor funcții."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The following errors can occur:"
msgstr "Pot apărea următoarele erori:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Domain error: I<x> * I<y> + I<z>, or I<x> * I<y> is invalid and I<z> is not a NaN"
msgstr "Eroare de domeniu: I<x> * I<y> + I<z>, sau I<x> * I<y> nu este valid și I<z> nu este un NaN"

#.  .I errno
#.  is set to
#.  .BR EDOM .
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "An invalid floating-point exception (B<FE_INVALID>)  is raised."
msgstr "Se declanșează o excepție de virgulă mobilă nevalidă (B<FE_INVALID>)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Range error: result overflow"
msgstr "Eroare de interval: depășire de rezultat"

#.  .I errno
#.  is set to
#.  .BR ERANGE .
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "An overflow floating-point exception (B<FE_OVERFLOW>)  is raised."
msgstr ""
"Se declanșează o excepție de supraîncărcare (de depășire a limitei) în "
"virgulă mobilă (B<FE_OVERFLOW>)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Range error: result underflow"
msgstr "Eroare de interval: rezultat sub limită"

#.  .I errno
#.  is set to
#.  .BR ERANGE .
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "An underflow floating-point exception (B<FE_UNDERFLOW>)  is raised."
msgstr ""
"Se declanșează o excepție în virgulă mobilă de tip sub-limită "
"(B<FE_UNDERFLOW>)."

#. #-#-#-#-#  archlinux: fma.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . Is it intentional that these functions do not set errno?
#.  Bug raised: https://www.sourceware.org/bugzilla/show_bug.cgi?id=6801
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: fma.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . Is it intentional that these functions do not set errno?
#.  Bug raised: http://sources.redhat.com/bugzilla/show_bug.cgi?id=6801
#. type: Plain text
#. #-#-#-#-#  debian-unstable: fma.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . Is it intentional that these functions do not set errno?
#.  Bug raised: https://www.sourceware.org/bugzilla/show_bug.cgi?id=6801
#. type: Plain text
#. #-#-#-#-#  fedora-42: fma.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . Is it intentional that these functions do not set errno?
#.  Bug raised: https://www.sourceware.org/bugzilla/show_bug.cgi?id=6801
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: fma.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . Is it intentional that these functions do not set errno?
#.  Bug raised: https://www.sourceware.org/bugzilla/show_bug.cgi?id=6801
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: fma.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . Is it intentional that these functions do not set errno?
#.  Bug raised: https://www.sourceware.org/bugzilla/show_bug.cgi?id=6801
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-16-0: fma.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . Is it intentional that these functions do not set errno?
#.  Bug raised: https://www.sourceware.org/bugzilla/show_bug.cgi?id=6801
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: fma.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . Is it intentional that these functions do not set errno?
#.  Bug raised: https://www.sourceware.org/bugzilla/show_bug.cgi?id=6801
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "These functions do not set I<errno>."
msgstr "Aceste funcții nu configurează I<errno>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRIBUTE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Pentru o explicație a termenilor folosiți în această secțiune, a se vedea "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interfață"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atribut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valoare"

#. type: tbl table
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<fma>(),\n"
"B<fmaf>(),\n"
"B<fmal>()"
msgstr ""
"B<fma>(),\n"
"B<fmaf>(),\n"
"B<fmal>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Siguranța firelor"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDE"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr "C11, POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ISTORIC"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "glibc 2.1.  C99, POSIX.1-2001."
msgstr "glibc 2.1.  C99, POSIX.1-2001."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<remainder>(3), B<remquo>(3)"
msgstr "B<remainder>(3), B<remquo>(3)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-12-15"
msgstr "15 decembrie 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pagini de manual de Linux 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "Math library (I<libm>, I<-lm>)"
msgstr "Biblioteca de matematică (I<libm>, I<-lm>)"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIUNI"

#. type: Plain text
#: debian-bookworm
msgid "These functions were added in glibc 2.1."
msgstr "Aceste funcții au fost adăugate în glibc 2.1."

#. type: Plain text
#: debian-bookworm
msgid "C99, POSIX.1-2001, POSIX.1-2008."
msgstr "C99, POSIX.1-2001, POSIX.1-2008."

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "2024-05-02"
msgstr "2 mai 2024"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Pagini de manual de Linux 6.9.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 octombrie 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pagini de manual de Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Pagini de manual Linux (nepublicate)"
