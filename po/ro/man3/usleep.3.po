# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.20.0\n"
"POT-Creation-Date: 2025-02-28 16:55+0100\n"
"PO-Revision-Date: 2023-11-26 20:22+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "usleep"
msgstr "usleep"

#. type: TH
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "2024-07-23"
msgstr "23 iulie 2024"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr "Pagini de manual de Linux 6.12"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "usleep - suspend execution for microsecond intervals"
msgstr "usleep - suspendă execuția pentru intervale de câteva microsecunde"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "Standard C library (I<libc>,\\ I<-lc>)"
msgstr "Biblioteca C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>unistd.hE<gt>>\n"
msgstr "B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<int usleep(useconds_t >I<usec>B<);>\n"
msgstr "B<int usleep(useconds_t >I<usec>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Cerințe pentru macrocomenzi de testare a caracteristicilor pentru glibc "
"(consultați B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<usleep>():"
msgstr "B<usleep>():"

#.     || _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.12:\n"
"        (_XOPEN_SOURCE E<gt>= 500) && ! (_POSIX_C_SOURCE E<gt>= 200809L)\n"
"            || /* glibc E<gt>= 2.19: */ _DEFAULT_SOURCE\n"
"            || /* glibc E<lt>= 2.19: */ _BSD_SOURCE\n"
"    Before glibc 2.12:\n"
"        _BSD_SOURCE || _XOPEN_SOURCE E<gt>= 500\n"
msgstr ""
"    Începând cu glibc 2.12:\n"
"        (_XOPEN_SOURCE E<gt>= 500) && ! (_POSIX_C_SOURCE E<gt>= 200809L)\n"
"            || /* glibc E<gt>= 2.19: */ _DEFAULT_SOURCE\n"
"            || /* glibc E<lt>= 2.19: */ _BSD_SOURCE\n"
"    Înainte de glibc 2.12:\n"
"        _BSD_SOURCE || _XOPEN_SOURCE E<gt>= 500\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<usleep>()  function suspends execution of the calling thread for (at "
"least) I<usec> microseconds.  The sleep may be lengthened slightly by any "
"system activity or by the time spent processing the call or by the "
"granularity of system timers."
msgstr ""
"Funcția B<usleep>() suspendă execuția firului apelant pentru (cel puțin) "
"I<usec> microsecunde.  Perioada de adormire poate fi prelungită ușor de "
"orice activitate a sistemului sau de timpul petrecut în procesarea apelului "
"sau de gradul de finețe al cronometrelor sistemului."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOAREA RETURNATĂ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<usleep>()  function returns 0 on success.  On error, -1 is returned, "
"with I<errno> set to indicate the error."
msgstr ""
"Funcția B<usleep>() returnează 0 în caz de succes.  În caz de eroare, se "
"returnează -1, cu I<errno> configurată pentru a indica eroarea."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERORI-IEȘIRE"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EINTR>"
msgstr "B<EINTR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Interrupted by a signal; see B<signal>(7)."
msgstr "Întrerupt de un semnal; a se vedea B<signal>(7)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<usec> is greater than or equal to 1000000.  (On systems where that is "
"considered an error.)"
msgstr ""
"I<usec> este mai mare sau egală cu 1000000 (pe sistemele în care acest lucru "
"este considerat o eroare)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRIBUTE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Pentru o explicație a termenilor folosiți în această secțiune, a se vedea "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interfață"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atribut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valoare"

#. type: tbl table
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<usleep>()"
msgstr "B<usleep>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Siguranța firelor"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDE"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "None."
msgstr "Niciunul."

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ISTORIC"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"4.3BSD, POSIX.1-2001.  POSIX.1-2001 declares it obsolete, suggesting "
"B<nanosleep>(2)  instead.  Removed in POSIX.1-2008."
msgstr ""
"4.3BSD, POSIX.1-2001.  POSIX.1-2001 o declară obsoletă, sugerând în schimb "
"B<nanosleep>(2).  Eliminată în POSIX.1-2008."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On the original BSD implementation, and before glibc 2.2.2, the return type "
"of this function is I<void>.  The POSIX version returns I<int>, and this is "
"also the prototype used since glibc 2.2.2."
msgstr ""
"În implementarea BSD originală și înainte de glibc 2.2.2.2, tipul de "
"returnare al acestei funcții este I<void>.  Versiunea POSIX returnează "
"I<int>, iar acesta este, de asemenea, prototipul utilizat începând cu glibc "
"2.2.2."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Only the B<EINVAL> error return is documented by SUSv2 and POSIX.1-2001."
msgstr ""
"Numai returnarea erorii B<EINVAL> este documentată de SUSv2 și POSIX.1-2001."

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "CAVEATS"
msgstr "LIMITĂRI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The interaction of this function with the B<SIGALRM> signal, and with other "
"timer functions such as B<alarm>(2), B<sleep>(3), B<nanosleep>(2), "
"B<setitimer>(2), B<timer_create>(2), B<timer_delete>(2), "
"B<timer_getoverrun>(2), B<timer_gettime>(2), B<timer_settime>(2), "
"B<ualarm>(3)  is unspecified."
msgstr ""
"Interacțiunea acestei funcții cu semnalul B<SIGALRM> și cu alte funcții de "
"temporizare/cronometrare, cum ar fi B<alarmă>(2), B<sleep>(3), "
"B<nanosleep>(2), B<setitimer>(2), B<timer_create>(2), B<timer_delete>(2), "
"B<timer_getoverrun>(2), B<timer_gettime>(2), B<timer_settime>(2), "
"B<timer_settime>(2), B<ualarm>(3) este nespecificată."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<alarm>(2), B<getitimer>(2), B<nanosleep>(2), B<select>(2), "
"B<setitimer>(2), B<sleep>(3), B<ualarm>(3), B<useconds_t>(3type), B<time>(7)"
msgstr ""
"B<alarm>(2), B<getitimer>(2), B<nanosleep>(2), B<select>(2), "
"B<setitimer>(2), B<sleep>(3), B<ualarm>(3), B<useconds_t>(3type), B<time>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 februarie 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pagini de manual de Linux 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca C standard (I<libc>, I<-lc>)"

#. type: Plain text
#: debian-bookworm
msgid ""
"4.3BSD, POSIX.1-2001.  POSIX.1-2001 declares this function obsolete; use "
"B<nanosleep>(2)  instead.  POSIX.1-2008 removes the specification of "
"B<usleep>()."
msgstr ""
"4.3BSD, POSIX.1-2001.  POSIX.1-2001 declară această funcție obsoletă; "
"utilizați în schimb B<nanosleep>(2).  POSIX.1-2008 elimină specificația lui "
"B<usleep>()."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NOTES"
msgstr "NOTE"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "2024-05-02"
msgstr "2 mai 2024"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Pagini de manual de Linux 6.9.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 octombrie 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pagini de manual de Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Pagini de manual Linux (nepublicate)"
