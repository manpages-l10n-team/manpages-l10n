# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2025.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.25.1\n"
"POT-Creation-Date: 2025-02-28 16:50+0100\n"
"PO-Revision-Date: 2025-02-04 14:02+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.5\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "standards"
msgstr "standards"

#. type: TH
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2 mai 2024"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr "Pagini de manual de Linux 6.12"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "standards - C and UNIX Standards"
msgstr "standards - standarde C și UNIX"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The STANDARDS section that appears in many manual pages identifies various "
"standards to which the documented interface conforms.  The following list "
"briefly describes these standards."
msgstr ""
"Secțiunea STANDARDE care apare în multe pagini de manual identifică diverse "
"standarde la care se conformează interfața documentată. Următoarea listă "
"descrie pe scurt aceste standarde."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<V7>"
msgstr "B<V7>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Version 7 (also known as Seventh Edition) UNIX, released by AT&T/Bell Labs "
"in 1979.  After this point, UNIX systems diverged into two main dialects: "
"BSD and System V."
msgstr ""
"Versiunea 7 (cunoscută și ca Seventh Edition) UNIX, lansată de AT&T/Bell "
"Labs în 1979. După acest moment, sistemele UNIX s-au divizat în două "
"dialecte principale: BSD și System V."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<4.2BSD>"
msgstr "B<4.2BSD>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This is an implementation standard defined by the 4.2 release of the "
"I<Berkeley Software Distribution>, released by the University of California "
"at Berkeley.  This was the first Berkeley release that contained a TCP/IP "
"stack and the sockets API.  4.2BSD was released in 1983."
msgstr ""
"Acesta este un standard de implementare definit de versiunea 4.2 a "
"I<Berkeley Software Distribution>, lansată de Universitatea din California "
"la Berkeley. Aceasta a fost prima versiune Berkeley care conținea o stivă "
"TCP/IP și API-ul sockets. 4.2BSD a fost lansat în 1983."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Earlier major BSD releases included I<3BSD> (1980), I<4BSD> (1980), and "
"I<4.1BSD> (1981)."
msgstr ""
"Versiunile BSD majore anterioare au inclus I<3BSD> (1980), I<4BSD> (1980) și "
"I<4.1BSD> (1981)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<4.3BSD>"
msgstr "B<4.3BSD>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The successor to 4.2BSD, released in 1986."
msgstr "Succesorul 4.2BSD, lansat în 1986."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<4.4BSD>"
msgstr "B<4.4BSD>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The successor to 4.3BSD, released in 1993.  This was the last major Berkeley "
"release."
msgstr ""
"Succesorul 4.3BSD, lansat în 1993. Aceasta a fost ultima versiune majoră "
"Berkeley."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<System V>"
msgstr "B<System V>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This is an implementation standard defined by AT&T's milestone 1983 release "
"of its commercial System V (five) release.  The previous major AT&T release "
"was I<System III>, released in 1981."
msgstr ""
"Acesta este un standard de punere în aplicare definit de AT&T prin lansarea "
"în 1983 a versiunii sale comerciale System V (cinci). Versiunea anterioară "
"majoră AT&T a fost I<System III>, lansată în 1981."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<System V release 2 (SVr2)>"
msgstr "B<System V release 2 (SVr2)>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This was the next System V release, made in 1985.  The SVr2 was formally "
"described in the I<System V Interface Definition version 1> (I<SVID 1>)  "
"published in 1985."
msgstr ""
"Aceasta a fost următoarea versiune a sistemului V, realizată în 1985. SVr2 a "
"fost descris oficial în I<System V Interface Definition version 1> (I<SVID "
"1>) publicată în 1985."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<System V release 3 (SVr3)>"
msgstr "B<System V release 3 (SVr3)>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This was the successor to SVr2, released in 1986.  This release was formally "
"described in the I<System V Interface Definition version 2> (I<SVID 2>)."
msgstr ""
"Aceasta a fost succesoarea SVr2, lansată în 1986. Această versiune a fost "
"descrisă oficial în I<System V Interface Definition version 2> (I<SVID 2>)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<System V release 4 (SVr4)>"
msgstr "B<System V release 4 (SVr4)>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This was the successor to SVr3, released in 1989.  This version of System V "
"is described in the \"Programmer's Reference Manual: Operating System API "
"(Intel processors)\" (Prentice-Hall 1992, ISBN 0-13-951294-2)  This release "
"was formally described in the I<System V Interface Definition version 3> "
"(I<SVID 3>), and is considered the definitive System V release."
msgstr ""
"Acesta a fost succesorul SVr3, lansat în 1989.  Această versiune a System V "
"este descrisă în \"Programmer's Reference Manual: Operating System API "
"(Intel processors)\" (Prentice-Hall 1992, ISBN 0-13-951294-2) Această "
"versiune a fost descrisă oficial în I<System V Interface Definition version "
"3> (I<SVID 3>) și este considerată versiunea definitivă a sistemului V."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<SVID 4>"
msgstr "B<SVID 4>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"System V Interface Definition version 4, issued in 1995.  Available online "
"at E<.UR http://www.sco.com\\:/developers\\:/devspecs/> E<.UE .>"
msgstr ""
"System V Interface Definition versiunea 4, publicată în 1995.  Disponibil în "
"Internet la adresa E<.UR http://www.sco.com\\:/developers\\:/devspecs/> "
"E<.UE .>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<C89>"
msgstr "B<C89>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This was the first C language standard, ratified by ANSI (American National "
"Standards Institute) in 1989 (I<X3.159-1989>).  Sometimes this is known as "
"I<ANSI C>, but since C99 is also an ANSI standard, this term is ambiguous.  "
"This standard was also ratified by ISO (International Standards "
"Organization) in 1990 (I<ISO/IEC 9899:1990>), and is thus occasionally "
"referred to as I<ISO C90>."
msgstr ""
"Acesta a fost primul standard al limbajului C, ratificat de ANSI (American "
"National Standards Institute) în 1989 (I<X3.159-1989>). Uneori, acesta este "
"cunoscut sub numele de I<ANSI C>, dar deoarece C99 este, de asemenea, un "
"standard ANSI, acest termen este ambiguu. Acest standard a fost ratificat și "
"de ISO (Organizația Internațională de Standardizare) în 1990 (I<ISO/IEC "
"9899:1990>), fiind astfel denumit uneori I<ISO C90>.\n"
"\n"
"Translated with DeepL.com (free version)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<C99>"
msgstr "B<C99>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This revision of the C language standard was ratified by ISO in 1999 (I<ISO/"
"IEC 9899:1999>).  Available online at E<.UR http://www.open-std.org\\:/"
"jtc1\\:/sc22\\:/wg14\\:/www\\:/standards> E<.UE .>"
msgstr ""
"Această revizuire a standardului limbajului C a fost ratificată de ISO în "
"1999 (I<ISO/IEC 9899:1999>).  Disponibil în Internet la adresa E<.UR http://"
"www.open-std.org\\:/jtc1\\:/sc22\\:/wg14\\:/www\\:/standards> E<.UE .>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<C11>"
msgstr "B<C11>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This revision of the C language standard was ratified by ISO in 2011 (I<ISO/"
"IEC 9899:2011>)."
msgstr ""
"Această revizuire a standardului limbajului C a fost ratificată de ISO în "
"2011 (I<ISO/IEC 9899:2011>)."

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<LFS>"
msgstr "B<LFS>"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The Large File Summit specification, completed in 1996.  This specification "
"defined mechanisms that allowed 32-bit systems to support the use of large "
"files (i.e., 64-bit file offsets).  See E<.UR https://www.opengroup.org\\:/"
"platform\\:/lfs.html> E<.UE .>"
msgstr ""
"Specificația Large File Summit, finalizată în 1996.  Această specificație a "
"definit mecanisme care au permis sistemelor pe 32 de biți să permită "
"utilizarea fișierelor de mari dimensiuni (de exemplu, offsets de fișiere pe "
"64 de biți).  A se vedea E<.UR https://www.opengroup.org\\:/platform\\:/"
"lfs.html> E<.UE .>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<POSIX.1-1988>"
msgstr "B<POSIX.1-1988>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This was the first POSIX standard, ratified by IEEE as IEEE Std 1003.1-1988, "
"and subsequently adopted (with minor revisions) as an ISO standard in 1990.  "
"The term \"POSIX\" was coined by Richard Stallman."
msgstr ""
"Acesta a fost primul standard POSIX, ratificat de IEEE ca IEEE Std "
"1003.1-1988 și adoptat ulterior (cu revizuiri minore) ca standard ISO în "
"1990. Termenul „POSIX” a fost inventat de Richard Stallman."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<POSIX.1-1990>"
msgstr "B<POSIX.1-1990>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"\"Portable Operating System Interface for Computing Environments\".  IEEE "
"1003.1-1990 part 1, ratified by ISO in 1990 (I<ISO/IEC 9945-1:1990>)."
msgstr ""
"„Interfața sistemului de operare portabil pentru medii de calcul”. IEEE "
"1003.1-1990 partea 1, ratificată de ISO în 1990 (I<ISO/IEC 9945-1:1990>)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<POSIX.2>"
msgstr "B<POSIX.2>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"IEEE Std 1003.2-1992, describing commands and utilities, ratified by ISO in "
"1993 (I<ISO/IEC 9945-2:1993>)."
msgstr ""
"IEEE Std 1003.2-1992, care descrie comenzile și utilitățile, ratificată de "
"ISO în 1993 (I<ISO/IEC 9945-2:1993>)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<POSIX.1b> (formerly known as I<POSIX.4>)"
msgstr "B<POSIX.1b> (cunoscut anterior ca I<POSIX.4>)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"IEEE Std 1003.1b-1993, describing real-time facilities for portable "
"operating systems, ratified by ISO in 1996 (I<ISO/IEC 9945-1:1996>)."
msgstr ""
"IEEE Std 1003.1b-1993, care descrie facilitățile în timp real pentru "
"sistemele de operare portabile, ratificată de ISO în 1996 (I<ISO/IEC "
"9945-1:1996>)."

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<POSIX.1c> (formerly known as I<POSIX.4a>)"
msgstr "B<POSIX.1c> (cunoscut anterior ca I<POSIX.4a>)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "IEEE Std 1003.1c-1995, which describes the POSIX threads interfaces."
msgstr "IEEE Std 1003.1c-1995, care descrie interfețele firelor POSIX."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<POSIX.1d>"
msgstr "B<POSIX.1d>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "IEEE Std 1003.1d-1999, which describes additional real-time extensions."
msgstr ""
"IEEE Std 1003.1d-1999, care descrie extensii suplimentare în timp real."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<POSIX.1g>"
msgstr "B<POSIX.1g>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"IEEE Std 1003.1g-2000, which describes networking APIs (including sockets)."
msgstr ""
"IEEE Std 1003.1g-2000, care descrie API-urile de rețea (inclusiv soclurile)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<POSIX.1j>"
msgstr "B<POSIX.1j>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "IEEE Std 1003.1j-2000, which describes advanced real-time extensions."
msgstr "IEEE Std 1003.1j-2000, care descrie extensiile avansate în timp real."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<POSIX.1-1996>"
msgstr "B<POSIX.1-1996>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "A 1996 revision of POSIX.1 which incorporated POSIX.1b and POSIX.1c."
msgstr "O revizuire din 1996 a POSIX.1 care a încorporat POSIX.1b și POSIX.1c."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<XPG3>"
msgstr "B<XPG3>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Released in 1989, this was the first release of the X/Open Portability Guide "
"to be based on a POSIX standard (POSIX.1-1988).  This multivolume guide was "
"developed by the X/Open Group, a multivendor consortium."
msgstr ""
"Publicată în 1989, aceasta a fost prima versiune a Ghidului de portabilitate "
"X/Open bazată pe un standard POSIX (POSIX.1-1988). Acest ghid în mai multe "
"volume a fost elaborat de X/Open Group, un consorțiu cu mai mulți furnizori."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<XPG4>"
msgstr "B<XPG4>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"A revision of the X/Open Portability Guide, released in 1992.  This revision "
"incorporated POSIX.2."
msgstr ""
"O revizuire a Ghidului de portabilitate X/Open, publicat în 1992. Această "
"revizuire a încorporat POSIX.2."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<XPG4v2>"
msgstr "B<XPG4v2>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"A 1994 revision of XPG4.  This is also referred to as I<Spec 1170>, where "
"1170 referred to the number of interfaces defined by this standard."
msgstr ""
"O revizuire din 1994 a XPG4  Acesta este denumit și I<Spec 1170>, unde 1170 "
"se referă la numărul de interfețe definite de acest standard."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<SUS (SUSv1)>"
msgstr "B<SUS (SUSv1)>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Single UNIX Specification.  This was a repackaging of XPG4v2 and other X/"
"Open standards (X/Open Curses Issue 4 version 2, X/Open Networking Service "
"(XNS) Issue 4).  Systems conforming to this standard can be branded I<UNIX "
"95>."
msgstr ""
"Specificație UNIX unică. Aceasta a fost o reambalare a XPG4v2 și a altor "
"standarde X/Open (X/Open Curses Issue 4 versiunea 2, X/Open Networking "
"Service (XNS) Issue 4).  Sistemele conforme cu acest standard pot fi "
"denumite I<UNIX 95>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<SUSv2>"
msgstr "B<SUSv2>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Single UNIX Specification version 2.  Sometimes also referred to "
"(incorrectly) as I<XPG5>.  This standard appeared in 1997.  Systems "
"conforming to this standard can be branded I<UNIX 98>.  See also E<.UR "
"http://www.unix.org\\:/version2/> E<.UE .)>"
msgstr ""
"Specificația unică UNIX versiunea 2. Uneori denumit (incorect) și I<XPG5>. "
"Acest standard a apărut în 1997. Sistemele conforme cu acest standard pot fi "
"denumite I<UNIX 98>. A se vedea și E<.UR http://www.unix.org\\:/version2/> "
"E<.UE .)>"

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<POSIX.1-2001>"
msgstr "B<POSIX.1-2001>"

#. type: TQ
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<SUSv3>"
msgstr "B<SUSv3>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This was a 2001 revision and consolidation of the POSIX.1, POSIX.2, and SUS "
"standards into a single document, conducted under the auspices of the Austin "
"Group E<.UR http://www.opengroup.org\\:/austin/> E<.UE .> The standard is "
"available online at E<.UR http://www.unix.org\\:/version3/> E<.UE .>"
msgstr ""
"Aceasta a fost o revizuire din 2001 și consolidarea standardelor POSIX.1, "
"POSIX.2 și SUS într-un singur document, realizată sub auspiciile grupului "
"Austin E<.UR http://www.opengroup.org\\:/austin/> E<.UE>. Standardul este "
"disponibil în Internet la E<.UR http://www.unix.org\\:/version3/> E<.UE.>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The standard defines two levels of conformance: I<POSIX conformance>, which "
"is a baseline set of interfaces required of a conforming system; and I<XSI "
"Conformance>, which additionally mandates a set of interfaces (the \"XSI "
"extension\") which are only optional for POSIX conformance.  XSI-conformant "
"systems can be branded I<UNIX 03>."
msgstr ""
"Standardul definește două niveluri de conformitate: I<Conformitate POSIX>, "
"care este un set de interfețe de bază necesare unui sistem conform; și "
"I<Conformitate XSI>, care impune în plus un set de interfețe („extensia "
"XSI”) care sunt doar opționale pentru conformitatea POSIX. Sistemele "
"conforme XSI pot fi etichetate I<UNIX 03>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The POSIX.1-2001 document is broken into four parts:"
msgstr "Documentul POSIX.1-2001 este împărțit în patru părți:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<XBD>: Definitions, terms, and concepts, header file specifications."
msgstr ""
"B<XBD>: Definiții, termeni și concepte, specificațiile fișierelor de antet."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<XSH>: Specifications of functions (i.e., system calls and library "
"functions in actual implementations)."
msgstr ""
"B<XSH>: Specificații ale funcțiilor (de exemplu, apeluri de sistem și "
"funcții de bibliotecă în implementările reale)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<XCU>: Specifications of commands and utilities (i.e., the area formerly "
"described by POSIX.2)."
msgstr ""
"B<XCU>: Specificații ale comenzilor și utilităților (adică aria descrisă "
"anterior de POSIX.2)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<XRAT>: Informative text on the other parts of the standard."
msgstr "B<XRAT>: Text informativ privind celelalte părți ale standardului."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"POSIX.1-2001 is aligned with C99, so that all of the library functions "
"standardized in C99 are also standardized in POSIX.1-2001."
msgstr ""
"POSIX.1-2001 este aliniat cu C99, astfel încât toate funcțiile de bibliotecă "
"standardizate în C99 sunt standardizate și în POSIX.1-2001."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The Single UNIX Specification version 3 (SUSv3) comprises the Base "
"Specifications containing XBD, XSH, XCU, and XRAT as above, plus X/Open "
"Curses Issue 4 version 2 as an extra volume that is not in POSIX.1-2001."
msgstr ""
"Specificația UNIX unică versiunea 3 (SUSv3) cuprinde specificațiile de bază "
"care conțin XBD, XSH, XCU și XRAT ca mai sus, plus X/Open Curses Issue 4 "
"versiunea 2 ca un volum suplimentar care nu se află în POSIX.1-2001."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Two Technical Corrigenda (minor fixes and improvements)  of the original "
"2001 standard have occurred: TC1 in 2003 and TC2 in 2004."
msgstr ""
"Au avut loc două rectificări tehnice (corecții și îmbunătățiri minore) ale "
"standardului original din 2001: TC1 în 2003 și TC2 în 2004."

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<POSIX.1-2008>"
msgstr "B<POSIX.1-2008>"

#. type: TQ
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<SUSv4>"
msgstr "B<SUSv4>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Work on the next revision of POSIX.1/SUS was completed and ratified in "
"2008.  The standard is available online at E<.UR http://www.unix.org\\:/"
"version4/> E<.UE .>"
msgstr ""
"Lucrările la următoarea revizuire a POSIX.1/SUS au fost finalizate și "
"ratificate în 2008.  Standardul este disponibil în Internet la adresa E<.UR "
"http://www.unix.org\\:/version4/> E<.UE .>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The changes in this revision are not as large as those that occurred for "
"POSIX.1-2001/SUSv3, but a number of new interfaces are added and various "
"details of existing specifications are modified.  Many of the interfaces "
"that were optional in POSIX.1-2001 become mandatory in the 2008 revision of "
"the standard.  A few interfaces that are present in POSIX.1-2001 are marked "
"as obsolete in POSIX.1-2008, or removed from the standard altogether."
msgstr ""
"Modificările din această revizuire nu sunt la fel de mari ca cele care au "
"avut loc pentru POSIX.1-2001/SUSv3, dar sunt adăugate o serie de interfețe "
"noi și sunt modificate diverse detalii ale specificațiilor existente. Multe "
"dintre interfețele care erau opționale în POSIX.1-2001 devin obligatorii în "
"revizuirea din 2008 a standardului. Câteva interfețe prezente în "
"POSIX.1-2001 sunt marcate ca fiind învechite în POSIX.1-2008 sau eliminate "
"complet din standard."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The revised standard is structured in the same way as its predecessor.  The "
"Single UNIX Specification version 4 (SUSv4) comprises the Base "
"Specifications containing XBD, XSH, XCU, and XRAT, plus X/Open Curses Issue "
"7 as an extra volume that is not in POSIX.1-2008."
msgstr ""
"Standardul revizuit este structurat în același mod ca și predecesorul său. "
"Single UNIX Specification versiunea 4 (SUSv4) cuprinde specificațiile de "
"bază care conțin XBD, XSH, XCU și XRAT, plus X/Open Curses Issue 7 ca volum "
"suplimentar care nu se află în POSIX.1-2008."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Again there are two levels of conformance: the baseline I<POSIX "
"Conformance>, and I<XSI Conformance>, which mandates an additional set of "
"interfaces beyond those in the base specification."
msgstr ""
"Din nou, există două niveluri de conformitate: linia de bază I<POSIX "
"Conformance>, și I<XSI Conformance>, care impune un set suplimentar de "
"interfețe în afara celor din specificația de bază."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In general, where the STANDARDS section of a manual page lists POSIX.1-2001, "
"it can be assumed that the interface also conforms to POSIX.1-2008, unless "
"otherwise noted."
msgstr ""
"În general, atunci când secțiunea STANDARDE a unei pagini de manual enumeră "
"POSIX.1-2001, se poate presupune că interfața este, de asemenea, conformă cu "
"POSIX.1-2008, dacă nu se specifică altfel."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Technical Corrigendum 1 (minor fixes and improvements)  of this standard was "
"released in 2013."
msgstr ""
"Technical Corrigendum - Corecții tehnice 1 (corecții și îmbunătățiri minore) "
"al acestui standard a fost publicat în 2013."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Technical Corrigendum 2 of this standard was released in 2016."
msgstr ""
"Tehnical Corrigendum - Corecții tehnice 2 al acestui standard a fost lansat "
"în 2016."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Further information can be found on the Austin Group web site, E<.UR http://"
"www.opengroup.org\\:/austin/> E<.UE .>"
msgstr ""
"Informații suplimentare pot fi găsite pe situl web al Austin Group, E<.UR "
"http://www.opengroup.org\\:/austin/> E<.UE .>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<SUSv4 2016 edition>"
msgstr "B<SUSv4 2016 edition>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This is equivalent to POSIX.1-2008, with the addition of Technical "
"Corrigenda 1 and 2 and the XCurses specification."
msgstr ""
"Aceasta este echivalentă cu POSIX.1-2008, cu adăugarea Technical Corrigenda "
"1 și 2 și a specificației XCurses."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<POSIX.1-2017>"
msgstr "B<POSIX.1-2017>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This revision of POSIX is technically identical to POSIX.1-2008 with "
"Technical Corrigenda 1 and 2 applied."
msgstr ""
"Această revizuire a POSIX este identică din punct de vedere tehnic cu "
"POSIX.1-2008, cu aplicarea corecțiilor tehnice 1 și 2."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<SUSv4 2018 edition>"
msgstr "B<SUSv4 2018 edition>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This is equivalent to POSIX.1-2017, with the addition of the XCurses "
"specification."
msgstr ""
"Aceasta este echivalentă cu POSIX.1-2017, cu adăugarea specificației XCurses."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The interfaces documented in POSIX.1/SUS are available as manual pages under "
"sections 0p (header files), 1p (commands), and 3p (functions); thus one can "
"write \"man 3p open\"."
msgstr ""
"Interfețele documentate în POSIX.1/SUS sunt disponibile ca pagini de manual "
"în secțiunile 0p (fișiere de antet), 1p (comenzi) și 3p (funcții); astfel, "
"se poate scrie „man 3p open”."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<getconf>(1), B<confstr>(3), B<pathconf>(3), B<sysconf>(3), "
"B<attributes>(7), B<feature_test_macros>(7), B<libc>(7), B<posixoptions>(7), "
"B<system_data_types>(7)"
msgstr ""
"B<getconf>(1), B<confstr>(3), B<pathconf>(3), B<sysconf>(3), "
"B<attributes>(7), B<feature_test_macros>(7), B<libc>(7), B<posixoptions>(7), "
"B<system_data_types>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-10-30"
msgstr "30 octombrie 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pagini de manual de Linux 6.03"

#. type: Plain text
#: debian-bookworm
msgid ""
"B<LFS> The Large File Summit specification, completed in 1996.  This "
"specification defined mechanisms that allowed 32-bit systems to support the "
"use of large files (i.e., 64-bit file offsets).  See E<.UR https://"
"www.opengroup.org\\:/platform\\:/lfs.html> E<.UE .>"
msgstr ""
"B<LFS> Specificația Large File Summit, finalizată în 1996. Această "
"specificație a definit mecanisme care au permis sistemelor pe 32 de biți să "
"susțină utilizarea fișierelor mari (de exemplu, offsets de fișiere pe 64 de "
"biți).  A se vedea E<.UR https://www.opengroup.org\\:/platform\\:/lfs.html> "
"E<.UE .>"

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<POSIX.1c  (formerly known as >I<POSIX.4a>B<)>"
msgstr "B<POSIX.1c (cunoscut anterior ca >I<POSIX.4a>B<)>"

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<POSIX.1-2001, SUSv3>"
msgstr "B<POSIX.1-2001, SUSv3>"

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<POSIX.1-2008, SUSv4>"
msgstr "B<POSIX.1-2008, SUSv4>"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Pagini de manual de Linux 6.9.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 octombrie 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pagini de manual de Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Pagini de manual Linux (nepublicate)"
