# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.18.1\n"
"POT-Creation-Date: 2025-02-28 16:55+0100\n"
"PO-Revision-Date: 2025-02-01 19:57+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.5\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "unicode"
msgstr "unicode"

#. type: TH
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "2025-01-22"
msgstr "22 ianuarie 2025"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr "Pagini de manual de Linux 6.12"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "unicode - universal character set"
msgstr "unicode - set de caractere universal"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The international standard ISO/IEC 10646 defines the Universal Character Set "
"(UCS).  UCS contains all characters of all other character set standards.  "
"It also guarantees \"round-trip compatibility\"; in other words, conversion "
"tables can be built such that no information is lost when a string is "
"converted from any other encoding to UCS and back."
msgstr ""
"Standardul internațional ISO/IEC 10646 definește setul universal de "
"caractere („Universal Character Set”: UCS). UCS conține toate caracterele "
"din toate celelalte standarde de seturi de caractere. De asemenea, "
"garantează „compatibilitatea dus-întors”; cu alte cuvinte, tabelele de "
"conversie pot fi construite astfel încât să nu se piardă nicio informație "
"atunci când un șir de caractere este convertit din orice altă codificare în "
"UCS și invers."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"UCS contains the characters required to represent practically all known "
"languages.  This includes not only the Latin, Greek, Cyrillic, Hebrew, "
"Arabic, Armenian, and Georgian scripts, but also Chinese, Japanese and "
"Korean Han ideographs as well as scripts such as Hiragana, Katakana, Hangul, "
"Devanagari, Bengali, Gurmukhi, Gujarati, Oriya, Tamil, Telugu, Kannada, "
"Malayalam, Thai, Lao, Khmer, Bopomofo, Tibetan, Runic, Ethiopic, Canadian "
"Syllabics, Cherokee, Mongolian, Ogham, Myanmar, Sinhala, Thaana, Yi, and "
"others.  For scripts not yet covered, research on how to best encode them "
"for computer usage is still going on and they will be added eventually.  "
"This might eventually include not only Hieroglyphs and various historic Indo-"
"European languages, but even some selected artistic scripts such as Tengwar, "
"Cirth, and Klingon.  UCS also covers a large number of graphical, "
"typographical, mathematical, and scientific symbols, including those "
"provided by TeX, Postscript, APL, MS-DOS, MS-Windows, Macintosh, OCR fonts, "
"as well as many word processing and publishing systems, and more are being "
"added."
msgstr ""
"UCS conține caracterele necesare pentru a reprezenta practic toate limbile "
"cunoscute. Acestea includ nu numai caracterele latine, grecești, chirilice, "
"ebraice, arabe, armenești și georgiene, ci și ideogramele chinezești, "
"japoneze și coreene Han, precum și caractere precum Hiragana, Katakana, "
"Hangul, Devanagari, Bengali, Gurmukhi, Gujarati, Oriya, Tamil, Telugu, "
"Kannada, Malayalam, Thai, Lao, Khmer, Bopomofo, Tibetană, Runic, Etiopiană, "
"Silabică canadiană, Cherokee, Mongolă, Ogham, Myanmar, Sinhala, Thaana, Yi "
"și altele. În ceea ce privește scripturile care nu sunt încă acoperite, "
"cercetările privind cea mai bună codificare a acestora pentru utilizarea pe "
"calculator sunt încă în curs de desfășurare și vor fi adăugate în cele din "
"urmă. Acestea ar putea include în cele din urmă nu numai hieroglife și "
"diverse limbi indo-europene istorice, ci chiar și unele scripturi artistice "
"selectate, cum ar fi Tengwar, Cirth și Klingon. UCS acoperă, de asemenea, un "
"număr mare de simboluri grafice, tipografice, matematice și științifice, "
"inclusiv cele furnizate de TeX, Postscript, APL, MS-DOS, MS-Windows, "
"Macintosh, fonturi OCR, precum și de multe sisteme de procesare a textelor "
"și de publicare, iar altele sunt în curs de adăugare."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The UCS standard (ISO/IEC 10646) describes a 31-bit character set "
"architecture consisting of 128 24-bit I<groups>, each divided into 256 16-"
"bit I<planes> made up of 256 8-bit I<rows> with 256 I<column> positions, one "
"for each character.  Part 1 of the standard (ISO/IEC 10646-1)  defines the "
"first 65534 code positions (0x0000 to 0xfffd), which form the I<Basic "
"Multilingual Plane> (BMP), that is plane 0 in group 0.  Part 2 of the "
"standard (ISO/IEC 10646-2)  adds characters to group 0 outside the BMP in "
"several I<supplementary planes> in the range 0x10000 to 0x10ffff.  There are "
"no plans to add characters beyond 0x10ffff to the standard, therefore of the "
"entire code space, only a small fraction of group 0 will ever be actually "
"used in the foreseeable future.  The BMP contains all characters found in "
"the commonly used other character sets.  The supplemental planes added by "
"ISO/IEC 10646-2 cover only more exotic characters for special scientific, "
"dictionary printing, publishing industry, higher-level protocol and "
"enthusiast needs."
msgstr ""
"Standardul UCS (ISO/IEC 10646) descrie o arhitectură a setului de caractere "
"pe 31 de biți care constă din 128 de I<grupuri> pe 24 de biți, fiecare fiind "
"împărțit în 256 de I<planuri> pe 16 biți, alcătuite din 256 de I<rânduri> pe "
"8 biți cu 256 de poziții I<coloană>, una pentru fiecare caracter. Partea 1 a "
"standardului (ISO/IEC 10646-1) definește primele 65534 de poziții de cod (de "
"la 0x0000 la 0xfffd), care formează I<Planul multilingvistic de bază> "
"(„Basic Multilingual Plane”: BMP), adică planul 0 din grupul 0. Partea 2 a "
"standardului (ISO/IEC 10646-2) adaugă caractere la grupul 0 în afară de BMP "
"în mai multe I<planuri suplimentare> în intervalul 0x10000 - 0x10ffff. Nu "
"există planuri de adăugare a caracterelor dincolo de 0x10ffff la standard, "
"prin urmare, din întregul spațiu de cod, doar o mică parte din grupul 0 va "
"fi utilizată în viitorul apropiat. BMP conține toate caracterele care se "
"găsesc în celelalte seturi de caractere utilizate în mod obișnuit. Planurile "
"suplimentare adăugate de ISO/IEC 10646-2 acoperă doar caracterele mai "
"exotice pentru nevoi speciale în domeniul științific, al imprimării "
"dicționarelor, al industriei editoriale, al protocoalelor de nivel superior "
"și al entuziaștilor."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The representation of each UCS character as a 2-byte word is referred to as "
"the UCS-2 form (only for BMP characters), whereas UCS-4 is the "
"representation of each character by a 4-byte word.  In addition, there exist "
"two encoding forms UTF-8 for backward compatibility with ASCII processing "
"software and UTF-16 for the backward-compatible handling of non-BMP "
"characters up to 0x10ffff by UCS-2 software."
msgstr ""
"Reprezentarea fiecărui caracter UCS sub forma unui cuvânt de 2 octeți se "
"numește forma UCS-2 (numai pentru caracterele BMP), în timp ce UCS-4 este "
"reprezentarea fiecărui caracter sub forma unui cuvânt de 4 octeți. În plus, "
"există două forme de codificare: UTF-8 pentru compatibilitatea cu programele "
"de procesare ASCII și UTF-16 pentru gestionarea retro-compatibilă a "
"caracterelor non-BMP până la 0x10ffff de către programele UCS-2."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The UCS characters 0x0000 to 0x007f are identical to those of the classic US-"
"ASCII character set and the characters in the range 0x0000 to 0x00ff are "
"identical to those in ISO/IEC\\ 8859-1 (Latin-1)."
msgstr ""
"Caracterele UCS de la 0x0000 la 0x007f sunt identice cu cele din setul "
"clasic de caractere US-ASCII, iar caracterele din intervalul de la 0x0000 la "
"0x00ff sunt identice cu cele din ISO/IEC 8859-1 (Latin-1)."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Combining characters"
msgstr "Combinarea caracterelor"

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid ""
"Some code points in UCS have been assigned to I<combining characters>.  "
"These are similar to the nonspacing accent keys on a typewriter.  A "
"combining character just adds an accent to the previous character.  The most "
"important accented characters have codes of their own in UCS; however, the "
"combining character mechanism allows us to add accents and other diacritical "
"marks to any character.  The combining characters always follow the "
"character which they modify.  For example, the German character Umlaut-A "
"(\"Latin capital letter A with diaeresis\") can either be represented by the "
"precomposed UCS code 0x00c4, or alternatively as the combination of a normal "
"\"Latin capital letter A\" followed by a \"combining diaeresis\": 0x0041 "
"0x0308."
msgstr ""
"Unele puncte de cod din UCS au fost atribuite pentru I<caractere de "
"combinare>. Acestea sunt similare cu tastele de accent fără spațiere de pe o "
"mașină de scris. O combinație de caractere nu face decât să adauge un accent "
"la caracterul anterior. Cele mai importante caractere accentuate au coduri "
"proprii în UCS; însă mecanismul de combinare a caracterelor ne permite să "
"adăugăm accente și alte semne diacritice la orice caracter. Un caracter de "
"combinare nu face decât să adauge un accent la caracterul anterior. Cele mai "
"importante caractere accentuate au coduri proprii în UCS, însă mecanismul de "
"combinare a caracterelor ne permite să adăugăm accente și alte semne "
"diacritice la orice caracter. Caracterele de combinare urmează întotdeauna "
"caracterul pe care îl modifică. De exemplu, caracterul german Umlaut-A "
"(„Litera majusculă latină A cu diereză”) poate fi reprezentat fie prin codul "
"UCS precompus 0x00c4, fie ca o combinație de „Litera majusculă latină A” "
"normală urmată de o „diereză combinatorie”: 0x0041 0x0308."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Combining characters are essential for instance for encoding the Thai script "
"or for mathematical typesetting and users of the International Phonetic "
"Alphabet."
msgstr ""
"Combinarea caracterelor este esențială, de exemplu, pentru codificarea "
"scriptului thailandez sau pentru compunerea matematică și utilizatorii "
"alfabetului fonetic internațional."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Implementation levels"
msgstr "Niveluri de implementare"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"As not all systems are expected to support advanced mechanisms like "
"combining characters, ISO/IEC 10646-1 specifies the following three "
"I<implementation levels> of UCS:"
msgstr ""
"Deoarece nu se așteaptă ca toate sistemele să accepte mecanisme avansate, "
"cum ar fi combinarea caracterelor, ISO/IEC 10646-1 specifică următoarele "
"trei I<niveluri de implementare> ale UCS:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Level 1"
msgstr "Nivelul 1"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Combining characters and Hangul Jamo (a variant encoding of the Korean "
"script, where a Hangul syllable glyph is coded as a triplet or pair of vowel/"
"consonant codes) are not supported."
msgstr ""
"Caracterele combinate și Hangul Jamo (o variantă de codificare a alfabetului "
"coreean, în care o pictogramă de silabă Hangul este codificată ca un triplet "
"sau o pereche de coduri de vocale/consonante) nu sunt acceptate."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Level 2"
msgstr "Nivelul 2"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In addition to level 1, combining characters are now allowed for some "
"languages where they are essential (e.g., Thai, Lao, Hebrew, Arabic, "
"Devanagari, Malayalam)."
msgstr ""
"În plus față de nivelul 1, acum sunt permise combinații de caractere pentru "
"unele limbi în care acestea sunt esențiale (de exemplu, thailandeză, lao, "
"ebraică, arabă, devanagari, malaieziană)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Level 3"
msgstr "Nivelul 3"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "All UCS characters are supported."
msgstr "Sunt acceptate toate caracterele UCS."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The Unicode 3.0 Standard published by the Unicode Consortium contains "
"exactly the UCS Basic Multilingual Plane at implementation level 3, as "
"described in ISO/IEC 10646-1:2000.  Unicode 3.1 added the supplemental "
"planes of ISO/IEC 10646-2.  The Unicode standard and technical reports "
"published by the Unicode Consortium provide much additional information on "
"the semantics and recommended usages of various characters.  They provide "
"guidelines and algorithms for editing, sorting, comparing, normalizing, "
"converting, and displaying Unicode strings."
msgstr ""
"Standardul Unicode 3.0 publicat de Consorțiul Unicode conține exact planul "
"multilingvistic de bază UCS la nivelul de implementare 3, așa cum este "
"descris în ISO/IEC 10646-1:2000. Unicode 3.1 a adăugat planurile "
"suplimentare din ISO/IEC 10646-2. Standardul Unicode și rapoartele tehnice "
"publicate de Consorțiul Unicode oferă numeroase informații suplimentare "
"privind semantica și utilizările recomandate pentru diverse caractere. "
"Acestea oferă orientări și algoritmi pentru editarea, sortarea, compararea, "
"normalizarea, conversia și afișarea șirurilor Unicode."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Unicode under Linux"
msgstr "Unicode în Linux"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Under GNU/Linux, the C type I<wchar_t> is a signed 32-bit integer type.  Its "
"values are always interpreted by the C library as UCS code values (in all "
"locales), a convention that is signaled by the GNU C library to applications "
"by defining the constant B<__STDC_ISO_10646__> as specified in the ISO C99 "
"standard."
msgstr ""
"În GNU/Linux, tipul C I<wchar_t> este un tip de număr întreg cu semn (+/-) "
"pe 32 de biți. Valorile sale sunt întotdeauna interpretate de biblioteca C "
"ca valori de cod UCS (în toate localizările), convenție care este semnalată "
"de biblioteca C GNU C aplicațiilor prin definirea constantei "
"B<__STDC_ISO_10646__> așa cum este specificată în standardul ISO C99."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"UCS/Unicode can be used just like ASCII in input/output streams, terminal "
"communication, plaintext files, filenames, and environment variables in the "
"ASCII compatible UTF-8 multibyte encoding.  To signal the use of UTF-8 as "
"the character encoding to all applications, a suitable I<locale> has to be "
"selected via environment variables (e.g., \"LANG=en_GB.UTF-8\")."
msgstr ""
"UCS/Unicode poate fi utilizat la fel ca ASCII în fluxurile de intrare/"
"ieșire, în comunicarea prin terminal, în fișierele de text simplu, în numele "
"fișierelor și în variabilele de mediu în codificarea multiocteți UTF-8 "
"compatibilă cu ASCII. Pentru a semnala tuturor aplicațiilor utilizarea UTF-8 "
"ca codificare a caracterelor, trebuie să se selecteze o configurare "
"regională adecvată (I<locale>) prin intermediul variabilelor de mediu (de "
"exemplu, „LANG=en_GB.UTF-8”)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<nl_langinfo(CODESET)> function returns the name of the selected "
"encoding.  Library functions such as B<wctomb>(3)  and B<mbsrtowcs>(3)  can "
"be used to transform the internal I<wchar_t> characters and strings into the "
"system character encoding and back and B<wcwidth>(3)  tells how many "
"positions (0\\[en]2) the cursor is advanced by the output of a character."
msgstr ""
"Funcția B<nl_langinfo(CODESET)> returnează numele codificării selectate. "
"Funcțiile de bibliotecă, cum ar fi B<wctomb>(3) și B<mbsrtowcs>(3), pot fi "
"utilizate pentru a transforma caracterele și șirurile interne I<wchar_t> în "
"codificarea caracterelor de sistem și invers, iar B<wcwidth>(3) informează "
"cu câte poziții (0\\[en]2) este avansat cursorul prin ieșirea unui caracter."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Private Use Areas (PUA)"
msgstr "Zone de utilizare privată („Private Use Areas”: PUA)"

#.  commit 9d85025b0418163fae079c9ba8f8445212de8568
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In the Basic Multilingual Plane, the range 0xe000 to 0xf8ff will never be "
"assigned to any characters by the standard and is reserved for private "
"usage.  For the Linux community, this private area has been subdivided "
"further into the range 0xe000 to 0xefff which can be used individually by "
"any end-user and the Linux zone in the range 0xf000 to 0xf8ff where "
"extensions are coordinated among all Linux users.  The registry of the "
"characters assigned to the Linux zone is maintained by LANANA and the "
"registry itself is I<Documentation/admin-guide/unicode.rst> in the Linux "
"kernel sources (or I<Documentation/unicode.txt> before Linux 4.10)."
msgstr ""
"În planul multilingvistic de bază, intervalul de la 0xe000 la 0xf8ff nu va "
"fi niciodată atribuit niciunui caracter de către standard și este rezervat "
"pentru uz privat. Pentru comunitatea Linux, această zonă privată a fost "
"subdivizată în continuare în intervalul 0xe000 - 0xefff, care poate fi "
"utilizat individual de orice utilizator final, și în zona Linux din "
"intervalul 0xf000 - 0xf8ff, unde extensiile sunt coordonate între toți "
"utilizatorii Linux. Registrul caracterelor atribuite zonei Linux este "
"menținut de LANANA, iar registrul propriu-zis este I<Documentation/admin-"
"guide/unicode.rst> în sursele nucleului Linux (sau I<Documentation/"
"unicode.txt> înainte de Linux 4.10)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Two other planes are reserved for private usage, plane 15 (Supplementary "
"Private Use Area-A, range 0xf0000 to 0xffffd)  and plane 16 (Supplementary "
"Private Use Area-B, range 0x100000 to 0x10fffd)."
msgstr ""
"Alte două planuri sunt rezervate pentru uz privat, planul 15 (Zona "
"suplimentară de utilizare privată-A, intervalul de la 0xf0000 la 0xffffd) și "
"planul 16 (Zona suplimentară de utilizare privată-B, intervalul de la "
"0x100000 la 0x10fffd)."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Literature"
msgstr "Literatură"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr "\\[bu]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Information technology \\[em] Universal Multiple-Octet Coded Character Set "
"(UCS) \\[em] Part 1: Architecture and Basic Multilingual Plane.  "
"International Standard ISO/IEC 10646-1, International Organization for "
"Standardization, Geneva, 2000."
msgstr ""
"Tehnologia informației \\[em] Set universal de caractere codificate cu "
"octeți multipli (UCS) \\[em] Partea 1: Arhitectura și planul multilingvistic "
"de bază. Standardul internațional ISO/IEC 10646-1, Organizația "
"Internațională pentru Standardizare, Geneva, 2000."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This is the official specification of UCS.  Available from E<.UR http://"
"www.iso.ch/> E<.UE .>"
msgstr ""
"Aceasta este specificația oficială a UCS. Disponibilă la E<.UR http://"
"www.iso.ch/> E<.UE .>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The Unicode Standard, Version 3.0.  The Unicode Consortium, Addison-Wesley, "
"Reading, MA, 2000, ISBN 0-201-61633-5."
msgstr ""
"Standardul Unicode, versiunea 3.0. The Unicode Consortium, Addison-Wesley, "
"Reading, MA, 2000, ISBN 0-201-61633-5."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"S.\\& Harbison, G.\\& Steele. C: A Reference Manual. Fourth edition, "
"Prentice Hall, Englewood Cliffs, 1995, ISBN 0-13-326224-3."
msgstr ""
"S.\\& Harbison, G.\\& Steele. C: Un manual de referință. A patra ediție, "
"Prentice Hall, Englewood Cliffs, 1995, ISBN 0-13-326224-3."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"A good reference book about the C programming language.  The fourth edition "
"covers the 1994 Amendment 1 to the ISO C90 standard, which adds a large "
"number of new C library functions for handling wide and multibyte character "
"encodings, but it does not yet cover ISO C99, which improved wide and "
"multibyte character support even further."
msgstr ""
"O bună carte de referință despre limbajul de programare C. Cea de-a patra "
"ediție acoperă amendamentul 1 din 1994 la standardul ISO C90, care adaugă un "
"număr mare de noi funcții de bibliotecă C pentru gestionarea codurilor de "
"caractere late și multioctet, dar nu acoperă încă ISO C99, care a "
"îmbunătățit și mai mult suportul pentru caracterele late și multioctet."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Unicode Technical Reports."
msgstr "Rapoarte tehnice Unicode."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "E<.UR http://www.unicode.org\\:/reports/> E<.UE>"
msgstr "E<.UR http://www.unicode.org\\:/reports/> E<.UE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Markus Kuhn: UTF-8 and Unicode FAQ for UNIX/Linux."
msgstr "Markus Kuhn: UTF-8 și Unicode FAQ pentru UNIX/Linux."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "E<.UR http://www.cl.cam.ac.uk\\:/\\[ti]mgk25\\:/unicode.html> E<.UE>"
msgstr "E<.UR http://www.cl.cam.ac.uk\\:/\\[ti]mgk25\\:/unicode.html> E<.UE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Bruno Haible: Unicode HOWTO."
msgstr "Bruno Haible: Unicode HOWTO."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "E<.UR http://www.tldp.org\\:/HOWTO\\:/Unicode-HOWTO.html> E<.UE>"
msgstr "E<.UR http://www.tldp.org\\:/HOWTO\\:/Unicode-HOWTO.html> E<.UE>"

#.  .SH AUTHOR
#.  Markus Kuhn <mgk25@cl.cam.ac.uk>
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<locale>(1), B<setlocale>(3), B<charsets>(7), B<utf-8>(7)"
msgstr "B<locale>(1), B<setlocale>(3), B<charsets>(7), B<utf-8>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 februarie 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pagini de manual de Linux 6.03"

#. type: Plain text
#: debian-bookworm
msgid ""
"The international standard ISO 10646 defines the Universal Character Set "
"(UCS).  UCS contains all characters of all other character set standards.  "
"It also guarantees \"round-trip compatibility\"; in other words, conversion "
"tables can be built such that no information is lost when a string is "
"converted from any other encoding to UCS and back."
msgstr ""
"Standardul internațional ISO 10646 definește setul universal de caractere "
"(„Universal Character Set”: UCS). UCS conține toate caracterele din toate "
"celelalte standarde de seturi de caractere. De asemenea, garantează "
"„compatibilitatea dus-întors”; cu alte cuvinte, tabelele de conversie pot fi "
"construite astfel încât să nu se piardă nicio informație atunci când un șir "
"de caractere este convertit din orice altă codificare în UCS și invers."

#. type: Plain text
#: debian-bookworm
msgid ""
"The UCS standard (ISO 10646) describes a 31-bit character set architecture "
"consisting of 128 24-bit I<groups>, each divided into 256 16-bit I<planes> "
"made up of 256 8-bit I<rows> with 256 I<column> positions, one for each "
"character.  Part 1 of the standard (ISO 10646-1)  defines the first 65534 "
"code positions (0x0000 to 0xfffd), which form the I<Basic Multilingual "
"Plane> (BMP), that is plane 0 in group 0.  Part 2 of the standard (ISO "
"10646-2)  adds characters to group 0 outside the BMP in several "
"I<supplementary planes> in the range 0x10000 to 0x10ffff.  There are no "
"plans to add characters beyond 0x10ffff to the standard, therefore of the "
"entire code space, only a small fraction of group 0 will ever be actually "
"used in the foreseeable future.  The BMP contains all characters found in "
"the commonly used other character sets.  The supplemental planes added by "
"ISO 10646-2 cover only more exotic characters for special scientific, "
"dictionary printing, publishing industry, higher-level protocol and "
"enthusiast needs."
msgstr ""
"Standardul UCS (ISO 10646) descrie o arhitectură a setului de caractere pe "
"31 de biți care constă din 128 de I<grupuri> pe 24 de biți, fiecare fiind "
"împărțit în 256 de I<planuri> pe 16 biți, alcătuite din 256 de I<rânduri> pe "
"8 biți cu 256 de poziții I<coloană>, una pentru fiecare caracter. Partea 1 a "
"standardului (ISO 10646-1) definește primele 65534 de poziții de cod (de la "
"0x0000 la 0xfffd), care formează I<Planul multilingvistic de bază> („Basic "
"Multilingual Plane”: BMP), adică planul 0 din grupul 0. Partea 2 a "
"standardului (ISO 10646-2) adaugă caractere la grupul 0 în afară de BMP în "
"mai multe I<planuri suplimentare> în intervalul 0x10000 - 0x10ffff. Nu "
"există planuri de adăugare a caracterelor dincolo de 0x10ffff la standard, "
"prin urmare, din întregul spațiu de cod, doar o mică parte din grupul 0 va "
"fi utilizată în viitorul apropiat. BMP conține toate caracterele care se "
"găsesc în celelalte seturi de caractere utilizate în mod obișnuit. Planurile "
"suplimentare adăugate de ISO 10646-2 acoperă doar caracterele mai exotice "
"pentru nevoi speciale în domeniul științific, al imprimării dicționarelor, "
"al industriei editoriale, al protocoalelor de nivel superior și al "
"entuziaștilor."

#. type: Plain text
#: debian-bookworm
msgid ""
"The UCS characters 0x0000 to 0x007f are identical to those of the classic US-"
"ASCII character set and the characters in the range 0x0000 to 0x00ff are "
"identical to those in ISO 8859-1 (Latin-1)."
msgstr ""
"Caracterele UCS de la 0x0000 la 0x007f sunt identice cu cele din setul "
"clasic de caractere US-ASCII, iar caracterele din intervalul de la 0x0000 la "
"0x00ff sunt identice cu cele din ISO 8859-1 (Latin-1)."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid ""
"Some code points in UCS have been assigned to I<combining characters>.  "
"These are similar to the nonspacing accent keys on a typewriter.  A "
"combining character just adds an accent to the previous character.  The most "
"important accented characters have codes of their own in UCS, however, the "
"combining character mechanism allows us to add accents and other diacritical "
"marks to any character.  The combining characters always follow the "
"character which they modify.  For example, the German character Umlaut-A "
"(\"Latin capital letter A with diaeresis\") can either be represented by the "
"precomposed UCS code 0x00c4, or alternatively as the combination of a normal "
"\"Latin capital letter A\" followed by a \"combining diaeresis\": 0x0041 "
"0x0308."
msgstr ""
"Unele puncte de cod din UCS au fost atribuite pentru I<caractere de "
"combinare>. Acestea sunt similare cu tastele de accent fără spațiere de pe o "
"mașină de scris. O combinație de caractere nu face decât să adauge un accent "
"la caracterul anterior. Cele mai importante caractere accentuate au coduri "
"proprii în UCS, însă mecanismul de combinare a caracterelor ne permite să "
"adăugăm accente și alte semne diacritice la orice caracter. Un caracter de "
"combinare nu face decât să adauge un accent la caracterul anterior. Cele mai "
"importante caractere accentuate au coduri proprii în UCS, însă mecanismul de "
"combinare a caracterelor ne permite să adăugăm accente și alte semne "
"diacritice la orice caracter. Caracterele de combinare urmează întotdeauna "
"caracterul pe care îl modifică. De exemplu, caracterul german Umlaut-A "
"(„Litera majusculă latină A cu diereză”) poate fi reprezentat fie prin codul "
"UCS precompus 0x00c4, fie ca o combinație de „Litera majusculă latină A” "
"normală urmată de o „diereză combinatorie”: 0x0041 0x0308."

#. type: Plain text
#: debian-bookworm
msgid ""
"As not all systems are expected to support advanced mechanisms like "
"combining characters, ISO 10646-1 specifies the following three "
"I<implementation levels> of UCS:"
msgstr ""
"Deoarece nu se așteaptă ca toate sistemele să accepte mecanisme avansate, "
"cum ar fi combinarea caracterelor, ISO 10646-1 specifică următoarele trei "
"I<niveluri de implementare> ale UCS:"

#. type: Plain text
#: debian-bookworm
msgid ""
"The Unicode 3.0 Standard published by the Unicode Consortium contains "
"exactly the UCS Basic Multilingual Plane at implementation level 3, as "
"described in ISO 10646-1:2000.  Unicode 3.1 added the supplemental planes of "
"ISO 10646-2.  The Unicode standard and technical reports published by the "
"Unicode Consortium provide much additional information on the semantics and "
"recommended usages of various characters.  They provide guidelines and "
"algorithms for editing, sorting, comparing, normalizing, converting, and "
"displaying Unicode strings."
msgstr ""
"Standardul Unicode 3.0 publicat de Consorțiul Unicode conține exact planul "
"multilingvistic de bază UCS la nivelul de implementare 3, așa cum este "
"descris în ISO 10646-1:2000. Unicode 3.1 a adăugat planurile suplimentare "
"din ISO 10646-2. Standardul Unicode și rapoartele tehnice publicate de "
"Consorțiul Unicode oferă numeroase informații suplimentare privind semantica "
"și utilizările recomandate pentru diverse caractere. Acestea oferă orientări "
"și algoritmi pentru editarea, sortarea, compararea, normalizarea, conversia "
"și afișarea șirurilor Unicode."

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "2024-05-02"
msgstr "2 mai 2024"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Pagini de manual de Linux 6.9.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2024-01-28"
msgstr "28 ianuarie 2024"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pagini de manual de Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Pagini de manual Linux (nepublicate)"
