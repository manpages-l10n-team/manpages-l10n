# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.19.0\n"
"POT-Creation-Date: 2025-02-28 16:55+0100\n"
"PO-Revision-Date: 2023-06-28 16:07+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "uts_namespaces"
msgstr "uts_namespaces"

#. type: TH
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2 mai 2024"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr "Pagini de manual de Linux 6.12"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "uts_namespaces - overview of Linux UTS namespaces"
msgstr "uts_namespaces - prezentare generală a spațiilor de nume Linux UTS"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"UTS namespaces provide isolation of two system identifiers: the hostname and "
"the NIS domain name.  These identifiers are set using B<sethostname>(2)  and "
"B<setdomainname>(2), and can be retrieved using B<uname>(2), "
"B<gethostname>(2), and B<getdomainname>(2).  Changes made to these "
"identifiers are visible to all other processes in the same UTS namespace, "
"but are not visible to processes in other UTS namespaces."
msgstr ""
"Spațiile de nume UTS asigură izolarea a doi identificatori de sistem: numele "
"de gazdă și numele de domeniu NIS.  Acești identificatori sunt stabiliți cu "
"B<sethostname>(2) și B<setdomainname>(2) și pot fi recuperați cu "
"B<uname>(2), B<gethostname>(2) și B<getdomainname>(2).  Modificările aduse "
"acestor identificatori sunt vizibile pentru toate celelalte procese din "
"același spațiu de nume UTS, dar nu sunt vizibile pentru procesele din alte "
"spații de nume UTS."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"When a process creates a new UTS namespace using B<clone>(2)  or "
"B<unshare>(2)  with the B<CLONE_NEWUTS> flag, the hostname and domain name "
"of the new UTS namespace are copied from the corresponding values in the "
"caller's UTS namespace."
msgstr ""
"Atunci când un proces creează un nou spațiu de nume UTS utilizând "
"B<clone>(2) sau B<unshare>(2) cu fanionul B<CLONE_NEWUTS>, numele de gazdă "
"și numele de domeniu ale noului spațiu de nume UTS sunt copiate de la "
"valorile corespunzătoare din spațiul de nume UTS al apelantului."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Use of UTS namespaces requires a kernel that is configured with the "
"B<CONFIG_UTS_NS> option."
msgstr ""
"Utilizarea spațiilor de nume UTS necesită un nucleu care să fie configurat "
"cu opțiunea B<CONFIG_UTS_NS>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<nsenter>(1), B<unshare>(1), B<clone>(2), B<getdomainname>(2), "
"B<gethostname>(2), B<setns>(2), B<uname>(2), B<unshare>(2), B<namespaces>(7)"
msgstr ""
"B<nsenter>(1), B<unshare>(1), B<clone>(2), B<getdomainname>(2), "
"B<gethostname>(2), B<setns>(2), B<uname>(2), B<unshare>(2), B<namespaces>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-12-04"
msgstr "4 decembrie 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pagini de manual de Linux 6.03"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Pagini de manual de Linux 6.9.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 octombrie 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pagini de manual de Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Pagini de manual Linux (nepublicate)"
