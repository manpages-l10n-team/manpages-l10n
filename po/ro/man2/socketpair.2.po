# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.20.0\n"
"POT-Creation-Date: 2025-02-28 16:50+0100\n"
"PO-Revision-Date: 2023-11-05 19:42+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "socketpair"
msgstr "socketpair"

#. type: TH
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "2024-07-23"
msgstr "23 iulie 2024"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr "Pagini de manual de Linux 6.12"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "socketpair - create a pair of connected sockets"
msgstr "socketpair - creează o pereche de socluri conectate"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "Standard C library (I<libc>,\\ I<-lc>)"
msgstr "Biblioteca C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>sys/socket.hE<gt>>\n"
msgstr "B<#include E<lt>sys/socket.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<int socketpair(int >I<domain>B<, int >I<type>B<, int >I<protocol>B<, int >I<sv>B<[2]);>\n"
msgstr "B<int socketpair(int >I<domain>B<, int >I<type>B<, int >I<protocol>B<, int >I<sv>B<[2]);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<socketpair>()  call creates an unnamed pair of connected sockets in "
"the specified I<domain>, of the specified I<type>, and using the optionally "
"specified I<protocol>.  For further details of these arguments, see "
"B<socket>(2)."
msgstr ""
"Apelul B<socketpair>() creează o pereche nenumită de socluri conectate în "
"I<domeniul> specificat, de I<tipul> specificat și utilizând I<protocolul> "
"specificat opțional.  Pentru mai multe detalii privind aceste argumente, a "
"se vedea B<socket>(2)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The file descriptors used in referencing the new sockets are returned in "
"I<sv[0]> and I<sv[1]>.  The two sockets are indistinguishable."
msgstr ""
"Descriptorii de fișiere utilizați pentru a face referire la noile socluri "
"sunt returnate în I<sv[0]> și I<sv[1]>.  Cele două socluri sunt de nedistins."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOAREA RETURNATĂ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On success, zero is returned.  On error, -1 is returned, I<errno> is set to "
"indicate the error, and I<sv> is left unchanged"
msgstr ""
"În caz de succes, se returnează zero.  În caz de eroare, se returnează -1, "
"I<errno> este configurată pentru a indica eroarea, iar I<sv> rămâne "
"neschimbată."

#.  http://austingroupbugs.net/view.php?id=483
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On Linux (and other systems), B<socketpair>()  does not modify I<sv> on "
"failure.  A requirement standardizing this behavior was added in "
"POSIX.1-2008 TC2."
msgstr ""
"În Linux (și în alte sisteme), B<socketpair>() nu modifică I<sv> în caz de "
"eșec.  O cerință care standardizează acest comportament a fost adăugată în "
"POSIX.1-2008 TC2."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERORI-IEȘIRE"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EAFNOSUPPORT>"
msgstr "B<EAFNOSUPPORT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The specified address family is not supported on this machine."
msgstr "Familia de adrese specificată nu este admisă pe această mașină."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The address I<sv> does not specify a valid part of the process address space."
msgstr ""
"Adresa I<sv> nu specifică o parte validă a spațiului de adrese al procesului."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EMFILE>"
msgstr "B<EMFILE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The per-process limit on the number of open file descriptors has been "
"reached."
msgstr ""
"Limita per proces a numărului de descriptori de fișiere deschise a fost "
"atinsă."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ENFILE>"
msgstr "B<ENFILE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The system-wide limit on the total number of open files has been reached."
msgstr ""
"Limita la nivel de sistem a numărului total de fișiere deschise a fost "
"atinsă."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EOPNOTSUPP>"
msgstr "B<EOPNOTSUPP>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The specified protocol does not support creation of socket pairs."
msgstr "Protocolul specificat nu acceptă crearea de perechi de socluri."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EPROTONOSUPPORT>"
msgstr "B<EPROTONOSUPPORT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The specified protocol is not supported on this machine."
msgstr "Protocolul specificat nu este acceptat pe această mașină."

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIUNI"

#.  commit: 70b03759e9ecfae400605fa34f3d7154cccbbba3
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On Linux, the only supported domains for this call are B<AF_UNIX> (or "
"synonymously, B<AF_LOCAL>)  and B<AF_TIPC> (since Linux 4.12)."
msgstr ""
"În Linux, singurele domenii acceptate pentru acest apel sunt B<AF_UNIX> "
"(sau, ca sinonim, B<AF_LOCAL>) și B<AF_TIPC> (începând cu Linux 4.12)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDE"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ISTORIC"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2001, 4.4BSD."
msgstr "POSIX.1-2001, 4.4BSD."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<socketpair>()  first appeared in 4.2BSD.  It is generally portable to/from "
"non-BSD systems supporting clones of the BSD socket layer (including "
"System\\ V variants)."
msgstr ""
"B<socketpair>() a apărut pentru prima dată în 4.2BSD.  În general, este "
"portabil către/de la sisteme non-BSD care acceptă clone ale stratului de "
"soclu BSD (inclusiv variantele System\\ V)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Since Linux 2.6.27, B<socketpair>()  supports the B<SOCK_NONBLOCK> and "
"B<SOCK_CLOEXEC> flags in the I<type> argument, as described in B<socket>(2)."
msgstr ""
"Începând cu Linux 2.6.27, B<socketpair>() acceptă fanioanele "
"B<SOCK_NONBLOCK> și B<SOCK_CLOEXEC> în argumentul I<type>, așa cum este "
"descris în B<socket>(2)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<pipe>(2), B<read>(2), B<socket>(2), B<write>(2), B<socket>(7), B<unix>(7)"
msgstr ""
"B<pipe>(2), B<read>(2), B<socket>(2), B<write>(2), B<socket>(7), B<unix>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-10-30"
msgstr "30 octombrie 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pagini de manual de Linux 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca C standard (I<libc>, I<-lc>)"

#. type: Plain text
#: debian-bookworm
msgid ""
"POSIX.1-2001, POSIX.1-2008, 4.4BSD.  B<socketpair>()  first appeared in "
"4.2BSD.  It is generally portable to/from non-BSD systems supporting clones "
"of the BSD socket layer (including System\\ V variants)."
msgstr ""
"POSIX.1-2001, POSIX.1-2008, 4.4BSD.  B<socketpair>() a apărut pentru prima "
"dată în 4.2BSD.  În general, este portabil către/de la sisteme non-BSD care "
"acceptă clone ale stratului de soclu BSD (inclusiv variantele System\\ V)."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NOTES"
msgstr "NOTE"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "2024-05-02"
msgstr "2 mai 2024"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Pagini de manual de Linux 6.9.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 octombrie 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pagini de manual de Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Pagini de manual Linux (nepublicate)"
