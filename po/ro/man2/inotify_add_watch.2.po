# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.20.0\n"
"POT-Creation-Date: 2025-02-28 16:36+0100\n"
"PO-Revision-Date: 2023-11-05 09:15+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "inotify_add_watch"
msgstr "inotify_add_watch"

#. type: TH
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "2024-07-23"
msgstr "23 iulie 2024"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr "Pagini de manual de Linux 6.12"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "inotify_add_watch - add a watch to an initialized inotify instance"
msgstr ""
"inotify_add_watch - adaugă o monitorizare la o instanță inotify inițializată"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "Standard C library (I<libc>,\\ I<-lc>)"
msgstr "Biblioteca C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>sys/inotify.hE<gt>>\n"
msgstr "B<#include E<lt>sys/inotify.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<int inotify_add_watch(int >I<fd>B<, const char *>I<pathname>B<, uint32_t >I<mask>B<);>\n"
msgstr "B<int inotify_add_watch(int >I<fd>B<, const char *>I<nume-rută>B<, uint32_t >I<masca>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<inotify_add_watch>()  adds a new watch, or modifies an existing watch, for "
"the file whose location is specified in I<pathname>; the caller must have "
"read permission for this file.  The I<fd> argument is a file descriptor "
"referring to the inotify instance whose watch list is to be modified.  The "
"events to be monitored for I<pathname> are specified in the I<mask> bit-mask "
"argument.  See B<inotify>(7)  for a description of the bits that can be set "
"in I<mask>."
msgstr ""
"B<inotify_add_watch>() adaugă o nouă monitorizare sau modifică o "
"monitorizare existentă pentru fișierul a cărui locație este specificată în "
"I<nume-rută>; apelantul trebuie să aibă permisiunea de citire pentru acest "
"fișier.  Argumentul I<fd> este un descriptor de fișier care se referă la "
"instanța inotify a cărei listă de monitorizare urmează să fie modificată.  "
"Evenimentele care trebuie monitorizate pentru I<nume-rută> sunt specificate "
"în argumentul I<masca> bit-mask.  A se vedea B<inotify>(7) pentru o "
"descriere a biților care pot fi definiți în I<masca>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"A successful call to B<inotify_add_watch>()  returns a unique watch "
"descriptor for this inotify instance, for the filesystem object (inode) that "
"corresponds to I<pathname>.  If the filesystem object was not previously "
"being watched by this inotify instance, then the watch descriptor is newly "
"allocated.  If the filesystem object was already being watched (perhaps via "
"a different link to the same object), then the descriptor for the existing "
"watch is returned."
msgstr ""
"Un apel reușit la B<inotify_add_watch>() returnează un descriptor de "
"monitorizare unic pentru această instanță inotify, pentru obiectul "
"sistemului de fișiere (nod-i) care corespunde lui I<nume-rută>.  În cazul în "
"care obiectul sistemului de fișiere nu a fost monitorizat anterior de către "
"această instanță inotify, atunci descriptorul de monitorizare este alocat "
"recent.  În cazul în care obiectul sistemului de fișiere a fost deja "
"monitorizat (poate prin intermediul unei legături diferite către același "
"obiect), se returnează descriptorul pentru monitorizarea existentă."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The watch descriptor is returned by later B<read>(2)s from the inotify file "
"descriptor.  These reads fetch I<inotify_event> structures (see "
"B<inotify>(7))  indicating filesystem events; the watch descriptor inside "
"this structure identifies the object for which the event occurred."
msgstr ""
"Descriptorul de monitorizare este returnat de către citirile B<read>(2) "
"ulterioare din descriptorul de fișier inotify.  Aceste citiri preiau "
"structurile I<inotify_event> (a se vedea B<inotify>(7)) care indică "
"evenimentele din sistemul de fișiere; descriptorul de ceas din această "
"structură identifică obiectul pentru care a avut loc evenimentul."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOAREA RETURNATĂ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On success, B<inotify_add_watch>()  returns a watch descriptor (a "
"nonnegative integer).  On error, -1 is returned and I<errno> is set to "
"indicate the error."
msgstr ""
"În caz de succes, B<inotify_add_watch>() returnează un descriptor de "
"monitorizare (un număr întreg nenegativ).  În caz de eroare, se returnează "
"-1, iar I<errno> este configurată pentru a indica eroarea."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERORI-IEȘIRE"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EACCES>"
msgstr "B<EACCES>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Read access to the given file is not permitted."
msgstr "Nu este permis accesul de citire la fișierul dat."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EBADF>"
msgstr "B<EBADF>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The given file descriptor is not valid."
msgstr "Descriptorul de fișier dat nu este valid."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EEXIST>"
msgstr "B<EEXIST>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<mask> contains B<IN_MASK_CREATE> and I<pathname> refers to a file already "
"being watched by the same I<fd>."
msgstr ""
"I<masca> conține B<IN_MASK_CREATE>, iar I<nume-rută> se referă la un fișier "
"care este deja monitorizat de același I<fd>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<pathname> points outside of the process's accessible address space."
msgstr "I<nume-rută> indică în afara spațiului de adrese accesibil procesului."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The given event mask contains no valid events; or I<mask> contains both "
"B<IN_MASK_ADD> and B<IN_MASK_CREATE>; or I<fd> is not an inotify file "
"descriptor."
msgstr ""
"Masca de eveniment dată nu conține evenimente valide; sau I<masca> conține "
"atât B<IN_MASK_ADD>, cât și B<IN_MASK_CREATE>; sau I<fd> nu este un "
"descriptor de fișier inotify."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ENAMETOOLONG>"
msgstr "B<ENAMETOOLONG>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<pathname> is too long."
msgstr "I<nume-rută> este prea lung."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOENT>"
msgstr "B<ENOENT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"A directory component in I<pathname> does not exist or is a dangling "
"symbolic link."
msgstr ""
"O componentă de director din I<nume-rută> nu există sau este o legătură "
"simbolică incertă."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr "B<ENOMEM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Insufficient kernel memory was available."
msgstr "Nu a fost disponibilă suficientă memorie pentru nucleu."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOSPC>"
msgstr "B<ENOSPC>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The user limit on the total number of inotify watches was reached or the "
"kernel failed to allocate a needed resource."
msgstr ""
"Limita utilizatorului privind numărul total de monitorizări inotify a fost "
"atinsă sau nucleul nu a reușit să aloce o resursă necesară."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOTDIR>"
msgstr "B<ENOTDIR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<mask> contains B<IN_ONLYDIR> and I<pathname> is not a directory."
msgstr "I<masca> conține B<IN_ONLYDIR>, iar I<nume-rută> nu este un director."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDE"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Linux."
msgstr "Linux."

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ISTORIC"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Linux 2.6.13."
msgstr "Linux 2.6.13."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "EXEMPLE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "See B<inotify>(7)."
msgstr "A se vedea B<inotify>(7)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<inotify_init>(2), B<inotify_rm_watch>(2), B<inotify>(7)"
msgstr "B<inotify_init>(2), B<inotify_rm_watch>(2), B<inotify>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-10-30"
msgstr "30 octombrie 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pagini de manual de Linux 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca C standard (I<libc>, I<-lc>)"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIUNI"

#. type: Plain text
#: debian-bookworm
msgid "Inotify was merged into the 2.6.13 Linux kernel."
msgstr "Inotify a fost introdusă în nucleul Linux 2.6.13."

#. type: Plain text
#: debian-bookworm
msgid "This system call is Linux-specific."
msgstr "Acest apel de sistem este specific pentru Linux."

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "2024-05-02"
msgstr "2 mai 2024"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Pagini de manual de Linux 6.9.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 octombrie 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pagini de manual de Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Pagini de manual Linux (nepublicate)"
