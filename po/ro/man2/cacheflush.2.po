# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.21.0\n"
"POT-Creation-Date: 2025-02-28 16:30+0100\n"
"PO-Revision-Date: 2025-02-02 01:05+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.5\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "cacheflush"
msgstr "cacheflush"

#. type: TH
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "2024-11-17"
msgstr "17 noiembrie 2024"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr "Pagini de manual de Linux 6.12"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "cacheflush - flush contents of instruction and/or data cache"
msgstr ""
"cacheflush - șterge conținutul cache-ului de instrucțiuni și/sau de date"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "Standard C library (I<libc>,\\ I<-lc>)"
msgstr "Biblioteca C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>sys/cachectl.hE<gt>>\n"
msgstr "B<#include E<lt>sys/cachectl.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<int cacheflush(void >I<addr>B<[.>I<nbytes>B<], int >I<nbytes>B<, int >I<cache>B<);>\n"
msgstr "B<int cacheflush(void >I<addr>B<[.>I<nbytes>B<], int >I<nbytes>B<, int >I<cache>B<);>\n"

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid ""
"I<Note>: On some architectures, there is no glibc wrapper for this system "
"call; see VERSIONS."
msgstr ""
"I<Notă>: Pe unele arhitecturi, nu există o funcție de învăluire glibc pentru "
"acest apel de sistem; a se vedea secțiunea VERSIUNI."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<cacheflush>()  flushes the contents of the indicated cache(s) for the user "
"addresses in the range I<addr> to I<(addr+nbytes-1)>.  I<cache> may be one "
"of:"
msgstr ""
"B<cacheflush>() golește conținutul cache-urilor indicate pentru adresele "
"utilizatorului din intervalul I<addr> până la I<(addr+nbytes-1)>.  I<cache> "
"poate fi unul dintre următoarele:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ICACHE>"
msgstr "B<ICACHE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Flush the instruction cache."
msgstr "Golește memoria cache de instrucțiuni."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<DCACHE>"
msgstr "B<DCACHE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Write back to memory and invalidate the affected valid cache lines."
msgstr ""
"Scrie înapoi în memorie și invalidează liniile de memorie cache valide "
"afectate."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<BCACHE>"
msgstr "B<BCACHE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Same as B<(ICACHE|DCACHE)>."
msgstr "La fel ca B<(ICACHE|DCACHE)>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOAREA RETURNATĂ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<cacheflush>()  returns 0 on success.  On error, it returns -1 and sets "
"I<errno> to indicate the error."
msgstr ""
"În caz de succes, B<cacheflush>() returnează 0; în caz contrar, returnează "
"-1 și configurează I<errno> pentru a indica eroarea."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERORI-IEȘIRE"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Some or all of the address range I<addr> to I<(addr+nbytes-1)> is not "
"accessible."
msgstr ""
"O parte sau întregul interval de adrese de la I<addr> la I<(addr+nbytes-1)> "
"nu este accesibil."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<cache> is not one of B<ICACHE>, B<DCACHE>, or B<BCACHE> (but see BUGS)."
msgstr ""
"I<cache> nu este unul dintre B<ICACHE>, B<DCACHE> sau B<BCACHE> (dar "
"consultați secțiunea ERORI)."

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIUNI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<cacheflush>()  should not be used in programs intended to be portable.  On "
"Linux, this call first appeared on the MIPS architecture, but nowadays, "
"Linux provides a B<cacheflush>()  system call on some other architectures, "
"but with different arguments."
msgstr ""
"B<cacheflush>() nu ar trebui să fie utilizat în programe destinate să fie "
"portabile. Pe Linux, acest apel a apărut pentru prima dată pe arhitectura "
"MIPS, dar în prezent, Linux oferă un apel de sistem B<cacheflush>() pe alte "
"câteva arhitecturi, dar cu argumente diferite."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Architecture-specific variants"
msgstr "Variante specifice arhitecturii"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"glibc provides a wrapper for this system call, with the prototype shown in "
"SYNOPSIS, for the following architectures: ARC, CSKY, MIPS, and NIOS2."
msgstr ""
"glibc oferă o funcție de învăluire pentru acest apel de sistem, cu "
"prototipul prezentat în secțiunea REZUMAT, pentru următoarele arhitecturi: "
"ARC, CSKY, MIPS și NIOS2."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On some other architectures, Linux provides this system call, with different "
"arguments:"
msgstr ""
"Pe alte arhitecturi, Linux oferă acest apel de sistem, cu argumente diferite:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "M68K:"
msgstr "M68K:"

#. type: Plain text
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int cacheflush(unsigned long >I<addr>B<, int >I<scope>B<, int >I<cache>B<,>\n"
"B<               unsigned long >I<size>B<);>\n"
msgstr ""
"B<int cacheflush(unsigned long >I<addr>B<, int >I<scope>B<, int >I<cache>B<,>\n"
"B<               unsigned long >I<size>B<);>\n"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SH:"
msgstr "SH:"

#. type: Plain text
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "B<int cacheflush(unsigned long >I<addr>B<, unsigned long >I<size>B<, int >I<op>B<);>\n"
msgstr "B<int cacheflush(unsigned long >I<addr>B<, unsigned long >I<size>B<, int >I<op>B<);>\n"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NDS32:"
msgstr "NDS32:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<int cacheflush(unsigned int >I<start>B<, unsigned int >I<end>B<, int >I<cache>B<);>\n"
msgstr "B<int cacheflush(unsigned int >I<start>B<, unsigned int >I<end>B<, int >I<cache>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On the above architectures, glibc does not provide a wrapper for this system "
"call; call it using B<syscall>(2)."
msgstr ""
"Pe arhitecturile de mai sus, glibc nu oferă o funcție de învăluire pentru "
"acest apel de sistem; apelați-l folosind B<syscall>(2)."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "GCC alternative"
msgstr "Alternativa GCC"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Unless you need the finer grained control that this system call provides, "
"you probably want to use the GCC built-in function "
"B<__builtin___clear_cache>(), which provides a portable interface across "
"platforms supported by GCC and compatible compilers:"
msgstr ""
"Cu excepția cazului în care aveți nevoie de controlul mai fin pe care îl "
"oferă acest apel de sistem, probabil că doriți să utilizați funcția "
"încorporată în GCC B<__builtin___clear_cache>(), care oferă o interfață "
"portabilă pe toate platformele acceptate de GCC și de compilatoarele "
"compatibile:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<void __builtin___clear_cache(void *>I<begin>B<, void *>I<end>B<);>\n"
msgstr "B<void __builtin___clear_cache(void *>I<begin>B<, void *>I<end>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On platforms that don't require instruction cache flushes, "
"B<__builtin___clear_cache>()  has no effect."
msgstr ""
"Pe platformele care nu necesită curățarea cache-ului de instrucțiuni, "
"B<__builtin___clear_cache>() nu are niciun efect."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<Note>: On some GCC-compatible compilers, the prototype for this built-in "
"function uses I<char *> instead of I<void *> for the parameters."
msgstr ""
"I<Notă>: La unele compilatoare compatibile cu GCC, prototipul acestei "
"funcții integrate utilizează I<char *> în loc de I<void *> pentru parametri."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Historically, this system call was available on all MIPS UNIX variants "
"including RISC/os, IRIX, Ultrix, NetBSD, OpenBSD, and FreeBSD (and also on "
"some non-UNIX MIPS operating systems), so that the existence of this call in "
"MIPS operating systems is a de-facto standard."
msgstr ""
"Din punct de vedere istoric, acest apel de sistem a fost disponibil pe toate "
"variantele MIPS UNIX, inclusiv RISC/os, IRIX, Ultrix, NetBSD, OpenBSD și "
"FreeBSD (și, de asemenea, pe unele sisteme de operare MIPS non-UNIX), astfel "
"încât existența acestui apel în sistemele de operare MIPS este un standard "
"de-facto."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ERORI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Linux kernels older than Linux 2.6.11 ignore the I<addr> and I<nbytes> "
"arguments, making this function fairly expensive.  Therefore, the whole "
"cache is always flushed."
msgstr ""
"Nucleele Linux mai vechi decât Linux 2.6.11 ignoră argumentele I<addr> și "
"I<nbytes>, ceea ce face ca această funcție să fie destul de costisitoare. "
"Prin urmare, întreaga memorie cache este întotdeauna golită."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This function always behaves as if B<BCACHE> has been passed for the "
"I<cache> argument and does not do any error checking on the I<cache> "
"argument."
msgstr ""
"Această funcție se comportă întotdeauna ca și cum B<BCACHE> ar fi fost "
"trecut pentru argumentul I<cache> și nu efectuează nicio verificare a "
"erorilor la argumentul I<cache>."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 februarie 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pagini de manual de Linux 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca C standard (I<libc>, I<-lc>)"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid ""
"I<Note>: On some architectures, there is no glibc wrapper for this system "
"call; see NOTES."
msgstr ""
"I<Notă>: Pe unele arhitecturi, nu există o funcție de învăluire glibc pentru "
"acest apel de sistem; a se vedea secțiunea NOTE."

#. type: SS
#: debian-bookworm
#, no-wrap
msgid "Caveat"
msgstr "Avertisment"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NOTES"
msgstr "NOTE"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
#, no-wrap
msgid ""
"B<int cacheflush(unsigned long >I<addr>B<, int >I<scope>B<, int >I<cache>B<,>\n"
"B<               unsigned long >I<len>B<);>\n"
msgstr ""
"B<int cacheflush(unsigned long >I<addr>B<, int >I<scope>B<, int >I<cache>B<,>\n"
"B<               unsigned long >I<len>B<);>\n"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
#, no-wrap
msgid "B<int cacheflush(unsigned long >I<addr>B<, unsigned long >I<len>B<, int >I<op>B<);>\n"
msgstr "B<int cacheflush(unsigned long >I<addr>B<, unsigned long >I<len>B<, int >I<op>B<);>\n"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "2024-05-02"
msgstr "2 mai 2024"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Pagini de manual de Linux 6.9.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 octombrie 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pagini de manual de Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Pagini de manual Linux (nepublicate)"
