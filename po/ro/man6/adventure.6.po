# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.23.1\n"
"POT-Creation-Date: 2025-02-16 05:40+0100\n"
"PO-Revision-Date: 2024-09-10 01:37+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.4.3\n"

#. type: Dd
#: archlinux fedora-42 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "May 31, 1993"
msgstr "31 mai 1993"

#. type: Dt
#: archlinux fedora-42 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "ADVENTURE 6"
msgstr "ADVENTURE 6"

#. type: Sh
#: archlinux fedora-42 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "E<.Nm adventure>"
msgstr "E<.Nm adventure>"

#. type: Nd
#: archlinux fedora-42 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "an exploration game"
msgstr "un joc de explorare"

#. type: Sh
#: archlinux fedora-42 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"The object of the game is to locate and explore Colossal Cave, find the "
"treasures hidden there, and bring them back to the building with you.  The "
"program is self-descriptive to a point, but part of the game is to discover "
"its rules."
msgstr ""
"Scopul jocului este de a localiza și de a explora Peștera colosală „Colossal "
"Cave”, de a găsi comorile ascunse acolo și de a le aduce înapoi în clădire "
"cu tine. Programul este autodescriptiv până la un punct, dar o parte din joc "
"constă în a-i descoperi regulile."

#. type: Plain text
#: archlinux
msgid ""
"To terminate a game, enter E<.Dq quit>; to save a game for later resumption, "
"enter E<.Dq save>."
msgstr ""
"Pentru a încheia un joc, introduceți E<.Dq quit>; pentru a salva un joc în "
"vederea reluării ulterioare, introduceți E<.Dq save>."

#. type: Sh
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "E<.Nm> E<.Op saved-file>"
msgstr "E<.Nm> E<.Op fișier-salvare>"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"To terminate a game, enter E<.Dq quit>; to save a game for later resumption, "
"enter E<.Dq suspend>."
msgstr ""
"Pentru a încheia un joc, introduceți E<.Dq quit>; pentru a salva un joc în "
"vederea reluării ulterioare, introduceți E<.Dq suspend>."
