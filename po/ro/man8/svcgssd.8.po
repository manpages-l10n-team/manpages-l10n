# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.20.0\n"
"POT-Creation-Date: 2024-08-02 17:33+0200\n"
"PO-Revision-Date: 2023-11-05 00:03+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: debian-bookworm debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "rpc.svcgssd"
msgstr "rpc.svcgssd"

#. type: TH
#: debian-bookworm debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "12 Jan 2007"
msgstr "12 ianuarie 2007"

#. type: SH
#: debian-bookworm debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: debian-bookworm debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid "rpc.svcgssd - server-side rpcsec_gss daemon"
msgstr "rpc.svcgssd - demonul rpcsec_gss pe partea de server"

#. type: SH
#: debian-bookworm debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: debian-bookworm debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<rpc.svcgssd [-n] [-v] [-r] [-i] [-f] [-p principal]>"
msgstr "B<rpc.svcgssd [-n] [-v] [-r] [-i] [-f] [-p principal]>"

#. type: SH
#: debian-bookworm debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: debian-bookworm debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The rpcsec_gss protocol gives a means of using the gss-api generic security "
"api to provide security for protocols using rpc (in particular, nfs).  "
"Before exchanging any rpc requests using rpcsec_gss, the rpc client must "
"first establish a security context with the rpc server.  The linux kernel's "
"implementation of rpcsec_gss depends on the userspace daemon B<rpc.svcgssd> "
"to handle context establishment on the rpc server.  The daemon uses files in "
"the proc filesystem to communicate with the kernel."
msgstr ""
"Protocolul rpcsec_gss oferă un mijloc de utilizare a api de securitate "
"generică gss-api pentru a asigura securitatea protocoalelor care utilizează "
"rpc (în special, nfs).  Înainte de a face schimb de cereri rpc utilizând "
"rpcsec_gss, clientul rpc trebuie să stabilească mai întâi un context de "
"securitate cu serverul rpc.  Implementarea rpcsec_gss în nucleul linux "
"depinde de demonul din spațiul utilizatorului B<rpc.svcgssd> pentru a se "
"ocupa de stabilirea contextului pe serverul rpc.  Daemonul utilizează "
"fișiere din sistemul de fișiere proc pentru a comunica cu nucleul."

#. type: SH
#: debian-bookworm debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPȚIUNI"

#. type: TP
#: debian-bookworm debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-f>"
msgstr "B<-f>"

#. type: Plain text
#: debian-bookworm debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Runs B<rpc.svcgssd> in the foreground and sends output to stderr (as opposed "
"to syslogd)"
msgstr ""
"Rulează B<rpc.svcgssd> în prim-plan și trimite ieșirea la ieșirea de eroare "
"standard (spre deosebire de «syslogd»)"

#. type: TP
#: debian-bookworm debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>"
msgstr "B<-v>"

#. type: Plain text
#: debian-bookworm debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Increases the verbosity of the output (can be specified multiple times)."
msgstr ""
"Mărește nivelul de detaliere al mesajelor informative de la ieșire (poate fi "
"specificată de mai multe ori)."

#. type: TP
#: debian-bookworm debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-r>"
msgstr "B<-r>"

#. type: Plain text
#: debian-bookworm debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the rpcsec_gss library supports setting debug level, increases the "
"verbosity of the output (can be specified multiple times)."
msgstr ""
"În cazul în care biblioteca rpcsec_gss acceptă definirea nivelului de "
"depanare, crește nivelul de detaliere al mesajelor informative de la ieșire "
"(poate fi specificată de mai multe ori)."

#. type: TP
#: debian-bookworm debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-i>"
msgstr "B<-i>"

#. type: Plain text
#: debian-bookworm debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the nfsidmap library supports setting debug level, increases the "
"verbosity of the output (can be specified multiple times)."
msgstr ""
"În cazul în care biblioteca nfsidmap acceptă definirea nivelului de "
"depanare, crește nivelul de detaliere al mesajelor informative de la ieșire "
"(poate fi specificată de mai multe ori)."

#. type: TP
#: debian-bookworm debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-p>"
msgstr "B<-p>"

#. type: Plain text
#: debian-bookworm debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid "Use I<principal> instead of the default nfs/I<FQDN>@I<REALM>."
msgstr "Utilizează I<principal> în loc de nfs/I<FQDN>@I<REALM> implicit."

#. type: TP
#: debian-bookworm debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-n>"
msgstr "B<-n>"

#. type: Plain text
#: debian-bookworm debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Use the system default credentials (host/I<FQDN>@I<REALM>)  rather than the "
"default nfs/I<FQDN>@I<REALM>."
msgstr ""
"Utilizează acreditările implicite ale sistemului (gazdă/I<FQDN>@I<REALM>) în "
"loc de cele implicite nfs/I<FQDN>@I<REALM>."

#. type: SH
#: debian-bookworm debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "CONFIGURATION FILE"
msgstr "FIȘIER DE CONFIGURARE"

#. type: Plain text
#: debian-bookworm debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Some of the options that can be set on the command line can also be "
"controlled through values set in the B<[svcgssd]> section of the I</etc/"
"nfs.conf> configuration file.  Values recognized include:"
msgstr ""
"Unele dintre opțiunile care pot fi definite în linia de comandă pot fi, de "
"asemenea, controlate prin intermediul valorilor stabilite în secțiunea "
"B<[svcgssd]> din fișierul de configurare I</etc/nfs.conf>.  Valorile "
"recunoscute includ:"

#. type: TP
#: debian-bookworm debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<principal>"
msgstr "B<principal>"

#. type: Plain text
#: debian-bookworm debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If set to B<system> this is equivalent to the B<-n> option.  If set to any "
"other value, that is used like the B<-p> option."
msgstr ""
"Dacă este stabilită la B<system>, aceasta este echivalentă cu opțiunea B<-"
"n>.  Dacă este stabilită la orice altă valoare, aceasta este utilizată ca și "
"opțiunea B<-p>."

#. type: TP
#: debian-bookworm debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<verbosity>"
msgstr "B<verbosity>"

#. type: Plain text
#: debian-bookworm debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid "Value which is equivalent to the number of B<-v>."
msgstr "Valoare care este echivalentă cu numărul pentru B<-v>."

#. type: TP
#: debian-bookworm debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<rpc-verbosity>"
msgstr "B<rpc-verbosity>"

#. type: Plain text
#: debian-bookworm debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid "Value which is equivalent to the number of B<-r>."
msgstr "Valoare care este echivalentă cu numărul pentru B<-r>."

#. type: TP
#: debian-bookworm debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<idmap-verbosity>"
msgstr "B<idmap-verbosity>"

#. type: Plain text
#: debian-bookworm debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid "Value which is equivalent to the number of B<-i>."
msgstr "Valoare care este echivalentă cu numărul de B<-i>."

#. type: SH
#: debian-bookworm debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: debian-bookworm debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<rpc.gssd(8),>"
msgstr "B<rpc.gssd(8),>"

#. type: SH
#: debian-bookworm debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AUTHORS"
msgstr "AUTORI"

#. type: Plain text
#: debian-bookworm debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid "Dug Song E<lt>dugsong@umich.eduE<gt>"
msgstr "Dug Song E<lt>dugsong@umich.eduE<gt>"

#. type: Plain text
#: debian-bookworm debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid "Andy Adamson E<lt>andros@umich.eduE<gt>"
msgstr "Andy Adamson E<lt>andros@umich.eduE<gt>"

#. type: Plain text
#: debian-bookworm debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid "Marius Aamodt Eriksen E<lt>marius@umich.eduE<gt>"
msgstr "Marius Aamodt Eriksen E<lt>marius@umich.eduE<gt>"

#. type: Plain text
#: debian-bookworm debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid "J. Bruce Fields E<lt>bfields@umich.eduE<gt>"
msgstr "J. Bruce Fields E<lt>bfields@umich.eduE<gt>"
