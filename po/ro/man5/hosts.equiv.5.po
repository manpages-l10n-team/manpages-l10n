# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.21.0\n"
"POT-Creation-Date: 2025-02-28 16:36+0100\n"
"PO-Revision-Date: 2024-02-15 23:41+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "hosts.equiv"
msgstr "hosts.equiv"

#. type: TH
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2 mai 2024"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr "Pagini de manual de Linux 6.12"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"hosts.equiv - list of hosts and users that are granted \"trusted\" B<r> "
"command access to your system"
msgstr ""
"hosts.echiv - lista de gazde și utilizatori cărora li se acordă acces de "
"„încredere” la comanda B<r> a sistemului dvs."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The file I</etc/hosts.equiv> allows or denies hosts and users to use the "
"B<r>-commands (e.g., B<rlogin>, B<rsh>, or B<rcp>)  without supplying a "
"password."
msgstr ""
"Fișierul I</etc/hosts.equiv> permite sau refuză ca gazdele și utilizatorii "
"să utilizeze comenzile B<r> (de exemplu, B<rlogin>, B<rsh> sau B<rcp>) fără "
"a furniza o parolă."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The file uses the following format:"
msgstr "Fișierul utilizează următorul format:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<+|[-]hostname|+@netgroup|-@netgroup> I<[+|[-]username|+@netgroup|-@netgroup]>"
msgstr "I<+|[-]nume-gazdă|+@grup-rețea|-@grup-rețea> I<[+|[-]nume-utilizator|+@grup-rețea|-@grup-rețea]>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The I<hostname> is the name of a host which is logically equivalent to the "
"local host.  Users logged into that host are allowed to access like-named "
"user accounts on the local host without supplying a password.  The "
"I<hostname> may be (optionally) preceded by a plus (+) sign.  If the plus "
"sign is used alone, it allows any host to access your system.  You can "
"explicitly deny access to a host by preceding the I<hostname> by a minus (-) "
"sign.  Users from that host must always supply additional credentials, "
"including possibly a password.  For security reasons you should always use "
"the FQDN of the hostname and not the short hostname."
msgstr ""
"I<nume-gazdă> este numele unei gazde care este logic echivalent cu gazda "
"locală. Utilizatorii conectați la această gazdă pot accesa conturile de "
"utilizator cu nume similar de pe gazda locală fără a furniza o parolă. "
"I<nume-gazdă> poate fi (opțional) precedat de un semn plus (+). Dacă semnul "
"plus este utilizat singur, acesta permite oricărei gazde să acceseze "
"sistemul dumneavoastră. Puteți refuza în mod explicit accesul la o gazdă "
"precedând I<nume-gazdă> de un semn minus (-). Utilizatorii de la acea gazdă "
"trebuie să furnizeze întotdeauna credențiale suplimentare, inclusiv, "
"eventual, o parolă.  Din motive de securitate, ar trebui să utilizați "
"întotdeauna numele complet „FQDN” al numelui de gazdă și nu numele scurt al "
"gazdei."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The I<username> entry grants a specific user access to all user accounts "
"(except root) without supplying a password.  That means the user is NOT "
"restricted to like-named accounts.  The I<username> may be (optionally) "
"preceded by a plus (+) sign.  You can also explicitly deny access to a "
"specific user by preceding the I<username> with a minus (-) sign.  This says "
"that the user is not trusted no matter what other entries for that host "
"exist."
msgstr ""
"Intrarea I<nume-utilizator> acordă unui anumit utilizator acces la toate "
"conturile de utilizator (cu excepția celui de root) fără a furniza o parolă. "
"Aceasta înseamnă că utilizatorul NU este restricționat la conturi cu nume "
"asemănător. Intrarea I<nume-utilizator> poate fi precedată (opțional) de un "
"semn plus (+). De asemenea, puteți refuza în mod explicit accesul unui "
"anumit utilizator, precedând I<nume-utilizator> cu un semn minus (-). Acest "
"lucru înseamnă că utilizatorul nu este de încredere, indiferent ce alte "
"intrări există pentru acea gazdă."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Netgroups can be specified by preceding the netgroup by an @ sign."
msgstr ""
"Grupurile de rețea pot fi specificate prin precedarea grupului de rețea de "
"un semn @."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Be extremely careful when using the plus (+) sign.  A simple typographical "
"error could result in a standalone plus sign.  A standalone plus sign is a "
"wildcard character that means \"any host\"!"
msgstr ""
"Fiți extrem de atent atunci când utilizați semnul plus (+). O simplă eroare "
"tipografică ar putea avea ca rezultat un semn plus de sine stătător. Un semn "
"plus independent este un caracter Jocker care înseamnă „orice gazdă”!"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "FIȘIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I</etc/hosts.equiv>"
msgstr "I</etc/hosts.equiv>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Some systems will honor the contents of this file only when it has owner "
"root and no write permission for anybody else.  Some exceptionally paranoid "
"systems even require that there be no other hard links to the file."
msgstr ""
"Unele sisteme vor respecta conținutul acestui fișier numai dacă acesta are "
"proprietar root și nimeni altcineva nu are permisiunea de scriere. Unele "
"sisteme deosebit de paranoice cer chiar să nu existe alte legături dure "
"către acest fișier."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Modern systems use the Pluggable Authentication Modules library (PAM).  With "
"PAM a standalone plus sign is considered a wildcard character which means "
"\"any host\" only when the word I<promiscuous> is added to the auth "
"component line in your PAM file for the particular service (e.g., B<rlogin>)."
msgstr ""
"Sistemele moderne utilizează biblioteca Pluggable Authentication Modules "
"(PAM). Cu PAM, un semn plus de sine stătător este considerat un caracter "
"Jocker care înseamnă „orice gazdă” numai atunci când cuvântul I<promiscuous> "
"este adăugat la linia componentei «auth» din fișierul PAM pentru serviciul "
"respectiv (de exemplu, B<rlogin>)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "EXEMPLE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Below are some example I</etc/host.equiv> or I<\\[ti]/.rhosts> files."
msgstr ""
"Mai jos sunt câteva exemple de fișiere I</etc/host.equiv> sau I<\\"
"[ti]/.rhosts>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Allow any user to log in from any host:"
msgstr "Permite oricărui utilizator să se conecteze de la orice gazdă:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "+\n"
msgstr "+\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Allow any user from I<host> with a matching local account to log in:"
msgstr ""
"Permite oricărui utilizator din I<gazdă> cu un cont local care corespunde să "
"se conecteze:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "host\n"
msgstr "gazdă\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Note: the use of I<+host> is never a valid syntax, including attempting to "
"specify that any user from the host is allowed."
msgstr ""
"Notă: utilizarea lui I<+gazdă> nu este niciodată o sintaxă validă, inclusiv "
"încercarea de a specifica faptul că orice utilizator de pe gazdă este permis."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Allow any user from I<host> to log in:"
msgstr "Permite oricărui utilizator din I<gazdă> să se conecteze:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "host +\n"
msgstr "gazdă +\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Note: this is distinct from the previous example since it does not require a "
"matching local account."
msgstr ""
"Notă: acest lucru este diferit de exemplul anterior, deoarece nu necesită un "
"cont local care să corespundă."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Allow I<user> from I<host> to log in as any non-root user:"
msgstr ""
"Permite I<utilizatorului> din I<gazdă> să se conecteze ca orice utilizator "
"non-root:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "host user\n"
msgstr "gazdă utilizator\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Allow all users with matching local accounts from I<host> to log in except "
"for I<baduser>:"
msgstr ""
"Permite tuturor utilizatorilor cu conturi locale care corespund din I<gazdă> "
"să se conecteze, cu excepția lui I<utilizatorMaleficr>:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"host -baduser\n"
"host\n"
msgstr ""
"gazdă -utilizatorMalefic\n"
"gazdă\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Deny all users from I<host>:"
msgstr "Refuză toți utilizatorii din I<gazdă>:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "-host\n"
msgstr "-gazdă\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Note: the use of I<-host\\ -user> is never a valid syntax, including "
"attempting to specify that a particular user from the host is not trusted."
msgstr ""
"Notă: utilizarea lui I<-gazdă\\ -utilizator> nu este niciodată o sintaxă "
"validă, inclusiv încercarea de a specifica faptul că un anumit utilizator de "
"pe gazdă nu este de încredere."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Allow all users with matching local accounts on all hosts in a I<netgroup>:"
msgstr ""
"Permite tuturor utilizatorilor cu conturi locale corespunzătoare să se "
"conecteze la toate gazdele dintr-un I<grup-rețea>:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "+@netgroup\n"
msgstr "+@grup-rețea\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Disallow all users on all hosts in a I<netgroup>:"
msgstr ""
"Interzice toți utilizatorii de pe toate gazdele dintr-un I<grup-rețea>:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "-@netgroup\n"
msgstr "-@grup-rețea\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Allow all users in a I<netgroup> to log in from I<host> as any non-root user:"
msgstr ""
"Permite tuturor utilizatorilor dintr-un I<grup-rețea> să se conecteze de la "
"I<gazdă> ca orice utilizator non-root:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "host +@netgroup\n"
msgstr "gazdă +@grup-rețea\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Allow all users with matching local accounts on all hosts in a I<netgroup> "
"except I<baduser>:"
msgstr ""
"Permite accesul tuturor utilizatorilor cu conturi locale corespunzătoare pe "
"toate gazdele dintr-un I<grup-rețea>, cu excepția lui I<utilizatorMalefic>:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"+@netgroup -baduser\n"
"+@netgroup\n"
msgstr ""
"+@grup-rețea -utilizatorMalefic\n"
"+@grup-rețea\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Note: the deny statements must always precede the allow statements because "
"the file is processed sequentially until the first matching rule is found."
msgstr ""
"Notă: declarațiile „deny” trebuie să preceadă întotdeauna declarațiile "
"„allow”, deoarece fișierul este procesat secvențial până când se găsește "
"prima regulă corespunzătoare."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<rhosts>(5), B<rlogind>(8), B<rshd>(8)"
msgstr "B<rhosts>(5), B<rlogind>(8), B<rshd>(8)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 februarie 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pagini de manual de Linux 6.03"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Pagini de manual de Linux 6.9.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 octombrie 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pagini de manual de Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Pagini de manual Linux (nepublicate)"
