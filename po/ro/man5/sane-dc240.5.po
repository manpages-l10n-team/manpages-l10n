# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.21.0\n"
"POT-Creation-Date: 2025-02-16 06:00+0100\n"
"PO-Revision-Date: 2023-12-17 22:36+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "sane-dc240"
msgstr "sane-dc240"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "11 Jul 2008"
msgstr "11 iulie 2008"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SANE Scanner Access Now Easy"
msgstr "SANE: „Scanner Access Now Easy”"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "sane-dc240 - SANE backend for Kodak DC240 Digital Camera"
msgstr "sane-dc240 - controlor SANE pentru camera digitală Kodak DC240"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<sane-dc240> library implements a SANE (Scanner Access Now Easy) "
"backend that provides access to the Kodak DC240 camera. THIS IS EXTREMELY "
"ALPHA CODE! USE AT YOUR OWN RISK!!"
msgstr ""
"Biblioteca B<sane-dc240> implementează un controlor SANE (Scanner Access Now "
"Easy) care oferă acces la camera Kodak DC240. ACESTA ESTE UN COD EXTREM DE "
"ALFA! UTILIZAȚI PE PROPRIUL RISC!!!"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DEVICE NAMES"
msgstr "NUME DE DISPOZITIVE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The current version of the backend only allows one camera to be connected.  "
"The device name is always \"0\"."
msgstr ""
"Versiunea actuală a controlorului permite conectarea unei singure camere. "
"Numele dispozitivului este întotdeauna \"0\"."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "CONFIGURATION"
msgstr "CONFIGURARE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The contents of the I<dc240.conf> specify the serial port and baud rate to "
"use.  The B<baud> rate specifies the maximum rate to use while downloading "
"pictures.  (The camera is always initialized using 9600 baud, then switches "
"to the higher rate).  On a 450MHz Pentium, I usually have no problems "
"downloading at 115200 baud, though the camera sometimes has to resend "
"packets due to lost characters.  Results are better when the \"interrupt-"
"unmask flag\" is set in the IDE driver (I<hdparm -u1>).  Supported baud "
"rates are: 9600, 19200, 38400, 57600, and 115200."
msgstr ""
"Conținutul fișierului I<dc240.conf> specifică portul serial și viteza de "
"transmisie care trebuie utilizate. Viteza B<baud> specifică viteza maximă "
"care trebuie utilizată în timpul descărcării imaginilor (camera este "
"inițializată întotdeauna folosind 9600 baud, apoi trece la o viteză mai "
"mare). Pe un Pentium de 450 MHz, de obicei nu am probleme cu descărcarea la "
"115200 baud, deși camera trebuie uneori să retrimită pachete din cauza "
"caracterelor pierdute. Rezultatele sunt mai bune atunci când „fanionul "
"interrupt-unmask” este stabilit în controlorul IDE la (I<hdparm -u1>). "
"Vitezele de baud suportate sunt: 9600, 19200, 38400, 57600 și 115200."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<dumpinquiry> line causes some information about the camera to be "
"printed."
msgstr "Linia B<dumpinquiry> determină afișarea unor informații despre cameră."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<cmdrespause> specifies how many usec (1,000,000ths of a second) to wait "
"between writing the command and reading the result. 125000 seems to be the "
"lowest I could go reliably."
msgstr ""
"B<cmdrespause> specifică numărul de 𝜇sec (1.000.000 de sutimi de secundă) "
"care trebuie așteptat între scrierea comenzii și citirea rezultatului. "
"125000 pare a fi cel mai mic nivel la care am putut ajunge în mod fiabil."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<breakpause> specifies how many usec (1,000,000ths of a second) between "
"sending the \"back to default\" break and sending commands."
msgstr ""
"B<breakpause> specifică numărul de usec (1.000.000 de sutimi de secundă) "
"între trimiterea pauzei „back to default” și trimiterea comenzilor."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Empty lines and lines starting with a hash mark (#) are ignored.  A sample "
"configuration file is shown below:"
msgstr ""
"Liniile goale și liniile care încep cu un semn „hash” (#) sunt ignorate. Un "
"exemplu de fișier de configurare este prezentat mai jos:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "port=/dev/ttyS0"
msgstr "port=/dev/ttyS0"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "# this is a comment"
msgstr "# acesta este un comentariu"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "baud=115200"
msgstr "baud=115200"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "dumpinquiry"
msgstr "„dumpinquiry”"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "cmdrespause=125000"
msgstr "cmdrespause=125000"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "breakpause=1000000"
msgstr "breakpause=1000000"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "FIȘIERE"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</etc/sane.d/dc240.conf>"
msgstr "I</etc/sane.d/dc240.conf>"

# R-GC, scrie:
# pentru motivul traducerii:
# backend = controlor, a se vedea
# pagina:
# <https://en.wikipedia.org/wiki/
# Scanner_Access_Now_Easy>
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The backend configuration file (see also description of B<SANE_CONFIG_DIR> "
"below)."
msgstr ""
"Fișierul de configurare al controlorului (a se vedea, de asemenea, "
"descrierea B<SANE_CONFIG_DIR> de mai jos)."

#. type: TP
#: archlinux
#, no-wrap
msgid "I</usr/lib/sane/libsane-dc240.a>"
msgstr "I</usr/lib/sane/libsane-dc240.a>"

# R-GC, scrie:
# pentru motivul traducerii:
# backend = controlor, a se vedea
# pagina:
# <https://en.wikipedia.org/wiki/
# Scanner_Access_Now_Easy>
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The static library implementing this backend."
msgstr "Biblioteca statică care implementează acest controlor."

#. type: TP
#: archlinux
#, no-wrap
msgid "I</usr/lib/sane/libsane-dc240.so>"
msgstr "I</usr/lib/sane/libsane-dc240.so>"

# R-GC, scrie:
# pentru motivul traducerii:
# backend = controlor, a se vedea
# pagina:
# <https://en.wikipedia.org/wiki/
# Scanner_Access_Now_Easy>
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The shared library implementing this backend (present on systems that "
"support dynamic loading)."
msgstr ""
"Biblioteca partajată care implementează acest controlor (prezentă pe "
"sistemele care acceptă încărcare dinamică)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ENVIRONMENT"
msgstr "MEDIU"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<SANE_CONFIG_DIR>"
msgstr "B<SANE_CONFIG_DIR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This environment variable specifies the list of directories that may contain "
"the configuration file.  On *NIX systems, the directories are separated by a "
"colon (`:'), under OS/2, they are separated by a semi-colon (`;').  If this "
"variable is not set, the configuration file is searched in two default "
"directories: first, the current working directory (\".\") and then in I</etc/"
"sane.d.> If the value of the environment variable ends with the directory "
"separator character, then the default directories are searched after the "
"explicitly specified directories.  For example, setting B<SANE_CONFIG_DIR> "
"to \"/tmp/config:\" would result in directories I<tmp/config>, I<.>, and I</"
"etc/sane.d> being searched (in this order)."
msgstr ""
"Această variabilă de mediu specifică lista de directoare care pot conține "
"fișierul de configurare. În sistemele *NIX, directoarele sunt separate prin "
"două puncte („:”), în OS/2, ele sunt separate prin punct și virgulă („;”). "
"Dacă această variabilă nu este definită, fișierul de configurare este căutat "
"în două directoare implicite: mai întâi, în directorul de lucru curent („.”) "
"și apoi în I</etc/sane.d>. Dacă valoarea variabilei de mediu se termină cu "
"caracterul de separare a directoarelor, atunci directoarele implicite sunt "
"căutate după directoarele specificate explicit. De exemplu, dacă se "
"definește B<SANE_CONFIG_DIR> la „/tmp/config:”, se vor căuta (în această "
"ordine) directoarele I<tmp/config>, I<.> și I</etc/sane.d>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<SANE_DEBUG_DC240>"
msgstr "B<SANE_DEBUG_DC240>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the library was compiled with debugging support enabled, this environment "
"variable controls the debug level for this backend.  A value of 128 requests "
"maximally copious debug output; smaller levels reduce verbosity."
msgstr ""
"Dacă biblioteca a fost compilată cu suportul de depanare activat, această "
"variabilă de mediu controlează nivelul de depanare pentru acest controlor. O "
"valoare de 128 solicită o ieșire de depanare extrem de abundentă; nivelurile "
"mai mici reduc volumul de informații.."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<sane>(7)"
msgstr "B<sane>(7)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Peter S. Fales"
msgstr "Peter S. Fales"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This backend borrows heavily from the B<sane-dc210>(5)  backend by Brian J. "
"Murrell which is based somewhat on the B<sane-dc25>(5)  backend by Peter "
"Fales."
msgstr ""
"Acest controlor împrumută mult din controlorul B<sane-dc210>(5) al lui Brian "
"J. Murrell, care se bazează oarecum pe controlorul B<sane-dc25>(5) al lui "
"Peter Fales."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The manpage was largely copied from the B<sane-dc210>(5)  manpage."
msgstr ""
"Pagina de manual a fost copiată în mare parte din pagina de manual B<sane-"
"dc210>(5)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ERORI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The major limitation that I know of is that the backend assumes the "
"directory in the camera is 100dc240.  Once the camera has taken more than "
"9999 pictures, the directory will increment to 101dc240.  Not only should we "
"check for the additional directory, but pictures may actually be found in "
"multiple directories."
msgstr ""
"Limitarea majoră pe care o cunosc este dată de faptul că controlorul "
"presupune că directorul din cameră este 100dc240. Odată ce camera a făcut "
"mai mult de 9999 fotografii, directorul va crește la 101dc240. Nu numai că "
"ar trebui să verificăm dacă există un director suplimentar, dar imaginile "
"pot fi găsite în mai multe directoare."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"More general comments, suggestions, and inquiries about frontends or SANE "
"should go to the SANE Developers mailing list (see I<http://www.sane-"
"project.org/mailing-lists.html> for details).  You must be subscribed to the "
"list, otherwise your mail won't be sent to the subscribers."
msgstr ""
"Comentariile, sugestiile și întrebările mai generale despre interfețe sau "
"SANE ar trebui să se adreseze listei de discuții SANE Developers (a se vedea "
"I<http://www.sane-project.org/mailing-lists.html> pentru detalii). Trebuie "
"să fiți abonat la listă, în caz contrar mesajul dvs. nu va fi trimis "
"abonaților."

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "I</usr/lib/x86_64-linux-gnu/sane/libsane-dc240.a>"
msgstr "I</usr/lib/x86_64-linux-gnu/sane/libsane-dc240.a>"

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "I</usr/lib/x86_64-linux-gnu/sane/libsane-dc240.so>"
msgstr "I</usr/lib/x86_64-linux-gnu/sane/libsane-dc240.so>"

#. type: TP
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "I</usr/lib64/sane/libsane-dc240.a>"
msgstr "I</usr/lib64/sane/libsane-dc240.a>"

#. type: TP
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "I</usr/lib64/sane/libsane-dc240.so>"
msgstr "I</usr/lib64/sane/libsane-dc240.so>"
