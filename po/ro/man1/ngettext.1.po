# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.18.1\n"
"POT-Creation-Date: 2025-02-28 16:41+0100\n"
"PO-Revision-Date: 2023-06-29 09:01+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NGETTEXT"
msgstr "NGETTEXT"

#. type: TH
#: archlinux
#, no-wrap
msgid "February 2025"
msgstr "februarie 2025"

#. type: TH
#: archlinux
#, no-wrap
msgid "GNU gettext-runtime 0.24"
msgstr "GNU gettext-runtime 0.24"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Comenzi utilizator"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "ngettext - translate message and choose plural form"
msgstr "ngettext - traduce mesajul și alege forma pluralului"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<ngettext> [I<\\,OPTION\\/>] [I<\\,TEXTDOMAIN\\/>] I<\\,MSGID MSGID-PLURAL "
"COUNT\\/>"
msgstr ""
"B<ngettext> [I<\\,OPȚIUNE\\/>] [I<\\,DOMENIU-TEXT\\/>] I<\\,MSGID MSGID-"
"PLURAL CONTOR\\/>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#.  Add any additional description here
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<ngettext> program translates a natural language message into the "
"user's language, by looking up the translation in a message catalog, and "
"chooses the appropriate plural form, which depends on the number I<COUNT> "
"and the language of the message catalog where the translation was found."
msgstr ""
"Programul B<ngettext> traduce un mesaj din limba originală în limba "
"utilizatorului, căutând traducerea într-un catalog de mesaje, și alege forma "
"de plural corespunzătoare, care depinde de numărul I<CONTOR> și de limba din "
"catalogul de mesaje în care a fost găsită traducerea."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Display native language translation of a textual message whose grammatical "
"form depends on a number."
msgstr ""
"Afișează traducerea în limba maternă a unui mesaj textual a cărui formă "
"gramaticală depinde de un număr."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-d>, B<--domain>=I<\\,TEXTDOMAIN\\/>"
msgstr "B<-d>, B<--domain>=I<\\,DOMENIU_TEXT\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "retrieve translated message from TEXTDOMAIN"
msgstr "preia mesajul tradus din DOMENIU_TEXT"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-c>, B<--context>=I<\\,CONTEXT\\/>"
msgstr "B<-c>, B<--context>=I<\\,CONTEXT\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "specify context for MSGID"
msgstr "specifică contextul pentru MSGID"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-e>"
msgstr "B<-e>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "enable expansion of some escape sequences"
msgstr "activează expansiunea unor caractere de eludare"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-E>"
msgstr "B<-E>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "(ignored for compatibility)"
msgstr "(ignorată pentru compatibilitate)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "[TEXTDOMAIN]"
msgstr "[DOMENIU_TEXT]"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MSGID MSGID-PLURAL"
msgstr "MSGID MSGID-PLURAL"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "translate MSGID (singular) / MSGID-PLURAL (plural)"
msgstr "traduce MSGID (singular) / MSGID-PLURAL (plural)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "COUNT"
msgstr "CONTOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "choose singular/plural form based on this value"
msgstr "alege forma de singular/plural pe baza acestei valori"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Informative output:"
msgstr "Ieșire informativă:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "afișează acest mesaj de ajutor și iese"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "display version information and exit"
msgstr "afișează informațiile despre versiune și iese"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the TEXTDOMAIN parameter is not given, the domain is determined from the "
"environment variable TEXTDOMAIN.  If the message catalog is not found in the "
"regular directory, another location can be specified with the environment "
"variable TEXTDOMAINDIR.  Standard search directory: /usr/share/locale"
msgstr ""
"Dacă parametrul DOMENIU-TEXT nu este furnizat, domeniul este determinat de "
"variabila de mediu TEXTDOMAIN.  În cazul în care catalogul de mesaje nu se "
"găsește în directorul obișnuit, se poate specifica o altă locație cu "
"ajutorul variabilei de mediu TEXTDOMAINDIR.  Directorul de căutare standard "
"este: /usr/share/locale"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Written by Ulrich Drepper."
msgstr "Scris de Ulrich Drepper."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPORTAREA ERORILOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Report bugs in the bug tracker at E<lt>https://savannah.gnu.org/projects/"
"gettextE<gt> or by email to E<lt>bug-gettext@gnu.orgE<gt>."
msgstr ""
"Raportați erorile în sistemul de urmărire a erorilor la E<lt>https://"
"savannah.gnu.org/projects/gettextE<gt> sau prin e-mail la E<lt>bug-"
"gettext@gnu.orgE<gt>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "DREPTURI DE AUTOR"

#. type: Plain text
#: archlinux
msgid ""
"Copyright \\(co 1995-2025 Free Software Foundation, Inc.  License GPLv3+: "
"GNU GPL version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>"
msgstr ""
"Drepturi de autor \\(co 1995-2025 Free Software Foundation, Inc. Licența "
"GPLv3+: GNU GPL versiunea 3 sau ulterioară E<lt>https://gnu.org/licenses/"
"gpl.htmlE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Acesta este software liber: sunteți liber să-l modificați și să-l "
"redistribuiți. Nu există NICIO GARANȚIE, în limitele prevăzute de lege."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The full documentation for B<ngettext> is maintained as a Texinfo manual.  "
"If the B<info> and B<ngettext> programs are properly installed at your site, "
"the command"
msgstr ""
"Documentația completă pentru B<ngettext> este menținută ca un manual "
"Texinfo. Dacă programele B<info> și B<ngettext> sunt instalate corect pe "
"sistemul dumneavoastră, comanda"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<info ngettext>"
msgstr "B<info ngettext>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "should give you access to the complete manual."
msgstr "ar trebui să vă permită accesul la manualul complet."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "February 2023"
msgstr "februarie 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "GNU gettext-runtime 0.21"
msgstr "GNU gettext-runtime 0.21"

#. type: Plain text
#: debian-bookworm
msgid ""
"Copyright \\(co 1995-1997, 2000-2020 Free Software Foundation, Inc.  License "
"GPLv3+: GNU GPL version 3 or later E<lt>https://gnu.org/licenses/"
"gpl.htmlE<gt>"
msgstr ""
"Drepturi de autor \\(co 1995-1997, 2000-2020 Free Software Foundation, Inc.  "
"Licența GPLv3+: GNU GPL versiunea 3 sau ulterioară E<lt>https://gnu.org/"
"licenses/gpl.htmlE<gt>"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "December 2024"
msgstr "decembrie 2024"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "GNU gettext-runtime 0.23.1"
msgstr "GNU gettext-runtime 0.23.1"

#. type: Plain text
#: debian-unstable fedora-42 fedora-rawhide
msgid ""
"Copyright \\(co 1995-2024 Free Software Foundation, Inc.  License GPLv3+: "
"GNU GPL version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>"
msgstr ""
"Drepturi de autor \\(co 1995-2024 Free Software Foundation, Inc. Licența "
"GPLv3+: GNU GPL versiunea 3 sau ulterioară E<lt>https://gnu.org/licenses/"
"gpl.htmlE<gt>"

#. type: TH
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "February 2024"
msgstr "februarie 2024"

#. type: TH
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "GNU gettext-runtime 0.22.5"
msgstr "GNU gettext-runtime 0.22.5"

#. type: Plain text
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Copyright \\(co 1995-1997, 2000-2023 Free Software Foundation, Inc.  License "
"GPLv3+: GNU GPL version 3 or later E<lt>https://gnu.org/licenses/"
"gpl.htmlE<gt>"
msgstr ""
"Drepturi de autor \\(co 1995-1997, 2000-2023 Free Software Foundation, Inc.  "
"Licența GPLv3+: GNU GPL versiunea 3 sau ulterioară E<lt>https://gnu.org/"
"licenses/gpl.htmlE<gt>"

#. type: TH
#: opensuse-leap-16-0
#, no-wrap
msgid "October 2022"
msgstr "octombrie 2022"

#. type: TH
#: opensuse-leap-16-0
#, no-wrap
msgid "GNU gettext-runtime 0.21.1"
msgstr "GNU gettext-runtime 0.21.1"

#. type: Plain text
#: opensuse-leap-16-0
msgid ""
"Copyright \\(co 1995-1997, 2000-2022 Free Software Foundation, Inc.  License "
"GPLv3+: GNU GPL version 3 or later E<lt>https://gnu.org/licenses/"
"gpl.htmlE<gt>"
msgstr ""
"Drepturi de autor \\(co 1995-1997, 2000-2022 Free Software Foundation, Inc.  "
"Licența GPLv3+: GNU GPL versiunea 3 sau ulterioară E<lt>https://gnu.org/"
"licenses/gpl.htmlE<gt>"
