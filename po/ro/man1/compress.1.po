# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.21.0\n"
"POT-Creation-Date: 2025-02-16 05:42+0100\n"
"PO-Revision-Date: 2024-06-02 11:05+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.4.3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "COMPRESS"
msgstr "COMPRESS"

#. type: TH
#: archlinux debian-bookworm fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "local"
msgstr "local"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "compress, uncompress, zcat - compress and expand data"
msgstr "compress, uncompress, zcat - comprimă și extinde datele"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<compress> [ B<-f> ] [ B<-k> ] [ B<-v> ] [ B<-c> ] [ B<-V> ] [ B<-r> ] [ B<-"
"b> I<bits> ] [ B<--> ] [ I<name \\&...> ]"
msgstr ""
"B<compress> [ B<-f> ] [ B<-k> ] [ B<-v> ] [ B<-c> ] [ B<-V> ] [ B<-r> ] [ B<-"
"b> I<biți> ] [ B<--> ] [ I<nume \\&...> ]"

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"B<uncompress> [ B<-f> ] [ B<-k> ] [ B<-v> ] [ B<-c> ] [ B<-V> ] [ B<--> ] "
"[ I<name \\&...> ]"
msgstr ""
"B<uncompress> [ B<-f> ] [ B<-k> ] [ B<-v> ] [ B<-c> ] [ B<-V> ] [ B<--> ] "
"[ I<nume \\&...> ]"

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "B<zcat> [ B<-V> ] [ B<--> ] [ I<name \\&...> ]"
msgstr "B<zcat> [ B<-V> ] [ B<--> ] [ I<nume \\&...> ]"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"I<Compress> reduces the size of the named files using adaptive Lempel-Ziv "
"coding.  Whenever possible, each file is replaced by one with the extension "
"B<\\&.Z,> while keeping the same ownership modes, access and modification "
"times.  If no files are specified, the standard input is compressed to the "
"standard output.  I<Compress> will only attempt to compress regular files.  "
"In particular, it will ignore symbolic links. If a file has multiple hard "
"links, I<compress> will refuse to compress it unless the B<-f> flag is given."
msgstr ""
"I<compress> reduce dimensiunea fișierelor numite utilizând codificarea "
"adaptivă Lempel-Ziv. Ori de câte ori este posibil, fiecare fișier este "
"înlocuit cu unul cu extensia B<\\&.Z,>, păstrând în același timp aceleași "
"moduri de proprietate, timpii de acces și de modificare. În cazul în care nu "
"se specifică niciun fișier, intrarea standard este comprimată la ieșirea "
"standard. I<compress> va încerca să comprime numai fișierele obișnuite. În "
"special, va ignora legăturile simbolice. În cazul în care un fișier are mai "
"multe legături dure, I<compress> va refuza să îl comprime, cu excepția "
"cazului în care se indică opțiunea B<-f>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"If B<-f> is not given and I<compress> is run in the foreground, the user is "
"prompted as to whether an existing file should be overwritten."
msgstr ""
"Dacă B<-f> nu este dată și I<compress> este rulat în prim-plan, utilizatorul "
"este întrebat dacă un fișier existent trebuie să fie suprascris."

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"Compressed files can be restored to their original form using I<uncompress> "
"or I<zcat.>"
msgstr ""
"Fișierele comprimate pot fi readuse la forma lor originală folosind "
"I<uncompress> sau I<zcat>."

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"I<uncompress> takes a list of files on its command line and replaces each "
"file whose name ends with B<\\&.Z> and which begins with the correct magic "
"number with an uncompressed file without the B<\\&.Z.> The uncompressed file "
"will have the mode, ownership and timestamps of the compressed file."
msgstr ""
"I<uncompress> primește o listă de fișiere în linia de comandă și înlocuiește "
"fiecare fișier al cărui nume se termină cu B<\\&.Z> și care începe cu "
"numărul magic corect cu un fișier necomprimat fără B<\\&.Z>. Fișierul "
"necomprimat va avea modul, proprietatea și marcajele de timp ale fișierului "
"comprimat."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"The B<-k> option makes I<compress/uncompress> keep the input files instead "
"of automatically removing them."
msgstr ""
"Opțiunea B<-k> face ca I<compress/uncompress> să păstreze fișierele de "
"intrare în loc să le elimine automat."

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"The B<-c> option makes I<compress/uncompress> write to the standard output; "
"no files are changed."
msgstr ""
"Opțiunea B<-c> face ca I<compress/uncompress> să scrie la ieșirea standard; "
"niciun fișier nu este modificat."

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"I<zcat> is identical to I<uncompress> B<-c.> I<zcat> uncompresses either a "
"list of files on the command line or its standard input and writes the "
"uncompressed data on standard output.  I<zcat> will uncompress files that "
"have the correct magic number whether they have a B<\\&.Z> suffix or not."
msgstr ""
"I<zcat> este identic cu I<uncompress> B<-c>. I<zcat> decomprimă fie o listă "
"de fișiere din linia de comandă, fie de la intrarea sa standard și scrie "
"datele decomprimate la ieșirea standard. I<zcat> va decomprima fișierele "
"care au numărul magic corect, indiferent dacă au sau nu sufixul B<\\&.Z>."

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"If the B<-r> flag is specified, I<compress> will operate recursively. If any "
"of the file names specified on the command line are directories, I<compress> "
"will descend into the directory and compress all the files it finds there.  "
"When compressing, any files already compressed will be ignored, and when "
"decompressing, any files already decompressed will be ignored."
msgstr ""
"Dacă se specifică opțiunea B<-r>, I<compress> va funcționa recursiv. Dacă "
"oricare dintre numele de fișiere specificate în linia de comandă sunt "
"directoare, I<compress> va coborî în directoare și va comprima toate "
"fișierele pe care le găsește acolo. La comprimare, orice fișier deja "
"comprimat va fi ignorat, iar la decomprimare, orice fișier deja decomprimat "
"va fi ignorat."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The B<-V> flag tells each of these programs to print its version and "
"patchlevel, along with any preprocessor flags specified during compilation, "
"on stderr before doing any compression or uncompression."
msgstr ""
"Opțiunea B<-V> îi indică fiecăruia dintre aceste programe să afișeze la "
"ieșirea de eroare standard versiunea și nivelul de patch, împreună cu orice "
"fanion de preprocesor specificat în timpul compilării, înainte de a efectua "
"orice comprimare sau decomprimare."

#. type: Plain text
#: archlinux debian-bookworm fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"I<Compress> uses the modified Lempel-Ziv algorithm popularized in \"A "
"Technique for High Performance Data Compression\", Terry A. Welch, I<IEEE "
"Computer,> vol. 17, no. 6 (June 1984), pp. 8-19.  Common substrings in the "
"file are first replaced by 9-bit codes 257 and up.  When code 512 is "
"reached, the algorithm switches to 10-bit codes and continues to use more "
"bits until the limit specified by the B<-b> flag is reached (default 16).  "
"I<Bits> must be between 9 and 16.  The default can be changed in the source "
"to allow I<compress> to be run on a smaller machine."
msgstr ""
"I<compress> utilizează algoritmul Lempel-Ziv modificat, popularizat în „A "
"Technique for High Performance Data Compression”, Terry A. Welch, I<IEEE "
"Computer>, vol. 17, nr. 6 (iunie 1984), pag. 8-19. Subșirurile comune din "
"fișier sunt mai întâi înlocuite cu codurile 257 și mai sus pe 9 biți. Când "
"se ajunge la codul 512, algoritmul trece la coduri pe 10 biți și continuă să "
"utilizeze mai mulți biți până când se atinge limita specificată de opțiunea "
"B<-b> (implicit 16). I<biți> trebuie să fie între 9 și 16. Valoarea "
"implicită poate fi modificată în sursă pentru a permite ca I<compress> să "
"fie rulat pe o mașină mai mică."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"After the I<bits> limit is attained, I<compress> periodically checks the "
"compression ratio.  If it is increasing, I<compress> continues to use the "
"existing code dictionary.  However, if the compression ratio decreases, "
"I<compress> discards the table of substrings and rebuilds it from scratch.  "
"This allows the algorithm to adapt to the next \"block\" of the file."
msgstr ""
"După ce limita de I<biți> biți este atinsă, I<compress> verifică periodic "
"raportul de comprimare. Dacă acesta crește, I<compress> continuă să "
"utilizeze dicționarul de cod existent. Cu toate acestea, dacă rata de "
"comprimare scade, I<compress> renunță la tabelul de subșiruri și îl "
"reconstruiește de la zero. Acest lucru permite algoritmului să se adapteze "
"la următorul „bloc” al fișierului."

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"Note that the B<-b> flag is omitted for I<uncompress,> since the I<bits> "
"parameter specified during compression is encoded within the output, along "
"with a magic number to ensure that neither decompression of random data nor "
"recompression of compressed data is attempted."
msgstr ""
"Rețineți că opțiunea B<-b> este omisă pentru I<uncompress>, deoarece "
"parametrul de I<biți> specificat în timpul comprimării este codificat în "
"ieșire, împreună cu un număr magic pentru a se asigura că nu se încearcă "
"nici decomprimarea datelor aleatorii, nici recomprimarea datelor comprimate."

#. type: Plain text
#: archlinux debian-bookworm fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"The amount of compression obtained depends on the size of the input, the "
"number of I<bits> per code, and the distribution of common substrings.  "
"Typically, text such as source code or English is reduced by 50-60%.  "
"Compression is generally much better than that achieved by Huffman coding "
"(as used in I<pack>), or adaptive Huffman coding (I<compact>), and takes "
"less time to compute."
msgstr ""
"Gradul de comprimare obținut depinde de dimensiunea datelor de intrare, de "
"numărul de I<biți> pe cod și de distribuția subșirurilor comune. În mod "
"obișnuit, un text, cum ar fi codul sursă sau limba engleză, este redus cu "
"50-60%. Comprimarea este, în general, mult mai bună decât cea obținută prin "
"codificarea Huffman (așa cum se utilizează în I<pachet>) sau prin "
"codificarea Huffman adaptivă (I<compact>) și durează mai puțin timp pentru "
"calcul."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Under the B<-v> option, a message is printed yielding the percentage of "
"reduction for each file compressed."
msgstr ""
"Cu opțiunea B<-v>, se afișează un mesaj care indică procentul de reducere "
"pentru fiecare fișier comprimat."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<--> may be used to halt option parsing and force all remaining arguments "
"to be treated as paths."
msgstr ""
"B<--> poate fi utilizat pentru a opri analiza opțiunilor și pentru a forța "
"toate argumentele rămase să fie tratate ca fiind rute."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "DIAGNOSTICS"
msgstr "DIAGNOSTICARE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Exit status is normally 0; if the last file is larger after (attempted) "
"compression, the status is 2; if an error occurs, exit status is 1."
msgstr ""
"Starea de ieșire este în mod normal 0; dacă ultimul fișier este mai mare "
"după (încercarea de) comprimare, starea este 2; dacă apare o eroare, starea "
"de ieșire este 1."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Usage: compress [-dfvcVr] [-b maxbits] [file ...]"
msgstr "Utilizare: compress [-dfvcVr] [-b nr-max-biți] [fișier ...]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Invalid options were specified on the command line."
msgstr "Au fost specificate opțiuni nevalide în linia de comandă."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Missing maxbits"
msgstr "Lipsește parametrul I<nr-max-biți>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Maxbits must follow B<-b>."
msgstr "I<nr-max-biți> trebuie să fie după opțiunea B<-b>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "I<file>: not in compressed format"
msgstr "I<fișier>: nu este în format comprimat"

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "The file specified to I<uncompress> has not been compressed."
msgstr "Fișierul specificat la I<uncompress> nu era comprimat."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "I<file>: compressed with I<xx> bits, can only handle I<yy> bits"
msgstr "I<fișier>: comprimat cu I<xx> biți, poate gestiona doar I<yy> biți"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"I<File> was compressed by a program that could deal with more I<bits> than "
"the compress code on this machine.  Recompress the file with smaller I<bits>."
msgstr ""
"I<fișier> a fost comprimat de un program care putea gestiona mai mulți "
"I<biți> decât codul de comprimare de pe această mașină. Recomprimați "
"fișierul cu un număr mai mic de I<biți>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "I<file>: already has .Z suffix -- no change"
msgstr "I<fișier>: are deja sufixul .Z -- nicio schimbare"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The file is assumed to be already compressed.  Rename the file and try again."
msgstr ""
"Se presupune că fișierul este deja comprimat. Redenumiți fișierul și "
"încercați din nou."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "I<file>: filename too long to tack on .Z"
msgstr "I<fișier>: nume de fișier prea lung pentru a fi adăugat .Z"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The file cannot be compressed because its name is longer than 12 "
"characters.  Rename and try again.  This message does not occur on BSD "
"systems."
msgstr ""
"Fișierul nu poate fi comprimat deoarece numele său are mai mult de 12 "
"caractere. Redenumiți-l și încercați din nou. Acest mesaj nu apare pe "
"sistemele BSD."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "I<file> already exists; do you wish to overwrite (y or n)?"
msgstr ""
"I<fișier> already exists; do you wish to overwrite (y or n)? (există deja; "
"doriți să îl suprascrieți (y sau n)?)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Respond \"y\" if you want the output file to be replaced; \"n\" if not."
msgstr ""
"Răspundeți cu „y” dacă doriți ca fișierul de ieșire să fie înlocuit; cu „n” "
"în caz contrar."

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "uncompress: corrupt input"
msgstr "uncompress: intrare coruptă"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"A SIGSEGV violation was detected which usually means that the input file has "
"been corrupted."
msgstr ""
"A fost detectată o încălcare SIGSEGV, ceea ce înseamnă, de obicei, că "
"fișierul de intrare a fost corupt."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Compression: I<xx.xx%>"
msgstr "Comprimare: I<xx.xx%>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Percentage of the input saved by compression.  (Relevant only for B<-v>.)"
msgstr ""
"Procentul de intrare salvat prin comprimare; (relevant numai pentru B<-v>)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "-- not a regular file or directory: ignored"
msgstr ""
"-- not a regular file or directory: ignored (nu este un fișier sau director "
"obișnuit: se ignoră)"

#. type: Plain text
#: archlinux debian-bookworm fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"When the input file is not a regular file or directory, (e.g. a symbolic "
"link, socket, FIFO, device file), it is left unaltered."
msgstr ""
"În cazul în care fișierul de intrare nu este un fișier sau un director "
"obișnuit (de exemplu, o legătură simbolică, un soclu, un FIFO, un fișier de "
"dispozitiv), acesta rămâne nealterat."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "-- has I<xx> other links: unchanged"
msgstr ""
"--- has I<xx> other links: unchanged (are I<xx> alte legături: neschimbat)"

#. type: Plain text
#: archlinux debian-bookworm fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"The input file has links; it is left unchanged.  See I<ln>(1)  for more "
"information. Use the B<-f> flag to force compression of multiply-linked "
"files."
msgstr ""
"Fișierul de intrare are legături; acesta este lăsat neschimbat. A se vedea "
"I<ln>(1) pentru mai multe informații. Utilizați opțiunea B<-f> pentru a "
"forța comprimarea fișierelor cu legături multiple."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "-- file unchanged"
msgstr "-- fișier nemodificat"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "No savings is achieved by compression.  The input remains virgin."
msgstr ""
"Prin comprimare nu se realizează nicio economie. Intrarea rămâne virgină."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ERORI"

#. type: Plain text
#: archlinux debian-bookworm fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Although compressed files are compatible between machines with large memory, "
"B<-b>\\12 should be used for file transfer to architectures with a small "
"process data space (64KB or less, as exhibited by the DEC PDP series, the "
"Intel 80286, etc.)"
msgstr ""
"Deși fișierele comprimate sunt compatibile între mașinile cu memorie mare, "
"B<-b>\\12 ar trebui să fie utilizat pentru transferul de fișiere către "
"arhitecturi cu un spațiu de procesare a datelor mic (64KB sau mai puțin, cum "
"ar fi seria DEC PDP, Intel 80286, etc.)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<pack>(1), B<compact>(1)"
msgstr "B<pack>(1), B<compact>(1)"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "compress, uncompress.real - compress and expand data"
msgstr "compress, uncompress.real - comprimă și extinde datele"

#. type: Plain text
#: debian-bookworm
msgid ""
"B<compress> [ B<-f> ] [ B<-v> ] [ B<-c> ] [ B<-V> ] [ B<-r> ] [ B<-b> "
"I<bits> ] [ B<--> ] [ I<name \\&...> ]"
msgstr ""
"B<compress> [ B<-f> ] [ B<-v> ] [ B<-c> ] [ B<-V> ] [ B<-r> ] [ B<-b> "
"I<biți> ] [ B<--> ] [ I<nume \\&...> ]"

#. type: Plain text
#: debian-bookworm
msgid ""
"B<uncompress.real> [ B<-f> ] [ B<-v> ] [ B<-c> ] [ B<-V> ] [ B<--> ] "
"[ I<name \\&...> ]"
msgstr ""
"B<uncompress.real> [ B<-f> ] [ B<-v> ] [ B<-c> ] [ B<-V> ] [ B<--> ] "
"[ I<nume \\&...> ]"

#. type: Plain text
#: debian-bookworm
msgid ""
"Note that the program that would normally be installed as I<uncompress> is "
"installed for Debian as I<uncompress.real.> This has been done to avoid "
"conflicting with the more-commonly-used program with the same name that is "
"part of the gzip package."
msgstr ""
"Rețineți că programul care în mod normal ar fi instalat ca I<uncompress> "
"este instalat pentru Debian ca I<uncompress.real.> Acest lucru a fost făcut "
"pentru a evita conflictul cu programul cu același nume, mai frecvent "
"utilizat, care face parte din pachetul „gzip”."

#. type: Plain text
#: debian-bookworm
msgid ""
"Compressed files can be restored to their original form using "
"I<uncompress.real.>"
msgstr ""
"Fișierele comprimate pot fi readuse la forma lor originală folosind "
"I<uncompress.real>."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"I<uncompress.real> takes a list of files on its command line and replaces "
"each file whose name ends with B<\\&.Z> and which begins with the correct "
"magic number with an uncompressed file without the B<\\&.Z.> The "
"uncompressed file will have the mode, ownership and timestamps of the "
"compressed file."
msgstr ""
"I<uncompress.real> primește o listă de fișiere în linia de comandă și "
"înlocuiește fiecare fișier al cărui nume se termină cu B<\\&.Z> și care "
"începe cu numărul magic corect cu un fișier necomprimat fără B<\\&.Z.> "
"Fișierul necomprimat va avea modul, proprietatea și marcajele de timp ale "
"fișierului comprimat."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The B<-c> option makes I<compress/uncompress.real> write to the standard "
"output; no files are changed."
msgstr ""
"Opțiunea B<-c> face ca I<compress/uncompress.real> să scrie la ieșirea "
"standard; niciun fișier nu este modificat."

#. type: Plain text
#: debian-bookworm
msgid ""
"If the B<-r> flag is specified, I<compress> will operate recursively. If any "
"of the file names specified on the command line are directories, I<compress> "
"will descend into the directory and compress all the files it finds there."
msgstr ""
"Dacă se specifică opțiunea B<-r>, I<compress> va funcționa recursiv. Dacă "
"oricare dintre numele de fișiere specificate în linia de comandă sunt "
"directoare, I<compress> va coborî în directoare și va comprima toate "
"fișierele pe care le găsește acolo."

#. type: Plain text
#: debian-bookworm
msgid ""
"Note that the B<-b> flag is omitted for I<uncompress.real,> since the "
"I<bits> parameter specified during compression is encoded within the output, "
"along with a magic number to ensure that neither decompression of random "
"data nor recompression of compressed data is attempted."
msgstr ""
"Rețineți că opțiunea B<-b> este omisă pentru I<uncompress.real>, deoarece "
"parametrul de I<biți> specificat în timpul comprimării este codificat în "
"ieșire, împreună cu un număr magic pentru a se asigura că nu se încearcă "
"nici decomprimarea datelor aleatorii, nici recomprimarea datelor comprimate."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "The file specified to I<uncompress.real> has not been compressed."
msgstr "Fișierul specificat la I<uncompress.real> nu era comprimat."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "uncompress.real: corrupt input"
msgstr "uncompress.real: intrare coruptă"

#. type: Plain text
#: debian-bookworm
msgid ""
"Invoking compress with a B<-r> flag will occasionally cause it to produce "
"spurious error warnings of the form"
msgstr ""
"Invocarea «compress» cu opțiunea B<-r> îl va face ocazional să producă "
"avertismente de eroare false de forma"

#. type: Plain text
#: debian-bookworm
msgid "E<lt>filenameE<gt>.Z already has .Z suffix - ignored"
msgstr "E<lt>nume-fișierE<gt>.Z are deja sufixul .Z - se ignoră"

#. type: Plain text
#: debian-bookworm
msgid ""
"These warnings can be ignored. See the comments in compress42.c:compdir()  "
"in the source distribution for an explanation."
msgstr ""
"Aceste avertismente pot fi ignorate. Pentru o explicație, consultați "
"comentariile din compres42.c:compdir() din distribuția sursei."

#. type: TH
#: debian-unstable
#, no-wrap
msgid "2024-05-21"
msgstr "21 mai 2024"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "ncompress"
msgstr "ncompress"

#. type: Plain text
#: debian-unstable
msgid ""
"B<uncompress.real> [ B<-f> ] [ B<-k> ] [ B<-v> ] [ B<-c> ] [ B<-V> ] [ B<--"
"> ] [ I<name \\&...> ]"
msgstr ""
"B<uncompress.real> [ B<-f> ] [ B<-k> ] [ B<-v> ] [ B<-c> ] [ B<-V> ] [ B<--"
"> ] [ I<nume \\&...> ]"

#. type: Plain text
#: debian-unstable
msgid ""
"Note that the program that would normally be installed as I<uncompress> is "
"installed for Debian as I<uncompress.real>.  This has been done to avoid "
"conflicting with the more-commonly-used program with the same name that is "
"part of the gzip package."
msgstr ""
"Rețineți că programul care în mod normal ar fi instalat ca I<uncompress> "
"este instalat pentru Debian ca I<uncompress.real>. Acest lucru a fost făcut "
"pentru a evita conflictul cu programul cu același nume, mai frecvent "
"utilizat, care face parte din pachetul „gzip”."

#. type: Plain text
#: debian-unstable
msgid ""
"I<Compress> reduces the size of the named files using adaptive Lempel-Ziv "
"coding.  Whenever possible, each file is replaced by one with the extension "
"B<\\&.Z,> while keeping the same ownership modes, access and modification "
"times.  If no files are specified, the standard input is compressed to the "
"standard output.  I<Compress> will only attempt to compress regular files.  "
"In particular, it will ignore symbolic links.  If a file has multiple hard "
"links, I<compress> will refuse to compress it unless the B<-f> flag is given."
msgstr ""
"I<compress> reduce dimensiunea fișierelor numite utilizând codificarea "
"adaptivă Lempel-Ziv. Ori de câte ori este posibil, fiecare fișier este "
"înlocuit cu unul cu extensia B<\\&.Z>, păstrând în același timp aceleași "
"moduri de proprietate, timpii de acces și de modificare. În cazul în care nu "
"se specifică niciun fișier, intrarea standard este comprimată la ieșirea "
"standard. I<compress> va încerca să comprime numai fișierele obișnuite. În "
"special, va ignora legăturile simbolice. În cazul în care un fișier are mai "
"multe legături dure, I<compress> va refuza să îl comprime, cu excepția "
"cazului în care se indică opțiunea B<-f>."

#. type: Plain text
#: debian-unstable
msgid ""
"Compressed files can be restored to their original form using "
"I<uncompress.real>."
msgstr ""
"Fișierele comprimate pot fi readuse la forma lor originală folosind "
"I<uncompress.real>."

#. type: Plain text
#: debian-unstable
msgid ""
"If the B<-r> flag is specified, I<compress> will operate recursively.  If "
"any of the file names specified on the command line are directories, "
"I<compress> will descend into the directory and compress all the files it "
"finds there.  When compressing, any files already compressed will be "
"ignored, and when decompressing, any files already decompressed will be "
"ignored."
msgstr ""
"Dacă se specifică opțiunea B<-r>, I<compress> va funcționa recursiv. Dacă "
"oricare dintre numele de fișiere specificate în linia de comandă sunt "
"directoare, I<compress> va coborî în directoare și va comprima toate "
"fișierele pe care le găsește acolo. La comprimare, orice fișier deja "
"comprimat va fi ignorat, iar la decomprimare, orice fișier deja decomprimat "
"va fi ignorat."

#. type: Plain text
#: debian-unstable
msgid ""
"I<Compress> uses the modified Lempel-Ziv algorithm popularized in \"A "
"Technique for High Performance Data Compression\", Terry A.\\& Welch, I<IEEE "
"Computer,> vol.\\& 17, no.\\& 6 (June 1984), pp.\\& 8\\(en19.  Common "
"substrings in the file are first replaced by 9-bit codes 257 and up.  When "
"code 512 is reached, the algorithm switches to 10-bit codes and continues to "
"use more bits until the limit specified by the B<-b> flag is reached "
"(default 16).  I<Bits> must be between 9 and 16.  The default can be changed "
"in the source to allow I<compress> to be run on a smaller machine."
msgstr ""
"I<compress> utilizează algoritmul Lempel-Ziv modificat, popularizat în „A "
"Technique for High Performance Data Compression”, Terry A. Welch, I<IEEE "
"Computer>, vol. 17, nr. 6 (iunie 1984), pag. 8-19. Subșirurile comune din "
"fișier sunt mai întâi înlocuite cu codurile 257 și mai sus pe 9 biți. Când "
"se ajunge la codul 512, algoritmul trece la coduri pe 10 biți și continuă să "
"utilizeze mai mulți biți până când se atinge limita specificată de opțiunea "
"B<-b> (implicit 16). I<biți> trebuie să fie între 9 și 16. Valoarea "
"implicită poate fi modificată în sursă pentru a permite ca I<compress> să "
"fie rulat pe o mașină mai mică."

#. type: Plain text
#: debian-unstable
msgid ""
"Note that the B<-b> flag is omitted for I<uncompress.real>, since the "
"I<bits> parameter specified during compression is encoded within the output, "
"along with a magic number to ensure that neither decompression of random "
"data nor recompression of compressed data is attempted."
msgstr ""
"Rețineți că opțiunea B<-b> este omisă pentru I<uncompress.real>, deoarece "
"parametrul de I<biți> specificat în timpul comprimării este codificat în "
"ieșire, împreună cu un număr magic pentru a se asigura că nu se încearcă "
"nici decomprimarea datelor aleatorii, nici recomprimarea datelor comprimate."

#. type: Plain text
#: debian-unstable
msgid ""
"The amount of compression obtained depends on the size of the input, the "
"number of I<bits> per code, and the distribution of common substrings.  "
"Typically, text such as source code or English is reduced by 50\\(en60%.  "
"Compression is generally much better than that achieved by Huffman coding "
"(as used in I<pack>), or adaptive Huffman coding (I<compact>), and takes "
"less time to compute."
msgstr ""
"Gradul de comprimare obținut depinde de dimensiunea datelor de intrare, de "
"numărul de I<biți> pe cod și de distribuția subșirurilor comune. În mod "
"obișnuit, un text, cum ar fi codul sursă sau limba engleză, este redus cu "
"50-60%. Comprimarea este, în general, mult mai bună decât cea obținută prin "
"codificarea Huffman (așa cum se utilizează în I<pachet>) sau prin "
"codificarea Huffman adaptivă (I<compact>) și durează mai puțin timp pentru "
"calcul."

#. type: Plain text
#: debian-unstable
msgid ""
"When the input file is not a regular file or directory, (e.g.\\& a symbolic "
"link, socket, FIFO, device file), it is left unaltered."
msgstr ""
"În cazul în care fișierul de intrare nu este un fișier sau un director "
"obișnuit (de exemplu, o legătură simbolică, un soclu, un FIFO, un fișier de "
"dispozitiv), acesta rămâne nealterat."

#. type: Plain text
#: debian-unstable
msgid ""
"The input file has links; it is left unchanged.  See I<ln>(1)  for more "
"information.  Use the B<-f> flag to force compression of multiply-linked "
"files."
msgstr ""
"Fișierul de intrare are legături; acesta este lăsat neschimbat. A se vedea "
"I<ln>(1) pentru mai multe informații. Utilizați opțiunea B<-f> pentru a "
"forța comprimarea fișierelor cu legături multiple."

#. type: Plain text
#: debian-unstable
msgid ""
"Although compressed files are compatible between machines with large memory, "
"B<-b>12 should be used for file transfer to architectures with a small "
"process data space (64\\ KiB or less, as exhibited by the DEC PDP series, "
"the Intel 80286, etc.)"
msgstr ""
"Deși fișierele comprimate sunt compatibile între mașinile cu memorie mare, "
"B<-b>12 ar trebui să fie utilizat pentru transferul de fișiere către "
"arhitecturi cu un spațiu de procesare a datelor mic (64KB sau mai puțin, cum "
"ar fi seria DEC PDP, Intel 80286, etc.)."
