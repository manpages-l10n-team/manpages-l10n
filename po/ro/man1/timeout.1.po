# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.18.1\n"
"POT-Creation-Date: 2025-02-28 16:54+0100\n"
"PO-Revision-Date: 2025-02-01 11:44+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.5\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "TIMEOUT"
msgstr "TIMPUL DE AȘTEPTARE (TIMEOUT)"

#. type: TH
#: archlinux fedora-rawhide
#, no-wrap
msgid "February 2025"
msgstr "februarie 2025"

#. type: TH
#: archlinux fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "GNU coreutils 9.6"
msgstr "GNU coreutils 9.6"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Comenzi utilizator"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "timeout - run a command with a time limit"
msgstr "timeout - execută o comandă cu o limită de timp"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<timeout> [I<\\,OPTION\\/>] I<\\,DURATION COMMAND \\/>[I<\\,ARG\\/>]..."
msgstr ""
"B<timeout> [I<\\,OPȚIUNE\\/>] I<\\,DURATA COMANDA \\/>[I<\\,ARG\\/>]..."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<timeout> [I<\\,OPTION\\/>]"
msgstr "B<timeout> [I<\\,OPȚIUNE\\/>]"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Start COMMAND, and kill it if still running after DURATION."
msgstr ""
"Pornește COMANDA și-o omoară dacă încă rulează după DURATA(s, m, h sau d)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Mandatory arguments to long options are mandatory for short options too."
msgstr ""
"Argumentele obligatorii pentru opțiunile lungi sunt de asemenea obligatorii "
"pentru opțiunile scurte."

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide opensuse-tumbleweed
msgid "B<-f>, B<--foreground>"
msgstr "B<-f>, B<--foreground>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "when not running timeout directly from a shell prompt,"
msgstr "când «timeout» nu rulează direct dintr-un prompt shell,"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"allow COMMAND to read from the TTY and get TTY signals; in this mode, "
"children of COMMAND will not be timed out"
msgstr ""
"permite COMENZII să citească din TTY și să primească semnale de la TTY; în "
"acest mod, procesele-copil ale COMENZII nu vor expira"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<-k>, B<--kill-after>=I<\\,DURATION\\/>"
msgstr "B<-k>, B<--kill-after>=I<\\,DURATA\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "also send a KILL signal if COMMAND is still running"
msgstr "trimite, de asemenea, un semnal KILL dacă COMANDA încă rulează"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "this long after the initial signal was sent"
msgstr "mult timp după ce a fost trimis semnalul inițial"

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide opensuse-tumbleweed
msgid "B<-p>, B<--preserve-status>"
msgstr "B<-p>, B<--preserve-status>"

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide opensuse-tumbleweed
msgid "exit with the same status as COMMAND,"
msgstr "iese cu aceeași stare ca a COMENZII,"

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide opensuse-tumbleweed
msgid "even when the command times out"
msgstr "chiar și atunci când comanda depășește timpul limită"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<-s>, B<--signal>=I<\\,SIGNAL\\/>"
msgstr "B<-s>, B<--signal>=I<\\,SEMNAL\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "specify the signal to be sent on timeout;"
msgstr "specifică semnalul care trebuie trimis la expirarea timpului;"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"SIGNAL may be a name like 'HUP' or a number; see 'kill B<-l>' for a list of "
"signals"
msgstr ""
"SEMNAL poate fi un nume precum „HUP” sau un număr; rulați «kill B<-l>» "
"pentru a obține o listă de semnale"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "diagnose to stderr any signal sent upon timeout"
msgstr ""
"diagnostichează la ieșirea de eroare standard orice semnal trimis la "
"expirarea timpului"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "afișează acest mesaj de ajutor și iese"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "afișează informațiile despre versiune și iese"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"DURATION is a floating point number with an optional suffix: \\&'s' for "
"seconds (the default), 'm' for minutes, 'h' for hours or 'd' for days.  A "
"duration of 0 disables the associated timeout."
msgstr ""
"DURATA este un număr în virgulă mobilă cu un sufix opțional: „s” pentru "
"secunde (implicit), „m” pentru minute, „h” pentru ore sau „d” pentru zile.  "
"O valoare de 0 dezactivează durata de timp asociată."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Upon timeout, send the TERM signal to COMMAND, if no other SIGNAL "
"specified.  The TERM signal kills any process that does not block or catch "
"that signal.  It may be necessary to use the KILL signal, since this signal "
"can't be caught."
msgstr ""
"După expirarea timpului, trimite semnalul TERM către COMANDĂ, dacă nu este "
"specificat niciun alt SEMNAL.  Semnalul TERM termină orice proces care nu "
"blochează sau nu captează acel semnal.  Poate fi necesar să folosiți "
"semnalul KILL, deoarece acest semnal nu poate fi captat."

#. type: SS
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Exit status:"
msgstr "Starea de ieșire:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "124"
msgstr "124"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "if COMMAND times out, and B<--preserve-status> is not specified"
msgstr ""
"dacă COMANDA expiră și nu este specificată opțiunea „--preserve-status”"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "125"
msgstr "125"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "if the timeout command itself fails"
msgstr "dacă comanda «timeout» însăși eșuează"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "126"
msgstr "126"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "if COMMAND is found but cannot be invoked"
msgstr "dacă COMANDA este găsită, dar nu poate fi invocată"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "127"
msgstr "127"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "if COMMAND cannot be found"
msgstr "dacă COMANDA nu poate fi găsită"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "137"
msgstr "137"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "if COMMAND (or timeout itself) is sent the KILL (9) signal (128+9)"
msgstr "dacă COMANDA (sau «timeout» însăși) primește semnalul KILL (9) (128+9)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "-"
msgstr "-"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "the exit status of COMMAND otherwise"
msgstr "starea de ieșire a COMENZII în caz contrar"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ERORI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Some platforms don't currently support timeouts beyond the year 2038."
msgstr ""
"Unele platforme nu acceptă în prezent temporizări dincolo de anul 2038."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Written by Padraig Brady."
msgstr "Scris de Padraig Brady."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPORTAREA ERORILOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Ajutor online GNU coreutils: E<lt>https://www.gnu.org/software/coreutils/"
"E<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Raportați orice erori de traducere la: E<lt>https://translationproject.org/"
"team/ro.htmlE<gt>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "DREPTURI DE AUTOR"

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"Copyright \\(co 2025 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Drepturi de autor \\(co 2025 Free Software Foundation, Inc. Licența GPLv3+: "
"GNU GPL versiunea 3 sau ulterioară E<lt>https://gnu.org/licenses/"
"gpl.htmlE<gt>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Acesta este software liber: sunteți liber să-l modificați și să-l "
"redistribuiți. Nu există NICIO GARANȚIE, în limitele prevăzute de lege."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<kill>(1)"
msgstr "B<kill>(1)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Full documentation E<lt>https://www.gnu.org/software/coreutils/timeoutE<gt>"
msgstr ""
"Documentația completă este disponibilă la E<lt>https://www.gnu.org/software/"
"coreutils/timeoutE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "or available locally via: info \\(aq(coreutils) timeout invocation\\(aq"
msgstr ""
"sau local rulând comanda: «info \\(aq(coreutils) timeout invocation\\(aq»"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "September 2022"
msgstr "septembrie 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
msgid "B<--preserve-status>"
msgstr "B<--preserve-status>"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
msgid "exit with the same status as COMMAND, even when the"
msgstr "iese cu aceeași stare ca a COMENZII, chiar și atunci când"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
msgid "command times out"
msgstr "comanda depășește timpul limită"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
msgid "B<--foreground>"
msgstr "B<--foreground>"

#. type: SS
#: debian-bookworm
#, no-wrap
msgid "EXIT status:"
msgstr "Starea de ieșire:"

#. type: Plain text
#: debian-bookworm
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Drepturi de autor © 2022 Free Software Foundation, Inc. Licența GPLv3+: GNU "
"GPL versiunea 3 sau ulterioară E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: debian-unstable
#, no-wrap
msgid "October 2024"
msgstr "octombrie 2024"

#. type: TH
#: debian-unstable mageia-cauldron
#, no-wrap
msgid "GNU coreutils 9.5"
msgstr "GNU coreutils 9.5"

#. type: Plain text
#: debian-unstable mageia-cauldron
msgid ""
"Copyright \\(co 2024 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Drepturi de autor © 2024 Free Software Foundation, Inc. Licența GPLv3+: GNU "
"GPL versiunea 3 sau ulterioară E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: fedora-42 opensuse-tumbleweed
#, no-wrap
msgid "January 2025"
msgstr "ianuarie 2025"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "March 2024"
msgstr "martie 2024"

#. type: TH
#: opensuse-leap-16-0
#, no-wrap
msgid "January 2024"
msgstr "ianuarie 2024"

#. type: TH
#: opensuse-leap-16-0
#, no-wrap
msgid "GNU coreutils 9.4"
msgstr "GNU coreutils 9.4"

#. type: Plain text
#: opensuse-leap-16-0
msgid ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Drepturi de autor \\(co 2023 Free Software Foundation, Inc. Licența GPLv3+: "
"GNU GPL versiunea 3 sau ulterioară E<lt>https://gnu.org/licenses/"
"gpl.htmlE<gt>."
