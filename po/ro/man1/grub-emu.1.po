# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.18.1\n"
"POT-Creation-Date: 2024-08-02 17:18+0200\n"
"PO-Revision-Date: 2023-10-04 17:20+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "GRUB-EMU"
msgstr "GRUB-EMU"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "February 2024"
msgstr "februarie 2024"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "GRUB 2.12-1~bpo12+1"
msgstr "GRUB 2.12-1~bpo12+1"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "User Commands"
msgstr "Comenzi utilizator"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "grub-emu - GRUB emulator"
msgstr "grub-emu - emulator de GRUB"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<grub-emu> [I<\\,OPTION\\/>...]"
msgstr "B<grub-emu> [I<\\,OPȚIUNE\\/>...]"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "GRUB emulator."
msgstr "Emulator de GRUB."

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-d>, B<--directory>=I<\\,DIR\\/>"
msgstr "B<-d>, B<--directory>=I<\\,DIRECTOR\\/>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "use GRUB files in the directory DIR [default=/boot/grub]"
msgstr ""
"utilizează fișierele GRUB din directorul DIRECTOR [implicit=/boot/grub]"

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-H>, B<--hold>[=I<\\,SECS\\/>]"
msgstr "B<-H>, B<--hold>[=I<\\,SECUNDE\\/>]"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "wait until a debugger will attach"
msgstr "așteaptă până când se atașează un depanator"

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<--memdisk>=I<\\,FILE\\/>"
msgstr "B<--memdisk>=I<\\,FIȘIER\\/>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "use FILE as memdisk"
msgstr "utilizează FIȘIERUL ca o imagine de disc de memorie"

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-m>, B<--device-map>=I<\\,FILE\\/>"
msgstr "B<-m>, B<--device-map>=I<\\,FIȘIER\\/>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "use FILE as the device map [default=/boot/grub/device.map]"
msgstr ""
"utilizează FIȘIERUL ca hartă de dispozitive [default=/boot/grub/device.map]"

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-r>, B<--root>=I<\\,DEVICE_NAME\\/>"
msgstr "B<-r>, B<--root>=I<\\,NUME_DISPOZITIV\\/>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "Set root device."
msgstr "Definește dispozitivul rădăcină."

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "print verbose messages."
msgstr "afișează mesaje detaliate."

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-X>, B<--kexec>"
msgstr "B<-X>, B<--kexec>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"use kexec to boot Linux kernels via systemctl (pass twice to enable "
"dangerous fallback to non-systemctl)."
msgstr ""
"utilizează «kexec» pentru a porni nucleele Linux prin «systemctl» (pasați-o "
"de două ori (-X -X) pentru a activa revenirea periculoasă la non-systemctl)."

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "-?, B<--help>"
msgstr "-?, B<--help>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "give this help list"
msgstr "oferă această listă de ajutor"

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "give a short usage message"
msgstr "oferă un mesaj de utilizare scurt"

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "print program version"
msgstr "afișează versiunea programului"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Mandatory or optional arguments to long options are also mandatory or "
"optional for any corresponding short options."
msgstr ""
"Argumentele obligatorii sau opționale pentru opțiunile lungi sunt "
"obligatorii sau opționale și pentru opțiunile corespunzătoare scurte."

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPORTAREA ERORILOR"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr "Raportați erorile la E<lt>bug-grub@gnu.orgE<gt>."

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"If you are trying to install GRUB, then you should use B<grub-install>(8)  "
"rather than this program."
msgstr ""
"Dacă încercați să instalați GRUB, atunci ar trebui să utilizați B<grub-"
"install>(8) în loc de acest program."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The full documentation for B<grub-emu> is maintained as a Texinfo manual.  "
"If the B<info> and B<grub-emu> programs are properly installed at your site, "
"the command"
msgstr ""
"Documentația completă pentru B<grub-emu> este menținută ca un manual "
"Texinfo. Dacă programele B<info> și B<grub-emu> sunt instalate corect în "
"sistemul dvs., comanda"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<info grub-emu>"
msgstr "B<info grub-emu>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "should give you access to the complete manual."
msgstr "ar trebui să vă permită accesul la manualul complet."

#. type: TH
#: debian-unstable
#, no-wrap
msgid "July 2024"
msgstr "iulie 2024"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "GRUB 2.12-5"
msgstr "GRUB 2.12-5"
