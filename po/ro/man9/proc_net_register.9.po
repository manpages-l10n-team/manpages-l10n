# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.23.1\n"
"POT-Creation-Date: 2024-03-23 07:49+0100\n"
"PO-Revision-Date: 2024-07-04 17:42+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.4.3\n"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "proc_net_register"
msgstr "proc_net_register"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "July 1997"
msgstr "iulie 1997"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux 2.0.30"
msgstr "Linux 2.0.30"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "/proc Functions"
msgstr "Funcții /proc"

#. type: SH
#: mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: mageia-cauldron
msgid ""
"proc_net_register, proc_net_unregister - register network entries in the /"
"proc filesystem"
msgstr ""
"proc_net_register, proc_net_unregister - înregistrează intrările de rețea în "
"sistemul de fișiere /proc"

#. type: SH
#: mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: mageia-cauldron
msgid "B<#include E<lt>linux/proc_fs.hE<gt>>"
msgstr "B<#include E<lt>linux/proc_fs.hE<gt>>"

#. type: TP
#: mageia-cauldron
#, no-wrap
msgid "B<int\\ proc_net_register(\\%struct\\ proc_dir_entry\\ *\\ >I<child>B<);>"
msgstr "B<int\\ proc_net_register(\\%struct\\ proc_dir_entry\\ *\\ >I<copil>B<);>"

#. type: TP
#: mageia-cauldron
#, no-wrap
msgid "B<int\\ proc_net_unregister(\\%int\\ >I<inode>B<);>"
msgstr "B<int\\ proc_net_unregister(\\%int\\ >I<nod-i>B<);>"

#. type: SH
#: mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: mageia-cauldron
msgid ""
"These are wrapper functions around B<proc_register> and B<proc_unregister>.  "
"They always use a parent of proc_net."
msgstr ""
"Acestea sunt funcții de învăluire în jurul B<proc_register> și "
"B<proc_unregister>. Acestea utilizează întotdeauna un părinte al proc_net."

#. type: SH
#: mageia-cauldron
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOAREA RETURNATĂ"

#. type: Plain text
#: mageia-cauldron
msgid "As for B<proc_register> and B<proc_unregister>."
msgstr "La fel ca cea returnată de B<proc_register> și B<proc_unregister>."

#. type: SH
#: mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: mageia-cauldron
msgid "B<proc_register>(9), B<proc_unregister>(9)"
msgstr "B<proc_register>(9), B<proc_unregister>(9)"

#. type: SH
#: mageia-cauldron
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: mageia-cauldron
msgid "Keith Owens E<lt>kaos@ocs.com.auE<gt>"
msgstr "Keith Owens E<lt>kaos@ocs.com.auE<gt>"
