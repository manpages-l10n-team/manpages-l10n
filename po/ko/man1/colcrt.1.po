# Korean translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-03-29 09:37+0100\n"
"PO-Revision-Date: 2000-04-29 08:57+0900\n"
"Last-Translator: Unknown <>\n"
"Language-Team: Korean <translation-team-ko@googlegroups.com>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "COLCRT"
msgstr "COLCRT"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr "2022년 5월 11일"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "User Commands"
msgstr "사용자 명령"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NAME"
msgstr "이름"

#. type: Plain text
#: debian-bookworm
msgid "colcrt - filter nroff output for CRT previewing"
msgstr "colcrt - 밑줄 속성을 가진 문자 변환 필터"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SYNOPSIS"
msgstr "요약"

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid "B<echo> [B<-neE>] [I<arg> ...]"
msgid "B<colcrt> [options] [I<file> ...]"
msgstr "B<echo> [B<-neE>] [I<인수> ...]"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "DESCRIPTION"
msgstr "설명"

#. type: Plain text
#: debian-bookworm
msgid ""
"B<colcrt> provides virtual half-line and reverse line feed sequences for "
"terminals without such capability, and on which overstriking is destructive. "
"Half-line characters and underlining (changed to dashing `-\\(aq) are placed "
"on new lines in between the normal output lines."
msgstr ""
"B<colcrt> 필터는 밑줄 속성을 가진 문자를 볼 수 없는 터미날이나, 인쇄할 수 없"
"는 프린터를 위해, 일반 텍스트 문서에서 그 문자 밑에 밑줄문자(\\(aq)를 넣어주"
"는 필터이다."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "OPTIONS"
msgstr "옵션"

#. type: Plain text
#: debian-bookworm
msgid "B<->, B<--no-underlining>"
msgstr "B<->, B<--no-underlining>"

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid ""
#| "Suppress all underlining.  This option is especially useful for "
#| "previewing E<.Em allboxed> tables from E<.Xr tbl 1>."
msgid ""
"Suppress all underlining. This option is especially useful for previewing "
"I<allboxed> tables from B<tbl>(1)."
msgstr ""
"밑줄 속성이 있는 문자열 아랫줄은 모두 밑줄로 표시한다.  이 옵션은 특별하게 "
"E<.Xr tbl 1 > 명령에 의해서 만들어지는 표상자를 미리 보는데 유용하게 사용된"
"다."

#. type: Plain text
#: debian-bookworm
msgid "B<-2>, B<--half-lines>"
msgstr "B<-2>, B<--half-lines>"

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid ""
#| "Causes all half-lines to be printed, effectively double spacing the "
#| "output.  Normally, a minimal space output format is used which will "
#| "suppress empty lines.  The program never suppresses two consecutive empty "
#| "lines, however.  The E<.Fl 2> option is useful for sending output to the "
#| "line printer when the output contains superscripts and subscripts which "
#| "would otherwise be invisible."
msgid ""
"Causes all half-lines to be printed, effectively double spacing the output. "
"Normally, a minimal space output format is used which will suppress empty "
"lines. The program never suppresses two consecutive empty lines, however. "
"The B<-2> option is useful for sending output to the line printer when the "
"output contains superscripts and subscripts which would otherwise be "
"partially invisible."
msgstr ""
"이 옵션을 사용하지 않으면, 밑줄 속성이 있는 문자열이 있는 다음줄에만 밀줄문자"
"(\\-)를 추가하기 위해 줄이 첨가된다.  이것을 인쇄할 경우에 줄 간격이 이상하"
"게 되어 읽기가 불편하다.  이런 것을 대비해서 이 옵션을 사용하면, 밑줄 속성이 "
"없는 문자열에서도 무조건 다음줄은 공백으로 한줄 띄우게 된다."

#. type: Plain text
#: debian-bookworm
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: debian-bookworm
msgid "Display help text and exit."
msgstr "도움말을 보여주고 마친다."

#. type: Plain text
#: debian-bookworm
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: debian-bookworm
msgid "Print version and exit."
msgstr "버전 정보를 보여주고 마친다."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "HISTORY"
msgstr "이력"

#. type: Plain text
#: debian-bookworm
msgid "The B<colcrt> command appeared in 3.0BSD."
msgstr "B<colcrt> 명령은 3.0BSD 에서 처음 사용되었다."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "BUGS"
msgstr "버그"

#. type: Plain text
#: debian-bookworm
msgid ""
"Should fold underlines onto blanks even with the B<-> option so that a true "
"underline character would show."
msgstr ""
"B<-> 옵션을 사용했음에도 불구하고 밑줄 문자가 만들어지지 않을 경우도 있다."

#. type: Plain text
#: debian-bookworm
msgid "Can\\(cqt back up more than 102 lines."
msgstr "102줄 이상 되는 문서에서는 백업이 불가능하다."

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid ""
#| "General overstriking is lost; as a special case E<.Ql \\&|> overstruck "
#| "with E<.Ql \\-> or underline becomes E<.Ql \\&+>."
msgid ""
"General overstriking is lost; as a special case \\(aq|\\(aq overstruck with "
"\\(aq-\\(aq or underline becomes \\(aq+\\(aq."
msgstr ""
"가운데줄 속성(흔히 취소선이라고함,overstriking)을 가진 문자들에 대해서는 변환"
"이 불가능하다.  E<.Ql \\&|> 기능을 이용할 경우에는 E<.Ql \\-> 문자나, E<.Ql "
"\\&+>.  문자로 보여질 수도 있다."

#. type: Plain text
#: debian-bookworm
#, fuzzy
msgid "Lines are trimmed to 132 characters."
msgstr "한글이 있는 문자열은 변환 불가능(팻치가 시급함)"

#. type: Plain text
#: debian-bookworm
#, fuzzy
msgid ""
"Some provision should be made for processing superscripts and subscripts in "
"documents which are already double-spaced."
msgstr ""
"이미 두 공백문자가 있는 문서 안의 슈퍼스크립트나 하위스크립트 처리를 위해서 "
"약간의 준비과정이 필요하다.(?)"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "EXAMPLES"
msgstr "폐제"

#. type: Plain text
#: debian-bookworm
msgid "A typical use of B<colcrt> would be:"
msgstr "B<colcrt> 필터는 전형적으로 다음과 같이 사용된다:"

#. type: Plain text
#: debian-bookworm
msgid "B<tbl exum2.n | nroff -ms | colcrt - | more>"
msgstr "B<tbl exum2.n | nroff -ms | colcrt - | more>"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SEE ALSO"
msgstr "추가 참조"

#. type: Plain text
#: debian-bookworm
msgid "B<col>(1), B<more>(1), B<nroff>(1), B<troff>(1), B<ul>(1)"
msgstr "B<col>(1), B<more>(1), B<nroff>(1), B<troff>(1), B<ul>(1)"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "REPORTING BUGS"
msgstr "버그 보고"

#. type: Plain text
#: debian-bookworm
msgid "For bug reports, use the issue tracker at"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AVAILABILITY"
msgstr "가용성"

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<colcrt> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""
