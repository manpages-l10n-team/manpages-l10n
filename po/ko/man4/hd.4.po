# Korean translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# ASPLINUX <man@asp-linux.co.kr>, 2000.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2025-02-28 16:35+0100\n"
"PO-Revision-Date: 2000-07-29 08:57+0900\n"
"Last-Translator: ASPLINUX <man@asp-linux.co.kr>\n"
"Language-Team: Korean <translation-team-ko@googlegroups.com>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "hd"
msgstr "hd"

#. type: TH
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2024년 5월 2일"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr "Linux man-pages 6.12"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "이름"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "hd - MFM/IDE hard disk devices"
msgstr "hd - MFM/IDE 하드 디스크 장치"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "설명"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<Hd*> are block devices to access MFM/IDE hard disk drives in raw mode.  "
#| "The master drive on the primary IDE controller (major device number 3) is "
#| "B<hda>; the slave drive is B<hdb>.  The master drive of the second "
#| "controller (major device number 22) is B<hdc> and the slave B<hdd>."
msgid ""
"The B<hd*> devices are block devices to access MFM/IDE hard disk drives in "
"raw mode.  The master drive on the primary IDE controller (major device "
"number 3) is B<hda>; the slave drive is B<hdb>.  The master drive of the "
"second controller (major device number 22)  is B<hdc> and the slave is "
"B<hdd>."
msgstr ""
"B<Hd*>는 포멧되지 않은 MFM/IDE하드에 억세스하기 위한 블록 장치이다.  프라이머"
"리 IDE 콘트롤러에 있는 마스터 드라이브는(주 장치 번호 3번)  B<hda>이다;슬레이"
"브는 B<hdb>이다. 세컨드 콘트롤러에 있는 마스터 드라이브는(주 장치 번호 22"
"번)B<hdc>이다.  그리고 슬레이브는 B<hdd>이다."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "General IDE block device names have the form B<hd>I<X>, or B<hd>I<XP>, "
#| "where I<X> is a letter denoting the physical drive, and I<P> is a number "
#| "denoting the partition on that physical drive.  The first form, "
#| "B<hd>I<X>, is used to address the whole drive.  Partition numbers are "
#| "assigned in the order the partitions are discovered, and only nonempty, "
#| "nonextended partitions get a number.  However, partition numbers 1\\[en]4 "
#| "are given to the four partitions described in the MBR (the \"primary\" "
#| "partitions), regardless of whether they are unused or extended.  Thus, "
#| "the first logical partition will be B<hd>I<X>B<5>\\&.  Both DOS-type "
#| "partitioning and BSD-disklabel partitioning are supported.  You can have "
#| "at most 63 partitions on an IDE disk."
msgid ""
"General IDE block device names have the form B<hd>I<X> , or B<hd>I<XP> , "
"where I<X> is a letter denoting the physical drive, and I<P> is a number "
"denoting the partition on that physical drive.  The first form, B<hd>I<X> , "
"is used to address the whole drive.  Partition numbers are assigned in the "
"order the partitions are discovered, and only nonempty, nonextended "
"partitions get a number.  However, partition numbers 1\\[en]4 are given to "
"the four partitions described in the MBR (the \"primary\" partitions), "
"regardless of whether they are unused or extended.  Thus, the first logical "
"partition will be B<hd>I<X>B<5> \\&.  Both DOS-type partitioning and BSD-"
"disklabel partitioning are supported.  You can have at most 63 partitions on "
"an IDE disk."
msgstr ""
"일반적인 IDE 블록 장치이름은 아래와 같은 형태를 가진다.  B<hd>I<X> , 혹은 "
"B<hd>I<XP,>B<이>I<경우> I<X> 는 물리적인 드라이브를 표시하고, I<P> 는 물리적"
"인 드라이브상의 파티션을 표시하는 번호이다.  첫번째 형태인, B<hd>I<X,> 는 전"
"체 드라이브를 표시하는데 쓰인다.  파티션 번호는 파티션이 발견된 순서에 따라 "
"할당되고, 비어이 있지않고, 확장 파티션이 아니어야 번호를 받을 수 있다.  하지"
"만 그것이 쓰이지 않거나 확장된 것임에 상관없이 1\\[en]4까지의 파티션 번호는 "
"MBR(프라이머리 파티션)상의 4개의 파티션에게 할당된다, 그래서 첫번째 논리적 파"
"티션은 B<hd>I<X>B<5> 이 된다.  \\&.  DOS 타입의 파티션과, BSD-disklabel 파티"
"션이 모두 지원된다 거의 63개의 파티션을 만들 수 있다.."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For example, I</dev/hda> refers to all of the first IDE drive in the system; "
"and I</dev/hdb3> refers to the third DOS \"primary\" partition on the second "
"one."
msgstr ""
"예를들어, I</dev/hda> 는 시스템상의 첫번째 IDE 드라이브 전체를 나타낸다; 그리"
"고 I</dev/hdb3> 는 세컨드 드라이브의 세번째 도스 프라이머리 파티션을 뜻한다."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "They are typically created by:"
msgstr "문자 디바이스들은 일반적으로 다음에 의해 만들어진다."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"mknod -m 660 /dev/hda b 3 0\n"
"mknod -m 660 /dev/hda1 b 3 1\n"
"mknod -m 660 /dev/hda2 b 3 2\n"
"\\&...\n"
"mknod -m 660 /dev/hda8 b 3 8\n"
"mknod -m 660 /dev/hdb b 3 64\n"
"mknod -m 660 /dev/hdb1 b 3 65\n"
"mknod -m 660 /dev/hdb2 b 3 66\n"
"\\&...\n"
"mknod -m 660 /dev/hdb8 b 3 72\n"
"chown root:disk /dev/hd*\n"
msgstr ""
"mknod -m 660 /dev/hda b 3 0\n"
"mknod -m 660 /dev/hda1 b 3 1\n"
"mknod -m 660 /dev/hda2 b 3 2\n"
"\\&...\n"
"mknod -m 660 /dev/hda8 b 3 8\n"
"mknod -m 660 /dev/hdb b 3 64\n"
"mknod -m 660 /dev/hdb1 b 3 65\n"
"mknod -m 660 /dev/hdb2 b 3 66\n"
"\\&...\n"
"mknod -m 660 /dev/hdb8 b 3 72\n"
"chown root:disk /dev/hd*\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "파일"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I</dev/hd*>"
msgstr "I</dev/hd*>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "추가 참조"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<chown>(1), B<mknod>(1), B<sd>(4), B<mount>(8)"
msgstr "B<chown>(1), B<mknod>(1), B<sd>(4), B<mount>(8)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "2023년 2월 5일"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Linux man-pages 6.9.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "2023년 10월 31일"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "Linux man-pages 6.03"
msgid "Linux man-pages (unreleased)"
msgstr "Linux man-pages 6.03"
