# Korean translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# ASPLINUX <man@asp-linux.co.kr>, 2000.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2025-02-28 16:33+0100\n"
"PO-Revision-Date: 2000-07-29 08:57+0900\n"
"Last-Translator: ASPLINUX <man@asp-linux.co.kr>\n"
"Language-Team: Korean <translation-team-ko@googlegroups.com>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "filesystems"
msgstr "filesystems"

#. type: TH
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2024년 5월 2일"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr "Linux man-pages 6.12"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "이름"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"filesystems - Linux filesystem types: ext, ext2, ext3, ext4, hpfs, iso9660, "
"JFS, minix, msdos, ncpfs nfs, ntfs, proc, Reiserfs, smb, sysv, umsdos, vfat, "
"XFS, xiafs"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "설명"

#.  commit: 6af9f7bf3c399e0ab1eee048e13572c6d4e15fe9
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"When, as is customary, the B<proc> filesystem is mounted on I</proc>, you "
"can find in the file I</proc/filesystems> which filesystems your kernel "
"currently supports; see B<proc>(5)  for more details.  There is also a "
"legacy B<sysfs>(2)  system call (whose availability is controlled by the "
"B<CONFIG_SYSFS_SYSCALL> kernel build configuration option since Linux 3.15)  "
"that enables enumeration of the currently available filesystem types "
"regardless of I</proc> availability and/or sanity."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "In the file I</proc/filesystems> you can find which filesystems your "
#| "kernel currently supports.  (If you need a currently unsupported one, "
#| "insert the corresponding module or recompile the kernel.)"
msgid ""
"If you need a currently unsupported filesystem, insert the corresponding "
"kernel module or recompile the kernel."
msgstr ""
"I</proc/filesystems> 파일에서 당신의 커널에서 지원하는 파일 시스템을 찾을 수 "
"있다.  (현제 지원되지 않는 것을 원한다면, 대응하는 모듈을 넣거나, 커널을 다"
"시 컴파일 하라)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In order to use a filesystem, you have to I<mount> it; see B<mount>(2)  and "
"B<mount>(8)."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The following list provides a short description of the available or "
"historically available filesystems in the Linux kernel.  See the kernel "
"documentation for a comprehensive description of all options and limitations."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<err>"
msgid "B<erofs>"
msgstr "B<err>"

#.  commit 47e4937a4a7ca4184fd282791dfee76c6799966a moves it out of staging
#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"is the Enhanced Read-Only File System, stable since Linux 5.4.  See "
"B<erofs>(5)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ext>"
msgstr "B<ext>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "is an elaborate extension of the B<minix> filesystem.  It has been "
#| "completely superseded by the second version of the extended filesystem "
#| "(B<ext2>)  and will eventually be removed from the kernel."
msgid ""
"is an elaborate extension of the B<minix> filesystem.  It has been "
"completely superseded by the second version of the extended filesystem "
"(B<ext2>)  and has been removed from the kernel (in Linux 2.1.21)."
msgstr ""
"는 B<minix> 파일 시스템의 정교한 확장이다. 이것은 두번째 버전의 확장 파일 시"
"스템 (B<ext2>)  에 완전히 대체되었고 결국 커널에서 지워졌다."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ext2>"
msgstr "B<ext2>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"is a disk filesystem that was used by Linux for fixed disks as well as "
"removable media.  The second extended filesystem was designed as an "
"extension of the extended filesystem (B<ext>).  See B<ext2>(5)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ext3>"
msgstr "B<ext3>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"is a journaling version of the B<ext2> filesystem.  It is easy to switch "
"back and forth between B<ext2> and B<ext3>.  See B<ext3>(5)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ext4>"
msgstr "B<ext4>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"is a set of upgrades to B<ext3> including substantial performance and "
"reliability enhancements, plus large increases in volume, file, and "
"directory size limits.  See B<ext4>(5)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<hpfs>"
msgstr "B<hpfs>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"is the High Performance Filesystem, used in OS/2.  This filesystem is read-"
"only under Linux due to the lack of available documentation."
msgstr ""
"는 OS/2에서 쓰이는 고성능 파일 시스템이다. 이것은 리눅스상에서는 가능한 문서"
"의 부족 때문에 읽기만 가능하다."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<iso9660>"
msgstr "B<iso9660>"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid "is a CD-ROM filesystem type conforming to the ISO 9660 standard."
msgid "is a CD-ROM filesystem type conforming to the ISO/IEC\\ 9660 standard."
msgstr "는 ISO 9660 표준을 따르는 CD-ROM 파일 시스템 형태이다."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<High Sierra>"
msgstr "B<High Sierra>"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Linux supports High Sierra, the precursor to the ISO 9660 standard for CD-"
#| "ROM filesystems.  It is automatically recognized within the B<iso9660> "
#| "filesystem support under Linux."
msgid ""
"Linux supports High Sierra, the precursor to the ISO/IEC\\ 9660 standard for "
"CD-ROM filesystems.  It is automatically recognized within the B<iso9660> "
"filesystem support under Linux."
msgstr ""
"리눅스는 ISO 9660 표준의 전신인 High Sierra를 지원한다 B<iso9660> 파일 시스템"
"에서 자동으로 인식된다."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<Rock Ridge>"
msgstr "B<Rock Ridge>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Linux also supports the System Use Sharing Protocol records specified by the "
"Rock Ridge Interchange Protocol.  They are used to further describe the "
"files in the B<iso9660> filesystem to a UNIX host, and provide information "
"such as long filenames, UID/GID, POSIX permissions, and devices.  It is "
"automatically recognized within the B<iso9660> filesystem support under "
"Linux."
msgstr ""
"Linux는 Rock Ridge 교환 프로토콜로 분류되는 System Use Sharing Protocol 역시 "
"지원한다. 그것들은 더 나아가서 유닉스 호스트에게 B<iso9660> 파일 시스템상에"
"서 파일을 알리고, 긴 파일이름이나 UID/GID, POSIX 인가, 그리고 디바이스와 같"
"은 정보를 알린다. 이것은 자동으로 B<iso9660> 파일 시스템상에서 인식된다."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<JFS>"
msgstr "B<JFS>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"is a journaling filesystem, developed by IBM, that was integrated into Linux "
"2.4.24."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<minix>"
msgstr "B<minix>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "is the filesystem used in the Minix operating system, the first to run "
#| "under Linux.  It has a number of shortcomings: a 64MB partition size "
#| "limit, short filenames, a single time stamp, etc."
msgid ""
"is the filesystem used in the Minix operating system, the first to run under "
"Linux.  It has a number of shortcomings, including a 64\\ MB partition size "
"limit, short filenames, and a single timestamp.  It remains useful for "
"floppies and RAM disks."
msgstr ""
"은 Minix 운영체제에서 쓰이는 Linux에서 사용할 수 있는 파일 시스템이다.  이것"
"은 몇가지 결점이 있다: 64MB 파티션 크기 제한, 짧은 파일 이름 등."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<msdos>"
msgstr "B<msdos>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "is the filesystem used by DOS, Windows, and some OS/2 computers.  "
#| "B<msdos> filenames can be no longer than an 8 character name followed by "
#| "an optional period and 3 character extension."
msgid ""
"is the filesystem used by DOS, Windows, and some OS/2 computers.  B<msdos> "
"filenames can be no longer than 8 characters, followed by an optional period "
"and 3 character extension."
msgstr ""
"는 DOS, Window와 몇몇 OS/2 컴퓨터에서 사용되는 파일 시스템이다.  B<msdos> 파"
"일 시스템은 홉션에 따른 8 글자의 파일명과 3 글자의 확장명만이 가능하다."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ncpfs>"
msgstr "B<ncpfs>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "is a network filesystem that supports the NCP protocol, used by Novell "
#| "NetWare."
msgid ""
"is a network filesystem that supports the NCP protocol, used by Novell "
"NetWare.  It was removed from the kernel in Linux 4.17."
msgstr ""
"는 Novell NetWare에서 사용하는 NCP 프로토콜을 지원하는 네트웍 파일 시스템이"
"다."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "To use B<ncpfs>, you need special programs, which can be found at I<ftp://"
#| "linux01.gwdg.de/pub/ncpfs>."
msgid ""
"To use B<ncpfs>, you need special programs, which can be found at E<.UR "
"ftp://ftp.gwdg.de\\:/pub\\:/linux\\:/misc\\:/ncpfs> E<.UE .>"
msgstr ""
"B<ncpfs>, 를 사용하기 위해선 아래에서 구할 수 있는 특별한 프로그램이 필요하"
"다.  I<ftp://linux01.gwdg.de/pub/ncpfs>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<nfs>"
msgstr "B<nfs>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"is the network filesystem used to access disks located on remote computers."
msgstr ""
"는 원거리 컴퓨터에 있는 디스크에 접근할 때 쓰이는 네트웍 파일 시스템이다."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ntfs>"
msgstr "B<ntfs>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"is the filesystem native to Microsoft Windows NT, supporting features like "
"ACLs, journaling, encryption, and so on."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<proc>"
msgstr "B<proc>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"is a pseudo filesystem which is used as an interface to kernel data "
"structures rather than reading and interpreting I</dev/kmem>.  In "
"particular, its files do not take disk space.  See B<proc>(5)."
msgstr ""
"는 읽기나 인터페이싱 보단 커널 데이터 구조를 위한 인터페이스로 사용되는 "
"pseudo 파일 시스템이다.  I</dev/kmem>.  특이하게 이 파일은 디스크 공간을 차지"
"하지 않는다. B<proc>(5)를 참조하라."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<Reiserfs>"
msgstr "B<Reiserfs>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"is a journaling filesystem, designed by Hans Reiser, that was integrated "
"into Linux 2.4.1."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<smb>"
msgstr "B<smb>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "is a network filesystem that supports the SMB protocol, used by Windows "
#| "for Workgroups, Windows NT, and Lan Manager."
msgid ""
"is a network filesystem that supports the SMB protocol, used by Windows.  "
"See E<.UR https://www.samba.org\\:/samba\\:/smbfs/> E<.UE .>"
msgstr ""
"는 Windows Workgroups, Windows NT, Lan Manager에서 쓰이는 SMB 프로토콜을 지원"
"하는 네트웍 파일 시스템이다."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<sysv>"
msgstr "B<sysv>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"is an implementation of the System V/Coherent filesystem for Linux.  It "
"implements all of Xenix FS, System V/386 FS, and Coherent FS."
msgstr ""
"는 리눅스를 위한 System V/Coherent 실행 시스템이다.  이것은 모든 Xenix FS, "
"System V/386 FS, Coherent FS을 실행한다."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<umsdos>"
msgstr "B<umsdos>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"is an extended DOS filesystem used by Linux.  It adds capability for long "
"filenames, UID/GID, POSIX permissions, and special files (devices, named "
"pipes, etc.) under the DOS filesystem, without sacrificing compatibility "
"with DOS."
msgstr ""
"는 리눅스에서 사용되는 확장 도스 파일 시스템이다. 이것은 긴 파일 이름과 UID/"
"GID, POSIX, 특수 파일(devices, named popes, etc)을 도스 파일 시스템상에서 도"
"스의 기능을 희생하지 않고 사용 할 수 있다."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<tmpfs>"
msgstr "B<tmpfs>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"is a filesystem whose contents reside in virtual memory.  Since the files on "
"such filesystems typically reside in RAM, file access is extremely fast.  "
"See B<tmpfs>(5)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<vfat>"
msgstr "B<vfat>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "is extended DOS filesystem used by Microsoft Windows95 and Windows NT.  "
#| "VFAT adds capability for long filenames under the MSDOS filesystem."
msgid ""
"is an extended FAT filesystem used by Microsoft Windows95 and Windows NT.  "
"B<vfat> adds the capability to use long filenames under the MSDOS filesystem."
msgstr ""
"는 MS Window95와 Window NT에서 사용되는 도스 파일 시스템이다.  VFAT는 MS 도"
"스 파일 시스템에 긴 파일 이름 기능을 추가하였다."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<XFS>"
msgstr "B<XFS>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"is a journaling filesystem, developed by SGI, that was integrated into Linux "
"2.4.20."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<xiafs>"
msgstr "B<xiafs>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "was designed and implemented to be a stable, safe filesystem by extending "
#| "the Minix filesystem code.  It provides the basic most requested features "
#| "without undue complexity."
msgid ""
"was designed and implemented to be a stable, safe filesystem by extending "
"the Minix filesystem code.  It provides the basic most requested features "
"without undue complexity.  The B<xiafs> filesystem is no longer actively "
"developed or maintained.  It was removed from the kernel in Linux 2.1.21."
msgstr ""
"는 Minix 파일시스템 코드를 확장함으로써 안정성을 중심으로 디자인 되었고, 또 "
"충족시키고 있다"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "추가 참조"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<fuse>(4), B<btrfs>(5), B<ext2>(5), B<ext3>(5), B<ext4>(5), B<nfs>(5), "
"B<proc>(5), B<sysfs>(5), B<tmpfs>(5), B<xfs>(5), B<fsck>(8), B<mkfs>(8), "
"B<mount>(8)"
msgstr ""
"B<fuse>(4), B<btrfs>(5), B<ext2>(5), B<ext3>(5), B<ext4>(5), B<nfs>(5), "
"B<proc>(5), B<sysfs>(5), B<tmpfs>(5), B<xfs>(5), B<fsck>(8), B<mkfs>(8), "
"B<mount>(8)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-12-05"
msgstr "2022년 12월 5일"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm
msgid "is a CD-ROM filesystem type conforming to the ISO 9660 standard."
msgstr "는 ISO 9660 표준을 따르는 CD-ROM 파일 시스템 형태이다."

#. type: Plain text
#: debian-bookworm
msgid ""
"Linux supports High Sierra, the precursor to the ISO 9660 standard for CD-"
"ROM filesystems.  It is automatically recognized within the B<iso9660> "
"filesystem support under Linux."
msgstr ""
"리눅스는 ISO 9660 표준의 전신인 High Sierra를 지원한다 B<iso9660> 파일 시스템"
"에서 자동으로 인식된다."

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Linux man-pages 6.9.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2024-01-28"
msgstr "2024년 1월 28일"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "Linux man-pages 6.03"
msgid "Linux man-pages (unreleased)"
msgstr "Linux man-pages 6.03"
