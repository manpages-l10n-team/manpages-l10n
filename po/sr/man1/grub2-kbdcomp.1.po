# Serbian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.11.0\n"
"POT-Creation-Date: 2025-02-28 16:35+0100\n"
"PO-Revision-Date: 2022-07-23 16:15+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Serbian <>\n"
"Language: sr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#. type: TH
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "GRUB-KBDCOMP"
msgstr "GRUB-KBDCOMP"

#. type: TH
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "February 2025"
msgstr "Фебруара 2025"

#. type: TH
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "grub-kbdcomp ()"
msgstr "grub-kbdcomp ()"

#. type: TH
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Корисничке наредбе"

#. type: SH
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "НАЗИВ"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "grub-kbdcomp - generate a GRUB keyboard layout file"
msgstr "grub-kbdcomp - прави датотеку распореда тастатуре ГРУБ-а"

#. type: SH
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "УВОД"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<grub-kbdcomp> I<\\,-o OUTPUT CKBMAP_ARGUMENTS\\/>..."
msgstr "B<grub-kbdcomp> I<\\,-o ИЗЛАЗ ЦКБМАПА_АРГУМЕНТИ\\/>..."

#. type: SH
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИС"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"grub-kbdcomp processes a X keyboard layout description in B<keymaps>(5)  "
"format into a format that can be used by GRUB's B<keymap> command."
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Make GRUB keyboard layout file."
msgstr "Прави датотеку распореда тастатуре ГРУБ-а."

#. type: TP
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "print this message and exit"
msgstr "исписује ову поруку и излази"

#. type: TP
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "print the version information and exit"
msgstr "исписује податке о издању и излази"

#. type: TP
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-o>, B<--output>=I<\\,FILE\\/>"
msgstr "B<-o>, B<--output>=I<\\,ДАТОТЕКА\\/>"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "save output in FILE [required]"
msgstr "чува излаз у ДАТОТЕКУ [потребно]"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "grub-kbdcomp generates a keyboard layout for GRUB using ckbcomp"
msgstr "„grub-kbdcomp“ ствара распоред тастатуре за ГРУБ користећи „ckbcomp“"

#. type: SH
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "ПРИЈАВЉИВАЊЕ ГРЕШАКА"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr "Грешке пријавите на: E<lt>bug-grub@gnu.orgE<gt>."

#. type: SH
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ВИДИТЕ ТАКОЂЕ"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<grub-mklayout>(8)"
msgstr "B<grub-mklayout>(8)"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"The full documentation for B<grub-kbdcomp> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-kbdcomp> programs are properly installed "
"at your site, the command"
msgstr ""
"Потпуна документација за B<grub-kbdcomp> је одржавана као Тексинфо "
"упутство.  Ако су B<info> и B<grub-kbdcomp> исправно инсталирани на вашем "
"сајту, наредба"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<info grub-kbdcomp>"
msgstr "B<info grub-kbdcomp>"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "should give you access to the complete manual."
msgstr "треба да вам да приступ потпуном упутству."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "October 2024"
msgstr "Октобра 2024"

#. type: TH
#: opensuse-leap-16-0
#, fuzzy, no-wrap
#| msgid "September 2024"
msgid "December 2024"
msgstr "Септембра 2024"
