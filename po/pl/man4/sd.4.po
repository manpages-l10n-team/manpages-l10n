# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Paweł Olszewski <alder@amg.net.pl>, 1998.
# Robert Luberda <robert@debian.org>, 2006, 2012.
# Michał Kułach <michal.kulach@gmail.com>, 2013, 2016, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-pl\n"
"POT-Creation-Date: 2025-02-28 16:48+0100\n"
"PO-Revision-Date: 2024-03-13 22:16+0100\n"
"Last-Translator: Michał Kułach <michal.kulach@gmail.com>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.12.3\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "sd"
msgstr "sd"

#. type: TH
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2 maja 2024 r."

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr "Linux man-pages 6.12"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "sd - driver for SCSI disk drives"
msgstr "sd - sterownik dysków twardych SCSI"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>linux/hdreg.hE<gt>        >/* for HDIO_GETGEO */\n"
"B<#include E<lt>linux/fs.hE<gt>           >/* for BLKGETSIZE and BLKRRPART */\n"
msgstr ""
"B<#include E<lt>linux/hdreg.hE<gt>        >/* dla HDIO_GETGEO */\n"
"B<#include E<lt>linux/fs.hE<gt>           >/* dla BLKGETSIZE i BLKRRPART */\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "CONFIGURATION"
msgstr "KONFIGURACJA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The block device name has the following form: B<sd>I<lp,> where I<l> is a "
"letter denoting the physical drive, and I<p> is a number denoting the "
"partition on that physical drive.  Often, the partition number, I<p>, will "
"be left off when the device corresponds to the whole drive."
msgstr ""
"Nazwa tego urządzenia blokowego ma następującą postać: B<sd>I<lp>, gdzie "
"I<l> jest literą oznaczającą fizyczny dysk, a I<p> jest cyfrą oznaczającą "
"partycję na tym fizycznym dysku. Często numer partycji I<p> jest opuszczany, "
"jeśli nazwa urządzenia odnosi się do całego dysku."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"SCSI disks have a major device number of 8, and a minor device number of the "
"form (16 * I<drive_number>) + I<partition_number>, where I<drive_number> is "
"the number of the physical drive in order of detection, and "
"I<partition_number> is as follows:"
msgstr ""
"Dyski SCSI mają główny numer urządzenia równy 8, a numer poboczny w postaci "
"(16 * I<numer_dysku>) + I<numer_partycji>, gdzie I<numer_dysku> jest numerem "
"fizycznego dysku wg kolejności wykrywania, a I<numer_partycji> jest "
"następujący:"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr "\\[bu]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "partition 0 is the whole drive"
msgstr "partycja 0 to cały dysk"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "partitions 1\\[en]4 are the DOS \"primary\" partitions"
msgstr "partycje 1\\[en]4 to \\[Bq]podstawowe\\[rq] partycje DOS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"partitions 5\\[en]8 are the DOS \"extended\" (or \"logical\") partitions"
msgstr ""
"partycje 5\\[en]8 to \\[Bq]rozszerzone\\[rq] (\\[Bq]logiczne\\[rq]) partycje "
"DOS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For example, I</dev/sda> will have major 8, minor 0, and will refer to all "
"of the first SCSI drive in the system; and I</dev/sdb3> will have major 8, "
"minor 19, and will refer to the third DOS \"primary\" partition on the "
"second SCSI drive in the system."
msgstr ""
"Na przykład I</dev/sda> ma numer główny 8, poboczny 0 i odnosić się będzie "
"do całego pierwszego dysku SCSI w systemie; I</dev/sdb3> ma numer główny 8, "
"poboczny 19 i odnosić się będzie do trzeciej \\[Bq]podstawowej\\[rq] "
"partycji DOS na drugim dysku SCSI w systemie."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"At this time, only block devices are provided.  Raw devices have not yet "
"been implemented."
msgstr ""
"W chwili obecnej, obsługiwane są jedynie urządzenia blokowe. Urządzenia "
"surowe nie zostały jeszcze zaimplementowane."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The following I<ioctl>s are provided:"
msgstr "Obsługiwane są następujące I<ioctl>e:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<HDIO_GETGEO>"
msgstr "B<HDIO_GETGEO>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Returns the BIOS disk parameters in the following structure:"
msgstr "Zwraca parametry dysku z BIOS w następującej strukturze:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"struct hd_geometry {\n"
"    unsigned char  heads;\n"
"    unsigned char  sectors;\n"
"    unsigned short cylinders;\n"
"    unsigned long  start;\n"
"};\n"
msgstr ""
"struct hd_geometry {\n"
"    unsigned char  heads;\n"
"    unsigned char  sectors;\n"
"    unsigned short cylinders;\n"
"    unsigned long  start;\n"
"};\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "A pointer to this structure is passed as the B<ioctl>(2)  parameter."
msgstr ""
"Wskaźnik do tej struktury przekazywany jest jako parametr funkcji "
"B<ioctl>(2)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The information returned in the parameter is the disk geometry of the drive "
"I<as understood by DOS!> This geometry is I<not> the physical geometry of "
"the drive.  It is used when constructing the drive's partition table, "
"however, and is needed for convenient operation of B<fdisk>(1), "
"B<efdisk>(1), and B<lilo>(1).  If the geometry information is not available, "
"zero will be returned for all of the parameters."
msgstr ""
"Informacja zwracana do parametru jest geometrią dysku I<w rozumieniu systemu "
"DOS!> Owa geometria I<nie> jest fizyczną geometrią dysku. Jest jednak "
"używana przy konstruowaniu tablicy partycji danego dysku i jest niezbędna "
"dla poprawnego działania programów B<fdisk>(1), B<efdisk>(1) i B<lilo>(1). "
"Jeśli informacja o geometrii jest niedostępna, zwrócona będzie wartość zero "
"dla wszystkich parametrów."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<BLKGETSIZE>"
msgstr "B<BLKGETSIZE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Returns the device size in sectors.  The B<ioctl>(2)  parameter should be a "
"pointer to a I<long>."
msgstr ""
"Zwraca rozmiar urządzenia w sektorach. Parametr funkcji B<ioctl>(2) powinien "
"być wskaźnikiem do zmiennej typu I<long>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<BLKRRPART>"
msgstr "B<BLKRRPART>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Forces a reread of the SCSI disk partition tables.  No parameter is needed."
msgstr ""
"Wymusza ponowny odczyt tablicy partycji dysku SCSI. Nie są wymagane żadne "
"parametry."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The SCSI B<ioctl>(2)  operations are also supported.  If the B<ioctl>(2)  "
"parameter is required, and it is NULL, then B<ioctl>(2)  fails with the "
"error B<EINVAL>."
msgstr ""
"Operacje B<ioctl>(2) dla SCSI są również obsługiwane. Jeśli wymagany jest "
"parametr funkcji B<ioctl>(2) i ma on wartość NULL, wówczas B<ioctl>(2) "
"zwraca błąd B<EINVAL>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "PLIKI"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</dev/sd[a-h]>"
msgstr "I</dev/sd>[I<a>-I<h>]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "the whole device"
msgstr "całe urządzenie"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</dev/sd[a-h][0-8]>"
msgstr "I</dev/sd>[I<a>-I<h>][I<0>-I<8>]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "individual block partitions"
msgstr "poszczególne partycje blokowe"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 lutego 2023 r."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Linux man-pages 6.9.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 października 2023 r."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Linux man-pages (niewydane)"
