# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Andrzej Krzysztofowicz <ankry@green.mf.pg.gda.pl>, 2001, 2002.
# Robert Luberda <robert@debian.org>, 2013, 2017, 2019.
# Michał Kułach <michal.kulach@gmail.com>, 2016, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-pl\n"
"POT-Creation-Date: 2025-02-28 16:46+0100\n"
"PO-Revision-Date: 2024-04-27 13:23+0200\n"
"Last-Translator: Michał Kułach <michal.kulach@gmail.com>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 22.12.3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "rint"
msgstr "rint"

#. type: TH
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "2024-07-23"
msgstr "23 lipca 2024 r."

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr "Linux man-pages 6.12"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"nearbyint, nearbyintf, nearbyintl, rint, rintf, rintl - round to nearest "
"integer"
msgstr ""
"nearbyint, nearbyintf, nearbyintl, rint, rintf, rintl - zaokrągla do "
"najbliższej liczby całkowitej"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTEKA"

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "Math library (I<libm>,\\ I<-lm>)"
msgstr "Biblioteka matematyczna (I<libm>,\\ I<-lm>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>math.hE<gt>>\n"
msgstr "B<#include E<lt>math.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<double nearbyint(double >I<x>B<);>\n"
"B<float nearbyintf(float >I<x>B<);>\n"
"B<long double nearbyintl(long double >I<x>B<);>\n"
msgstr ""
"B<double nearbyint(double >I<x>B<);>\n"
"B<float nearbyintf(float >I<x>B<);>\n"
"B<long double nearbyintl(long double >I<x>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<double rint(double >I<x>B<);>\n"
"B<float rintf(float >I<x>B<);>\n"
"B<long double rintl(long double >I<x>B<);>\n"
msgstr ""
"B<double rint(double >I<x>B<);>\n"
"B<float rintf(float >I<x>B<);>\n"
"B<long double rintl(long double >I<x>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Wymagane ustawienia makr biblioteki glibc (patrz B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<nearbyint>(), B<nearbyintf>(), B<nearbyintl>():"
msgstr "B<nearbyint>(), B<nearbyintf>(), B<nearbyintl>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "    _POSIX_C_SOURCE E<gt>= 200112L || _ISOC99_SOURCE\n"
msgstr "    _POSIX_C_SOURCE E<gt>= 200112L || _ISOC99_SOURCE\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<rint>():"
msgstr "B<rint>():"

#.     || _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"
"        || _XOPEN_SOURCE E<gt>= 500\n"
"        || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"_ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"
"    || _XOPEN_SOURCE E<gt>= 500\n"
"    || /* Od glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* glibc w wersji E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<rintf>(), B<rintl>():"
msgstr "B<rintf>(), B<rintl>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"
"        || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"_ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"
"    || /* Od glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* glibc w wersji E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<nearbyint>(), B<nearbyintf>(), and B<nearbyintl>()  functions round "
"their argument to an integer value in floating-point format, using the "
"current rounding direction (see B<fesetround>(3))  and without raising the "
"I<inexact> exception.  When the current rounding direction is to nearest, "
"these functions round halfway cases to the even integer in accordance with "
"IEEE-754."
msgstr ""
"Funkcje B<nearbyint>(), B<nearbyintf>() oraz B<nearbyintl>() zaokrąglają "
"swoje argumenty do wartości całkowitej w postaci zmiennoprzecinkowej, "
"korzystając z bieżącego kierunku zaokrąglania (patrz B<fesetround>(3)) i nie "
"powodując wystąpienia wyjątku I<inexact>. Gdy bieżących kierunkiem "
"zaokrąglania jest do najbliższej, w przypadkach połówkowych funkcje te "
"zaokrąglają do całkowitej parzystej, zgodnie z IEEE-754."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<rint>(), B<rintf>(), and B<rintl>()  functions do the same, but will "
"raise the I<inexact> exception (B<FE_INEXACT>, checkable via "
"B<fetestexcept>(3))  when the result differs in value from the argument."
msgstr ""
"Funkcje B<rint>(), B<rintf>() oraz B<rintl>() robią to samo, ale powodują "
"wystąpienie wyjątku I<inexact> (B<FE_INEXACT>, wystąpienie wyjątku można "
"sprawdzić za pomocą B<fetestexcept>(3)), gdy wynik ma inną wartość niż "
"argument."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "WARTOŚĆ ZWRACANA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "These functions return the rounded integer value."
msgstr "Funkcje te zwracają zaokrągloną wartość całkowitą."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "If I<x> is integral, +0, -0, NaN, or infinite, I<x> itself is returned."
msgstr ""
"Jeśli I<x> jest całkowite, +0, -0, NaN lub nieskończoność, to zwracane jest "
"I<x>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "BŁĘDY"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "No errors occur."
msgstr "Nie występują."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRYBUTY"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Informacje o pojęciach używanych w tym rozdziale można znaleźć w podręczniku "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interfejs"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atrybut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Wartość"

#. type: tbl table
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<nearbyint>(),\n"
"B<nearbyintf>(),\n"
"B<nearbyintl>(),\n"
"B<rint>(),\n"
"B<rintf>(),\n"
"B<rintl>()"
msgstr ""
"B<nearbyint>(),\n"
"B<nearbyintf>(),\n"
"B<nearbyintl>(),\n"
"B<rint>(),\n"
"B<rintf>(),\n"
"B<rintl>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Bezpieczeństwo wątkowe"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-bezpieczne"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDY"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr "C11, POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIA"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "C99, POSIX.1-2001."
msgstr "C99, POSIX.1-2001."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "SUSv2 and POSIX.1-2001 contain text about overflow (which might set "
#| "I<errno> to B<ERANGE>, or raise an B<FE_OVERFLOW> exception).  In "
#| "practice, the result cannot overflow on any current machine, so this "
#| "error-handling stuff is just nonsense.  (More precisely, overflow can "
#| "happen only when the maximum value of the exponent is smaller than the "
#| "number of mantissa bits.  For the IEEE-754 standard 32-bit and 64-bit "
#| "floating-point numbers the maximum value of the exponent is 127 "
#| "(respectively, 1023), and the number of mantissa bits including the "
#| "implicit bit is 24 (respectively, 53).)"
msgid ""
"SUSv2 and POSIX.1-2001 contain text about overflow (which might set I<errno> "
"to B<ERANGE>, or raise an B<FE_OVERFLOW> exception).  In practice, the "
"result cannot overflow on any current machine, so this error-handling stuff "
"was just nonsense.  (More precisely, overflow can happen only when the "
"maximum value of the exponent is smaller than the number of mantissa bits.  "
"For the IEEE-754 standard 32-bit and 64-bit floating-point numbers the "
"maximum value of the exponent is 127 (respectively, 1023), and the number of "
"mantissa bits including the implicit bit is 24 (respectively, 53).)  This "
"was removed in POSIX.1-2008."
msgstr ""
"SUSv2 oraz POSIX.1-2001 zawierają tekst dotyczący przepełnienia (które może "
"spowodować ustawienie I<errno> na B<ERANGE> lub spowodować wyjątek "
"B<FE_OVERFLOW>). W praktyce wynik nie może spowodować przepełnienia na "
"żadnej współczesnej maszynie, więc obsługa tego błędu jest po prostu "
"bezsensowna. (Dokładniej, przepełnienie może wystąpić, tylko gdy wartość "
"maksymalna wykładnika jest mniejsza niż liczba bitów mantysy. Według "
"standardu IEEE-754 dla 32 i 64-bitowych liczb zmiennoprzecinkowych wartość "
"maksymalna wykładnika wynosi 127 (odpowiednio 1023), a liczba bitów mantysy, "
"w tym bit niejawny, wynosi 24 (odpowiednio 53))."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If you want to store the rounded value in an integer type, you probably want "
"to use one of the functions described in B<lrint>(3)  instead."
msgstr ""
"Aby przechowywać zaokrągloną wartość w typie całkowitym, należy użyć jednej "
"z funkcji opisanych w podręczniku B<lrint>(3)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<ceil>(3), B<floor>(3), B<lrint>(3), B<round>(3), B<trunc>(3)"
msgstr "B<ceil>(3), B<floor>(3), B<lrint>(3), B<round>(3), B<trunc>(3)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 lutego 2023 r."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "Math library (I<libm>, I<-lm>)"
msgstr "Biblioteka matematyczna (I<libm>, I<-lm>)"

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"No errors occur.  POSIX.1-2001 documents a range error for overflows, but "
"see NOTES."
msgstr ""
"Nie występują. POSIX.1-2001 dokumentuje błąd zakresu przy przepełnieniu "
"\\(em patrz UWAGI."

#. type: Plain text
#: debian-bookworm
msgid "C99, POSIX.1-2001, POSIX.1-2008."
msgstr "C99, POSIX.1-2001, POSIX.1-2008."

#. type: SH
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "NOTES"
msgstr "UWAGI"

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"SUSv2 and POSIX.1-2001 contain text about overflow (which might set I<errno> "
"to B<ERANGE>, or raise an B<FE_OVERFLOW> exception).  In practice, the "
"result cannot overflow on any current machine, so this error-handling stuff "
"is just nonsense.  (More precisely, overflow can happen only when the "
"maximum value of the exponent is smaller than the number of mantissa bits.  "
"For the IEEE-754 standard 32-bit and 64-bit floating-point numbers the "
"maximum value of the exponent is 127 (respectively, 1023), and the number of "
"mantissa bits including the implicit bit is 24 (respectively, 53).)"
msgstr ""
"SUSv2 oraz POSIX.1-2001 zawierają tekst dotyczący przepełnienia (które może "
"spowodować ustawienie I<errno> na B<ERANGE> lub spowodować wyjątek "
"B<FE_OVERFLOW>). W praktyce wynik nie może spowodować przepełnienia na "
"żadnej współczesnej maszynie, więc obsługa tego błędu jest po prostu "
"bezsensowna. (Dokładniej, przepełnienie może wystąpić, tylko gdy wartość "
"maksymalna wykładnika jest mniejsza niż liczba bitów mantysy. Według "
"standardu IEEE-754 dla 32 i 64-bitowych liczb zmiennoprzecinkowych wartość "
"maksymalna wykładnika wynosi 127 (odpowiednio 1023), a liczba bitów mantysy, "
"w tym bit niejawny, wynosi 24 (odpowiednio 53))."

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "2024-06-16"
msgstr "16 czerwca 2024 r."

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Linux man-pages 6.9.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 października 2023 r."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Linux man-pages (niewydane)"
