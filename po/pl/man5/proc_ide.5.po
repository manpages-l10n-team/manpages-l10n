# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Michał Kułach <michal.kulach@gmail.com>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.22.0\n"
"POT-Creation-Date: 2025-02-28 16:43+0100\n"
"PO-Revision-Date: 2024-03-31 17:57+0200\n"
"Last-Translator: Michał Kułach <michal.kulach@gmail.com>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 22.12.3\n"

#. type: TH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "proc_ide"
msgstr "proc_ide"

#. type: TH
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2 maja 2024 r."

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr "Linux man-pages 6.12"

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "/proc/ide/ - IDE channels and attached devices"
msgstr "/proc/ide/ - kanały IDE i dołączone urządzenia"

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</proc/ide>"
msgstr "I</proc/ide>"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This directory exists on systems with the IDE bus.  There are directories "
"for each IDE channel and attached device.  Files include:"
msgstr ""
"Katalog ten istnieje w systemach zawierających magistralę IDE. Zawiera po "
"jednym katalogu dla każdego kanału IDE oraz dla przyłączonych urządzeń. "
"Wśród plików są:"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"cache              buffer size in KB\n"
"capacity           number of sectors\n"
"driver             driver version\n"
"geometry           physical and logical geometry\n"
"identify           in hexadecimal\n"
"media              media type\n"
"model              manufacturer\\[aq]s model number\n"
"settings           drive settings\n"
"smart_thresholds   IDE disk management thresholds (in hex)\n"
"smart_values       IDE disk management values (in hex)\n"
msgstr ""
"cache              rozmiar bufora w KB\n"
"capacity           liczba sektorów\n"
"driver             wersja sterownika\n"
"geometry           geometria fizyczna i logiczna\n"
"identify           szesnastkowo\n"
"media              rodzaj nośnika\n"
"model              numer modelu producenta\n"
"settings           ustawienia napędu\n"
"smart_thresholds   progi zarządzania dyskiem IDE (szesn.)\n"
"smart_values       wartości zarządzania dyskiem IDE (szesn.)\n"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<hdparm>(8)  utility provides access to this information in a friendly "
"format."
msgstr ""
"Dostęp do tych informacji w przyjaznym formacie umożliwia program "
"narzędziowy B<hdparm>(8)."

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<proc>(5)"
msgstr "B<proc>(5)"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Linux man-pages 6.9.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-08-15"
msgstr "15 sierpnia 2023 r."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Linux man-pages (niewydane)"
