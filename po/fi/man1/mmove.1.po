# Finnish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Jari Kivirinta <jkivirin@ratol.fi>, 1998.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2025-02-28 16:40+0100\n"
"PO-Revision-Date: 1998-04-10 11:01+0200\n"
"Last-Translator: Jari Kivirinta <jkivirin@ratol.fi>\n"
"Language-Team: Finnish <>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "mmove"
msgstr "mmove"

#. type: TH
#: archlinux debian-unstable fedora-42
#, no-wrap
msgid "19Jan25"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-42
#, no-wrap
msgid "mtools-4.0.47"
msgstr "mtools-4.0.47"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Name"
msgstr "Nimi"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "mmove - move or rename an MSDOS file or subdirectory"
msgstr "mmove - siirrä tai nimeä uudelleen MSDOS tiedosto tai alihakemisto"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Note\\ of\\ warning"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This manpage has been automatically generated from mtools's texinfo "
"documentation, and may not be entirely accurate or complete.  See the end of "
"this man page for details."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Description"
msgstr "Kuvaus"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The \\&CW<mmove> command is used to move or rename an existing MS-DOS file "
"or subdirectory."
msgstr ""
"\\&CW<mmove> komentoa käytetään siirtämään tai uudelleennimeämään MS-DOS "
"tiedosto tai alihakemisto."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "\\&\\&CW<mmove> [\\&CW<-voOsSrRA>] I<sourcefile> I<targetfile> \\&\\&CW<mmove> [\\&CW<-voOsSrRA>] I<sourcefile> [ I<sourcefiles>\\&... ] I<targetdirectory>"
msgid ""
"I<\\&>\\&CW<mmove> [\\&CW<-v>] [\\&CW<-D> I<clash_option>] I<sourcefile> I<targetfile>\n"
"\\&\\&CW<mmove> [\\&CW<-v>]  [\\&CW<-D> I<clash_option>] I<sourcefile> [ I<sourcefiles>\\&... ] I<targetdirectory>\n"
msgstr "\\&\\&CW<mmove> [\\&CW<-voOsSrRA>] I<lähdetiedosto> I<kohdetiedosto> \\&\\&CW<mmove> [\\&CW<-voOsSrRA>] I<lähdetiedosto> [ I<lähdetiedostot>\\&... ] I<kohdehakemisto>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
msgid ""
"\\&\\&CW<Mmove> moves or renames an existing MS-DOS file or subdirectory. "
"Unlike the MS-DOS version of \\&CW<MOVE>, \\&CW<mmove> is able to move "
"subdirectories.  Files or directories can only be moved within one file "
"system. Data cannot be moved from MS-DOS to Unix or vice-versa.  If you omit "
"the drive letter from the target file or directory, the same letter as for "
"the source is assumed.  If you omit the drive letter from all parameters, "
"drive a: is assumed by default."
msgstr ""
"\\&\\&CW<Mmove> siirtää tai uudelleennimeää MS-DOS tiedoston tai "
"alihakemiston.  Toisin kuin MS-DOSin \\&CW<MOVE>, \\&CW<mmove> pystyy "
"siirtämään alihakemistoja."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "See\\ Also"
msgstr "Katso\\ myös"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Mtools' texinfo doc"
msgstr "Mtools texinfo-dokumentti"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Viewing\\ the\\ texi\\ doc"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This manpage has been automatically generated from mtools's texinfo "
"documentation. However, this process is only approximative, and some items, "
"such as crossreferences, footnotes and indices are lost in this translation "
"process.  Indeed, these items have no appropriate representation in the "
"manpage format.  Moreover, not all information has been translated into the "
"manpage version.  Thus I strongly advise you to use the original texinfo "
"doc.  See the end of this manpage for instructions how to view the texinfo "
"doc."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "* \\ \\ "
msgstr "* \\ \\ "

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"To generate a printable copy from the texinfo doc, run the following "
"commands:"
msgstr ""
"Luodaksesi tulostettavan kopion texinfo-dokumentista, suorita seuraavat "
"komennot:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<    ./configure; make dvi; dvips mtools.dvi>\n"
msgstr "B<    ./configure; make dvi; dvips mtools.dvi>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "To generate a html copy, run:"
msgstr "Luodaksesi html-kopion, suorita:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<    ./configure; make html>\n"
msgstr "B<    ./configure; make html>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"\\&A premade html can be found at \\&\\&CW<\\(ifhttp://www.gnu.org/software/"
"mtools/manual/mtools.html\\(is>"
msgstr ""
"\\&Valmis html-kopio löytyy osoitteesta: \\&\\&CW<\\(ifhttp://www.gnu.org/"
"software/mtools/manual/mtools.html\\(is>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "To generate an info copy (browsable using emacs' info mode), run:"
msgstr "Luodaksesi info-kopion (selattavissa emacsin info-tilassa), suorita:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<    ./configure; make info>\n"
msgstr "B<    ./configure; make info>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The texinfo doc looks most pretty when printed or as html.  Indeed, in the "
"info version certain examples are difficult to read due to the quoting "
"conventions used in info."
msgstr ""
"Texinfo-dokumentti näyttää parhaimmalta tulostettuna tai html-muodossa.  "
"Info-versiossa tietyt esimerkit ovat vaikeita lukea johtuen tietyistä infon "
"käyttämistä lainaustavoista."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "10Jul21"
msgstr "10. heinäkuuta 2021"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "mtools-4.0.32"
msgstr "mtools-4.0.32"

#. type: TH
#: fedora-rawhide
#, no-wrap
msgid "22Feb25"
msgstr ""

#. type: TH
#: fedora-rawhide
#, no-wrap
msgid "mtools-4.0.48"
msgstr "mtools-4.0.48"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "02Jun24"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "mtools-4.0.44"
msgstr "mtools-4.0.44"

#. type: TH
#: opensuse-leap-16-0
#, no-wrap
msgid "21Mar23"
msgstr ""

#. type: TH
#: opensuse-leap-16-0
#, no-wrap
msgid "mtools-4.0.43"
msgstr "mtools-4.0.43"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "28Sep24"
msgstr ""

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "mtools-4.0.45"
msgstr "mtools-4.0.45"
