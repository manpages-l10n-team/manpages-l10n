# Finnish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Marko Viirelä <mviirela@paju.oulu.fi>, 1998.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2025-02-28 16:31+0100\n"
"PO-Revision-Date: 1998-04-10 11:01+0200\n"
"Last-Translator: Marko Viirelä <mviirela@paju.oulu.fi>\n"
"Language-Team: Finnish <>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "CP"
msgstr "CP"

#. type: TH
#: archlinux fedora-rawhide
#, no-wrap
msgid "February 2025"
msgstr "Helmikuuta 2025"

#. type: TH
#: archlinux fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "GNU coreutils 9.6"
msgstr "GNU coreutils 9.6"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Käyttäjän sovellukset"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NIMI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
msgid "cp - copy files and directories"
msgstr "kohdehakemisto ei ole sallittu asennettaessa hakemistoa"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "YLEISKATSAUS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<cp> [I<\\,OPTION\\/>]... [I<\\,-T\\/>] I<\\,SOURCE DEST\\/>"
msgstr "B<cp> [I<\\,VALITSIN\\/>]... [I<\\,-T\\/>] I<\\,LÄHDE KOHDE\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<cp> [I<\\,OPTION\\/>]... I<\\,SOURCE\\/>... I<\\,DIRECTORY\\/>"
msgstr "B<cp> [I<\\,VALITSIN\\/>]... I<\\,LÄHDE\\/>... I<\\,HAKEMISTO\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<cp> [I<\\,OPTION\\/>]... I<\\,-t DIRECTORY SOURCE\\/>..."
msgstr "B<cp> [I<\\,VALITSIN\\/>]... I<\\,-t HAKEMISTO LÄHDE\\/>..."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "KUVAUS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Copy SOURCE to DEST, or multiple SOURCE(s) to DIRECTORY."
msgstr "Kopioi LÄHDE KOHTEEseen, tai useita LÄHTEitä HAKEMISTOon."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Mandatory arguments to long options are mandatory for short options too."
msgstr ""
"Pitkien valitsinten pakolliset argumentit ovat pakollisia myös lyhyille."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-a>, B<--archive>"
msgstr "B<-a>, B<--archive>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "same as B<-dR> B<--preserve>=I<\\,all\\/>"
msgstr "sama kuin B<-dR> B<--preserve>=I<\\,all\\/>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--attributes-only>"
msgstr "B<--attributes-only>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "don't copy the file data, just the attributes"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--backup>[=I<\\,CONTROL\\/>]"
msgstr "B<--backup>[=I<\\,MENETELMÄ\\/>]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "make a backup of each existing destination file"
msgstr "varmuuskopioi jokainen olemassaoleva kohdetiedosto"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-b>"
msgstr "B<-b>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "like B<--backup> but does not accept an argument"
msgstr "kuten B<--backup>, mutta ilman argumenttia"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--copy-contents>"
msgstr "B<--copy-contents>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "copy contents of special files when recursive"
msgstr "kopioi erikoistiedostojen sisältö rekursiota käytettäessä"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-d>"
msgstr "B<-d>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "same as B<--no-dereference> B<--preserve>=I<\\,links\\/>"
msgstr "sama kuin B<--no-dereference> B<--preserve>=I<\\,links\\/>"

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--debug>"
msgstr "B<--debug>"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "explain how a file is copied.  Implies B<-v>"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-f>, B<--force>"
msgstr "B<-f>, B<--force>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"if an existing destination file cannot be opened, remove it and try again "
"(this option is ignored when the B<-n> option is also used)"
msgstr ""
"jos olemassaolevaa kohdetiedostoa ei voi avata, poista se ja yritä uudelleen "
"(tarpeeton valitsinta B<-n> käytettäessä)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-i>, B<--interactive>"
msgstr "B<-i>, B<--interactive>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "prompt before overwrite (overrides a previous B<-n> option)"
msgstr "kysy ennen korvaamista (kumoaa aiemman B<-n>-valitsimen)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-H>"
msgstr "B<-H>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "follow command-line symbolic links in SOURCE"
msgstr "seuraa komentorivillä annettuja LÄHTEEN symbolisia linkkejä"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-l>, B<--link>"
msgstr "B<-l>, B<--link>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "hard link files instead of copying"
msgstr "linkitä tiedostot kopioinnin sijaan"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-L>, B<--dereference>"
msgstr "B<-L>, B<--dereference>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "always follow symbolic links in SOURCE"
msgstr "seuraa aina LÄHTEEN symbolisia linkkejä"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-n>, B<--no-clobber>"
msgstr "B<-n>, B<--no-clobber>"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "(deprecated) silently skip existing files.  See also B<--update>"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-P>, B<--no-dereference>"
msgstr "B<-P>, B<--no-dereference>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "never follow symbolic links in SOURCE"
msgstr "älä koskaan seuraa LÄHTEEN symlinkkejä"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-p>"
msgstr "B<-p>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "same as B<--preserve>=I<\\,mode\\/>,ownership,timestamps"
msgstr "sama kuin B<--preserve>=I<\\,mode\\/>,ownership,timestamps"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--preserve>[=I<\\,ATTR_LIST\\/>]"
msgstr "B<--preserve>[=I<\\,OMIN.LUETT\\/>]"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid "don't preserve the specified attributes"
msgid "preserve the specified attributes"
msgstr "älä säilytä annettuja ominaisuuksia"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--no-preserve>=I<\\,ATTR_LIST\\/>"
msgstr "B<--no-preserve>=I<\\,OMIN.LUETT\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "don't preserve the specified attributes"
msgstr "älä säilytä annettuja ominaisuuksia"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--parents>"
msgstr "B<--parents>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "use full source file name under DIRECTORY"
msgstr "lisää lähdepolku HAKEMISTOon"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-R>, B<-r>, B<--recursive>"
msgstr "B<-R>, B<-r>, B<--recursive>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "copy directories recursively"
msgstr "kopioi hakemistot rekursiivisesti"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--reflink>[=I<\\,WHEN\\/>]"
msgstr "B<--reflink>[=I<\\,MILLOIN\\/>]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "control clone/CoW copies. See below"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--remove-destination>"
msgstr "B<--remove-destination>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"remove each existing destination file before attempting to open it (contrast "
"with B<--force>)"
msgstr ""
"poista kukin olemassaoleva kohdetiedosto ennen avausyritystä (vertaa "
"valitsimeen B<--force>)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--sparse>=I<\\,WHEN\\/>"
msgstr "B<--sparse>=I<\\,MILLOIN\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "control creation of sparse files. See below"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--strip-trailing-slashes>"
msgstr "B<--strip-trailing-slashes>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "remove any trailing slashes from each SOURCE argument"
msgstr "poista /-merkit jokaisen LÄHDEargumentin perästä"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-s>, B<--symbolic-link>"
msgstr "B<-s>, B<--symbolic-link>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "make symbolic links instead of copying"
msgstr "tee symbolisia linkkejä kopioinnin sijaan"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-S>, B<--suffix>=I<\\,SUFFIX\\/>"
msgstr "B<-S>, B<--suffix>=I<\\,JÄLKILIITE\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "override the usual backup suffix"
msgstr "syrjäytä tavanomainen varmuuskopion jälkiliite"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-t>, B<--target-directory>=I<\\,DIRECTORY\\/>"
msgstr "B<-t>, B<--target-directory>=I<\\,HAKEMISTO\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "copy all SOURCE arguments into DIRECTORY"
msgstr "siirrä kaikki LÄHDE-argumentit HAKEMISTOon"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-T>, B<--no-target-directory>"
msgstr "B<-T>, B<--no-target-directory>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "treat DEST as a normal file"
msgstr "käsittele KOHDE normaalina tiedostona"

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<--hide>=I<\\,PATTERN\\/>"
msgid "B<--update>[=I<\\,UPDATE\\/>]"
msgstr "B<--hide>=I<\\,HAHMO\\/>"

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"control which existing files are updated; UPDATE={all,none,none-"
"fail,older(default)}"
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-u>"
msgstr "B<-u>"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "equivalent to B<--update>[=I<\\,older\\/>].  See below"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "explain what is being done"
msgstr "selitä mitä tapahtuu"

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<--keep-file-symbols>"
msgid "B<--keep-directory-symlink>"
msgstr "B<--keep-file-symbols>"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "follow existing symlinks to directories"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-x>, B<--one-file-system>"
msgstr "B<-x>, B<--one-file-system>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "stay on this file system"
msgstr "pysy tässä tiedostojärjestelmässä"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-Z>"
msgstr "B<-Z>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "set SELinux security context of destination file to default type"
msgstr "asettaa SELinux-suojauskontekstin määränpäähän tiedosto oletustyyppiin"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--context>[=I<\\,CTX\\/>]"
msgstr "B<--context>[=I<\\,CTX\\/>]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"like B<-Z>, or if CTX is specified then set the SELinux or SMACK security "
"context to CTX"
msgstr ""
"kuten B<-Z>, tai jos CTX on määritetty, aseta SELinux- tai SMACK-"
"suojauskonteksti CTX:ään"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "näytä tämä ohje ja poistu"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "näytä versiotiedot ja poistu"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"ATTR_LIST is a comma-separated list of attributes. Attributes are 'mode' for "
"permissions (including any ACL and xattr permissions), 'ownership' for user "
"and group, 'timestamps' for file timestamps, 'links' for hard links, "
"'context' for security context, 'xattr' for extended attributes, and 'all' "
"for all attributes."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"By default, sparse SOURCE files are detected by a crude heuristic and the "
"corresponding DEST file is made sparse as well.  That is the behavior "
"selected by B<--sparse>=I<\\,auto\\/>.  Specify B<--sparse>=I<\\,always\\/> "
"to create a sparse DEST file whenever the SOURCE file contains a long enough "
"sequence of zero bytes.  Use B<--sparse>=I<\\,never\\/> to inhibit creation "
"of sparse files."
msgstr ""
"Hajanaiset LÄHDEtiedostot tunnistetaan oletuksena karkealla heuristiikalla, "
"ja vastaavasta KOHDEtiedostosta tehdään myös hajanainen. Tämän "
"käyttäytymisen valitsee B<--sparse>=I<\\,auto\\/>. Antamalla valitsin B<--"
"sparse>=I<\\,always\\/> luodaan hajanainen KOHDEtiedosto aina, kun LÄHDE "
"sisältää riittävän pitkän sarjan 0-tavuja. Valitsin B<--sparse>=I<\\,never\\/"
"> estää hajanaisten tiedostojen luomisen."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"UPDATE controls which existing files in the destination are replaced.  "
"\\&'all' is the default operation when an B<--update> option is not "
"specified, and results in all existing files in the destination being "
"replaced.  \\&'none' is like the B<--no-clobber> option, in that no files in "
"the destination are replaced, and skipped files do not induce a failure.  "
"\\&'none-fail' also ensures no files are replaced in the destination, but "
"any skipped files are diagnosed and induce a failure.  \\&'older' is the "
"default operation when B<--update> is specified, and results in files being "
"replaced if they're older than the corresponding source file."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
msgid ""
"When B<--reflink>[=I<\\,always\\/>] is specified, perform a lightweight "
"copy, where the data blocks are copied only when modified.  If this is not "
"possible the copy fails, or if B<--reflink>=I<\\,auto\\/> is specified, fall "
"back to a standard copy.  Use B<--reflink>=I<\\,never\\/> to ensure a "
"standard copy is performed."
msgstr ""
"Kun B<--reflink>[=I<\\,always\\/>] on annettu, suoritetaan kevytkopiointi, "
"jolloin vain muuttuneet datalohkot kopioidaan. Mikäli tämä ei ole "
"mahdollista, kopiointi epäonnistuu, tai B<--reflink>=I<\\,auto\\/>:n "
"tapauksessa suoritetaan normaali kopiointi."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The backup suffix is '~', unless set with B<--suffix> or "
"SIMPLE_BACKUP_SUFFIX.  The version control method may be selected via the "
"B<--backup> option or through the VERSION_CONTROL environment variable.  "
"Here are the values:"
msgstr ""
"Varmuuskopion jälkiliite on ”~”, ellei muuta ole asetettu valitsimella B<--"
"suffix> tai ympäristömuuttujalla SIMPLE_BACKUP_SUFFIX. "
"Versionhallintamenetelmän voi valita käyttäen valitsinta B<--backup> tai "
"ympäristömuuttujaa VERSION_CONTROL. Arvot ovat seuraavat:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "none, off"
msgstr "none, off"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "never make backups (even if B<--backup> is given)"
msgstr "älä varmuuskopioi koskaan (vaikka B<--backup> olisi annettu)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "numbered, t"
msgstr "numbered, t"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "make numbered backups"
msgstr "tee numeroituja varmuuskopioita"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "existing, nil"
msgstr "existing, nil"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "numbered if numbered backups exist, simple otherwise"
msgstr "numeroituja jos sellaisia on jo olemassa, muuten yksinkertaisia"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "simple, never"
msgstr "simple, never"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "always make simple backups"
msgstr "tee aina yksinkertaisia varmuuskopiota"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"As a special case, cp makes a backup of SOURCE when the force and backup "
"options are given and SOURCE and DEST are the same name for an existing, "
"regular file."
msgstr ""
"Erikoistapaus: B<cp> tekee varmuuskopion LÄHTEestä, jos B<--force>- ja B<--"
"backup>- valitsimet on annettu, ja LÄHDE ja KOHDE ovat sama nimi "
"olemassaolevalle tavalliselle tiedostolle."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "TEKIJÄ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Written by Torbjorn Granlund, David MacKenzie, and Jim Meyering."
msgstr "Kirjoittaneet Torbjorn Granlund, David MacKenzie ja Jim Meyering."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "VIRHEISTÄ ILMOITTAMINEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "GNU diffutils home page: E<lt>https://www.gnu.org/software/diffutils/E<gt>"
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"GNU diffutils-kotisivu: E<lt>https://www.gnu.org/software/diffutils/E<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Ilmoita käännösvirheistä osoitteeseen E<lt>https://translationproject.org/"
"team/fi.htmlE<gt>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "TEKIJÄNOIKEUDET"

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"Copyright \\(co 2025 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2025 Free Software Foundation, Inc.  Lisenssi GPLv3+: GNU "
"GPL versio 3 tai myöhempi E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Tämä on vapaa ohjelmisto; sitä saa vapaasti muuttaa ja levittää edelleen. "
"Siinä määrin kuin laki sallii, TAKUUTA EI OLE."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "KATSO MYÖS"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid "B<tac>(1)"
msgid "B<install>(1)"
msgstr "B<tac>(1)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Full documentation E<lt>https://www.gnu.org/software/coreutils/cpE<gt>"
msgstr ""
"Koko dokumentaatio: E<lt>https://www.gnu.org/software/coreutils/cpE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "or available locally via: info \\(aq(coreutils) cp invocation\\(aq"
msgstr "tai saatavilla paikallisesti: info \\(aq(coreutils) cp invocation\\(aq"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "September 2022"
msgstr "Syyskuuta 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#. type: Plain text
#: debian-bookworm
msgid "do not overwrite an existing file (overrides a previous B<-i> option)"
msgstr ""
"älä ylikirjoita olemassa olevaa tiedostoa (kumoaa aiemman B<-i>-valitsimen)"

#. type: Plain text
#: debian-bookworm
msgid ""
"preserve the specified attributes (default: mode,ownership,timestamps), if "
"possible additional attributes: context, links, xattr, all"
msgstr ""
"säilytä annetut ominaisuudet (oletus: mode,ownership,timestamps), jos "
"mahdollista, lisäominaisuudet: context, links, xattr, all"

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<-u>, B<--update>"
msgstr "B<-u>, B<--update>"

#. type: Plain text
#: debian-bookworm
msgid ""
"copy only when the SOURCE file is newer than the destination file or when "
"the destination file is missing"
msgstr ""
"kopioi vain jos LÄHDEtiedosto on uudempi kuin kohde, tai jos kohdetiedosto "
"puuttuu"

#. type: Plain text
#: debian-bookworm
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  Lisenssi GPLv3+: GNU "
"GPL versio 3 tai myöhempi E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: debian-unstable
#, no-wrap
msgid "October 2024"
msgstr "Lokakuuta 2024"

#. type: TH
#: debian-unstable mageia-cauldron
#, no-wrap
msgid "GNU coreutils 9.5"
msgstr "GNU coreutils 9.5"

#. type: Plain text
#: debian-unstable mageia-cauldron
msgid ""
"control which existing files are updated; UPDATE={all,none,none-"
"fail,older(default)}."
msgstr ""

#. type: Plain text
#: debian-unstable mageia-cauldron
msgid ""
"Copyright \\(co 2024 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2024 Free Software Foundation, Inc.  Lisenssi GPLv3+: GNU "
"GPL versio 3 tai myöhempi E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: fedora-42 opensuse-tumbleweed
#, no-wrap
msgid "January 2025"
msgstr "Tammikuuta 2025"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "March 2024"
msgstr "Maaliskuuta 2024"

#. type: TH
#: opensuse-leap-16-0
#, no-wrap
msgid "January 2024"
msgstr "Tammikuuta 2024"

#. type: TH
#: opensuse-leap-16-0
#, no-wrap
msgid "GNU coreutils 9.4"
msgstr "GNU coreutils 9.4"

#. type: Plain text
#: opensuse-leap-16-0
#, fuzzy
#| msgid ""
#| "do not overwrite an existing file (overrides a previous B<-i> option)"
msgid ""
"do not overwrite an existing file (overrides a B<-u> or previous B<-i> "
"option). See also B<--update>"
msgstr ""
"älä ylikirjoita olemassa olevaa tiedostoa (kumoaa aiemman B<-i>-valitsimen)"

#. type: Plain text
#: opensuse-leap-16-0
msgid ""
"control which existing files are updated; UPDATE={all,none,older(default)}.  "
"See below"
msgstr ""

#. type: Plain text
#: opensuse-leap-16-0
msgid "equivalent to B<--update>[=I<\\,older\\/>]"
msgstr ""

#. type: Plain text
#: opensuse-leap-16-0
msgid ""
"UPDATE controls which existing files in the destination are replaced.  "
"\\&'all' is the default operation when an B<--update> option is not "
"specified, and results in all existing files in the destination being "
"replaced.  \\&'none' is similar to the B<--no-clobber> option, in that no "
"files in the destination are replaced, but also skipped files do not induce "
"a failure.  \\&'older' is the default operation when B<--update> is "
"specified, and results in files being replaced if they're older than the "
"corresponding source file."
msgstr ""

#. type: Plain text
#: opensuse-leap-16-0
msgid ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  Lisenssi GPLv3+: GNU "
"GPL versio 3 tai myöhempi E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
