# Brazilian Portuguese translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Paulo César Mendes <drpc@ism.com.br>, 2000.
# André Luiz Fassone <lonely_wolf@ig.com.br>, 2000.
# Rafael Fontenelle <rafaelff@gnome.org>, 2020-2024.
#
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2025-02-28 16:30+0100\n"
"PO-Revision-Date: 2024-09-28 15:23-0300\n"
"Last-Translator: Rafael Fontenelle <rafaelff@gnome.org>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-"
"portuguese@lists.debian.org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1)\n"
"X-Generator: Gtranslator 46.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "clock"
msgstr "clock"

#. type: TH
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "2024-07-23"
msgstr "23 julho 2024"

#. type: TH
#: archlinux
#, fuzzy, no-wrap
#| msgid "Linux man-pages 6.03"
msgid "Linux man-pages 6.12"
msgstr "Linux man-pages 6.03"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "clock - determine processor time"
msgstr "clock - retorna o tempo de CPU"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux opensuse-tumbleweed
#, fuzzy
#| msgid "Standard C library (I<libc>, I<-lc>)"
msgid "Standard C library (I<libc>,\\ I<-lc>)"
msgstr "Biblioteca C Padrão (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>time.hE<gt>>\n"
msgstr "B<#include E<lt>time.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<clock_t clock(void);>\n"
msgstr "B<clock_t clock(void);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIÇÃO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<clock>()  function returns an approximation of processor time used by "
"the program."
msgstr ""
"A função B<clock>() retorna uma aproximação do tempo de processamento usado "
"pelo programa."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOR DE RETORNO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The value returned is the CPU time used so far as a I<clock_t>; to get the "
"number of seconds used, divide by B<CLOCKS_PER_SEC>.  If the processor time "
"used is not available or its value cannot be represented, the function "
"returns the value I<(clock_t)\\ -1>."
msgstr ""
"O valor retornado é o tempo de CPU usado até o momento como um B<clock_t>. "
"Para obter o número de segundos, divida por B<CLOCKS_PER_SEC>. Se o tempo de "
"processador não estiver disponível ou o valor não puder ser representado, a "
"função retorna I<(clock_t)\\ -1>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRIBUTOS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Para uma explicação dos termos usados nesta seção, consulte B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interface"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atributo"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valor"

#. type: tbl table
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<clock>()"
msgstr "B<clock>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Thread safety"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERSÕES"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"XSI requires that B<CLOCKS_PER_SEC> equals 1000000 independent of the actual "
"resolution."
msgstr ""
"XSI exige que B<CLOCKS_PER_SEC> seja 1000000 independentemente da resolução "
"verdadeira."

#.  I have seen this behavior on Irix 6.3, and the OSF/1, HP/UX, and
#.  Solaris manual pages say that clock() also does this on those systems.
#.  POSIX.1-2001 doesn't explicitly allow this, nor is there an
#.  explicit prohibition. -- MTK
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On several other implementations, the value returned by B<clock>()  also "
"includes the times of any children whose status has been collected via "
"B<wait>(2)  (or another wait-type call).  Linux does not include the times "
"of waited-for children in the value returned by B<clock>().  The "
"B<times>(2)  function, which explicitly returns (separate) information about "
"the caller and its children, may be preferable."
msgstr ""
"Em várias outras implementações, o valor retornado por B<clock>() também "
"inclui os horários de todos os filhos cujo status foi coletado por "
"B<wait>(2) (ou outra chamada do tipo espera). O Linux não inclui os tempos "
"de filhos esperados no valor retornado por B<clock>(). A função B<times>(2), "
"que retorna explicitamente informações (separadas) sobre o chamador e seus "
"filhos, pode ser preferível."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "PADRÕES"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr "C11, POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTÓRICO"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2001, C89."
msgstr "POSIX.1-2001, C89."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In glibc 2.17 and earlier, B<clock>()  was implemented on top of "
"B<times>(2).  For improved accuracy, since glibc 2.18, it is implemented on "
"top of B<clock_gettime>(2)  (using the B<CLOCK_PROCESS_CPUTIME_ID> clock)."
msgstr ""
"No glibc 2.17 e versões anteriores, B<clock>() estava implementado usando "
"B<times>(2). Para maior precisão, desde o glibc 2.18, ele é implementado "
"sobre B<clock_gettime>(2) (usando o relógio B<CLOCK_PROCESS_CPUTIME_ID>)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTAS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The C standard allows for arbitrary values at the start of the program; "
"subtract the value returned from a call to B<clock>()  at the start of the "
"program to get maximum portability."
msgstr ""
"O padrão C permite valores arbitrários no início do programa. Use a "
"diferença entre uma chamada a B<clock>() no início e outra no final do "
"programa para obter portabilidade máxima."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Note that the time can wrap around.  On a 32-bit system where "
"B<CLOCKS_PER_SEC> equals 1000000 this function will return the same value "
"approximately every 72 minutes."
msgstr ""
"O tempo pode sofrer 'overflow'. Num sistema de 32 bits com B<CLOCKS_PER_SEC> "
"igual a 1000000, esta função retornará um valor idêntico aproximadamente a "
"cada 72 minutos."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VEJA TAMBÉM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<clock_gettime>(2), B<getrusage>(2), B<times>(2)"
msgstr "B<clock_gettime>(2), B<getrusage>(2), B<times>(2)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-12-29"
msgstr "29 dezembro 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca C Padrão (I<libc>, I<-lc>)"

#. type: Plain text
#: debian-bookworm
msgid ""
"POSIX.1-2001, POSIX.1-2008, C99.  XSI requires that B<CLOCKS_PER_SEC> equals "
"1000000 independent of the actual resolution."
msgstr ""
"POSIX.1-2001, POSIX.1-2008, C99. XSI exige que B<CLOCKS_PER_SEC> seja "
"1000000 independente da resolução verdadeira."

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "2024-05-02"
msgstr "2 maio 2024"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Linux man-pages 6.9.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 outubro 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Linux man-pages (não lançado)"
