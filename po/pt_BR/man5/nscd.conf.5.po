# Brazilian Portuguese translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Ricardo C.O.Freitas <english.quest@best-service.com>, 2001.
# André Luiz Fassone <lonely_wolf@ig.com.br>, 2001.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2025-02-16 05:53+0100\n"
"PO-Revision-Date: 2000-05-31 17:26+0200\n"
"Last-Translator: André Luiz Fassone <lonely_wolf@ig.com.br>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-"
"portuguese@lists.debian.org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 20.04.1\n"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "NSCD.CONF"
msgstr "NSCD.CONF"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "07 January 2001"
msgstr "7 de janeiro de 2001"

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOME"

#. type: Plain text
#: debian-bookworm debian-unstable
#, fuzzy
msgid "nscd.conf - configuration file for Name Service Caching Daemon"
msgstr ""
"nscd.conf - arquivo de configuração do 'cache' de nomes de serviços 'daemon'"

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIÇÃO"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"I</etc/nscd.conf> configures the caches used by B<nscd>(8)  as well as some "
"generic options.  B<nscd>(8)  is able to use a configuration file at a "
"different location, when supplied with the I<-f> or I<--config-file> command "
"line option."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The configuration file consists of a set of lines.  Empty lines, and text "
"after a '#' is ignored.  All remaining lines denote the setting of an "
"option.  White space before and after options, and between options and "
"option arguments is ignored."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"There are two kinds of options: General options influence B<nscd>(8)'s "
"general behaviour, while cache related options only affect the specified "
"cache. Options are set like this:"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
#, no-wrap
msgid ""
"  general_option option\n"
"  cache_option cache_name option\n"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable
#, fuzzy, no-wrap
#| msgid "OPTIONS"
msgid "GENERAL OPTIONS"
msgstr "OPÇÕES"

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<logfile>I<\\ file>"
msgstr "B<logfile>I<\\ arquivo>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Specifies the name of the debug log-file that B<nscd>(8)  should use if "
"B<debug-level> is higher than B<0>.  If this option is not set, B<nscd>(8)  "
"will write its debug output to stderr."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, fuzzy, no-wrap
#| msgid "B<debug-level> I<value>"
msgid "B<debug-level>I<\\ level>"
msgstr "B<debug-level> I<valor>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"If I<level> is higher than B<0>, B<nscd>(8)  will create some debug output. "
"The higher the level, the more verbose the output."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, fuzzy, no-wrap
#| msgid "B<threads> I<number>"
msgid "B<threads>I<\\ #threads>"
msgstr "B<threads> I<número>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"This option sets the number of threads that B<nscd>(8)  should use by "
"default. It can be overridden by calling B<nscd>(8)  with the I<-t> or I<--"
"nthreads> argument. If neither this configuration option nor the command "
"line argument is given, B<nscd>(8)  uses 5 threads by default. The minimum "
"is 3. More threads means more simultaneous connections that B<nscd>(8)  can "
"handle."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<max-threads>I<\\ #threads>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "Specifies the maximum number of threads to be started."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<server-user>I<\\ user>"
msgstr "B<server-user>I<\\ usuário>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"By default, B<nscd>(8)  is run as user root. This option can be set to force "
"B<nscd>(8)  to drop root privileges after startup. It cannot be used when "
"B<nscd>(8)  is called with the I<-S> or I<--secure> argument. Also note that "
"some services require that nscd run as root, so using this may break those "
"lookup services."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<stat-user>I<\\ user>"
msgstr "B<stat-user>I<\\ usuário>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Specifies the user who is allowed to request statistics."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<paranoia>I<\\ bool>"
msgstr "B<paranoia>I<\\ bool>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"I<bool> must be one of B<yes> or B<no>.  Enabling paranoia mode causes "
"B<nscd>(8)  to restart itself periodically."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<restart-interval>I<\\ time>"
msgstr "B<restart-interval>I<\\ tempo>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Sets the restart interval to time seconds if periodic restart is enabled by "
"enabling paranoia mode. The default value is 3600."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable
#, fuzzy, no-wrap
#| msgid "OPTIONS"
msgid "CACHE OPTIONS"
msgstr "OPÇÕES"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"All cache options take two arguments. The first one denotes the service or "
"cache the option should affect. Currently I<service> can be one of "
"B<passwd>, B<group>, or B<hosts>."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<enable-cache>I<\\ service\\ bool>"
msgstr "B<enable-cache>I<\\ serviço\\ bool>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"I<bool> must be one of B<yes> or B<no>.  Each cache is disabled by default "
"and must be enabled explicitly by setting this options to B<yes>."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, fuzzy, no-wrap
#| msgid "B<positive-time-to-live> I<service> I<value>"
msgid "B<positive-time-to-live>I<\\ service\\ secs>"
msgstr "B<positive-time-to-live>I<serviço> I<valor>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"This is the number of seconds after which a cached entry is removed from the "
"cache. This defaults to 3600 seconds (i. e.  one hour)."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, fuzzy, no-wrap
#| msgid "B<negative-time-to-live> I<service> I<value>"
msgid "B<negative-time-to-live>I<\\ service\\ secs>"
msgstr "B<negative-time-to-live> I<serviço> I<valor>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"If an entry is not found by the Name Service, it is added to the cache and "
"marked as \"not existent\". This option sets the number of seconds after "
"which such a not existent entry is removed from the cache. This defaults to "
"20 seconds for the B<password> and B<host> caches and to 60 seconds for the "
"B<group> cache."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, fuzzy, no-wrap
#| msgid "B<suggested-size> I<service> I<value>"
msgid "B<suggested-size>I<\\ service\\ prime-number>"
msgstr "B<suggested-size> I<serviço> I<valor>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"This option sets the size of the hash that is used to store the cache "
"entries. As this is a hash, it should be reasonably larger than the maximum "
"number of entries that is expected to be cached simultaneously and should be "
"a prime number. It defaults to a size of 211 entries."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<check-files>I<\\ service\\ bool>"
msgstr "B<check-files>I<\\ serviço\\ bool>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"I<bool> must be one of B<yes> (default) or B<no>.  If file checking is "
"enabled, B<nscd>(8)  periodically checks the modification time of I</etc/"
"passwd>, I</etc/group>, or I</etc/hosts> (for the B<passwd>, B<group>, and "
"B<host> cache respectively)  and invalidates the cache if the file has "
"changed since the last check."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<persistent>I<\\ service\\ bool>"
msgstr "B<persistent>I<\\ serviço\\ bool>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"I<bool> must be one of B<yes> (default) or B<no>.  Keep the content of the "
"cache for service over B<nscd>(8)  restarts. Useful when paranoia mode is "
"set."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<shared>I<\\ service\\ bool>"
msgstr "B<shared>I<\\ serviço\\ bool>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"I<bool> must be one of B<yes> (default) or B<no>.  The memory mapping of the "
"B<nscd>(8)  databases for service is shared with the clients so that they "
"can directly search in them instead of having to ask the daemon over the "
"socket each time a lookup is performed."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, fuzzy, no-wrap
#| msgid "B<threads> I<number>"
msgid "B<reload-count>I<\\ #number>"
msgstr "B<threads> I<número>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Sets the number of times a cached record is reloaded before it is pruned "
"from the cache. Each cache record has a timeout, when that timeout expires, "
"B<nscd>(8)  will either reload it (query the NSS service again if the data "
"hasn't changed), or drop it."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<max-db-size>I<\\ service\\ number-of-bytes>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "Sets the maximum allowable size for the service."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<auto-propagate>I<\\ service\\ bool>"
msgstr "B<auto-propagate>I<\\ serviço\\ bool>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"When set to no for passwd or group service, then the I<.byname> requests are "
"not added to I<passwd.byuid> or I<group.bygid> cache. This may help for "
"tables containing multiple records for the same id."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "EXAMPLE"
msgstr "EXEMPLO"

#. type: Plain text
#: debian-bookworm debian-unstable
#, no-wrap
msgid "# This is a comment.\n"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
#, no-wrap
msgid ""
"    logfile                 /var/log/nscd.log\n"
"    threads                 6\n"
"    server-user             nobody\n"
"    debug-level             0\n"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
#, no-wrap
msgid ""
"    enable-cache            passwd          yes\n"
"    positive-time-to-live   passwd          600\n"
"    negative-time-to-live   passwd          20\n"
"    suggested-size          passwd          211\n"
"    check-files             passwd          yes\n"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
#, no-wrap
msgid ""
"    enable-cache            group           yes\n"
"    positive-time-to-live   group           3600\n"
"    negative-time-to-live   group           60\n"
"    suggested-size          group           211\n"
"    check-files             group           yes\n"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
#, no-wrap
msgid ""
"    enable-cache            hosts           yes\n"
"    positive-time-to-live   hosts           3600\n"
"    negative-time-to-live   hosts           20\n"
"    suggested-size          hosts           211\n"
"    check-files             hosts           yes\n"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VEJA TAMBÉM"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "nscd(8), nsswitch.conf(5)"
msgstr "nscd(8), nsswitch.conf(5)"

#. type: TH
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "nscd.conf"
msgstr "nscd.conf"

#. type: TH
#: fedora-42 fedora-rawhide opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2 maio 2024"

#. type: TH
#: fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Linux man-pages 6.9.1"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "nscd.conf - name service cache daemon configuration file"
msgstr ""
"nscd.conf - arquivo de configuração do 'cache' de nomes de serviços 'daemon'"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The file B</etc/nscd.conf> is read from B<nscd>(8)  at startup. Each line "
#| "specifies either an attribute and a value, or an attribute, service, and "
#| "a value. Fields are separated either by SPACE or TAB characters. A `#' "
#| "(number sign) indicates the beginning of a comment; characters up to the "
#| "end of the line are not interpreted by nscd."
msgid ""
"The file I</etc/nscd.conf> is read from B<nscd>(8)  at startup.  Each line "
"specifies either an attribute and a value, or an attribute, service, and a "
"value.  Fields are separated either by SPACE or TAB characters.  A \\[aq]#\\"
"[aq] (number sign) indicates the beginning of a comment; following "
"characters, up to the end of the line, are not interpreted by nscd."
msgstr ""
"O arquivo B</etc/nscd.conf> é lido à partir de B<nscd>(8) na inicialização. "
"Cada linha especifica, tanto um atributo e um valor, ou um atributo, "
"serviço, e um valor. Campos são separados tanto por caracteres ESPAÇO ou "
"TAB. Um `#' (símbolo de número) indica o inicio de um comentário; caracteres "
"até o fim da linha não são interpretados por nscd."

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Valid services are I<passwd>, I<group>, I<hosts>, I<services>, or "
"I<netgroup>."
msgstr ""
"Serviços válidos são I<passwd>, I<group>, I<hosts>, I<services> ou "
"I<netgroup>."

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<logfile> I<debug-file-name>"
msgstr "B<logfile> I<nome-arquivo-debug>"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Specifies name of the file to which debug info should be written."
msgstr ""
"Especifica nome do arquivo, para o qual a informação 'debug' deverá ser "
"escrita."

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<debug-level> I<value>"
msgstr "B<debug-level> I<valor>"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Sets the desired debug level.  0 hides debug info.  1 shows general debug "
"info.  2 additionally shows data in cache dumps.  3 (and above) shows all "
"debug info.  The default is 0."
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<threads> I<number>"
msgstr "B<threads> I<número>"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "This is the number of how many threads are started and waiting for "
#| "requests. At least five threads will always be created."
msgid ""
"This is the initial number of threads that are started to wait for "
"requests.  At least five threads will always be created.  The number of "
"threads may increase dynamically up to B<max-threads> in response to demand "
"from clients, but never decreases."
msgstr ""
"Este é o múmero de quantas instâncias são iniciadas e aguardando "
"solicitações. Pelo menos, cinco instâncias sempre serão criadas."

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<max-threads> I<number>"
msgstr "B<max-threads> I<número>"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Specifies the maximum number of threads.  The default is 32."
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<server-user> I<user>"
msgstr "B<server-user> I<usuário>"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"If this option is set, nscd will run as this user and not as root.  If a "
"separate cache for every user is used (-S parameter), this option is ignored."
msgstr ""
"Se esta opção é escolhida, nscd iniciará como este usuário e não como root. "
"Se um 'cache' for utilizado para dada usuário (parâmetro -S), esta opção é "
"ignorada."

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<stat-user> I<user>"
msgstr "B<stat-user> I<usuário>"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<reload-count> unlimited | I<number>"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Sets a limit on the number of times a cached entry gets reloaded without "
"being used before it gets removed.  The limit can take values ranging from 0 "
"to 254; values 255 or higher behave the same as B<unlimited>.  Limit values "
"can be specified in either decimal or hexadecimal with a \"0x\" prefix.  The "
"special value B<unlimited> is case-insensitive.  The default limit is 5.  A "
"limit of 0 turns off the reloading feature.  See NOTES below for further "
"discussion of reloading."
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<paranoia> I<E<lt>yes|noE<gt>>"
msgstr "B<paranoia> I<E<lt>yes|noE<gt>>"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Enabling paranoia mode causes nscd to restart itself periodically.  The "
"default is no."
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<restart-interval> I<time>"
msgstr "B<restart-interval> I<tempo>"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Sets the restart interval to I<time> seconds if periodic restart is enabled "
"by enabling B<paranoia> mode.  The default is 3600."
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<enable-cache> I<service> I<E<lt>yes|noE<gt>>"
msgstr "B<enable-cache> I<serviço> I<E<lt>yes|noE<gt>>"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, fuzzy
#| msgid "Enables or disables the specified I<service> cache."
msgid "Enables or disables the specified I<service> cache.  The default is no."
msgstr "habilita ou desabilita o especificado I<serviço> 'cache'."

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<positive-time-to-live> I<service> I<value>"
msgstr "B<positive-time-to-live> I<serviço> I<valor>"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Sets the TTL (time-to-live) for positive entries (successful queries)  in "
#| "the specified cache for I<service>.  I<Value> is in seconds.  Larger "
#| "values increase cache hit rates and reduce mean response times, but "
#| "increase problems with cache coherence."
msgid ""
"Sets the TTL (time-to-live) for positive entries (successful queries)  in "
"the specified cache for I<service>.  I<Value> is in seconds.  Larger values "
"increase cache hit rates and reduce mean response times, but increase "
"problems with cache coherence.  Note that for some name services (including "
"specifically DNS)  the TTL returned from the name service is used and this "
"attribute is ignored."
msgstr ""
"Fixa o TTL (tempo de vida) para entradas positivas (pesquisa bem sucedida) "
"em um 'cache' especifico para um I<serviço>. I<Valor> está em segundos. "
"Valores maiores aumentam o índice de entradas no 'cache' e reduz o tempo de "
"resposta, mas aumenta os problemas com a coerência do 'cache'."

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<negative-time-to-live> I<service> I<value>"
msgstr "B<negative-time-to-live> I<serviço> I<valor>"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Sets the TTL (time-to-live) for negative entries (unsuccessful queries)  in "
"the specified cache for I<service>.  I<Value> is in seconds.  Can result in "
"significant performance improvements if there are several files owned by "
"UIDs (user IDs) not in system databases (for example untarring the Linux "
"kernel sources as root); should be kept small to reduce cache coherency "
"problems."
msgstr ""
"Fixa o TTL (tempo de vida) para entradas negativas (pesquisas mal sucedidas) "
"em um 'cache' específico para um I<serviço>. I<Valor> está em segundos. Pode "
"resultar em significantes melhorias de performance se houverem diversos "
"arquivos pertencentes à UIDs (user IDs) que não estejam no banco de dados "
"(por exemplo descompactando (untarring) o fonte do kernel Linux como root); "
"deveriam ser mantidos pequenos para reduzir os problemas de coerência do "
"'cache'."

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<suggested-size> I<service> I<value>"
msgstr "B<suggested-size> I<serviço> I<valor>"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "This is the internal hash table size, I<value> should remain a prime "
#| "number for optimum efficiency."
msgid ""
"This is the internal hash table size, I<value> should remain a prime number "
"for optimum efficiency.  The default is 211."
msgstr ""
"Este é o tamanho da tabela 'hash' interna, I<valor> deveria permanecer como "
"um número primo, para uma eficiência ótima."

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<check-files> I<service> I<E<lt>yes|noE<gt>>"
msgstr "B<check-files> I<serviço> I<E<lt>yes|noE<gt>>"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Enables or disables checking the file belonging to the specified "
#| "I<service> for changes. The files are I</etc/passwd>, I</etc/group> and "
#| "I</etc/hosts>."
msgid ""
"Enables or disables checking the file belonging to the specified I<service> "
"for changes.  The files are I</etc/passwd>, I</etc/group>, I</etc/hosts>, I</"
"etc/resolv.conf>, I</etc/services>, and I</etc/netgroup>.  The default is "
"yes."
msgstr ""
"Habilita ou desabilita a verificação do arquivo pertencente ao I<service> "
"especificado para alterações. Os arquivos são I</etc/passwd>, I</etc/group> "
"e I</etc/hosts>."

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<persistent> I<service> I<E<lt>yes|noE<gt>>"
msgstr "B<persistent> I<serviço> I<E<lt>yes|noE<gt>>"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Keep the content of the cache for I<service> over server restarts; useful "
"when B<paranoia> mode is set.  The default is no."
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<shared> I<service> I<E<lt>yes|noE<gt>>"
msgstr "B<shared> I<serviço> I<E<lt>yes|noE<gt>>"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"The memory mapping of the nscd databases for I<service> is shared with the "
"clients so that they can directly search in them instead of having to ask "
"the daemon over the socket each time a lookup is performed.  The default is "
"no.  Note that a cache miss will still result in asking the daemon over the "
"socket."
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, fuzzy
#| msgid "B<suggested-size> I<service> I<value>"
msgid "B<max-db-size> I<service> I<bytes>"
msgstr "B<suggested-size> I<serviço> I<valor>"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"The maximum allowable size, in bytes, of the database files for the "
"I<service>.  The default is 33554432."
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<auto-propagate> I<service> I<E<lt>yes|noE<gt>>"
msgstr "B<auto-propagate> I<serviço> I<E<lt>yes|noE<gt>>"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"When set to I<no> for I<passwd> or I<group> service, then the I<.byname> "
"requests are not added to I<passwd.byuid> or I<group.bygid> cache.  This can "
"help with tables containing multiple records for the same ID.  The default "
"is yes.  This option is valid only for services I<passwd> and I<group>."
msgstr ""

#. type: SH
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTAS"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"The default values stated in this manual page originate from the source code "
"of B<nscd>(8)  and are used if not overridden in the configuration file.  "
"The default values used in the configuration file of your distribution might "
"differ."
msgstr ""

#. type: SS
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "Reloading"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"B<nscd>(8)  has a feature called reloading, whose behavior can be surprising."
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Reloading is enabled when the B<reload-count> attribute has a non-zero "
"value.  The default value in the source code enables reloading, although "
"your distribution may differ."
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"When reloading is enabled, positive cached entries (the results of "
"successful queries)  do not simply expire when their TTL is up.  Instead, at "
"the expiry time, B<nscd> will \"reload\", i.e., re-issue to the name service "
"the same query that created the cached entry, to get a new value to cache.  "
"Depending on I</etc/nsswitch.conf> this may mean that a DNS, LDAP, or NIS "
"request is made.  If the new query is successful, reloading will repeat when "
"the new value would expire, until B<reload-count> reloads have happened for "
"the entry, and only then will it actually be removed from the cache.  A "
"request from a client which hits the entry will reset the reload counter on "
"the entry.  Purging the cache using I<nscd\\ -i> overrides the reload logic "
"and removes the entry."
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Reloading has the effect of extending cache entry TTLs without compromising "
"on cache coherency, at the cost of additional load on the backing name "
"service.  Whether this is a good idea on your system depends on details of "
"your applications' behavior, your name service, and the effective TTL values "
"of your cache entries.  Note that for some name services (for example, DNS), "
"the effective TTL is the value returned from the name service and I<not> the "
"value of the B<positive-time-to-live> attribute."
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Please consider the following advice carefully:"
msgstr ""

#. type: IP
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr "\\[bu]"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"If your application will make a second request for the same name, after more "
"than 1 TTL but before B<reload-count> TTLs, and is sensitive to the latency "
"of a cache miss, then reloading may be a good idea for you."
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"If your name service is configured to return very short TTLs, and your "
"applications only make requests rarely under normal circumstances, then "
"reloading may result in additional load on your backing name service without "
"any benefit to applications, which is probably a bad idea for you."
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"If your name service capacity is limited, reloading may have the surprising "
"effect of increasing load on your name service instead of reducing it, and "
"may be a bad idea for you."
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Setting B<reload-count> to B<unlimited> is almost never a good idea, as it "
"will result in a cache that never expires entries and puts never-ending "
"additional load on the backing name service."
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Some distributions have an init script for B<nscd>(8)  with a I<reload> "
"command which uses I<nscd\\ -i> to purge the cache.  That use of the word "
"\"reload\" is entirely different from the \"reloading\" described here."
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<nscd>(8)"
msgstr "B<nscd>(8)"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 outubro 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Linux man-pages (não lançado)"
