# Brazilian Portuguese translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# André Luiz Fassone <lonely_wolf@ig.com.br>, 2000.
# Ricardo C.O.Freitas <english.quest@best-service.com>, 2000.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2025-02-28 16:37+0100\n"
"PO-Revision-Date: 2000-06-02 19:20-0300\n"
"Last-Translator: Ricardo C.O.Freitas <english.quest@best-service.com>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-"
"portuguese@lists.debian.org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Virtaal 1.0.0-beta1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ipc"
msgstr "ipc"

#. type: TH
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "2024-07-23"
msgstr "23 julho 2024"

#. type: TH
#: archlinux
#, fuzzy, no-wrap
#| msgid "Linux man-pages 6.03"
msgid "Linux man-pages 6.12"
msgstr "Linux man-pages 6.03"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "ipc - System V IPC system calls"
msgstr "ipc - chamadas de sistema IPC do System V"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux opensuse-tumbleweed
#, fuzzy
#| msgid "Standard C library (I<libc>, I<-lc>)"
msgid "Standard C library (I<libc>,\\ I<-lc>)"
msgstr "Biblioteca C Padrão (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>linux/ipc.hE<gt>>        /* Definition of needed constants */\n"
"B<#include E<lt>sys/syscall.hE<gt>>      /* Definition of B<SYS_*> constants */\n"
"B<#include E<lt>unistd.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<int ipc(unsigned int>I< call, >B<int>I< first, >B<int>I< second, > B<int>I< third, >B<void *>I<ptr, >B<long>I< fifth);>"
msgid ""
"B<int syscall(SYS_ipc, unsigned int >I<call>B<, int >I<first>B<,>\n"
"B<            unsigned long >I<second>B<, unsigned long >I<third>B<, void *>I<ptr>B<,>\n"
"B<            long >I<fifth>B<);>\n"
msgstr "B<int ipc(unsigned int>I< call, >B<int>I< first, >B<int>I< second, > B<int>I< third, >B<void *>I<ptr, >B<long>I< fifth);>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<Note>: glibc provides no wrapper for B<ipc>(), necessitating the use of "
"B<syscall>(2)."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIÇÃO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<ipc>()  is a common kernel entry point for the System\\ V IPC calls for "
"messages, semaphores, and shared memory.  I<call> determines which IPC "
"function to invoke; the other arguments are passed through to the "
"appropriate call."
msgstr ""
"B<ipc>() é um ponto comum de entrada no kernel para as chamada IPC do System "
"V\\ para mensagens, sinalizadores e memória compartilhada. I<call> determina "
"qual função IPC chamar; os outros argumentos são passados através da chamada "
"apropriada."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "User programs should call the appropriate functions by their usual "
#| "names.  Only standard library implementors and kernel hackers need to "
#| "know about B<ipc>."
msgid ""
"User-space programs should call the appropriate functions by their usual "
"names.  Only standard library implementors and kernel hackers need to know "
"about B<ipc>()."
msgstr ""
"Programas de usuários podem chamar funções apropriadas por seus nomes "
"usuáis. Somente bibliotecas padrão de implementadores e 'hackers' do kernel "
"precisam conhecer sobre B<ipc>."

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERSÕES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On some architectures\\[em]for example x86-64 and ARM\\[em]there is no "
"B<ipc>()  system call; instead, B<msgctl>(2), B<semctl>(2), B<shmctl>(2), "
"and so on really are implemented as separate system calls."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "PADRÕES"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Linux."
msgstr "Linux."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VEJA TAMBÉM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<msgctl>(2), B<msgget>(2), B<msgrcv>(2), B<msgsnd>(2), B<semctl>(2), "
#| "B<semget>(2), B<semop>(2), B<shmat>(2), B<shmctl>(2), B<shmdt>(2), "
#| "B<shmget>(2)"
msgid ""
"B<msgctl>(2), B<msgget>(2), B<msgrcv>(2), B<msgsnd>(2), B<semctl>(2), "
"B<semget>(2), B<semop>(2), B<semtimedop>(2), B<shmat>(2), B<shmctl>(2), "
"B<shmdt>(2), B<shmget>(2), B<sysvipc>(7)"
msgstr ""
"B<msgctl>(2), B<msgget>(2), B<msgrcv>(2), B<msgsnd>(2), B<semctl>(2), "
"B<semget>(2), B<semop>(2), B<semtimedop>(2), B<shmat>(2), B<shmctl>(2), "
"B<shmdt>(2), B<shmget>(2), B<sysvipc>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 fevereiro 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca C Padrão (I<libc>, I<-lc>)"

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid ""
#| "B<ipc> is Linux specific, and should not be used in programs intended to "
#| "be portable."
msgid ""
"B<ipc>()  is Linux-specific, and should not be used in programs intended to "
"be portable."
msgstr ""
"B<ipc>() é específca do Linux, e não deveria ser usada em programas que "
"pretendam ser portáveis."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NOTES"
msgstr "NOTAS"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "2024-05-02"
msgstr "2 maio 2024"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Linux man-pages 6.9.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 outubro 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Linux man-pages (não lançado)"
