# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Artyom Kunyov <artkun@guitarplayer.ru>, 2012.
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2012, 2017.
# Dmitry Bolkhovskikh <d20052005@yandex.ru>, 2017.
# Katrin Kutepova <blackkatelv@gmail.com>, 2018.
# Konstantin Shvaykovskiy <kot.shv@gmail.com>, 2012.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017, 2019.
# Kirill Rekhov <krekhov.dev@gmail.com>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2025-02-28 16:38+0100\n"
"PO-Revision-Date: 2024-12-21 19:50+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <debian-l10n-russian@lists.debian.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "glibc"
msgid "libc"
msgstr "glibc"

#. type: TH
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2 мая 2024 г."

#. type: TH
#: archlinux
#, fuzzy, no-wrap
#| msgid "Linux man-pages 6.03"
msgid "Linux man-pages 6.12"
msgstr "Справочные страницы Linux 6.03"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "НАИМЕНОВАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "libc - overview of standard C libraries on Linux"
msgstr "libc - обзор стандартных библиотек C в Linux"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The term \"libc\" is commonly used as a shorthand for the \"standard C "
#| "library\", a library of standard functions that can be used by all C "
#| "programs (and sometimes by programs in other languages).  Because of some "
#| "history (see below), use of the term \"libc\" to refer to the standard C "
#| "library is somewhat ambiguous on Linux."
msgid ""
"The term \\[lq]libc\\[rq] is commonly used as a shorthand for the \\"
"[lq]standard C library\\[rq] a library of standard functions that can be "
"used by all C programs (and sometimes by programs in other languages).  "
"Because of some history (see below), use of the term \\[lq]libc\\[rq] to "
"refer to the standard C library is somewhat ambiguous on Linux."
msgstr ""
"Термин «libc» обычно используется как сокращение обозначения «стандартной "
"библиотеки C» — библиотеки стандартных функций, которые могут использоваться "
"всеми программами, написанными на C (и, иногда, программами, написанными на "
"других языках). Из-за некоторых исторических событий (см. ниже), "
"использование термина «libc» как обозначение стандартной библиотеки С в "
"Linux, несколько некорректно."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "glibc"
msgstr "glibc"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "By far the most widely used C library on Linux is the GNU C Library E<.UR "
#| "http://www.gnu.org\\:/software\\:/libc/> E<.UE ,> often referred to as "
#| "I<glibc>.  This is the C library that is nowadays used in all major Linux "
#| "distributions.  It is also the C library whose details are documented in "
#| "the relevant pages of the I<man-pages> project (primarily in Section 3 of "
#| "the manual).  Documentation of glibc is also available in the glibc "
#| "manual, available via the command I<info libc>.  Release 1.0 of glibc was "
#| "made in September 1992.  (There were earlier 0.x releases.)  The next "
#| "major release of glibc was 2.0, at the beginning of 1997."
msgid ""
"By far the most widely used C library on Linux is the E<.UR http://"
"www.gnu.org\\:/software\\:/libc/> GNU C Library E<.UE ,> often referred to "
"as I<glibc>.  This is the C library that is nowadays used in all major Linux "
"distributions.  It is also the C library whose details are documented in the "
"relevant pages of the I<man-pages> project (primarily in Section 3 of the "
"manual).  Documentation of glibc is also available in the glibc manual, "
"available via the command I<info libc>.  Release 1.0 of glibc was made in "
"September 1992.  (There were earlier 0.x releases.)  The next major release "
"of glibc was 2.0, at the beginning of 1997."
msgstr ""
"Вне всяких сомнений, наиболее широко используемой в Linux библиотекой C "
"является библиотека GNU C E<.UR http://www.gnu.org\\:/software\\:/libc/> "
"E<.UE ,>(I<http://wwwgnuorg/software/libc/>) часто упоминаемая как I<glibc>. "
"В настоящее время данная библиотека используется во всех основных "
"дистрибутивах Linux. Также эта библиотека описана в соответствующих "
"справочных страницах проекта I<man-pages> (в основном, в разделе 3). "
"Документация к glibc также доступна в руководстве glibc, доступном по "
"команде I<info libc>. Выпуск 1.0 glibc состоялся в сентябре 1992 года (до "
"этого было несколько выпусков 0.x). Следующий большой выпуск glibc версии "
"2.0 состоялся в начале 1997 года."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The pathname I</lib/libc.so.6> (or something similar) is normally a "
#| "symbolic link that points to the location of the glibc library, and "
#| "executing this pathname will cause glibc to display various information "
#| "about the version installed on your system."
msgid ""
"The pathname I</lib/libc.so.6> (or something similar)  is normally a "
"symbolic link that points to the location of the glibc library, and "
"executing this pathname will cause glibc to display various information "
"about the version installed on your system."
msgstr ""
"Путь I</lib/libc.so.6> (или подобный), обычно является символической ссылкой "
"на расположение библиотеки glibc, а выполнение этого пути приводит к "
"отображению различной информации о glibc, установленной в системе."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux libc"
msgstr "Linux libc"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "In the early to mid 1990s, there was for a while I<Linux libc>, a fork of "
#| "glibc 1.x created by Linux developers who felt that glibc development at "
#| "the time was not sufficing for the needs of Linux.  Often, this library "
#| "was referred to (ambiguously) as just \"libc\".  Linux libc released "
#| "major versions 2, 3, 4, and 5, as well as many minor versions of those "
#| "releases.  Linux libc4 was the last version to use the a.out binary "
#| "format, and the first version to provide (primitive) shared library "
#| "support.  Linux libc 5 was the first version to support the ELF binary "
#| "format; this version used the shared library soname I<libc.so.5>.  For a "
#| "while, Linux libc was the standard C library in many Linux distributions."
msgid ""
"In the early to mid 1990s, there was for a while I<Linux libc>, a fork of "
"glibc 1.x created by Linux developers who felt that glibc development at the "
"time was not sufficing for the needs of Linux.  Often, this library was "
"referred to (ambiguously) as just \\[lq]libc\\[rq].  Linux libc released "
"major versions 2, 3, 4, and 5, as well as many minor versions of those "
"releases.  Linux libc4 was the last version to use the a.out binary format, "
"and the first version to provide (primitive) shared library support.  Linux "
"libc 5 was the first version to support the ELF binary format; this version "
"used the shared library soname I<libc.so.5>.  For a while, Linux libc was "
"the standard C library in many Linux distributions."
msgstr ""
"В первой половине 1990х годов какое-то время существовала I<Linux libc>, "
"ответвление glibc 1.x, созданное разработчиками Linux, которые были не "
"удовлетворены ходом разработки glibc для Linux. Часто, эту библиотеку "
"называли (необоснованно) просто «libc». Было несколько основных версий Linux "
"libc — 2, 3, 4 и 5, а также много промежуточных. Linux libc4 была последней "
"версией, которая использовала формат двоичных файлов a.out и первой версий "
"предоставившей (простейшую) поддержку общих библиотек. Linux libc 5 была "
"первой версией с поддержкой формата двоичных файлов ELF; в этой версии "
"использовалась общая библиотека с soname I<libc.so.5>. Некоторое время Linux "
"libc была стандартной библиотекой Си во многих дистрибутивах Linux."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"However, notwithstanding the original motivations of the Linux libc effort, "
"by the time glibc 2.0 was released (in 1997), it was clearly superior to "
"Linux libc, and all major Linux distributions that had been using Linux libc "
"soon switched back to glibc.  To avoid any confusion with Linux libc "
"versions, glibc 2.0 and later used the shared library soname I<libc.so.6>."
msgstr ""
"Однако, несмотря на всю первоначальную заинтересованность в разработке Linux "
"libc, к этому времени была выпущена glibc 2.0 (в 1997 году), которая была "
"однозначно лучше Linux libc, и все основные дистрибутивы Linux, которые "
"использовали Linux libc, скоро переключились обратно на glibc. Чтобы "
"избежать двусмысленности в версиях Linux libc, в glibc 2.0 и последующих "
"версиях в качестве имени общей библиотеки используется soname I<libc.so.6>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Since the switch from Linux libc to glibc 2.0 occurred long ago, I<man-"
"pages> no longer takes care to document Linux libc details.  Nevertheless, "
"the history is visible in vestiges of information about Linux libc that "
"remain in a few manual pages, in particular, references to I<libc4> and "
"I<libc5>."
msgstr ""
"Так как переход с Linux libc на glibc 2.0 произошёл давно, в I<man-pages> "
"Linux libc больше не описывается. Тем не менее частичные упоминания о Linux "
"libc ещё встречаются в некоторых справочных страницах под именами I<libc4> и "
"I<libc5>."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Other C libraries"
msgstr "Другие библиотеки C"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "There are various other less widely used C libraries for Linux.  These "
#| "libraries are generally smaller than glibc, both in terms of features and "
#| "memory footprint, and often intended for building small binaries, perhaps "
#| "targeted at development for embedded Linux systems.  Among such libraries "
#| "are E<.UR http://www.uclibc.org/> I<uClibc> E<.UE ,> E<.UR http://"
#| "www.fefe.de/dietlibc/> I<dietlibc> E<.UE ,> and E<.UR http://www.musl-"
#| "libc.org/> I<musl libc> E<.UE .> Details of these libraries are covered "
#| "by the I<man-pages> project, where they are known."
msgid ""
"There are various other less widely used C libraries for Linux.  These "
"libraries are generally smaller than glibc, both in terms of features and "
"memory footprint, and often intended for building small binaries, perhaps "
"targeted at development for embedded Linux systems.  Among such libraries "
"are E<.UR http://www\\:.uclibc\\:.org/> I<uClibc> E<.UE ,> E<.UR http://"
"www\\:.fefe\\:.de/\\:dietlibc/> I<dietlibc> E<.UE ,> and E<.UR http://"
"www\\:.musl-libc\\:.org/> I<musl libc> E<.UE .> Details of these libraries "
"are covered by the I<man-pages> project, where they are known."
msgstr ""
"Существует несколько других менее распространённых библиотек C для Linux Эти "
"библиотеки, обычно, меньше glibc, как по возможностям так и по потреблению "
"памяти, и часто предназначены для сборки исполняемых файлов малого размера, "
"используемых, в основном, для разработки встраиваемых систем Linux. Такими "
"библиотеками являются E<.UR http://www.uclibc.org/> I<uClibc> E<.UE ,> E<.UR "
"http://www.fefe.de/dietlibc/> I<dietlibc> E<.UE ,> и E<.UR http://www.musl-"
"libc.org/> I<musl libc> E<.UE .> Описание этих библиотек охватывается "
"проектом I<man-pages>, если что-то известно."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМОТРИТЕ ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<syscalls>(2), B<getauxval>(3), B<proc>(5), B<feature_test_macros>(7), "
"B<man-pages>(7), B<standards>(7), B<vdso>(7)"
msgstr ""
"B<syscalls>(2), B<getauxval>(3), B<proc>(5), B<feature_test_macros>(7), "
"B<man-pages>(7), B<standards>(7), B<vdso>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 февраля 2023 г."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Справочные страницы Linux 6.03"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Справочные страницы Linux 6.9.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 октября 2023 г."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Справочные страницы Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Справочные страницы Linux (невыпущенные)"
