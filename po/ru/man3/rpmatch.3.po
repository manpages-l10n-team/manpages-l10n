# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# aereiae <aereiae@gmail.com>, 2014.
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2013-2014, 2016.
# Dmitriy S. Seregin <dseregin@59.ru>, 2013.
# Katrin Kutepova <blackkatelv@gmail.com>, 2018.
# Lockal <lockalsash@gmail.com>, 2013.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Баринов Владимир, 2016.
# Иван Павлов <pavia00@gmail.com>, 2017,2019.
# Kirill Rekhov <krekhov.dev@gmail.com>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2025-02-28 16:47+0100\n"
"PO-Revision-Date: 2024-12-21 19:50+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <debian-l10n-russian@lists.debian.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<rpmatch>()"
msgid "rpmatch"
msgstr "B<rpmatch>()"

#. type: TH
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "2024-07-23"
msgstr "23 июля 2024 г."

#. type: TH
#: archlinux
#, fuzzy, no-wrap
#| msgid "Linux man-pages 6.03"
msgid "Linux man-pages 6.12"
msgstr "Справочные страницы Linux 6.03"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "НАИМЕНОВАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"rpmatch - determine if the answer to a question is affirmative or negative"
msgstr ""
"rpmatch - определяет, является ли ответ на вопрос утвердительным или "
"отрицательным"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "БИБЛИОТЕКА"

#. type: Plain text
#: archlinux opensuse-tumbleweed
#, fuzzy
#| msgid "Standard C library (I<libc>, I<-lc>)"
msgid "Standard C library (I<libc>,\\ I<-lc>)"
msgstr "Стандартная библиотека языка C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ОБЗОР"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>stdlib.hE<gt>>\n"
msgstr "B<#include E<lt>stdlib.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<int rpmatch(const char *>I<response>B<);>\n"
msgstr "B<int rpmatch(const char *>I<response>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Требования макроса тестирования свойств для glibc (см. "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid "B<rpmatch>()"
msgid "B<rpmatch>():"
msgstr "B<rpmatch>()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    glibc 2.19 and earlier:\n"
"        _SVID_SOURCE\n"
msgstr ""
"    начиная с glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    glibc 2.19 и старее:\n"
"        _SVID_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<rpmatch>()  handles a user response to yes or no questions, with support "
"for internationalization."
msgstr ""
"Функция B<rpmatch>() обрабатывает ответ пользователя «да» или «нет» на "
"вопросы, учитывая интернационализацию."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<response> should be a null-terminated string containing a user-supplied "
"response, perhaps obtained with B<fgets>(3)  or B<getline>(3)."
msgstr ""
"В аргументе I<response> должен указываться ответ пользователя в виде строки, "
"заканчивающейся null, полученной, например, с помощью B<fgets>(3) или "
"B<getline>(3)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The user's language preference is taken into account per the environment "
"variables B<LANG>, B<LC_MESSAGES>, and B<LC_ALL>, if the program has called "
"B<setlocale>(3)  to effect their changes."
msgstr ""
"Пользовательские языковые настройки определяются с помощью переменных "
"окружения B<LANG>, B<LC_MESSAGES> и B<LC_ALL>, если для их учёта программа "
"вызывала B<setlocale>(3)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Regardless of the locale, responses matching B<^[Yy]> are always accepted "
#| "as affirmative, and those matching B<^[Nn]> are always accepted as "
#| "negative."
msgid ""
"Regardless of the locale, responses matching B<\\[ha][Yy]> are always "
"accepted as affirmative, and those matching B<\\[ha][Nn]> are always "
"accepted as negative."
msgstr ""
"Вне зависимости от локали, ответы, соответствующие B<^[Yy]>, всегда "
"считаются утвердительными, а соответствующие B<^[Nn]>, всегда принимаются за "
"отрицательные."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"After examining I<response>, B<rpmatch>()  returns 0 for a recognized "
"negative response (\"no\"), 1 for a recognized positive response (\"yes\"), "
"and -1 when the value of I<response> is unrecognized."
msgstr ""
"После проверки I<response>, функция B<rpmatch>() возвращает 0 при найденном "
"отрицательном ответе («нет»), 1 при положительном ответе («да») и -1, когда "
"значение I<response> не распознано."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"A return value of -1 may indicate either an invalid input, or some other "
"error.  It is incorrect to only test if the return value is nonzero."
msgstr ""
"Возвращаемое значение -1 может указывать на неправильные входные данные, "
"либо какую-то другую ошибку. Не стоит проверять является ли возвращаемое "
"значение лишь отличным от нуля."

#. type: Plain text
#: archlinux opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<rpmatch>()  can fail for any of the reasons that B<regcomp>(3)  or "
#| "B<regexec>(3)  can fail; the cause of the error is not available from "
#| "I<errno> or anywhere else, but indicates a failure of the regex engine "
#| "(but this case is indistinguishable from that of an unrecognized value of "
#| "I<response>)."
msgid ""
"B<rpmatch>()  can fail for any of the reasons that B<regcomp>(3)  or "
"B<regexec>(3)  can fail; the error is not available from I<errno> or "
"anywhere else, but indicates a failure of the regex engine (but this case is "
"indistinguishable from that of an unrecognized value of I<response>)."
msgstr ""
"Функция B<rpmatch>() может завершиться с ошибкой по любой из причин, по "
"которым могут не выполниться B<regcomp>(3) или B<regexec>(3); причина ошибки "
"не указывается в I<errno> или где-то ещё — это указывает на ошибку в самом "
"механизме regex (но этот случай не отличается от нераспознанных значений "
"I<response>)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "АТРИБУТЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr "Описание терминов данного раздела смотрите в B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Интерфейс"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Атрибут"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Значение"

#. type: tbl table
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<rpmatch>()"
msgstr "B<rpmatch>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Безвредность в нитях"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe locale"
msgstr "MT-Safe locale"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "None."
msgstr "Отсутствуют."

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ИСТОРИЯ"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "GNU, FreeBSD, AIX."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ОШИБКИ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<YESEXPR> and B<NOEXPR> of some locales (including \"C\") only inspect "
"the first character of the I<response>.  This can mean that \"yno\" et al. "
"resolve to B<1>.  This is an unfortunate historical side-effect which should "
"be fixed in time with proper localisation, and should not deter from "
"B<rpmatch>()  being the proper way to distinguish between binary answers."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "ПРИМЕРЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The following program displays the results when B<rpmatch>()  is applied to "
"the string given in the program's command-line argument."
msgstr ""
"Следующая программа показывает результаты, когда в B<rpmatch>() передана "
"строка, полученная программой из командной строки."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"#define _DEFAULT_SOURCE\n"
"#include E<lt>locale.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    if (argc != 2 || strcmp(argv[1], \"--help\") == 0) {\n"
"        fprintf(stderr, \"%s response\\[rs]n\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    setlocale(LC_ALL, \"\");\n"
"    printf(\"rpmatch() returns: %d\\[rs]n\", rpmatch(argv[1]));\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#.  SRC END
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМОТРИТЕ ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<fgets>(3), B<getline>(3), B<nl_langinfo>(3), B<regcomp>(3), B<setlocale>(3)"
msgstr ""
"B<fgets>(3), B<getline>(3), B<nl_langinfo>(3), B<regcomp>(3), B<setlocale>(3)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 февраля 2023 г."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Справочные страницы Linux 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Стандартная библиотека языка C (I<libc>, I<-lc>)"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid ""
"B<rpmatch>()  can fail for any of the reasons that B<regcomp>(3)  or "
"B<regexec>(3)  can fail; the cause of the error is not available from "
"I<errno> or anywhere else, but indicates a failure of the regex engine (but "
"this case is indistinguishable from that of an unrecognized value of "
"I<response>)."
msgstr ""
"Функция B<rpmatch>() может завершиться с ошибкой по любой из причин, по "
"которым могут не выполниться B<regcomp>(3) или B<regexec>(3); причина ошибки "
"не указывается в I<errno> или где-то ещё — это указывает на ошибку в самом "
"механизме regex (но этот случай не отличается от нераспознанных значений "
"I<response>)."

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid ""
#| "B<rpmatch>()  is not required by any standard, but is available on a few "
#| "other systems."
msgid ""
"B<rpmatch>()  is not required by any standard, but available under the GNU C "
"library, FreeBSD, and AIX."
msgstr ""
"Функция B<rpmatch>() не является стандартной, но доступна в некоторых других "
"системах."

#. type: Plain text
#: debian-bookworm
#, fuzzy, no-wrap
#| msgid ""
#| "#define _SVID_SOURCE\n"
#| "#include E<lt>locale.hE<gt>\n"
#| "#include E<lt>stdlib.hE<gt>\n"
#| "#include E<lt>string.hE<gt>\n"
#| "#include E<lt>stdio.hE<gt>\n"
msgid ""
"#define _DEFAULT_SOURCE\n"
"#include E<lt>locale.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
msgstr ""
"#define _SVID_SOURCE\n"
"#include E<lt>locale.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    if (argc != 2 || strcmp(argv[1], \"--help\") == 0) {\n"
"        fprintf(stderr, \"%s response\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    if (argc != 2 || strcmp(argv[1], \"--help\") == 0) {\n"
"        fprintf(stderr, \"%s response\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    setlocale(LC_ALL, \"\");\n"
"    printf(\"rpmatch() returns: %d\\en\", rpmatch(argv[1]));\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"    setlocale(LC_ALL, \"\");\n"
"    printf(\"rpmatch() returns: %d\\en\", rpmatch(argv[1]));\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "2024-06-15"
msgstr "15 июня 2024 г."

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Справочные страницы Linux 6.9.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 октября 2023 г."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Справочные страницы Linux 6.06"

#. type: Plain text
#: mageia-cauldron
#, no-wrap
msgid ""
"#define _DEFAULT_SOURCE\n"
"#include E<lt>locale.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    if (argc != 2 || strcmp(argv[1], \"--help\") == 0) {\n"
"        fprintf(stderr, \"%s response\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    setlocale(LC_ALL, \"\");\n"
"    printf(\"rpmatch() returns: %d\\en\", rpmatch(argv[1]));\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Справочные страницы Linux (невыпущенные)"
