# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2014.
# Dmitriy S. Seregin <dseregin@59.ru>, 2013.
# Dmitry Bolkhovskikh <d20052005@yandex.ru>, 2017.
# Katrin Kutepova <blackkatelv@gmail.com>, 2018.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
# Kirill Rekhov <krekhov.dev@gmail.com>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2025-02-28 16:30+0100\n"
"PO-Revision-Date: 2024-12-21 19:49+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <debian-l10n-russian@lists.debian.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<clock_getcpuclockid>()"
msgid "clock_getcpuclockid"
msgstr "B<clock_getcpuclockid>()"

#. type: TH
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "2024-07-23"
msgstr "23 июля 2024 г."

#. type: TH
#: archlinux
#, fuzzy, no-wrap
#| msgid "Linux man-pages 6.03"
msgid "Linux man-pages 6.12"
msgstr "Справочные страницы Linux 6.03"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "НАИМЕНОВАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "clock_getcpuclockid - obtain ID of a process CPU-time clock"
msgstr "clock_getcpuclockid - возвращает ID процессорных часов процесса"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "БИБЛИОТЕКА"

#. type: Plain text
#: archlinux opensuse-tumbleweed
#, fuzzy
#| msgid "Standard C library (I<libc>, I<-lc>), since glibc 2.17"
msgid "Standard C library (I<libc>,\\ I<-lc>), since glibc 2.17"
msgstr "Стандартная библиотека C (I<libc>, I<-lc>), начиная с glibc 2.17"

#. type: Plain text
#: archlinux opensuse-tumbleweed
#, fuzzy
#| msgid "Before glibc 2.17, Real-time library (I<librt>, I<-lrt>)"
msgid "Before glibc 2.17, Real-time library (I<librt>,\\ I<-lrt>)"
msgstr "До glibc 2.17, библиотека реального времени (I<librt>, I<-lrt>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ОБЗОР"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<#include E<lt>time.hE<gt>>"
msgstr "B<#include E<lt>time.hE<gt>>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<int clock_getcpuclockid(pid_t >I<pid>B<, clockid_t *>I<clock_id>B<);>\n"
msgid "B<int clock_getcpuclockid(pid_t >I<pid>B<, clockid_t *>I<clockid>B<);>\n"
msgstr "B<int clock_getcpuclockid(pid_t >I<pid>B<, clockid_t *>I<clock_id>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Требования макроса тестирования свойств для glibc (см. "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<clock_getcpuclockid>():"
msgstr "B<clock_getcpuclockid>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "    _POSIX_C_SOURCE E<gt>= 200112L\n"
msgstr "    _POSIX_C_SOURCE E<gt>= 200112L\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The B<clock_getcpuclockid>()  function obtains the ID of the CPU-time "
#| "clock of the process whose ID is I<pid>, and returns it in the location "
#| "pointed to by I<clock_id>.  If I<pid> is zero, then the clock ID of the "
#| "CPU-time clock of the calling process is returned."
msgid ""
"The B<clock_getcpuclockid>()  function obtains the ID of the CPU-time clock "
"of the process whose ID is I<pid>, and returns it in the location pointed to "
"by I<clockid>.  If I<pid> is zero, then the clock ID of the CPU-time clock "
"of the calling process is returned."
msgstr ""
"Функция B<clock_getcpuclockid>() возвращает ID процессорных часов процесса с "
"заданным I<pid> в виде указателя в I<clock_id>. Если значение I<pid> равно "
"0, то возвращается ID процессорных часов вызывающего процесса."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On success, B<clock_getcpuclockid>()  returns 0; on error, it returns one of "
"the positive error numbers listed in ERRORS."
msgstr ""
"При успешном выполнении B<clock_getcpuclockid>() возвращает 0; при ошибке "
"возвращается одно из положительных значений ошибки, перечисленных в разделе "
"ОШИБКИ."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOSYS>"
msgstr "B<ENOSYS>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The kernel does not support obtaining the per-process CPU-time clock of "
"another process, and I<pid> does not specify the calling process."
msgstr ""
"Ядро не поддерживает возврат процессорных часов другого процесса и значение "
"I<pid> не указывает на вызывающий процесс."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The caller does not have permission to access the CPU-time clock of the "
"process specified by I<pid>.  (Specified in POSIX.1-2001; does not occur on "
"Linux unless the kernel does not support obtaining the per-process CPU-time "
"clock of another process.)"
msgstr ""
"Вызывающий не имеет прав доступа к процессорным часам процесса, указанного в "
"I<pid> (определено в POSIX.1-2001; не возникает в Linux, если ядро не "
"собрано с поддержкой получения процессорных часов процесса, отличного от "
"вызывающего)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ESRCH>"
msgstr "B<ESRCH>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "There is no process with the ID I<pid>."
msgstr "Процесс с идентификатором I<pid> не найден."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "АТРИБУТЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr "Описание терминов данного раздела смотрите в B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Интерфейс"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Атрибут"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Значение"

#. type: tbl table
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<clock_getcpuclockid>()"
msgstr "B<clock_getcpuclockid>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Безвредность в нитях"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ИСТОРИЯ"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "glibc 2.2.  POSIX.1-2001."
msgstr "glibc 2.2.  POSIX.1-2001."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ПРИМЕЧАНИЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Calling B<clock_gettime>(2)  with the clock ID obtained by a call to "
"B<clock_getcpuclockid>()  with a I<pid> of 0, is the same as using the clock "
"ID B<CLOCK_PROCESS_CPUTIME_ID>."
msgstr ""
"Вызов B<clock_gettime>(2) с ID часов, полученных от вызова "
"B<clock_getcpuclockid>() с I<pid> равным 0, делает то же, что и с ID часов, "
"равным B<CLOCK_PROCESS_CPUTIME_ID>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "ПРИМЕРЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The example program below obtains the CPU-time clock ID of the process whose "
"ID is given on the command line, and then uses B<clock_gettime>(2)  to "
"obtain the time on that clock.  An example run is the following:"
msgstr ""
"Представленный ниже пример программы возвращает ID процессорных часов "
"процесса, чей ID указан в командной строке, а затем используется "
"B<clock_gettime>(2) для получения времени этих часов. Пример запуска:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"$B< ./a.out 1>                 # Show CPU clock of init process\n"
"CPU-time clock for PID 1 is 2.213466748 seconds\n"
msgstr ""
"$B< ./a.out 1>              # показать процессорные часы процесса init\n"
"Процессорные часы для PID 1 показывают 2.213466748 секунд\n"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Program source"
msgstr "Исходный код программы"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"#define _XOPEN_SOURCE 600\n"
"#include E<lt>stdint.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>time.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    clockid_t clockid;\n"
"    struct timespec ts;\n"
"\\&\n"
"    if (argc != 2) {\n"
"        fprintf(stderr, \"%s E<lt>process-IDE<gt>\\[rs]n\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    if (clock_getcpuclockid(atoi(argv[1]), &clockid) != 0) {\n"
"        perror(\"clock_getcpuclockid\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    if (clock_gettime(clockid, &ts) == -1) {\n"
"        perror(\"clock_gettime\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    printf(\"CPU-time clock for PID %s is %jd.%09ld seconds\\[rs]n\",\n"
"           argv[1], (intmax_t) ts.tv_sec, ts.tv_nsec);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#.  SRC END
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМОТРИТЕ ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<clock_getres>(2), B<timer_create>(2), B<pthread_getcpuclockid>(3), "
"B<time>(7)"
msgstr ""
"B<clock_getres>(2), B<timer_create>(2), B<pthread_getcpuclockid>(3), "
"B<time>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-12-15"
msgstr "15 декабря 2022 г."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Справочные страницы Linux 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "Standard C library (I<libc>, I<-lc>), since glibc 2.17"
msgstr "Стандартная библиотека C (I<libc>, I<-lc>), начиная с glibc 2.17"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "Before glibc 2.17, Real-time library (I<librt>, I<-lrt>)"
msgstr "До glibc 2.17, библиотека реального времени (I<librt>, I<-lrt>)"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid ""
#| "The B<clock_getcpuclockid>()  function is available in glibc since "
#| "version 2.2."
msgid "The B<clock_getcpuclockid>()  function is available since glibc 2.2."
msgstr ""
"Функция B<clock_getcpuclockid>() доступна в glibc начиная с версии 2.2."

#. type: Plain text
#: debian-bookworm
msgid "POSIX.1-2001, POSIX.1-2008."
msgstr "POSIX.1-2001, POSIX.1-2008."

#. type: Plain text
#: debian-bookworm
#, fuzzy, no-wrap
#| msgid ""
#| "#define _XOPEN_SOURCE 600\n"
#| "#include E<lt>stdio.hE<gt>\n"
#| "#include E<lt>unistd.hE<gt>\n"
#| "#include E<lt>stdlib.hE<gt>\n"
#| "#include E<lt>time.hE<gt>\n"
msgid ""
"#define _XOPEN_SOURCE 600\n"
"#include E<lt>stdint.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>time.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
msgstr ""
"#define _XOPEN_SOURCE 600\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>time.hE<gt>\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    clockid_t clockid;\n"
"    struct timespec ts;\n"
msgstr ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    clockid_t clockid;\n"
"    struct timespec ts;\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    if (argc != 2) {\n"
"        fprintf(stderr, \"%s E<lt>process-IDE<gt>\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    if (argc != 2) {\n"
"        fprintf(stderr, \"%s E<lt>ID_процессаE<gt>\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    if (clock_getcpuclockid(atoi(argv[1]), &clockid) != 0) {\n"
"        perror(\"clock_getcpuclockid\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    if (clock_getcpuclockid(atoi(argv[1]), &clockid) != 0) {\n"
"        perror(\"clock_getcpuclockid\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    if (clock_gettime(clockid, &ts) == -1) {\n"
"        perror(\"clock_gettime\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    if (clock_gettime(clockid, &ts) == -1) {\n"
"        perror(\"clock_gettime\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: debian-bookworm
#, fuzzy, no-wrap
#| msgid ""
#| "    printf(\"CPU-time clock for PID %s is %ld.%09ld seconds\\en\",\n"
#| "            argv[1], (long) ts.tv_sec, (long) ts.tv_nsec);\n"
#| "    exit(EXIT_SUCCESS);\n"
#| "}\n"
msgid ""
"    printf(\"CPU-time clock for PID %s is %jd.%09ld seconds\\en\",\n"
"           argv[1], (intmax_t) ts.tv_sec, ts.tv_nsec);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"    printf(\"процессорные часы для PID %s показывают %ld.%09ld секунд\\en\",\n"
"            argv[1], (long) ts.tv_sec, (long) ts.tv_nsec);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "2024-06-15"
msgstr "15 июня 2024 г."

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Справочные страницы Linux 6.9.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 октября 2023 г."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Справочные страницы Linux 6.06"

#. type: Plain text
#: mageia-cauldron
#, no-wrap
msgid ""
"#define _XOPEN_SOURCE 600\n"
"#include E<lt>stdint.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>time.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    clockid_t clockid;\n"
"    struct timespec ts;\n"
"\\&\n"
"    if (argc != 2) {\n"
"        fprintf(stderr, \"%s E<lt>process-IDE<gt>\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    if (clock_getcpuclockid(atoi(argv[1]), &clockid) != 0) {\n"
"        perror(\"clock_getcpuclockid\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    if (clock_gettime(clockid, &ts) == -1) {\n"
"        perror(\"clock_gettime\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    printf(\"CPU-time clock for PID %s is %jd.%09ld seconds\\en\",\n"
"           argv[1], (intmax_t) ts.tv_sec, ts.tv_nsec);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Справочные страницы Linux (невыпущенные)"
