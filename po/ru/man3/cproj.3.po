# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2014.
# Dmitriy S. Seregin <dseregin@59.ru>, 2013.
# Dmitry Bolkhovskikh <d20052005@yandex.ru>, 2017.
# Katrin Kutepova <blackkatelv@gmail.com>, 2018.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
# Kirill Rekhov <krekhov.dev@gmail.com>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2025-02-28 16:31+0100\n"
"PO-Revision-Date: 2024-12-21 19:49+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <debian-l10n-russian@lists.debian.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "cproj"
msgstr ""

#. type: TH
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "2024-07-23"
msgstr "23 июля 2024 г."

#. type: TH
#: archlinux
#, fuzzy, no-wrap
#| msgid "Linux man-pages 6.03"
msgid "Linux man-pages 6.12"
msgstr "Справочные страницы Linux 6.03"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "НАИМЕНОВАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "cproj, cprojf, cprojl - project into Riemann Sphere"
msgstr "cproj, cprojf, cprojl - проекция на римановскую сферу"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "БИБЛИОТЕКА"

#. type: Plain text
#: archlinux opensuse-tumbleweed
#, fuzzy
#| msgid "Math library (I<libm>, I<-lm>)"
msgid "Math library (I<libm>,\\ I<-lm>)"
msgstr "Математическая библиотека (I<libm>, I<-lm>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ОБЗОР"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>complex.hE<gt>>\n"
msgstr "B<#include E<lt>complex.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<double cos(double >I<x>B<);>\n"
#| "B<float cosf(float >I<x>B<);>\n"
#| "B<long double cosl(long double >I<x>B<);>\n"
msgid ""
"B<double complex cproj(double complex >I<z>B<);>\n"
"B<float complex cprojf(float complex >I<z>B<);>\n"
"B<long double complex cprojl(long double complex >I<z>B<);>\n"
msgstr ""
"B<double cos(double >I<x>B<);>\n"
"B<float cosf(float >I<x>B<);>\n"
"B<long double cosl(long double >I<x>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"These functions project a point in the plane onto the surface of a Riemann "
"Sphere, the one-point compactification of the complex plane.  Each finite "
"point I<z> projects to I<z> itself.  Every complex infinite value is "
"projected to a single infinite value, namely to positive infinity on the "
"real axis."
msgstr ""
"Эти функции проецируют точку плоскости на поверхность римановской сферы, "
"одноточечную компактификацию комплексной плоскости. Каждая конечная точка "
"I<z> проецируется в саму I<z>. Все комплексные бесконечные значения "
"проецируются в одно бесконечное значение, а именно в положительную "
"бесконечность действительной оси."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "АТРИБУТЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr "Описание терминов данного раздела смотрите в B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Интерфейс"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Атрибут"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Значение"

#. type: tbl table
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<cproj>(),\n"
"B<cprojf>(),\n"
"B<cprojl>()"
msgstr ""
"B<cproj>(),\n"
"B<cprojf>(),\n"
"B<cprojl>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Безвредность в нитях"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr "C11, POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ИСТОРИЯ"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "glibc 2.1.  C99, POSIX.1-2001."
msgstr "glibc 2.1.  C99, POSIX.1-2001."

#. #-#-#-#-#  archlinux: cproj.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=10401
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: cproj.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  http://sources.redhat.com/bugzilla/show_bug.cgi?id=10401
#. type: Plain text
#. #-#-#-#-#  debian-unstable: cproj.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=10401
#. type: Plain text
#. #-#-#-#-#  fedora-42: cproj.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=10401
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: cproj.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=10401
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: cproj.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=10401
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-16-0: cproj.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=10401
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: cproj.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=10401
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In glibc 2.11 and earlier, the implementation does something different (a "
"I<stereographic> projection onto a Riemann Sphere)."
msgstr ""
"Реализацией в glibc 2.11 и старее выполнялось что-то другое "
"(I<стереографическая> проекция на римановскую сферу)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМОТРИТЕ ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<cabs>(3), B<complex>(7)"
msgstr "B<cabs>(3), B<complex>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-12-15"
msgstr "15 декабря 2022 г."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Справочные страницы Linux 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "Math library (I<libm>, I<-lm>)"
msgstr "Математическая библиотека (I<libm>, I<-lm>)"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#. type: Plain text
#: debian-bookworm
msgid "These functions were added in glibc 2.1."
msgstr "Эти функции были добавлены в glibc 2.1."

#. type: Plain text
#: debian-bookworm
msgid "C99, POSIX.1-2001, POSIX.1-2008."
msgstr "C99, POSIX.1-2001, POSIX.1-2008."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NOTES"
msgstr "ПРИМЕЧАНИЯ"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "2024-05-02"
msgstr "2 мая 2024 г."

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Справочные страницы Linux 6.9.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 октября 2023 г."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Справочные страницы Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Справочные страницы Linux (невыпущенные)"
