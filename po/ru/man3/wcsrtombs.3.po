# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2016.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2015, 2017-2019.
# Kirill Rekhov <krekhov.dev@gmail.com>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2025-02-28 16:56+0100\n"
"PO-Revision-Date: 2024-12-21 19:50+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <debian-l10n-russian@lists.debian.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<wcsrtombs>()"
msgid "wcsrtombs"
msgstr "B<wcsrtombs>()"

#. type: TH
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "2024-11-17"
msgstr "17 ноября 2024 г."

#. type: TH
#: archlinux
#, fuzzy, no-wrap
#| msgid "Linux man-pages 6.03"
msgid "Linux man-pages 6.12"
msgstr "Справочные страницы Linux 6.03"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "НАИМЕНОВАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "wcsrtombs - convert a wide-character string to a multibyte string"
msgstr "wcsrtombs - преобразует широкосимвольную строку в многобайтовую строку"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "БИБЛИОТЕКА"

#. type: Plain text
#: archlinux opensuse-tumbleweed
#, fuzzy
#| msgid "Standard C library (I<libc>, I<-lc>)"
msgid "Standard C library (I<libc>,\\ I<-lc>)"
msgstr "Стандартная библиотека языка C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ОБЗОР"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>wchar.hE<gt>>\n"
msgstr "B<#include E<lt>wchar.hE<gt>>\n"

#. type: Plain text
#: archlinux opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<size_t wcsrtombs(char *>I<dest>B<, const wchar_t **>I<src>B<,>\n"
#| "B<                 size_t >I<len>B<, mbstate_t *>I<ps>B<);>\n"
msgid ""
"B<size_t wcsrtombs(char >I<dest>B<[restrict .>I<size>B<], const wchar_t **restrict >I<src>B<,>\n"
"B<                 size_t >I<size>B<, mbstate_t *restrict >I<ps>B<);>\n"
msgstr ""
"B<size_t wcsrtombs(char *>I<dest>B<, const wchar_t **>I<src>B<,>\n"
"B<                 size_t >I<len>B<, mbstate_t *>I<ps>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "If I<dest> is not NULL, the B<wcsrtombs>()  function converts the wide-"
#| "character string I<*src> to a multibyte string starting at I<dest>.  At "
#| "most I<len> bytes are written to I<dest>.  The shift state I<*ps> is "
#| "updated.  The conversion is effectively performed by repeatedly calling "
#| "I<wcrtomb(dest, *src, ps)>, as long as this call succeeds, and then "
#| "incrementing I<dest> by the number of bytes written and I<*src> by one.  "
#| "The conversion can stop for three reasons:"
msgid ""
"If I<dest> is not NULL, the B<wcsrtombs>()  function converts the wide-"
"character string I<*src> to a multibyte string starting at I<dest>.  At most "
"I<size> bytes are written to I<dest>.  The shift state I<*ps> is updated.  "
"The conversion is effectively performed by repeatedly calling "
"I<wcrtomb(dest, *src, ps)>, as long as this call succeeds, and then "
"incrementing I<dest> by the number of bytes written and I<*src> by one.  The "
"conversion can stop for three reasons:"
msgstr ""
"Если I<dest> не равно NULL, функция B<wcsrtombs>() преобразует "
"широкосимвольную строку I<*src> в многобайтовую строку, начиная с I<dest>. В "
"I<dest> будет записано максимум I<len> байтов. Обновляется состояние "
"смещения в I<*ps>. Замена происходит путем повторяющихся вызовов "
"I<wcrtomb(dest, *src, ps)> до тех пор, пока они выполняются без ошибок. При "
"этом значение I<dest> увеличивается на количество записанных байтов, а "
"I<*src> — на единицу. Замена может быть остановлена в трех случаях:"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr "\\[bu]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"A wide character has been encountered that can not be represented as a "
"multibyte sequence (according to the current locale).  In this case, I<*src> "
"is left pointing to the invalid wide character, I<(size_t)\\ -1> is "
"returned, and I<errno> is set to B<EILSEQ>."
msgstr ""
"Встретившийся широкий символ нельзя представить в виде многобайтовой "
"последовательности (в соответствии с текущей системной локалью). В этом "
"случае I<*src> будет указывать на неправильный широкий символ, возвращается "
"I<(size_t)\\ -1>, а значение I<errno> становится равным B<EILSEQ>."

#. type: Plain text
#: archlinux opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The length limit forces a stop.  In this case, I<*src> is left pointing "
#| "to the next wide character to be converted, and the number of bytes "
#| "written to I<dest> is returned."
msgid ""
"The size limit forces a stop.  In this case, I<*src> is left pointing to the "
"next wide character to be converted, and the number of bytes written to "
"I<dest> is returned."
msgstr ""
"Остановка при достижении ограничения длины. В этом случае I<*src> также "
"будет указывать на следующий широкий символ, подлежащий преобразованию, и "
"возвращается количество байтов, записанных в I<dest>."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The wide-character string has been completely converted, including the "
#| "terminating null wide character (L\\(aq\\e0\\(aq), which has the side "
#| "effect of bringing back I<*ps> to the initial state.  In this case, "
#| "I<*src> is set to NULL, and the number of bytes written to I<dest>, "
#| "excluding the terminating null byte (\\(aq\\e0\\(aq), is returned."
msgid ""
"The wide-character string has been completely converted, including the "
"terminating null wide character (L\\[aq]\\[rs]0\\[aq]), which has the side "
"effect of bringing back I<*ps> to the initial state.  In this case, I<*src> "
"is set to NULL, and the number of bytes written to I<dest>, excluding the "
"terminating null byte (\\[aq]\\[rs]0\\[aq]), is returned."
msgstr ""
"Широкосимвольная строка была полностью преобразована, включая завершающий "
"широкий символ null (L\\(aq\\e0\\(aq) (побочный эффект: I<*ps> возвращается "
"в начальное состояние). В этом случае I<*src> устанавливается равным NULL и "
"возвращается количество байтов, записанных в I<dest>, не считая завершающего "
"байта null (\\(aq\\e0\\(aq)."

#. type: Plain text
#: archlinux opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "If I<dest> is NULL, I<len> is ignored, and the conversion proceeds as "
#| "above, except that the converted bytes are not written out to memory, and "
#| "that no length limit exists."
msgid ""
"If I<dest> is NULL, I<size> is ignored, and the conversion proceeds as "
"above, except that the converted bytes are not written out to memory, and "
"that no size limit exists."
msgstr ""
"Если значение I<dest> равно NULL, то I<len> игнорируется и преобразование "
"выполняется как описано выше, исключая то, что преобразованные байты не "
"записываются в память и нет ограничения по длине."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In both of the above cases, if I<ps> is NULL, a static anonymous state known "
"only to the B<wcsrtombs>()  function is used instead."
msgstr ""
"В обоих перечисленных случаях, если I<ps> равно NULL, то используется "
"статическое анонимное состояние, известное только функции B<wcsrtombs>()."

#. type: Plain text
#: archlinux opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The programmer must ensure that there is room for at least I<len> bytes "
#| "at I<dest>."
msgid ""
"The programmer must ensure that there is room for at least I<size> bytes at "
"I<dest>."
msgstr ""
"Программист должен быть уверен, что в I<dest> достаточно места для, по "
"крайней мере, I<len> байт."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<wcsrtombs>()  function returns the number of bytes that make up the "
"converted part of multibyte sequence, not including the terminating null "
"byte.  If a wide character was encountered which could not be converted, "
"I<(size_t)\\ -1> is returned, and I<errno> set to B<EILSEQ>."
msgstr ""
"Функция B<wcsrtombs>() возвращает количество байт в полученной многобайтовой "
"последовательности, не включая завершающий байт null. Если встречается "
"широкий символ, который невозможно преобразовать, возвращается I<(size_t)\\ "
"-1> и значение I<errno> становится равным B<EILSEQ>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "АТРИБУТЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr "Описание терминов данного раздела смотрите в B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Интерфейс"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Атрибут"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Значение"

#. type: tbl table
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<wcsrtombs>()"
msgstr "B<wcsrtombs>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Безвредность в нитях"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Unsafe race:wcsrtombs/!ps"
msgstr "MT-Unsafe race:wcsrtombs/!ps"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr "C11, POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ИСТОРИЯ"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2001, C99."
msgstr "POSIX.1-2001, C99."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ПРИМЕЧАНИЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The behavior of B<wcsrtombs>()  depends on the B<LC_CTYPE> category of the "
"current locale."
msgstr ""
"Поведение B<wcsrtombs>() зависит от категории B<LC_CTYPE> текущей локали."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Passing NULL as I<ps> is not multithread safe."
msgstr "Передавать NULL в качестве I<ps> небезопасно при работе с нитями."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМОТРИТЕ ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<iconv>(3), B<mbsinit>(3), B<wcrtomb>(3), B<wcsnrtombs>(3), B<wcstombs>(3)"
msgstr ""
"B<iconv>(3), B<mbsinit>(3), B<wcrtomb>(3), B<wcsnrtombs>(3), B<wcstombs>(3)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 февраля 2023 г."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Справочные страницы Linux 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Стандартная библиотека языка C (I<libc>, I<-lc>)"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
#, fuzzy, no-wrap
#| msgid ""
#| "B<size_t wcsrtombs(char *>I<dest>B<, const wchar_t **>I<src>B<,>\n"
#| "B<                 size_t >I<len>B<, mbstate_t *>I<ps>B<);>\n"
msgid ""
"B<size_t wcsrtombs(char >I<dest>B<[restrict .>I<len>B<], const wchar_t **restrict >I<src>B<,>\n"
"B<                 size_t >I<len>B<, mbstate_t *restrict >I<ps>B<);>\n"
msgstr ""
"B<size_t wcsrtombs(char *>I<dest>B<, const wchar_t **>I<src>B<,>\n"
"B<                 size_t >I<len>B<, mbstate_t *>I<ps>B<);>\n"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid ""
"If I<dest> is not NULL, the B<wcsrtombs>()  function converts the wide-"
"character string I<*src> to a multibyte string starting at I<dest>.  At most "
"I<len> bytes are written to I<dest>.  The shift state I<*ps> is updated.  "
"The conversion is effectively performed by repeatedly calling "
"I<wcrtomb(dest, *src, ps)>, as long as this call succeeds, and then "
"incrementing I<dest> by the number of bytes written and I<*src> by one.  The "
"conversion can stop for three reasons:"
msgstr ""
"Если I<dest> не равно NULL, функция B<wcsrtombs>() преобразует "
"широкосимвольную строку I<*src> в многобайтовую строку, начиная с I<dest>. В "
"I<dest> будет записано максимум I<len> байтов. Обновляется состояние "
"смещения в I<*ps>. Замена происходит путем повторяющихся вызовов "
"I<wcrtomb(dest, *src, ps)> до тех пор, пока они выполняются без ошибок. При "
"этом значение I<dest> увеличивается на количество записанных байтов, а "
"I<*src> — на единицу. Замена может быть остановлена в трех случаях:"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid ""
"The length limit forces a stop.  In this case, I<*src> is left pointing to "
"the next wide character to be converted, and the number of bytes written to "
"I<dest> is returned."
msgstr ""
"Остановка при достижении ограничения длины. В этом случае I<*src> также "
"будет указывать на следующий широкий символ, подлежащий преобразованию, и "
"возвращается количество байтов, записанных в I<dest>."

#. type: Plain text
#: debian-bookworm mageia-cauldron
#, fuzzy
#| msgid ""
#| "The wide-character string has been completely converted, including the "
#| "terminating null wide character (L\\(aq\\e0\\(aq), which has the side "
#| "effect of bringing back I<*ps> to the initial state.  In this case, "
#| "I<*src> is set to NULL, and the number of bytes written to I<dest>, "
#| "excluding the terminating null byte (\\(aq\\e0\\(aq), is returned."
msgid ""
"The wide-character string has been completely converted, including the "
"terminating null wide character (L\\[aq]\\e0\\[aq]), which has the side "
"effect of bringing back I<*ps> to the initial state.  In this case, I<*src> "
"is set to NULL, and the number of bytes written to I<dest>, excluding the "
"terminating null byte (\\[aq]\\e0\\[aq]), is returned."
msgstr ""
"Широкосимвольная строка была полностью преобразована, включая завершающий "
"широкий символ null (L\\(aq\\e0\\(aq) (побочный эффект: I<*ps> возвращается "
"в начальное состояние). В этом случае I<*src> устанавливается равным NULL и "
"возвращается количество байтов, записанных в I<dest>, не считая завершающего "
"байта null (\\(aq\\e0\\(aq)."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid ""
"If I<dest> is NULL, I<len> is ignored, and the conversion proceeds as above, "
"except that the converted bytes are not written out to memory, and that no "
"length limit exists."
msgstr ""
"Если значение I<dest> равно NULL, то I<len> игнорируется и преобразование "
"выполняется как описано выше, исключая то, что преобразованные байты не "
"записываются в память и нет ограничения по длине."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid ""
"The programmer must ensure that there is room for at least I<len> bytes at "
"I<dest>."
msgstr ""
"Программист должен быть уверен, что в I<dest> достаточно места для, по "
"крайней мере, I<len> байт."

#. type: Plain text
#: debian-bookworm
msgid "POSIX.1-2001, POSIX.1-2008, C99."
msgstr "POSIX.1-2001, POSIX.1-2008, C99."

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "2024-06-15"
msgstr "15 июня 2024 г."

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Справочные страницы Linux 6.9.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 октября 2023 г."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Справочные страницы Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Справочные страницы Linux (невыпущенные)"
