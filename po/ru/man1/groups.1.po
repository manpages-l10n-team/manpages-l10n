# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Андрей Догадкин <adogadkin@outlook.com>, 2024.
# Kirill Rekhov <krekhov.dev@gmail.com>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.22.0\n"
"POT-Creation-Date: 2025-02-28 16:35+0100\n"
"PO-Revision-Date: 2024-12-21 19:49+0300\n"
"Last-Translator: Andrey Dogadkin <adogadkin@outlook.com>\n"
"Language-Team: Russian <debian-l10n-russian@lists.debian.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Poedit 3.4.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "GROUPS"
msgstr "GROUPS"

#. type: TH
#: archlinux fedora-rawhide
#, fuzzy, no-wrap
#| msgid "February 2023"
msgid "February 2025"
msgstr "февраль 2023 г."

#. type: TH
#: archlinux fedora-42 fedora-rawhide opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "GNU coreutils 9.1"
msgid "GNU coreutils 9.6"
msgstr "GNU coreutils 9.1"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Команды пользователя"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "НАИМЕНОВАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "groups - print the groups a user is in"
msgstr "groups — вывести группы, в которых состоит пользователь"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ОБЗОР"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<groups> [I<\\,OPTION\\/>]... [I<\\,USERNAME\\/>]..."
msgstr "B<groups> [I<\\,ПАРАМЕТР\\/>]... [I<\\,ИМЯ_ПОЛЬЗОВАТЕЛЯ\\/>]..."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Print group memberships for each USERNAME or, if no USERNAME is specified, "
"for the current process (which may differ if the groups database has "
"changed)."
msgstr ""
"Вывести информацию о принадлежности к группам для каждого "
"ИМЕНИ_ПОЛЬЗОВАТЕЛЯ, или, если ИМЯ_ПОЛЬЗОВАТЕЛЯ не указано, для текущего "
"процесса (результаты могут различаться, если база данных групп была "
"изменена)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "отобразить эту справочную информацию и завершить работу"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "вывести информацию о версии и завершить работу"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "АВТОРЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Written by David MacKenzie and James Youngman."
msgstr ""
"Программа написана Дэвидом Маккензи (David MacKenzie) и Джеймсом Янгмэном "
"(James Youngman)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "ИНФОРМАЦИЯ ОБ ОШИБКАХ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Онлайн-справка GNU coreutils: E<lt>https://www.gnu.org/software/coreutils/"
"E<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Сообщайте обо всех ошибках перевода по адресу E<lt>https://"
"translationproject.org/team/ru.htmlE<gt>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "АВТОРСКИЕ ПРАВА"

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU "
#| "GPL version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgid ""
"Copyright \\(co 2025 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2022 Free Software Foundation, Inc. Лицензия GPLv3+: GNU GPL "
"версии 3 или выше E<lt>https://gnu.org/licenses/gpl.ru.htmlE<gt>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Это свободное программное обеспечение: вы можете изменять и распространять "
"его. Не предоставляется НИКАКИХ ГАРАНТИЙ в той мере, в которой это разрешено "
"законом."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМОТРИТЕ ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<getent>(1)"
msgstr "B<getent>(1)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Full documentation E<lt>https://www.gnu.org/software/coreutils/groupsE<gt>"
msgstr ""
"Полная документация на E<lt>https://www.gnu.org/software/coreutils/"
"groupsE<gt>,"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "or available locally via: info \\(aq(coreutils) groups invocation\\(aq"
msgstr ""
"также доступна локально посредством info \\(aq(coreutils) groups invocation\\"
"(aq"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "September 2022"
msgstr "сентябрь 2022 г."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#. type: Plain text
#: debian-bookworm
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2022 Free Software Foundation, Inc. Лицензия GPLv3+: GNU GPL "
"версии 3 или выше E<lt>https://gnu.org/licenses/gpl.ru.htmlE<gt>."

#. type: TH
#: debian-unstable
#, no-wrap
msgid "October 2024"
msgstr "октябрь 2024 г."

#. type: TH
#: debian-unstable mageia-cauldron
#, no-wrap
msgid "GNU coreutils 9.5"
msgstr "GNU coreutils 9.5"

#. type: Plain text
#: debian-unstable mageia-cauldron
msgid ""
"Copyright \\(co 2024 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2024 Free Software Foundation, Inc. Лицензия GPLv3+: GNU GPL "
"версии 3 или выше E<lt>https://gnu.org/licenses/gpl.ru.htmlE<gt>."

#. type: TH
#: fedora-42 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "January 2024"
msgid "January 2025"
msgstr "январь 2024 г."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "March 2024"
msgstr "март 2024 г."

#. type: TH
#: opensuse-leap-16-0
#, no-wrap
msgid "January 2024"
msgstr "январь 2024 г."

#. type: TH
#: opensuse-leap-16-0
#, no-wrap
msgid "GNU coreutils 9.4"
msgstr "GNU coreutils 9.4"

#. type: Plain text
#: opensuse-leap-16-0
msgid ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2023 Free Software Foundation, Inc. Лицензия GPLv3+: GNU GPL "
"версии 3 или выше E<lt>https://gnu.org/licenses/gpl.ru.htmlE<gt>."
