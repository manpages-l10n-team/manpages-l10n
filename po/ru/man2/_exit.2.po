# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2013, 2015-2017, 2019.
# Kirill Rekhov <krekhov.dev@gmail.com>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2025-02-28 16:32+0100\n"
"PO-Revision-Date: 2024-12-21 19:49+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <debian-l10n-russian@lists.debian.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "_exit"
msgstr "_exit"

#. type: TH
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "2024-07-23"
msgstr "23 июля 2024 г."

#. type: TH
#: archlinux
#, fuzzy, no-wrap
#| msgid "Linux man-pages 6.03"
msgid "Linux man-pages 6.12"
msgstr "Справочные страницы Linux 6.03"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "НАИМЕНОВАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "_exit, _Exit - terminate the calling process"
msgstr "_exit, _Exit - завершает вызывающий процесс"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "БИБЛИОТЕКА"

#. type: Plain text
#: archlinux opensuse-tumbleweed
#, fuzzy
#| msgid "Standard C library (I<libc>, I<-lc>)"
msgid "Standard C library (I<libc>,\\ I<-lc>)"
msgstr "Стандартная библиотека языка C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ОБЗОР"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>unistd.hE<gt>>\n"
msgstr "B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<[[noreturn]] void _exit(int >I<status>B<);>\n"
msgstr "B<[[noreturn]] void _exit(int >I<status>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>stdlib.hE<gt>>\n"
msgstr "B<#include E<lt>stdlib.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<[[noreturn]] void _Exit(int >I<status>B<);>\n"
msgstr "B<[[noreturn]] void _Exit(int >I<status>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Требования макроса тестирования свойств для glibc (см. "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<_Exit>():"
msgstr "B<_Exit>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"
msgstr "    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<_exit>()  terminates the calling process \"immediately\".  Any open file "
"descriptors belonging to the process are closed.  Any children of the "
"process are inherited by B<init>(1)  (or by the nearest \"subreaper\" "
"process as defined through the use of the B<prctl>(2)  "
"B<PR_SET_CHILD_SUBREAPER> operation).  The process's parent is sent a "
"B<SIGCHLD> signal."
msgstr ""
"B<_exit>() «безотлагательно» завершает вызывающий процесс. Все открытые "
"дескрипторы файлов, принадлежащие процессу, закрываются. Все его дочерние "
"процессы наследуются B<init>(1) (или ближайшим «собирающим» процессом, "
"определённым вызовом B<prctl>(2) с операцией "
"B<PR_SET_CHILD_SUBREAPER>).Родительскому процессу посылается сигнал "
"B<SIGCHLD>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The value I<status & 0xFF> is returned to the parent process as the "
"process's exit status, and can be collected by the parent using one of the "
"B<wait>(2)  family of calls."
msgstr ""
"Значение I<status & 0xFF> возвращается родительскому процессу как код "
"завершаемого процесса и может быть получено родительским процессом с помощью "
"одного из семейства вызовов B<wait>(2)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The function B<_Exit>()  is equivalent to B<_exit>()."
msgstr "Функция B<_Exit>() эквивалентна B<_exit>()."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "These functions do not return."
msgstr "Эти функции не выполняют возврат."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<_exit>()"
msgstr "B<_exit>()"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<_Exit>()"
msgstr "B<_Exit>()"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr "C11, POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ИСТОРИЯ"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2001, SVr4, 4.3BSD."
msgstr "POSIX.1-2001, SVr4, 4.3BSD."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<_Exit>()  was introduced by C99."
msgstr "B<_Exit>() была добавлена в стандарт C99."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ПРИМЕЧАНИЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For a discussion on the effects of an exit, the transmission of exit status, "
"zombie processes, signals sent, and so on, see B<exit>(3)."
msgstr ""
"Обсуждение эффектов при завершении работы, передачу кода выхода, зомби-"
"процессы, сигналы и т. п., смотрите в B<exit>(3)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The function B<_exit>()  is like B<exit>(3), but does not call any functions "
"registered with B<atexit>(3)  or B<on_exit>(3).  Open B<stdio>(3)  streams "
"are not flushed.  On the other hand, B<_exit>()  does close open file "
"descriptors, and this may cause an unknown delay, waiting for pending output "
"to finish.  If the delay is undesired, it may be useful to call functions "
"like B<tcflush>(3)  before calling B<_exit>().  Whether any pending I/O is "
"canceled, and which pending I/O may be canceled upon B<_exit>(), is "
"implementation-dependent."
msgstr ""
"Функция B<_exit>() подобна B<exit>(3), но не вызывает никаких функций, "
"зарегистрированных с помощью B<atexit>(3) или B<on_exit>(3). Открытые потоки "
"посредством B<stdio>(3) не сбрасываются. С другой стороны, B<_exit>() "
"закрывает открытые дескрипторы файлов, а это может привести к неопределенной "
"задержке, так как происходит ожидание завершения вывода данных. Если "
"задержка нежелательна, то может быть полезным перед вызовом B<_exit>() "
"вызывать функцию типа B<tcflush>(3). Будет ли отмена ожидания ввод-вывода, а "
"также какие именно ожидающие операции ввода-вывода будут завершены при "
"вызове B<_exit>, зависит от реализации."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "C library/kernel differences"
msgstr "Отличия между библиотекой C и ядром"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The text above in DESCRIPTION describes the traditional effect of "
"B<_exit>(), which is to terminate a process, and these are the semantics "
"specified by POSIX.1 and implemented by the C library wrapper function.  On "
"modern systems, this means termination of all threads in the process."
msgstr ""
"Текст выше в разделе DESCRIPTION описывает традиционный эффект B<_exit>(), "
"который заключается в завершении процесса, и это семантика, определенная "
"POSIX.1 и реализованная библиотечной оберткой на C. На современных системах "
"это означает завершение всех потоков в процессе."

#.  _exit() is used by pthread_exit() to terminate the calling thread
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"By contrast with the C library wrapper function, the raw Linux B<_exit>()  "
"system call terminates only the calling thread, and actions such as "
"reparenting child processes or sending B<SIGCHLD> to the parent process are "
"performed only if this is the last thread in the thread group."
msgstr ""
"В отличие от функции-оболочки библиотеки C, системный вызов B<_exit>() в "
"Linux завершает только вызывающий поток, и такие действия, как смена "
"родителя для дочерних процессов или отправка B<SIGCHLD> родительскому "
"процессу, выполняются только если это последний поток в группе потоков."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Up to glibc 2.3, the B<_exit>()  wrapper function invoked the kernel system "
"call of the same name.  Since glibc 2.3, the wrapper function invokes "
"B<exit_group>(2), in order to terminate all of the threads in a process."
msgstr ""
"До glibc 2.3 функция-обертка B<_exit>() вызывала системный вызов ядра с "
"таким же именем. Начиная с glibc 2.3, функция-обертка вызывает "
"B<exit_group>(2), чтобы завершить все нити процесса."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМОТРИТЕ ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<execve>(2), B<exit_group>(2), B<fork>(2), B<kill>(2), B<wait>(2), "
"B<wait4>(2), B<waitpid>(2), B<atexit>(3), B<exit>(3), B<on_exit>(3), "
"B<termios>(3)"
msgstr ""
"B<execve>(2), B<exit_group>(2), B<fork>(2), B<kill>(2), B<wait>(2), "
"B<wait4>(2), B<waitpid>(2), B<atexit>(3), B<exit>(3), B<on_exit>(3), "
"B<termios>(3)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-01-22"
msgstr "22 января 2023 г."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Справочные страницы Linux 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Стандартная библиотека языка C (I<libc>, I<-lc>)"

#. type: Plain text
#: debian-bookworm
msgid ""
"POSIX.1-2001, POSIX.1-2008, SVr4, 4.3BSD.  The function B<_Exit>()  was "
"introduced by C99."
msgstr ""
"POSIX.1-2001, POSIX.1-2008, SVr4, 4.3BSD. Функция B<_Exit>() была "
"представлена в C99."

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "2024-05-02"
msgstr "2 мая 2024 г."

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Справочные страницы Linux 6.9.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 октября 2023 г."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Справочные страницы Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Справочные страницы Linux (невыпущенные)"
