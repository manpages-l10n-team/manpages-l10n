# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Artyom Kunyov <artkun@guitarplayer.ru>, 2012.
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2012, 2017.
# Dmitry Bolkhovskikh <d20052005@yandex.ru>, 2017.
# Katrin Kutepova <blackkatelv@gmail.com>, 2018.
# Konstantin Shvaykovskiy <kot.shv@gmail.com>, 2012.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017, 2019.
# Kirill Rekhov <krekhov.dev@gmail.com>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2025-02-28 16:38+0100\n"
"PO-Revision-Date: 2024-12-21 19:49+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <debian-l10n-russian@lists.debian.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "landlock_add_rule"
msgstr "landlock_add_rule"

#. type: TH
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "2024-08-21"
msgstr "21 августа 2024 г."

#. type: TH
#: archlinux
#, fuzzy, no-wrap
#| msgid "Linux man-pages 6.03"
msgid "Linux man-pages 6.12"
msgstr "Справочные страницы Linux 6.03"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "НАИМЕНОВАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "landlock_add_rule - add a new Landlock rule to a ruleset"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "БИБЛИОТЕКА"

#. type: Plain text
#: archlinux opensuse-tumbleweed
#, fuzzy
#| msgid "Standard C library (I<libc>, I<-lc>)"
msgid "Standard C library (I<libc>,\\ I<-lc>)"
msgstr "Стандартная библиотека языка C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ОБЗОР"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>linux/landlock.hE<gt>>  /* Definition of B<LANDLOCK_*> constants */\n"
"B<#include E<lt>sys/syscall.hE<gt>>     /* Definition of B<SYS_*> constants */\n"
msgstr ""
"B<#include E<lt>linux/landlock.hE<gt>>  /* определения констант B<LANDLOCK_*> */\n"
"B<#include E<lt>sys/syscall.hE<gt>>     /* определения констант B<SYS_*> */\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int syscall(SYS_landlock_add_rule, int >I<ruleset_fd>B<,>\n"
"B<            enum landlock_rule_type >I<rule_type>B<,>\n"
"B<            const void *>I<rule_attr>B<, uint32_t >I<flags>B<);>\n"
msgstr ""
"B<int syscall(SYS_landlock_add_rule, int >I<ruleset_fd>B<,>\n"
"B<            enum landlock_rule_type >I<rule_type>B<,>\n"
"B<            const void *>I<rule_attr>B<, uint32_t >I<flags>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid ""
"A Landlock rule describes an action on an object which the process intends "
"to perform.  A set of rules is aggregated in a ruleset, which can then "
"restrict the thread enforcing it, and its future children."
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid ""
"The B<landlock_add_rule>()  system call adds a new Landlock rule to an "
"existing ruleset.  See B<landlock>(7)  for a global overview."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<ruleset_fd> is a Landlock ruleset file descriptor obtained with "
"B<landlock_create_ruleset>(2)."
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid ""
"I<rule_type> identifies the structure type pointed to by I<rule_attr>.  "
"Currently, Linux supports the following I<rule_type> values:"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<LANDLOCK_RULE_PATH_BENEATH>"
msgstr "B<LANDLOCK_RULE_PATH_BENEATH>"

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid ""
"For these rules, the object is a file hierarchy, and the related filesystem "
"actions are defined with I<filesystem access rights>."
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "In this case, I<rule_attr> points to the following structure:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"struct landlock_path_beneath_attr {\n"
"    __u64 allowed_access;\n"
"    __s32 parent_fd;\n"
"} __attribute__((packed));\n"
msgstr ""
"struct landlock_path_beneath_attr {\n"
"    __u64 allowed_access;\n"
"    __s32 parent_fd;\n"
"} __attribute__((packed));\n"

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid ""
"I<allowed_access> contains a bitmask of allowed filesystem actions, which "
"can be applied on the given I<parent_fd> (see B<Filesystem actions> in "
"B<landlock>(7))."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<parent_fd> is an opened file descriptor, preferably with the I<O_PATH> "
"flag, which identifies the parent directory of the file hierarchy or just a "
"file."
msgstr ""

#. type: TP
#: archlinux opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<LANDLOCK_RULE_PATH_BENEATH>"
msgid "B<LANDLOCK_RULE_NET_PORT>"
msgstr "B<LANDLOCK_RULE_PATH_BENEATH>"

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid ""
"For these rules, the object is a TCP port, and the related actions are "
"defined with I<network access rights>."
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "struct landlock_path_beneath_attr {\n"
#| "    __u64 allowed_access;\n"
#| "    __s32 parent_fd;\n"
#| "} __attribute__((packed));\n"
msgid ""
"struct landlock_net_port_attr {\n"
"    __u64 allowed_access;\n"
"    __u64 port;\n"
"};\n"
msgstr ""
"struct landlock_path_beneath_attr {\n"
"    __u64 allowed_access;\n"
"    __s32 parent_fd;\n"
"} __attribute__((packed));\n"

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid ""
"I<allowed_access> contains a bitmask of allowed network actions, which can "
"be applied on the given port."
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "I<port> is the network port in host endianness."
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid ""
"It should be noted that port 0 passed to B<bind>(2)  will bind to an "
"available port from the ephemeral port range.  This can be configured in the "
"I</proc/sys/net/ipv4/ip_local_port_range> sysctl (also used for IPv6)."
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid ""
"A Landlock rule with port 0 and the B<LANDLOCK_ACCESS_NET_BIND_TCP> right "
"means that requesting to bind on port 0 is allowed and it will automatically "
"translate to binding on the related port range."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<flags> must be 0."
msgstr "I<flags> должен быть равен 0."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "On success, zero is returned.  On error, -1 is returned and I<errno> is "
#| "set to indicate the error."
msgid ""
"On success, B<landlock_add_rule>()  returns 0.  On error, -1 is returned and "
"I<errno> is set to indicate the error."
msgstr ""
"При успешном выполнении возвращается ноль. В случае ошибки возвращается -1, "
"а I<errno> устанавливается в соответствующее значение ошибки."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid "The B<lio_listio>()  function may fail for the following reasons:"
msgid "B<landlock_add_rule>()  can fail for the following reasons:"
msgstr ""
"Функция B<lio_listio>() может завершиться с ошибкой по следующим причинам:"

#. type: TP
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "B<EAFNOSUPPORT>"
msgstr "B<EAFNOSUPPORT>"

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid ""
"I<rule_type> is B<LANDLOCK_RULE_NET_PORT>, but TCP is not supported by the "
"running kernel."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EOPNOTSUPP>"
msgstr "B<EOPNOTSUPP>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Landlock is supported by the kernel but disabled at boot time."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "I<flags> is not 0."
msgstr "Значение I<flags> не равно 0."

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid ""
"The rule accesses are inconsistent (i.e., I<rule_attr-E<gt>allowed_access> "
"is not a subset of the ruleset handled accesses)."
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid ""
"In I<\\%struct\\ landlock_path_beneath_attr>, the rule accesses are not "
"applicable to the file (i.e., some access rights in I<\\%rule_attr-"
"E<gt>allowed_access> are only applicable to directories, but I<\\%rule_attr-"
"E<gt>parent_fd> does not refer to a directory)."
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid ""
"In I<\\%struct\\ landlock_net_port_attr>, the port number is greater than "
"65535."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMSG>"
msgstr "B<ENOMSG>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Empty accesses (i.e., I<rule_attr-E<gt>allowed_access> is 0)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EBADF>"
msgstr "B<EBADF>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<ruleset_fd> is not a file descriptor for the current thread, or a member "
"of I<rule_attr> is not a file descriptor as expected."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EBADFD>"
msgstr "B<EBADFD>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "I<fd> is not an open file descriptor; or I<cmd> is B<F_LOCK> or "
#| "B<F_TLOCK> and I<fd> is not a writable file descriptor."
msgid ""
"I<ruleset_fd> is not a ruleset file descriptor, or a member of I<rule_attr> "
"is not the expected file descriptor type."
msgstr ""
"Значение I<fd> не является открытым файловым дескриптором, или значение "
"I<cmd> равно B<F_LOCK> или B<F_TLOCK> и файловый дескриптор I<fd> не открыт "
"на запись."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<ruleset_fd> has no write access to the underlying ruleset."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid "I<sem> is not a valid semaphore."
msgid "I<rule_attr> was not a valid address."
msgstr "Значение I<sem> не является корректным для семафора."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Linux."
msgstr "Linux."

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ИСТОРИЯ"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Linux 5.13."
msgstr "Linux 5.13."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "ПРИМЕРЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "See B<landlock>(7)."
msgstr "Смотрите B<landlock>(7)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМОТРИТЕ ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<landlock_create_ruleset>(2), B<landlock_restrict_self>(2), B<landlock>(7)"
msgstr ""
"B<landlock_create_ruleset>(2), B<landlock_restrict_self>(2), B<landlock>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-10"
msgstr "10 февраля 2023 г."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Справочные страницы Linux 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Стандартная библиотека языка C (I<libc>, I<-lc>)"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid ""
"A Landlock rule describes an action on an object.  An object is currently a "
"file hierarchy, and the related filesystem actions are defined with a set of "
"access rights.  This B<landlock_add_rule>()  system call enables adding a "
"new Landlock rule to an existing ruleset created with "
"B<landlock_create_ruleset>(2).  See B<landlock>(7)  for a global overview."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid ""
"I<rule_type> identifies the structure type pointed to by I<rule_attr>.  "
"Currently, Linux supports the following I<rule_type> value:"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid ""
"This defines the object type as a file hierarchy.  In this case, "
"I<rule_attr> points to the following structure:"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid ""
"I<allowed_access> contains a bitmask of allowed filesystem actions for this "
"file hierarchy (see B<Filesystem actions> in B<landlock>(7))."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "On success, B<landlock_add_rule>()  returns 0."
msgstr ""

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid "The B<lio_listio>()  function may fail for the following reasons:"
msgid "B<landlock_add_rule>()  can failed for the following reasons:"
msgstr ""
"Функция B<lio_listio>() может завершиться с ошибкой по следующим причинам:"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid ""
"I<flags> is not 0, or the rule accesses are inconsistent (i.e., I<rule_attr-"
"E<gt>allowed_access> is not a subset of the ruleset handled accesses)."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#. type: Plain text
#: debian-bookworm
msgid "Landlock was added in Linux 5.13."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "This system call is Linux-specific."
msgstr "Данный вызов есть только в Linux."

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "2024-05-02"
msgstr "2 мая 2024 г."

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Справочные страницы Linux 6.9.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 октября 2023 г."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Справочные страницы Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Справочные страницы Linux (невыпущенные)"
