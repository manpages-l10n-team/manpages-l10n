# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# aereiae <aereiae@gmail.com>, 2014.
# Alexey <a.chepugov@gmail.com>, 2015.
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2013-2017.
# Dmitriy S. Seregin <dseregin@59.ru>, 2013.
# Dmitry Bolkhovskikh <d20052005@yandex.ru>, 2017.
# ITriskTI <ITriskTI@gmail.com>, 2013.
# Max Is <ismax799@gmail.com>, 2016.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
# Малянов Евгений Викторович <maljanow@outlook.com>, 2014.
# Kirill Rekhov <krekhov.dev@gmail.com>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2025-02-28 16:40+0100\n"
"PO-Revision-Date: 2024-12-21 19:49+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <debian-l10n-russian@lists.debian.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "mremap"
msgstr "mremap"

#. type: TH
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "2024-11-13"
msgstr "13 ноября 2024 г."

#. type: TH
#: archlinux
#, fuzzy, no-wrap
#| msgid "Linux man-pages 6.03"
msgid "Linux man-pages 6.12"
msgstr "Справочные страницы Linux 6.03"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "НАИМЕНОВАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "mremap - remap a virtual memory address"
msgstr "mremap - изменяет отображение адреса виртуальной памяти"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "БИБЛИОТЕКА"

#. type: Plain text
#: archlinux opensuse-tumbleweed
#, fuzzy
#| msgid "Standard C library (I<libc>, I<-lc>)"
msgid "Standard C library (I<libc>,\\ I<-lc>)"
msgstr "Стандартная библиотека языка C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ОБЗОР"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#define _GNU_SOURCE>         /* See feature_test_macros(7) */\n"
"B<#include E<lt>sys/mman.hE<gt>>\n"
msgstr ""
"B<#define _GNU_SOURCE>         /* Смотрите feature_test_macros(7) */\n"
"B<#include E<lt>sys/mman.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<void *mremap(void *>I<old_address>B<, size_t >I<old_size>B<,>\n"
#| "B<             size_t >I<new_size>B<, int >I<flags>B<, ... /* void *>I<new_address>B< */);>\n"
msgid ""
"B<void *mremap(void >I<old_address>B<[.>I<old_size>B<], size_t >I<old_size>B<,>\n"
"B<             size_t >I<new_size>B<, int >I<flags>B<, ... /* void *>I<new_address>B< */);>\n"
msgstr ""
"B<void *mremap(void *>I<old_address>B<, size_t >I<old_size>B<,>\n"
"B<             size_t >I<new_size>B<, int >I<flags>B<, ... /* void *>I<new_address>B< */);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<mremap>()  expands (or shrinks) an existing memory mapping, potentially "
"moving it at the same time (controlled by the I<flags> argument and the "
"available virtual address space)."
msgstr ""
"Вызов B<mremap>() увеличивает (или уменьшает) размер существующего "
"отображения памяти, при необходимости, перемещая его (это контролируется "
"аргументом I<flags> и доступным виртуальным адресным пространством)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<old_address> is the old address of the virtual memory block that you want "
"to expand (or shrink).  Note that I<old_address> has to be page aligned.  "
"I<old_size> is the old size of the virtual memory block.  I<new_size> is the "
"requested size of the virtual memory block after the resize.  An optional "
"fifth argument, I<new_address>, may be provided; see the description of "
"B<MREMAP_FIXED> below."
msgstr ""
"В I<old_address> указывается старый адрес блока виртуальной памяти, который "
"вы хотите изменить. Заметим, что I<old_address> должен быть выровнен по "
"границе страницы. В I<old_size> задаётся старый размер блока виртуальной "
"памяти. В I<new_size> задаётся запрашиваемый размер блока виртуальной памяти "
"после изменения. Описание необязательного пятого аргумента I<new_address> "
"смотрите далее в параграфе о B<MREMAP_FIXED>."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "If the value of I<old_size> is zero, and I<old_address> refers to a "
#| "shareable mapping (see B<mmap>(2)  B<MAP_SHARED>), then B<mremap>()  will "
#| "create a new mapping of the same pages.  I<new_size> will be the size of "
#| "the new mapping and the location of the new mapping may be specified with "
#| "I<new_address>; see the description of B<MREMAP_FIXED> below.  If a new "
#| "mapping is requested via this method, then the B<MREMAP_MAYMOVE> flag "
#| "must also be specified."
msgid ""
"If the value of I<old_size> is zero, and I<old_address> refers to a "
"shareable mapping (see the description of B<MAP_SHARED> in B<mmap>(2)), then "
"B<mremap>()  will create a new mapping of the same pages.  I<new_size> will "
"be the size of the new mapping and the location of the new mapping may be "
"specified with I<new_address>; see the description of B<MREMAP_FIXED> "
"below.  If a new mapping is requested via this method, then the "
"B<MREMAP_MAYMOVE> flag must also be specified."
msgstr ""
"Если значение I<old_size> равно нулю и I<old_address> указывает на общее "
"отображение (смотрите B<mmap>(2)  B<MAP_SHARED>), то B<mremap>() создаст "
"новое отображение тех же страниц.Размер нового отображения будет равен "
"значению I<new_size>, а расположение можно указать в I<new_address>; "
"смотрите описание B<MREMAP_FIXED> далее. Если новое отображение "
"запрашивается через этот метод, то также должен быть указан флаг "
"B<MREMAP_MAYMOVE>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The I<flags> bit-mask argument may be 0, or include the following flags:"
msgstr ""
"Аргумент битовой маски I<flags> может быть равен 0 или содержать следующие "
"флаги:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<MREMAP_MAYMOVE>"
msgstr "B<MREMAP_MAYMOVE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"By default, if there is not sufficient space to expand a mapping at its "
"current location, then B<mremap>()  fails.  If this flag is specified, then "
"the kernel is permitted to relocate the mapping to a new virtual address, if "
"necessary.  If the mapping is relocated, then absolute pointers into the old "
"mapping location become invalid (offsets relative to the starting address of "
"the mapping should be employed)."
msgstr ""
"По умолчанию, если для расширения отображения недостаточно пространства в "
"текущем расположении, то вызов B<mremap>() завершается с ошибкой. Если "
"указан флаг, то, если нужно, ядру разрешается переместить отображение на "
"новый виртуальный адрес. Если отображение перемещено, то абсолютные "
"указатели в старом расположении отображения становятся недействительными "
"(должно быть выполнено смещение относительно начального адреса отображения)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<MREMAP_FIXED> (since Linux 2.3.31)"
msgstr "B<MREMAP_FIXED> (начиная с Linux 2.3.31)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This flag serves a similar purpose to the B<MAP_FIXED> flag of B<mmap>(2).  "
"If this flag is specified, then B<mremap>()  accepts a fifth argument, "
"I<void\\ *new_address>, which specifies a page-aligned address to which the "
"mapping must be moved.  Any previous mapping at the address range specified "
"by I<new_address> and I<new_size> is unmapped."
msgstr ""
"Этот флаг играет ту же роль, что и B<MAP_FIXED> для B<mmap>(2). Если указан "
"этот флаг, то B<mremap>() учитывает пятый аргумент I<void\\ *new_address>, в "
"котором задан выровненный на страницу адрес, куда должно быть перемещено "
"отображение. Все предыдущие отображения в адресном диапазоне, задаваемом "
"I<new_address> и I<new_size>, удаляются."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<MREMAP_FIXED> was specified without also specifying B<MREMAP_MAYMOVE>;"
msgid ""
"If B<MREMAP_FIXED> is specified, then B<MREMAP_MAYMOVE> must also be "
"specified."
msgstr "задан B<MREMAP_FIXED> без B<MREMAP_MAYMOVE>;"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<MREMAP_DONTUNMAP> (since Linux 5.7)"
msgstr "B<MREMAP_DONTUNMAP> (начиная с Linux 5.7)"

#.  commit e346b3813067d4b17383f975f197a9aa28a3b077
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This flag, which must be used in conjunction with B<MREMAP_MAYMOVE>, remaps "
"a mapping to a new address but does not unmap the mapping at I<old_address>."
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid ""
"The B<MREMAP_DONTUNMAP> flag can be used only with mappings that are not "
"B<VM_DONTEXPAND> or B<VM_MIXEDMAP>.  Before Linux 5.13, the "
"B<MREMAP_DONTUNMAP> flag could be used only with private anonymous mappings "
"(see the description of B<MAP_PRIVATE> and B<MAP_ANONYMOUS> in B<mmap>(2))."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"After completion, any access to the range specified by I<old_address> and "
"I<old_size> will result in a page fault.  The page fault will be handled by "
"a B<userfaultfd>(2)  handler if the address is in a range previously "
"registered with B<userfaultfd>(2).  Otherwise, the kernel allocates a zero-"
"filled page to handle the fault."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<MREMAP_DONTUNMAP> flag may be used to atomically move a mapping while "
"leaving the source mapped.  See NOTES for some possible applications of "
"B<MREMAP_DONTUNMAP>."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the memory segment specified by I<old_address> and I<old_size> is locked "
"(using B<mlock>(2)  or similar), then this lock is maintained when the "
"segment is resized and/or relocated.  As a consequence, the amount of memory "
"locked by the process may change."
msgstr ""
"Если сегмент памяти, указанный I<old_address> и I<old_size>, заблокирован (с "
"помощью B<mlock>(2) или подобного вызова), то эта блокировка останется при "
"изменении/перемещении сегмента. Следовательно, количество заблокированной "
"процессом памяти может измениться."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "On success B<mremap>()  returns a pointer to the new virtual memory "
#| "area.  On error, the value B<MAP_FAILED> (that is, I<(void\\ *)\\ -1>) is "
#| "returned, and I<errno> is set appropriately."
msgid ""
"On success B<mremap>()  returns a pointer to the new virtual memory area.  "
"On error, the value B<MAP_FAILED> (that is, I<(void\\ *)\\ -1>) is returned, "
"and I<errno> is set to indicate the error."
msgstr ""
"При успешном выполнении B<mremap>() возвращается указатель на новую "
"виртуальную область памяти. При ошибке, возвращается значение B<MAP_FAILED> "
"(то есть I<(void\\ *)\\ -1>), а I<errno> устанавливается в соответствующее "
"значение."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EAGAIN>"
msgstr "B<EAGAIN>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The caller tried to expand a memory segment that is locked, but this was not "
"possible without exceeding the B<RLIMIT_MEMLOCK> resource limit."
msgstr ""
"Вызывающий пытается расширить заблокированный сегмент памяти, но это "
"невозможно без превышения предела ресурса B<RLIMIT_MEMLOCK>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Some address in the range I<old_address> to I<old_address>+I<old_size> is an "
"invalid virtual memory address for this process.  You can also get B<EFAULT> "
"even if there exist mappings that cover the whole address space requested, "
"but those mappings are of different types."
msgstr ""
"Один из адресов в диапазоне от I<old_address> до I<old_address>+I<old_size> "
"является некорректным адресом виртуальной памяти для этого процесса. Также "
"вы можете получить B<EFAULT> даже если существующие отображения покрывают "
"всё запрошенное адресное пространство, но имеют различные типы."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "An invalid argument was given.  Possible causes are:"
msgstr "Указан недопустимый аргумент. Возможные причины:"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr "\\[bu]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<old_address> was not page aligned;"
msgstr "не выровнено значение адреса I<old_address>;"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "a value other than B<MREMAP_MAYMOVE> or B<MREMAP_FIXED> was specified in "
#| "I<flags>;"
msgid ""
"a value other than B<MREMAP_MAYMOVE> or B<MREMAP_FIXED> or "
"B<MREMAP_DONTUNMAP> was specified in I<flags>;"
msgstr ""
"помимо B<MREMAP_MAYMOVE> или B<MREMAP_FIXED> в I<flags> указано что-то ещё;"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<new_size> was zero;"
msgstr "значение I<new_size> равно нулю;"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<new_size> or I<new_address> was invalid;"
msgstr "некорректное значение I<new_size> или I<new_address>;"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"the new address range specified by I<new_address> and I<new_size> overlapped "
"the old address range specified by I<old_address> and I<old_size>;"
msgstr ""
"новый диапазон адресов, указанный I<new_address> и I<new_size>, перекрывает "
"старый диапазон адресов, указанный I<old_address> и I<old_size>;"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<MREMAP_FIXED> was specified without also specifying B<MREMAP_MAYMOVE>;"
msgid ""
"B<MREMAP_FIXED> or B<MREMAP_DONTUNMAP> was specified without also specifying "
"B<MREMAP_MAYMOVE>;"
msgstr "задан B<MREMAP_FIXED> без B<MREMAP_MAYMOVE>;"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<MREMAP_DONTUNMAP> was specified, but one or more pages in the range "
"specified by I<old_address> and I<old_size> were not private anonymous;"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<MREMAP_DONTUNMAP> was specified and I<old_size> was not equal to "
"I<new_size>;"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<old_size> was zero and I<old_address> does not refer to a shareable "
"mapping (but see BUGS);"
msgstr ""
"значение I<old_size> равно нулю и I<old_address> не указывает на общее "
"отображение (но смотрите ДЕФЕКТЫ);"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<old_size> was zero and the B<MREMAP_MAYMOVE> flag was not specified."
msgstr "значение I<old_size> равно нулю и не указан флаг B<MREMAP_MAYMOVE>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr "B<ENOMEM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Not enough memory was available to complete the operation.  Possible causes "
"are:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The memory area cannot be expanded at the current virtual address, and the "
"B<MREMAP_MAYMOVE> flag is not set in I<flags>.  Or, there is not enough "
"(virtual) memory available."
msgstr ""
"Область памяти не может быть расширена от текущего виртуального адреса и в "
"I<flags> не указано значение B<MREMAP_MAYMOVE>. Или недостаточно свободной "
"(виртуальной) памяти."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<MREMAP_DONTUNMAP> was used causing a new mapping to be created that would "
"exceed the (virtual) memory available.  Or, it would exceed the maximum "
"number of allowed mappings."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Linux."
msgstr "Linux."

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ИСТОРИЯ"

#. #-#-#-#-#  archlinux: mremap.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  4.2BSD had a (never actually implemented)
#.  .BR mremap (2)
#.  call with completely different semantics.
#.  .P
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: mremap.2.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  debian-unstable: mremap.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  4.2BSD had a (never actually implemented)
#.  .BR mremap (2)
#.  call with completely different semantics.
#.  .P
#. type: Plain text
#. #-#-#-#-#  fedora-42: mremap.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  4.2BSD had a (never actually implemented)
#.  .BR mremap (2)
#.  call with completely different semantics.
#.  .P
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: mremap.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  4.2BSD had a (never actually implemented)
#.  .BR mremap (2)
#.  call with completely different semantics.
#.  .P
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: mremap.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  4.2BSD had a (never actually implemented)
#.  .BR mremap (2)
#.  call with completely different semantics.
#.  .P
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-16-0: mremap.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  4.2BSD had a (never actually implemented)
#.  .BR mremap (2)
#.  call with completely different semantics.
#.  .P
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: mremap.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  4.2BSD had a (never actually implemented)
#.  .BR mremap (2)
#.  call with completely different semantics.
#.  .P
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Prior to version 2.4, glibc did not expose the definition of "
#| "B<MREMAP_FIXED>, and the prototype for B<mremap>()  did not allow for the "
#| "I<new_address> argument."
msgid ""
"Prior to glibc 2.4, glibc did not expose the definition of B<MREMAP_FIXED>, "
"and the prototype for B<mremap>()  did not allow for the I<new_address> "
"argument."
msgstr ""
"До версии 2.4, в glibc не был определён флаг B<MREMAP_FIXED>, а прототип "
"B<mremap>() не позволял указывать аргумент I<new_address>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ПРИМЕЧАНИЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<mremap>()  changes the mapping between virtual addresses and memory "
"pages.  This can be used to implement a very efficient B<realloc>(3)."
msgstr ""
"B<mremap>() изменяет отображение между виртуальными адресами и страницами "
"памяти. Это можно использовать при реализации очень эффективной функции "
"B<realloc>(3)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "In Linux the memory is divided into pages.  A user process has (one or)  "
#| "several linear virtual memory segments.  Each virtual memory segment has "
#| "one or more mappings to real memory pages (in the page table).  Each "
#| "virtual memory segment has its own protection (access rights), which may "
#| "cause a segmentation violation if the memory is accessed incorrectly "
#| "(e.g., writing to a read-only segment).  Accessing virtual memory outside "
#| "of the segments will also cause a segmentation violation."
msgid ""
"In Linux, memory is divided into pages.  A process has (one or)  several "
"linear virtual memory segments.  Each virtual memory segment has one or more "
"mappings to real memory pages (in the page table).  Each virtual memory "
"segment has its own protection (access rights), which may cause a "
"segmentation violation (B<SIGSEGV>)  if the memory is accessed incorrectly "
"(e.g., writing to a read-only segment).  Accessing virtual memory outside of "
"the segments will also cause a segmentation violation."
msgstr ""
"В Linux память делится на страницы. Пользовательскому процессу выделяется "
"один или несколько непрерывных сегментов виртуальной памяти. Каждый из этих "
"сегментов имеет одно или несколько отображений в реальных страницах памяти "
"(в таблице страниц). У каждого виртуального сегмента памяти есть своя защита "
"(права доступа), которые можно нарушить, если произвести попытку "
"некорректного доступа (например, записать информацию в сегмент, который "
"доступен только для чтения). Доступ к виртуальной памяти за пределами "
"сегментов также приводит к ошибке сегментации."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If B<mremap>()  is used to move or expand an area locked with B<mlock>(2)  "
"or equivalent, the B<mremap>()  call will make a best effort to populate the "
"new area but will not fail with B<ENOMEM> if the area cannot be populated."
msgstr ""
"Если B<mremap>() используется для перемещения или расширения области, "
"заблокированной B<mlock>(2) или эквивалентом, то вызов B<mremap>() "
"постарается заполнить новую область, но не завершится с ошибкой B<ENOMEM>, "
"если область невозможно заполнить."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MREMAP_DONTUNMAP use cases"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Possible applications for B<MREMAP_DONTUNMAP> include:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Non-cooperative B<userfaultfd>(2): an application can yank out a virtual "
"address range using B<MREMAP_DONTUNMAP> and then employ a B<userfaultfd>(2)  "
"handler to handle the page faults that subsequently occur as other threads "
"in the process touch pages in the yanked range."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Garbage collection: B<MREMAP_DONTUNMAP> can be used in conjunction with "
"B<userfaultfd>(2)  to implement garbage collection algorithms (e.g., in a "
"Java virtual machine).  Such an implementation can be cheaper (and simpler)  "
"than conventional garbage collection techniques that involve marking pages "
"with protection B<PROT_NONE> in conjunction with the use of a B<SIGSEGV> "
"handler to catch accesses to those pages."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ОШИБКИ"

#.  commit dba58d3b8c5045ad89c1c95d33d01451e3964db7
#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Before Linux 4.14, if I<old_size> was zero and the mapping referred to by "
#| "I<old_address> was a private mapping (B<mmap>(2) B<MAP_PRIVATE>), "
#| "B<mremap>()  created a new private mapping unrelated to the original "
#| "mapping.  This behavior was unintended and probably unexpected in user-"
#| "space applications (since the intention of B<mremap>()  is to create a "
#| "new mapping based on the original mapping).  Since Linux 4.14, "
#| "B<mremap>()  fails with the error B<EINVAL> in this scenario."
msgid ""
"Before Linux 4.14, if I<old_size> was zero and the mapping referred to by "
"I<old_address> was a private mapping (see the description of B<MAP_PRIVATE> "
"in B<mmap>(2)), B<mremap>()  created a new private mapping unrelated to the "
"original mapping.  This behavior was unintended and probably unexpected in "
"user-space applications (since the intention of B<mremap>()  is to create a "
"new mapping based on the original mapping).  Since Linux 4.14, B<mremap>()  "
"fails with the error B<EINVAL> in this scenario."
msgstr ""
"До Linux 4.14, если I<old_size> равно нулю и отображение, на которое "
"указывает I<old_address> — частное отображение (B<mmap>(2) B<MAP_PRIVATE>), "
"то вызов B<mremap>() создавал новое частное отображение, не относящееся к "
"первоначальному отображению. Такое поведение не предусматривалось и, "
"вероятно, не ожидается в приложениях пользовательского пространства (так "
"предназначение B<mremap>() — создание нового отображения на основе "
"первоначального). Начиная с Linux 4.14, в этом случае B<mremap>() "
"завершается ошибкой B<EINVAL>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМОТРИТЕ ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<brk>(2), B<getpagesize>(2), B<getrlimit>(2), B<mlock>(2), B<mmap>(2), "
"B<sbrk>(2), B<malloc>(3), B<realloc>(3)"
msgstr ""
"B<brk>(2), B<getpagesize>(2), B<getrlimit>(2), B<mlock>(2), B<mmap>(2), "
"B<sbrk>(2), B<malloc>(3), B<realloc>(3)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Your favorite text book on operating systems for more information on paged "
"memory (e.g., I<Modern Operating Systems> by Andrew S.\\& Tanenbaum, "
"I<Inside Linux> by Randolph Bentson, I<The Design of the UNIX Operating "
"System> by Maurice J.\\& Bach)"
msgstr ""
"Описание страничной памяти в вашей любимой книге по операционным системам "
"(например, I<Современные операционные системы> Эндрю С.\\& Таненбаума, "
"I<Внутри Linux> Рэндольфа Бентсона, I<Архитектура операционной системы UNIX "
"> Мориса Дж.\\& Баха)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 февраля 2023 г."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Справочные страницы Linux 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Стандартная библиотека языка C (I<libc>, I<-lc>)"

#. type: Plain text
#: debian-bookworm
msgid ""
"If the value of I<old_size> is zero, and I<old_address> refers to a "
"shareable mapping (see B<mmap>(2)  B<MAP_SHARED>), then B<mremap>()  will "
"create a new mapping of the same pages.  I<new_size> will be the size of the "
"new mapping and the location of the new mapping may be specified with "
"I<new_address>; see the description of B<MREMAP_FIXED> below.  If a new "
"mapping is requested via this method, then the B<MREMAP_MAYMOVE> flag must "
"also be specified."
msgstr ""
"Если значение I<old_size> равно нулю и I<old_address> указывает на общее "
"отображение (смотрите B<mmap>(2)  B<MAP_SHARED>), то B<mremap>() создаст "
"новое отображение тех же страниц.Размер нового отображения будет равен "
"значению I<new_size>, а расположение можно указать в I<new_address>; "
"смотрите описание B<MREMAP_FIXED> далее. Если новое отображение "
"запрашивается через этот метод, то также должен быть указан флаг "
"B<MREMAP_MAYMOVE>."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid ""
"The B<MREMAP_DONTUNMAP> flag can be used only with private anonymous "
"mappings (see the description of B<MAP_PRIVATE> and B<MAP_ANONYMOUS> in "
"B<mmap>(2))."
msgstr ""

#.  4.2BSD had a (never actually implemented)
#.  .BR mremap (2)
#.  call with completely different semantics.
#. type: Plain text
#: debian-bookworm
msgid ""
"This call is Linux-specific, and should not be used in programs intended to "
"be portable."
msgstr ""
"Данный вызов существует только в Linux и не должен использоваться в "
"программах, которые должны быть переносимыми."

#.  commit dba58d3b8c5045ad89c1c95d33d01451e3964db7
#. type: Plain text
#: debian-bookworm
msgid ""
"Before Linux 4.14, if I<old_size> was zero and the mapping referred to by "
"I<old_address> was a private mapping (B<mmap>(2) B<MAP_PRIVATE>), "
"B<mremap>()  created a new private mapping unrelated to the original "
"mapping.  This behavior was unintended and probably unexpected in user-space "
"applications (since the intention of B<mremap>()  is to create a new mapping "
"based on the original mapping).  Since Linux 4.14, B<mremap>()  fails with "
"the error B<EINVAL> in this scenario."
msgstr ""
"До Linux 4.14, если I<old_size> равно нулю и отображение, на которое "
"указывает I<old_address> — частное отображение (B<mmap>(2) B<MAP_PRIVATE>), "
"то вызов B<mremap>() создавал новое частное отображение, не относящееся к "
"первоначальному отображению. Такое поведение не предусматривалось и, "
"вероятно, не ожидается в приложениях пользовательского пространства (так "
"предназначение B<mremap>() — создание нового отображения на основе "
"первоначального). Начиная с Linux 4.14, в этом случае B<mremap>() "
"завершается ошибкой B<EINVAL>."

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "2024-05-02"
msgstr "2 мая 2024 г."

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Справочные страницы Linux 6.9.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2024-01-16"
msgstr "16 января 2024 г."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Справочные страницы Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Справочные страницы Linux (невыпущенные)"
