# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <ccb@club-internet.fr>, 1997, 2002, 2003.
# Michel Quercia <quercia AT cal DOT enst DOT fr>, 1997.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999.
# Frédéric Delanoy <delanoy_f@yahoo.com>, 2000.
# Thierry Vignaud <tvignaud@mandriva.com>, 2000.
# Christophe Sauthier <christophe@sauthier.com>, 2001.
# Sébastien Blanchet, 2002.
# Jérôme Perzyna <jperzyna@yahoo.fr>, 2004.
# Aymeric Nys <aymeric AT nnx POINT com>, 2004.
# Alain Portal <aportal@univ-montp2.fr>, 2005, 2006.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006.
# Yves Rütschlé <l10n@rutschle.net>, 2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006.
# Julien Cristau <jcristau@debian.org>, 2006.
# Philippe Piette <foudre-blanche@skynet.be>, 2006.
# Jean-Baka Domelevo-Entfellner <domelevo@gmail.com>, 2006.
# Nicolas Haller <nicolas@boiteameuh.org>, 2006.
# Sylvain Archenault <sylvain.archenault@laposte.net>, 2006.
# Valéry Perrin <valery.perrin.debian@free.fr>, 2006.
# Jade Alglave <jade.alglave@ens-lyon.org>, 2006.
# Nicolas François <nicolas.francois@centraliens.net>, 2007.
# Alexandre Kuoch <alex.kuoch@gmail.com>, 2008.
# Lyes Zemmouche <iliaas@hotmail.fr>, 2008.
# Florentin Duneau <fduneau@gmail.com>, 2006, 2008, 2009, 2010.
# Alexandre Normand <aj.normand@free.fr>, 2010.
# David Prévot <david@tilapin.org>, 2010-2015.
msgid ""
msgstr ""
"Project-Id-Version: manpages-fr-extra-util-linux\n"
"POT-Creation-Date: 2024-03-29 10:47+0100\n"
"PO-Revision-Date: 2024-06-03 18:26+0200\n"
"Last-Translator: David Prévot <david@tilapin.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.5\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "UUID_CLEAR"
msgstr "UUID_CLEAR"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr "11 mai 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Programmer\\(aqs Manual"
msgstr "Manuel du programmeur"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: debian-bookworm
msgid "uuid_clear - reset value of UUID variable to the NULL value"
msgstr "uuid_clear - Mettre à NULL la valeur de la variable UUID"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: debian-bookworm
msgid "B<#include E<lt>uuid.hE<gt>>"
msgstr "B<#include E<lt>uuid.hE<gt>>"

#. type: Plain text
#: debian-bookworm
msgid "B<void uuid_clear(uuid_t >I<uu>B<);>"
msgstr "B<void uuid_clear(uuid_t >I<uu>B<);>"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<uuid_clear>() function sets the value of the supplied uuid variable "
"I<uu> to the NULL value."
msgstr ""
"La fonction B<uuid_clear>() définit la valeur de la variable d’UUID fournie "
"I<uu> à la valeur NULL."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AUTHORS"
msgstr "AUTEURS"

#. type: Plain text
#: debian-bookworm
msgid "Theodore Y. Ts\\(cqo"
msgstr "Theodore Y. Ts\\(cqo"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: debian-bookworm
msgid ""
"B<uuid>(3), B<uuid_compare>(3), B<uuid_copy>(3), B<uuid_generate>(3), "
"B<uuid_is_null>(3), B<uuid_parse>(3), B<uuid_unparse>(3)"
msgstr ""
"B<uuid>(3), B<uuid_compare>(3), B<uuid_copy>(3), B<uuid_generate>(3), "
"B<uuid_is_null>(3), B<uuid_parse>(3), B<uuid_unparse>(3)"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "REPORTING BUGS"
msgstr "SIGNALER DES BOGUES"

#. type: Plain text
#: debian-bookworm
msgid "For bug reports, use the issue tracker at"
msgstr ""
"Pour envoyer un rapport de bogue, utilisez le système de gestion des "
"problèmes à l'adresse"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AVAILABILITY"
msgstr "DISPONIBILITÉ"

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<libuuid> library is part of the util-linux package since version "
"2.15.1. It can be downloaded from"
msgstr ""
"La bibliothèque B<libuuid> fait partie du paquet util-linux depuis la "
"version 2.15.1. Elle peut être obtenue à l'adresse"
