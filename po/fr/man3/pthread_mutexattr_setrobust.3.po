# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010, 2012.
# Frédéric Hantrais <fhantrais@gmail.com>, 2013, 2014.
# Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2025-02-28 16:45+0100\n"
"PO-Revision-Date: 2023-07-25 16:53+0200\n"
"Last-Translator: Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Lokalize 22.12.3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "pthread_mutexattr_setrobust"
msgstr "pthread_mutexattr_setrobust"

#. type: TH
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "2024-07-23"
msgstr "23 juillet 2024"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr "Pages du manuel de Linux 6.12"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"pthread_mutexattr_getrobust, pthread_mutexattr_setrobust - get and set the "
"robustness attribute of a mutex attributes object"
msgstr ""
"pthread_mutexattr_getrobust, pthread_mutexattr_setrobust — Définir ou "
"obtenir l'attribut de robustesse d'un objet d'attributs de mutex"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHÈQUE"

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "POSIX threads library (I<libpthread>,\\ I<-lpthread>)"
msgstr "Bibliothèque de threads POSIX (I<libpthread>,\\ I<-lpthread>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>pthread.hE<gt>>\n"
msgstr "B<#include E<lt>pthread.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int pthread_mutexattr_getrobust(const pthread_mutexattr_t *>I<attr>B<,>\n"
"B<                                int *>I<robustness>B<);>\n"
"B<int pthread_mutexattr_setrobust(pthread_mutexattr_t *>I<attr>B<,>\n"
"B<                                int >I<robustness>B<);>\n"
msgstr ""
"B<int pthread_mutexattr_getrobust(const pthread_mutexattr_t *>I<attr>B<,>\n"
"B<                                int *>I<robustesse>B<);>\n"
"B<int pthread_mutexattr_setrobust(pthread_mutexattr_t *>I<attr>B<,>\n"
"B<                                int >I<robustesse>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Exigences de macros de test de fonctionnalités pour la glibc (consulter "
"B<feature_test_macros>(7)) :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<pthread_mutexattr_getrobust>(), B<pthread_mutexattr_setrobust>():"
msgstr "B<pthread_mutexattr_getrobust>(), B<pthread_mutexattr_setrobust>() :"

#.  FIXME .
#.  But see https://sourceware.org/bugzilla/show_bug.cgi?id=22125
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "    _POSIX_C_SOURCE E<gt>= 200809L\n"
msgstr "    _POSIX_C_SOURCE E<gt>= 200809L\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<pthread_mutexattr_getrobust>()  function places the value of the "
"robustness attribute of the mutex attributes object referred to by I<attr> "
"in I<*robustness>.  The B<pthread_mutexattr_setrobust>()  function sets the "
"value of the robustness attribute of the mutex attributes object referred to "
"by I<attr> to the value specified in I<*robustness>."
msgstr ""
"La fonction B<pthread_mutexattr_getrobust>() place la valeur de l'attribut "
"de robustesse de l'objet d'attributs de mutex auquel I<attr> fait référence "
"dans I<*robustesse>. La fonction B<pthread_mutexattr_setrobust>() définit à "
"la valeur spécifiée dans I<*robustesse> la valeur de l'attribut de "
"robustesse de l'objet d'attributs de mutex auquel I<attr> fait référence."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The robustness attribute specifies the behavior of the mutex when the owning "
"thread dies without unlocking the mutex.  The following values are valid for "
"I<robustness>:"
msgstr ""
"L'attribut de robustesse définit le comportement du mutex quand le thread "
"propriétaire finit sans déverrouiller le mutex. Les valeurs suivantes sont "
"permises pour I<robustesse> :"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<PTHREAD_MUTEX_STALLED>"
msgstr "B<PTHREAD_MUTEX_STALLED>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This is the default value for a mutex attributes object.  If a mutex is "
"initialized with the B<PTHREAD_MUTEX_STALLED> attribute and its owner dies "
"without unlocking it, the mutex remains locked afterwards and any future "
"attempts to call B<pthread_mutex_lock>(3)  on the mutex will block "
"indefinitely."
msgstr ""
"C'est la valeur par défaut pour un objet d'attributs de mutex. Si un mutex "
"est initialisé avec l'attribut B<PTHREAD_MUTEX_STALLED> et si son "
"propriétaire finit sans le déverrouiller, le mutex demeure verrouillé "
"ensuite et toute tentative ultérieure pour appeler B<pthread_mutex_lock>(3) "
"sur le mutex bloquera indéfiniment."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<PTHREAD_MUTEX_ROBUST>"
msgstr "B<PTHREAD_MUTEX_ROBUST>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If a mutex is initialized with the B<PTHREAD_MUTEX_ROBUST> attribute and its "
"owner dies without unlocking it, any future attempts to call "
"B<pthread_mutex_lock>(3)  on this mutex will succeed and return "
"B<EOWNERDEAD> to indicate that the original owner no longer exists and the "
"mutex is in an inconsistent state.  Usually after B<EOWNERDEAD> is returned, "
"the next owner should call B<pthread_mutex_consistent>(3)  on the acquired "
"mutex to make it consistent again before using it any further."
msgstr ""
"Si un mutex est initialisé avec l'attribut B<PTHREAD_MUTEX_ROBUST> et si son "
"propriétaire finit sans le déverrouiller, toute tentative ultérieure pour "
"appeler B<pthread_mutex_lock>(3) sur ce mutex réussira et renverra "
"B<EOWNERDEAD> pour indiquer que le propriétaire d'origine n'existe plus et "
"que le mutex est dans un état incohérent. Habituellement, après l'envoi de "
"B<EOWNERDEAD>, le propriétaire suivant devrait appeler "
"B<pthread_mutex_consistent>(3) sur le mutex acquis pour le rendre à nouveau "
"cohérent avant de l'utiliser plus avant."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the next owner unlocks the mutex using B<pthread_mutex_unlock>(3)  before "
"making it consistent, the mutex will be permanently unusable and any "
"subsequent attempts to lock it using B<pthread_mutex_lock>(3)  will fail "
"with the error B<ENOTRECOVERABLE>.  The only permitted operation on such a "
"mutex is B<pthread_mutex_destroy>(3)."
msgstr ""
"Si le propriétaire suivant déverrouille le mutex avec "
"B<pthread_mutex_unlock>(3) avant de le rendre cohérent, le mutex demeurera "
"inutilisable de façon permanente et toute tentative ultérieure pour le "
"verrouiller avec B<pthread_mutex_lock>(3) échouera avec l'erreur "
"B<ENOTRECOVERABLE>. B<pthread_mutex_destroy>(3) est la seule opération "
"permise sur un mutex dans cet état."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the next owner terminates before calling B<pthread_mutex_consistent>(3), "
"further B<pthread_mutex_lock>(3)  operations on this mutex will still return "
"B<EOWNERDEAD>."
msgstr ""
"Si le propriétaire suivant termine avant d'appeler "
"B<pthread_mutex_consistent>(3), les opérations B<pthread_mutex_lock>(3) "
"ultérieures sur ce mutex renverront encore B<EOWNERDEAD>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Note that the I<attr> argument of B<pthread_mutexattr_getrobust>()  and "
"B<pthread_mutexattr_setrobust>()  should refer to a mutex attributes object "
"that was initialized by B<pthread_mutexattr_init>(3), otherwise the behavior "
"is undefined."
msgstr ""
"Veuillez noter que l'argument I<attr> de B<pthread_mutexattr_getrobust>() et "
"de B<pthread_mutexattr_setrobust>() doit faire référence à l'objet "
"d'attributs de mutex initialisé par B<pthread_mutexattr_init>(3), autrement "
"le comportement est indéfini."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On success, these functions return 0.  On error, they return a positive "
"error number."
msgstr ""
"En cas de réussite, ces fonctions renvoient B<0> ; en cas d'erreur, elles "
"renvoient un code d'erreur positif."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In the glibc implementation, B<pthread_mutexattr_getrobust>()  always return "
"zero."
msgstr ""
"Dans l'implémentation de la glibc, B<pthread_mutexattr_getrobust>() renvoie "
"toujours zéro."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERREURS"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"A value other than B<PTHREAD_MUTEX_STALLED> or B<PTHREAD_MUTEX_ROBUST> was "
"passed to B<pthread_mutexattr_setrobust>()."
msgstr ""
"Une valeur autre que B<PTHREAD_MUTEX_STALLED> ou B<PTHREAD_MUTEX_ROBUST> a "
"été passée à B<pthread_mutexattr_setrobust>()."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONS"

#.  E.g., Solaris, according to its manual page
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In the Linux implementation, when using process-shared robust mutexes, a "
"waiting thread also receives the B<EOWNERDEAD> notification if the owner of "
"a robust mutex performs an B<execve>(2)  without first unlocking the mutex.  "
"POSIX.1 does not specify this detail, but the same behavior also occurs in "
"at least some other implementations."
msgstr ""
"Dans l'implémentation de Linux, lors de l'utilisation de mutex à processus "
"partagé robustes, un thread en attente reçoit également la notification "
"B<EOWNERDEAD> si le propriétaire d'un mutex robuste réalise une commande "
"B<execve>(2) sans avoir préalablement déverrouillé le mutex. POSIX.1 ne "
"spécifie pas ce détail, mais le même comportement se produit aussi dans au "
"moins quelques autres implémentations."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIQUE"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "glibc 2.12.  POSIX.1-2008."
msgstr "glibc 2.12.  POSIX.1-2008."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Before the addition of B<pthread_mutexattr_getrobust>()  and "
"B<pthread_mutexattr_setrobust>()  to POSIX, glibc defined the following "
"equivalent nonstandard functions if B<_GNU_SOURCE> was defined:"
msgstr ""
"Avant l'ajout à POSIX de B<pthread_mutexattr_getrobust>() et "
"B<pthread_mutexattr_setrobust>(), la glibc définissait les fonctions non "
"standard équivalentes suivantes si B<_GNU_SOURCE> était défini :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<[[deprecated]]>\n"
"B<int pthread_mutexattr_getrobust_np(const pthread_mutexattr_t *>I<attr>B<,>\n"
"B<                                   int *>I<robustness>B<);>\n"
"B<[[deprecated]]>\n"
"B<int pthread_mutexattr_setrobust_np(const pthread_mutexattr_t *>I<attr>B<,>\n"
"B<                                   int >I<robustness>B<);>\n"
msgstr ""
"B<[[obsolète]]>\n"
"B<int pthread_mutexattr_getrobust_np(const pthread_mutexattr_t *>I<attr>B<,>\n"
"B<                                   int *>I<robustesse>B<);>\n"
"B<[[obsolète]]>\n"
"B<int pthread_mutexattr_setrobust_np(const pthread_mutexattr_t *>I<attr>B<,>\n"
"B<                                   int >I<robustesse>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Correspondingly, the constants B<PTHREAD_MUTEX_STALLED_NP> and "
"B<PTHREAD_MUTEX_ROBUST_NP> were also defined."
msgstr ""
"De la même façon, les constantes B<PTHREAD_MUTEX_STALLED_NP> et "
"B<PTHREAD_MUTEX_ROBUST_NP> étaient également définies."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"These GNU-specific APIs, which first appeared in glibc 2.4, are nowadays "
"obsolete and should not be used in new programs; since glibc 2.34 these APIs "
"are marked as deprecated."
msgstr ""
"Ces API propres à GNU, apparues dans la glibc 2.4, sont à présent obsolètes "
"et ne devraient pas être utilisées dans de nouveaux programmes. Depuis la "
"glibc 2.34, ces API sont marquées comme obsolètes."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "EXEMPLES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The program below demonstrates the use of the robustness attribute of a "
"mutex attributes object.  In this program, a thread holding the mutex dies "
"prematurely without unlocking the mutex.  The main thread subsequently "
"acquires the mutex successfully and gets the error B<EOWNERDEAD>, after "
"which it makes the mutex consistent."
msgstr ""
"Le programme ci-dessous montre l'utilisation de l'attribut de robustesse "
"d'un objet d'attributs de mutex. Dans ce programme, un thread possédant le "
"mutex s'interrompt prématurément sans déverrouiller le mutex. Le thread "
"principal acquiert ensuite le mutex avec succès et reçoit l'erreur "
"B<EOWNERDEAD> après quoi il rend le mutex cohérent."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The following shell session shows what we see when running this program:"
msgstr ""
"La session d'interpréteur de commande suivante montre l'exécution de ce "
"programme :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"$ B<./a.out>\n"
"[original owner] Setting lock...\n"
"[original owner] Locked. Now exiting without unlocking.\n"
"[main] Attempting to lock the robust mutex.\n"
"[main] pthread_mutex_lock() returned EOWNERDEAD\n"
"[main] Now make the mutex consistent\n"
"[main] Mutex is now consistent; unlocking\n"
msgstr ""
"$ B<./a.out>\n"
"[original owner] Mise en place du verrou...\n"
"[original owner] Verrouillé. Sortie sans déverrouillage.\n"
"[main] Tentative de verrouillage du mutex robuste.\n"
"[main] pthread_mutex_lock() renvoie EOWNERDEAD\n"
"[main] Maintenant rendre le mutex cohérent\n"
"[main] Le mutex est maintenant cohérent ; déverrouillage\n"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Program source"
msgstr "Source du programme"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "#include E<lt>errno.hE<gt>\n"
#| "#include E<lt>pthread.hE<gt>\n"
#| "#include E<lt>stdio.hE<gt>\n"
#| "#include E<lt>stdlib.hE<gt>\n"
#| "#include E<lt>unistd.hE<gt>\n"
#| "\\&\n"
#| "#define handle_error_en(en, msg) \\e\n"
#| "        do { errno = en; perror(msg); exit(EXIT_FAILURE); } while (0)\n"
#| "\\&\n"
#| "static pthread_mutex_t mtx;\n"
#| "\\&\n"
#| "static void *\n"
#| "original_owner_thread(void *ptr)\n"
#| "{\n"
#| "    printf(\"[original owner] Setting lock...\\en\");\n"
#| "    pthread_mutex_lock(&mtx);\n"
#| "    printf(\"[original owner] Locked. Now exiting without unlocking.\\en\");\n"
#| "    pthread_exit(NULL);\n"
#| "}\n"
#| "\\&\n"
#| "int\n"
#| "main(void)\n"
#| "{\n"
#| "    pthread_t thr;\n"
#| "    pthread_mutexattr_t attr;\n"
#| "    int s;\n"
#| "\\&\n"
#| "    pthread_mutexattr_init(&attr);\n"
#| "\\&\n"
#| "    pthread_mutexattr_setrobust(&attr, PTHREAD_MUTEX_ROBUST);\n"
#| "\\&\n"
#| "    pthread_mutex_init(&mtx, &attr);\n"
#| "\\&\n"
#| "    pthread_create(&thr, NULL, original_owner_thread, NULL);\n"
#| "\\&\n"
#| "    sleep(2);\n"
#| "\\&\n"
#| "    /* \"original_owner_thread\" should have exited by now. */\n"
#| "\\&\n"
#| "    printf(\"[main] Attempting to lock the robust mutex.\\en\");\n"
#| "    s = pthread_mutex_lock(&mtx);\n"
#| "    if (s == EOWNERDEAD) {\n"
#| "        printf(\"[main] pthread_mutex_lock() returned EOWNERDEAD\\en\");\n"
#| "        printf(\"[main] Now make the mutex consistent\\en\");\n"
#| "        s = pthread_mutex_consistent(&mtx);\n"
#| "        if (s != 0)\n"
#| "            handle_error_en(s, \"pthread_mutex_consistent\");\n"
#| "        printf(\"[main] Mutex is now consistent; unlocking\\en\");\n"
#| "        s = pthread_mutex_unlock(&mtx);\n"
#| "        if (s != 0)\n"
#| "            handle_error_en(s, \"pthread_mutex_unlock\");\n"
#| "\\&\n"
#| "        exit(EXIT_SUCCESS);\n"
#| "    } else if (s == 0) {\n"
#| "        printf(\"[main] pthread_mutex_lock() unexpectedly succeeded\\en\");\n"
#| "        exit(EXIT_FAILURE);\n"
#| "    } else {\n"
#| "        printf(\"[main] pthread_mutex_lock() unexpectedly failed\\en\");\n"
#| "        handle_error_en(s, \"pthread_mutex_lock\");\n"
#| "    }\n"
#| "}\n"
msgid ""
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>pthread.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"\\&\n"
"#define handle_error_en(en, msg) \\[rs]\n"
"        do { errno = en; perror(msg); exit(EXIT_FAILURE); } while (0)\n"
"\\&\n"
"static pthread_mutex_t mtx;\n"
"\\&\n"
"static void *\n"
"original_owner_thread(void *ptr)\n"
"{\n"
"    printf(\"[original owner] Setting lock...\\[rs]n\");\n"
"    pthread_mutex_lock(&mtx);\n"
"    printf(\"[original owner] Locked. Now exiting without unlocking.\\[rs]n\");\n"
"    pthread_exit(NULL);\n"
"}\n"
"\\&\n"
"int\n"
"main(void)\n"
"{\n"
"    pthread_t thr;\n"
"    pthread_mutexattr_t attr;\n"
"    int s;\n"
"\\&\n"
"    pthread_mutexattr_init(&attr);\n"
"\\&\n"
"    pthread_mutexattr_setrobust(&attr, PTHREAD_MUTEX_ROBUST);\n"
"\\&\n"
"    pthread_mutex_init(&mtx, &attr);\n"
"\\&\n"
"    pthread_create(&thr, NULL, original_owner_thread, NULL);\n"
"\\&\n"
"    sleep(2);\n"
"\\&\n"
"    /* \"original_owner_thread\" should have exited by now. */\n"
"\\&\n"
"    printf(\"[main] Attempting to lock the robust mutex.\\[rs]n\");\n"
"    s = pthread_mutex_lock(&mtx);\n"
"    if (s == EOWNERDEAD) {\n"
"        printf(\"[main] pthread_mutex_lock() returned EOWNERDEAD\\[rs]n\");\n"
"        printf(\"[main] Now make the mutex consistent\\[rs]n\");\n"
"        s = pthread_mutex_consistent(&mtx);\n"
"        if (s != 0)\n"
"            handle_error_en(s, \"pthread_mutex_consistent\");\n"
"        printf(\"[main] Mutex is now consistent; unlocking\\[rs]n\");\n"
"        s = pthread_mutex_unlock(&mtx);\n"
"        if (s != 0)\n"
"            handle_error_en(s, \"pthread_mutex_unlock\");\n"
"\\&\n"
"        exit(EXIT_SUCCESS);\n"
"    } else if (s == 0) {\n"
"        printf(\"[main] pthread_mutex_lock() unexpectedly succeeded\\[rs]n\");\n"
"        exit(EXIT_FAILURE);\n"
"    } else {\n"
"        printf(\"[main] pthread_mutex_lock() unexpectedly failed\\[rs]n\");\n"
"        handle_error_en(s, \"pthread_mutex_lock\");\n"
"    }\n"
"}\n"
msgstr ""
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>pthread.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"\\&\n"
"#define handle_error_en(en, msg) \\e\n"
"        do { errno = en; perror(msg); exit(EXIT_FAILURE); } while (0)\n"
"\\&\n"
"static pthread_mutex_t mtx;\n"
"\\&\n"
"static void *\n"
"original_owner_thread(void *ptr)\n"
"{\n"
"    printf(\"[original owner] Mise en place du verrou...\\en\");\n"
"    pthread_mutex_lock(&mtx);\n"
"    printf(\"[original owner] Verrouillé. Sortie sans déverrouillage.\\en\");\n"
"    pthread_exit(NULL);\n"
"}\n"
"\\&\n"
"int\n"
"main(void)\n"
"{\n"
"    pthread_t thr;\n"
"    pthread_mutexattr_t attr;\n"
"    int s;\n"
"\\&\n"
"    pthread_mutexattr_init(&attr);\n"
"\\&\n"
"    pthread_mutexattr_setrobust(&attr, PTHREAD_MUTEX_ROBUST);\n"
"\\&\n"
"    pthread_mutex_init(&mtx, &attr);\n"
"\\&\n"
"    pthread_create(&thr, NULL, original_owner_thread, NULL);\n"
"\\&\n"
"    sleep(2);\n"
"\\&\n"
"    /* \"original_owner_thread\" devrait avoir quitté maintenant. */\n"
"\\&\n"
"    printf(\"[main] Tentative de verrouillage du mutex robuste.\\en\");\n"
"    s = pthread_mutex_lock(&mtx);\n"
"    if (s == EOWNERDEAD) {\n"
"        printf(\"[main] pthread_mutex_lock() renvoie EOWNERDEAD\\en\");\n"
"        printf(\"[main] Maintenant rendre le mutex cohérent\\en\");\n"
"        s = pthread_mutex_consistent(&mtx);\n"
"        if (s != 0)\n"
"            handle_error_en(s, \"pthread_mutex_consistent\");\n"
"        printf(\"[main] Le mutex est maintenant cohérent ; déverrouillage\\en\");\n"
"        s = pthread_mutex_unlock(&mtx);\n"
"        if (s != 0)\n"
"            handle_error_en(s, \"pthread_mutex_unlock\");\n"
"\\&\n"
"        exit(EXIT_SUCCESS);\n"
"    } else if (s == 0) {\n"
"        printf(\"[main] pthread_mutex_lock() réussit de façon inattendue\\en\");\n"
"        exit(EXIT_FAILURE);\n"
"    } else {\n"
"        printf(\"[main] pthread_mutex_lock() échoue de façon inattendue\\en\");\n"
"        handle_error_en(s, \"pthread_mutex_lock\");\n"
"    }\n"
"}\n"

#.  SRC END
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<get_robust_list>(2), B<set_robust_list>(2), "
"B<pthread_mutex_consistent>(3), B<pthread_mutex_init>(3), "
"B<pthread_mutex_lock>(3), B<pthreads>(7)"
msgstr ""
"B<get_robust_list>(2), B<set_robust_list>(2), "
"B<pthread_mutex_consistent>(3), B<pthread_mutex_init>(3), "
"B<pthread_mutex_lock>(3), B<pthreads>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-12-04"
msgstr "4 décembre 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "POSIX threads library (I<libpthread>, I<-lpthread>)"
msgstr "Bibliothèque de threads POSIX (I<libpthread>, I<-lpthread>)"

#. type: Plain text
#: debian-bookworm
msgid ""
"B<pthread_mutexattr_getrobust>()  and B<pthread_mutexattr_setrobust>()  were "
"added in glibc 2.12."
msgstr ""
"B<pthread_mutexattr_getrobust>() et B<pthread_mutexattr_setrobust>() ont été "
"ajoutées dans la glibc version 2.12."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>pthread.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
msgstr ""
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>pthread.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"#define handle_error_en(en, msg) \\e\n"
"        do { errno = en; perror(msg); exit(EXIT_FAILURE); } while (0)\n"
msgstr ""
"#define handle_error_en(en, msg) \\e\n"
"        do { errno = en; perror(msg); exit(EXIT_FAILURE); } while (0)\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "static pthread_mutex_t mtx;\n"
msgstr "static pthread_mutex_t mtx;\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"static void *\n"
"original_owner_thread(void *ptr)\n"
"{\n"
"    printf(\"[original owner] Setting lock...\\en\");\n"
"    pthread_mutex_lock(&mtx);\n"
"    printf(\"[original owner] Locked. Now exiting without unlocking.\\en\");\n"
"    pthread_exit(NULL);\n"
"}\n"
msgstr ""
"static void *\n"
"original_owner_thread(void *ptr)\n"
"{\n"
"    printf(\"[original owner] Mise en place du verrou...\\en\");\n"
"    pthread_mutex_lock(&mtx);\n"
"    printf(\"[original owner] Verrouillé. Sortie sans déverrouillage.\\en\");\n"
"    pthread_exit(NULL);\n"
"}\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"int\n"
"main(void)\n"
"{\n"
"    pthread_t thr;\n"
"    pthread_mutexattr_t attr;\n"
"    int s;\n"
msgstr ""
"int\n"
"main(void)\n"
"{\n"
"    pthread_t thr;\n"
"    pthread_mutexattr_t attr;\n"
"    int s;\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "    pthread_mutexattr_init(&attr);\n"
msgstr "    pthread_mutexattr_init(&attr);\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "    pthread_mutexattr_setrobust(&attr, PTHREAD_MUTEX_ROBUST);\n"
msgstr "    pthread_mutexattr_setrobust(&attr, PTHREAD_MUTEX_ROBUST);\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "    pthread_mutex_init(&mtx, &attr);\n"
msgstr "    pthread_mutex_init(&mtx, &attr);\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "    pthread_create(&thr, NULL, original_owner_thread, NULL);\n"
msgstr "    pthread_create(&thr, NULL, original_owner_thread, NULL);\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "    sleep(2);\n"
msgstr "    sleep(2);\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "    /* \"original_owner_thread\" should have exited by now. */\n"
msgstr "    /* \"original_owner_thread\" devrait avoir quitté maintenant. */\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    printf(\"[main] Attempting to lock the robust mutex.\\en\");\n"
"    s = pthread_mutex_lock(&mtx);\n"
"    if (s == EOWNERDEAD) {\n"
"        printf(\"[main] pthread_mutex_lock() returned EOWNERDEAD\\en\");\n"
"        printf(\"[main] Now make the mutex consistent\\en\");\n"
"        s = pthread_mutex_consistent(&mtx);\n"
"        if (s != 0)\n"
"            handle_error_en(s, \"pthread_mutex_consistent\");\n"
"        printf(\"[main] Mutex is now consistent; unlocking\\en\");\n"
"        s = pthread_mutex_unlock(&mtx);\n"
"        if (s != 0)\n"
"            handle_error_en(s, \"pthread_mutex_unlock\");\n"
msgstr ""
"    printf(\"[main] Tentative de verrouillage du mutex robuste.\\en\");\n"
"    s = pthread_mutex_lock(&mtx);\n"
"    if (s == EOWNERDEAD) {\n"
"        printf(\"[main] pthread_mutex_lock() renvoie EOWNERDEAD\\en\");\n"
"        printf(\"[main] Maintenant rendre le mutex cohérent\\en\");\n"
"        s = pthread_mutex_consistent(&mtx);\n"
"        if (s != 0)\n"
"            handle_error_en(s, \"pthread_mutex_consistent\");\n"
"        printf(\"[main] Le mutex est maintenant cohérent ; déverrouillage\\en\");\n"
"        s = pthread_mutex_unlock(&mtx);\n"
"        if (s != 0)\n"
"            handle_error_en(s, \"pthread_mutex_unlock\");\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"        exit(EXIT_SUCCESS);\n"
"    } else if (s == 0) {\n"
"        printf(\"[main] pthread_mutex_lock() unexpectedly succeeded\\en\");\n"
"        exit(EXIT_FAILURE);\n"
"    } else {\n"
"        printf(\"[main] pthread_mutex_lock() unexpectedly failed\\en\");\n"
"        handle_error_en(s, \"pthread_mutex_lock\");\n"
"    }\n"
"}\n"
msgstr ""
"        exit(EXIT_SUCCESS);\n"
"    } else if (s == 0) {\n"
"        printf(\"[main] pthread_mutex_lock() réussit de façon inattendue\\en\");\n"
"        exit(EXIT_FAILURE);\n"
"    } else {\n"
"        printf(\"[main] pthread_mutex_lock() échoue de façon inattendue\\en\");\n"
"        handle_error_en(s, \"pthread_mutex_lock\");\n"
"    }\n"
"}\n"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "2024-06-15"
msgstr "15 juin 2024"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Pages du manuel de Linux 6.9.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 octobre 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pages du manuel de Linux 6.06"

#. type: Plain text
#: mageia-cauldron
#, no-wrap
msgid ""
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>pthread.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"\\&\n"
"#define handle_error_en(en, msg) \\e\n"
"        do { errno = en; perror(msg); exit(EXIT_FAILURE); } while (0)\n"
"\\&\n"
"static pthread_mutex_t mtx;\n"
"\\&\n"
"static void *\n"
"original_owner_thread(void *ptr)\n"
"{\n"
"    printf(\"[original owner] Setting lock...\\en\");\n"
"    pthread_mutex_lock(&mtx);\n"
"    printf(\"[original owner] Locked. Now exiting without unlocking.\\en\");\n"
"    pthread_exit(NULL);\n"
"}\n"
"\\&\n"
"int\n"
"main(void)\n"
"{\n"
"    pthread_t thr;\n"
"    pthread_mutexattr_t attr;\n"
"    int s;\n"
"\\&\n"
"    pthread_mutexattr_init(&attr);\n"
"\\&\n"
"    pthread_mutexattr_setrobust(&attr, PTHREAD_MUTEX_ROBUST);\n"
"\\&\n"
"    pthread_mutex_init(&mtx, &attr);\n"
"\\&\n"
"    pthread_create(&thr, NULL, original_owner_thread, NULL);\n"
"\\&\n"
"    sleep(2);\n"
"\\&\n"
"    /* \"original_owner_thread\" should have exited by now. */\n"
"\\&\n"
"    printf(\"[main] Attempting to lock the robust mutex.\\en\");\n"
"    s = pthread_mutex_lock(&mtx);\n"
"    if (s == EOWNERDEAD) {\n"
"        printf(\"[main] pthread_mutex_lock() returned EOWNERDEAD\\en\");\n"
"        printf(\"[main] Now make the mutex consistent\\en\");\n"
"        s = pthread_mutex_consistent(&mtx);\n"
"        if (s != 0)\n"
"            handle_error_en(s, \"pthread_mutex_consistent\");\n"
"        printf(\"[main] Mutex is now consistent; unlocking\\en\");\n"
"        s = pthread_mutex_unlock(&mtx);\n"
"        if (s != 0)\n"
"            handle_error_en(s, \"pthread_mutex_unlock\");\n"
"\\&\n"
"        exit(EXIT_SUCCESS);\n"
"    } else if (s == 0) {\n"
"        printf(\"[main] pthread_mutex_lock() unexpectedly succeeded\\en\");\n"
"        exit(EXIT_FAILURE);\n"
"    } else {\n"
"        printf(\"[main] pthread_mutex_lock() unexpectedly failed\\en\");\n"
"        handle_error_en(s, \"pthread_mutex_lock\");\n"
"    }\n"
"}\n"
msgstr ""
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>pthread.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"\\&\n"
"#define handle_error_en(en, msg) \\e\n"
"        do { errno = en; perror(msg); exit(EXIT_FAILURE); } while (0)\n"
"\\&\n"
"static pthread_mutex_t mtx;\n"
"\\&\n"
"static void *\n"
"original_owner_thread(void *ptr)\n"
"{\n"
"    printf(\"[original owner] Mise en place du verrou...\\en\");\n"
"    pthread_mutex_lock(&mtx);\n"
"    printf(\"[original owner] Verrouillé. Sortie sans déverrouillage.\\en\");\n"
"    pthread_exit(NULL);\n"
"}\n"
"\\&\n"
"int\n"
"main(void)\n"
"{\n"
"    pthread_t thr;\n"
"    pthread_mutexattr_t attr;\n"
"    int s;\n"
"\\&\n"
"    pthread_mutexattr_init(&attr);\n"
"\\&\n"
"    pthread_mutexattr_setrobust(&attr, PTHREAD_MUTEX_ROBUST);\n"
"\\&\n"
"    pthread_mutex_init(&mtx, &attr);\n"
"\\&\n"
"    pthread_create(&thr, NULL, original_owner_thread, NULL);\n"
"\\&\n"
"    sleep(2);\n"
"\\&\n"
"    /* \"original_owner_thread\" devrait avoir quitté maintenant. */\n"
"\\&\n"
"    printf(\"[main] Tentative de verrouillage du mutex robuste.\\en\");\n"
"    s = pthread_mutex_lock(&mtx);\n"
"    if (s == EOWNERDEAD) {\n"
"        printf(\"[main] pthread_mutex_lock() renvoie EOWNERDEAD\\en\");\n"
"        printf(\"[main] Maintenant rendre le mutex cohérent\\en\");\n"
"        s = pthread_mutex_consistent(&mtx);\n"
"        if (s != 0)\n"
"            handle_error_en(s, \"pthread_mutex_consistent\");\n"
"        printf(\"[main] Le mutex est maintenant cohérent ; déverrouillage\\en\");\n"
"        s = pthread_mutex_unlock(&mtx);\n"
"        if (s != 0)\n"
"            handle_error_en(s, \"pthread_mutex_unlock\");\n"
"\\&\n"
"        exit(EXIT_SUCCESS);\n"
"    } else if (s == 0) {\n"
"        printf(\"[main] pthread_mutex_lock() réussit de façon inattendue\\en\");\n"
"        exit(EXIT_FAILURE);\n"
"    } else {\n"
"        printf(\"[main] pthread_mutex_lock() échoue de façon inattendue\\en\");\n"
"        handle_error_en(s, \"pthread_mutex_lock\");\n"
"    }\n"
"}\n"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Pages du manuel de Linux (non publiées)"
