# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010-2012, 2014.
# Frédéric Hantrais <fhantrais@gmail.com>, 2013, 2014.
# Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2025-02-28 16:29+0100\n"
"PO-Revision-Date: 2023-03-14 11:40+0100\n"
"Last-Translator: Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Lokalize 22.12.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "bstring"
msgstr "bstring"

#. type: TH
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "2024-07-23"
msgstr "23 juillet 2024"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr "Pages du manuel de Linux 6.12"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"bcmp, bcopy, bzero, memccpy, memchr, memcmp, memcpy, memfrob, memmem, "
"memmove, memset - byte string operations"
msgstr ""
"bcmp, bcopy, bzero, memccpy, memchr, memcmp, memcpy, memfrob, memmem, "
"memmove, memset - Opérations sur des chaînes d'octets"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHÈQUE"

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "Standard C library (I<libc>,\\ I<-lc>)"
msgstr "Bibliothèque C standard (I<libc>,\\ I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>string.hE<gt>>\n"
msgstr "B<#include E<lt>string.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<int bcmp(const void >I<s1>B<[.>I<n>B<], const void >I<s2>B<[.>I<n>B<], size_t >I<n>B<);>\n"
msgstr "B<int bcmp(const void >I<s1>B<[.>I<n>B<], const void >I<s2>B<[.>I<n>B<], size_t >I<n>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<void bcopy(const void >I<src>B<[.>I<n>B<], void >I<dest>B<[.>I<n>B<], size_t >I<n>B<);>\n"
msgstr "B<void bcopy(const void >I<src>B<[.>I<n>B<], void >I<dest>B<[.>I<n>B<], size_t >I<n>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<void bzero(void >I<s>B<[.>I<n>B<], size_t >I<n>B<);>\n"
msgstr "B<void bzero(void >I<s>B<[.>I<n>B<], size_t >I<n>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<void *memccpy(void >I<dest>B<[.>I<n>B<], const void >I<src>B<[.>I<n>B<], int >I<c>B<, size_t >I<n>B<);>\n"
msgstr "B<void *memccpy(void >I<dest>B<[.>I<n>B<], const void >I<src>B<[.>I<n>B<], int >I<c>B<, size_t >I<n>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<void *memchr(const void >I<s>B<[.>I<n>B<], int >I<c>B<, size_t >I<n>B<);>\n"
msgstr "B<void *memchr(const void >I<s>B<[.>I<n>B<], int >I<c>B<, size_t >I<n>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<int memcmp(const void >I<s1>B<[.>I<n>B<], const void >I<s2>B<[.>I<n>B<], size_t >I<n>B<);>\n"
msgstr "B<int memcmp(const void >I<s1>B<[.>I<n>B<], const void >I<s2>B<[.>I<n>B<], size_t >I<n>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<void *memcpy(void >I<dest>B<[.>I<n>B<], const void >I<src>B<[.>I<n>B<], size_t >I<n>B<);>\n"
msgstr "B<void *memcpy(void >I<dest>B<[.>I<n>B<], const void >I<src>B<[.>I<n>B<], size_t >I<n>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<void *memfrob(void >I<s>B<[.>I<n>B<], size_t >I<n>B<);>\n"
msgstr "B<void *memfrob(void >I<s>B<[.>I<n>B<], size_t >I<n>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<void *memmem(const void >I<haystack>B<[.>I<haystacklen>B<], size_t >I<haystacklen>B<,>\n"
"B<             const void >I<needle>B<[.>I<needlelen>B<], size_t >I<needlelen>B<);>\n"
msgstr ""
"B<void *memmem(const void >I<haystack>B<[.>I<haystacklen>B<], size_t >I<haystacklen>B<,>\n"
"B<             const void >I<needle>B<[.>I<needlelen>B<], size_t >I<needlelen>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<void *memmove(void >I<dest>B<[.>I<n>B<], const void >I<src>B<[.>I<n>B<], size_t >I<n>B<);>\n"
msgstr "B<void *memmove(void >I<dest>B<[.>I<n>B<], const void >I<src>B<[.>I<n>B<], size_t >I<n>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<void *memset(void >I<s>B<[.>I<n>B<], int >I<c>B<, size_t >I<n>B<);>\n"
msgstr "B<void *memset(void >I<s>B<[.>I<n>B<], int >I<c>B<, size_t >I<n>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The byte string functions perform operations on strings (byte arrays)  that "
"are not necessarily null-terminated.  See the individual man pages for "
"descriptions of each function."
msgstr ""
"Ces fonctions agissent sur des chaînes d'octets (tableaux d'octets) qui ne "
"se terminent pas nécessairement par le caractère nul. Reportez-vous aux "
"pages de manuel de chaque fonction pour obtenir leur description."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#.  The old functions are not even available on some non-GNU/Linux systems.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The functions B<bcmp>()  and B<bcopy>()  are obsolete.  Use B<memcmp>()  and "
"B<memmove>()  instead."
msgstr ""
"Les fonctions B<bcmp>() et B<bcopy>() sont obsolètes. Utilisez B<memcmp>() "
"et B<memcpy>() à la place."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<bcmp>(3), B<bcopy>(3), B<bzero>(3), B<memccpy>(3), B<memchr>(3), "
"B<memcmp>(3), B<memcpy>(3), B<memfrob>(3), B<memmem>(3), B<memmove>(3), "
"B<memset>(3), B<string>(3)"
msgstr ""
"B<bcmp>(3), B<bcopy>(3), B<bzero>(3), B<memccpy>(3), B<memchr>(3), "
"B<memcmp>(3), B<memcpy>(3), B<memfrob>(3), B<memmem>(3), B<memmove>(3), "
"B<memset>(3), B<string>(3)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-01-07"
msgstr "7 janvier 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Bibliothèque C standard (I<libc>, I<-lc>)"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "2024-05-02"
msgstr "2 mai 2024"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Pages du manuel de Linux 6.9.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 octobre 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pages du manuel de Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Pages du manuel de Linux (non publiées)"
