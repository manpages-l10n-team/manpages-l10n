# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006, 2013.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010, 2012, 2013.
# Cédric Boutillier <cedric.boutillier@gmail.com>, 2011, 2012.
# Frédéric Hantrais <fhantrais@gmail.com>, 2013, 2014.
# Jean-Philippe MENGUAL <jpmengual@debian.org>, 2020-2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2025-02-28 16:34+0100\n"
"PO-Revision-Date: 2024-01-29 11:42+0100\n"
"Last-Translator: Jean-Philippe MENGUAL <jpmengual@debian.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Lokalize 22.12.3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "getpid"
msgstr "getpid"

#. type: TH
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "2024-07-23"
msgstr "23 juillet 2024"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr "Pages du manuel de Linux 6.12"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "getpid, getppid - get process identification"
msgstr "getpid, getppid - Obtenir l'identifiant d'un processus"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHÈQUE"

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "Standard C library (I<libc>,\\ I<-lc>)"
msgstr "Bibliothèque C standard (I<libc>,\\ I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>unistd.hE<gt>>\n"
msgstr "B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<pid_t getpid(void);>\n"
"B<pid_t getppid(void);>\n"
msgstr ""
"B<pid_t getpid(void);>\n"
"B<pid_t getppid(void);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<getpid>()  returns the process ID (PID) of the calling process.  (This is "
"often used by routines that generate unique temporary filenames.)"
msgstr ""
"B<getpid>() renvoie l'identifiant du processus appelant (cela est souvent "
"utilisé par des routines qui génèrent des noms de fichier temporaire "
"uniques)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<getppid>()  returns the process ID of the parent of the calling process.  "
"This will be either the ID of the process that created this process using "
"B<fork>(), or, if that process has already terminated, the ID of the process "
"to which this process has been reparented (either B<init>(1)  or a "
"\"subreaper\" process defined via the B<prctl>(2)  B<PR_SET_CHILD_SUBREAPER> "
"operation)."
msgstr ""
"B<getppid>() renvoie l'ID du processus parent de celui appelant. Il s'agira "
"soit de l'ID du processus qui a créé ce processus en utilisant B<fork>(), "
"soit, si ce processus s'est déjà terminé, de l'ID du processus auquel il a "
"été réaffilié (B<init>(1) ou un processus « subreaper » défini avec "
"l'opération B<PR_SET_CHILD_SUBREAPER> de B<prctl>(2))."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERREURS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "These functions are always successful."
msgstr "Ces fonctions réussissent toujours."

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On Alpha, instead of a pair of B<getpid>()  and B<getppid>()  system calls, "
"a single B<getxpid>()  system call is provided, which returns a pair of PID "
"and parent PID.  The glibc B<getpid>()  and B<getppid>()  wrapper functions "
"transparently deal with this.  See B<syscall>(2)  for details regarding "
"register mapping."
msgstr ""
"Sur Alpha, au lieu d'une paire d'appels système B<getpid>() et B<getppid>(), "
"un seul appel B<getxpid>() est fourni, qui renvoie une paire PID et PID "
"parent. Les fonctions enveloppes B<getpid>() et B<getppid>() de la glibc "
"gèrent cela de manière transparente. Voir B<syscall>(2) pour des détails sur "
"les tableaux de registre."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIQUE"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2001, 4.3BSD, SVr4."
msgstr "POSIX.1-2001, 4.3BSD, SVr4."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "C library/kernel differences"
msgstr "Différences entre bibliothèque C et noyau"

#
#
#
#
#.  The following program demonstrates this "feature":
#.  #define _GNU_SOURCE
#.  #include <sys/syscall.h>
#.  #include <sys/wait.h>
#.  #include <stdint.h>
#.  #include <stdio.h>
#.  #include <stdlib.h>
#.  #include <unistd.h>
#.  int
#.  main(int argc, char *argv[])
#.  {
#.     /* The following statement fills the getpid() cache */
#.     printf("parent PID = %ld
#. ", (intmax_t) getpid());
#.     if (syscall(SYS_fork) == 0) {
#.         if (getpid() != syscall(SYS_getpid))
#.             printf("child getpid() mismatch: getpid()=%jd; "
#.                     "syscall(SYS_getpid)=%ld
#. ",
#.                     (intmax_t) getpid(), (long) syscall(SYS_getpid));
#.         exit(EXIT_SUCCESS);
#.     }
#.     wait(NULL);
#. }
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"From glibc 2.3.4 up to and including glibc 2.24, the glibc wrapper function "
"for B<getpid>()  cached PIDs, with the goal of avoiding additional system "
"calls when a process calls B<getpid>()  repeatedly.  Normally this caching "
"was invisible, but its correct operation relied on support in the wrapper "
"functions for B<fork>(2), B<vfork>(2), and B<clone>(2): if an application "
"bypassed the glibc wrappers for these system calls by using B<syscall>(2), "
"then a call to B<getpid>()  in the child would return the wrong value (to be "
"precise: it would return the PID of the parent process).  In addition, there "
"were cases where B<getpid>()  could return the wrong value even when "
"invoking B<clone>(2)  via the glibc wrapper function.  (For a discussion of "
"one such case, see BUGS in B<clone>(2).)  Furthermore, the complexity of the "
"caching code had been the source of a few bugs within glibc over the years."
msgstr ""
"De la glibc 2.3.4 jusqu'à la glibc 2.24 incluse, la fonction enveloppe de la "
"glibc pour B<getpid>() faisait un cache des PID, de façon à éviter des "
"appels système supplémentaires quand un processus appelle B<getpid>() de "
"façon répétée. Normalement, cette mise en cache n'était pas visible, mais "
"son fonctionnement correct reposait sur la gestion du cache dans les "
"fonctions enveloppes pour B<fork>(2), B<vfork>(2) et B<clone>(2) : si une "
"application se passait des enveloppes de la glibc pour ces appels système en "
"appelant B<syscall>(2), alors un appel à B<getpid>() dans l'enfant renvoyait "
"la mauvaise valeur (pour être précis : il renvoyait le PID du processus "
"parent). En outre, il y avait des cas où B<getpid>() renvoyait la mauvaise "
"valeur même avec un appel B<clone>(2) par la fonction enveloppe de la glibc "
"(voir BOGUES de B<clone>(2) pour un point sur ce cas). De plus, la "
"complexité du code de mise en cache était devenue, au fil des années, la "
"source de quelques bogues dans la glibc."

#.  commit c579f48edba88380635ab98cb612030e3ed8691e
#.  https://sourceware.org/glibc/wiki/Release/2.25#pid_cache_removal
#.  FIXME .
#.  Review progress of https://bugzilla.redhat.com/show_bug.cgi?id=1469757
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Because of the aforementioned problems, since glibc 2.25, the PID cache is "
"removed: calls to B<getpid>()  always invoke the actual system call, rather "
"than returning a cached value."
msgstr ""
"Du fait des problèmes susmentionnés, depuis la glibc version 2.25, le cache "
"du PID est retiré : les appels à B<getpid>() appellent toujours le vrai "
"appel système au lieu de renvoyer une valeur mise en cache."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the caller's parent is in a different PID namespace (see "
"B<pid_namespaces>(7)), B<getppid>()  returns 0."
msgstr ""
"Si le parent de l'appelant est dans un espace de noms de PID différent (voir "
"B<pid_namespaces>(7)), B<getppid>() renvoie B<0>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"From a kernel perspective, the PID (which is shared by all of the threads in "
"a multithreaded process)  is sometimes also known as the thread group ID "
"(TGID).  This contrasts with the kernel thread ID (TID), which is unique for "
"each thread.  For further details, see B<gettid>(2)  and the discussion of "
"the B<CLONE_THREAD> flag in B<clone>(2)."
msgstr ""
"Du point de vue du noyau, le PID (qui est partagé par tous les threads dans "
"un processus multithreads) est parfois connu sous l'ID du groupe du thread "
"(TGID). Cela contraste avec l'ID du thread (TID) du noyau qui est unique "
"pour chaque thread. Pour plus de détails, voir B<gettid>(2) ou le point sur "
"l'attribut B<CLONE_THREAD> dans B<clone>(2)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<clone>(2), B<fork>(2), B<gettid>(2), B<kill>(2), B<exec>(3), "
"B<mkstemp>(3), B<tempnam>(3), B<tmpfile>(3), B<tmpnam>(3), "
"B<credentials>(7), B<pid_namespaces>(7)"
msgstr ""
"B<clone>(2), B<fork>(2), B<gettid>(2), B<kill>(2), B<exec>(3), "
"B<mkstemp>(3), B<tempnam>(3), B<tmpfile>(3), B<tmpnam>(3), "
"B<credentials>(7), B<pid_namespaces>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-01-22"
msgstr "22 janvier 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Bibliothèque C standard (I<libc>, I<-lc>)"

#. type: Plain text
#: debian-bookworm
msgid "POSIX.1-2001, POSIX.1-2008, 4.3BSD, SVr4."
msgstr "POSIX.1-2001, POSIX.1-2008, 4.3BSD, SVr4."

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "2024-05-02"
msgstr "2 mai 2024"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Pages du manuel de Linux 6.9.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 octobre 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pages du manuel de Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Pages du manuel de Linux (non publiées)"
