# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010-2014.
msgid ""
msgstr ""
"Project-Id-Version: manpages-fr 4.22.0\n"
"POT-Creation-Date: 2025-02-28 16:51+0100\n"
"PO-Revision-Date: 2024-07-06 18:01+0200\n"
"Last-Translator: Jean-Paul Guillonneau <guillonneau.jeanpaul@free.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Gvim\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "sysfs"
msgstr "sysfs"

#. type: TH
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-06-15"
msgstr "15 juin 2024"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr "Pages du manuel de Linux 6.12"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "sysfs - a filesystem for exporting kernel objects"
msgstr "sysfs – Système de fichiers pour l’exportation d’objets du noyau"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<sysfs> filesystem is a pseudo-filesystem which provides an interface "
"to kernel data structures.  (More precisely, the files and directories in "
"B<sysfs> provide a view of the I<kobject> structures defined internally "
"within the kernel.)  The files under B<sysfs> provide information about "
"devices, kernel modules, filesystems, and other kernel components."
msgstr ""
"Le système de fichiers B<sysfs> est un pseudo-système de fichiers "
"fournissant une interface aux structures de données du noyau (plus "
"précisément, les fichiers et les répertoires dans B<sysfs> fournissent une "
"vue des structures I<kobject> définies à l’intérieur du noyau). Les fichiers "
"sous B<sysfs> fournissent des informations sur les périphériques, les "
"modules du noyau, les systèmes de fichiers et d’autres composants du noyau."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<sysfs> filesystem is commonly mounted at I</sys>.  Typically, it is "
"mounted automatically by the system, but it can also be mounted manually "
"using a command such as:"
msgstr ""
"Le système de fichiers B<sysfs> est communément monté sur I</sys>. "
"Normalement, il est monté automatiquement par le système, mais peut l’être "
"aussi manuellement en utilisant une commande telle que :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "mount -t sysfs sysfs /sys\n"
msgstr "mount -t sysfs sysfs /sys\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Many of the files in the B<sysfs> filesystem are read-only, but some files "
"are writable, allowing kernel variables to be changed.  To avoid redundancy, "
"symbolic links are heavily used to connect entries across the filesystem "
"tree."
msgstr ""
"Bon nombre de fichiers du système de fichiers B<sysfs> sont en lecture "
"seule, mais certains sont éditables, permettant de modifier les variables du "
"noyau. Pour éviter des redondances, les liens symboliques sont abondamment "
"utilisés pour des entrées dans l’arbre du système de fichiers."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Files and directories"
msgstr "Fichiers et répertoires"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The following list describes some of the files and directories under the I</"
"sys> hierarchy."
msgstr ""
"La liste suivante décrit quelques fichiers et répertoires dans la hiérarchie "
"I</sys>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</sys/block>"
msgstr "I</sys/block>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This subdirectory contains one symbolic link for each block device that has "
"been discovered on the system.  The symbolic links point to corresponding "
"directories under I</sys/devices>."
msgstr ""
"Ce sous-répertoire contient un lien symbolique pour chaque périphérique en "
"mode bloc qui a été découvert dans le système. Les liens symboliques "
"pointent vers les répertoires correspondants dans I</sys/devices>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</sys/bus>"
msgstr "I</sys/bus>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This directory contains one subdirectory for each of the bus types in the "
"kernel.  Inside each of these directories are two subdirectories:"
msgstr ""
"Ce répertoire contient un sous-répertoire pour chacun des types de bus du "
"noyau. Dans chacun de ces répertoires deux sous-répertoires existent :"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<devices>"
msgstr "I<devices>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This subdirectory contains symbolic links to entries in I</sys/devices> that "
"correspond to the devices discovered on this bus."
msgstr ""
"ce sous-répertoire contient des liens symboliques vers les entrées dans I</"
"sys/devices> qui correspondent aux périphériques découverts sur ce bus ;"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<drivers>"
msgstr "I<drivers>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This subdirectory contains one subdirectory for each device driver that is "
"loaded on this bus."
msgstr ""
"ce sous-répertoire contient un sous-répertoire par chaque pilote de "
"périphérique qui est chargé sur ce bus."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</sys/class>"
msgstr "I</sys/class>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This subdirectory contains a single layer of further subdirectories for each "
"of the device classes that have been registered on the system (e.g., "
"terminals, network devices, block devices, graphics devices, sound devices, "
"and so on).  Inside each of these subdirectories are symbolic links for each "
"of the devices in this class.  These symbolic links refer to entries in the "
"I</sys/devices> directory."
msgstr ""
"Ce sous-répertoire contient une seule couche de sous-répertoires "
"supplémentaires pour chacune des classes de périphérique qui ont été "
"enregistrées sur le système (par exemple, terminaux, périphériques réseau, "
"périphériques en mode bloc, périphériques graphiques, périphériques "
"audio, etc.). Dans chacun de ces sous-répertoires, des liens symboliques "
"existent pour chacun des périphériques de cette classe. Ces liens "
"symboliques font référence aux entrées dans le répertoire I</sys/devices>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</sys/class/net>"
msgstr "I</sys/class/net>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Each of the entries in this directory is a symbolic link representing one of "
"the real or virtual networking devices that are visible in the network "
"namespace of the process that is accessing the directory.  Each of these "
"symbolic links refers to entries in the I</sys/devices> directory."
msgstr ""
"Chacune des entrées dans ce répertoire est un lien symbolique représentant "
"un des périphériques réseau réel ou virtuel qui sont visibles dans l’espace "
"de noms réseau du processus accédant à ce répertoire. Chacun de ces liens "
"symboliques fait référence à une entrée dans le répertoire I</sys/devices>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</sys/dev>"
msgstr "I</sys/dev>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This directory contains two subdirectories I<block/> and I<char/>, "
"corresponding, respectively, to the block and character devices on the "
"system.  Inside each of these subdirectories are symbolic links with names "
"of the form I<major-ID>:I<minor-ID>, where the ID values correspond to the "
"major and minor ID of a specific device.  Each symbolic link points to the "
"B<sysfs> directory for a device.  The symbolic links inside I</sys/dev> thus "
"provide an easy way to look up the B<sysfs> interface using the device IDs "
"returned by a call to B<stat>(2)  (or similar)."
msgstr ""
"Ce répertoire contient deux sous-répertoires I<block/> et I<char/>, "
"correspondant, respectivement, aux périphériques en mode bloc et à ceux en "
"mode caractère du système. Dans chacun de ces sous-répertoires des liens "
"symboliques existent avec des noms de la forme I<ID_majeur>:I<ID_mineur>, où "
"les valeurs d’ID correspondent aux ID majeur et mineur d’un périphérique "
"particulier. Chaque lien symbolique pointe vers le répertoire B<sysfs> pour "
"un périphérique. Les liens symboliques dans I</sys/dev> fournissent ainsi un "
"moyen facile pour rechercher l’interface B<sysfs> en utilisant les ID de "
"périphérique renvoyés par un appel à B<stat>(2) (ou similaire)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The following shell session shows an example from I</sys/dev>:"
msgstr "La session d’interpréteur suivante montre un exemple de I</sys/dev> :"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"$ B<stat -c \"%t %T\" /dev/null>\n"
"1 3\n"
"$ B<readlink /sys/dev/char/1\\[rs]:3>\n"
"\\&../../devices/virtual/mem/null\n"
"$ B<ls -Fd /sys/devices/virtual/mem/null>\n"
"/sys/devices/virtual/mem/null/\n"
"$ B<ls -d1 /sys/devices/virtual/mem/null/*>\n"
"/sys/devices/virtual/mem/null/dev\n"
"/sys/devices/virtual/mem/null/power/\n"
"/sys/devices/virtual/mem/null/subsystem@\n"
"/sys/devices/virtual/mem/null/uevent\n"
msgstr ""
"$ B<stat -c \"%t %T\" /dev/null>\n"
"1 3\n"
"$ B<readlink /sys/dev/char/1\\[rs]:3>\n"
"\\&../../devices/virtual/mem/null\n"
"$ B<ls -Fd /sys/devices/virtual/mem/null>\n"
"/sys/devices/virtual/mem/null/\n"
"$ B<ls -d1 /sys/devices/virtual/mem/null/*>\n"
"/sys/devices/virtual/mem/null/dev\n"
"/sys/devices/virtual/mem/null/power/\n"
"/sys/devices/virtual/mem/null/subsystem@\n"
"/sys/devices/virtual/mem/null/uevent\n"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</sys/devices>"
msgstr "I</sys/devices>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This is a directory that contains a filesystem representation of the kernel "
"device tree, which is a hierarchy of I<device> structures within the kernel."
msgstr ""
"Ce répertoire contient une représentation du système de fichiers de l’arbre "
"des périphériques du noyau qui est une hiérarchie de structures I<device> "
"dans le noyau."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</sys/firmware>"
msgstr "I</sys/firmware>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This subdirectory contains interfaces for viewing and manipulating firmware-"
"specific objects and attributes."
msgstr ""
"Ce sous-répertoire contient des interfaces pour voir et manipuler des objets "
"et des attributs spécifiques au microprogramme."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</sys/fs>"
msgstr "I</sys/fs>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This directory contains subdirectories for some filesystems.  A filesystem "
"will have a subdirectory here only if it chose to explicitly create the "
"subdirectory."
msgstr ""
"Ce répertoire contient des sous-répertoires pour certains systèmes de "
"fichiers. Un système de fichiers aura un sous-répertoire ici seulement s’il "
"choisit explicitement de le créer."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</sys/fs/cgroup>"
msgstr "I</sys/fs/cgroup>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This directory conventionally is used as a mount point for a B<tmpfs>(5)  "
"filesystem containing mount points for B<cgroups>(7)  filesystems."
msgstr ""
"Conventionnellement ce répertoire est utilisé comme point de montage pour un "
"système de fichiers B<tmpfs>(5) contenant des points de montage pour des "
"systèmes de fichiers B<cgroups>(7)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</sys/fs/smackfs>"
msgstr "I</sys/fs/smackfs>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The directory contains configuration files for the SMACK LSM.  See the "
"kernel source file I<Documentation/admin-guide/LSM/Smack.rst>."
msgstr ""
"Ce répertoire contient les fichiers de configuration pour les modules SMACK "
"LSM. Consulter le fichier des sources du noyau I<Documentation/admin-guide/"
"LSM/Smack.rst>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</sys/hypervisor>"
msgstr "I</sys/hypervisor>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "[To be documented]"
msgstr "[À documenter]"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</sys/kernel>"
msgstr "I</sys/kernel>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This subdirectory contains various files and subdirectories that provide "
"information about the running kernel."
msgstr ""
"Ce sous-répertoire contient divers fichiers et sous-répertoires qui "
"fournissent des informations sur le noyau en cours de fonctionnement."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</sys/kernel/cgroup/>"
msgstr "I</sys/kernel/cgroup/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "For information about the files in this directory, see B<cgroups>(7)."
msgstr ""
"Pour des informations sur les fichiers de ce répertoire, consulter "
"B<cgroups>(7)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</sys/kernel/debug/tracing>"
msgstr "I</sys/kernel/debug/tracing>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Mount point for the I<tracefs> filesystem used by the kernel's I<ftrace> "
"facility.  (For information on I<ftrace>, see the kernel source file "
"I<Documentation/trace/ftrace.txt>.)"
msgstr ""
"C’est le point de montage du système de fichiers I<tracefs> utilisé par la "
"fonction I<ftrace> du noyau (pour des informations sur I<ftrace>, consulter "
"le fichier des sources du noyau I<Documentation/trace/ftrace.txt>)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</sys/kernel/mm>"
msgstr "I</sys/kernel/mm>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This subdirectory contains various files and subdirectories that provide "
"information about the kernel's memory management subsystem."
msgstr ""
"Ce sous-répertoire contient divers fichiers et sous-répertoires qui "
"fournissent des informations à propos du sous-système de gestion de la "
"mémoire par le noyau."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</sys/kernel/mm/hugepages>"
msgstr "I</sys/kernel/mm/hugepages>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This subdirectory contains one subdirectory for each of the huge page sizes "
"that the system supports.  The subdirectory name indicates the huge page "
"size (e.g., I<hugepages-2048kB>).  Within each of these subdirectories is a "
"set of files that can be used to view and (in some cases) change settings "
"associated with that huge page size.  For further information, see the "
"kernel source file I<Documentation/admin-guide/mm/hugetlbpage.rst>."
msgstr ""
"Ce sous-répertoire contient un sous-répertoire pour chacune des tailles de "
"page large que le système prend en charge. Le nom de sous-répertoire indique "
"la taille de page large (par exemple, I<hugepages-2048kB>). Dans chacun de "
"ces sous-répertoires se trouve un ensemble de fichiers qui peuvent être "
"utilisés pour voir et (dans certains cas) modifier les réglages associés à "
"cette taille de page large. Pour des informations complémentaires, consulter "
"le fichier des sources du noyau I<Documentation/admin-guide/mm/hugetlbpage."
"rst>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</sys/module>"
msgstr "I</sys/module>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This subdirectory contains one subdirectory for each module that is loaded "
"into the kernel.  The name of each directory is the name of the module.  In "
"each of the subdirectories, there may be following files:"
msgstr ""
"Ce sous-répertoire contient un sous-répertoire pour chaque module qui est "
"chargé dans le noyau. Le nom de chaque sous-répertoire est le nom du module. "
"Dans chacun des sous-répertoires, les fichiers suivants peuvent être "
"présents :"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<coresize>"
msgstr "I<coresize>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "[to be documented]"
msgstr "[Documentation à venir]"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<initsize>"
msgstr "I<initsize>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<initstate>"
msgstr "I<initstate>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<refcnt>"
msgstr "I<refcnt>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<srcversion>"
msgstr "I<srcversion>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<taint>"
msgstr "I<taint>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<uevent>"
msgstr "I<uevent>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<version>"
msgstr "I<version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "In each of the subdirectories, there may be following subdirectories:"
msgstr ""
"Dans chacun des sous-répertoires, les sous-répertoires suivants peuvent être "
"présents :"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<holders>"
msgstr "I<holders>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<notes>"
msgstr "I<notes>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<parameters>"
msgstr "I<parameters>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This directory contains one file for each module parameter, with each file "
"containing the value of the corresponding parameter.  Some of these files "
"are writable, allowing the"
msgstr ""
"Ce répertoire contient un fichier pour chaque paramètre de module, chaque "
"fichier contenant la valeur du paramètre correspondant. Certains de ces "
"fichiers sont éditables, permettant la modification de la valeur."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<sections>"
msgstr "I<sections>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This subdirectories contains files with information about module sections.  "
"This information is mainly used for debugging."
msgstr ""
"Ce sous-répertoire contient des fichiers comportant des informations sur les "
"sections de I<module>. Ces informations sont principalement utilisées à "
"titre de débogage."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<[To be documented]>"
msgstr "I<[Documentation à venir]>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</sys/power>"
msgstr "I</sys/power>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Linux."
msgstr "Linux."

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIQUE"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Linux 2.6.0."
msgstr "Linux 2.6.0."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This manual page is incomplete, possibly inaccurate, and is the kind of "
"thing that needs to be updated very often."
msgstr ""
"Cette page de manuel est incomplète, peut être imprécise, et devrait être "
"mise à jour très souvent."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<proc>(5), B<udev>(7)"
msgstr "B<proc>(5), B<udev>(7)"

#.  https://www.kernel.org/pub/linux/kernel/people/mochel/doc/papers/ols-2005/mochel.pdf
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"P.\\& Mochel. (2005).  I<The sysfs filesystem>.  Proceedings of the 2005 "
"Ottawa Linux Symposium."
msgstr ""
"P.\\& Mochel. (2005).  I<The sysfs filesystem>. Exposé lors du Symposium "
"Linux à Ottawa en 2005."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The kernel source file I<Documentation/filesystems/sysfs.txt> and various "
"other files in I<Documentation/ABI> and I<Documentation/*/sysfs.txt>"
msgstr ""
"Le fichier des sources du noyau I<Documentation/filesystems/sysfs.txt> et "
"divers autre fichiers dans I<Documentation/ABI> et I<Documentation/*/sysfs."
"txt>"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-10-30"
msgstr "30 octobre 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: Plain text
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid ""
"$ B<stat -c \"%t %T\" /dev/null>\n"
"1 3\n"
"$ B<readlink /sys/dev/char/1\\e:3>\n"
"\\&../../devices/virtual/mem/null\n"
"$ B<ls -Fd /sys/devices/virtual/mem/null>\n"
"/sys/devices/virtual/mem/null/\n"
"$ B<ls -d1 /sys/devices/virtual/mem/null/*>\n"
"/sys/devices/virtual/mem/null/dev\n"
"/sys/devices/virtual/mem/null/power/\n"
"/sys/devices/virtual/mem/null/subsystem@\n"
"/sys/devices/virtual/mem/null/uevent\n"
msgstr ""
"$ B<stat -c \"%t %T\" /dev/null>\n"
"1 3\n"
"$ B<readlink /sys/dev/char/1\\e:3>\n"
"\\&../../devices/virtual/mem/null\n"
"$ B<ls -Fd /sys/devices/virtual/mem/null>\n"
"/sys/devices/virtual/mem/null/\n"
"$ B<ls -d1 /sys/devices/virtual/mem/null/*>\n"
"/sys/devices/virtual/mem/null/dev\n"
"/sys/devices/virtual/mem/null/power/\n"
"/sys/devices/virtual/mem/null/subsystem@\n"
"/sys/devices/virtual/mem/null/uevent\n"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONS"

#. type: Plain text
#: debian-bookworm
msgid "The B<sysfs> filesystem first appeared in Linux 2.6.0."
msgstr "Le système de fichiers B<sysfs> est apparu dans Linux 2.6.0."

#. type: Plain text
#: debian-bookworm
msgid "The B<sysfs> filesystem is Linux-specific."
msgstr "Le système de fichiers B<sysfs> est spécifique à Linux."

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Pages du manuel de Linux 6.9.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 octobre 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pages du manuel de Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Pages du manuel de Linux (non publiées)"
