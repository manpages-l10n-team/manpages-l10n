# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.23.1\n"
"POT-Creation-Date: 2025-02-28 16:35+0100\n"
"PO-Revision-Date: 2024-10-14 13:33+0200\n"
"Last-Translator: Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 22.12.3\n"

#. type: TH
#: fedora-42 fedora-rawhide
#, no-wrap
msgid "GRUB-SWITCH-TO-BLSCFG"
msgstr "GRUB-SWITCH-TO-BLSCFG"

#. type: TH
#: fedora-42 fedora-rawhide
#, no-wrap
msgid "February 2025"
msgstr "Février 2025"

#. type: TH
#: fedora-42 fedora-rawhide
#, no-wrap
msgid "GRUB 2.12"
msgstr "GRUB 2.12"

#. type: TH
#: fedora-42 fedora-rawhide
#, no-wrap
msgid "System Administration Utilities"
msgstr "Utilitaires d'administration système"

#. type: SH
#: fedora-42 fedora-rawhide
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: fedora-42 fedora-rawhide
msgid "grub-switch-to-blscfg - switch to using BLS config files"
msgstr ""
"grub-switch-to-blscfg – Passer à l'utilisation des fichiers de configuration "
"BLS"

#. type: SH
#: fedora-42 fedora-rawhide
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: fedora-42 fedora-rawhide
msgid "B<grub-switch-to-blscfg>"
msgstr "B<grub-switch-to-blscfg>"

#. type: SH
#: fedora-42 fedora-rawhide
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: fedora-42 fedora-rawhide
msgid "Switch to BLS config files."
msgstr "Passer à l'utilisation des fichiers de configuration BLS."

#. type: TP
#: fedora-42 fedora-rawhide
#, no-wrap
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: fedora-42 fedora-rawhide
msgid "print this message and exit"
msgstr "Afficher ce message et quitter."

#. type: TP
#: fedora-42 fedora-rawhide
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: fedora-42 fedora-rawhide
msgid "print the version information and exit"
msgstr "Afficher les informations de version et quitter."

#. type: TP
#: fedora-42 fedora-rawhide
#, no-wrap
msgid "B<--backup-suffix>=I<\\,SUFFIX\\/>"
msgstr "B<--backup-suffix>=I<\\,SUFFIXE\\/>"

#. type: Plain text
#: fedora-42 fedora-rawhide
msgid "\\&.bak"
msgstr "\\&.bak"

#. type: TP
#: fedora-42 fedora-rawhide
#, no-wrap
msgid "B<--bls-directory>=I<\\,DIR\\/>"
msgstr "B<--bls-directory>=I<\\,RÉP\\/>"

#. type: Plain text
#: fedora-42 fedora-rawhide
msgid "I<\\,/boot/loader/entries\\/>"
msgstr "I<\\,/boot/loader/entries\\/>"

#. type: TP
#: fedora-42 fedora-rawhide
#, no-wrap
msgid "B<--config-file>=I<\\,FILE\\/>"
msgstr "B<--config-file>=I<\\,FICHIER\\/>"

#. type: Plain text
#: fedora-42 fedora-rawhide
msgid "I<\\,/etc/grub2-efi.cfg\\/>"
msgstr "I<\\,/etc/grub2-efi.cfg\\/>"

#. type: TP
#: fedora-42 fedora-rawhide
#, no-wrap
msgid "B<--grub-defaults>=I<\\,FILE\\/>"
msgstr "B<--grub-defaults>=I<\\,FICHIER\\/>"

#. type: Plain text
#: fedora-42 fedora-rawhide
msgid "I<\\,/etc/default/grub\\/>"
msgstr "I<\\,/etc/default/grub\\/>"

#. type: TP
#: fedora-42 fedora-rawhide
#, no-wrap
msgid "B<--grub-directory>=I<\\,DIR\\/>"
msgstr "B<--grub-directory>=I<\\,RÉP\\/>"

#. type: Plain text
#: fedora-42 fedora-rawhide
msgid "/boot/efi/EFI/fedora/"
msgstr "I</boot/efi/EFI/fedora/>"

#. type: SH
#: fedora-42 fedora-rawhide
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: fedora-42 fedora-rawhide
msgid ""
"The full documentation for B<grub-switch-to-blscfg> is maintained as a "
"Texinfo manual.  If the B<info> and B<grub-switch-to-blscfg> programs are "
"properly installed at your site, the command"
msgstr ""
"La documentation complète de B<grub-switch-to-blscfg> est disponible dans un "
"manuel Texinfo. Si les programmes B<info> et B<grub-switch-to-blscfg> sont "
"correctement installés, la commande"

#. type: Plain text
#: fedora-42 fedora-rawhide
msgid "B<info grub-switch-to-blscfg>"
msgstr "B<info grub-switch-to-blscfg>"

#. type: Plain text
#: fedora-42 fedora-rawhide
msgid "should give you access to the complete manual."
msgstr "devrait vous donner accès au manuel complet."
