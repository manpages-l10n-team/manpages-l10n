# Ukrainian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Yuri Chornoivan <yurchor@ukr.net>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-03-29 09:49+0100\n"
"PO-Revision-Date: 2022-08-25 21:05+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <trans-uk@lists.fedoraproject.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 20.12.0\n"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "READPROFILE"
msgstr "READPROFILE"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr "11 травня 2022 року"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "System Administration"
msgstr "Керування системою"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NAME"
msgstr "НАЗВА"

#. type: Plain text
#: debian-bookworm
msgid "readprofile - read kernel profiling information"
msgstr "readprofile — витання даних профілювання ядра"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SYNOPSIS"
msgstr "КОРОТКИЙ ОПИС"

#. type: Plain text
#: debian-bookworm
msgid "B<readprofile> [options]"
msgstr "B<readprofile> [параметри]"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "VERSION"
msgstr "ВЕРСІЯ"

#. type: Plain text
#: debian-bookworm
msgid "This manpage documents version 2.0 of the program."
msgstr "На цій сторінці підручника описано версію 2.0 програми."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИС"

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<readprofile> command uses the I</proc/profile> information to print "
"ascii data on standard output. The output is organized in three columns: the "
"first is the number of clock ticks, the second is the name of the C function "
"in the kernel where those many ticks occurred, and the third is the "
"normalized `load\\(aq of the procedure, calculated as a ratio between the "
"number of ticks and the length of the procedure. The output is filled with "
"blanks to ease readability."
msgstr ""
"Програма B<readprofile> використовує відомості I</proc/profile> для "
"виведення даних ascii до стандартного виведення. Виведені дані буде поділено "
"між трьома стовпчиками: у першому буде виведено такти годинника, у другому — "
"назву функції C у ядрі, у якій відбулася ця кількість тактів, а у третьому — "
"нормалізоване «навантаження» процедури, обчислене, як відношення кількості "
"тактів до довжини процедури. Виведені дані буде доповнено пробілами для "
"полегшення читання."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "OPTIONS"
msgstr "ПАРАМЕТРИ"

#. type: Plain text
#: debian-bookworm
msgid "B<-a>, B<--all>"
msgstr "B<-a>, B<--all>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Print all symbols in the mapfile. By default the procedures with reported "
"ticks are not printed."
msgstr ""
"Вивести усі символи у файлі карти. Типово, процедури із повідомленими "
"позначками виведено не буде."

#. type: Plain text
#: debian-bookworm
msgid "B<-b>, B<--histbin>"
msgstr "B<-b>, B<--histbin>"

#. type: Plain text
#: debian-bookworm
msgid "Print individual histogram-bin counts."
msgstr ""
"Вивести окремі значення позначок гістограми\n"
"."

#. type: Plain text
#: debian-bookworm
msgid "B<-i>, B<--info>"
msgstr "B<-i>, B<--info>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Info. This makes B<readprofile> only print the profiling step used by the "
"kernel. The profiling step is the resolution of the profiling buffer, and is "
"chosen during kernel configuration (through B<make config>), or in the "
"kernel\\(cqs command line. If the B<-t> (terse) switch is used together with "
"B<-i> only the decimal number is printed."
msgstr ""
"Відомості. Наказує B<readprofile> лише вивести крок профілювання, який "
"використано ядром. Крок профілювання є роздільністю буфера профілювання. "
"Його вибір відбувається під час налаштовування ядра (за допомогою B<make "
"config>) або у рядку команди ядра. Якщо використано перемикач B<-t> (terse) "
"разом із B<-i>, буде виведено лише десяткове число."

#. type: Plain text
#: debian-bookworm
msgid "B<-m>, B<--mapfile> I<mapfile>"
msgstr "B<-m>, B<--mapfile> I<файл-карти>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Specify a mapfile, which by default is I</usr/src/linux/System.map>. You "
"should specify the map file on cmdline if your current kernel isn\\(cqt the "
"last one you compiled, or if you keep System.map elsewhere. If the name of "
"the map file ends with I<.gz> it is decompressed on the fly."
msgstr ""
"Вказати файл карти, типовим є I</usr/src/linux/System.map>. Вам слід вказати "
"файл карти у рядку команди, якщо ваше поточне ядро не є останнім ядром, яке "
"ви зібрали, або якщо ви зберігаєте ваш System.map у якомусь нестандартному "
"місці. Якщо назва файла карти завершується на I<.gz>, його вміст буде "
"розпаковано на льоту."

#. type: Plain text
#: debian-bookworm
msgid "B<-M>, B<--multiplier> I<multiplier>"
msgstr "B<-M>, B<--multiplier> I<множник>"

#. type: Plain text
#: debian-bookworm
msgid ""
"On some architectures it is possible to alter the frequency at which the "
"kernel delivers profiling interrupts to each CPU. This option allows you to "
"set the frequency, as a multiplier of the system clock frequency, HZ. Linux "
"2.6.16 dropped multiplier support for most systems. This option also resets "
"the profiling buffer, and requires superuser privileges."
msgstr ""
"На деяких архітектурах можна змінювати частоту, з якою ядро надсилає "
"переривання профілювання до кожного з процесорів. За допомогою цього "
"параметра ви можете встановити частоту, як коефіцієнт частоти годинника "
"системи, у Гц. У Linux 2.6.16 відкинуто підтримку коефіцієнта у більшості "
"систем. Цей параметр також скидає буфер профілювання. Його використання "
"потребує привілеїв надкористувача."

#. type: Plain text
#: debian-bookworm
msgid "B<-p>, B<--profile> I<pro-file>"
msgstr "B<-p>, B<--profile> I<файл-профілювання>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Specify a different profiling buffer, which by default is I</proc/profile>. "
"Using a different pro-file is useful if you want to `freeze\\(aq the kernel "
"profiling at some time and read it later. The I</proc/profile> file can be "
"copied using B<cat>(1) or B<cp>(1). There is no more support for compressed "
"profile buffers, like in B<readprofile-1.1>, because the program needs to "
"know the size of the buffer in advance."
msgstr ""
"Вказати інший буфер профілювання, типовим є I</proc/profile>. Використання "
"іншого профілю є корисним, якщо ви хочете «заморозити» профілювання ядра у "
"певний момент часу і прочитати його пізніше. Файл I</proc/profile> може бути "
"скопійовано за допомогою B<cat>(1) або B<cp>(1). Підтримки стиснених буферів "
"профілювання, подібних до передбачених у *readprofile-1.1*, не передбачено, "
"оскільки програмі потрібно буде наперед знати розмір буфера."

#. type: Plain text
#: debian-bookworm
msgid "B<-r>, B<--reset>"
msgstr "B<-r>, B<--reset>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Reset the profiling buffer. This can only be invoked by root, because I</"
"proc/profile> is readable by everybody but writable only by the superuser. "
"However, you can make B<readprofile> set-user-ID 0, in order to reset the "
"buffer without gaining privileges."
msgstr ""
"Скинути буфер профілювання. Цю команду може бути викликано лише від імені "
"root, оскільки читання I</proc/profile> може здійснювати будь-який "
"користувач, а запис — лише надкористувач. Втім, ви можете встановити для "
"B<readprofile> set-user-ID 0, щоб скинути буфер без набуття додаткових прав "
"доступу."

#. type: Plain text
#: debian-bookworm
msgid "B<-s, --counters>"
msgstr "B<-s, --counters>"

#. type: Plain text
#: debian-bookworm
msgid "Print individual counters within functions."
msgstr "Вивести значення окремих лічильників у функціях."

#. type: Plain text
#: debian-bookworm
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Verbose. The output is organized in four columns and filled with blanks. The "
"first column is the RAM address of a kernel function, the second is the name "
"of the function, the third is the number of clock ticks and the last is the "
"normalized load."
msgstr ""
"Докладний режим. Виведені дані буде упорядковано за чотирма стовпчиками і "
"доповнено пробілами. У першому стовпчику буде показано адресу функції ядра в "
"оперативній пам'яті, у другому — назву функції, у третьому — кількість "
"тактів годинника, а в останньому — нормалізоване навантаження."

#. type: Plain text
#: debian-bookworm
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: debian-bookworm
msgid "Display help text and exit."
msgstr "Вивести текст довідки і завершити роботу."

#. type: Plain text
#: debian-bookworm
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: debian-bookworm
msgid "Print version and exit."
msgstr "Вивести дані щодо версії і завершити роботу."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "FILES"
msgstr "ФАЙЛИ"

#. type: Plain text
#: debian-bookworm
msgid "I</proc/profile>"
msgstr "I</proc/profile>"

#. type: Plain text
#: debian-bookworm
msgid "A binary snapshot of the profiling buffer."
msgstr "Двійковий знімок буфера профілювання."

#. type: Plain text
#: debian-bookworm
msgid "I</usr/src/linux/System.map>"
msgstr "I</usr/src/linux/System.map>"

#. type: Plain text
#: debian-bookworm
msgid "The symbol table for the kernel."
msgstr "Таблиця символів для ядра."

#. type: Plain text
#: debian-bookworm
msgid "I</usr/src/linux/*>"
msgstr "I</usr/src/linux/*>"

#. type: Plain text
#: debian-bookworm
msgid "The program being profiled :-)"
msgstr "Програма, профілювання якої виконується :-)"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "BUGS"
msgstr "ВАДИ"

#. type: Plain text
#: debian-bookworm
msgid ""
"B<readprofile> only works with a 1.3.x or newer kernel, because I</proc/"
"profile> changed in the step from 1.2 to 1.3."
msgstr ""
"B<readprofile> працює лише з ядром 1.3.x та новішими ядрами, оскільки I</"
"proc/profile> було змінено десь між версіями 1.2 та 1.3."

#. type: Plain text
#: debian-bookworm
msgid ""
"This program only works with ELF kernels. The change for a.out kernels is "
"trivial, and left as an exercise to the a.out user."
msgstr ""
"Ця програма працює лише з ядрами ELF. Зміна для ядер a.out є тривіальною, її "
"може без проблем виконати користувач a.out."

#. type: Plain text
#: debian-bookworm
msgid ""
"To enable profiling, the kernel must be rebooted, because no profiling "
"module is available, and it wouldn\\(cqt be easy to build. To enable "
"profiling, you can specify B<profile>=I<2> (or another number) on the kernel "
"commandline. The number you specify is the two-exponent used as profiling "
"step."
msgstr ""
"Щоб увімкнути профілювання, ядро має бути перезавантажено, оскільки не буде "
"доступним модуль профілювання, а зібрати його не просто. Для вмикання "
"профілювання ви можете вказати B<profile>=I<2> (або інше число) у рядку "
"команди ядра. Вказане вами число має бути степенем двійки, який буде "
"використано як крок профілювання."

#. type: Plain text
#: debian-bookworm
msgid ""
"Profiling is disabled when interrupts are inhibited. This means that many "
"profiling ticks happen when interrupts are re-enabled. Watch out for "
"misleading information."
msgstr ""
"Профілювання вимкнено, якщо заборонено переривання. Це означає, що "
"відбудеться багато тактів профілювання, коли переривання буде повторно "
"увімкнено. Остерігайтеся помилкових даних."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "EXAMPLE"
msgstr "ПРИКЛАДИ"

#. type: Plain text
#: debian-bookworm
msgid "Browse the profiling buffer ordering by clock ticks:"
msgstr ""
"Навігація буфером профілювання, який упорядковано за тактами годинника:"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "   readprofile | sort -nr | less\n"
msgstr "   readprofile | sort -nr | less\n"

#. type: Plain text
#: debian-bookworm
msgid "Print the 20 most loaded procedures:"
msgstr "Вивести 20 найбільш завантажуваних процедур:"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "   readprofile | sort -nr +2 | head -20\n"
msgstr "   readprofile | sort -nr +2 | head -20\n"

#. type: Plain text
#: debian-bookworm
msgid "Print only filesystem profile:"
msgstr "Вивести лише профіль файлової системи:"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "   readprofile | grep _ext2\n"
msgstr "   readprofile | grep _ext2\n"

#. type: Plain text
#: debian-bookworm
msgid "Look at all the kernel information, with ram addresses:"
msgstr "Шукати в усіх даних ядра із адресами в оперативній пам'яті:"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "   readprofile -av | less\n"
msgstr "   readprofile -av | less\n"

#. type: Plain text
#: debian-bookworm
msgid "Browse a \\(aqfrozen\\(aq profile buffer for a non current kernel:"
msgstr ""
"Навігація «замороженим» буфером профілювання для ядра, відмінного від "
"поточного:"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "   readprofile -p ~/profile.freeze -m /zImage.map.gz\n"
msgstr "   readprofile -p ~/profile.freeze -m /zImage.map.gz\n"

#. type: Plain text
#: debian-bookworm
msgid "Request profiling at 2kHz per CPU, and reset the profiling buffer:"
msgstr ""
"Надіслати запит щодо профілювання при 2кГц на процесор і скинути вміст "
"буфера профілювання:"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "   sudo readprofile -M 20\n"
msgstr "   sudo readprofile -M 20\n"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "REPORTING BUGS"
msgstr "ЗВІТИ ПРО ВАДИ"

#. type: Plain text
#: debian-bookworm
msgid "For bug reports, use the issue tracker at"
msgstr "Для звітування про вади використовуйте систему стеження помилками на"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AVAILABILITY"
msgstr "ДОСТУПНІСТЬ"

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<readprofile> command is part of the util-linux package which can be "
"downloaded from"
msgstr "B<readprofile> є частиною пакунка util-linux, який можна отримати з"
