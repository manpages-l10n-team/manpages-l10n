# Ukrainian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Yuri Chornoivan <yurchor@ukr.net>, 2022, 2023, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2025-02-28 16:46+0100\n"
"PO-Revision-Date: 2024-12-15 19:02+0200\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <trans-uk@lists.fedoraproject.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 23.04.3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RM"
msgstr "RM"

#. type: TH
#: archlinux fedora-rawhide
#, no-wrap
msgid "February 2025"
msgstr "Лютий 2025 року"

#. type: TH
#: archlinux fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "GNU coreutils 9.6"
msgstr "GNU coreutils 9.6"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Команди користувача"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "НАЗВА"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "rm - remove files or directories"
msgstr "rm — вилучення файлів або каталогів"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "КОРОТКИЙ ОПИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<rm> [I<\\,OPTION\\/>]... [I<\\,FILE\\/>]..."
msgstr "B<rm> [I<\\,ПАРАМЕТР\\/>]... [I<\\,ФАЙЛ\\/>]..."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This manual page documents the GNU version of B<rm>.  B<rm> removes each "
"specified file.  By default, it does not remove directories."
msgstr ""
"Цю сторінку підручника присвячено документації версії GNU команди B<rm>. "
"B<rm> вилучає усі вказані програмі файли. Типово, програма не вилучає "
"каталогів."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the I<-I> or I<--interactive=once> option is given, and there are more "
"than three files or the I<-r>, I<-R>, or I<--recursive> are given, then "
"B<rm> prompts the user for whether to proceed with the entire operation.  If "
"the response is not affirmative, the entire command is aborted."
msgstr ""
"Якщо вказано параметр I<-I> or I<--interactive=once> і три або більше файлів "
"чи вказано параметри I<-r>, I<-R> або I<--recursive>, B<rm> запитуватиме "
"користувача про те, чи хоче він/вона продовжити виконання дії. Якщо не буде "
"отримано підтвердження, виконання команди буде перервано."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Otherwise, if a file is unwritable, standard input is a terminal, and the I<-"
"f> or I<--force> option is not given, or the I<-i> or I<--"
"interactive=always> option is given, B<rm> prompts the user for whether to "
"remove the file.  If the response is not affirmative, the file is skipped."
msgstr ""
"В інших випадках, якщо файл є непридатним до запису, терміналом є стандартне "
"джерело вхідних даних і не вказано параметр I<-f> або I<--force>, або "
"вказано параметр I<-i> чи I<--interactive=always>, B<rm> запитуватиме у "
"користувача, чи слід вилучати файл. Якщо не буде отримано підтвердження, "
"файл буде пропущено."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "ПАРАМЕТРИ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Remove (unlink) the FILE(s)."
msgstr "Вилучити (скасувати посилання) ФАЙЛ(и)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-f>, B<--force>"
msgstr "B<-f>, B<--force>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "ignore nonexistent files and arguments, never prompt"
msgstr ""
"ігнорувати файли та аргументи, яких не існує, ніколи не запитувати "
"користувача про варіант дій"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-i>"
msgstr "B<-i>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "prompt before every removal"
msgstr "запитувати перед кожним вилученням"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-I>"
msgstr "B<-I>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"prompt once before removing more than three files, or when removing "
"recursively; less intrusive than B<-i>, while still giving protection "
"against most mistakes"
msgstr ""
"запитувати один раз перед вилученням більше, ніж трьох файлів, або при "
"рекурсивному вилученні; менш набридливий варіант B<-i>, який забезпечує "
"захист від більшості помилок"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--interactive>[=I<\\,WHEN\\/>]"
msgstr "B<--interactive>[=I<\\,УМОВА\\/>]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"prompt according to WHEN: never, once (B<-I>), or always (B<-i>); without "
"WHEN, prompt always"
msgstr ""
"запитувати відповідно до УМОВИ: never (ніколи), once (один раз, B<-I>) або "
"always (завжди, B<-i>); без УМОВИ, запитувати завжди"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--one-file-system>"
msgstr "B<--one-file-system>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"when removing a hierarchy recursively, skip any directory that is on a file "
"system different from that of the corresponding command line argument"
msgstr ""
"при вилученні ієрархії рекурсивно, пропускати каталоги, що зберігаються на "
"інших файлових системах, ніж вказані аргументами командного рядку"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--no-preserve-root>"
msgstr "B<--no-preserve-root>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "do not treat '/' specially"
msgstr "не обробляти «/» особливим чином"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--preserve-root>[=I<\\,all\\/>]"
msgstr "B<--preserve-root>[=I<\\,all\\/>]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"do not remove '/' (default); with 'all', reject any command line argument on "
"a separate device from its parent"
msgstr ""
"не вилучати «/» (типова поведінка); з «all», відкидати усі аргументи "
"командного рядка на окремому пристрої з його батьківського запису"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-r>, B<-R>, B<--recursive>"
msgstr "B<-r>, B<-R>, B<--recursive>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "remove directories and their contents recursively"
msgstr "вилучати каталоги та їхній вміст рекурсивно"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-d>, B<--dir>"
msgstr "B<-d>, B<--dir>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "remove empty directories"
msgstr "вилучати порожні каталоги"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "explain what is being done"
msgstr "пояснити виконувані дії"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "відображає довідку і виходить"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "виводить інформацію про версію і виходить"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"By default, rm does not remove directories.  Use the B<--recursive> (B<-r> "
"or B<-R>)  option to remove each listed directory, too, along with all of "
"its contents."
msgstr ""
"Зазвичай rm не вилучає каталоги.  Використовуйте ключ B<--recursive> (B<-r> "
"або B<-R>), щоб вилучити всі перелічені каталоги разом з їхнім вмістом."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Any attempt to remove a file whose last file name component is '.' or '..' "
"is rejected with a diagnostic."
msgstr ""
"Будь-які спроби вилучити файл, останньою компонентою назви якого є «.» або "
"«..», буде відкинуто із діагностичним повідомленням."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"To remove a file whose name starts with a '-', for example '-foo', use one "
"of these commands:"
msgstr ""
"Для вилучення файла, що починається з «-» (приклад: «-foo»), скористайтеся "
"однією з таких команд:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "rm B<--> B<-foo>"
msgstr "rm B<--> B<-foo>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "rm ./-foo"
msgstr "rm ./-foo"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"If you use rm to remove a file, it might be possible to recover some of its "
"contents, given sufficient expertise and/or time.  For greater assurance "
"that the contents are unrecoverable, consider using B<shred>(1)."
msgstr ""
"При використанні rm для вилучення файла його вміст зазвичай можна відновити. "
"Використовуйте B<shred>(1), якщо потрібна більша впевненість у неможливості "
"відновлення вмісту."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "АВТОР"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Written by Paul Rubin, David MacKenzie, Richard M. Stallman, and Jim "
"Meyering."
msgstr ""
"Авторами програми є Paul Rubin, David MacKenzie, Richard M. Stallman і Jim "
"Meyering."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "ЗВІТИ ПРО ВАДИ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Мережева довідка GNU coreutils: E<lt>https://www.gnu.org/software/coreutils/"
"E<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Повідомляйте про будь-які помилки в перекладі на E<lt>https://"
"translationproject.org/team/E<gt>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "АВТОРСЬКІ ПРАВА"

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"Copyright \\(co 2025 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Авторські права \\(co 2025 Free Software Foundation, Inc.  Ліцензія GPLv3+: "
"GNU GPL версії 3 або пізнішої E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Це вільне програмне забезпечення: Ви можете вільно змінювати і "
"розповсюджувати його. БЕЗ ЖОДНИХ ГАРАНТІЙ, в межах, дозволених законом."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ДИВ. ТАКОЖ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<unlink>(1), B<unlink>(2), B<chattr>(1), B<shred>(1)"
msgstr "B<unlink>(1), B<unlink>(2), B<chattr>(1), B<shred>(1)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Full documentation E<lt>https://www.gnu.org/software/coreutils/rmE<gt>"
msgstr ""
"Повна документація: E<lt>https://www.gnu.org/software/coreutils/rmE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "or available locally via: info \\(aq(coreutils) rm invocation\\(aq"
msgstr ""
"або доступна локально через виклик info \\(aq(coreutils) rm invocation\\(aq"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "September 2022"
msgstr "Вересень 2022 року"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid ""
"Note that if you use rm to remove a file, it might be possible to recover "
"some of its contents, given sufficient expertise and/or time.  For greater "
"assurance that the contents are truly unrecoverable, consider using "
"B<shred>(1)."
msgstr ""
"Зауважте, що при використанні rm для вилучення файла його вміст зазвичай "
"можна відновити. Використовуйте B<shred>(1), якщо потрібна більша "
"впевненість у неможливості відновлення вмісту."

#. type: Plain text
#: debian-bookworm
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Авторські права \\(co 2022 Free Software Foundation, Inc.  Ліцензія GPLv3+: "
"GNU GPL версії 3 або пізнішої E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: debian-unstable
#, no-wrap
msgid "October 2024"
msgstr "Жовтень 2024 року"

#. type: TH
#: debian-unstable mageia-cauldron
#, no-wrap
msgid "GNU coreutils 9.5"
msgstr "GNU coreutils 9.5"

#. type: Plain text
#: debian-unstable mageia-cauldron
msgid ""
"Copyright \\(co 2024 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Авторські права \\(co 2024 Free Software Foundation, Inc.  Ліцензія GPLv3+: "
"GNU GPL версії 3 або пізнішої E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: fedora-42 opensuse-tumbleweed
#, no-wrap
msgid "January 2025"
msgstr "Січень 2025 року"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "March 2024"
msgstr "Березень 2024 року"

#. type: TH
#: opensuse-leap-16-0
#, no-wrap
msgid "January 2024"
msgstr "Січень 2024 року"

#. type: TH
#: opensuse-leap-16-0
#, no-wrap
msgid "GNU coreutils 9.4"
msgstr "GNU coreutils 9.4"

#. type: Plain text
#: opensuse-leap-16-0
msgid ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Авторські права \\(co 2023 Free Software Foundation, Inc.  Ліцензія GPLv3+: "
"GNU GPL версії 3 або пізнішої E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
