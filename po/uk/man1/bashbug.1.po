# Ukrainian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Yuri Chornoivan <yurchor@ukr.net>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2025-02-16 05:41+0100\n"
"PO-Revision-Date: 2023-02-01 20:29+0200\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <trans-uk@lists.fedoraproject.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 20.12.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "BASHBUG"
msgstr "BASHBUG"

#. type: TH
#: archlinux fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2020 August 1"
msgstr "1 серпня 2020 року"

#. type: TH
#: archlinux fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "GNU Bash 5.1"
msgstr "GNU Bash 5.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "НАЗВА"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "bashbug - report a bug in bash"
msgstr "bashbug - сповіщення про ваду у bash"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "КОРОТКИЙ ОПИС"

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<bashbug> [I<--version>] [I<--help>] [I<email-address>]"
msgstr "B<bashbug> [I<--version>] [I<--help>] [I<адреса-електронної-пошти>]"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИС"

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"B<bashbug> is a shell script to help the user compose and mail bug reports "
"concerning bash in a standard format.  B<bashbug> invokes the editor "
"specified by the environment variable E<.SM> B<EDITOR> on a temporary copy "
"of the bug report format outline. The user must fill in the appropriate "
"fields and exit the editor.  B<bashbug> then mails the completed report to "
"I<bug-bash@gnu.org>, or I<email-address>.  If the report cannot be mailed, "
"it is saved in the file I<dead.bashbug> in the invoking user's home "
"directory."
msgstr ""
"B<bashbug> є скриптом командної оболонки, який допомагає користувачам "
"складати і надсилати електронною поштою звіти у стандартному форматі щодо "
"вад, що стосуються bash. B<bashbug> викликає редактор, який визначено "
"змінною середовища E<.SM> B<EDITOR> для тимчасової копії каркаса "
"форматованого звіту щодо вади. Користувачеві слід заповнити відповідні поля "
"і вийти з редактора. Після цього B<bashbug> надішле електронною поштою "
"заповнений звіт на адресу I<bug-bash@gnu.org> або I<адресу-електронної-"
"пошти>. Якщо звіт не може бути надіслано електронною поштою, його буде "
"збережено до файла I<dead.bashbug> у домашньому каталозі користувача, який "
"викликав програму."

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"The bug report format outline consists of several sections.  The first "
"section provides information about the machine, operating system, the bash "
"version, and the compilation environment.  The second section should be "
"filled in with a description of the bug.  The third section should be a "
"description of how to reproduce the bug.  The optional fourth section is for "
"a proposed fix.  Fixes are encouraged."
msgstr ""
"Каркас форматованого звіту щодо вади складається з декількох розділів. У "
"першому розділі буде зібрано відомості щодо архітектури комп'ютера, "
"операційної системи, версії bash та середовища збирання. У другому розділі "
"має бути заповнено опис вади. У третьому розділі має бути опис способу "
"відтворення вади. Необов'язковий четвертий розділ призначено для "
"пропонованого виправлення. Ми будемо раді вашим виправленням."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ENVIRONMENT"
msgstr "СЕРЕДОВИЩЕ"

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"B<bashbug> will utilize the following environment variables if they exist:"
msgstr ""
"B<bashbug> використовуватиме такі змінні середовища, якщо їх визначено:"

#. #-#-#-#-#  archlinux: bashbug.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  debian-bookworm: bashbug.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  debian-unstable: bashbug.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  fedora-42: bashbug.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-rawhide: bashbug.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  mageia-cauldron: bashbug.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  opensuse-leap-16-0: bashbug.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  opensuse-tumbleweed: bashbug.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EDITOR>"
msgstr "B<EDITOR>"

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Specifies the preferred editor. If E<.SM> B<EDITOR> is not set, B<bashbug> "
"attempts to locate a number of alternative editors, including B<emacs>.  If "
"B<bashbug> cannot locate any of the alternative editors, it attempts to "
"execute B<vi>."
msgstr ""
"Визначає бажаний редактор. Якщо E<.SM> B<EDITOR> не встановлено, B<bashbug> "
"спробує виявити один з декількох альтернативних редакторів, зокрема "
"B<emacs>. Якщо B<bashbug> не зможе знайти жодного з альтернативних "
"редакторів, буде виконано спробу запустити B<vi>."

#. type: TP
#: archlinux fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<HOME>"
msgstr "B<HOME>"

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Directory in which the failed bug report is saved if the mail fails."
msgstr ""
"Каталог, у якому буде збережено звіт щодо вади, якщо надіслати його "
"електронною поштою не вдасться."

#. type: TP
#: archlinux fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<TMPDIR>"
msgstr "B<TMPDIR>"

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Directory in which to create temporary files and directories."
msgstr "Каталог, у якому слід створити тимчасові файли і каталоги."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ДИВ. ТАКОЖ"

#. type: TP
#: archlinux fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "I<bash>(1)"
msgstr "I<bash>(1)"

#. type: SH
#: archlinux fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "AUTHORS"
msgstr "АВТОРИ"

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Brian Fox, Free Software Foundation"
msgstr "Brian Fox, Free Software Foundation"

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "bfox@gnu.org"
msgstr "bfox@gnu.org"

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Chet Ramey, Case Western Reserve University"
msgstr "Chet Ramey, Західний резервний університет Кейса"

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "chet@po.cwru.edu"
msgstr "chet@po.cwru.edu"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "11 December 2007"
msgstr "11 грудня 2007 року"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "GNU Bash 3.1"
msgstr "GNU Bash 3.1"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "bashbug"
msgstr "bashbug"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<bashbug> [B<--help>] [B<--version>] [I<bug-report-email-addresses>]"
msgstr ""
"B<bashbug> [B<--help>] [B<--version>] [I<адреси-електронної-пошти-звіту-про-"
"ваду>]"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<bashbug> is a utility for reporting bugs in Bash to the maintainers."
msgstr ""
"B<bashbug> є допоміжною програмою для звітування розробникам щодо вад у Bash."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"B<bashbug> will start up your preferred editor with a preformatted bug "
"report template for you to fill in. Save the file and quit the editor once "
"you have completed the missing fields.  B<bashbug> will notify you of any "
"problems with the report and ask for confirmation before sending it. By "
"default the bug report is mailed to both the GNU developers and the Debian "
"Bash maintainers. The recipients can be changed by giving a comma separated "
"list of I<bug-report-email-addresses>."
msgstr ""
"B<bashbug> запустить ваш бажаний редактор із попередньо форматованим "
"шаблоном звітування щодо вад. Ви маєте заповнити цей шаблон. Збережіть файл "
"і завершіть роботу редактора, щойно буде заповнено усі поля. B<bashbug> "
"сповістить вас про будь-які проблеми зі звітом і попросить вас підтвердити "
"надсилання. Типово, звіт щодо вади буде надіслано електронною поштою одразу "
"розробникам GNU та супровідникам Bash у Debian. Отримувачів можна змінити, "
"вказавши список відокремлених комами адрес I<адреси-електронної-пошти-звіту-"
"про-ваду>."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"If you invoke B<bashbug> by accident, just quit your editor. You will always "
"be asked for confirmation before a bug report is sent."
msgstr ""
"Якщо ви запустите B<bashbug> випадково, просто завершіть роботу редактора. "
"Програма завжди питатиме у вас про підтвердження до того, як буде надіслано "
"звіт щодо вади."

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "OPTIONS"
msgstr "ПАРАМЕТРИ"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "Show a brief usage message and exit."
msgstr ""
"Показати коротке повідомлення щодо користування програмою та закривається."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "Show the version of B<bashbug> and exit."
msgstr "Показати версію B<bashbug> і завершити роботу."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<bug-report-email-addresses>"
msgstr "B<bug-report-email-addresses>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Comma separated list of recipients\\' email addresses. By default the report "
"is mailed to both the GNU developers and the Debian Bash maintainers."
msgstr ""
"Список відокремлених комами адрес електронної пошти отримувачів. Типово, "
"звіт буде надіслано електронною поштою одразу розробникам GNU та "
"супровідникам Bash у Debian."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<DEFEDITOR>"
msgstr "B<DEFEDITOR>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "Editor to use for editing the bug report."
msgstr "Редактор, який буде використано для редагування звіту щодо вади."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "Editor to use for editing the bug report (overridden by B<DEFEDITOR>)."
msgstr ""
"Редактор, який буде використано для редагування звіту щодо вади (може бути "
"перевизначено за допомогою B<DEFEDITOR>)."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"B<bash>(1), B<reportbug>(1), B<update-alternatives>(8)  for preferred editor."
msgstr ""
"B<bash>(1), B<reportbug>(1), B<update-alternatives>(8) щодо бажаного "
"редактора."

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "AUTHOR"
msgstr "АВТОР"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"This manual page was written by Christer Andersson E<lt>klamm@comhem.seE<gt> "
"for the Debian project (but may be used by others)."
msgstr ""
"Цю сторінку підручника написав Christer Andersson E<lt>klamm@comhem.seE<gt> "
"для проєкту Debian (але може використовуватися й на інших)."
