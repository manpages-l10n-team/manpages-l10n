# Hungarian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Kovács Emese <emese@eik.bme.hu>, 2001.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2025-02-16 05:59+0100\n"
"PO-Revision-Date: 2001-01-05 12:34+0100\n"
"Last-Translator: Kovács Emese <emese@eik.bme.hu>\n"
"Language-Team: Hungarian <>\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.12.0\n"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "RUP"
msgstr "RUP"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Sept 25, 1995"
msgstr "1995. szeptember 25."

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Linux 1.2"
msgstr "Linux 1.2"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Linux Programmer's Manuel"
msgstr "Linux Programozói Kézikönyv"

#. #-#-#-#-#  debian-bookworm: rup.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  debian-unstable: rup.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  fedora-42: rup.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Sh
#. #-#-#-#-#  fedora-rawhide: rup.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Sh
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "NAME"
msgstr "NÉV"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "rup - remote uptime display"
msgstr "rup - távoli állapot kijelző"

#. #-#-#-#-#  debian-bookworm: rup.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  debian-unstable: rup.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  fedora-42: rup.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Sh
#. #-#-#-#-#  fedora-rawhide: rup.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Sh
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÖSSZEGZÉS"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<rup> { -u | -v | I<hosts> ... }"
msgstr "B<rup> { -u | -v | I<hosts> ... }"

#. #-#-#-#-#  debian-bookworm: rup.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  debian-unstable: rup.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  fedora-42: rup.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Sh
#. #-#-#-#-#  fedora-rawhide: rup.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Sh
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "DESCRIPTION"
msgstr "LEÍRÁS"

#. type: Plain text
#: debian-bookworm debian-unstable
#, fuzzy
msgid ""
"B<rup> displays a summary of the current status of a particular host or all "
"hosts on the local network."
msgstr ""
"Az B<rup> összefoglalást ír ki egy adott B<host> vagy a lokális hálózaton "
"lévő összes host pillanatnyi állapotáról.  A kimenetről leolvashatjuk a "
"jelenlegi időt, hogy milyen rég óta működik a gép (\"uptime\") és a terhelés "
"átlagát (\"load average\").  Az átlagos terhelésnél leovasott számok "
"megadják a \"run queue\"-ban lévő processzek számát 1, 5 és 15 percre "
"átlagolva."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The output shows how long the system has been up, the number of users "
"currently on the system (if the system is running the current version of "
"B<rpc.rstatd(8)>), and the load averages."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The load average numbers give the number of jobs in the run queue averaged "
"over 1, 5 and 15 minutes."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The B<rpc.rstatd(8)> daemon must be running on the remote host for this "
"command to work."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<rup> uses an RPC protocol defined in /usr/include/rpcsvc/rstat.x"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "COMMAND LINE OPTIONS"
msgstr "PARANCSSORI OPCIÓK"

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-v>"
msgstr "B<-v>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "prints the current version of rup and exits"
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-u>  "
msgstr "B<-u>  "

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"broadcasts for infornmation from machines capable of returning users "
"information implemented in the newest version of rpc.rstatd"
msgstr ""

#. #-#-#-#-#  debian-bookworm: rup.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  debian-unstable: rup.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  fedora-42: rup.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Sh
#. #-#-#-#-#  fedora-rawhide: rup.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Sh
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "DIAGNOSTICS"
msgstr "DIAGNOSZTIKA"

#. #-#-#-#-#  debian-bookworm: rup.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  debian-unstable: rup.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-42: rup.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: It
#. #-#-#-#-#  fedora-rawhide: rup.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: It
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "rup: RPC: Program not registered"
msgstr "rup: RPC: Program not registered"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "The B<rpc.rstatd(8)> daemon has not been started on the remote host."
msgstr "Az B<rpc.rstatd>(8) démont nem indították el a távoli hoston."

#. #-#-#-#-#  debian-bookworm: rup.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  debian-unstable: rup.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-42: rup.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: It
#. #-#-#-#-#  fedora-rawhide: rup.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: It
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "rup: RPC: Timed out"
msgstr "rup: RPC: Timed out"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"A communication error occurred.  Either the network is excessively "
"congested, or the B<rpc.rstatd(8)> daemon has terminated on the remote host."
msgstr ""
"Kommunikációs hiba történt. Vagy nagyon le van terhelve a hálózat, vagy az "
"B<rpc.rstatd>(8) démon meghalt a távoli gépen."

#. #-#-#-#-#  debian-bookworm: rup.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  debian-unstable: rup.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-42: rup.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: It
#. #-#-#-#-#  fedora-rawhide: rup.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: It
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "rup: RPC: Port mapper failure - RPC: Timed out"
msgstr "rup: RPC: Port mapper failure - RPC: Timed out"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The remote host is not running the portmapper (see B<rpc.rstatd(8)>)  and "
"cannot accomodate any RPC-based services.  The host may be down."
msgstr ""
"A távoli gépen nem fut a portmapper (lásd B<portmap>(8)), így nem tud RPC "
"alapú szolgáltatásokat nyújtani. Lehet hogy a host nem működik."

#. #-#-#-#-#  debian-bookworm: rup.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  debian-unstable: rup.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  fedora-42: rup.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Sh
#. #-#-#-#-#  fedora-rawhide: rup.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Sh
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "SEE ALSO"
msgstr "LÁSD MÉG"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<portmap(8)>, B<rpc.rstatd(8)>, B<inetd(8)>"
msgstr "B<portmap(8)>, B<rpc.rstatd(8)>, B<inetd(8)>"

#. #-#-#-#-#  debian-bookworm: rup.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  debian-unstable: rup.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  fedora-42: rup.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Sh
#. #-#-#-#-#  fedora-rawhide: rup.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Sh
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "HISTORY"
msgstr "TÖRTÉNET"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "The B<rup> command appeared in B<SunOS>."
msgstr "Az B<rup> parancs B<SunOS>-ben jelent meg."

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "BUGS"
msgstr "HIBÁK"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "The sorting options are not implemented."
msgstr "A rendező opciók még nincsenek implementálva."

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "AUTHOR"
msgstr "SZERZŐ"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "Adam Migus (amigus@cs.mun.ca)"
msgstr "Adam Migus (amigus@cs.mun.ca)"

#. type: Dd
#: fedora-42 fedora-rawhide
#, no-wrap
msgid "August 15, 1999"
msgstr "1999. augusztus 15."

#. type: Dt
#: fedora-42 fedora-rawhide
#, no-wrap
msgid "RUP 1"
msgstr "RUP 1"

#. type: Os
#: fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux NetKit (0.17)"
msgstr "Linux NetKit (0.17)"

#. type: Plain text
#: fedora-42 fedora-rawhide
msgid "E<.Nm rup >"
msgstr "E<.Nm rup >"

#. type: Nd
#: fedora-42 fedora-rawhide
#, no-wrap
msgid "remote status display"
msgstr "távoli állapot kijelző"

#. type: Plain text
#: fedora-42 fedora-rawhide
msgid "E<.Nm rup > E<.Op Fl dshlt> E<.Op Ar host ...>"
msgstr "E<.Nm rup > E<.Op Fl dshlt> E<.Op Ar host ...>"

#. type: Plain text
#: fedora-42 fedora-rawhide
msgid ""
"E<.Nm rup> displays a summary of the current system status of a particular "
"E<.Em host > or all hosts on the local network.  The output shows the "
"current time of day, how long the system has been up, and the load "
"averages.  The load average numbers give the number of jobs in the run queue "
"averaged over 1, 5 and 15 minutes."
msgstr ""
"Az E<.Nm rup> összefoglalást ír ki egy adott E<.Em host > vagy a lokális "
"hálózaton lévő összes host pillanatnyi állapotáról.  A kimenetről "
"leolvashatjuk a jelenlegi időt, hogy milyen rég óta működik a gép "
"(\"uptime\") és a terhelés átlagát (\"load average\").  Az átlagos "
"terhelésnél leovasott számok megadják a \"run queue\"-ban lévő processzek "
"számát 1, 5 és 15 percre átlagolva."

#. type: Plain text
#: fedora-42 fedora-rawhide
msgid "The following options are available:"
msgstr "A következő opciók használhatók:"

#. type: It
#: fedora-42 fedora-rawhide
#, no-wrap
msgid "Fl d"
msgstr "Fl d"

#. type: Plain text
#: fedora-42 fedora-rawhide
msgid ""
"For each host, report what its local time is.  This is useful for checking "
"time syncronization on a network."
msgstr ""
"Minden host lokális idejét jelzi ki. Ez akkor hasznos, ha ellenőrizni "
"akarjuk a hálózaton történő idő szinkronizálást."

#. type: It
#: fedora-42 fedora-rawhide
#, no-wrap
msgid "Fl s"
msgstr "Fl s"

#. type: Plain text
#: fedora-42 fedora-rawhide
msgid ""
"Print time data in seconds (seconds of uptime or seconds since the epoch), "
"for scripts."
msgstr ""

#. type: It
#: fedora-42 fedora-rawhide
#, no-wrap
msgid "Fl h"
msgstr "Fl h"

#. type: Plain text
#: fedora-42 fedora-rawhide
msgid "Sort the display alphabetically by host name."
msgstr "A kimenetet hostnevek alapján rendezi."

#. type: It
#: fedora-42 fedora-rawhide
#, no-wrap
msgid "Fl l"
msgstr "Fl l"

#. type: Plain text
#: fedora-42 fedora-rawhide
msgid "Sort the display by load average."
msgstr "A kimenetet terhelés alapján rendezi."

#. type: It
#: fedora-42 fedora-rawhide
#, no-wrap
msgid "Fl t"
msgstr "Fl t"

#. type: Plain text
#: fedora-42 fedora-rawhide
msgid "Sort the display by up time."
msgstr "A kimentet az eltelt \"uptime\" azaz működési idő alapján rendezi."

#. type: Plain text
#: fedora-42 fedora-rawhide
msgid ""
"The E<.Xr rpc.rstatd 8> daemon must be running on the remote host for this "
"command to work.  E<.Nm rup> uses an RPC protocol defined in /usr/include/"
"rpcsvc/rstat.x."
msgstr ""
"Ahhoz hogy ez a parancs működjön, az E<.Xr rpc.rstatd 8> démonnak futnia "
"kell a távoli gépeken.  Az E<.Nm rup> egy RPC protokolt használ, melyet a /"
"usr/include/rpcsvc/rstat.x fájlban defináltak."

#. type: Sh
#: fedora-42 fedora-rawhide
#, no-wrap
msgid "EXAMPLE"
msgstr "PÉLDA"

#. type: Plain text
#: fedora-42 fedora-rawhide
#, no-wrap
msgid ""
"example% rup otherhost\n"
"otherhost      up 6 days, 16:45,  load average: 0.20, 0.23, 0.18\n"
"example%\n"
msgstr ""
"example% rup otherhost\n"
"otherhost      up 6 days, 16:45,  load average: 0.20, 0.23, 0.18\n"
"example%\n"

#. type: Plain text
#: fedora-42 fedora-rawhide
msgid "The E<.Xr rpc.rstatd 8> daemon has not been started on the remote host."
msgstr "Az E<.Xr rpc.rstatd 8> démont nem indították el a távoli hoston."

#. type: Plain text
#: fedora-42 fedora-rawhide
msgid ""
"A communication error occurred.  Either the network is excessively "
"congested, or the E<.Xr rpc.rstatd 8> daemon has terminated on the remote "
"host."
msgstr ""
"Kommunikációs hiba történt. Vagy nagyon le van terhelve a hálózat, vagy az "
"E<.Xr rpc.rstatd 8> démon meghalt a távoli gépen."

#. type: Plain text
#: fedora-42 fedora-rawhide
msgid ""
"The remote host is not running the portmapper (see E<.Xr portmap 8 ),> and "
"cannot accomodate any RPC-based services.  The host may be down."
msgstr ""
"A távoli gépen nem fut a portmapper (lásd E<.Xr portmap 8 ),> így nem tud "
"RPC alapú szolgáltatásokat nyújtani. Lehet hogy a host nem működik."

#. type: Plain text
#: fedora-42 fedora-rawhide
msgid "E<.Xr ruptime 1>, E<.Xr portmap 8>, E<.Xr rpc.rstatd 8>"
msgstr "E<.Xr ruptime 1>, E<.Xr portmap 8>, E<.Xr rpc.rstatd 8>"

#. type: Plain text
#: fedora-42 fedora-rawhide
msgid "The E<.Nm rup> command appeared in E<.Tn SunOS>."
msgstr "Az E<.Nm rup> parancs E<.Tn SunOS>-ben jelent meg."
